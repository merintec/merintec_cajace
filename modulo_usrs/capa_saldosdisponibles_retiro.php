<?PHP 
echo '<meta charset="ISO-8859-1">';
	include_once('../comunes/conexion_basedatos.php');
	include_once('../comunes/formularios_funciones.php');
	$cedula = $_POST[cedu_soci];
    $sql = "SELECT cedu_soci, ";
    $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_ing, ";
    $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_egr, "; 
    $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_ing, ";
    $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_egr, "; 
    $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_ing, ";
    $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_egr, ";
    $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='AP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_p_ing, ";
    $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='RP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_p_ing, ";
    $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='IP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_p_ing ";
    $sql .= "FROM socios_movimientos sm WHERE cedu_soci='".$cedula."' GROUP BY sm.cedu_soci ORDER BY codg_movi";
    $res = mysql_fetch_array(mysql_query($sql));

    if ($res['apr_ing']==NULL){ $res['apr_ing'] = 0; }
    if ($res['ret_ing']==NULL){ $res['ret_ing'] = 0; }
    if ($res['rei_ing']==NULL){ $res['rei_ing'] = 0; }
    if ($res['apr_egr']==NULL){ $res['apr_egr'] = 0; }
    if ($res['ret_egr']==NULL){ $res['ret_egr'] = 0; }
    if ($res['rei_egr']==NULL){ $res['rei_egr'] = 0; }
    if ($res['apr_p_ing']==NULL){ $res['apr_p_ing'] = 0; }
    if ($res['ret_p_ing']==NULL){ $res['ret_p_ing'] = 0; }
    if ($res['rei_p_ing']==NULL){ $res['rei_p_ing'] = 0; }

    $saldo_apr = $res['apr_ing']-$res['apr_egr'];
    $saldo_ret = $res['ret_ing']-$res['ret_egr'];
    $saldo_rei = $res['rei_ing']-$res['rei_egr'];
    $saldo_apr_p = $res['apr_p_ing'];
    $saldo_ret_p = $res['ret_p_ing'];
    $saldo_rei_p = $res['rei_p_ing'];

    if ($_POST[tipo_reti]=='R'){
        $saldo_apr = 0;
        $saldo_ret = 0;
    }


    ////// Prestamos pendientes por pagar
    $fecha_max = date('Y-m-d');
    $cuenta_p = 1;
        $sql_prestamos = "SELECT *, IF ((SELECT SUM(capi_prtm) from prestamos_mov WHERE codg_prst = p.codg_prst AND freg_prtm <= '".$fecha_max."' GROUP BY codg_prst),(SELECT SUM(capi_prtm) from prestamos_mov WHERE codg_prst = p.codg_prst AND freg_prtm <= '".$fecha_max."' GROUP BY codg_prst),0) AS pagado FROM prestamos p, tipo_prestamos tp WHERE p.codg_tipo_pres = tp.codg_tipo_pres AND p.cedu_soci = '".$cedula."' AND p.fcha_acta < '".$fecha_max."'  GROUP BY p.codg_prst HAVING (pagado) <= p.mont_apro";
        $bus_prestamos = mysql_query($sql_prestamos);
        while($res_prestamos = mysql_fetch_array($bus_prestamos)){
            $prestamos[$cuenta_p] = $res_prestamos;
            if ($res_prestamos[sald_inic_real] > 0){
                $prestamos[$cuenta_p][mont_apro] = $res_prestamos[sald_inic_real];
                $prestamos[$cuenta_p][mont_cuot] = $res_prestamos[mont_cuot_real];
            }
            $sql_fianza = "SELECT * FROM prestamos_fia WHERE codg_prst = ".$res_prestamos[codg_prst];
            $bus_fianza = mysql_query($sql_fianza);
            $fianza = '<ul>';
            while ($res_fianza = mysql_fetch_array($bus_fianza)) {
                $fianza .= '<li>';
                if ($res_fianza[desc_fian]) { 
                    $fianza .= $res_fianza[desc_fian].'.'; 
                } 
                else { 
                    $sql_fiador = "SELECT * FROM socios WHERE cedu_soci = ".$res_fianza[cedu_soci];
                    $bus_fiador = mysql_query($sql_fiador);
                    if ($res_fiador = mysql_fetch_array($bus_fiador)){
                        $fianza .= $res_fiador[apel_soci].' '.$res_fiador[nomb_soci].'.<br>C.I.&nbsp;'.redondear($res_fianza[cedu_soci],0,'.',',');    
                    }
                }
                $fianza .= '<br><b>Monto Bs:&nbsp;'.redondear($res_fianza[mont_fian],2,'.',',').'</b>';
                $fianza .= '</li>';
            }
            $fianza .= '</ul>';
            $prestamos_fianza[$cuenta_p] = $fianza;
            $total_prestamos += redondear(($prestamos[$cuenta_p][mont_apro] - $prestamos[$cuenta_p][pagado]),2,'','.'); 
            $cuenta_p += 1;
        }

        ////// Prestamos pendientes por pagar en los que sirvi� de fiador
	    $fecha_max = date('Y-m-d');
	    $cuenta_f = 1;
        $sql_prestamos = "SELECT *, IF ((SELECT SUM(capi_prtm) from prestamos_mov WHERE codg_prst = p.codg_prst GROUP BY codg_prst),(SELECT SUM(capi_prtm) from prestamos_mov WHERE codg_prst = p.codg_prst GROUP BY codg_prst),0) AS pagado FROM prestamos p, prestamos_fia pf, tipo_prestamos pt, socios s WHERE pf.cedu_soci = ".$cedula." AND pf.codg_prst=p.codg_prst AND p.codg_tipo_pres=pt.codg_tipo_pres AND pf.cedu_soli = s.cedu_soci GROUP BY pf.codg_prst HAVING (pagado) < p.mont_apro";
        $bus_prestamos = mysql_query($sql_prestamos);
        while($res_prestamos = mysql_fetch_array($bus_prestamos)){
            $prestamosf[$cuenta_f] = $res_prestamos;
            if ($res_prestamos[sald_inic_real] > 0){
                $prestamosf[$cuenta_f][mont_apro] = $res_prestamos[sald_inic_real];
                $prestamosf[$cuenta_f][mont_cuot] = $res_prestamos[mont_cuot_real];
            }
            $total_fianzas += redondear(($prestamosf[$cuenta_f][mont_fian]),2,'','.'); 
            $cuenta_f += 1;
        }
        // calculando el monto disponible para retiro para cada saldo
	    if ($_POST[tipo_reti]=='P'){
	    	$PRM = buscar_campo('val_val', 'valores', 'WHERE des_val="PRM"' );
	    	$PRM = $PRM[0];
	    	$saldo_apr = redondear(($saldo_apr * $PRM),2,'','.');
	    	$saldo_ret = redondear(($saldo_ret * $PRM),2,'','.');
		    $monto_bloqueado = redondear(($total_fianzas + $total_prestamos),2,'','.');
		    //// verificamos si el saldo bloqueado es menor o igual que la suma de los diponible por aporte y retencion
		    $total_disponible = redondear(($saldo_apr + $saldo_ret),2,'','.');
		    if ($monto_bloqueado<=$total_disponible){
				$monto_bloqueado_repartido = redondear(($monto_bloqueado/2),2,'','.');
				if($saldo_apr>=$monto_bloqueado_repartido && $saldo_ret>=$monto_bloqueado_repartido){
					$saldo_apr = redondear(($saldo_apr - $monto_bloqueado_repartido),2,'','.');
		    		$saldo_ret = redondear(($saldo_ret - $monto_bloqueado_repartido),2,'','.');
				}
				elseif ($saldo_apr<$monto_bloqueado_repartido ) {
					$monto_bloqueado = $monto_bloqueado - $saldo_apr;
					$saldo_apr = 0;
					$saldo_ret = $saldo_ret - $monto_bloqueado;
				}
				elseif ($saldo_ret<$monto_bloqueado_repartido ) {
					$monto_bloqueado = $monto_bloqueado - $saldo_ret;
					$saldo_ret = 0;
					$saldo_apr = $saldo_apr - $monto_bloqueado;
				}
		    }
		    //// si no se cumple que bloqueado sea menor o igual que Aporte + Retencion, comprobamos sumandole los reintegros
		    else{
		    	$total_disponible = redondear(($total_disponible + $saldo_rei),2,'','.');
		    	if ($monto_bloqueado<$total_disponible){
		    		$monto_bloqueado = redondear(($monto_bloqueado - (redondear(($saldo_ret + $saldo_apr),2,'','.'))),2,'','.');
		    		if ($monto_bloqueado<=$saldo_rei){
		    			$saldo_rei = redondear(($saldo_rei - $monto_bloqueado),2,'','.');
		    		}
		    	}
		    	$saldo_ret = 0;
		    	$saldo_apr = 0;
		    }
	    }
	    $saldo_tot = redondear(($saldo_ret + $saldo_apr + $saldo_rei),2,'','.');

?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".datos").keyup(function()
        {
        	var aporte = Number($('#mont_ret_apr').val());
        	var aporte_dis = Number($('#mont_apr').val());
        	var retenc = Number($('#mont_ret_ret').val());
        	var retenc_dis = Number($('#mont_ret').val());
        	var reinte = Number($('#mont_ret_rei').val());
        	var reinte_dis = Number($('#mont_rei').val());
        	if (aporte > aporte_dis){
        		alert("El monto por Aporte es mayor al disponible");		
        	}
        	if (retenc > retenc_dis){
        		alert("El monto por Ahorros es mayor al disponible");		
        	}
        	if (reinte > reinte_dis){
        		alert("El monto por Reintegro es mayor al disponible");		
        	}
        	var total = Number(aporte + retenc + reinte);
        	total = total.toFixed(2);
        	$('#mont_ret_tot').val(total);
        	recalcular();
        });
    });
</script>
<table border="1px" cellpadding="0" cellspacing="0" class="etiquetas" style="width: 100%; text-align: center;">
	<tr>
		<td colspan="7" class="titulo">Consulta de Montos</td>
	</tr>
	<tr>
		<td colspan="4">Montos Disponibles:</td><td colspan="3">Montos por pagar:</td>
	</tr>
	<tr>
		<td>Ahorros</td><td>Aportes</td><td>Reintegros</td><td>TOTAL</td><td>Ahorros</td><td>Aportes</td><td>Reintegros</td>
	</tr>
	<tr align="right">
		<td><?php echo redondear($saldo_ret,2,'.',','); ?>&nbsp;<input type="hidden" size="7px" id="mont_ret" value="<?php echo redondear($saldo_ret,2,'','.'); ?>" style="text-align: right;"></td>
		<td><?php echo redondear($saldo_apr,2,'.',','); ?>&nbsp;<input type="hidden" size="7px" id="mont_apr" value="<?php echo redondear($saldo_apr,2,'','.'); ?>" style="text-align: right;"></td>
		<td><?php echo redondear($saldo_rei,2,'.',','); ?>&nbsp;<input type="hidden" size="7px" id="mont_rei" value="<?php echo redondear($saldo_rei,2,'','.'); ?>" style="text-align: right;"></td>
		<td><?php echo redondear($saldo_tot,2,'.',','); ?>&nbsp;<input type="hidden" size="7px" id="mont_tot" value="<?php echo redondear($saldo_tot,2,'','.'); ?>" style="text-align: right;"></td>
		<td><?php echo redondear($saldo_ret_p,2,'.',','); ?>&nbsp;</td>
		<td><?php echo redondear($saldo_apr_p,2,'.',','); ?>&nbsp;</td>
		<td><?php echo redondear($saldo_rei_p,2,'.',','); ?>&nbsp;</td>
	</tr>
</table>
<br>
<table border="1px" cellpadding="0" cellspacing="0" class="etiquetas" style="width: 100%; text-align: center;">
	<tr>
		<td colspan="4" class="titulo">Indique los Montos a Retirar</td>
	</tr>
	<tr>
		<td>Ahorros</td><td>Aportes</td><td>Reintegros</td><td>TOTAL</td>
	</tr>
	<tr align="right">
		<td><input type="text" size="10px" name="mont_ret_ret" id="mont_ret_ret" value="<?php echo redondear($saldo_ret,2,'','.'); ?>" style="text-align: right; border: 0px;" class="datos"></td>
		<td><input type="text" size="10px" name="mont_ret_apr" id="mont_ret_apr" value="<?php echo redondear($saldo_apr,2,'','.'); ?>" style="text-align: right; border: 0px;" class="datos"></td>
		<td><input type="text" size="10px" name="mont_ret_rei" id="mont_ret_rei" value="<?php echo redondear($saldo_rei,2,'','.'); ?>" style="text-align: right; border: 0px;" class="datos"></td>
		<td><input type="text" size="10px" name="mont_ret_tot" id="mont_ret_tot" value="<?php echo redondear($saldo_tot,2,'','.'); ?>" style="text-align: right; border: 0px;" readonly="readonly"></td>
	</tr>
</table>