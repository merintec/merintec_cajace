<?php 
	$codg_depn = $_POST['codg_depn'];
	include('../comunes/conexion_basedatos.php');
	include ('../comunes/comprobar_inactividad.php');
	include ('../comunes/titulos.php');
	include ('../comunes/mensajes.php');
	if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } 
?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.nomina {
    font-family: arial;
    font-size: 10px;
}
.nomina_titulo {
    font-weight: bold;
    text-align: center;
    font-size: 12px;
}
-->
</style>
<script type="text/javascript">
    function bloqueada (estado) {
        if (estado=='S'){
            document.getElementById("bloqueo_img_no").style.display="none";
            document.getElementById("bloqueo_img_si").style.display="";
        }
        if (estado==''){
            document.getElementById("bloqueo_img_no").style.display="";
            document.getElementById("bloqueo_img_si").style.display="none";
        }
    }     
    </script>
<?php 
	include ('../comunes/formularios_funciones.php');
	/// cargamos el valor de los porcentajes de retenci�n, aportes y montepio
    $sql_val="select * from valores WHERE des_val='PR' OR des_val='PA' OR des_val='PMPIO'";
    $res_val = mysql_query($sql_val);
    while ($row_val = mysql_fetch_array($res_val))
    {
        // el nombre de la variable es $SUSTRAC_ISRL
        $$row_val['des_val'] = $row_val['val_val'];
    }
            
	$prm = llamar_permisos ($_GET["seccion"]);
	$porcien_apr = $PA;
	$porcien_ret = $PR;
	$porcien_mpio = $PMPIO;
	if ($_POST['bloquear']=="SI"){
	    $sql_bloqueo = "UPDATE nominas SET lock_nomina = 'S' WHERE codg_nmna =".$_POST['bloqueo'].";"; 
	    mysql_query($sql_bloqueo);
	}
	if ($_POST['bloquear']=="NO"){
	    $sql_bloqueo = "UPDATE nominas SET lock_nomina = '' WHERE codg_nmna =".$_POST['bloqueo'].";";
	    mysql_query($sql_bloqueo); 
	}
	if ($_POST['accion']=="Insertar" && $_POST['mnto_dlle']!='' && $_POST['mnto_dlle']>0){
	    $sentencia_sql = "INSERT INTO nominas_detalle (codg_dlle,codg_nmna,moti_dlle,rela_dlle,mnto_dlle,cedu_soci,codg_pago) VALUES (NULL,";
	    $sentencia_sql .= $_POST['codg_nmna'].',';
	    $sentencia_sql .= '"'.$_POST['moti_dlle'].'",';
	    $sentencia_sql .= '"'.$_POST['rela_dlle'].'",';
	    $sentencia_sql .= '"'.$_POST['mnto_dlle'].'",';
	    $sentencia_sql .= '"'.$_POST['cedu_soci'].'",';
	    $sentencia_sql .= 'NULL)';
	    mysql_query($sentencia_sql);
	    if ($_POST['moti_dlle']=="Aporte" || $_POST['moti_dlle']=="Retenci�n" || $_POST['moti_dlle']=="Reintegro"){
	        /// Registrar un movimiento para el socio
	        $mov_fecha = date('Y-m-d');
	        $dest_movi = $_POST['moti_dlle'][0];
	        // en el caso de reintegro se cambia el des_movi a I para evitar confusion entre R de Retencion y R de Reintegro
	        if ($_POST['moti_dlle']=="Reintegro"){
	            $dest_movi = "I";        
	        }
	        if ($_POST['porci_sel']==8){
	            $concepto_add = 'Mes';
	        }
	        if ($_POST['porci_sel']>=6 && $_POST['porci_sel']<=7){
	            $concepto_add = ($_POST['porci_sel']-5).'� Quincena';
	        }
	        if ($_POST['porci_sel']>10){
	            $concepto_add = ($_POST['porci_sel']-10).'� Semana';
	        }
	        $sql_movimiento = "INSERT INTO socios_movimientos (codg_movi,cedu_soci,fcha_movi,conc_movi,tipo_movi,dest_movi,mont_movi,orig_movi)";
	        $sql_movimiento.=" VALUES (NULL, '".$_POST['cedu_soci']."', '".$mov_fecha."', '".$_POST['moti_dlle']." por Registro de Nomina (".$concepto_add." ".$_POST['mes_sel']."/".$_POST['ano_sel'].")', 'I', '".$dest_movi."', ".$_POST['mnto_dlle'].", 'A');";	        
	        mysql_query($sql_movimiento);
	        /// Actualizaci�n de Saldo del socio
	        $sql_saldo = "UPDATE socios_saldo SET ";
	        $sql_saldo_cons = "SELECT * FROM socios_saldo WHERE cedu_soci='".$_POST['cedu_soci']."'";
	        $reg_saldo_cons = mysql_fetch_array(mysql_query($sql_saldo_cons)); 
	        if ($_POST['moti_dlle']=='Aporte') {
	            $monto_movi = $reg_saldo_cons['apor_comp']+$_POST['mnto_dlle'];
 	            $sql_saldo .= "apor_comp = ".$monto_movi;
  	            $sql_saldo .= " WHERE cedu_soci='".$_POST['cedu_soci']."'";
	        }
	        if ($_POST['moti_dlle']=='Retenci�n' || $_POST['moti_dlle']=='Reintegro') {
	            $monto_movi = $reg_saldo_cons['rete_comp']+$_POST['mnto_dlle'];
 	            $sql_saldo .= "rete_comp = ".$monto_movi;
  	            $sql_saldo .= " WHERE cedu_soci='".$_POST['cedu_soci']."'";
	        }
	        mysql_query($sql_saldo);
	        
	    }
	}
	if ($_POST['accion']=="Eliminar"){
	    $sentencia_sql = "DELETE FROM nominas_detalle WHERE codg_dlle=".$_POST['codg_dlle'];
	    mysql_query($sentencia_sql);
	    /// Eliminar el Movimiento
	        $dest_movi = $_POST['moti_dlle'][0];
      	        // en el caso de reintegro se cambia el des_movi a I para evitar confusion entre R de Retencion y R de Reintegro
	        if ($_POST['moti_dlle']=="Reintegro"){
	            $dest_movi = "I";        
	        }
	        if ($_POST['porci_sel']==8){
	            $concepto_add = 'Mes';
	        }
	        if ($_POST['porci_sel']>=6 && $_POST['porci_sel']<=7){
	            $concepto_add = ($_POST['porci_sel']-5).'� Quincena';
	        }
	        if ($_POST['porci_sel']<=5){
	            $concepto_add = ($_POST['porci_sel']-10).'� Semana';
	        }
  	        $sql_movimiento = "DELETE FROM socios_movimientos WHERE 
  	        cedu_soci = '".$_POST['cedu_soci']."' AND 
  	        conc_movi = '".$_POST['moti_dlle']." por Registro de Nomina (".$concepto_add." ".$_POST['mes_sel']."/".$_POST['ano_sel'].")' AND
  	        tipo_movi = 'I' AND 
  	        dest_movi = '".$dest_movi."' AND 
  	        mont_movi = ".$_POST['mnto_dlle']." AND 
  	        orig_movi = 'A';";
  	        mysql_query($sql_movimiento);
	    /// Actualizaci�n de Saldo del socio
	        $sql_saldo = "UPDATE socios_saldo SET ";
	        $sql_saldo_cons = "SELECT * FROM socios_saldo WHERE cedu_soci='".$_POST['cedu_soci']."'";
	        $reg_saldo_cons = mysql_fetch_array(mysql_query($sql_saldo_cons)); 
	        if ($_POST['moti_dlle']=='Aporte') {
	            $monto_movi = $reg_saldo_cons['apor_comp']-$_POST['mnto_dlle'];
 	            $sql_saldo .= "apor_comp = ".$monto_movi;
  	            $sql_saldo .= " WHERE cedu_soci='".$_POST['cedu_soci']."'";
	        }
	        if ($_POST['moti_dlle']=='Retenci�n' || $_POST['moti_dlle']=='Reintegro') {
	            $monto_movi = $reg_saldo_cons['rete_comp']-$_POST['mnto_dlle'];
 	            $sql_saldo .= "rete_comp = ".$monto_movi;
  	            $sql_saldo .= " WHERE cedu_soci='".$_POST['cedu_soci']."'";
	        }
	        mysql_query($sql_saldo);
	}
?>
<form id="form1" name="form1" method="post" action="">
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
   	<tr><td>&nbsp;</td></tr>
   	<tr>
      	<td class="titulo" colspan="2">
      		REGISTRO DE NOMINAS 	
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td align="center">
			<span class="etiquetas">Dependencia: </span><?php combo('codg_depn', $codg_depn, 'dependencias', $link, 0, 0, 1, '', 'codg_depn', " title='Seleccione la Dependencia que desea procesar' OnChange='submit()' ", $boton, "", ""); ?>
			<br><br><span class="etiquetas">
			    Mes:
			            <?php if (! $_POST['mes_sel']) { $mes_sel=date("m"); } else {$mes_sel = $_POST['mes_sel'];} ?>
                        <select name="mes_sel" title="Mes de nomina a procesar" onchange="submit();">
                            <?php 
                                for ($i=1;$i<=12;$i++){
                                    echo '<option value="'.$i.'" '; if($mes_sel==$i){ echo 'selected'; } echo '>'.convertir_mes($i).'</option>';
                                }
                            ?>
                        </select>
                A�o:
                        <?php if (! $_POST['ano_sel']) { $ano_sel=date("y"); } else {$ano_sel = $_POST['ano_sel'];} ?>
            			<select name="ano_sel"  title="A&ntilde;o de nomina a procesar" onchange="submit();">
						  	<?php
								$ano_act=date("Y");
								for($i=$ano_act;$i>2010;$i--){
									echo"<option value='$i'";
										if($i==$ano_sel){ echo "selected";}
									echo">A�o: $i</option>";
							} ?>
                        </select>
			    Porci�n: 
			            <?php
			                if ($_POST["codg_depn"]){
			        // calculamos los viernes        
                                $viernes_t=calculo_viernes ($mes_sel,$ano_sel);
                                $viernes=$viernes_t[0];
                                // consultamos las nominas disponibles para la dependencia
                                $sql_porci = "SELECT * FROM dependencias WHERE codg_depn=".$_POST["codg_depn"];
                                if ($reg_proci = mysql_fetch_array(mysql_query($sql_porci))){
                                    if ($reg_proci["peri_sema"]==S){
                                        $dato_semana = semanas($_POST["ano_sel"].'-'.$_POST["mes_sel"].'-01');
                                        $week_ini = $dato_semana['num_semana']-1;
                                        $week_fin = $dato_semana['num_semana']+5;
                                        $options_porci_sem = '';
                                        for ($week=$week_ini;$week<=$week_fin;$week++){
                                            $options_porci_sem .= '<option value="'.($week+10).'"'; if ($_POST["porci_sel"]==($week+10)) {$options_porci_sem .= 'selected'; } $options_porci_sem .= '>Semana '.$week.'�</option>';
                                        }
                                    }
                                    if ($reg_proci["peri_quin"]==S){
                                        $options_porci_qui = '<option value="6"'; if ($_POST["porci_sel"]==6) {$options_porci_qui .= 'selected'; } $options_porci_qui .= '>Quincena 1�</option>';
                                        $options_porci_qui .= '<option value="7"'; if ($_POST["porci_sel"]==7) {$options_porci_qui .= 'selected'; } $options_porci_qui .= '>Quincena 2�</option>';
                                    }
                                    if ($reg_proci["peri_mens"]==S){
                                        $options_porci_mes = '<option value="8"'; if ($_POST["porci_sel"]==8) {$options_porci_mes .= 'selected'; } $options_porci_mes .= '>Mensual</option>';
                                    }
                                }  		                
			                }
			            ?>
			            <select name="porci_sel"  title="Proci�n de nomina a procesar" onchange="submit();">
			                <option value="0">Seleccione...</option>
			                <?php echo $options_porci_sem.$options_porci_qui.$options_porci_mes; ?>
						</select>
			    </span>
		</td>
		<td align="center" width="100px">
		    <input type="image" src="../imagenes/check.png" id="bloqueo_img_no" name="bloqueo_img_no" value="" title="Bloquear Nomina" Onclick="valor_acampo('SI','bloquear');" style="display: none">
		    <input type="image" src="../imagenes/cerrar.png" id="bloqueo_img_si" name="bloqueo_img_si" value="" title="Bloquear Nomina" Onclick="valor_acampo('NO','bloquear');" style="display: none">
		    <input type="hidden" name="bloqueo" id="bloqueo" value="">
		    <input type="hidden" name="bloquear" id="bloquear" value="">
		</td>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
	</table>
	<?php if ($_POST['codg_depn']!=0 && $_POST['mes_sel']!=0 && $_POST['ano_sel']!=0 && $_POST['porci_sel']!=0) { 
	    ////// verificamos si ya existe el registro de nomina 
	    $sql_nomina = "SELECT * FROM nominas WHERE codg_depn=".$_POST['codg_depn']." AND mess_nmna =".$_POST['mes_sel']." AND anno_nmna=".$_POST['ano_sel']." AND prdo_nmna=".$_POST['porci_sel'];
	    $res_nomina = mysql_query($sql_nomina);
	    ////// si existe nos traemos el codigo de la nomina
	    if ($reg_nomina = mysql_fetch_array($res_nomina)){
	        $codg_nmna = $reg_nomina["codg_nmna"];
	        if ($reg_nomina["lock_nomina"]=='S' || $reg_nomina["lock_pagos"]=='S' ){
	            $lock_nmna = 'S';
	        }
	        else { $lock_nmna = ''; }
	    } 
	    ////// si no la creamos un registro y nos traemos con que codg_nmna se insert� 
	    else{
	        $sql_nomina_ins = 'INSERT INTO nominas (anno_nmna,mess_nmna,prdo_nmna,codg_depn) VALUES ('.$_POST["ano_sel"].', '.$_POST["mes_sel"].', '.$_POST["porci_sel"].', '.$_POST["codg_depn"].');';
	        mysql_query($sql_nomina_ins); 
	        $codg_nmna=mysql_insert_id(); 
	    }
	    echo "<script>bloqueada('".$lock_nmna."');</script>";
	    echo "<script>valor_acampo('".$codg_nmna."','bloqueo');</script>";
	    ////// datos de los detalles
	        $cuenta_pers = 0;
	        $perio = "";
	        if ($_POST['porci_sel']>=11 && $_POST['porci_sel']<=62){
	                $perio = "S";
	                $semana = semana($_POST["ano_sel"],($_POST["porci_sel"]-10));
      	                $fecha_max = $semana['ultimo_dia'];
	        }
	        if ($_POST['porci_sel']==6){
	                $perio = "Q";
	                $fecha_max = $_POST['ano_sel'].'-'.$_POST['mes_sel'].'-15';
	        }
	        if ($_POST['porci_sel']==7){
	                $perio = "Q";
	                $fecha_max = $_POST['ano_sel'].'-'.$_POST['mes_sel'].'-'.dias_mes ($_POST['mes_sel'],$_POST['ano_sel']);
	        }
	        if ($_POST['porci_sel']==8){
	                $perio = "M";
	                $fecha_max = $_POST['ano_sel'].'-'.$_POST['mes_sel'].'-'.dias_mes ($_POST['mes_sel'],$_POST['ano_sel']);
	        }
            $sql_pers = "SELECT * FROM  socios_dat_lab sl, socios s, socios_estado se WHERE sl.codg_depn=".$_POST["codg_depn"]." AND sl.peri_suel='".$perio."' AND sl.cedu_soci=s.cedu_soci AND sl.codg_dlab IN (SELECT MAX(codg_dlab) FROM socios_dat_lab WHERE cedu_soci=s.cedu_soci AND fchi_dlab<='".$fecha_max."') AND s.cedu_soci=se.cedu_soci AND se.codg_stat='2' AND se.codg_estd=(SELECT MAX(codg_estd) FROM socios_estado WHERE cedu_soci=s.cedu_soci AND fcha_estd<='".$fecha_max."') ORDER BY s.apel_soci,s.nomb_soci";
            $res_pers = mysql_query($sql_pers);
            while ($reg_pers = mysql_fetch_array($res_pers)){
                $datos_pers[$cuenta_pers][0]=$reg_pers["cedu_soci"];
                $datos_pers[$cuenta_pers][1]=$reg_pers["apel_soci"]."&nbsp;".$reg_pers["nomb_soci"];
                /// calculamos los montos de aportes y retenciones
                    $monto_aporte = redondear(($reg_pers["suel_dlab"]*$porcien_apr),2,"",".");
                    $monto_retenc = redondear(($reg_pers["suel_dlab"]*$porcien_ret),2,"",".");
                    if ($reg_pers["retn_dlab"]!=0.00){
                       $monto_retenc = redondear(($reg_pers["suel_dlab"]*$reg_pers["retn_dlab"]),2,"",".");
                    }
                    if ($reg_pers["aprt_dlab"]!=0.00){
                       $monto_aporte = redondear(($reg_pers["suel_dlab"]*$reg_pers["aprt_dlab"]),2,"",".");
                    }
                    $datos_pers[$cuenta_pers][2]=$monto_aporte;
                    $datos_pers[$cuenta_pers][3]=$monto_retenc;
                    /// consultamos si existe un registro de aporte en nominas_detalle
                        $sql_aporte = "SELECT * FROM nominas_detalle WHERE codg_nmna=".$codg_nmna." AND moti_dlle='Aporte' AND rela_dlle='' AND cedu_soci=".$reg_pers["cedu_soci"];
                        if ($reg_aporte = mysql_fetch_array(mysql_query($sql_aporte))){
                            $datos_aportes[$cuenta_pers] = $reg_aporte["codg_dlle"];
                            $aporte_pago[$cuenta_pers] = $reg_aporte["codg_pago"];
                            $total_aportes += $reg_aporte["mnto_dlle"];
                            if ($reg_aporte["mnto_dlle"]!=$monto_aporte){
                                $change_color_aprt[$cuenta_pers] = "S";
                            }
                        }
                        else { $datos_aportes[$cuenta_pers] = ''; }
                    /// consultamos si existe un registro de retencion en nominas_detalle
                        $sql_retencion = "SELECT * FROM nominas_detalle WHERE codg_nmna=".$codg_nmna." AND moti_dlle='Retenci�n' AND rela_dlle='' AND cedu_soci=".$reg_pers["cedu_soci"];
                        if ($reg_retencion = mysql_fetch_array(mysql_query($sql_retencion))){
                            $datos_retencion[$cuenta_pers] = $reg_retencion["codg_dlle"];
                            $retencion_pago[$cuenta_pers] = $reg_retencion["codg_pago"];
                            $total_retenciones += $reg_retencion["mnto_dlle"];
                            if ($reg_retencion["mnto_dlle"]!=$monto_retenc){
                                $change_color_retn[$cuenta_pers] = "S";
                            }
                        }
                        else { $datos_retencion[$cuenta_pers] = ''; }
                /// consultamos los prestamos que tenga el socio
                    $sql_prestamo = "SELECT * FROM prestamos WHERE cedu_soci=".$reg_pers["cedu_soci"]." AND stat_prst='A' AND fcha_acta<='".$fecha_max."' AND codg_prst NOT IN (SELECT codg_prst_paga FROM prestamos WHERE fcha_acta<='".$fecha_max."');";
                    $res_prestamo = mysql_query($sql_prestamo);
                    $nprestamos = 0;
                    while ($reg_prestamo = mysql_fetch_array($res_prestamo)){
                        //// verificamos cuantos registros tiene el prestamo en Abono y en nominas
                            if ($reg_prestamo["codg_prst"]!=''){
                                $codg_prst=$reg_prestamo["codg_prst"];
                                $plaz_prst = buscar_campo('plaz_prst', 'prestamos', 'WHERE codg_prst='.$codg_prst);  
		                $tipo_cuot = buscar_campo('tipo_cuot', 'prestamos', 'WHERE codg_prst='.$codg_prst); 
		                if($tipo_cuot['tipo_cuot']=="S"){ $cant=$plaz_prst['plaz_prst']*4; }elseif($tipo_cuot['tipo_cuot']=="Q"){ $cant=$plaz_prst['plaz_prst']*2; }else{ $cant=$plaz_prst['plaz_prst']; }
		                if ($reg_prestamo["paga_cuot"]>0){
		                   $cant = $reg_prestamo["paga_cuot"];
		                }
		                $cont_nomin = buscar_campo('COUNT(rela_dlle) AS cont_nomin ', 'nominas_detalle', 'WHERE moti_dlle="Pr�stamo" AND rela_dlle='.$codg_prst);
		                $cont_abono = buscar_campo('SUM(numr_cuot) AS cont_abono ', 'abonos', 'WHERE codg_prst='.$codg_prst.' GROUP BY codg_prst');
		                $resta = $cant - $cont_abono['cont_abono'] - $cont_nomin['cont_nomin'].'<br>';
		                $cont_nomin_act = buscar_campo('COUNT(rela_dlle) AS cont_nomin_act ', 'nominas_detalle', 'WHERE moti_dlle="Pr�stamo" AND rela_dlle='.$codg_prst.' AND codg_nmna='.$codg_nmna);		      
                            }
                        if ($resta>=1 || $cont_nomin_act['cont_nomin_act'] > 0){
                                $datos_prestamo[$cuenta_pers][$nprestamos+1]=$reg_prestamo["mont_cuot"]; 
                                $datos_prestamo2[$cuenta_pers][$nprestamos+1]=$reg_prestamo["codg_prst"];
                                if ($reg_prestamo["mont_cuot_real"]!=0.00){
                                   $datos_prestamo[$cuenta_pers][$nprestamos+1]=$reg_prestamo["mont_cuot_real"]; 
                                   $datos_prestamo2[$cuenta_pers][$nprestamos+1]=$reg_prestamo["codg_prst"];
                                }  
                                ////// verificamos si existe un registro de este en nominas_detalle
                                $sql_pres_en_detalle = "SELECT * FROM nominas_detalle WHERE codg_nmna=".$codg_nmna." AND moti_dlle='Pr�stamo' AND rela_dlle=".$reg_prestamo["codg_prst"]." AND cedu_soci=".$reg_pers["cedu_soci"];
                                if ($reg_pres_en_detalle = mysql_fetch_array(mysql_query($sql_pres_en_detalle))) {
                                    $datos_prestamo3[$cuenta_pers][$nprestamos+1] = $reg_pres_en_detalle["codg_dlle"];
                                    $prestamo_pago[$cuenta_pers][$nprestamos+1] = $reg_pres_en_detalle["codg_pago"];
                                    $total_prestamos += $reg_pres_en_detalle["mnto_dlle"];                                    
                                }
                                else { $datos_prestamo3[$cuenta_pers][$nprestamos+1]= ''; }
                                $nprestamos++;
                        }
                    }
                    $datos_prestamo[$cuenta_pers][0]=$nprestamos;
                /// Consultamos los montepio que tenga el socio
                    $sql_montepio = "SELECT ms.mnto_mpio,ms.codg_mpsc,s.nomb_soci,apel_soci FROM montepio m, montepio_socios ms, socios s WHERE m.codg_mpio=ms.codg_mpio AND ms.cedu_soci=".$reg_pers["cedu_soci"]." AND s.cedu_soci=m.cedu_soci AND (YEAR(fcha_mpio)<=".$_POST['ano_sel']." AND MONTH(fcha_mpio)<=".$_POST['mes_sel'].") AND codg_mpsc NOT IN (SELECT rela_dlle FROM nominas_detalle WHERE moti_dlle='MONTEPIO' AND codg_nmna!=".$codg_nmna.");";
                    $res_montepio = mysql_query($sql_montepio);
                    $nmpio = 0;
                    while ($reg_montepio = mysql_fetch_array($res_montepio)){
                        $datos_mpio[$cuenta_pers][$nmpio+1]=$reg_montepio["mnto_mpio"]; 
                        $datos_mpio2[$cuenta_pers][$nmpio+1]=$reg_montepio["codg_mpsc"]; 
                        $datos_mpio4[$cuenta_pers][$nmpio+1]=$reg_montepio["nomb_soci"].'&nbsp;'.$reg_montepio["apel_soci"]; 
                        ////// verificamos si existe un registro de este en nominas_detalle
                        $sql_mpio_en_detalle = "SELECT * FROM nominas_detalle WHERE codg_nmna=".$codg_nmna." AND moti_dlle='MONTEPIO' AND rela_dlle=".$reg_montepio["codg_mpsc"]." AND cedu_soci=".$reg_pers["cedu_soci"];
                        if ($reg_mpio_en_detalle = mysql_fetch_array(mysql_query($sql_mpio_en_detalle))) {
                            $datos_mpio3[$cuenta_pers][$nmpio+1] = $reg_mpio_en_detalle["codg_dlle"];
                            $mpio_pago[$cuenta_pers][$nmpio+1] = $reg_mpio_en_detalle["codg_pago"];
                            $total_mpios += $reg_mpio_en_detalle["mnto_dlle"];
                        }
                        else { $datos_mpio3[$cuenta_pers][$nmpio+1]= ''; }
                        $nmpio++;
                    }
                    $datos_mpio[$cuenta_pers][0]=$nmpio;
                    /// consultamos si existe un registro de reintegro en nominas_detalle
                        $sql_reintegro = "SELECT * FROM nominas_detalle WHERE codg_nmna=".$codg_nmna." AND moti_dlle='Reintegro' AND mnto_dlle>0 AND cedu_soci=".$reg_pers["cedu_soci"];
                        if ($reg_reintegro = mysql_fetch_array(mysql_query($sql_reintegro))){
                            $datos_reintegro[$cuenta_pers][0] = $reg_reintegro["mnto_dlle"];
                            $datos_reintegro[$cuenta_pers][1] = $reg_reintegro["codg_dlle"];
                            $datos_reintegro[$cuenta_pers][2] = $reg_reintegro["codg_pago"];
                            $total_reintegros += $reg_reintegro["mnto_dlle"];
                        }
                        else {  $datos_reintegro[$cuenta_pers][0] = '';
                                $datos_reintegro[$cuenta_pers][1] = '';
                                $datos_reintegro[$cuenta_pers][2] = '';
                             }
                /////////////////////////////////////////////////
                $cuenta_pers++;   
            }
    ?>    
    <input type="hidden" name="accion" id="accion" value="">
    <input type="hidden" name="codg_dlle" id="codg_dlle" value="" size="8">
    <input type="hidden" name="codg_nmna" id="codg_nmna" value="<?php echo $codg_nmna; ?>" size="8">
    <input type="hidden" name="moti_dlle" id="moti_dlle" value="" size="8">
    <input type="hidden" name="rela_dlle" id="rela_dlle" value="" size="8">
    <input type="hidden" name="mnto_dlle" id="mnto_dlle" value="" size="8">
    <input type="hidden" name="cedu_soci" id="cedu_soci" value="" size="8">
	<table width="99%" cellspacing="0" cellpadding="0" align="center"  style="border-collapse:collapse;" border="1" bordercolor="#000000" class="nomina">
        <tr class="nomina_titulo"><td width="20px">&nbsp;N�&nbsp;</td><td width="70px">C�dula</td><td>Apellidos y Nombres</td><td width="75px">Aporte</td><td width="75px">Retenci�n</td><td width="75px">Prestamo</td><td width="75px">MONTEPIO</td><td width="75px">REINTEGRO</td></tr>
        <?php
            for ($i=0;$i<$cuenta_pers;$i++){
                echo '<tr class="nomina_titulo"><td align="right">'.($i+1).'&nbsp;</td><td align="right">'.redondear(($datos_pers[$i][0]),0,".",",").'&nbsp;</td><td align="left">&nbsp;'.$datos_pers[$i][1].'</td>';
                echo '<td align="right">';
                if ($change_color_aprt[$i]){
                        echo '<font color="#FF0000">'.redondear(($datos_pers[$i][2]),2,".",",").'</font>';
                }
                else {
                        echo redondear(($datos_pers[$i][2]),2,".",",");                
                }
                if ($datos_aportes[$i]==''){
                    $pasar_datos = "valor_acampo('Insertar','accion');";
                    $pasar_datos .= "valor_acampo('Aporte','moti_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_pers[$i][2]."','mnto_dlle');";
                    $pasar_datos .= "valor_acampo('','rela_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_pers[$i][0]."','cedu_soci');";
                    $pasar_datos .= "submit();";
                    $checked='';
                    $tooltip = "Haga click para marcar";
                }
                else {
                    $pasar_datos = "valor_acampo('Eliminar','accion');";
                    $pasar_datos .= "valor_acampo('".$datos_aportes[$i]."','codg_dlle');";
                    $pasar_datos .= "valor_acampo('Aporte','moti_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_pers[$i][2]."','mnto_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_pers[$i][0]."','cedu_soci');";
                    $pasar_datos .= "submit();";
                    $checked='checked';
                    $tooltip = "Haga click para desmarcar";
                } 
                if ($lock_nmna=='S'){
                    $pasar_datos = "alert('La Nomina Se encuentra Bloqueada');";
                    $pasar_datos .= "submit();";
                    $tooltip = "Los cambios no son permitidos. Debido a Bloqueo de Nomina.";
                }
                if ($aporte_pago[$i]!='' && $lock_nmna!='S'){
                    $pasar_datos = "alert('Debido a que ya tiene registrado un pago.".'\n'."El movimiento se encuentra Bloqueado.');";
                    $pasar_datos .= "submit();";
                    $tooltip = "Los cambios no son permitidos, debido a que ya se encuentra pagado.";
                }
                $func_aporte = 'OnClick="'.$pasar_datos.'"';
                echo '<input type="checkbox" name="aporte_id'.$i.$j.'" value="" title="'.$tooltip.'" '.$func_aporte.' '.$checked.'>';
                echo '</td><td align="right">';
                if ($change_color_retn[$i]){
                        echo '<font color="#FF0000">'.redondear(($datos_pers[$i][3]),2,".",",").'</font>';
                }
                else {
                        echo redondear(($datos_pers[$i][3]),2,".",",");                
                }
                if ($datos_retencion[$i]==''){
                    $pasar_datos = "valor_acampo('Insertar','accion');";
                    $pasar_datos .= "valor_acampo('Retenci�n','moti_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_pers[$i][3]."','mnto_dlle');";
                    $pasar_datos .= "valor_acampo('','rela_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_pers[$i][0]."','cedu_soci');";
                    $pasar_datos .= "submit();";
                    $checked='';
                    $tooltip = "Haga click para marcar";
                }
                else {
                    $pasar_datos = "valor_acampo('Eliminar','accion');";
                    $pasar_datos .= "valor_acampo('".$datos_retencion[$i]."','codg_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_pers[$i][2]."','mnto_dlle');";
                    $pasar_datos .= "valor_acampo('Retenci�n','moti_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_pers[$i][0]."','cedu_soci');";
                    $pasar_datos .= "submit();";
                    $checked='checked';
                    $tooltip = "Haga click para desmarcar";
                } 
                if ($lock_nmna=='S'){
                    $pasar_datos = "alert('La Nomina Se encuentra Bloqueada');";
                    $pasar_datos .= "submit();";
                    $tooltip = "Los cambios no son permitidos. Debido a Bloqueo de Nomina.";
                }
                if ($retencion_pago[$i]!='' && $lock_nmna!='S'){
                    $pasar_datos = "alert('Debido a que ya tiene registrado un pago.".'\n'."El movimiento se encuentra Bloqueado.');";
                    $pasar_datos .= "submit();";
                    $tooltip = "Los cambios no son permitidos, debido a que ya se encuentra pagado.";
                }
                $func_retencion = 'OnClick="'.$pasar_datos.'"';
                echo '<input type="checkbox" name="aporte_id'.$i.$j.'" value="" title="'.$tooltip.'" '.$func_retencion.' '.$checked.'>';
                echo '</td>';
                echo '<td align="right">'; 
                        for ($j=1;$j<=$datos_prestamo[$i][0];$j++){
                            if ($datos_prestamo3[$i][$j]==''){
                                $pasar_datos = "valor_acampo('Insertar','accion');";
                                $pasar_datos .= "valor_acampo('Pr�stamo','moti_dlle');";
                                $pasar_datos .= "valor_acampo('".$datos_prestamo[$i][$j]."','mnto_dlle');";
                                $pasar_datos .= "valor_acampo('".$datos_prestamo2[$i][$j]."','rela_dlle');";
                                $pasar_datos .= "valor_acampo('".$datos_pers[$i][0]."','cedu_soci');";
                                $pasar_datos .= "submit();";
                                $checked='';
                                $tooltip = "Haga click para marcar";
                            }
                            else {
                                $pasar_datos = "valor_acampo('Eliminar','accion');";
                                $pasar_datos .= "valor_acampo('".$datos_prestamo3[$i][$j]."','codg_dlle');";
                                $pasar_datos .= "submit();";
                                $checked='checked';
                                $tooltip = "Haga click para desmarcar";
                            } 
                            if ($lock_nmna=='S'){
                                $pasar_datos = "alert('La Nomina Se encuentra Bloqueada');";
                                $pasar_datos .= "submit();";
                                $tooltip = "Los cambios no son permitidos. Debido a Bloqueo de Nomina.";
                            }
                            if ($prestamo_pago[$i][$j]!='' && $lock_nmna!='S'){
                                $pasar_datos = "alert('Debido a que ya tiene registrado un pago.".'\n'."El movimiento se encuentra Bloqueado.');";
                                $pasar_datos .= "submit();";
                                $tooltip = "Los cambios no son permitidos, debido a que ya se encuentra pagado.";
                            }
                            $func_checkbox = 'OnClick="'.$pasar_datos.'"';
                            echo redondear($datos_prestamo[$i][$j],2,".",",");
                            echo '<input type="checkbox" name="prestamo_id'.$i.$j.'" value="'.$datos_prestamo2[$i][$j].'" title="'.$tooltip.'" '.$func_checkbox.' '.$checked.'>
                            <img style="cursor: pointer;" src="../imagenes/editar.png" width="14px" id="editar" name="editar" value="editar" title="Editar">
                            <br>';
                        }
                echo'</td>';
                ////// Montepio ////
                echo '<td align="right">'; 
                        for ($j=1;$j<=$datos_mpio[$i][0];$j++){
                            if ($datos_mpio3[$i][$j]==''){
                                $pasar_datos = "valor_acampo('Insertar','accion');";
                                $pasar_datos .= "valor_acampo('MONTEPIO','moti_dlle');";
                                $pasar_datos .= "valor_acampo('".$datos_mpio[$i][$j]."','mnto_dlle');";
                                $pasar_datos .= "valor_acampo('".$datos_mpio2[$i][$j]."','rela_dlle');";
                                $pasar_datos .= "valor_acampo('".$datos_pers[$i][0]."','cedu_soci');";
                                $pasar_datos .= "submit();";
                                $checked='';
                                $tooltip = "Haga click para marcar";
                            }
                            else {
                                $pasar_datos = "valor_acampo('Eliminar','accion');";
                                $pasar_datos .= "valor_acampo('".$datos_mpio3[$i][$j]."','codg_dlle');";
                                $pasar_datos .= "submit();";
                                $checked='checked';
                                $tooltip = "Haga click para desmarcar";
                            } 
                            if ($lock_nmna=='S'){
                                $pasar_datos = "alert('La Nomina Se encuentra Bloqueada');";
                                $pasar_datos .= "submit();";
                                $tooltip = "Los cambios no son permitidos. Debido a Bloqueo de Nomina.";
                            }
                            if ($mpio_pago[$i][$j]!='' && $lock_nmna!='S'){
                                $pasar_datos = "alert('Debido a que ya tiene registrado un pago.".'\n'."El movimiento se encuentra Bloqueado.');";
                                $pasar_datos .= "submit();";
                                $tooltip = "Los cambios no son permitidos, debido a que ya se encuentra pagado.";
                            }
                            $func_checkbox = 'OnClick="'.$pasar_datos.'"';
                            echo '<span title="Socio:&nbsp;'.$datos_mpio4[$i][$j].'">'.redondear($datos_mpio[$i][$j],2,".",",")."</span>";
                            echo '<input type="checkbox" name="mpio_id'.$i.$j.'" value="'.$datos_mpio2[$i][$j].'" title="'.$tooltip.'" '.$func_checkbox.' '.$checked.'><br>';
                        }
                echo'</td>'; 
                //Reintegros
                echo '<td width="85px" align="right">';
                if ($datos_reintegro[$i][0]==''){
                    $pasar_datos = "valor_acampo('Insertar','accion');";
                    $pasar_datos .= "valor_acampo('Reintegro','moti_dlle');";
                    $pasar_datos .= "valor_acampo(reintegro".$i.$j.".value,'mnto_dlle');";
                    $pasar_datos .= "valor_acampo('','rela_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_pers[$i][0]."','cedu_soci');";
                    $pasar_datos .= "submit();";
                    $checked='';
                    $tooltip = "Haga click para marcar";
                }
                else {
                    $pasar_datos = "valor_acampo('Eliminar','accion');";
                    $pasar_datos .= "valor_acampo('".$datos_reintegro[$i][1]."','codg_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_reintegro[$i][0]."','mnto_dlle');";
                    $pasar_datos .= "valor_acampo('Reintegro','moti_dlle');";
                    $pasar_datos .= "valor_acampo('".$datos_pers[$i][0]."','cedu_soci');";
                    $pasar_datos .= "submit();";
                    $checked='checked';
                    $tooltip = "Haga click para desmarcar";
                } 
                if ($lock_nmna=='S'){
                    $pasar_datos = "alert('La Nomina Se encuentra Bloqueada');";
                    $pasar_datos .= "submit();";
                    $tooltip = "Los cambios no son permitidos. Debido a Bloqueo de Nomina.";
                }
                if ($datos_reintegro[$i][2]!='' && $lock_nmna!='S'){
                    $pasar_datos = "alert('Debido a que ya tiene registrado un pago.".'\n'."El movimiento se encuentra Bloqueado.');";
                    $pasar_datos .= "submit();";
                    $tooltip = "Los cambios no son permitidos, debido a que ya se encuentra pagado.";
                }
                $func_reintegro = 'OnClick="'.$pasar_datos.'"';
                if ($datos_reintegro[$i][0]>0){
                        echo redondear($datos_reintegro[$i][0],2,'.',',');                        
                }
                else {
                        echo '<input type="text" name="reintegro'.$i.$j.'" value="'.$datos_reintegro[$i][0].'" size="5">';
                }
                echo '<input type="checkbox" name="reintegro_id'.$i.$j.'" value="" title="'.$tooltip.'" '.$func_reintegro.' '.$checked.'>';
                echo '</td>';
                echo '</tr>';
            }
        ?>
        <tr class="nomina_titulo">
                <td colspan="3">TOTALES MARCADOS</td>
                <td align="right"><?php echo redondear($total_aportes,2,'.',','); ?>&nbsp;</td>
                <td align="right"><?php echo redondear($total_retenciones,2,'.',','); ?>&nbsp;</td>
                <td align="right"><?php echo redondear($total_prestamos,2,'.',','); ?>&nbsp;</td>
                <td align="right"><?php echo redondear($total_mpios,2,'.',','); ?>&nbsp;</td>
                <td align="right"><?php echo redondear($total_reintegros,2,'.',','); ?>&nbsp;</td>
        </tr>     	    
   	</table>
   	<?php } ?>
</form>
