<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Datos Laborales de Ahorristas</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cedu_soci'];
include ('../comunes/formularios_funciones.php');
$codg_depn=$_POST['codg_depn'];
$codg_tcrg=$_POST['codg_tcrg'];
$peri_suel=$_POST['peri_suel'];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'socios_datos_lab.php?cedu_soci='.$_GET["cedu_soci"].'&seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$pagina = 'socios_datos_lab.php?cedu_soci='.$_GET["cedu_soci"].'&seccion='.$_GET["seccion"];
$tabla = "socios_dat_lab";
$ncampos = "9";
$datos[0] = crear_datos ("cedu_soci","C�dula",$_POST['cedu_soci'],"1","12","numericos");
$datos[1] = crear_datos ("codg_depn","Dependencia",$_POST['codg_depn'],"1","11","numericos");
$datos[2] = crear_datos ("carg_dlab","Cargo",$_POST['carg_dlab'],"1","50","alfanumericos");
$datos[3] = crear_datos ("codg_tcrg","Tipo de Cargo",$_POST['codg_tcrg'],"1","11","numericos");
$datos[4] = crear_datos ("fchi_dlab","Fecha",$_POST['fchi_dlab'],"1","10","fecha");
$datos[5] = crear_datos ("peri_suel","Tipo de Periodo",$_POST['peri_suel'],"1","1","alfabeticos");
$datos[6] = crear_datos ("suel_dlab","Sueldo",$_POST['suel_dlab'],"1","12","decimal");
$datos[7] = crear_datos ("retn_dlab","Procentaje de Retencion",$_POST['retn_dlab'],"0","5","decimal");
$datos[8] = crear_datos ("aprt_dlab","Procentaje de Aporte",$_POST['aprt_dlab'],"0","5","decimal");

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_dlab = $row["codg_dlab"];
	    $cedu_soci = $row["cedu_soci"];
	    $codg_depn = $row["codg_depn"];
	    $carg_dlab = $row["carg_dlab"];
	    $codg_tcrg = $row["codg_tcrg"];
	    $fchi_dlab = $row["fchi_dlab"];
		$peri_suel = $row["peri_suel"];
		$suel_dlab = $row["suel_dlab"];
		$retn_dlab = $row["retn_dlab"];
		$aprt_dlab = $row["aprt_dlab"];
	    if ($retn_dlab==0.00){
		$retn_dlab = '';
	    }
	    if ($aprt_dlab==0.00){
		$aprt_dlab='';
	    }
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_dlab",$_POST["codg_dlab"],$pagina2);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar";
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_dlab"],"codg_dlab",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"codg_dlab","socios_dat_lab",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="600" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Datos Laborales de 
                    	<?php $sql_socio = "SELECT * FROM socios WHERE cedu_soci=".$viene_val; 
                    	$res_socio = mysql_fetch_array(mysql_query($sql_socio));
                    	echo $res_socio[apel_soci].' '.$res_socio[nomb_soci];
                    	?>
                    </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Dependencias:</td>
                        <td width="75%">
							<?php escribir_campo('codg_dlab',$_POST["codg_dlab"],$codg_dlab,'readonly',12,15,'Codigo del Dato Laboral',$boton,$existe,'','','oculto'); ?>
							<?php escribir_campo('cedu_soci',$viene_val,$cedu_soci,'readonly',12,15,'Cedula del Ahorrista',$boton,$existe,'','','oculto'); ?>
							<?php combo('codg_depn', $codg_depn, 'dependencias', $link, 0, 0, 1, '', 'codg_depn', " onchange='submit();' ", $boton, "", ""); ?>
						</td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Tipo&nbsp;de&nbsp;Periodo:</td>
                        <td>
							<?PHP if ($boton != "Modificar"){ 
										echo '<select name="peri_suel"><option value="">Seleccione...</option>';
										$sql_peri="select peri_sema, peri_quin, peri_mens from dependencias where codg_depn=$codg_depn";
										$busq_peri=mysql_query($sql_peri);
										$reg_peri=mysql_fetch_array($busq_peri);										
										if($reg_peri['peri_sema']=="S"){ echo "<option value='S' "; if($peri_suel=="S"){ echo "selected"; } echo " >Semanal</option>"; }
										if($reg_peri['peri_quin']=="S"){ echo "<option value='Q' "; if($peri_suel=="Q"){ echo "selected"; } echo " >Quincenal</option>"; }
										if($reg_peri['peri_mens']=="S"){ echo "<option value='M' "; if($peri_suel=="M"){ echo "selected"; } echo " >Mensual</option>"; }
										echo '</select>';	
									}else{ 
											echo '<input type="hidden" name="peri_suel" id="peri_suel" value="'.$peri_suel.'" >'; 
											if($peri_suel == "S"){ echo 'Semanal'; } 
											if($peri_suel == "Q"){ echo 'Quincenal'; }
											if($peri_suel == "M"){ echo 'Mensual'; }
									}
								?>
							
						</td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Cargo:</td>
                        <td>
							<?php escribir_campo('carg_dlab',$_POST["carg_dlab"],$carg_dlab,'',50,30,'Cargo',$boton,$existe,'','',''); ?>
						</td>
                      </tr>
					  <tr>
                        <td class="etiquetas">Tipo&nbsp;de&nbsp;Cargo:</td>
                        <td><?php combo('codg_tcrg', $codg_tcrg, 'tipo_cargo', $link, 0, 0, 1, '', 'codg_tcrg', "", $boton, "", ""); ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha&nbsp;de&nbsp;Ingreso:</td>
                        <td>
							<?php escribir_campo('fchi_dlab',$_POST["fchi_dlab"],$fchi_dlab,'',11,15,'Fecha de Movimiento',$boton,$existe,'fecha','',''); ?>
						</td>
					  </tr>
                      <tr>
                        <td class="etiquetas">Sueldo:</td>
                        <td>
							<?php escribir_campo('suel_dlab',$_POST["suel_dlab"],$suel_dlab,'',11,15,'Sueldo del Ahorrista',$boton,$existe,'','',''); ?>
						</td>
					  </tr>
		      <tr>
		        <td colspan="2" class="etiquetas" align="center">
		        <?php 
		        /// cargamos el valor de los porcentajes de retenci�n, aportes y montepio
                        $sql_val="select * from valores WHERE des_val='PR' OR des_val='PA'";
                        $res_val = mysql_query($sql_val);
                        while ($row_val = mysql_fetch_array($res_val))
                        {
                                // el nombre de la variable es $PR y $PA
                                $$row_val['des_val'] = $row_val['val_val'];
                        }
		        ?>
		          Puede que el Trabajador haya autorizado una retenci�n mayor a la establecida para todos (<?php echo $PR*100; ?>%).<br>
		          Tambi�n es posible que la Dependencia haya aprobado un aporte mayor al establecido para todos (<?php echo $PA*100; ?>%)<br><br>
		          Si es as�, por favor indique estos porcentajes en formato decimal (<?php echo $PR*100; ?>% equivale a <?php echo $PR; ?>)
		        </td>
		      </tr>
                      <tr>
                        <td class="etiquetas">Porcentaje&nbsp;de&nbsp;Retenci�n:</td>
                        <td>
							<?php escribir_campo('retn_dlab',$_POST["retn_dlab"],$retn_dlab,'',5,15,'Retenci�n expresada en numeros decimales',$boton,$existe,'','',''); ?>
						</td>
					  </tr>
                      <tr>
                        <td class="etiquetas">Porcentaje&nbsp;de&nbsp;Aporte:</td>
                        <td>
							<?php escribir_campo('aprt_dlab',$_POST["aprt_dlab"],$aprt_dlab,'',5,15,'Aporte expresado en numeros decimales',$boton,$existe,'','',''); ?>
						</td>
					  </tr>
		           </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_socios_datos_lab.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
