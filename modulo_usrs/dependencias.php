<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'dependencias.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "dependencias";	// nombre de la tabla
$ncampos = "9";			//numero de campos del formulario
$datos[0] = crear_datos ("nomb_depn","Nombre de la Dependencia",$_POST['nomb_depn'],"1","100","alfanumericos");
$datos[1] = crear_datos ("desc_depn","Descripcion de la Dependencia",$_POST['desc_depn'],"1","255","alfanumericos");
$datos[2] = crear_datos ("dirc_depn","Direccion de la Dependencia",$_POST['dirc_depn'],"1","255","alfanumericos");
$datos[3] = crear_datos ("nomb_auth","Nombre de la Autoridad",$_POST['nomb_auth'],"1","100","alfabeticos");
$datos[4] = crear_datos ("titu_auth","T�tulo de la Autoridad",$_POST['titu_auth'],"1","50","alfabeticos");
$datos[5] = crear_datos ("carg_auth","Cargo de la Autoridad",$_POST['carg_auth'],"1","50","alfabeticos");
$datos[6] = crear_datos ("peri_sema","Per�odo Semanal",$_POST['peri_sema'],"0","1","alfabeticos");
$datos[7] = crear_datos ("peri_quin","Per�odo Quincenal",$_POST['peri_quin'],"0","1","alfabeticos");
$datos[8] = crear_datos ("peri_mens","Per�odo Mensual",$_POST['peri_mens'],"0","1","alfabeticos");

if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Nombre";
		$datos[0]="nomb_depn";
		$parametro[1]="Descripci�n";
		$datos[1]="desc_depn";
		busqueda_varios(4,$buscando,$datos,$parametro,"codg_depn");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_depn = $row["codg_depn"];
		$nomb_depn = $row["nomb_depn"];
	    $desc_depn = $row["desc_depn"];
	    $dirc_depn = $row["dirc_depn"];
	    $nomb_auth = $row["nomb_auth"];
	    $titu_auth = $row["titu_auth"];
	    $carg_auth = $row["carg_auth"];
	    $peri_sema = $row["peri_sema"];
	    $peri_quin = $row["peri_quin"];
	    $peri_mens = $row["peri_mens"];
	    
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_depn",$_POST["codg_depn"],$pagina,"");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_depn"],"codg_depn",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de las Dependencias</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Nombre:</td><?php escribir_campo('codg_depn',$_POST["codg_depn"],$codg_depn,'',12,15,'Codigo de la Dependencia',$boton,$existe,'','','oculto');?>
                        <td width="75%"><?php escribir_campo('nomb_depn',$_POST["nomb_depn"],$nomb_depn,'',100,25,'Nombre de la Dependencia',$boton,$existe,'','','');?>
                        <?php if ($boton=="Modificar"){ echo "(C�digo interno: ".$codg_depn.")";}?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Descripci&oacute;n:</td>
                        <td width="75%"><?php echo escribir_area('desc_depn',$_POST["desc_depn"],$desc_depn,'',30,3,'Descripci&oacute;n de la Dependencia',$boton,$existe,'')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Direcci&oacute;n:</td>
                        <td width="75%"><?php echo escribir_area('dirc_depn',$_POST["dirc_depn"],$dirc_depn,'',30,3,'Direcci&oacute;n de la Dependencia',$boton,$existe,'')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Nombre de la Autoridad:</td>
                        <td width="75%"><?php escribir_campo('nomb_auth',$_POST["nomb_auth"],$nomb_auth,'',100,50,'Nombre de la Autoridad Principal',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Titulo de la Autoridad:</td>
                        <td width="75%"><?php escribir_campo('titu_auth',$_POST["titu_auth"],$titu_auth,'',50,50,'Titulo de la Autoridad Principal',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Cargo de la Autoridad:</td>
                        <td width="75%"><?php escribir_campo('carg_auth',$_POST["carg_auth"],$carg_auth,'',100,50,'Cargo de la Autoridad Principal',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N�minas que maneja:</td>
                        <td width="75%">
                            <input name="peri_sema" id="peri_sema" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$peri_sema.'" '; } else { echo 'type="checkbox" value="S"'; } if($_POST["peri_sema"]=='S' || $peri_sema == 'S') { echo 'checked'; } ?> title="N�minas Semanales">Semanales<?php if ($boton=='Modificar') { if ($peri_sema=='S') {echo ': <b>SI</b>'; } else { echo ': <b>NO</b>'; } } ?> 
                            <input name="peri_quin" id="peri_quin" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$peri_quin.'" '; } else { echo 'type="checkbox" value="S"'; } if($_POST["peri_quin"]=='S' || $peri_quin == 'S') { echo 'checked'; } ?> title="N�minas Quincenales">Quincenales<?php if ($boton=='Modificar') { if ($peri_quin=='S') {echo ': <b>SI</b>'; } else { echo ': <b>NO</b>'; } } ?> 
                            <input name="peri_mens" id="peri_mens" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$peri_mens.'" '; } else { echo 'type="checkbox" value="S"'; } if($_POST["peri_mens"]=='S' || $peri_mens == 'S') { echo 'checked'; } ?> title="N�minas Mensuales">Mensuales<?php if ($boton=='Modificar') { if ($peri_mens=='S') {echo ': <b>SI</b>'; } else { echo ': <b>NO</b>'; } } ?> 
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
					
                  <tr>
				  	<td>
					
					</td>
				  </tr>
				  <tr>
                    <td>
					<?php 
						$ncriterios =3;
						$criterios[0] = "Nombre"; 
						$campos[0] ="nomb_depn";
						$criterios[1] = "Descripci&oacute;n"; 
						$campos[1] ="desc_depn";
						$criterios[2] = "Direcci&oacute;n"; 
						$campos[2] ="dirc_depn";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
