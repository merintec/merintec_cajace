<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>

<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'parentescos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "parentescos";	// nombre de la tabla
$ncampos = "2";			//numero de campos del formulario
$datos[0] = crear_datos ("nomb_pare","Nombre de tipo de parentesco",$_POST['nomb_pare'],"1","50","alfabeticos");
$datos[1] = crear_datos ("desc_pare","Descripción",$_POST['desc_pare'],"1","255","alfanumericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Nombre";
		$datos[0]="nomb_pare";
		$parametro[1]="Descripcion";
		$datos[1]="desc_pare";
		busqueda_varios(4,$buscando,$datos,$parametro,"codg_pare");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_pare = $row["codg_pare"];
	    $nomb_pare = $row["nomb_pare"];
	    $desc_pare = $row["desc_pare"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_pare",$_POST["codg_pare"],$pagina,"");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	//establecer segun tipo de busqueda
	$boton=comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_pare"],"codg_pare",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo"> Parentescos </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Denominación:</td><?php escribir_campo('codg_pare',$_POST["codg_pare"],$codg_pare,'',12,15,'Codigo del parentesco',$boton,$existe,'','','oculto')?>
                        <td width="75%"><?php escribir_campo('nomb_pare',$_POST["nomb_pare"],$nomb_pare,'',50,30,'Denominación del Parentesco',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Descripción:</td>
                        <td width="75%"><?php echo escribir_area('desc_pare',$_POST["desc_pare"],$desc_pare,'',30,3,'Descripción del Parentesco',$boton,$existe,'')?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
					
                  <tr>
				  	<td>
					
					</td>
				  </tr>
				  <tr>
                    <td>
					<?php 
						$ncriterios =2;
						$criterios[0] = "Nombre"; 
						$campos[0] ="nomb_pare";
						$criterios[1] = "Descripción"; 
						$campos[1] ="desc_pare";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
