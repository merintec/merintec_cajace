<?php
include_once('../comunes/conexion_basedatos.php');
include_once('../comunes/formularios_funciones.php');

	////// mostrar las nominas que cuentan con exceso asociado a una cuenta por pagar
	$sql_porpagar = "SELECT *, (if((SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_exce = dpp.codg_exce), (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_exce = dpp.codg_exce),0)) as pagado, (if((SELECT SUM(debe_movi) FROM movimientos_contables WHERE codg_exce = dpp.codg_exce), (SELECT SUM(debe_movi) FROM movimientos_contables WHERE codg_exce = dpp.codg_exce),0)) as mas_debe, (if((SELECT SUM(haber_movi) FROM movimientos_contables WHERE codg_exce = dpp.codg_exce), (SELECT SUM(haber_movi) FROM movimientos_contables WHERE codg_exce = dpp.codg_exce),0)) as mas_haber, (if((SELECT SUM(mnto_debe) FROM egresos_conceptos WHERE codg_exce = dpp.codg_exce), (SELECT SUM(mnto_debe) FROM egresos_conceptos WHERE codg_exce = dpp.codg_exce),0)) as pag_debe, (if((SELECT SUM(mnto_hber) FROM egresos_conceptos WHERE codg_exce = dpp.codg_exce), (SELECT SUM(mnto_hber) FROM egresos_conceptos WHERE codg_exce = dpp.codg_exce),0)) as pag_haber FROM dependencias_pagos_exce dpp, nominas n, dependencias_pagos p, dependencias d WHERE dpp.nomb_cnta LIKE '%por Pagar%' AND dpp.codg_nmna=n.codg_nmna AND p.codg_depn = d.codg_depn ";
	if ($_POST["codg_depn"]){ 
		$sql_porpagar .= "AND n.codg_depn='".$_POST["codg_depn"]."' ";
	} 
	$sql_porpagar .= "AND dpp.nomb_cnta NOT LIKE '%montepio%' AND dpp.codg_pago=p.codg_pago HAVING pagado < mont_exce";
	$bus_porpagar = mysql_query($sql_porpagar);
	echo mysql_error();
	if (mysql_num_rows($bus_porpagar)>0){
		echo '<center><span class="etiquetas">Cuentas Pendientes por Pagar</span></center>';
		echo '<table width="99%" cellspacing="0" cellpadding="0" align="center"  style="border-collapse:collapse;" border="1" bordercolor="#000000" class="nomina">'; 
			echo '<tr class="nomina_titulo" style="color: #FFFFFF;" bgcolor="#67BABA">
				<td>#</td><td>Nomina</td><td>Cod. Cuenta</td><td>Nombre Cuenta</td><td>Monto</td><td>Sel.</td>
			</tr>
			<tr><td colspan="5" align="right">Ninguno&nbsp;</td><td align="center"><input type="radio" name="por_pagar" id="por_pagar" value=""></td></tr>';
	}
	$num_pagar = 1;
	while($res_porpagar = mysql_fetch_array($bus_porpagar)){
		switch ($res_porpagar[prdo_nmna]) {
			case 8:
				$porpagar_nom = 'Mes';
				break;
			case 7:
				$porpagar_nom = '2da Quincena';
				break;
			case 6:
				$porpagar_nom = '1era Quincena';
				break;
			case ($res_porpagar[prdo_nmna] > 10):
				$porpagar_nom = 'Semana '.($res_porpagar[prdo_nmna]-10).'�';
				break;
			default:
				# code...
				break;
		}
		$monto = ($res_porpagar[mont_exce]+$res_porpagar[mas_haber]+$res_porpagar[pag_haber]-$res_porpagar[pagado]-$res_porpagar[mas_debe]-$res_porpagar[pag_debe]);
		if ($monto!=0){		
			echo '<tr class="nomina_detalle" style="line-height: 20px;font-size:10px; text-align:left;">
					<td style="text-align:right; padding-left: 3px; padding-right: 3px;">'.$num_pagar.'</td>
					<td style="padding-left: 3px; padding-right: 3px;">'; 
					if (!$_POST[codg_depn]){
						echo $res_porpagar[nomb_depn].'<br>';
					}
					echo $porpagar_nom.' '.convertir_mes($res_porpagar[mess_nmna]).' '.$res_porpagar[anno_nmna].'</td>
					<td style="padding-left: 3px; padding-right: 3px;">'.$res_porpagar[nmro_cnta].'</td>
					<td style="padding-left: 3px; padding-right: 3px;">'.$res_porpagar[nomb_cnta].'</td>
					<td style="text-align:right; padding-left: 3px; padding-right: 3px;">'.redondear($monto,2,'.',',').'<input type="hidden" name="monto_exec_'.$res_porpagar[codg_exce].'" id="monto_exec_'.$res_porpagar[codg_exce].'" value="'.$monto.'"></td>
					<td style="text-align:center; padding-left: 3px; padding-right: 3px;"><input type="radio" name="por_pagar" id="por_pagar" value="'.$res_porpagar[codg_exce].'" '; if ($_POST[por_pagar]==$res_porpagar[codg_exce]){ echo ' checked=chequed '; } echo'></td>
				</tr>';
				$num_pagar++;
		}
	}
	if (mysql_num_rows($bus_porpagar)>0){
		echo '</table>'; 
	} 
?>