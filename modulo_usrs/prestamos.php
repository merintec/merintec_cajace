<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<SCRIPT>
function tipo_prestamo_plazo(valor, campo)
{
   var nuevo_valor;
   <?php 
      $sql_tipo_prst = "SELECT * FROM tipo_prestamos";
      $bus_tipo_prst = mysql_query($sql_tipo_prst);
      while ($res_tipo_prst=mysql_fetch_array($bus_tipo_prst)){
         $val_max=number_format($res_tipo_prst['valormax_tipo_pres'], 0, '', '');
         echo "if (valor=='".$res_tipo_prst['codg_tipo_pres']."') { 
           nuevo_valor = '".$val_max."'; 
           document.form1[campo].readOnly =  false; 
           document.form1[campo].focus(); 
         }
         ";
      }
   ?>
   if (valor=='') {
        document.form1[campo].readOnly =  false;
        nuevo_valor = '';
   }
   document.form1[campo].value = nuevo_valor;
}
</SCRIPT>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$error=false;
$agregar=$_POST['agregar'];
$cedu_soci=$_POST['cedu_soci'];
$cedu_fiad=$_POST['cedu_fiad'];
$codg_prst_paga=$_POST['codg_prst_paga'];
$id_prst_mov_paga=$_POST['id_prst_mov_paga'];
$max_pres=$_POST['max_pres'];
$saldo=$_POST['sum_saldo'];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$codg_tipo_pres = $_POST['codg_tipo_pres'];
$stat_prst = $_POST['stat_prst'];
$existe = '';
$pagina = 'prestamos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "prestamos";	// nombre de la tabla
$ncampos = "18";			//numero de campos del formulario
$datos[0] = crear_datos ("fcha_prst","Fecha de Solicitud",$_POST['fcha_prst'],"10","10","fecha");
$datos[1] = crear_datos ("cedu_soci","Ahorrista",$_POST['cedu_soci'],"1","12","numericos");
$datos[2] = crear_datos ("mont_prst","Monto Solicitado",$_POST['mont_prst'],"1","12","decimal");
$datos[3] = crear_datos ("nsol_prst","Num. de Solicitud",$_POST['nsol_prst'],"1","11","numericos");
$datos[4] = crear_datos ("codg_tipo_pres","Tipo de Pr�stamo",$_POST['codg_tipo_pres'],"1","1","numericos");
$datos[5] = crear_datos ("plaz_prst","Plazo del Pr�stamo",$_POST['plaz_prst'],"1","2","numericos");
$datos[6] = crear_datos ("obsr_prst","Observaciones",$_POST['obsr_prst'],"0","255","alfanumericos");
$datos[7] = crear_datos ("stat_prst","Estatus del Pr�stamo",$_POST['stat_prst'],"0","1","alfabeticos");
$datos[8] = crear_datos ("numr_acta","Num. de Acta",$_POST['numr_acta'],"0","50","alfanumericos");
$datos[9] = crear_datos ("fcha_acta","Fecha del Acta",$_POST['fcha_acta'],"0","10","fecha");
$datos[10] = crear_datos ("mont_apro","Monto Otorgado",$_POST['mont_apro'],"0","12","decimal");
$datos[11] = crear_datos ("tipo_cuot","Tipo de Cuotas",$_POST['tipo_cuot'],"0","1","alfabeticos");
$datos[12] = crear_datos ("mont_intr","Tasa de Inter�s",$_POST['mont_intr'],"0","12","decimal");
$datos[13] = crear_datos ("mont_cuot","Monto de las Cuotas",$_POST['mont_cuot'],"0","12","decimal");
$datos[14] = crear_datos ("sald_inic_real","Monto real de la cuotas",$_POST['sald_inic_real'],"0","12","decimal");
$datos[15] = crear_datos ("mont_cuot_real","Monto real de la cuotas",$_POST['mont_cuot_real'],"0","12","decimal");
$datos[16] = crear_datos ("paga_cuot","Cuotas pendientes por pagar",$_POST['paga_cuot'],"0","12","numericos");
$datos[17] = crear_datos ("codg_prst_paga","prestamo que paga",$_POST['codg_prst_paga'],"0","12","numericos");
$datos[18] = crear_datos ("capi_prst_paga","Capital del prestamo que paga",$_POST['capi_prst_paga'],"0","12","decimal");
$datos[19] = crear_datos ("inte_prst_paga","Intereses del prestamo que paga",$_POST['inte_prst_paga'],"0","12","decimal");
$datos[20] = crear_datos ("totl_prst_paga","Total del prestamo que paga",$_POST['totl_prst_paga'],"0","12","decimal");

if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Fecha";
		$datos[0]="fcha_prst";
		$parametro[1]="Ahorrista";
		$datos[1]="cedu_soci";
		$parametro[2]="N� Solicitud";
		$datos[2]="nsol_prst";
		$parametro[2]="Estatus";
		$datos[2]="stat_prst";
		busqueda_varios(5,$buscando,$datos,$parametro,"codg_prst");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_prst = $row["codg_prst"];
	    $cedu_soci = $row["cedu_soci"];
	    $mont_prst = $row["mont_prst"];
	    $fcha_prst = $row["fcha_prst"];
	    $nsol_prst = $row["nsol_prst"];
	    $codg_tipo_pres = $row["codg_tipo_pres"];
	    $plaz_prst = $row["plaz_prst"];
	    $obsr_prst = $row["obsr_prst"];
	    $stat_prst = $row["stat_prst"];
	    $numr_acta = $row["numr_acta"];
	    $fcha_acta = $row["fcha_acta"];
	    if ($fcha_acta=='0000-00-00') { $fcha_acta = ''; }
	    $mont_apro = $row["mont_apro"];
	    $tipo_cuot = $row["tipo_cuot"];
	    $mont_cuot = $row["mont_cuot"];
	    $mont_intr = $row["mont_intr"];
	    $mont_cuot_real = $row["mont_cuot_real"];
	    $sald_inic_real = $row["sald_inic_real"];
	    $paga_cuot = $row["paga_cuot"];
	    $codg_prst_paga = $row["codg_prst_paga"];
	    
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
    ////// calculamos el monto de cada cuota
    $monto_mensual=0;
	$monto_cuota=0;
	$deuda = $_POST['mont_apro'];
	$interes_calculo = redondear(($_POST['mont_intr']/12),2,"",".");
	if ($_POST['tipo_cuot']=='Q'){ 
	    $cuenta_for = $_POST['plaz_prst'] * 2;
	    $interes = ($interes_calculo/100)/2;
	}
	if ($_POST['tipo_cuot']=='S'){
	    $cuenta_for = $_POST['plaz_prst'] * 4;
	    $interes = ($interes_calculo/100)/4;
	}
	if ($_POST['tipo_cuot']=='M'){ 
	    $cuenta_for = $_POST['plaz_prst'];
	    $interes = ($interes_calculo/100);
	}
	$monto_mensual = $_POST['mont_apro']*(($interes*pow(1+$interes,$cuenta_for))/(pow(1+$interes,$cuenta_for)-1));
	$monto_mensual = redondear($monto_mensual,2,"",".");
    $monto_cuota = $monto_mensual;
    $datos[13] = crear_datos ("mont_cuot","Monto de las Cuotas",$monto_cuota,"0","12","decimal");
    ////////////////////////////////////////
	$validacion = validando_campos ($ncampos,$datos);
	//////////// validar monto maximo para pretamo
	if($max_pres<$datos[2][2]){
		echo "<script>alert('El Monto Solicitado no puede exceder el m�ximo estipulado para prestamos')</script>";
		$validacion=false;
		$boton = "Actualizar";	
	}
	if($datos[2][2]<$datos[10][2]){
		echo "<script>alert('El Monto Aprobado No puede exceder el monto solicitado.')</script>";
		$validacion=false;
		$boton = "Actualizar";	
	}
	if($max_pres<$datos[10][2]){
		echo "<script>alert('El Monto Aprobado No puede exceder el monto m�ximo permitido')</script>";
		$validacion=false;
		$boton = "Actualizar";	
	}
	////////////////////////////////////
	if ($validacion) {
	    $validacion= '';
	    if ($_POST[codg_tipo_pres]!=$_POST[tipo_prst_ant] || $_POST[plaz_prst]!=$_POST[plaz_prst_ant] || $_POST[mont_apro]!=$_POST[mont_apro_ant] || $_POST[tipo_cuot]!=$_POST[tipo_cuot_ant] || $_POST[mont_intr]!=$_POST[mont_intr_ant]){
	        $mensaje_add = "Recuerde imprimir nuevamente la Tabla de Amortizaci�n";
	        echo "<SCRIPT>alert('".$mensaje_add."')</SCRIPT>";	        
	    }
	    $validacion = 1;
	}
	if ($validacion) {	
	        if ($_POST['codg_prst_paga']!=$_POST['codg_prst_paga_aux']){
	                $sql_prst_mov = "DELETE FROM prestamos_mov WHERE rela_prtm=".$_POST['codg_prst']." AND orgn_prtm='Registro de Pr�stamo'";
                        mysql_query($sql_prst_mov);
                        if ($_POST['codg_prst_paga']){
                               $sql_prst_mov = "INSERT INTO prestamos_mov (codg_prst,mnto_prtm,capi_prtm,inte_prtm,orgn_prtm,rela_prtm,freg_prtm,fpag_prtm) values (".$_POST['codg_prst_paga'].",".$_POST['totl_prst_paga'].",".$_POST['capi_prst_paga'].",".$_POST['inte_prst_paga'].",'Registro de Pr�stamo',".$_POST["codg_prst"].",'".$_POST['fcha_acta']."','".$_POST['fcha_acta']."')";
                                mysql_query($sql_prst_mov);  
                        }
	        }
		  else {
				if ($_POST['codg_prst_paga']){
                               $sql_prst_mov = "UPDATE prestamos_mov SET mnto_prtm=".$_POST['totl_prst_paga'].", capi_prtm=".$_POST['capi_prst_paga'].", inte_prtm=".$_POST['inte_prst_paga'].", freg_prtm=".$_POST['fcha_acta'].", fpag_prtm=".$_POST['fcha_acta']." WHERE codg_prtm = ".$id_prst_mov_paga;
                                mysql_query($sql_prst_mov);  
                        }
		  }
		modificar_func($ncampos,$datos,$tabla,"codg_prst",$_POST["codg_prst"],$pagina,"");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	////// validar monto maximo para prestamos
	if($max_pres<$datos[2][2]){
		echo "<script>alert('El Monto Solicitado no puede exceder el maximo estipulado para prestamos')</script>";
		$boton = "Verificar";
	}
	if($datos[2][2]<$datos[10][2]){
		echo "<script>alert('El Monto Aprobado No puede exceder el monto solicitado.')</script>";
		$validacion=false;
		$boton = "Verificar";	
	}
	if($max_pres<$datos[10][2]){
		echo "<script>alert('El Monto Aprobado No puede exceder el monto m�ximo permitido')</script>";
		$validacion=false;
		$boton = "Verificar";	
	}
	//////////////////////////////////////
}
if ($_POST["confirmar"]=="Guardar") 
{
    ////// calculamos el monto de cada cuota
    $monto_mensual=0;
	$monto_cuota=0;
	$deuda = $_POST['mont_apro'];
	$interes_calculo = redondear(($_POST['mont_intr']/12),2,"",".");
	if ($_POST['tipo_cuot']=='Q'){ 
	    $cuenta_for = $_POST['plaz_prst'] * 2;
	    $interes = ($interes_calculo/100)/2;
	}
	if ($_POST['tipo_cuot']=='S'){ 
	    $cuenta_for = $_POST['plaz_prst'] * 4;
	    $interes = ($interes_calculo/100)/4;
	}
	if ($_POST['tipo_cuot']=='M'){ 
	    $cuenta_for = $_POST['plaz_prst'];
	    $interes = ($interes_calculo/100);
	}
	$monto_mensual = $_POST['mont_apro']*(($interes*pow(1+$interes,$cuenta_for))/(pow(1+$interes,$cuenta_for)-1));
	$monto_mensual = redondear($monto_mensual,2,"",".");
    $monto_cuota = $monto_mensual;
    $datos[13] = crear_datos ("mont_cuot","Monto de las Cuotas",$monto_cuota,"0","12","decimal");
    ///// Calculamos la cantidad de cuotas pendientes
    if ($_POST["sald_inic_real"] && $_POST['mont_cuot_real']){
        $n_cuotas = 1;
        $saldo_viejo = $_POST["sald_inic_real"];
        while ($saldo_viejo>0){
            $interes_viejo = redondear(($saldo_viejo*$interes),2,'','.');
            $amortiza_viejo = $_POST['mont_cuot_real'] - $interes_viejo;
            $saldo_viejo = $saldo_viejo - $amortiza_viejo;
            $n_cuotas+=1;
        }
        $n_cuotas-=1;
        $datos[16] = crear_datos ("paga_cuot","Cuotas pendientes por pagar",$n_cuotas,"0","12","numericos");
    }
    ////////////////////////////////////////
	$insertado = insertar_func_return($ncampos,$datos,$tabla);
        if ($_POST['codg_prst_paga']!=''){
                $sql_prst_mov = "INSERT INTO prestamos_mov (codg_prst,mnto_prtm,capi_prtm,inte_prtm,orgn_prtm,rela_prtm,freg_prtm,fpag_prtm) values (".$_POST['codg_prst_paga'].",".$_POST['totl_prst_paga'].",".$_POST['capi_prst_paga'].",".$_POST['inte_prst_paga'].",'Registro de Pr�stamo',".$insertado.",'".$_POST['fcha_acta']."','".$_POST['fcha_acta']."')";
                mysql_query($sql_prst_mov);
        }    
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
        $sql_prst_mov = "DELETE FROM prestamos_mov WHERE rela_prtm=".$_POST['codg_prst']." AND orgn_prtm='Registro de Pr�stamo'";
        mysql_query($sql_prst_mov);
	eliminar_func($_POST["codg_prst"],"codg_prst",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="actualizar padre") 
{
    $tipo="individual";
 	$buscando = busqueda_func($codg_prst,codg_prst,"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_prst = $row["codg_prst"];
	    $cedu_soci = $row["cedu_soci"];
	    $mont_prst = $row["mont_prst"];
	    $fcha_prst = $row["fcha_prst"];
	    $nsol_prst = $row["nsol_prst"];
	    $codg_tipo_pres = $row["codg_tipo_pres"];
	    $plaz_prst = $row["plaz_prst"];
	    $obsr_prst = $row["obsr_prst"];
	    $stat_prst = $row["stat_prst"];
	    $numr_acta = $row["numr_acta"];
	    $fcha_acta = $row["fcha_acta"];

	    $codg_egre = $row["codg_egre"];
	    $mont_apro = $row["mont_apro"];
	    $tipo_cuot = $row["tipo_cuot"];
	    $mont_cuot = $row["mont_cuot"];
	    $mont_intr = $row["mont_intr"];
	    $mont_cuot_real = $row["mont_cuot_real"];
	    $sald_inic_real = $row["sald_inic_real"];
	    $paga_cuot = $row["paga_cuot"];
	    
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
	$boton = "Modificar";
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo"> Registro de Pr�stamos </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Fecha de Registro:<?php escribir_campo('codg_prst',$_POST["codg_prst"],$codg_prst,'',12,15,'Codigo del prestamo',$boton,$existe,'','','oculto'); ?></td>
                        <td width="75%"><?php escribir_campo('fcha_prst',$_POST["fcha_prst"],$fcha_prst,'',50,10,'Fecha de Registro del Pr�stamo',$boton,$existe,'fecha','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Ahorrista&nbsp;Relacionado:                          </td>
			            <td>
                        <?php
                            if ($boton == 'Verificar'){
                                $funcion_cambio_ahorrista=' Onchange="submit()"';
                            } 
                            if ($cedu_soci=='' && $_POST['cedu_soci']==''){ $vista='vista_socios_activos'; } else { $vista='vista_socios'; } combo('cedu_soci', $cedu_soci, $vista, $link, 0, 0, 1, '', 'cedu_soci', ".$funcion_cambio_ahorrista.", $boton, "ORDER BY nomb_soci",''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto Disponible:</td>
						<?PHP $val_ppm = buscar_campo('val_val', 'valores', 'WHERE des_val="PPM"' ); ?>
						<?PHP $sum_saldo = buscar_campo('(apor_sald + rete_sald + rein_sald + apor_comp + rete_comp + rein_comp) AS sum_saldo ', 'socios_saldo', 'WHERE cedu_soci='.$cedu_soci); ?>
						<?PHP
							$sql_ppp='select p.mont_apro, p.codg_prst, (select SUM(mnto_prtm) from prestamos_mov where codg_prst=p.codg_prst) as mnto_paga from prestamos p WHERE p.stat_prst="A" AND p.cedu_soci='.$cedu_soci;
							$busq_ppp=mysql_query($sql_ppp);
							if($reg_ppp=@mysql_fetch_array($busq_ppp)){
								do{
									$ppp_apro+=$reg_ppp['mont_apro'];
									$ppp_prtm+=$reg_ppp['mnto_paga'];
								}while($reg_ppp=mysql_fetch_array($busq_ppp));
							}
							$ppp=$ppp_apro-$ppp_prtm;
							
							$sql_ppf='select f.mont_fian, p.codg_prst, (select SUM(mnto_prtm) from prestamos_mov where codg_prst=f.codg_prst) as mnto_paga from prestamos_fia f, prestamos p WHERE f.codg_prst=p.codg_prst AND p.stat_prst="A" AND f.cedu_soci='.$cedu_soci;
							$busq_ppf=mysql_query($sql_ppf);
							if($reg_ppf=@mysql_fetch_array($busq_ppf)){
								do{
									$ppf_afian+=$reg_ppf['mont_fian'];
									$ppf_prtm+=$reg_ppf['mnto_paga'];
								}while($reg_ppf=mysql_fetch_array($busq_ppf));
							}
							$ppf=$ppf_apro-$ppf_prtm;							
						?>
						<?PHP if($sum_saldo['sum_saldo']!=""){ $max_pres=(($sum_saldo['sum_saldo']*$val_ppm['val_val'])-($ppp+$ppf)); } ?>
                        <input name="sum_saldo" type="hidden" value="<?PHP echo $sum_saldo['sum_saldo']; ?>" />
						<td width="75%">
							
							<?PHP  if($max_pres>0){ echo "&nbsp;Maximo&nbspa&nbsp;prestar&nbsp;sin&nbsp;fiador: ".redondear($max_pres,2,"","."); } ?>
						</td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Agregar&nbsp;Fiador:                          </td>
			            <td>
                        <?php
                            if ($boton == 'Verificar'){
                                $funcion_cambio_ahorrista='';
                            } 
                            if ($cedu_fiad=='' && $_POST['cedu_fiad']==''){ $vista='vista_socios_activos'; } else { $vista='vista_socios'; } combo('cedu_fiad', $cedu_fiad, $vista, $link, 0, 0, 1, '', 'cedu_soci', ".$funcion_cambio_ahorrista.", $boton, "ORDER BY nomb_soci",''); ?><input name="agregar" type="submit" value="Agregar"/><input name="agregar" type="submit" value="Limpiar"/></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fiadores:</td>
			            <td><?PHP if(($cedu_fiad or $_POST['cedu_fiad']) and $_POST['agregar']!="Limpiar"){ ?>
							<table width="100%" border="1" cellspacing="0" cellpadding="0">
							  <tr class="etiquetas">
								<td width="5%">Nro</td>
								<td width="10%">Cedula</td>
								<td width="55%">Nombre</td>
								<td width="15%">Maxima&nbsp;Fianza</td>
								<td width="15%">Monto</td>
							  </tr>
							     <?PHP 
							  		if(!$_POST['i_fiad']){ $i_fiad=1; }else{ $i_fiad=$_POST['i_fiad']; }
									for($i=1;$i<=$i_fiad;$i++){ $cedu_fiador_aux[$i]=$_POST['cedu_fiad'.$i]; $mont_fiador_aux[$i]=$_POST['mont_fiad'.$i];  }
									$j=1;
									for($i=1;$i<=$i_fiad;$i++){ if($cedu_fiador_aux[$i]!=$cedu_fiad and $cedu_fiador_aux[$i]!=""){ $cedu_fiador[$j]=$cedu_fiador_aux[$i]; $j++; } }
									if(!$cedu_fiador[1]){ $cedu_fiador[1]=$cedu_fiad; $j=1; }else{ $cedu_fiador[$j]=$cedu_fiad; }
									for($i=1;$i<=$j;$i++){
										$reg_fiad[$i]=buscar_campo('*', 'socios', ' WHERE cedu_soci='.$cedu_fiador[$i]); 
									  ?>
									  <tr class="etiquetas">
										<td><?PHP echo $i; ?></td>
										<td><?PHP echo $reg_fiad[$i]['cedu_soci']; ?><input name="cedu_fiad<?PHP echo $i; ?>" type="hidden" value="<?PHP echo $reg_fiad[$i]['cedu_soci']; ?>" /></td>
										<td><?PHP echo $reg_fiad[$i]['apel_soci']."<br>".$reg_fiad[$i]['nomb_soci']; ?></td>
										<td align="right">&nbsp;
											<?PHP $f_ma = buscar_campo('(apor_sald + rete_sald + rein_sald + apor_comp + rete_comp + rein_comp) AS f_ma ', 'socios_saldo', 'WHERE cedu_soci='.$reg_fiad[$i]['cedu_soci']); ?>
											<?PHP
												$sql_f_ppp='select p.mont_apro, p.codg_prst, (select SUM(mnto_prtm) from prestamos_mov where codg_prst=p.codg_prst) as mnto_paga from prestamos p WHERE p.stat_prst="A" AND p.cedu_soci='.$reg_fiad[$i]['cedu_soci'];
												$busq_f_ppp=mysql_query($sql_f_ppp);
												if($reg_f_ppp=@mysql_fetch_array($busq_f_ppp)){
													do{
														$f_ppp_apro[$i]+=$reg_f_ppp['mont_apro'];
														$f_ppp_prtm[$i]+=$reg_f_ppp['mnto_paga'];
													}while($reg_f_ppp=mysql_fetch_array($busq_f_ppp));
												}
												$f_ppp[$i]=$f_ppp_apro[$i]-$f_ppp_prtm[$i];
												
												$sql_f_ppf='select f.mont_fian, p.codg_prst, (select SUM(mnto_prtm) from prestamos_mov where codg_prst=f.codg_prst) as mnto_paga from prestamos_fia f, prestamos p WHERE f.codg_prst=p.codg_prst AND p.stat_prst="A" AND f.cedu_soci='.$reg_fiad[$i]['cedu_soci'];
												$busq_f_ppf=mysql_query($sql_f_ppf);
												if($reg_f_ppf=@mysql_fetch_array($busq_f_ppf)){
													do{
														$f_ppf_afian[$i]+=$reg_f_ppf['mont_fian'];
														$f_ppf_prtm[$i]+=$reg_f_ppf['mnto_paga'];
													}while($reg_f_ppf=mysql_fetch_array($busq_f_ppf));
												}
												$f_ppf[$i]=$f_ppf_apro[$i]-$f_ppf_prtm[$i];							
											?>
											<?PHP if($f_ma['f_ma']!=""){ $f_md[$i]=(($f_ma['f_ma']*$val_ppm['val_val'])-($f_ppp[$i]+$f_ppf[$i])); } ?>
											<?PHP if($f_md[$i]<0){ $f_md[$i]=0; } echo redondear($f_md[$i],2,"",".");  ?>
										</td><?PHP if(!$mont_fiador_aux[$i]){ $mont_fiador_aux[$i]=0; } if(!$f_md[$i]){ $f_md[$i]=0; } ?>
										<td><input name="mont_fiad<?PHP echo $i; ?>" type="<?PHP if($f_md[$i]>0){ echo "text"; }else{ echo "hidden"; } if($mont_fiador_aux[$i]>$f_md[$i]){ $error=true; }else{ $max_pres+=$mont_fiador_aux[$i]; } ?>" value="<?PHP echo $mont_fiador_aux[$i]; ?>" maxlength="11" size="5" /></td>
									  </tr>								
							  	  <?PHP 
							  		} 
								  ?>
							  		<input name="i_fiad" type="hidden" value="<?PHP echo $i; ?>" />
									<tr class="etiquetas"><td colspan="5">Para Agregar monto(s) de respaldo de fiador(es), coloque el(los) monto(s) y haga click nuevamente en el boton Agregar</td></tr>
							</table>
							<?PHP } ?>
						</td>
                      </tr>
					  <tr>
                        <td width="25%" class="etiquetas">Monto con fiadores:</td>
                        <td width="75%">
							<?PHP if($max_pres>0){ echo "&nbsp;Maximo&nbspa&nbsp;prestar: ".redondear($max_pres,2,"","."); } ?>
							<?PHP if($error==true){ echo "<script>alert('Error: se excedio del monto de respaldo en alguno de los fiadores');</script>"; $boton="Verificar"; } ?>
						</td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Agregar Fianza:</td>
                        <td width="75%">
							<?php escribir_campo('desc_fian','','','',50,30,'Descripcion Fianza',$boton,$existe,'','','');?>
							<?php escribir_campo('mont_fian','','','',11,10,'Monto de Fianza',$boton,$existe,'','','');?><input name="agregar" type="submit" value="Agregar"/>
						</td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fianzas:</td>
                        <td width="75%">
						<?PHP  ?>
							<table width="100%" border="1" cellspacing="0" cellpadding="0">
							  <tr class="etiquetas">
								<td width="10%">Nro</td>
								<td width="60%">Descripcion</td>
								<td width="30%">Monto</td>
							  </tr>
							  <?PHP
							  		if(!$_POST['i_fian']){ $i_fian=1; }else{ $i_fian=$_POST['i_fian']; }
									for($n=1;$n<=$i_fian;$n++){ $f_desc_fian[$n]=$_POST['f_desc_fian'.$n]; $f_mont_fian[$n]=$_POST['f_mont_fian'.$n]; }
									if(!$f_mont_fian[1]){ $f_mont_fian[1]=$_POST['mont_fian']; $f_desc_fian[1]=$_POST['desc_fian']; $i_fian=1; }else{ $f_mont_fian[$i_fian]=$_POST['mont_fian'];  $f_desc_fian[$i_fian]=$_POST['desc_fian']; }							
							  		$o=1;
							  ?>
							  <?PHP for($n=1;$n<=$i_fian;$n++){ ?>
								  <?PHP if($f_desc_fian[$n] and $f_mont_fian[$n]){ ?>
								  <tr class="etiquetas">
									<td><?PHP echo $o; $max_pres+=$f_mont_fian[$n]; ?></td>
									<td><?PHP echo $f_desc_fian[$n]; ?><input name="f_desc_fian<?PHP echo $n; ?>" type="hidden" value="<?PHP echo $f_desc_fian[$n]; ?>" /></td>
									<td><?PHP echo $f_mont_fian[$n]; ?><input name="f_mont_fian<?PHP echo $n; ?>" type="hidden" value="<?PHP echo $f_mont_fian[$n]; ?>" /></td>
								  </tr>
								  <?PHP $o+=1; } ?>
							  <?PHP } ?>				  		
							  	<input name="i_fian" type="hidden" value="<?PHP echo $n; ?>" />
							 </table>
						<?PHP  ?>						
						</td>
                      </tr>
					  <tr>
                        <td width="25%" class="etiquetas">Monto con fianzas:</td>
                        <td width="75%">
							<?PHP echo "&nbsp;Maximo&nbspa&nbsp;prestar: ".redondear($max_pres,2,"","."); ?>
						</td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto Solicitado:</td>
                        <td width="75%">
							<?PHP if($mont_prst>$max_pres){ echo "<script>alert('Error: el monto solicitado es mayor al Maximo disponible para prestar');</script>"; $boton="Verificar"; $mont_prst=0; $_POST["mont_prst"]=0; } ?>
							<?php escribir_campo('mont_prst',$_POST["mont_prst"],$mont_prst,'',12,10,'Monto Solicitado',$boton,$existe,'','onchange="submit();"',''); ?><input name="max_pres" type="hidden" value="<?PHP echo $max_pres; ?>" />
						</td>
                      </tr>	  
                      <tr>
                        <td width="25%" class="etiquetas">N� de Solicitud:</td>
                        <td width="75%"><?php escribir_campo('nsol_prst',$_POST["nsol_prst"],$nsol_prst,'',11,10,'N� de Solicitud',$boton,$existe,'','','');?></td>
                      </tr>
                     <tr>
                     <?php 
                      $funcion_combo_tipo = 'Onchange="tipo_prestamo_plazo(this.value, ';
					   	 $funcion_combo_tipo .= "'plaz_prst')";
					     	 $funcion_combo_tipo .= '";';
					     ?>
                        <td width="25%" class="etiquetas">Tipo de Prestamo:</td>
                        <td width="75%"><?php combo('codg_tipo_pres', $codg_tipo_pres, 'tipo_prestamos', $link, 0, 1, '', '', 'codg_tipo_pres', $funcion_combo_tipo, $boton, "ORDER BY  nomb_tipo_pres",''); ?>
 								<?php escribir_campo('plaz_prst',$_POST["plaz_prst"],$plaz_prst,'',2,3,'Plazo de Prestamo',$boton,$existe,'','readonly ','')?> Meses
								<input type="hidden" name="tipo_prst_ant" id="tipo_prst_ant" value="<?php if ($existe) { echo $codg_tipo_pres;} else { echo $_POST["tipo_prst_ant"]; }?>">
								<input type="hidden" name="plaz_prst_ant" id="plaz_prst_ant" value="<?php if ($existe) { echo $plaz_prst;} else { echo $_POST["plaz_prst_ant"]; }?>">
                                             
                        </td>
                      </tr>
                     
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td width="75%"><?php echo escribir_area('obsr_prst',$_POST["obsr_prst"],$obsr_prst,'',30,3,'Observaciones adicionales sobre el MONTEPIO',$boton,$existe,'')?></td>
                      </tr>
                      <tr>
                      	<td class="etiquetas">Estatus de Prestamo:</td>
                        <td>
                            <?php if ($boton != "Modificar" && $boton != "Guardar") { echo '<select name="stat_prst" title="Estatus de Pr�stamo">
                          <option value="">Seleccione...</option>
                          <option value="A" '; if ($stat_prst == "A" || $_POST['stat_prst'] =="A") { echo 'selected'; } echo '>Aprobado</option>
                          <option value="N" '; if ($stat_prst == "N" || $_POST['stat_prst'] =="N") { echo 'selected'; } echo '>Negado</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="stat_prst" id="stat_prst" value="'.$stat_prst.'" >'; 
						    if ($stat_prst == "A") { echo 'Aprobado'; } 
							if ($stat_prst == "N") { echo 'Negado'; }
						}?>
					  </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N� de Acta:</td>
                        <td width="75%"><?php escribir_campo('numr_acta',$_POST["numr_acta"],$numr_acta,'',11,10,'N� de Solicitud',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha&nbsp;de&nbsp;Acta:</td>
                        <td width="75%"><?php escribir_campo('fcha_acta',$_POST["fcha_acta"],$fcha_acta,'',50,10,'Fecha de Acta Defuncion',$boton,$existe,'fecha','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto Otorgado:</td>
                        <td width="75%"><?php escribir_campo('mont_apro',$_POST["mont_apro"],$mont_apro,'',12,10,'Monto Otorgado',$boton,$existe,'','','')?>
                        <input type="hidden" name="mont_apro_ant" id="mont_apro_ant" value="<?php if ($existe) { echo $mont_apro;} else { echo $_POST["mont_apro_ant"]; }?>"></td>
                      </tr>
                      <tr>
                      	<td class="etiquetas">Tipo de Cuotas:</td>
                        <td>
                          <?php
                          if ($boton=="Verificar" && $_POST['cedu_soci']!=''){
                                $sql_cuota = "Select peri_suel FROM socios_dat_lab WHERE codg_dlab = (SELECT MAX(codg_dlab) FROM socios_dat_lab WHERE cedu_soci=".$_POST['cedu_soci']." );";
                                $reg_cuota = mysql_fetch_array(mysql_query($sql_cuota));
                                $tipo_cuot = $reg_cuota['peri_suel'];
                                $_POST['tipo_cuot']=$reg_cuota['peri_suel'];
                          }
                          if ($tipo_cuot == "M" || $_POST['tipo_cuot'] =="M") { $tipo_c = 'Mensuales'; }
                          if ($tipo_cuot == "Q" || $_POST['tipo_cuot'] =="Q") { $tipo_c = 'Quincenales'; }
                          if ($tipo_cuot == "S" || $_POST['tipo_cuot'] =="S") { $tipo_c = 'Semanales'; }
                          echo $tipo_c;  
                          escribir_campo('tipo_cuot',$_POST["tipo_cuot"],$tipo_cuot,'',12,10,'Monto Otorgado',$boton,$existe,'','','oculto');                              
                          ?>
                          <input type="hidden" name="tipo_cuot_ant" id="tipo_cuot_ant" value="<?php if ($existe) { echo $tipo_cuot;} else { echo $_POST["tipo_cuot_ant"]; }?>"></td>
			</td>
		      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Tasa de Inter�s:</td>
                        <td width="75%"><?php escribir_campo('mont_intr',$_POST["mont_intr"],$mont_intr,'',12,10,'Tasa de Inter�s anual',$boton,$existe,'','','')?>
                        <input type="hidden" name="mont_intr_ant" id="mont_intr_ant" value="<?php if ($existe) { echo $mont_intr;} else { echo $_POST["mont_intr_ant"]; }?>"></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto de Cuotas:</td>
                        <td width="75%"><?php escribir_campo('mont_cuot',$_POST["mont_cuot"],$mont_cuot,'',12,10,'Monto de las cuotas generado automaticamente por el sistema',$boton,$existe,'','readonly','')?></td>
                      </tr>
                      <tr>
                        <td colspan=2><hr></td>
                      </tr>
                      <tr>
                        <td colspan=2 class="etiquetas">Si se est� pagando un prestamo anterior por favor seleccionelo de la lista</td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Pr�stamo que paga:</td>
                        <td width="75%">
        		<input type="hidden" name="codg_prst_paga_aux" id="codg_prst_paga_aux" value="<?php if ($existe) { echo $codg_prst_paga;} else { echo $_POST['codg_prst_paga_aux']; }?>">
                        <?php 
                        if ($boton=="Actualizar"){ $combo_add=" AND codg_prst<>".$_POST['codg_prst']." AND fcha_acta<".$_POST['fcha_acta'];}
                        combo('codg_prst_paga', $codg_prst_paga, 'prestamos', $link, 0, 3, 12, 14, '', "Onchange=submit()", $boton, " WHERE cedu_soci=".$_POST["cedu_soci"]." ".$combo_add." ORDER BY codg_prst DESC",'codg_prst'); ?>
                        <?php 
                        if ($_POST["confirmar"]=='' && $_POST['codg_prst']){ $boton = "Actualizar"; }
                        if ($_POST["Buscar"]||$_POST["BuscarInd"]) { $boton = "Modificar"; }
                        if ($codg_prst_paga) { 
					$sql_montos_pagados = "SELECT * from prestamos_mov WHERE rela_prtm = ".$codg_prst." AND orgn_prtm='Registro de Pr�stamo'";
					$bus_montos_pagados = mysql_query($sql_montos_pagados);
					$paga_otro_prestamo = '';
					if ($res_montos_pagados=mysql_fetch_array($bus_montos_pagados)){
						$paga_otro_prestamo = "SI";
						$id_prst_mov_paga = $res_montos_pagados['codg_prtm'];
						$saldo_afecha = $res_montos_pagados['capi_prtm'];
						$deuda_interes = $res_montos_pagados['inte_prtm'];
						$deuda_total = $res_montos_pagados['mnto_prtm'];
					}
					if ($saldo_afecha > 0 || $deuda_interes > 0 || $deuda_total > 0){
?>
                                <br>Saldo del Pr�stamo: <b><? echo redondear($saldo_afecha,2,'.',','); ?></b><input type="hidden" name="id_prst_mov_paga" value="<?php echo $id_prst_mov_paga; ?>"><input type="<?PHP if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; }?>" name="capi_prst_paga" value="<?php echo $saldo_afecha; ?>">
                                <br>Intereses del Pr�stamo: <b><? echo redondear($deuda_interes,2,'.',','); ?></b><input type="<?PHP if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; }?>" name="inte_prst_paga" value="<?php echo $deuda_interes; ?>">
                                <br>Total de la Deuda: <b><? echo redondear(($deuda_total),2,'.',','); ?></b><input type="<?PHP if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; }?>" name="totl_prst_paga" value="<?php echo $deuda_total; ?>">
                                <?php
                        }}
                        if ($codg_prst_paga && $paga_otro_prestamo == '') {                                
                                /// buscamos el ultimo pago de n�mina
                                $sql_prst_pagar = "SELECT * FROM prestamos_mov WHERE codg_prst=".$codg_prst_paga." AND orgn_prtm='Registro de N�mina' AND fpag_prtm!='0000-00-00' ORDER BY fpag_prtm DESC LIMIT 1" ;
                                $ultimo_pago = mysql_fetch_array(mysql_query($sql_prst_pagar));
                                /// Buscamos el monto pagado del prestamo
                                $sql_prst_pagar = "SELECT SUM(capi_prtm) as pagado FROM prestamos_mov WHERE codg_prst=".$codg_prst_paga;
                                if ($codg_prst || $_POST['codg_prst']){
                                        $sql_prst_pagar.=" AND orgn_prtm!='Registro de Pr�stamo'";
                                }
                                $monto_pagado = mysql_fetch_array(mysql_query($sql_prst_pagar));
                                ///  Buscamos los datos del prestamos
                                $sql_prst_pagar = "SELECT * FROM prestamos WHERE codg_prst=".$codg_prst_paga;
                                $datos_prestamo = mysql_fetch_array(mysql_query($sql_prst_pagar));
                                //// calcular los montos de capital e intereses por pagar
                                //// capital
                                if ($datos_prestamo['sald_inic_real']>0){
                                $saldo_inicial = $datos_prestamo['sald_inic_real'];
                                }
                                else { $saldo_inicial = $datos_prestamo['mont_apro']; }
                                $saldo_afecha = $saldo_inicial - $monto_pagado['pagado'];
                                //// meses sin pagar
                                $ultima_fecha = strtotime($ultimo_pago['fpag_prtm']);
                                if ($fcha_acta){
                                        $fecha_actual = strtotime($fcha_acta);
                                }
                                else {
                                        $fecha_actual = strtotime($_POST['fcha_acta']);                                
                                }
                                if (!$ultima_fecha){
                                        $ultima_fecha = strtotime($datos_prestamo['fcha_acta']);
                                }
                                $calculo = $ultima_fecha;
                                $deuda_meses = 0;
                                while ($calculo < $fecha_actual){
                                   $calculo = strtotime("+1 months", $calculo);
                                   $deuda_meses++;
                                }
                                //// Intereses
                                $monto_interes = redondear(($datos_prestamo['mont_intr']/12),2,'','.');
                                $deuda_interes = redondear(($saldo_afecha * ($monto_interes/100)),2,'','.');
                                $deuda_interes = redondear(($deuda_interes * $deuda_meses),2,'','.');
                                //// total
                                $deuda_total = redondear(($saldo_afecha+$deuda_interes),2,'','.');
                                ?>
                                <br>Saldo del Pr�stamo: <b><? echo redondear($saldo_afecha,2,'.',','); ?></b><input type="text" name="capi_prst_paga" value="<?php echo $saldo_afecha; ?>">
                                <br>Intereses del Pr�stamo: <b><? echo redondear($deuda_interes,2,'.',','); ?></b><input type="text" name="inte_prst_paga" value="<?php echo $deuda_interes; ?>">
                                <br>Total de la Deuda: <b><? echo redondear(($deuda_total),2,'.',','); ?></b><input type="text" name="totl_prst_paga" value="<?php echo $deuda_total; ?>">
                                <?php
                        }
                        ?>
                        </td>
                      </tr>
                      <tr>
                        <td colspan=2><hr></td>
                      </tr>
                      <tr>
                        <td colspan=2 class="etiquetas">Datos adicionales si el pr�stamo fue otorgado antes de la instalaci�n del sistema</td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Saldo al 31-12-12:</td>
                        <td width="75%"><?php escribir_campo('sald_inic_real',$_POST["sald_inic_real"],$sald_inic_real,'',12,10,'Saldo de la deuda al 31-12-12',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto de Cuotas Real:</td>
                        <td width="75%"><?php escribir_campo('mont_cuot_real',$_POST["mont_cuot_real"],$mont_cuot_real,'',12,10,'Monto de las cuotas real calculado fuera de sistema',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Cuotas por pagar:</td>
                        <td width="75%"><?php escribir_campo('paga_cuot',$_POST["paga_cuot"],$paga_cuot,'',12,10,'Cantidad de cuotas pendientes por pagar',$boton,$existe,'','','')?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
                  <tr>
                    <td align="center">
                        <?php if($boton=='Modificar'){ abrir_ventana('../reportes/tabla_amortizacion.php','v_imprimir','Tabla de Amortizaci�n',"codg_prst=".$codg_prst."&seccion=".$_GET['seccion']); } ?>
                        <?php if($boton=='Modificar'){ abrir_ventana('../reportes/acta_prestamo.php','v_imprimir','Acta de Prestamo',"codg_prst=".$codg_prst."&seccion=".$_GET['seccion']); } ?>
                    </td>
                  </tr>
				  <tr>
					<td>
					<?php 
						$ncriterios =2;
						$criterios[0] = "Fecha"; 
						$campos[0] ="fcha_prst";
						$criterios[1] = "C�dula"; 
						$campos[1] ="cedu_soci";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     $funcion_combo = '"criterio1.checked=true; valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
                                             echo '<center>Buscar C�dula: '; 
                                             combo('cedu_soci2', $cedu_soci, 'vista_socios', $link, 0, 0, 1, '', 'cedu_soci', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nomb_soci",'');
                                             echo '</center>'; 
				   	   }  ?>
				   	   </td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
