<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="../comunes/jquery.min.js"></script>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></link>
<script type="text/javascript" src="../comunes/calendar.js?"></script>
<script type="text/javascript">

function valida_envia(){	
	if(document.form1.cedu_soci.selectedIndex==0){
		alert("Debe seleccionar un socio");
		document.form1.cedu_soci.focus();
		return 0;
	}
	if (document.form1.mont_prst.value.length==0){
		alert("Debe colocar un monto a solicitar");
		document.form1.mont_prst.focus();
		return 0;
	}
	if(document.form1.codg_tipo_pres.selectedIndex==0){
		alert("Debe seleccionar un tipo de prestamo");
		document.form1.codg_tipo_pres.focus();
		return 0;
	}
	if(document.form1.stat_prst.selectedIndex==0){
		alert("Debe indicar el estatus del prestamo");
		document.form1.stat_prst.focus();
		return 0;
	}
	if (document.form1.mont_apro.value.length==0){
		alert("Debe indicar un monto otorgado para el prestamo");
		document.form1.mont_apro.focus();
		return 0;
	}
	if (document.form1.mont_intr.value.length==0){
		alert("Debe indicar una tasa de interes para el prestamo");
		document.form1.mont_intr.focus();
		return 0;
	}
	if (document.form1.mont_cuot.value.length==0){
		alert("Debe calcular el monto de las cuotas antes de Guardar");
		document.form1.mont_cuot.focus();
		return 0;
	}
	document.form1.submit();
}

</script>
<SCRIPT>
function tipo_prestamo_plazo(valor, campo)
{
   var nuevo_valor;
   <?php 
      $sql_tipo_prst = "SELECT * FROM tipo_prestamos";
      $bus_tipo_prst = mysql_query($sql_tipo_prst);
      while ($res_tipo_prst=mysql_fetch_array($bus_tipo_prst)){
         $val_max=number_format($res_tipo_prst['valormax_tipo_pres'], 0, '', '');
         echo "if (valor=='".$res_tipo_prst['codg_tipo_pres']."') { 
           nuevo_valor = '".$val_max."'; 
           document.form1[campo].readOnly =  false; 
           document.form1[campo].focus(); 
         }
         ";
      }
   ?>
   if (valor=='') {
        document.form1[campo].readOnly =  false;
        nuevo_valor = '';
   }
   document.form1[campo].value = nuevo_valor;
}
</SCRIPT>
<script>
function actualizar_dato(tabla,campo,valor,campo_id,valor_id,responder)
{
    //para verificar si es una fecha voltearla al momento de guardarla
      var nueva;
      nueva = valor.split("-");
      if (valor.length == 10 && nueva[0].length == 2 && nueva[1].length == 2 && nueva[2].length == 4){
        valor = nueva[2]+'-'+nueva[1]+'-'+nueva[0];
      }
      var parametros = {
        "var_tabla": tabla,
        "var_campo" : campo,
        "var_valor" : valor,
        "var_id" : campo_id,
        "var_id_val" : valor_id
      };
      var url="../comunes/funcion_actualizarcampo.php"; 
      $.ajax
      ({
        type: "POST",
          url: url,
          data: parametros,
          success: function(data)
          {
			$('#'+responder).html(data);            
          }
      });
      return false; 
}
</script>
<?php 
include ('../comunes/formularios_funciones.php');
    
    $sql_val="select * from valores WHERE des_val = 'EDITPRES'";
    $res_val = mysql_query($sql_val);
    while ($row_val = mysql_fetch_array($res_val))
    {
        // el nombre de la variable es $SUSTRAC_ISRL
        $$row_val['des_val'] = $row_val['val_val'];
    }


$existe = '';
$pagina = 'prestamos_nueva.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$busqueda_varios=false;
if($_POST['boton']){ $boton=$_POST['boton']; }else{ $boton=$_GET['boton']; $busqueda=$_GET['busqueda']; }
$accion=$_POST['accion'];
if($_POST['codg_prst']){ $codg_prst=$_POST['codg_prst']; }else{ $codg_prst=$_GET['codg_prst']; }
$fcha_desd=$_POST['fcha_desd'];
$fcha_hast=$_POST['fcha_hast']; 
$cedu_soci=$_POST['cedu_soci']; 
$mont_prst=$_POST['mont_prst']; 
$fcha_prst=$_POST['fcha_prst']; 
$nsol_prst=$_POST['nsol_prst']; 
$tipo_prst=$_POST['codg_tipo_pres']; 
$plaz_prst=$_POST['plaz_prst'];
$obsr_prst=$_POST['obsr_prst']; 
$stat_prst=$_POST['stat_prst']; 
$numr_acta=$_POST['numr_acta']; 
$fcha_acta=$_POST['fcha_acta']; 
$mont_apro=$_POST['mont_apro']; 
$tipo_cuot=$_POST['tipo_cuot']; 
$mont_cuot=$_POST['mont_cuot']; 
$mont_intr=$_POST['mont_intr']; 
$paga_cuot=$_POST['paga_cuot']; 
$codg_tipo_pres=$_POST['codg_tipo_pres']; 
$n_ppp=$_POST['n_ppp']; 
$error=false;
if($boton=="Buscar" OR $boton=="Eliminar"){ $accion=""; }
if($accion!="Guardar"){
	$sql_pres="delete from prestamos_fia where codg_prst is NULL";
	mysql_query($sql_pres);
}else{
	mysql_query('AUTOCOMMIT=0');
	mysql_query('BEGIN');
	$sql_pres="INSERT INTO prestamos(cedu_soci, mont_prst, fcha_prst, nsol_prst, tipo_prst, plaz_prst, obsr_prst, stat_prst, numr_acta, fcha_acta, mont_apro, tipo_cuot, mont_cuot, mont_intr, codg_tipo_pres)
	VALUES ('$cedu_soci', '$mont_prst', '$fcha_prst', '$nsol_prst', '$tipo_prst', '$plaz_prst', '$obsr_prst', '$stat_prst', '$numr_acta', '$fcha_acta', '$mont_apro', '$tipo_cuot', '$mont_cuot', '$mont_intr', '$codg_tipo_pres')";
	if(mysql_query($sql_pres)){
		$codg_prst=mysql_insert_id();
		$sql_fia="update prestamos_fia set codg_prst='$codg_prst' where codg_prst is NULL";
		if(!mysql_query($sql_fia)){
			$error=true;
		}
		for($i=1;$i<=$n_ppp;$i++){
			if($_POST['pagar_ppp'.$i]==1){
				$sql_ppp="insert into prestamos_mov(codg_prst, mnto_prtm, capi_prtm, inte_prtm, orgn_prtm, rela_prtm, freg_prtm, fpag_prtm) values(".$_POST['codg_prst_ppp'.$i].", ".$_POST['total_ppp'.$i].", ".$_POST['capital_pagar'.$i].", ".$_POST['inter_ppp'.$i].", 'Pagado por prestamo', $codg_prst, '$fcha_acta', '$fcha_acta')";
				if(!mysql_query($sql_ppp)){
					$error=true;
				}
			}
		}
		if($error==true){ 
			mysql_query('ROLLBACK'); 		
		}else{ 
			mysql_query('COMMIT'); 
			echo '<script>alert("Los datos del prestamo han sido Guardados con exito");</script>';
			echo '<script>window.location="prestamos_nueva.php"</script>';
		}
	}else{
		mysql_query('ROLLBACK');
	}
}
if($boton=="Buscar"){
	if($busqueda=="individual"){
		$sql="SELECT p.*, s.*, t.* FROM tipo_prestamos t, prestamos p LEFT JOIN socios s ON s.cedu_soci=p.cedu_soci WHERE codg_prst='$codg_prst' AND p.codg_tipo_pres = t.codg_tipo_pres";
	}else{
		$sql="SELECT p.*, s.*, t.* FROM tipo_prestamos t, prestamos p LEFT JOIN socios s ON s.cedu_soci=p.cedu_soci WHERE fcha_prst>='$fcha_desd' AND fcha_prst<='$fcha_hast' AND p.codg_tipo_pres = t.codg_tipo_pres order by fcha_prst desc";
		if($_POST['cedu_busq']){
			$sql="SELECT p.*, s.*, t.* FROM tipo_prestamos t, prestamos p LEFT JOIN socios s ON s.cedu_soci=p.cedu_soci WHERE p.cedu_soci like '%".$_POST['cedu_busq']."%' AND p.codg_tipo_pres = t.codg_tipo_pres order by fcha_prst desc";
		}	
	}
	$busq=mysql_query($sql);
	$reg=mysql_fetch_array($busq);
	mysql_num_rows($busq);
	if(mysql_num_rows($busq)>1){
		$busqueda_varios=true;
	}
	if(mysql_num_rows($busq)==0){
			echo '<script>alert("No existen resultados para la busqueda");</script>';
			echo '<script>window.location="prestamos_nueva.php"</script>';
	}
	if(mysql_num_rows($busq)==1){
		$codg_prst=$reg['codg_prst']; 
		$cedu_soci=$reg['cedu_soci']; 
		$mont_prst=$reg['mont_prst']; 
		$fcha_prst=$reg['fcha_prst']; 
		$nsol_prst=$reg['nsol_prst']; 
		$tipo_prst=$reg['codg_tipo_pres']; 
		$plaz_prst=$reg['plaz_prst'];
		$obsr_prst=$reg['obsr_prst']; 
		$stat_prst=$reg['stat_prst']; 
		$numr_acta=$reg['numr_acta']; 
		$fcha_acta=$reg['fcha_acta']; 
		$mont_apro=$reg['mont_apro']; 
		$tipo_cuot=$reg['tipo_cuot']; 
		$mont_cuot=$reg['mont_cuot']; 
		$mont_intr=$reg['mont_intr']; 
		$paga_cuot=$reg['paga_cuot']; 
		$codg_tipo_pres=$reg['codg_tipo_pres']; 	
	}
}

if($boton=="Eliminar"){
	$sql_pres='delete from prestamos where codg_prst="'.$codg_prst.'"';
	mysql_query($sql_pres);
	$sql_pres='delete from prestamos_mov WHERE orgn_prtm="Pagado por prestamo" AND rela_prtm="'.$codg_prst.'"';
	mysql_query($sql_pres);
	echo '<script>alert("El registro fue eliminado con exito");</script>';
	echo '<script>window.location="prestamos_nueva.php"</script>';
}
?>

<form id="form1" name="form1" method="post" action="">
<input name="codg_prst" id="codg_prst" type="hidden" value="<?PHP echo $codg_prst; ?>" />
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center"><?PHP if($busqueda_varios==false){ ?>
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo"> Registro de Pr�stamos </td>
                  </tr>
                  <tr>
                    <td width="550"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td class="etiquetas" width="26%">Buscar (Rango):</td>
                        <td>Desde:<?php escribir_campo('fcha_desd',$_POST["fcha_desd"],$fcha_desd,'',50,10,'Fecha desde donde se desea buscar','Verificar',$existe,'hoy','style="width: 80px"','')?>&nbsp;hasta:<?php escribir_campo('fcha_hast',$_POST["fcha_hast"],$fcha_hast,'',50,10,'Fecha hasta donde se desea buscar','Verificar',$existe,'hoy','style="width: 80px"','')?>                          </td>
                        <td width="20%" rowspan="2" valign="middle">
                          <input name="boton" id="boton" type="submit" value="Buscar" /></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Buscar (Cedula):</td>
                        <td><?php escribir_campo('cedu_busq','',$cedu_busq,'',50,10,'Buscar por cedula','Verificar',$cedu_busq,'','','')?></td>
                        </tr>
                      <tr>
                        <td colspan=3><hr></td>
                      </tr>
                      <tr>
                        <td width="27%" class="etiquetas">Fecha de Registro:<input name="codg_prst" id="codg_prst" type="hidden" value="<?PHP echo $codg_prst; ?>" /></td>
                        <td colspan="2"><?php escribir_campo('fcha_prst',$_POST["fcha_prst"],$fcha_prst,'',50,10,'Fecha de Registro del Pr�stamo','Verificar',$fcha_prst,'fecha_','required','')?>
                        <?php 
                        	if ($boton == 'Buscar' && $EDITPRES==1) { 
                        		$onclick_fecha = "actualizar_dato('prestamos','fcha_prst',$('#fcha_prst').val(),'codg_prst',$('#codg_prst').val(),'fcha_prst_resul');";
                        		echo '<img src="../imagenes/default_on.png" style="cursor: pointer;" onclick="'.$onclick_fecha.'"> <span id="fcha_prst_resul"></span>'; 
                        	} 
                        ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="27%" class="etiquetas">Ahorrista&nbsp;Relacionado:
<script>
function monto_disponible(cedu_soci){
        var parametros = {
                "cedu_soci" : cedu_soci
        };
        $.ajax({
                data:  parametros,
                url:   'capa_prestamos_md.php',
                type:  'post',
                beforeSend: function () {
                        $("#md").html("Procesando, espere por favor...");
                },
                success:  function (response) {	
                        $("#md").html(response);
						actualizar_capa("tipo_cuotas");
						actualizar_capa("prestamos_por_pagar");
						actualizar_capa("prestamo_maximo");
                }
        });
}
</script>						                         </td>
			            <td colspan="2"><?PHP if ($cedu_soci=='' && $_POST['cedu_soci']==''){ $vista='vista_socios_activos'; } else { $vista='vista_socios'; }  $param = "$('#cedu_soci').val()"; combo('cedu_soci', $cedu_soci, $vista, $link, 0, 0, 1, '', 'cedu_soci', 'href="javascript:;" onchange="monto_disponible('.$param.');return false;"', $boton, "ORDER BY nomb_soci",''); ?>						</td>
                      </tr>
<script>
function f_monto_disponible(cedu_fiad){
        var parametros = {
                "cedu_fiad" : cedu_fiad
        };
        $.ajax({
                data:  parametros,
                url:   'capa_prestamos_f_md.php',
                type:  'post',
                beforeSend: function () {
                        $("#f_md").html("Procesando, espere por favor...");
                },
                success:  function (response) {	
                        $("#f_md").html(response);
                }
        });
}

function actualizar_capa(contenedor){
	if(contenedor=='fiadores'){
		var parametros = {
			"orig_fian" : "fiador",
			"codg_prst" : $('#codg_prst').val(),
			"cedu_soli" : $('#cedu_soci').val()
		};
		var url = 'capa_prestamos_fiadores.php';
	}
	if(contenedor=='fianzas'){
		var parametros = {
			"orig_fian" : "fianza",
			"codg_prst" : $('#codg_prst').val(),
			"cedu_soli" : $('#cedu_soci').val()
		};
		var url = 'capa_prestamos_fiadores.php';
	}
	if(contenedor=='tipo_cuotas'){
		var parametros = {
			"cedu_soci" : $('#cedu_soci').val()
		};
		var url = 'capa_prestamos_tipo_cuotas.php';
	}
	if(contenedor=='prestamos_por_pagar'){
		var parametros = {
			"cedu_soci" : $('#cedu_soci').val(),
			"tipo_accion" : 'prestamo'
		};
		var url = 'capa_prestamos_por_pagar.php';
	}
	if(contenedor=='prestamo_maximo'){
		var parametros = {
			"md_ocu" : $('#md_ocu').val(),
			"mont_fiadores" : $('#mont_fiadores').val(),
			"mont_fianzas" : $('#mont_fianzas').val()
		};
		var url = 'capa_prestamo_maximo.php';
	}
	if(contenedor=='carcular_cuotas'){
		var parametros = {
		};
		var url = 'capa_prestamos_calcular_cuotas.php';
	}
        $.ajax({
                data:  parametros,
                url:   url,
                type:  'post',
                beforeSend: function () {
                        $('#' + contenedor).html("Procesando, espere por favor...");
                },
                success:  function (response) {	
                        $('#' + contenedor).html(response);
                }
        });
}

function registra_fiador(codg_prst,cedu_fiad,orig_fian){
	if(!$('#monto_fiador').val()){
		alert('Error: el monto solicitado no puede ser cero');
	}else{
		if(($('#md_f_ocu').val()-$('#monto_fiador').val())<0){
			alert('Error: el monto solicitado es mayor al disponible para el fiador');
		}else{
			if($('#cedu_soci').val()==cedu_fiad){
				alert("No se puede colocar a un ahorrista como fiador de si mismo");
			}else{
				var parametros = {
					"codg_prst" : codg_prst,
					"cedu_soli" : $('#cedu_soci').val(),
					"cedu_fiad" : cedu_fiad,
					"orig_fian" : orig_fian,
					"monto_fiador" : $('#monto_fiador').val()
				};
				$.ajax({
					data:  parametros,
					url:   'prestamos_registra_fiador.php',
					type:  'post',
					beforeSend: function () {
						$("#f_md").html("Procesando, espere por favor...");
					},
					success:  function (response) {	
						$("#f_md").html(response);
						actualizar_capa("fiadores");
					}
				});
			}
		}
	}
}
function elimina_fiador(codg_prtf, contenedor){
	var parametros = {
		"codg_prtf" : codg_prtf
	};
	$.ajax({
		data:  parametros,
		url:   'prestamos_elimina_fiador.php',
		type:  'post',
		beforeSend: function () {			
		},
		success:  function (response) {	
			actualizar_capa(contenedor);
		}
	});
}
</script>	
					  <?PHP if($boton!="Buscar"){ ?>
                      <tr>
                        <td width="27%" class="etiquetas">Monto&nbsp;Disponible&nbsp;(MD):</td>
						<td colspan="2"><span id="md"><?PHP include_once('capa_prestamos_md.php'); ?></span></td>
                      </tr>					  
                      <tr>
                        <td width="27%" class="etiquetas">Agregar&nbsp;Fiador:</td>

			            <td colspan="2"><?PHP if ($cedu_fiad=='' && $_POST['cedu_fiad']==''){ $vista='vista_socios_activos'; } else { $vista='vista_socios'; }  $param = "$('#cedu_fiad').val()"; combo('cedu_fiad', $cedu_fiad, $vista, $link, 0, 0, 1, '', 'cedu_fiad', 'href="javascript:;" onchange="f_monto_disponible('.$param.');return false;"', $boton, "ORDER BY nomb_soci",''); ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">MD del Fiador: </td>
                        <td colspan="2"><span id="f_md"><?PHP include_once('capa_prestamos_f_md.php'); ?></span>
                          <?PHP $param="$('#codg_prst').val(), $('#cedu_fiad').val(), 'fiador'"; ?><?PHP if($boton!="Buscar"){ ?>
                          <img src="../imagenes/mas.png" width="24" height="24" style="cursor:pointer"  <?PHP echo 'href="javascript:;" onclick="registra_fiador('.$param.');return false;"'; ?> />
                          <?PHP } ?></td>
                      </tr>
					  <?PHP } ?>
                      <tr>
                        <td width="27%" class="etiquetas">Fiadores:</td>
			            <td colspan="2">&nbsp;<span id="fiadores"><?PHP $orig_fian='fiador'; include_once('capa_prestamos_fiadores.php'); ?></span></td>
                      </tr>
<script>
function registra_fianza(codg_prst,desc_fian,orig_fian){
	if(!$('#monto_fianza').val() || !desc_fian){
		alert('Error: ningun dato de la fianza puede estar vacio' );
	}else{
		var parametros = {
			"codg_prst" : codg_prst,
			"cedu_soli" : $('#cedu_soci').val(),
			"desc_fian" : desc_fian,
			"orig_fian" : orig_fian,
			"monto_fiador" : $('#monto_fianza').val()
		};
		$.ajax({
			data:  parametros,
			url:   'prestamos_registra_fiador.php',
				type:  'post',
				beforeSend: function () {
						$("#fza").html("Procesando, espere por favor...");
				},
				success:  function (response) {	
					$("#fza").html(response);
					actualizar_capa("fianzas");
				}
		});
	}
}
</script>
<script>
	function suma_prestamo_pagado(origen,monto){
		if (!monto){
			alert('Debe especificar cantidad de meses para efectuar calculo de inter�s');
			$(origen).attr('checked', false);
		}
		else {
			var estado = $(origen).attr('checked');
			var nuevo_max =  Number($('#prest_maximo').val());
			monto = Number(monto);
			if (estado){
				nuevo_max = nuevo_max + monto;
			}else{
				nuevo_max = nuevo_max - monto;
			}
			$('#md_ocu').val(nuevo_max);
			$('#prest_maximo').val(nuevo_max);
			$('#label_prest_maximo').html(nuevo_max);
		}		
	}
</script>
                      <?PHP if($boton!="Buscar"){ ?>
					  <tr>
                        <td width="27%" class="etiquetas">Agregar Fianza:</td>	
                        <td colspan="2"><?PHP $param="$('#codg_prst').val(), $('#desc_fian').val(), 'fianza'"; ?><input name="desc_fian" id="desc_fian" type="text" size="15" maxlength="50" placeholder="Objeto de fianza" title="Objeto de Fianza, Ej. Terreno, Casa, Carro, Maquinaria, entre otros" /><input name="monto_fianza" id="monto_fianza" type="text" size="5" maxlength="11" placeholder="0.00" /><?PHP if($boton!="Buscar"){ ?><img src="../imagenes/mas.png" width="24" height="24" style="cursor:pointer" <?PHP echo 'href="javascript:;" onclick="registra_fianza('.$param.');return false;"'; ?> /><?PHP } ?></td>
                      </tr>
					  <?PHP } ?>
                      <tr>
                        <td width="27%" class="etiquetas">Fianzas:</td>
                        <td colspan="2"><span id="fianzas"><?PHP $orig_fian='fianza'; include('capa_prestamos_fiadores.php'); ?></span></td>
                      </tr>
					  <?PHP if($boton!="Buscar"){ ?>
					  <tr>
                        <td width="27%" class="etiquetas">Monto Maximo:</td>
                        <td colspan="2"><span id="prestamo_maximo"><?PHP include_once('capa_prestamo_maximo.php'); ?></span></td>
                      </tr>
					  <?PHP } ?>
                      <tr>
                        <td width="27%" class="etiquetas">Monto Solicitado:</td>
                        <td colspan="2"><input name="mont_prst" id="mont_prst" type="text" size="15" maxlength="11" value="<?PHP echo $mont_prst; ?>" placeholder="0.00" /></td>
                      </tr>	  
                      <tr>
                        <td width="27%" class="etiquetas">N� de Solicitud:</td>
                        <td colspan="2"><input name="nsol_prst" id="nsol_prst" type="text" size="15" maxlength="11" value="<?PHP echo $nsol_prst; ?>" />
                        <?php 
                        	if ($boton == 'Buscar' && $EDITPRES==1) { 
                        		$onclick_nsol_prst = "actualizar_dato('prestamos','nsol_prst',$('#nsol_prst').val(),'codg_prst',$('#codg_prst').val(),'nsol_prst_resul');";
                        		echo '<img src="../imagenes/default_on.png" style="cursor: pointer;" onclick="'.$onclick_nsol_prst.'"> <span id="nsol_prst_resul"></span>'; 
                        	} 
                        ?>
                        </td>
                      </tr> 
                     <?php 
                      $funcion_combo_tipo = 'Onchange="tipo_prestamo_plazo(this.value, ';
					   	 $funcion_combo_tipo .= "'plaz_prst')";
					     	 $funcion_combo_tipo .= '";';
					     ?>                    
                      <tr>
                        <td class="etiquetas">Tipo de Prestamo </td>
                        <td colspan="2">&nbsp;<?php combo('codg_tipo_pres', $codg_tipo_pres, 'tipo_prestamos', $link, 0, 1, '', '', 'codg_tipo_pres', $funcion_combo_tipo, $boton, "ORDER BY  nomb_tipo_pres",''); ?>
                         <input name="plaz_prst" id="plaz_prst" type="text" size="2" maxlength="3" value="<?PHP echo $plaz_prst; ?>" />Meses</td>
                      </tr>
                      <tr>
                        <td width="27%" class="etiquetas">Observaciones:</td>
                        <td colspan="2"><label>
                          <textarea id="obsr_prst" name="obsr_prst" cols="30" rows="3"><?PHP echo $obsr_prst; ?></textarea>
                        </label>
                        <?php 
                        	if ($boton == 'Buscar' && $EDITPRES==1) { 
                        		$onclick_obsr_prst = "actualizar_dato('prestamos','obsr_prst',$('#obsr_prst').val(),'codg_prst',$('#codg_prst').val(),'obsr_prst_resul');";
                        		echo '<img src="../imagenes/default_on.png" style="cursor: pointer;" onclick="'.$onclick_obsr_prst.'"> <span id="obsr_prst_resul"></span>'; 
                        	} 
                        ?>
                        </td>
                      </tr>
                      <tr>
                      	<td class="etiquetas">Estatus de Prestamo:</td>
                        <td colspan="2"><?php  echo '<select name="stat_prst" title="Estatus de Pr�stamo">
                          <option value="">Seleccione...</option>
						  <option value="P" '; if ($stat_prst == "P" || $_POST['stat_prst'] =="P") { echo 'selected'; } echo '>Pendiente</option>
                          <option value="A" '; if ($stat_prst == "A" || $_POST['stat_prst'] =="A") { echo 'selected'; } echo '>Aprobado</option>
                          <option value="N" '; if ($stat_prst == "N" || $_POST['stat_prst'] =="N") { echo 'selected'; } echo '>Negado</option>
                        </select>'; 
							?></tr>
                      <tr>
                        <td width="27%" class="etiquetas">N� de Acta:</td>
                        <td colspan="2"><input name="numr_acta" id="numr_acta" type="text" size="15" maxlength="11" value="<?PHP echo $numr_acta; ?>" />
                        <?php 
                        	if ($boton == 'Buscar' && $EDITPRES==1) { 
                        		$onclick_numr_acta = "actualizar_dato('prestamos','numr_acta',$('#numr_acta').val(),'codg_prst',$('#codg_prst').val(),'numr_acta_resul');";
                        		echo '<img src="../imagenes/default_on.png" style="cursor: pointer;" onclick="'.$onclick_numr_acta.'"> <span id="numr_acta_resul"></span>'; 
                        	} 
                        ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="27%" class="etiquetas">Fecha&nbsp;de&nbsp;Acta:</td>
                        <td colspan="2"><?php escribir_campo('fcha_acta',$_POST["fcha_acta"],$fcha_acta,'',50,10,'Fecha de Registro del Pr&eacute;stamo','Verificar',$fcha_acta,'fecha','','')?>
                        <?php 
                        	if ($boton == 'Buscar' && $EDITPRES==1) { 
                        		$onclick_fcha_acta = "actualizar_dato('prestamos','fcha_acta',$('#fcha_acta').val(),'codg_prst',$('#codg_prst').val(),'fcha_acta_resul');";
                        		echo '<img src="../imagenes/default_on.png" style="cursor: pointer;" onclick="'.$onclick_fcha_acta.'"> <span id="fcha_acta_resul"></span>'; 
                        	} 
                        ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="27%" class="etiquetas">Monto Otorgado:</td>
                        <td colspan="2"><input name="mont_apro" id="mont_apro" type="text" size="15" maxlength="11" value="<?PHP echo $mont_apro; ?>" placeholder="0.00" />
                        <?php 
                        	if ($boton == 'Buscar' && $EDITPRES==1) { 
                        		$onclick_mont_apro = "actualizar_dato('prestamos','mont_apro',$('#mont_apro').val(),'codg_prst',$('#codg_prst').val(),'mont_apro_resul');";
                        		$onclick_mont_apro .= "actualizar_dato('prestamos','mont_cuot_real','00.0','codg_prst',$('#codg_prst').val(),'mont_apro_resul');";
                        		$onclick_mont_apro .= "actualizar_dato('prestamos','sald_inic_real','00.0','codg_prst',$('#codg_prst').val(),'mont_apro_resul');";
                        		echo '<img src="../imagenes/default_on.png" style="cursor: pointer;" onclick="'.$onclick_mont_apro.'"> <span id="mont_apro_resul"></span>'; 
                        	} 
                        ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="27%" class="etiquetas">Tipo de Cuotas:</td>
                        <td colspan="2"><span id="tipo_cuotas"><?PHP include_once('capa_prestamos_tipo_cuotas.php'); ?></span></td>
                      </tr>
                      <tr>
                        <td width="27%" class="etiquetas">Tasa de Inter�s:</td>
                        <td colspan="2"><input name="mont_intr" id="mont_intr" type="text" size="5" maxlength="11" value="<?PHP echo $mont_intr; ?>" /></td>
                      </tr>
                      <tr>
                        <td width="27%" class="etiquetas">Monto de Cuotas:</td>
<script>
function calcular_cuotas(){
	var error=0;
	if(!$('#mont_apro').val() || !$('#mont_intr').val() || !$('#tipo_cuot').val() || !$('#plaz_prst').val() || !$('#mont_prst').val() || !$('#prest_maximo').val()){
		alert('Error: hacen falta datos para calcular las cuotas');
		error=1;
	}else if(Number($('#mont_prst').val()) > Number($('#prest_maximo').val())){ 
		alert('Error: el monto solicitado no puede ser mayor que el maximo a prestar'); 
		error=1;
	}else if(Number($('#mont_apro').val()) > Number($('#mont_prst').val())){ 
		alert('Error: el monto otorgado no puede ser mayor que el monto solicitado'); 
		error=1;
	}else if(Number($('#mont_apro').val()) > Number($('#prest_maximo').val())){
		alert('Error: el monto otorgado no puede ser mayor que el maximo a prestar'); 
		error=1;		
	}
	if(error==0){
		
		var parametros = {
			"mont_apro" : $('#mont_apro').val(),
			"mont_intr" : $('#mont_intr').val(),
			"tipo_cuot" : $('#tipo_cuot').val(),
			"plaz_prst" : $('#plaz_prst').val()
		};
		$.ajax({
			data:  parametros,
			url:   'capa_prestamos_calcular_cuotas.php',
			type:  'post',
			beforeSend: function () {
				$("#carcular_cuotas").html("Procesando, espere por favor...");
			},
			success:  function (response) {	
				$("#carcular_cuotas").html(response);
			}
		});
	}else{
		actualizar_capa("carcular_cuotas");
	}
}
</script>
                        <td colspan="2"><span id="carcular_cuotas"><?PHP include_once('capa_prestamos_calcular_cuotas.php'); ?></span>
                          <?PHP if($boton!="Buscar"){ ?><input name="boton" id="calcula" type="button" <?PHP echo 'href="javascript:;" onclick="calcular_cuotas();return false;"'; ?> value="Calcular" /><?PHP } ?>
                        <?php 
                        	if ($boton == 'Buscar' && $EDITPRES==1) { 
                        		$onclick_mont_cuot = "actualizar_dato('prestamos','mont_cuot',$('#mont_cuot').val(),'codg_prst',$('#codg_prst').val(),'mont_cuot_resul');";
                        		$onclick_mont_cuot .= "actualizar_dato('prestamos','mont_cuot_real','00.0','codg_prst',$('#codg_prst').val(),'mont_cuot_resul');";
                        		$onclick_mont_cuot .= "actualizar_dato('prestamos','sald_inic_real','00.0','codg_prst',$('#codg_prst').val(),'mont_cuot_resul');";
                        		echo '<img src="../imagenes/default_on.png" style="cursor: pointer;" onclick="'.$onclick_mont_cuot.'"> <span id="mont_cuot_resul"></span>';
                        	} 
                        ?>
                        </td>
                      </tr>
                      <tr>
                        <td colspan=3><hr></td>
                      </tr>
					  <?PHP if($boton!="Buscar"){ ?>
                      <tr>
                        <td colspan=3 class="etiquetas_centradas">Si se est� pagando un prestamo anterior por favor seleccionelo de la lista</td>
                      </tr>					  
                      <tr>
                        <td colspan="3">&nbsp;<span id="prestamos_por_pagar"><?PHP include_once('capa_prestamos_por_pagar.php'); ?></span></td>
                      </tr>
					  <?PHP }else{ ?>
                      <tr>
                        <td colspan=3 class="etiquetas_centradas">Prestamos que fueron cancelados con la realizacion del que consulta</td>
                      </tr>	
                      <tr>
                        <td colspan="3">&nbsp;<span id="prestamos_por_pagar"><?PHP include_once('capa_prestamos_pagados.php'); ?></span></td>
                      </tr>					  
					  <?PHP } ?>
                      <tr>
                        <td colspan="3" align="center"><hr></td>
                      </tr>
                      <tr>
                        <td colspan="3" align="center"><input name="accion" id="accion" type="hidden" value="Guardar" />&nbsp;<input name="boton" id="boton" type="button" value="Guardar" <?PHP if($boton=="Buscar"){ echo 'disabled="disabled"'; } ?> onclick="valida_envia();"  />
                        &nbsp;
						  <?PHP if($boton=="Buscar"){ ?>
						  <?PHP $pres_mov = buscar_campo('*', 'prestamos_mov', 'WHERE codg_prst="'.$codg_prst.'"' ); ?>		
                          <input name="boton" id="boton" type="submit" value="Eliminar"  <?PHP if($pres_mov){ echo 'disabled="disabled"'; } ?>  />
                          <?PHP } ?></td>
                      </tr>
					  <tr>
                        <td colspan="3" align="center">
                        <?php if($boton=='Buscar'){ abrir_ventana('../reportes/tabla_amortizacion.php','v_imprimir','Tabla de Amortizaci�n',"codg_prst=".$codg_prst."&seccion=".$_GET['seccion']); } ?>
                        <?php if($boton=='Buscar'){ abrir_ventana('../reportes/tabla_amortizacion_real.php','v_imprimir','Tabla de Ejecuci�n Real',"codg_prst=".$codg_prst."&seccion=".$_GET['seccion']); } ?><br>
                    	<?php if($boton=='Buscar'){ abrir_ventana('../reportes/solicitud_prestamo.php','v_prestamo','Solicitud de Pr�stamo',"cedu_soci=".$cedu_soci."&codg_prst=".$codg_prst."&seccion=".$_GET['seccion']); } ?>
                        <?php if($boton=='Buscar'){ abrir_ventana('../reportes/acta_prestamo.php','v_imprimir','Acta de Prestamo',"codg_prst=".$codg_prst."&seccion=".$_GET['seccion']); } ?>                        

                        </td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                </table>
				<?PHP
					}else{
				?>
<table width="90%" border="1" cellspacing="0" cellpadding="0" class="etiquetas">
  <tr>
    <td width="20px" height="20">#</td>
    <td width="60px">Num.</td>
    <td width="60px">Fecha</td>
	<td width="70px">Cedula</td>
    <td>Socio</td>
	<td>Tipo</td>
    <td width="70px">Cuota</td>
    <td width="80px">Monto</td>
    <td width="80px">Deuda</td>
	<td width="50px">Acciones</td>
  </tr>
  <?PHP $i=1; do{ 
	$sql_deuda = "SELECT if (sum(capi_prtm), sum(capi_prtm), 0) as pagado FROM prestamos_mov WHERE codg_prst = ".$reg['codg_prst'];
	$pagado = mysql_fetch_array(mysql_query($sql_deuda));
	$pagado = $pagado[pagado];
  ?>
  <tr>
    <td><?PHP echo $i; ?></td>    
    <td><?PHP echo $reg['codg_prst']; ?></td>
    <td><?PHP echo ordenar_fecha($reg['fcha_prst']); ?></td>
    <td><?PHP echo $reg['naci_soci']."-".$reg['cedu_soci']; ?></td>
	<td><?PHP echo $reg['nomb_soci']."&nbsp;".$reg['apel_soci']; ?></td>
	<td><?PHP echo $reg['nomb_tipo_pres']; ?></td>
    <td align="right"><?PHP echo redondear($reg['mont_cuot'],2,'.',','); ?>&nbsp;</td>
    <td align="right"><?PHP echo redondear($reg['mont_apro'],2,'.',','); ?>&nbsp;</td>
    <td align="right"><?PHP echo redondear(($reg['mont_apro']-$pagado),2,'.',','); ?>&nbsp;</td>
	<td><a href="prestamos_nueva.php?<?PHP echo "codg_prst=".$reg['codg_prst']."&boton=Buscar&busqueda=individual"; ?>"><img src="../imagenes/buscar.png" width="24" height="24" border="0" /></a></td>
  </tr>
  <?PHP $i++; }while($reg=mysql_fetch_array($busq)); ?>
</table>				
				<?PHP
					}
				?>				
                </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
