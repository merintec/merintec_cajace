<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../comunes/jquery.min.js'></script>
<?php 
include ('../comunes/formularios_funciones.php');
$codg_cnta=$_POST['codg_cnta'];
$nmro_egre=$_POST['nmro_egre'];
$fcha_egre=$_POST['fcha_egre'];
$moti_egre=$_POST['moti_egre'];
$codg_egre=$_POST['codg_egre'];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'egresos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "egresos";	// nombre de la tabla
$ncampos = "19";			//numero de campos del formulario
$datos[0] = crear_datos ("fcha_egre","Fecha del egreso",$_POST['fcha_egre'],"10","10","fecha");
$datos[1] = crear_datos ("nmro_egre","Numero de comprobante de Pago",$_POST['nmro_egre'],"0","4","numericos");
$datos[2] = crear_datos ("moti_egre","Motivo del Egreso",$_POST['moti_egre'],"1","255","alfanumericos");
$datos[3] = crear_datos ("codg_mpio","Codigo de Montepio",$_POST['codg_mpio'],"0","11","alfanumericos");
$datos[4] = crear_datos ("cod_pro","Codigo de Proveedor",$_POST['cod_pro'],"0","11","alfanumericos");
$datos[5] = crear_datos ("codg_prst","Codigo de Prestamo",$_POST['codg_prst'],"0","11","alfanumericos");
$datos[6] = crear_datos ("codg_reti","Codigo de Retiro",$_POST['codg_reti'],"0","11","alfanumericos");
$datos[7] = crear_datos ("nomb_rela","Nombre o Raz�n Social",$_POST['nomb_rela'],"1","100","alfanumericos");
$datos[8] = crear_datos ("codg_rela","RIF o C�dula de Nombre o Raz�n Social",$_POST['codg_rela'],"0","50","alfanumericos");
$datos[9] = crear_datos ("codg_cnta","Banco",$_POST['codg_cnta'],"1","11","numericos");
$datos[10] = crear_datos ("nmro_cheq","N� de Cheque o Transferencia",$_POST['nmro_cheq'],"1","11","numericos");
$datos[11] = crear_datos ("elab_egre","Elaborado",$_POST['elab_egre'],"1","30","alfabeticos");
$datos[12] = crear_datos ("revi_egre","Revisado",$_POST['revi_egre'],"1","30","alfabeticos");
$datos[13] = crear_datos ("autr_egre","Autorizado",$_POST['autr_egre'],"1","30","alfabeticos");
$datos[14] = crear_datos ("cntb_egre","Contabilizado",$_POST['cntb_egre'],"1","30","alfabeticos");
$datos[15] = crear_datos ("anul_egre","Anulado",$_POST['anul_egre'],"0","2","alfabeticos");
$datos[16] = crear_datos ("anul_obsr","Anulado",$_POST['anul_obsr'],"0","255","alfabeticos");
$datos[17] = crear_datos ("dedu_egre","Deducciones",$_POST['dedu_egre'],"0","12","decimal");
$datos[18] = crear_datos ("obsr_egre","Observaciones",$_POST['obsr_egre'],"0","150","alfanumericos");
//// si se est� utilizando el combo de ayuda de cuentas
if ($_POST['ayuda_cuenta']){
  $boton = "Modificar";	
  $_POST["BuscarInd"]='si';
  $_POST["buscar_a"]=$_POST['codg_egre'];
  $_POST["criterio"]='codg_egre';
  $ayuda_cuenta = $_POST['ayuda_cuenta'];
  $sql_cuenta = "SELECT * FROM cuentas WHERE codg_pcnta = ".$ayuda_cuenta;
  $res_cuenta = mysql_fetch_array(mysql_query($sql_cuenta));
  $codg_ctab_in = $res_cuenta["nmro_cnta"];
  $conc_egre_in = $res_cuenta["nomb_cnta"]; 
}
if ($_POST['asiento'] && !$_POST["Buscar"] && !$_POST["BuscarInd"]){
	echo '<script>alert("No se pueden efectuar modificaciones ya que el comprobante tiene un asiento registrado. \n\nElimine el asiento si desea modificar.")</script>';
	$boton = "Modificar";	
	$_POST["BuscarInd"]='si';
	$_POST["buscar_a"]=$_POST['codg_egre'];
	$_POST["criterio"]='codg_egre';
} 
//// si se esta ingresando un nuevo concepto
if ($_POST['accion']=="insertar_concepto" && !$_POST['asiento']){
	$_POST['moti_egre']='';
	///// creamos la matriz de datos para insertar un  nuevo concepto
	$tabla1 = "egresos_conceptos";	// nombre de la tabla
    $ncampos1 = "6";			//numero de campos del formulario
    $datos1[0] = crear_datos ("codg_egre","Codigo del egreso",$_POST['codg_egre'],"0","11","numericos");
    $datos1[1] = crear_datos ("codg_ctab","Condigo de Cuenta Contable",$_POST['codg_ctab_in'],"0","100","alfanumericos");
    $datos1[2] = crear_datos ("conc_egre","Concepto del Egreso",$_POST['conc_egre_in'],"1","255","alfanumericos");    
    $datos1[3] = crear_datos ("mnto_debe","Monto por el DEBE",$_POST['mnto_debe_in'],"0","13","decimal");
    $datos1[4] = crear_datos ("mnto_hber","Monto por el HABER",$_POST['mnto_hber_in'],"0","13","decimal");
    $datos1[5] = crear_datos ("codg_exce","Cuenta por pagar",$_POST['por_pagar'],"0","13","numericos");
    ////// Validar los campos de concepto nuevo
    $validacion1 = validando_campos ($ncampos1,$datos1);
    if ($validacion1 && $_POST['mnto_hber_in']=='' && $_POST['mnto_debe_in']==''){
            echo "<script>alert('Debe indicar un monto en el Debe o en el Haber');</script>";
            $validacion1 = '';        
        }
    //// verificamos que el monto del debe no exeda el monto a pagar
        if ($_POST['monto_pagar']<$_POST['mnto_debe_in'] && $validacion1 && $_POST['mnto_debe_in']!='' && ($_POST['moti_egre']=="MONTEPIO" AND $_POST['$moti_egre']=="Retiro" || $_POST['$moti_egre']=="Pr�stamo")){
            echo "<script>alert('El monto del Debe no puede exeder el Monto a Pagar. El concepto no se almacenar�');</script>";
            $validacion1 = '';        
        }
        /*if ($_POST['mnto_hber_in']!='' && (($_POST['total_haber']+$_POST['mnto_hber_in'])>$_POST['total_debe']) ){
            echo "<script>alert('El ".$_POST['total_debe']." ".($_POST['total_haber']+$_POST['mnto_hber_in'])." monto del Haber no puede exeder el Monto del Debe. El concepto no se almacenar�');</script>";
            $validacion1 = '';        
        }*/
	if ($validacion1) { 
	    insertar_func_nomina($ncampos1,$datos1,$tabla1);
	    $codg_ctab_in='';
	    $conc_egre_in='';
	    $mnto_debe_in='';
	    $mnto_hber_in='';
	}
	$boton="Modificar"; 
	$_POST["BuscarInd"]='si';
	$_POST["buscar_a"]=$_POST['codg_egre'];
	$_POST["criterio"]='codg_egre';
}
if ($_POST['accion']=="eliminar_concepto" && !$_POST['asiento']){
	$tabla1 = "egresos_conceptos";	// nombre de la tabla
    $ncampos1 = "5";		    	//numero de campos del formulario
    if ($_POST['valor']){
        $sql_eliminar = "DELETE FROM ".$tabla1." WHERE codg_egco =".$_POST['valor'];
        mysql_query($sql_eliminar);
    }
	$boton="Modificar"; 
	$_POST["BuscarInd"]='si';
	$_POST["buscar_a"]=$_POST['codg_egre'];
	$_POST["criterio"]='codg_egre';
}
if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Fecha";
		$datos[0]="fcha_egre";
		$parametro[1]="N� Comprobante";
		$datos[1]="nmro_egre";
		$parametro[2]="Motivo";
		$datos[2]="moti_egre";
		$parametro[3]="Beneficiario";
		$datos[3]="nomb_rela";
		busqueda_varios(6,$buscando,$datos,$parametro,"codg_egre");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_egre = $row["codg_egre"];
	    $fcha_egre = $row["fcha_egre"];
	    $nmro_egre = $row["nmro_egre"];
	    $moti_egre = $row["moti_egre"];
	    $codg_mpio = $row["codg_mpio"];
	    if ($codg_mpio=='') { $codg_mpio='NULL'; }
	    $cod_pro = $row["cod_pro"];
	    if ($cod_pro=='') { $cod_pro='NULL'; }
	    $codg_prst = $row["codg_prst"];
	    if ($codg_prst=='') { $codg_prst='NULL'; }
	    $codg_reti = $row["codg_reti"];
	    if ($codg_reti=='') { $codg_reti='NULL'; }
	    $nomb_rela = $row["nomb_rela"];
	    $codg_rela = $row["codg_rela"];
	    $codg_cnta = $row["codg_cnta"];
	    $nmro_cheq = $row["nmro_cheq"];
	    $elab_egre = $row["elab_egre"];
	    $revi_egre = $row["revi_egre"];
	    $autr_egre = $row["autr_egre"];
	    $cntb_egre = $row["cntb_egre"];
	    $anul_egre = $row["anul_egre"];
	    $anul_obsr = $row["anul_obsr"];
	    $dedu_egre = $row["dedu_egre"];
	    $obsr_egre = $row["obsr_egre"];
	    if ($moti_egre=="Pr�stamo"){
	        $sql_moti = "Select * from prestamos WHERE codg_prst=".$codg_prst;
	        $res_moti = mysql_fetch_array(mysql_query($sql_moti));
	        $monto_pagar = $res_moti['mont_apro'];
	        //** consultamos los montos que ya se han pagado al beneficiario seleccionado
	        $sql_pagado = "SELECT SUM(mnto_debe) as monto FROM egresos e, egresos_conceptos ec WHERE e.moti_egre='Pr�stamo' AND e.codg_prst=".$codg_prst." AND e.codg_rela='".$codg_rela."' AND e.nomb_rela='".$nomb_rela."' AND e.codg_egre=ec.codg_egre GROUP BY e.codg_mpio,codg_rela";
	        $res_pagado = mysql_fetch_array(mysql_query($sql_pagado));
	        // restamos el monto que ya se ha pagado
	        $monto_pagar = $monto_pagar - $res_pagado['monto'];
	    }
	    if ($moti_egre=="Retiro"){
	        $sql_moti = "Select * from retiros WHERE codg_reti=".$codg_reti;
	        $res_moti = mysql_fetch_array(mysql_query($sql_moti));
	        $monto_pagar = redondear(($res_moti["apr_reti"]+$res_moti["ret_reti"]+$res_moti["rei_reti"]),2,"",".");
	        //** consultamos los montos que ya se han pagado al beneficiario seleccionado
	        $sql_pagado = "SELECT SUM(mnto_debe) as monto FROM egresos e, egresos_conceptos ec WHERE e.moti_egre='Retiro' AND e.codg_reti=".$codg_reti." AND e.codg_rela='".$codg_rela."' AND e.nomb_rela='".$nomb_rela."' AND e.codg_egre=ec.codg_egre GROUP BY e.codg_mpio,codg_rela";
	        $res_pagado = mysql_fetch_array(mysql_query($sql_pagado));
	        // restamos el monto que ya se ha pagado
	        $monto_pagar = $monto_pagar - $res_pagado['monto'];
	    }
	    if ($moti_egre=="MONTEPIO"){
	        // consultamos los datos del montepio a pagar
	        $sql_mpio = "SELECT * FROM montepio WHERE codg_mpio=".$codg_mpio;
	        $reg_mpio = mysql_fetch_array(mysql_query($sql_mpio));
	        // consultamos el monto total a pagar por este montepio
	        if ($reg_mpio[mont_mpio]>0){
	        	$monto_pagar = $reg_mpio[mont_mpio];
	        }
	        else{
		        $sql_moti = "SELECT SUM(haber_movi) as mont_pagar from movimientos_contables WHERE codg_pcnta = ".$reg_mpio[codg_pcnta];
		        $res_moti = mysql_fetch_array(mysql_query($sql_moti));
		        $monto_pagar = $res_moti['mont_pagar'];
	        }
	        // si es por muerte del asociado consultar el porcentaje que corresponde al beneficiario seleccionado
	        if ($reg_mpio['tipo_mpio']=='S'){
                    $sql_por_benef = "SELECT * FROM socios_beneficiarios where cedu_soci=".$reg_mpio['cedu_soci']." AND cedu_benf='".$codg_rela."' AND CONCAT(apel_benf, ' ' ,nomb_benf)='".$nomb_rela."'";
	            $res_por_benef = mysql_fetch_array(mysql_query($sql_por_benef));
	            $monto_pagar = redondear(($monto_pagar*$res_por_benef['porc_benf']/100),2,'','.');	        
	        }

	        //** consultamos los montos que ya se han pagado al beneficiario seleccionado
	        $sql_pagado = "SELECT SUM(mnto_debe) as monto FROM egresos e, egresos_conceptos ec WHERE e.moti_egre='MONTEPIO' AND e.codg_mpio=".$codg_mpio." AND e.codg_rela='".$codg_rela."' AND e.nomb_rela='".$nomb_rela."' AND e.codg_egre=ec.codg_egre GROUP BY e.codg_mpio,codg_rela";
	        $res_pagado = mysql_fetch_array(mysql_query($sql_pagado));
	        // restamos el monto que ya se ha pagado
	        $monto_pagar = $monto_pagar - $res_pagado['monto'];
	    }
	    $_POST['moti_egre']='';
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
///// si el motivo del egreso es un montepio se debe verificar si es necesarios pasar el montepio a estatus PAGADO
if (($_POST["confirmar"]=="Guardar") ||  ($_POST["confirmar"]=="Actualizar") && ($_POST['moti_egre']=='MONTEPIO') && !$_POST['asiento']) {
        /// si se esta actualizando le quitamos el estatus de pagado al montepio que estaba inicialmente registrado, 
        /// Esto por si se cambia de montepio seleccionado para luego reasignarle el estatus
        if ($_POST["confirmar"]=="Actualizar") {
                $sql_tmp = "SELECT * FROM egresos WHERE codg_egre=".$_POST['codg_egre'];
                $res_tmp = mysql_fetch_array(mysql_query($sql_tmp));
                $update_tmp = "UPDATE montepio set stat_mpio='' WHERE codg_mpio=".$res_tmp['codg_mpio'];
                mysql_query($update_tmp);
        }
        /// Se consulta el montepio para verificar que tipo de montepio es (S o F)
	$sql_mpio = "SELECT * FROM montepio WHERE codg_mpio = ".$_POST['codg_mpio'];
	$reg_mpio = mysql_fetch_array(mysql_query($sql_mpio));
 	/// Si es por muerte de un familiar se asigna el estatos P al montepio a cancelar
 	if ($reg_mpio['tipo_mpio']=='F'){
		//$sql_update="UPDATE montepio set stat_mpio='P' WHERE codg_mpio = ".$_POST['codg_mpio'];	
		mysql_query($sql_update);
	}
	/// Si es por muerte del asociado se verifica que el n�mero de beneficiarios sea igual al numero de beneficiarios que reciben pago por este motivo
	if ($reg_mpio['tipo_mpio']=='S')
	{
	   $sql_benef = "SELECT * FROM socios s, socios_beneficiarios sf WHERE s.cedu_soci=".$reg_mpio[cedu_soci]." AND s.cedu_soci=sf.cedu_soci AND sf.porc_benf>0 AND sf.cedu_benf  NOT IN (SELECT codg_rela FROM egresos WHERE moti_egre='MONTEPIO' AND codg_mpio='$reg_mpio[codg_mpio]' AND codg_rela=sf.cedu_benf AND nomb_rela=CONCAT(sf.apel_benf, ' ' ,sf.nomb_benf))" ;
	   $cuenta=mysql_num_rows(mysql_query($sql_benef));
	   if (($_POST['confirmar']=="Actualizar" && $cuenta==0) || ($_POST['confirmar']=="Guardar" && $cuenta==1))
	   {
	   	//$sql_update="UPDATE montepio set stat_mpio='P' WHERE codg_mpio = ".$_POST['codg_mpio'];	
	    	mysql_query($sql_update);
	   }
	}	
}
if ($_POST["confirmar"]=="Actualizar" && !$_POST['asiento']) 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_egre",$_POST["codg_egre"],$pagina,"");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar" && !$_POST['asiento']) 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar" && !$_POST['asiento']) 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar" && !$_POST['asiento']) 
{
    /// Generar el Numero de Egreso
    $sql = "SELECT MAX(nmro_egre) AS nmro_egre FROM egresos WHERE YEAR(fcha_egre)=";
	$sql .= $_POST["fcha_egre"][0].$_POST["fcha_egre"][1].$_POST["fcha_egre"][2].$_POST["fcha_egre"][3]." AND MONTH(fcha_egre)=".$_POST["fcha_egre"][5].$_POST["fcha_egre"][6];
   	$res = mysql_fetch_array(mysql_query($sql));
   	$nmro_egre = $res['nmro_egre'] + 1;
    $datos[1] = crear_datos ("nmro_egre","Numero de comprobante de Pago",$nmro_egre,"0","4","numericos");
    
	
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	
	return;
}
if ($_POST["confirmar"]=="Eliminar" && !$_POST['asiento']) 
{
	eliminar_func($_POST["codg_egre"],"codg_egre",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<script>
	function asientos(accion,egreso,codigo){
		if (accion == 'imprimir' ){
			window.open('../reportes/reporte_asientos.php?asiento2=' + codigo +'&status=ver','_blank','scrollbars=yes,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500');
		}
		if (accion == 'generar'){
			var parametros = {
	   			"codg_egre" : egreso,
	   			"status": accion
	   		};
			$.ajax({
				data:  parametros,
			    url:   '../reportes/comprobante_asientos.php',
			    type:  'get',
			    beforeSend: function () {
		        	$("#asientos").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
		    	},
		    	success:  function (response) {
		    		$("#asientos").html(response);
		    		
		    	}
			});
		}
		if (accion == 'eliminar'){
			var parametros = {
	   			"codg_egre" : egreso,
	   			"status": accion,
	   			"asiento": codigo
	   		};
			$.ajax({
				data:  parametros,
			    url:   '../reportes/comprobante_asientos.php',
			    type:  'get',
			    beforeSend: function () {
		        	$("#asientos").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
		    	},
		    	success:  function (response) {
		    		$("#asientos").html(response);
		    	}
			});
		}
	}
</script>

<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">EGRESOS (Comprobante de Pago)</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Fecha:</td><?php escribir_campo('codg_egre',$_POST["codg_egre"],$codg_egre,'',12,15,'Codigo del Egreso',$boton,$existe,'','','oculto')?>
                        <td width="75%"><?php escribir_campo('fcha_egre',$_POST["fcha_egre"],$fcha_egre,'',10,10,'Fecha de Elaboraci�n',$boton,$existe,'fecha','','')?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">N&uacute;mero de Comprobante:</td>
                        <td width="75%">
			                <input name="nmro_egre" type="hidden" id="nmro_egre" value="<?php if(! $existe) { echo $_POST['nmro_egre']; } else { echo $nmro_egre; } ?>" size="35" title="Num Orden"><?php if ($boton != "Verificar" && $boton != "Guardar") { echo substr($fcha_egre, 5, 2).substr($fcha_egre, 2, 2).'-'.$nmro_egre; } else echo 'Por Asignar...'; ?>                        
			            </td>
                      </tr>
                      <tr>
                      	<td class="etiquetas">Motivo del Egreso:</td>
                        <td>
                            <?php 
                             $func_cambiar_motivo = "valor_acampo('','codg_rela');valor_acampo('','nomb_rela');valor_acampo('NULL','codg_mpio');valor_acampo('NULL','cod_pro');valor_acampo('NULL','codg_prst');valor_acampo('NULL','codg_reti'); submit()";
							 if ($boton != "Modificar" && $boton != "Guardar" && $boton!="Actualizar") { echo '<select name="moti_egre" title="Motivo por el cual se efect�a el egreso" OnChange="'.$func_cambiar_motivo.'">
                          <option value="">Seleccione...</option>
                          <option value="MONTEPIO" '; if ($moti_egre == "MONTEPIO" || $_POST['moti_egre'] =="MONTEPIO") { echo 'selected'; } echo '>Pago de MONTEPIO</option>
                          <option value="Proveedor" '; if ($moti_egre == "Proveedor" || $_POST['moti_egre'] =="Proveedor") { echo 'selected'; } echo '>Pago a Proveedor</option>
                          <option value="Pr�stamo" '; if ($moti_egre == "Pr�stamo" || $_POST['moti_egre'] =="Pr�stamo") { echo 'selected'; } echo '>Entrega de Pr�stamo</option>
                          <option value="Retiro" '; if ($moti_egre == "Retiro" || $_POST['moti_egre'] =="Retiro") { echo 'selected'; } echo '>Retiro Parcial/Total</option>
                          <option value="Dividendos" '; if ($moti_egre == "Dividendos" || $_POST['moti_egre'] =="Dividendos") { echo 'selected'; } echo '>Pago de Dividendos</option>
                          <option value="Caja Chica" '; if ($moti_egre == "Caja Chica" || $_POST['moti_egre'] =="Caja Chica") { echo 'selected'; } echo '>Caja Chica</option>
                          <option value="Inversiones" '; if ($moti_egre == "Inversiones" || $_POST['moti_egre'] =="Inversiones") { echo 'selected'; } echo '>Inversiones</option>
                          <option value="Reimpresi�n" '; if ($moti_egre == "Reimpresi�n" || $_POST['moti_egre'] =="Reimpresi�n") { echo 'selected'; } echo '>Reimpresi�n de Cheque</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="moti_egre" id="moti_egre" value="'.$moti_egre.'" >'; 
						    if ($moti_egre == "MONTEPIO") { echo 'Pago de MONTEPIO'; } 
							if ($moti_egre == "Proveedor") { echo 'Pago a Proveedor'; }
						    if ($moti_egre == "Pr�stamo") { echo 'Entrega de Pr�stamo'; } 
							if ($moti_egre == "Retiro") { echo 'Retiro Parcial/Total/Por Muerte'; }
							if ($moti_egre == "Dividendos") { echo 'Pagos de Dividendos'; }
							if ($moti_egre == "Caja Chica") { echo 'Caja Chica'; }
						}?></td>
                      </tr>
                      <?php
                        ////// Si motivo del egreso es un MONTEPIO se muestran los montepio pendientes por pagar 
                        if ($_POST['moti_egre']=='MONTEPIO') {
                            $sql_mpio = "SELECT *, (SELECT nomb_cnta FROM cuentas WHERE codg_pcnta = m.codg_pcnta) as cuenta_pagar, if (m.mont_mpio, m.mont_mpio, if ((SELECT SUM(haber_movi) from movimientos_contables WHERE codg_pcnta=m.codg_pcnta)>0,(SELECT SUM(haber_movi) from movimientos_contables WHERE codg_pcnta=m.codg_pcnta),0)) as apagar, if ((SELECT SUM(mnto_debe) FROM egresos e, egresos_conceptos ec WHERE e.moti_egre='MONTEPIO' AND e.codg_mpio = m.codg_mpio AND e.codg_egre=ec.codg_egre)>0,(SELECT SUM(mnto_debe) FROM egresos e, egresos_conceptos ec WHERE e.moti_egre='MONTEPIO' AND e.codg_mpio = m.codg_mpio AND e.codg_egre=ec.codg_egre),0) as pagado FROM montepio m, socios where m.cedu_soci=socios.cedu_soci ";
                            $sql_mpio .= "AND ((m.stat_mpio != 'P')";
                            if ($_POST['codg_mpio']!='NULL'){
                                $sql_mpio .= " OR (m.codg_mpio IN (SELECT codg_mpio FROM egresos WHERE codg_mpio = ".$_POST['codg_mpio']."))";
                            }
                            $sql_mpio .= ")";
							$sql_mpio .= " GROUP BY m.codg_mpio HAVING apagar > pagado";
                            $res_mpio = mysql_query($sql_mpio);
                            echo '<tr><td>&nbsp;</td><td class="etiquetas">MONTEPIO PENDIENTE POR PAGAR:</td></tr>';
                            echo '<tr><td>&nbsp;</td><td><table cellpadding="0" cellspacing="0" border="1" width="100%">';
                            echo '<tr class="etiquetas" align="center"><td>Sel.</td><td>Apellidos y Nombres</td><td>Fallecimiento de</td></tr>';
                            while ($reg_mpio=mysql_fetch_array($res_mpio)){
                                //// si Fallece el socio se buscan los beneficiarios que tengan porcentaje mayor a 0
                                if($reg_mpio["tipo_mpio"]=='S'){
                                    $sql_benef = "SELECT * FROM socios s, socios_beneficiarios sf WHERE s.cedu_soci=".$reg_mpio[cedu_soci]." AND s.cedu_soci=sf.cedu_soci AND sf.porc_benf>0 " ;
                                    if (($_POST['confirmar']=="" && $_POST['moti_egre']=="MONTEPIO") || ($_POST['confirmar']=="Verificar")){
                                        //$sql_benef .= " AND ((Select SUM(mnto_mpio) from montepio_socios WHERE codg_mpio=".$reg_mpio['codg_mpio']." GROUP BY codg_mpio)*sf.porc_benf/100) > (SELECT SUM(mnto_debe) FROM egresos e, egresos_conceptos ec WHERE e.moti_egre='MONTEPIO' AND e.codg_mpio=".$reg_mpio['codg_mpio']." AND e.codg_rela=sf.cedu_benf AND e.nomb_rela=CONCAT(sf.apel_benf,' ',sf.nomb_benf) AND e.codg_egre=ec.codg_egre GROUP BY e.codg_mpio,codg_rela)";
                                        //$sql_benef .= " OR ((Select SUM(mnto_mpio) from montepio_socios WHERE codg_mpio=".$reg_mpio['codg_mpio']." GROUP BY codg_mpio)*sf.porc_benf/100) > (SELECT SUM(mnto_debe) FROM egresos e, egresos_conceptos ec WHERE e.moti_egre='MONTEPIO' AND e.codg_mpio=".$reg_mpio['codg_mpio']." AND e.codg_rela=sf.cedu_benf AND e.nomb_rela=CONCAT(sf.apel_benf,' ',sf.nomb_benf) AND e.codg_egre=ec.codg_egre GROUP BY e.codg_mpio,codg_rela) IS NULL";
                                    } 
                                    $res_benef = mysql_query($sql_benef);
                                    while ($reg_benef=mysql_fetch_array($res_benef)){
                                        $func_cambiar = "valor_acampo('".$reg_benef[cedu_benf]."','codg_rela');valor_acampo('".$reg_benef[apel_benf]." ".$reg_benef[nomb_benf]."','nomb_rela')";
                                        $func_cambiar .= ";valor_acampo('".$reg_mpio['codg_mpio']."','codg_mpio');";     
                                        $func_cambiar .= "valor_acampo('NULL','cod_pro');";     
                                        $func_cambiar .= "valor_acampo('NULL','codg_prst');";     
                                        $func_cambiar .= "valor_acampo('NULL','codg_reti');";
                                        echo '<tr>
                                        <td align="center"><input type="radio" name="benficiario" '; if ($_POST["codg_mpio"]==$reg_mpio["codg_mpio"]  && $_POST["codg_rela"]==$reg_benef['cedu_benf']){ echo 'checked'; } echo ' value="'.$reg_mpio["codg_mpio"].'" OnClick="'.$func_cambiar.'" title="Haga Click para seleccionar"></td>
                                        <td>&nbsp;'.$reg_benef["apel_benf"].'&nbsp;'.$reg_benef["nomb_benf"].'</td>
                                        <td align="center">'.$reg_mpio["apel_soci"].'&nbsp;'.$reg_mpio["nomb_soci"].'</td>
                                        </tr>';
                                    }
                                }
                                //// si fallece un familiar
                                if($reg_mpio["tipo_mpio"]=='F'){
                                    $func_cambiar = "valor_acampo('".$reg_mpio[cedu_soci]."','codg_rela');valor_acampo('".$reg_mpio[apel_soci]." ".$reg_mpio[nomb_soci]."','nomb_rela')";
                                        $func_cambiar .= ";valor_acampo('".$reg_mpio['codg_mpio']."','codg_mpio');";     
                                        $func_cambiar .= "valor_acampo('NULL','cod_pro');";     
                                        $func_cambiar .= "valor_acampo('NULL','codg_prst');";     
                                        $func_cambiar .= "valor_acampo('NULL','codg_reti');";
                                    echo '<tr>
                                    <td align="center"><input type="radio" name="benficiario" value="1" '; if ($_POST["codg_mpio"]==$reg_mpio["codg_mpio"]){ echo 'checked'; } echo ' OnClick="'.$func_cambiar.'" title="Haga Click para seleccionar"></td>
                                    <td>&nbsp;'.$reg_mpio["apel_soci"].'&nbsp;'.$reg_mpio["nomb_soci"].'</td>
                                    <td align="center">Un Familiar</td>
                                    </tr>';
                                }
                            }
                            echo '</table></td></tr>';
                        }
                        ////// Si motivo del egreso es un PAGO A PROVEEDOR se muestra combo para seleccionar proveedores 
                        if ($_POST['moti_egre']=='Proveedor' || $_POST['moti_egre']=='Inversiones') {
                            echo '<tr><td>&nbsp;</td><td class="etiquetas">Seleccione un Proveedor:<br>';
                            if ($_POST['cod_pro2']!=''){ $cod_pro = $_POST['cod_pro2']; } else { $cod_pro = $_POST['cod_pro']; } 
                            combo('cod_pro2', $cod_pro, 'proveedores', $link, 0, 0, 3, '', 'cod_pro', "OnChange=submit()", $boton, "ORDER BY nom_pro",'');
                            echo '</td></tr>';
                            if ($_POST['cod_pro2']!=''){
                                $sql_proveedor = "SELECT * FROM proveedores WHERE cod_pro = ".$_POST['cod_pro2'];
                                $reg_proveedor = mysql_fetch_array(mysql_query($sql_proveedor));
                                $_POST['nomb_rela']=$reg_proveedor['nom_pro'];
                                $_POST['codg_rela']=$reg_proveedor['rif_pro'];
                                $_POST['cod_pro']=$reg_proveedor['cod_pro'];
                            }
                        }
                        ////// Si motivo del egreso es Entrega de Pr�stamo se muestra prestamos pendiente por liquidar  
                        if ($_POST['moti_egre']=='Pr�stamo') {
                            $sql_pres = "SELECT * FROM prestamos, socios where prestamos.sald_inic_real='' AND prestamos.cedu_soci=socios.cedu_soci AND stat_prst='A' AND ((codg_prst NOT IN (SELECT codg_prst FROM egresos WHERE codg_prst IS NOT NULL)) ";
                            if ($_POST['codg_prst']!='NULL'){
                                $sql_pres .= "OR (codg_prst IN (SELECT codg_prst FROM egresos WHERE codg_prst = ".$_POST['codg_prst']."))";
                            } 
                            $sql_pres .= ")";
                            $res_pres = mysql_query($sql_pres);
                            echo '<tr><td>&nbsp;</td><td class="etiquetas">PR�STAMOS PENDIENTE POR LIQUIDAR:</td></tr>';
                            echo '<tr><td>&nbsp;</td><td><table cellpadding="0" cellspacing="0" border="1" width="100%">';
                            echo '<tr class="etiquetas" align="center"><td>Sel.</td><td>Apellidos y Nombres</td><td>Monto</td></tr>';
                            while ($reg_pres=mysql_fetch_array($res_pres)){
                                $func_cambiar = "valor_acampo('".$reg_pres[cedu_soci]."','codg_rela');valor_acampo('".$reg_pres[apel_soci]." ".$reg_pres[nomb_soci]."','nomb_rela')";
                                $func_cambiar .= ";valor_acampo('NULL','codg_mpio');";     
                                $func_cambiar .= "valor_acampo('NULL','cod_pro');";     
                                $func_cambiar .= "valor_acampo('".$reg_pres['codg_prst']."','codg_prst');";     
                                $func_cambiar .= "valor_acampo('NULL','codg_reti');";
                                echo '<tr>
                                <td align="center"><input type="radio" name="benficiario" '; if ($_POST["codg_prst"]==$reg_pres['codg_prst']){ echo 'checked'; } echo ' value="1" OnClick="'.$func_cambiar.'" title="Haga Click para seleccionar"></td>
                                <td>&nbsp;'.$reg_pres["apel_soci"].'&nbsp;'.$reg_pres["nomb_soci"].'</td>
                                <td align="right">'.Redondear($reg_pres["mont_apro"],2,".",",").'&nbsp;</td>
                                </tr>';
                            }
                            echo '</table></td></tr>';
                        }
                        ////// Si motivo del egreso es un Retiro se muestran los retiros pendientes por pagar 
                        if ($_POST['moti_egre']=='Retiro') {
                            $sql_retiro = "SELECT * FROM retiros rt, socios where rt.cedu_soci=socios.cedu_soci ";
                            $sql_retiro .= "AND (rt.stat_reti = 'A') ";
                            $sql_retiro .= "AND ((rt.codg_reti NOT IN (SELECT r.codg_reti FROM egresos e, retiros r WHERE e.moti_egre = 'Retiro' AND e.codg_reti=r.codg_reti AND e.codg_rela!='' AND r.tipo_reti!='M' AND r.tipo_reti!='R' AND e.anul_egre!='S'))";
                            if ($_POST['codg_reti']!='NULL'){
                                $sql_retiro .= " OR (rt.codg_reti IN (SELECT codg_reti FROM egresos WHERE codg_reti = ".$_POST['codg_reti']."))";
                            } 
                            $sql_retiro .= ")";
                            $res_retiro = mysql_query($sql_retiro);
                            echo '<tr><td>&nbsp;</td><td class="etiquetas">RETIROS PENDIENTE POR LIQUIDAR:</td></tr>';
                            echo '<tr><td>&nbsp;</td><td><table cellpadding="0" cellspacing="0" border="1" width="100%">';
                            echo '<tr class="etiquetas" align="center"><td>Sel.</td><td>Apellidos y Nombres</td><td>Tipo</td><td>Monto</td></tr>';
                            while ($reg_retiro=mysql_fetch_array($res_retiro)){
                            	/// verificar si el socio tiene un retiro por muerte
                            	$sql_ret_ant = "SELECT * FROM retiros WHERE cedu_soci = ".$reg_retiro[cedu_soci]." AND tipo_reti = 'M'";
								$res_ret_ant = mysql_fetch_array(mysql_query($sql_ret_ant));

                                //// si es por muerte del socio se buscan los beneficiarios que tengan porcentaje mayor a 0
                                if($reg_retiro["tipo_reti"]=='M' || $res_ret_ant[tipo_reti]=='M'){
                                    $sql_benef = "SELECT * FROM socios s, socios_beneficiarios sf WHERE s.cedu_soci=".$reg_retiro[cedu_soci]." AND s.cedu_soci=sf.cedu_soci AND sf.porc_benf>0 AND sf.cedu_benf NOT IN (SELECT codg_rela FROM egresos WHERE moti_egre='Retiro' AND codg_reti=".$reg_retiro[codg_reti].")";
                                    $res_benef = mysql_query($sql_benef);
                                    while ($reg_benef=mysql_fetch_array($res_benef)){
                                        $func_cambiar = "valor_acampo('".$reg_benef[cedu_benf]."','codg_rela');valor_acampo('".$reg_benef[apel_benf]." ".$reg_benef[nomb_benf]."','nomb_rela')";
                                        $func_cambiar .= ";valor_acampo('NULL','codg_mpio');";     
                                        $func_cambiar .= "valor_acampo('NULL','cod_pro');";     
                                        $func_cambiar .= "valor_acampo('NULL','codg_prst');";     
                                        $func_cambiar .= "valor_acampo('".$reg_retiro['codg_reti']."','codg_reti');";
                                        echo '<tr>
                                        <td align="center"><input type="radio" name="benficiario" '; if ($_POST["codg_reti"]==$reg_retiro["codg_reti"] && $_POST["codg_rela"]==$reg_benef['cedu_benf']){ echo 'checked'; } echo ' value="'.$reg_reti["codg_reti"].'" OnClick="'.$func_cambiar.'" title="Haga Click para seleccionar"></td>
                                        <td>&nbsp;'.$reg_benef["apel_benf"].'&nbsp;'.$reg_benef["nomb_benf"].'</td>
                                        <td align="center">&nbsp;'.$reg_retiro["tipo_reti"].'&nbsp;</td>';
                                        $mretiro = redondear(($reg_retiro["apr_reti"]+$reg_retiro["ret_reti"]+$reg_retiro["rei_reti"]),2,"",".");
										$mretiro = redondear(($mretiro*($reg_benef["porc_benf"]/100)),2,"",".");
                                        echo '<td align="right">&nbsp;'.redondear($mretiro,2,".",".").'&nbsp;</td>
                                        </tr>';
                                    }
                                }
                                //// si el retiro es Parcial o Total
                                if($reg_retiro["tipo_reti"]=='P' || $reg_retiro["tipo_reti"]=='T' || ($reg_retiro["tipo_reti"]=='R' AND $res_ret_ant[tipo_reti]!='M')){
                                    $func_cambiar = "valor_acampo('".$reg_retiro[cedu_soci]."','codg_rela');valor_acampo('".$reg_retiro[apel_soci]." ".$reg_retiro[nomb_soci]."','nomb_rela')";
                                        $func_cambiar .= ";valor_acampo('NULL','codg_mpio');";     
                                        $func_cambiar .= "valor_acampo('NULL','cod_pro');";     
                                        $func_cambiar .= "valor_acampo('NULL','codg_prst');";     
                                        $func_cambiar .= "valor_acampo('".$reg_retiro['codg_reti']."','codg_reti');";
                                    echo '<tr>
                                    <td align="center"><input type="radio" name="benficiario" value="1" '; if ($_POST["codg_reti"]==$reg_retiro["codg_reti"]){ echo 'checked'; } echo ' OnClick="'.$func_cambiar.'" title="Haga Click para seleccionar"></td>
                                    <td>&nbsp;'.$reg_retiro["apel_soci"].'&nbsp;'.$reg_retiro["nomb_soci"].'</td>
                                    <td align="center">&nbsp;'.$reg_retiro["tipo_reti"].'&nbsp;</td>';
                                    echo $mretiro = redondear(($reg_retiro["apr_reti"]+$reg_retiro["ret_reti"]+$reg_retiro["rei_reti"]),2,"",".");
                                    echo '<td align="right">&nbsp;'.redondear($mretiro,2,".",".").'&nbsp;</td>
                                    </tr>';
                                }
                            }
                            echo '</table></td></tr>';
                        }
                        ////// Si motivo del egreso es un PAGO A PROVEEDOR se muestra combo para seleccionar proveedores 
                        if ($_POST['moti_egre']=='Dividendos' || $_POST['moti_egre']=='Caja Chica') {
                            echo '<tr><td>&nbsp;</td><td class="etiquetas">Seleccione un Socio:<br>';
                            if ($_POST['cedu_soci']!=''){ $cedu_soci = $_POST['cedu_soci']; } else { $cedu_soci = $_POST['cedu_soci']; } 
                            if ($_POST['moti_egre']=='Caja Chica'){
                              $sql_junta = "SELECT MAX(codg_junt) FROM emp_junta_direct";
                              $junta = mysql_fetch_array(mysql_query($sql_junta));
                              combo('cedu_soci', $cedu_soci, 'vista_miembros_junta_direct', $link, 2, 2, 5, '', 'cod_pro', "OnChange=submit()", $boton, "WHERE codg_junt=".$junta[0]." ORDER BY nomb_soci",'');
                            }
                            else{
                              combo('cedu_soci', $cedu_soci, 'vista_socios_activos', $link, 0, 0, 1, '', 'cod_pro', "OnChange=submit()", $boton, "ORDER BY nomb_soci",'');                            
                            }
                            echo '</td></tr>';
                            if ($_POST['cedu_soci']!=''){
                                $sql_proveedor = "SELECT * FROM vista_socios_activos WHERE cedu_soci = ".$_POST['cedu_soci'];
                                $reg_proveedor = mysql_fetch_array(mysql_query($sql_proveedor));
                                $_POST['nomb_rela']=$reg_proveedor['nomb_soci'];
                                $_POST['codg_rela']=$reg_proveedor['cedu_soci'];
                            }
                        }
                      ?>
                      <tr>
                        <td width="25%" class="etiquetas">Nombre o Raz�n Social:</td>
                        <td width="75%">
				    <?php if ($moti_egre == "Reimpresi�n" || $_POST['moti_egre'] =="Reimpresi�n") { $add_text = ''; } else { $add_text = ' Readonly '; } ?>
                            <?php escribir_campo('codg_mpio',$_POST["codg_mpio"],$codg_mpio,'',11,50,'C�digo del Montepio',$boton,$existe,'',' readonly ','oculto')?>
                            <?php escribir_campo('cod_pro',$_POST["cod_pro"],$cod_pro,'',11,50,'C�digo del Proveedor',$boton,$existe,'',' readonly ','oculto')?>
                            <?php escribir_campo('codg_prst',$_POST["codg_prst"],$codg_prst,'',11,50,'C�digo del Prestamo',$boton,$existe,'',' readonly ','oculto')?>
                            <?php escribir_campo('codg_reti',$_POST["codg_reti"],$codg_reti,'',11,50,'C�digo del Retiro',$boton,$existe,'',' readonly ','oculto')?>
                            <?php escribir_campo('nomb_rela',$_POST["nomb_rela"],$nomb_rela,'',100,50,'Nombre o Raz�n Social',$boton,$existe,'',$add_text,'')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">RIF/C�dula:</td>
                        <td width="75%">
					<?php 
					escribir_campo('codg_rela',$_POST["codg_rela"],$codg_rela,'',11,50,'N�mero de RIF o C�dula de la Raz�n Social',$boton,$existe,'',$add_text ,'')?>
				</td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Banco:</td>
			            <td>
                        <?php combo('codg_cnta', $codg_cnta, 'banco_cuentas', $link, 0, 0, 1, '', 'codg_cnta', "", $boton, "ORDER BY bnco_cnta",''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N�&nbsp;de&nbsp;Cheque/Transferencia:</td>
                        <td width="75%"><?php escribir_campo('nmro_cheq',$_POST["nmro_cheq"],$nmro_cheq,'',11,10,'N�mero de cheque/Transferencia',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto deducciones:</td>
                        <td width="75%"><?php escribir_campo('dedu_egre',$_POST["dedu_egre"],$dedu_egre,'',10,12,'Si existe indique las deduciones a aplicar a este pago',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Elaborado:</td>
                        <td width="75%"><?php escribir_campo('elab_egre',$_POST["elab_egre"],$elab_egre,'',30,50,'Comprobante de pago elaborado por',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Revisado:</td>
                        <td width="75%"><?php escribir_campo('revi_egre',$_POST["revi_egre"],$revi_egre,'',30,50,'Comprobante de pago revisado por',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Autorizado:</td>
                        <td width="75%"><?php escribir_campo('autr_egre',$_POST["autr_egre"],$autr_egre,'',30,50,'Comprobante de pago autorizado por',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Contabilizado:</td>
                        <td width="75%"><?php escribir_campo('cntb_egre',$_POST["cntb_egre"],$cntb_egre,'',30,50,'N�mero del Acta de Defunci�n',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td width="75%"><?php echo escribir_area('obsr_egre',$_POST["obsr_egre"],$obsr_egre,'',30,3,'Observaciones adicionales',$boton,$existe,'')?></td>
                      </tr>
                      <?php if ($codg_egre && $boton=="Modificar"  && ($moti_egre=="MONTEPIO" || $moti_egre=="Retiro" || $moti_egre=="Pr�stamo")) { ?>
                      <tr>
                        <td width="25%" class="etiquetas"> Monto&nbsp;Pendiente&nbsp;por&nbsp;Pagar:</td>
                        <td width="75%"> 
                        	<input type="hidden" name="monto_pagar" id="monto_pagar" value="<?php echo $monto_pagar; ?>">
                        	<b>
                        		<?php 
                        			if($moti_egre=="Retiro"){
										$sql_re = "SELECT * FROM retiros WHERE codg_reti=".$codg_reti;
                        				$res_re = mysql_fetch_array(mysql_query($sql_re));

		                            	/// verificar si el socio tiene un retiro por muerte
		                            	$sql_ret_ant = "SELECT * FROM retiros WHERE cedu_soci = ".$res_re[cedu_soci]." AND tipo_reti = 'M'";
										$res_ret_ant = mysql_fetch_array(mysql_query($sql_ret_ant));

		                                if($res_re["tipo_reti"]=='M' || ($res_re["tipo_reti"]=='R' AND $res_ret_ant[tipo_reti]=='M')){
		                                    $sql_benef = "SELECT * FROM socios s, socios_beneficiarios sf WHERE s.cedu_soci=".$res_re[cedu_soci]." AND s.cedu_soci=sf.cedu_soci AND sf.cedu_benf =".$codg_rela;
		                                    $res_benef = mysql_query($sql_benef);
		                                    while ($reg_benef=mysql_fetch_array($res_benef)){
		                                        $monto_pagar = redondear(($res_re["apr_reti"]+$res_re["ret_reti"]+$res_re["rei_reti"]),2,"",".");
												$monto_pagar = redondear(($monto_pagar*($reg_benef["porc_benf"]/100)),2,"",".");
		                                    }
		                                }
		                                else{
		                                	$monto_pagar = redondear(($res_re["apr_reti"]+$res_re["ret_reti"]+$res_re["rei_reti"]),2,"",".");
		                                }
                        			}
                        			$sql_pagado = "SELECT SUM(mnto_debe) as monto FROM egresos e, egresos_conceptos ec WHERE e.moti_egre='Retiro' AND e.codg_reti=".$codg_reti." AND e.codg_rela='".$codg_rela."' AND e.nomb_rela='".$nomb_rela."' AND e.codg_egre=ec.codg_egre GROUP BY e.codg_mpio,codg_rela";
							        $res_pagado = mysql_fetch_array(mysql_query($sql_pagado));
							        $monto_pagar = redondear(($monto_pagar - $res_pagado[monto]),2,'','.');
							        echo '<script>$(\'#monto_pagar\').val('.$monto_pagar.');</script>';
                        		echo '<font color="red">'.redondear($monto_pagar,2,".",",").'</font>'; 
                        				echo '<br>Usar Cuenta: ';
                        				$sql_cuenta_mostrar = "SELECT nomb_cnta, nmro_cnta FROM montepio m, cuentas c WHERE m.codg_mpio = ".$codg_mpio." AND m.codg_pcnta=c.codg_pcnta";
                        				$res_cuenta_mostrar = mysql_fetch_array(mysql_query($sql_cuenta_mostrar));
                        				echo $res_cuenta_mostrar[nmro_cnta].' - '.$res_cuenta_mostrar[nomb_cnta];
                        		?>
                        	</b>
                        </td>
                      </tr>
                      <?php } ?>
                      <?php if ($boton!="Verificar") { 
                            if (($boton=="Modificar" && $anul_egre=='S')||($boton=="Actualizar")) {?>
                      <tr>
                        <td width="25%" class="etiquetas">Anular Comprobante:</td>
                        <td width="75%"><input name="anul_egre" id="anul_egre" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$anul_egre.'" '; } else { echo 'type="checkbox" value="S"'; } if($_POST["anul_egre"]=='S' || $anul_egre == 'S') { echo 'checked'; } ?> title="Marque para Anular"><?php if ($boton=='Modificar') { if ($anul_egre=='S') {echo ' <center><FONT size="16px" color="#FF0000"><b>ANULADO</b></font></center>'; } } ?> </td>
                      </tr>
                      <?php } } ?>
                      <?php if ($codg_egre && $boton=="Modificar") { ?>
                      <tr>
                        <td colspan="2" style="border:1px solid #000000;">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr class="etiquetas" align="center" bgcolor="#EEEEEE"><td>C�DIGO</td><td>CONCEPTO</td><td>DEBE</td><td>HABER</td><td><input type="hidden" name="accion" id="accion" value=""><input type="hidden" name="valor" id="valor" value="">&nbsp</td></tr>
                                <?php   
                                        $total_debe=0;$total_haber=0;
                                        $sql_conc = "SELECT * FROM egresos_conceptos WHERE codg_egre = ".$codg_egre." ORDER BY mnto_hber DESC"; 
                                        $res_conc = mysql_query($sql_conc);
                                        while ($reg_conc=mysql_fetch_array($res_conc)){
                                            $total_debe=redondear(($total_debe+$reg_conc['mnto_debe']),2,"",".");
                                            $total_haber=redondear(($total_haber+$reg_conc['mnto_hber']),2,"",".");
                                            echo '<tr class="etiquetas" align="center"><td>'.$reg_conc["codg_ctab"].'</td><td>'.$reg_conc["conc_egre"].'</td><td align="right">'.redondear($reg_conc["mnto_debe"],2,".",",").'</td><td align="right">'.redondear($reg_conc["mnto_hber"],2,".",",").'</td><td>';
                                            if ($anul_egre!='S' && $prm[1]=='A'){
                                                if (($reg_conc["mnto_debe"]>0) || $reg_conc["mnto_hber"]>0){
                                                  echo '<input type="image" src="../imagenes/cancelar.png" id="eliminar_conc" name="eliminar_conc" value="mas_btn" width="20" height="20" title="Presione click para Eliminar Concepto" OnClick=valor_acampo("eliminar_concepto","accion");valor_acampo("'.$reg_conc["codg_egco"].'","valor");>';
                                                }                                               
                                            }
                                            echo'</td></tr>';
                                        }
                                ?><tr class="etiquetas" align="center" bgcolor="#EEEEEE"><td colspan="2" align="right">TOTALES:&nbsp</td><td align="right"><input type="hidden" name="total_debe" id="total_debe" value="<?php echo $total_debe; ?>"><?php echo redondear($total_debe,2,".",","); ?></td><td align="right"><input type="hidden" name="total_haber" id="total_haber" value="<?php echo $total_haber; ?>"><?php echo redondear($total_haber,2,".",","); ?></td><td>&nbsp</td></tr>
                                <?php if ($anul_egre!='S' && $prm[1]=='A'){ ?>
                                <tr>
                                    <td colspan="5">
                                      <? 
                                        $boton_ant = $boton; 
                                        $boton="Verificar";
                                        combo('ayuda_cuenta', $codg_pcnta, 'cuentas c, plan_cuentas pc', $link, 0, 1, 2, '', 'codg_pcnta', "OnChange=submit()", $boton, "WHERE pc.stat_pcuen='Activo' AND pc.codg_pcuen=c.codg_pcuen AND c.movi_cnta='S' ORDER BY c.nmro_cnta ",''); 
                                        $boton = $boton_ant;
                                        /// verificar si es una cuenta por pagar
                                        $sql_porpagar = "SELECT * FROM valores WHERE des_val='CNT_PORPAGAR'";
										$cuenta = mysql_query($sql_porpagar);
										$cuenta = mysql_fetch_array($cuenta);
                                        if ($_POST[ayuda_cuenta]==$cuenta[val_val]){
                                        	include('nomina_porpagar.php');
                                        }
                                      ?>
                                    </td>
                                </tr>
                                <tr class="etiquetas">
                                    <td><input title="Numero de cuenta Contable" type="text" name="codg_ctab_in" id="codg_ctab_in" readonly="true" value="<?php echo $codg_ctab_in; ?>" maxlength="100" size="10"/></td>                                   
                                    <td><input title="Concepto del Egreso" type="text" name="conc_egre_in" id="conc_egre_in" readonly="true" value="<?php echo $conc_egre_in; ?>" maxlength="255" size="30"/></td>                                    
                                    <td><input title="Monto por el DEBE" type="text" name="mnto_debe_in" id="mnto_debe_in" value="<?php echo $mnto_debe_in; ?>" maxlength="13" size="10"/></td>                                  
                                    <td><input title="Monto por el HABER" type="text" name="mnto_hber_in" id="mnto_hber_in" value="<?php echo $mnto_hber_in; ?>" maxlength="13" size="10"/></td>
                                    <td><input type="image" src="../imagenes/mas.png" id="agregar_conc" name="agregar_conc" value="mas_btn" width="20" height="20" title="Presione click para Registrar Concepto" OnClick="valor_acampo('insertar_concepto','accion');"></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </td>
                      </tr>
                      <?php } ?>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
          <?php if ($boton == 'Modificar' && $total_debe==$total_haber && $total_debe!=0) { ?>
		      <tr>
		        <td height="45" colspan="2" align="center" valign="top" class="etiquetas"><table width="100%" align="center" cellspacing="10">
                  <tr>
				  	<td align="center">
				  	    <?php if($boton=='Modificar'){ abrir_ventana('../reportes/comprobantepago.php','v_pago','Imprimir Comprobante de Pago',"codg_egre=".$codg_egre."&seccion=".$_GET['seccion']); } ?>
				    </td>               
				  	<td align="center">
				  	    <?php if($boton=='Modificar'){ abrir_ventana('../reportes/comprobantepago.php','v_pago','Imprimir Cheque',"codg_egre=".$codg_egre."&cheque=1&seccion=".$_GET['seccion']); } ?>
				    </td>               
				  	<td align="center">
				  	    <?php
				  	        if ($codg_mpio!='NULL'){
				  	            $opciones = "&codg_mpio=".$codg_mpio;
				  	        } 
				  	        if ($codg_reti!='NULL'){
				  	          $opciones = "&codg_reti=".$codg_reti;
				  	        } 
				  	        if($boton=='Modificar' && ($moti_egre=="MONTEPIO")){ abrir_ventana('../reportes/recibo_pago.php','v_pago','Imprimir Recibo',"codg_egre=".$codg_egre.$opciones."&seccion=".$_GET['seccion']); } 
   				  	        if($boton=='Modificar' && ($moti_egre=="Retiro")){ abrir_ventana('../reportes/recibo_pago_retiro.php','v_pago','Imprimir Recibo',"codg_egre=".$codg_egre.$opciones."&seccion=".$_GET['seccion']); } ?>
				    </td>               
				    <td id="asientos">
				    	<?php 
				    		if ($_POST[codg_egre]){ $codg_egre = $_POST[codg_egre]; }
				    		$sql_asientos = "SELECT * FROM asientos a, asientos_numero an WHERE a.orgn_asien = 'Comprobante Pago' AND a.codg_rela = ".$codg_egre." AND a.codg_asien = an.codg_asien";
				    		$bus_asientos = mysql_query($sql_asientos);
				    		if ($res_asientos = mysql_fetch_array($bus_asientos)){				    			
								echo '<input type="hidden" id="asiento" name="asiento" value="'.$res_asientos[codg_asien].'">';
			                  	$parametros = "'eliminar','".$codg_egre."','".$res_asientos[codg_asien]."'";
			                  	echo '<input type="button" name="v_reporte" id="v_reporte" value="Eliminar Asiento ('.$res_asientos[numero].')" onclick="asientos('.$parametros.')">';
			                  	$parametros = "'imprimir','".$codg_egre."','".$res_asientos[codg_asien]."'";
			                  	echo '<input type="button" name="v_reporte" id="v_reporte" value="Imprimir Asiento ('.$res_asientos[numero].')" onclick="asientos('.$parametros.')">';
				    		}
				    		else{
								$parametros = "'generar','".$codg_egre."','0'";
			                  	echo '<input type="button" name="v_reporte" id="v_reporte" value="Generar Asiento" onclick="asientos('.$parametros.')">';
				    		}
				    	?>
				    </td>
                  </tr>
                </table>		          
				</td>
		        </tr>
                    </table></td>
                  </tr>
		  <?php } ?> 
					
                  <tr>
				  	<td>
					
					</td>
				  </tr>
				  <tr>
                    <td>
					<?php 
						$ncriterios =4;
						$criterios[0] = "Comprobante"; 
						$campos[0] ="nmro_egre";
						$criterios[1] = "RIF/C�dula"; 
						$campos[1] ="codg_rela";
						$criterios[2] = "Motivo"; 
						$campos[2] ="moti_egre";
						$criterios[3] = "Nombre / Raz�n Social"; 
						$campos[3] ="nomb_rela";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?>
					  <?php 
					        $func_cambiar_busq = "criterio2.checked=true; valor_acampo(this.value,'buscar_a');";
                                                echo '<center><select name="moti_egre_bus" title="Motivo por el cual se efect�a el egreso" OnChange="'.$func_cambiar_busq.'">
                          <option value="">Seleccione...</option>
                          <option value="MONTEPIO" '; if ($_POST['moti_egre_bus'] =="MONTEPIO") { echo 'selected'; } echo '>Pago de MONTEPIO</option>
                          <option value="Proveedor" '; if ($_POST['moti_egre_bus'] =="Proveedor") { echo 'selected'; } echo '>Pago a Proveedor</option>
                          <option value="Pr�stamo" '; if ($_POST['moti_egre_bus'] =="Pr�stamo") { echo 'selected'; } echo '>Entrega de Pr�stamo</option>
                          <option value="Retiro" '; if ($_POST['moti_egre_bus'] =="Retiro") { echo 'selected'; } echo '>Retiro Parcial/Total</option>
                          <option value="Dividendos" '; if ($_POST['moti_egre_bus'] =="Dividendos") { echo 'selected'; } echo '>Pago de Dividendos</option>
                          <option value="Caja Chica" '; if ($_POST['moti_egre_bus'] =="Caja Chica") { echo 'selected'; } echo '>Caja Chica</option>
                          <option value="Inversiones" '; if ($_POST['moti_egre_bus'] =="Inversiones") { echo 'selected'; } echo '>Inversiones</option>
                          </select></center>'; ?>
                          </td>
					  
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
