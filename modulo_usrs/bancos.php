<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'bancos.php?seccion='.$_GET["seccion"];
$tabla = "banco_cuentas";	// nombre de la tabla
$ncampos = "6";		//numero de campos del formulario
$datos[0] = crear_datos ("bnco_cnta","Banco",$_POST['bnco_cnta'],"1","50","alfabeticos");
$datos[1] = crear_datos ("nmro_cnta","Num Cuenta",$_POST['nmro_cnta'],"20","20","numericos");
$datos[2] = crear_datos ("tipo_cnta","Tipo de Cuenta",$_POST['tipo_cnta'],"1","50","alfabeticos");
$datos[3] = crear_datos ("mnto_cnta","Monto en Cuenta",$_POST['mnto_cnta'],"1","50","decimal");
$datos[4] = crear_datos ("obsr_cnta","Observaciones sobre la Cuenta",$_POST['obsr_cnta'],"0","255","alfanumericos");
$datos[5] = crear_datos ("codg_pcnta","Cuentas",$_POST['codg_pcnta'],"1","30","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Banco";
		$datos[0]="bnco_cnta";
		$parametro[1]="N� de Cuenta";
		$datos[1]="nmro_cnta";
		$parametro[2]="Monto";
		$datos[2]="mnto_cnta";
		busqueda_varios(7,$buscando,$datos,$parametro,"codg_cnta");
		return;	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_cnta = $row["codg_cnta"];
	    $bnco_cnta = $row["bnco_cnta"];
	    $nmro_cnta = $row["nmro_cnta"];
	    $tipo_cnta = $row["tipo_cnta"];
	    $mnto_cnta = $row["mnto_cnta"];
	    $obsr_cnta = $row["obsr_cnta"];
	    $codg_pcnta = $row["codg_pcnta"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{	$boton = "Actualizar";
	$boton = comp_exist($datos[1][0],$datos[1][2]."' AND codg_cnta <> '".$_POST['codg_cnta'],$tabla,$boton,'si',"Bancos.");
	if ($boton == "Actualizar")
	{ $validacion = validando_campos ($ncampos,$datos); }
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_cnta",$_POST["codg_cnta"],$pagina,"");
		auditoria_func ('modificar','',$_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[1][0],$datos[1][2],$tabla,$boton,'si',"Bancos");
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_cnta"],"codg_cnta",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="actualizar padre") 
{
        $codg_cnta = $_POST["codg_cnta"];
        $bnco_cnta = $_POST["bnco_cnta"];
	$nmro_cnta = $_POST["nmro_cnta"];
	$tipo_cnta = $_POST["tipo_cnta"];
	$mnto_cnta = $_POST["mnto_cnta"];
	$obsr_cnta = $_POST["obsr_cnta"];
	$codg_pcnta = $_POST["codg_pcnta"];
	$boton = "Modificar";
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de Bancos</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">Banco:</td>
                        <td width="75%">
									<input name="codg_cnta" type="hidden" id="codg_cnta" value="<?php if(! $existe) { echo $_POST["codg_cnta"]; } else { echo $codg_cnta; } ?>" size="35" title="Codigo de Banco">
                       		<?php escribir_campo('bnco_cnta',$_POST["bnco_cnta"],$bnco_cnta,'',50,20,'Nombre de la entidad Bancaria',$boton,$existe,'','',''); ?>
                        </td>
                      </tr>
		      <tr>
                        <td class="etiquetas">N�m. de Cuenta:</td>
                        <td width="75%">
                        	<?php escribir_campo('nmro_cnta',$_POST["nmro_cnta"],$nmro_cnta,'',20,20,'N�mero de cuenta bancaria',$boton,$existe,'','',''); ?>
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Tipo de Cuenta:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="tipo_cnta" title="Tipo de cuenta bancaria">
                          <option>Seleccione...</option>
                          <option value="A" '; if ($tipo_cnta == "A" || $_POST['tipo_cnta'] =="A") { echo 'selected'; } echo '>Ahorro</option>
                          <option value="C" '; if ($tipo_cnta == "C" || $_POST['tipo_cnta'] =="C") { echo 'selected'; } echo '>Corriente</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tipo_cnta" id="tipo_cnta" value="'.$tipo_cnta.'" >'; 
						    if ($tipo_cnta == "A") { echo 'Ahorros'; } 
							if ($tipo_cnta == "C") { echo 'Corriente'; }
						}?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Monto en Cuenta:</td>
                        <td width="75%">
                        	<?php escribir_campo('mnto_cnta',$_POST["mnto_cnta"],$mnto_cnta,'',50,20,'Monto Disponible en la cuenta',$boton,$existe,'','',''); ?>
                        </td>
                      </tr>
                       <tr>
                        <td width="25%" class="etiquetas">Cuenta Presupuestaria:  </td>
                        <td width="75%"><?php combo('codg_pcnta', $codg_pcnta, 'cuentas c, plan_cuentas pc', $link, 0, 1, 2, '', 'codg_pcnta', " ", $boton, "WHERE pc.stat_pcuen='Activo' AND pc.codg_pcuen=c.codg_pcuen AND c.movi_cnta='S' ORDER BY c.nmro_cnta",''); ?></td>
                      </tr>   
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td width="75%"><?php echo escribir_area('obsr_cnta',$_POST["obsr_cnta"],$obsr_cnta,'',30,3,'Observaciones adicionales para la cuenta bancaria',$boton,$existe,'')?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center">
                    		<?php if($boton=='Modificar'){ abrir_ventana('bancos_movimientos.php','v_movimientos','Movimientos',"codg_cnta=".$codg_cnta."&seccion=".$_GET['seccion']); } ?>
                   	 	<?php if($boton=='Modificar'){ abrir_ventana('bancos_libro.php','v_libro','Libro de Banco',"codg_cnta=".$codg_cnta."&seccion=".$_GET['seccion']); } ?>
                    </td>
                  </tr>
                  <tr>
                    <td>
			<?php 
			$ncriterios =2;
			$criterios[0] = "Banco";
			$campos[0] = "bnco_cnta";
			$criterios[1] = "Cuenta"; 
			$campos[1] ="nmro_cnta";
			if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {					
			crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
