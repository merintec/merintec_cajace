<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'valores.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "valores";	// nombre de la tabla
$ncampos = "4";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_val","C�digo",$_POST['cod_val'],"0","11","numericos");
$datos[1] = crear_datos ("des_val","Nombre",$_POST['des_val'],"1","25","alfanumericos");
$datos[2] = crear_datos ("val_val","Monto del Valor",$_POST['val_val'],"1","12","decimal");
$datos[3] = crear_datos ("con_val","Concepto del Valor",$_POST['con_val'],"0","100","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Concepto";
		$datos[0]="con_val";
		busqueda_varios(3,$buscando,$datos,$parametro,"cod_val");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_val = $row['cod_val'];
	    $des_val = $row['des_val'];
	    $val_val = $row['val_val'];
	    $con_val = $row['con_val'];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_val",$_POST["cod_val"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[3][0],$datos[3][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_val"],"cod_val",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de Valores</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">C&oacute;digo:</td>
                        <td width="75%">
                        <?php escribir_campo('cod_val',$_POST["cod_val"],$cod_val,'readonly',11,35,'C�digo de Valor',$boton,$existe,'','','')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Nombre del Valor:</td>
			            <td>
                        <?php escribir_campo('des_val',$_POST["des_val"],$des_val,'readonly',25,35,'Nombre de la variable que se utilizara para el Valor',$boton,$existe,'','','')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Concepto:</td>
                        <td width="75%">
                        <?php escribir_campo('con_val',$_POST["con_val"],$con_val,'',100,35,'Concepto del Valor',$boton,$existe,'','','')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto del Valor:</td>
			            <td>
                        <?php escribir_campo('val_val',$_POST["val_val"],$val_val,'',12,35,'Monto del Valor',$boton,$existe,'','','')?>
                        </td>
                      </tr>
                      
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$ncriterios =2; 
						$criterios[0] = "Concepto"; 
						$campos[0] ="con_val";
						$criterios[1] = "Nombre"; 
						$campos[1] ="des_val";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
