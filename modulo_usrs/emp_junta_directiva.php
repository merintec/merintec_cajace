<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'emp_junta_directiva.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "emp_junta_direct";	// nombre de la tabla
$ncampos = "3";			//numero de campos del formulario
$datos[0] = crear_datos ("acta_junt","Nro de Acta de Nombramiento",$_POST['acta_junt'],"1","50","alfanumericos");
$datos[1] = crear_datos ("fchi_junt","Fecha de inicio de junta",$_POST['fchi_junt'],"1","10","fecha");
$datos[2] = crear_datos ("fchf_junt","Fecha de fin de junta",$_POST['fchf_junt'],"1","10","fecha");

if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Nro&nbsp;Acta";
		$datos[0]="acta_junt";
		$parametro[1]="Inicio";
		$datos[1]="fchi_junt";
		$parametro[2]="Fin";
		$datos[2]="fchf_junt";
		busqueda_varios(5,$buscando,$datos,$parametro,"codg_junt");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_junt = $row["codg_junt"];
	    $acta_junt = $row["acta_junt"];
	    $fchi_junt = $row["fchi_junt"];
		$fchf_junt = $row["fchf_junt"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_junt",$_POST["codg_junt"],$pagina,"");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_junt"],"codg_junt",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de las Juntas Directivas</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Nro&nbsp;de&nbsp;Acta:</td><?php escribir_campo('codg_junt',$_POST["codg_junt"],$codg_junt,'',12,15,'Codigo de Junta Directiva',$boton,$existe,'','','oculto')?>
                        <td width="75%"><?php escribir_campo('acta_junt',$_POST["acta_junt"],$acta_junt,'',12,15,'Numero de acta de creaci�n de Junta Directiva',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha&nbsp;de&nbsp;Inicio&nbsp;de&nbsp;Junta:</td>
                        <td width="75%"><?php escribir_campo('fchi_junt',$_POST["fchi_junt"],$fchi_junt,'',12,15,'Fecha de Inicio de Junta Directiva',$boton,$existe,'fecha','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha&nbsp;de&nbsp;Fin&nbsp;de&nbsp;Junta:</td>
                        <td width="75%"><?php escribir_campo('fchf_junt',$_POST["fchf_junt"],$fchf_junt,'',12,15,'Fecha de Fin de Junta Directiva',$boton,$existe,'fecha','','')?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
					<?php if ($boton == 'Modificar') { echo '<tr><td colspan="2" align="center"><hr></td></tr>'; } ?> 
                  <tr>
				  	<td>
					<table width="100%" align="center" cellspacing="10">
                  		<tr>
		    				<td width="<?PHP echo (100/1)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('emp_miembros.php','v_miembros','Miembros',"codg_junt=".$codg_junt."&seccion=".$_GET['seccion']); } ?></td>
                  		</tr>
                	</table>
					</td>
				  </tr>
				  <tr>
                    <td>
					<?php 
						$ncriterios =2;
						$criterios[0] = "Codigo"; 
						$campos[0] ="codg_junt";
						$criterios[1] = "Nro&nbsp;Acta"; 
						$campos[1] ="acta_junt";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
