<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<title>Administrar Miembros de las Juntas Directivas</title>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['codg_junt'];
$codg_tmie = $_POST['codg_tmie'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'emp_miembros.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$pagina2 = 'emp_miembros.php?codg_junt='.$_GET["codg_junt"].'&seccion='.$_GET["seccion"];
$tabla = " emp_miembros";	// nombre de la tabla
$ncampos = "3";		//numero de campos del formulario
$datos[0] = crear_datos ("codg_junt","Codigo de Junta",$viene_val,"1","12","numericos");
$datos[1] = crear_datos ("cedu_soci","Cedula del Miembro",$_POST['cedu_soci'],"1","12","numericos");
$datos[2] = crear_datos ("codg_tmie","Cargo en la Junta",$_POST['codg_tmie'],"1","11","numericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_miem = $row["codg_miem"];
	    $codg_junt = $row["codg_junt"];
	    $cedu_soci = $row["cedu_soci"];
		$codg_tmie = $row["codg_tmie"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_miem",$_POST["codg_miem"],$pagina2,"");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
		$boton = comp_exist($datos[1][0],$datos[1][2],"socios",$boton,'no',"Socios");
		if ($_POST['cedu_soci'])
		{
		$boton = comp_exist($datos[2][0],$datos[2][2]."' AND codg_junt = '".$datos[0][2],$tabla,$boton,'si',"Miembros de Junta Directiva");
		}
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_miem"],"codg_miem",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"codg_miem",$tabla,$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Miembros de Junta Directiva</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Cédula&nbsp;de&nbsp;Socio:</td>
						<?php escribir_campo('codg_miem',$_POST["codg_miem"],$codg_miem,'',12,15,'Codigo de Miembro',$boton,$existe,'','','oculto')?>
						<?php escribir_campo('codg_junt',$_POST["codg_junt"],$viene_val,'',12,15,'Codigo de Junta Directiva',$boton,$existe,'','','oculto')?>
                        <td width="75%"><?php escribir_campo('cedu_soci',$_POST["cedu_soci"],$cedu_soci,'',12,15,'Cedula de Socio que es parte de Junta Directiva',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Cargo&nbsp;de&nbsp;Socio:</td>
                        <td width="75%"><?php	combo('codg_tmie', $codg_tmie, 'emp_miemb_tipo', $link, 0, 0, 1, "", 'codg_tmie', "", $boton, "", ""); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_miembros.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
