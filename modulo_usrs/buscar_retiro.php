<?php echo '<meta charset="ISO-8859-1">'; ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php include('../comunes/conexion_basedatos.php');
include ('../comunes/formularios_funciones.php');
if ($_POST[codg_reti]){
	$codg_reti = $_POST[codg_reti];
	$sql = "SELECT * FROM retiros r, socios s WHERE r.codg_reti = ".$codg_reti." AND r.cedu_soci = s.cedu_soci";
	$res = mysql_fetch_array(mysql_query($sql));
	$total = Redondear(($res[apr_reti]+$res[ret_reti]),2,'','.');
	$total = Redondear(($total+$res[rei_reti]),2,'','.');
	?>
	<table width="550" border="0" cellspacing="4" cellpadding="0">
		<tr>
			<td class="etiquetas">
				Fecha de Registro:
			</td>
			<td>
				<?php echo ordenar_fecha($res[fcha_reti]); ?>
			</td>
		</tr>
		<tr>
			<td class="etiquetas">
				Ahorrista Relacionado:
			</td>
			<td>
				<?php echo $res[apel_soci].' '.$res[nomb_soci]; ?>
			</td>
		</tr>
		<tr>
			<td class="etiquetas">
				Tipo de Retiro:
			</td>
			<td>
				<?php
					if ($res[tipo_reti]=='T'){echo 'RETIRO TOTAL'; }
					if ($res[tipo_reti]=='P'){echo 'RETIRO PARCIAL'; }
					if ($res[tipo_reti]=='M'){echo 'RETIRO POR MUERTE DEL AHORRISTA'; } 
					if ($res[tipo_reti]=='R'){echo 'RETIRO DE REINTEGROS'; }
				?>
			</td>
		</tr>
		<tr>
			<td class="etiquetas">
				&nbsp;
			</td>
			<td>
				<table border="1px" cellpadding="0" cellspacing="0" class="etiquetas" style="width: 100%; text-align: center;">
					<tr class="titulo"><td colspan="2">Montos del Retiro:</td></tr>
					<tr><td>Aportes:</td><td align="right"><?php echo Redondear($res[apr_reti],2,'.',','); ?></td></tr>
					<tr><td>Retenciones:</td><td align="right"><?php echo Redondear($res[ret_reti],2,'.',','); ?></td></tr>
					<tr><td>Reintegros:</td><td align="right"><?php echo Redondear($res[rei_reti],2,'.',','); ?></td></tr>
					<tr style="font-weight: bold;"><td align="right">TOTAL:</td><td align="right"><?php echo Redondear($total,2,'.',','); ?></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="etiquetas">
				&nbsp;
			</td>
			<td>
				<table border="1px" cellpadding="0" cellspacing="0" class="etiquetas" style="width: 100%; text-align: center;">
					<tr class="titulo"><td colspan="5">Pr�stamos que Paga</td></tr>
					<tr align="center" class="etiquetas"><td>N�</td><td>Tipo Pr�stamo</td><td>Capital</td><td>Intereses</td><td>Total</td></tr>
					<?php 
						$conta_prst = 1;
						$sql_pres = "SELECT * FROM prestamos_mov pm, prestamos p, tipo_prestamos pt WHERE p.codg_prst = pm.codg_prst AND pm.orgn_prtm = 'Retiro' AND pm.rela_prtm = ".$codg_reti." AND p.codg_tipo_pres = pt.codg_tipo_pres";
						$bus_pres = mysql_query($sql_pres);
						while($res_pres = mysql_fetch_array($bus_pres)){
							echo '<tr align="right"><td>'.$conta_prst.'&nbsp;</td><td align="left">'.$res_pres[nomb_tipo_pres].'</td><td>'.redondear($res_pres[capi_prtm],2,'.',',').'</td><td>'.redondear($res_pres[inte_prtm],2,'.',',').'</td><td>'.redondear($res_pres[mnto_prtm],2,'.',',').'</td></tr>';	
							$total_pres = redondear(($total_pres + $res_pres[mnto_prtm]),2,'','.');
							$conta_prst += 1;
						}
						echo '<tr align="right"><td colspan="4">TOTAL</td><td>'.redondear($total_pres,2,'.',',').'</td></tr>';
					?>
				</table>
			</td>
		</tr>
		<tr>
			<td class="etiquetas">
				&nbsp;
			</td>
			<td>
				<table border="1px" cellpadding="0" cellspacing="0" class="etiquetas" style="width: 100%; text-align: center;">
					<tr class="titulo"><td colspan="5">Pr�stamos Respaldados Liberados</td></tr>
					<tr align="center">
		    	<td width="30px" height="20">Nro</td>
			    <td>Beneficiario</td>
			    <td width="60px">Ver</td>
			    <td width="60px">Garant�a</td>
					<?php 
						$conta_libre = 1;
						$sql_pres = "SELECT * FROM prestamos_fia pf, socios s WHERE pf.codg_reti = ". $codg_reti. " AND libre_fian != '' AND pf.cedu_soli = s.cedu_soci";
						$bus_pres = mysql_query($sql_pres);
						while($res_pres = mysql_fetch_array($bus_pres)){
							echo '<tr align="right"><td>'.$conta_prst.'&nbsp;</td><td align="left">'.$res_pres[apel_soci].' '.$res_pres[nomb_soci].'<br>C.I. '.redondear($res_pres[cedu_soci],0,'.',',').'</td><td align="center"><img src="../imagenes/edo_cnta.png" height="30px" title="Ver Estado de Cuenta" onclick=\'window.open("../reportes/socios_edocuenta.php?cedu_soci='.$res_pres[cedu_soci].'","_blank","scrollbars=no,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500");\' style="cursor: pointer; margin: 3px;"><img src="../imagenes/tabla.png" height="30px" title="Ver Tabla de Ejecuci�n" onclick=\'window.open("../reportes/tabla_amortizacion_real.php?codg_prst='.$res_pres[codg_prst].'","_blank","scrollbars=no,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500");\' style="cursor: pointer; margin: 3px;"></td><td>'.redondear($res_pres[mont_fian],2,'.',',').'</td></tr>';	
							$total_libre = redondear(($total_libre + $res_pres[mont_fian]),2,'','.');
							$conta_libre +=1;
						}
						echo '<tr align="right"><td colspan="3">TOTAL</td><td>'.redondear($total_libre,2,'.',',').'</td></tr>';
					?>
				</table>
			</td>
		</tr>
		<tr>
			<td class="etiquetas">
				&nbsp;
			</td>
			<td>
				<table border="1px" cellpadding="0" cellspacing="0" class="etiquetas" style="width: 100%; text-align: center;">
					<tr class="titulo"><td colspan="5">Pr�stamos Respaldados Retenidos</td></tr>
					<tr align="center">
				    	<td width="30px" height="20">Nro</td>
					    <td>Beneficiario</td>
					    <td width="60px">Ver</td>
					    <td width="60px">Garant�a</td>
					</tr>		   
					<?php 
						$conta_ret = 1;
						$sql_pres = "SELECT * FROM socios_movimientos sm WHERE sm.conc_movi LIKE '%Retenci�n de fianza por Retiro%' AND sm.codg_dlle =". $codg_reti;
						$bus_pres = mysql_query($sql_pres);
						while($res_pres = mysql_fetch_array($bus_pres)){
							$codg_presta = str_replace('Retenci�n de fianza por Retiro (Pr�stamo ', '', $res_pres[conc_movi]);
							$codg_presta = str_replace(')', '', $codg_presta);
							$sql_presta = "SELECT * FROM prestamos p, socios s WHERE p.cedu_soci = s.cedu_soci AND p.codg_prst = ".$codg_presta;
							$bus_presta = mysql_query($sql_presta);
							$res_presta = mysql_fetch_array($bus_presta);						
							echo '<tr align="right"><td>'.$conta_ret.'&nbsp;</td><td align="left">'.$res_presta[apel_soci].' '.$res_presta[nomb_soci].'<br>C.I. '.redondear($res_presta[cedu_soci],0,'.',',').'</td><td align="center"><img src="../imagenes/edo_cnta.png" height="30px" title="Ver Estado de Cuenta" onclick=\'window.open("../reportes/socios_edocuenta.php?cedu_soci='.$res_presta[cedu_soci].'","_blank","scrollbars=no,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500");\' style="cursor: pointer; margin: 3px;"><img src="../imagenes/tabla.png" height="30px" title="Ver Tabla de Ejecuci�n" onclick=\'window.open("../reportes/tabla_amortizacion_real.php?codg_prst='.$res_presta[codg_prst].'","_blank","scrollbars=no,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500");\' style="cursor: pointer; margin: 3px;"></td><td>'.redondear($res_pres[mont_movi],2,'.',',').'</td></tr>';	
							$total_ret = redondear(($total_ret + $res_pres[mont_movi]),2,'','.');
							$conta_ret += 1;
						}
						echo '<tr align="right"><td colspan="3">TOTAL</td><td>'.redondear($total_ret,2,'.',',').'</td></tr>';
					?>
				</table>
			</td>
		<tr>
			<td class="etiquetas">
				Acta N�
			</td>
			<td>
				<?php echo $res[numr_acta] ?>
			</td>
		</tr>
		<tr>
			<td class="etiquetas">
				Fecha de Acta:
			</td>
			<td>
				<?php echo ordenar_fecha($res[fcha_acta]); ?>
			</td>
		</tr>
			<td class="etiquetas">
				Observaciones:
			</td>
			<td>
				<?php echo $res[obsr_reti]; ?>
			</td>
		</tr>
		<tr>
			<td class="etiquetas">
				Monto del Cheque:
			</td>
			<td>
				<?php echo redondear($res[mont_reti],2,'.',','); ?>
			</td>
		</tr>
      	<tr>
        	<td colspan="2" align="center">
        		<button onclick="eliminar_retiro(<?php echo $codg_reti; ?>);return(false)">Eliminar</button>
        		<?php abrir_ventana('../reportes/solicitud_retiro_new.php','v_imprimir','Imprimir Solicitud',"codg_reti=".$codg_reti."&cedu_soci=".$res[cedu_soci]."&seccion=".$_GET['seccion']); ?> 
        	</td>
      	</tr>
	</table>
<?php } ?>