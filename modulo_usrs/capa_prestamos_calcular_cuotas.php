<?PHP 
	include_once('../comunes/conexion_basedatos.php');
	include_once('../comunes/formularios_funciones.php');
	////// calculamos el monto de cada cuota
    $monto_mensual=0;
	$monto_cuota=0;
	$deuda = $_POST['mont_apro'];
	$interes_calculo = redondear(($_POST['mont_intr']/12),2,"",".");
	if ($_POST['tipo_cuot']=='Q'){ 
	    $cuenta_for = $_POST['plaz_prst'] * 2;
	    $interes = ($interes_calculo/100)/2;
	}
	if ($_POST['tipo_cuot']=='S'){ 
	    $cuenta_for = $_POST['plaz_prst'] * 4;
	    $interes = ($interes_calculo/100)/4;
	}
	if ($_POST['tipo_cuot']=='M'){ 
	    $cuenta_for = $_POST['plaz_prst'];
	    $interes = ($interes_calculo/100);
	}
	$monto_mensual = $_POST['mont_apro']*(($interes*pow(1+$interes,$cuenta_for))/(pow(1+$interes,$cuenta_for)-1));
	$monto_mensual = redondear($monto_mensual,2,"",".");
    $monto_cuota = $monto_mensual;
	if($monto_cuota<=0){
		$monto_cuota='';
	}else{
		echo '<input name="accion" type="hidden" value="Guardar" />';
	}
?>
<input name="mont_cuot" id="mont_cuot" type="text" size="15" maxlength="11" value="<?PHP if($mont_cuot){ echo $mont_cuot; }else{ echo $monto_cuota; } ?>" />
