<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Datos de Saldos</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cedu_soci'];
include ('../comunes/formularios_funciones.php');
$codg_depn=$_POST['codg_depn'];
$codg_tcrg=$_POST['codg_tcrg'];
$tipo_movi=$_POST['tipo_movi'];
$dest_movi=$_POST['dest_movi'];
$mont_movi2=$_POST['mont_movi2'];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'socios_saldo.php?cedu_soci='.$_GET["cedu_soci"].'&seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$pagina = 'socios_saldo.php?cedu_soci='.$_GET["cedu_soci"].'&seccion='.$_GET["seccion"];
$tabla = "socios_movimientos";
$ncampos = "7";
$datos[0] = crear_datos ("cedu_soci","C�dula",$_POST['cedu_soci'],"1","12","numericos");
$datos[1] = crear_datos ("fcha_movi","Fecha",$_POST['fcha_movi'],"1","10","fecha");
$datos[2] = crear_datos ("conc_movi","Concepto",$_POST['conc_movi'],"1","255","alfanumericos");
$datos[3] = crear_datos ("tipo_movi","Tipo de Movimiento",$_POST['tipo_movi'],"1","1","alfabeticos");
$datos[4] = crear_datos ("dest_movi","Destino de Movimiento",$_POST['dest_movi'],"1","1","alfabeticos");
$datos[5] = crear_datos ("mont_movi","Monto de Movimiento",$_POST['mont_movi'],"1","12","decimal");
$datos[6] = crear_datos ("orig_movi","Origen de Movimiento","M","1","1","alfabeticos");


if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_movi = $row["codg_movi"];
	    $cedu_soci = $row["cedu_soci"];
	    $fcha_movi = $row["fcha_movi"];
	    $conc_movi = $row["conc_movi"];
	    $tipo_movi = $row["tipo_movi"];
		$dest_movi = $row["dest_movi"];
	    $mont_movi = $row["mont_movi"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		$sql="select s.* from socios_saldo s where ".$datos[0][0]."=".$datos[0][2]."";
		$reg=mysql_fetch_array(mysql_query($sql));

		if ($reg=mysql_fetch_array(mysql_query($sql))){
			if($datos[4][2]=="A"){
				if($datos[3][2]=="I"){ $monto=($reg['apor_sald']+$datos[5][2]); $campo="apor_sald"; }
				if($datos[3][2]=="E"){ $monto=($reg['apor_sald']-$datos[5][2]); $campo="apor_sald"; }
			}
			if($datos[4][2]=="R"){
				if($datos[3][2]=="I"){ $monto=($reg['rete_sald']+$datos[5][2]); $campo="rete_sald"; }
				if($datos[3][2]=="E"){ $monto=($reg['rete_sald']-$datos[5][2]); $campo="rete_sald"; }
			}
			$sql="update socios_saldo set fcha_sald='".$datos[1][2]."', ".$campo."=".$monto." where ".$datos[0][0]."=".$datos[0][2];
		}
		else { 
			if($datos[4][2]=="A"){
				$campo="apor_sald";
			}
			if($datos[4][2]=="R"){
				$campo="rete_sald"; 
			}
			$sql="INSERT INTO socios_saldo (cedu_soci,fcha_sald,".$campo.") VALUES ('".$datos[0][2]."','".$datos[1][2]."',".$datos[5][2].");";	
		}
		mysql_query($sql);
		modificar_func($ncampos,$datos,$tabla,"codg_movi",$_POST["codg_movi"],$pagina2, "");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
		//$boton = comp_exist($datos[1][0],$datos[1][2]."' AND cedu_soci = '".$datos[0][2],$tabla,$boton,'si',"Datos de Saldo");
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	$sql="select s.* from socios_saldo s where ".$datos[0][0]."=".$datos[0][2]."";
	if ($reg=mysql_fetch_array(mysql_query($sql))){
		if($datos[4][2]=="A"){
			if($datos[3][2]=="I"){ $monto=($reg['apor_sald']+$datos[5][2]); $campo="apor_sald"; }
			if($datos[3][2]=="E"){ $monto=($reg['apor_sald']-$datos[5][2]); $campo="apor_sald"; }
		}
		if($datos[4][2]=="R"){
			if($datos[3][2]=="I"){ $monto=($reg['rete_sald']+$datos[5][2]); $campo="rete_sald"; }
			if($datos[3][2]=="E"){ $monto=($reg['rete_sald']-$datos[5][2]); $campo="rete_sald"; }
		}
		$sql="update socios_saldo set fcha_sald='".$datos[1][2]."', ".$campo."=".$monto." where ".$datos[0][0]."=".$datos[0][2];
	}
	else { 
		if($datos[4][2]=="A"){
			$campo="apor_sald";
		}
		if($datos[4][2]=="R"){
			$campo="rete_sald"; 
		}
		$sql="INSERT INTO socios_saldo (cedu_soci,fcha_sald,".$campo.") VALUES ('".$datos[0][2]."','".$datos[1][2]."',".$datos[5][2].");";	
	}
	mysql_query($sql);
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_movi"],"codg_movi",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"codg_movi","socios_movimientos",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="98%" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar los Movimientos de  
                    	<?php $sql_socio = "SELECT * FROM socios WHERE cedu_soci=".$viene_val; 
                    	$res_socio = mysql_fetch_array(mysql_query($sql_socio));
                    	echo $res_socio[apel_soci].' '.$res_socio[nomb_soci];
                    	?>
                    </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Fecha&nbsp;del&nbsp;Movimiento:</td>
                        <td width="75%">
							<?php escribir_campo('codg_movi',$_POST["codg_movi"],$codg_movi,'readonly',12,15,'Codigo del Dato de Saldo',$boton,$existe,'','','oculto'); ?>
							<?php escribir_campo('cedu_soci',$viene_val,$cedu_soci,'readonly',12,15,'Cedula del Ahorrista',$boton,$existe,'','','oculto'); ?>
							<?php escribir_campo('fcha_movi',date("Y-m-d"),$fcha_movi,'',11,15,'Fecha del Movimiento (No se puede modificar)',$boton,$existe,'fecha','',''); ?>
						</td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Concepto:</td>
                        <td>
							<?php escribir_campo('conc_movi',$_POST["conc_movi"],$conc_movi,'',80,30,'Sueldo del Ahorrista',$boton,$existe,'','',''); ?>
						</td>
					  </tr>
                      <tr>
                        <td class="etiquetas">Tipo&nbsp;de&nbsp;Movimiento:</td>
                        <td><?php if ($boton != "Modificar" AND $boton != "Actualizar") { echo '<select name="tipo_movi" title="Tipo de Movimiento">
                          <option>Seleccione...</option>
                          <option value="I" '; if ($tipo_movi == "I" || $_POST['tipo_movi'] =="I") { echo 'selected'; } echo '>Ingreso</option>
                          <option value="E" '; if ($tipo_movi == "E" || $_POST['tipo_movi'] =="E") { echo 'selected'; } echo '>Egreso</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tipo_movi" id="tipo_movi" value="'.$tipo_movi.'" >'; 
						    if ($tipo_movi == "I") { echo 'Ingreso'; } 
							if ($tipo_movi == "E") { echo 'Egreso'; }
						}?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Destino&nbsp;del&nbsp;Movimiento:</td>
                        <td><?php if ($boton != "Modificar" AND $boton != "Actualizar") { echo '<select name="dest_movi" title="Destino del Movimiento">
                          <option>Seleccione...</option>
                          <option value="A" '; if ($dest_movi == "A" || $_POST['dest_movi'] =="A") { echo 'selected'; } echo '>Aportes</option>
                          <option value="R" '; if ($dest_movi == "R" || $_POST['dest_movi'] =="R") { echo 'selected'; } echo '>Retenciones</option>
                          <option value="I" '; if ($dest_movi == "I" || $_POST['dest_movi'] =="I") { echo 'selected'; } echo '>Reintegros</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="dest_movi" id="dest_movi" value="'.$dest_movi.'" >'; 
						    if ($dest_movi == "A") { echo 'Aportes'; } 
							if ($dest_movi == "R") { echo 'Retenciones'; }
							if ($dest_movi == "I") { echo 'Reintegros'; }
						}?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Monto&nbsp;del&nbsp;Movimiento:</td>
                        <td>
							<?php escribir_campo('mont_movi',$_POST["mont_movi"],$mont_movi,'',11,15,'Monto del Movimiento',$boton,$existe,'','',''); ?>
							<?php escribir_campo('mont_movi2',$_POST["mont_movi"],$mont_movi,'',11,15,'Monto del Movimiento',$boton,$existe,'','','oculto'); ?>
						</td>
					  </tr>
		           </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_socios_datos.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_socios_saldo.php'); ?></td>
                  </tr>
				  <tr>
                    <td align="center"><?php include ('capa_socios_movimientos.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onClick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>