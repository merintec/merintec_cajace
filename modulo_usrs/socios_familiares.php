<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Familiares de Ahorristas</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cedu_soci'];
include ('../comunes/formularios_funciones.php');
$codg_pare=$_POST['codg_pare'];
$porc_benf2=$_POST['porc_benf2'];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'socios_familiares.php?cedu_soci='.$_GET["cedu_soci"].'&seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$pagina2 = 'socios_familiares.php?cedu_soci='.$_GET["cedu_soci"].'&seccion='.$_GET["seccion"];
$tabla = " socios_beneficiarios";	// nombre de la tabla
$ncampos = "9";		//numero de campos del formulario
$datos[0] = crear_datos ("cedu_soci","Cedula del Ahorrista",$_POST['cedu_soci'],"1","12","numericos");
$datos[1] = crear_datos ("naci_benf","Nacionalidad",$_POST['naci_benf'],"0","1","alfabeticos");
$datos[2] = crear_datos ("cedu_benf","Cedula del Familiar",$_POST['cedu_benf'],"0","12","numericos");
$datos[3] = crear_datos ("stdc_benf","Estado Civil",$_POST['stdc_benf'],"1","1","alfabeticos");
$datos[4] = crear_datos ("nomb_benf","Nombre",$_POST['nomb_benf'],"1","50","alfabeticos");
$datos[5] = crear_datos ("apel_benf","Apellido",$_POST['apel_benf'],"1","50","alfabeticos");
$datos[6] = crear_datos ("ciud_benf","Ciudad",$_POST['ciud_benf'],"1","30","alfanumericos");
$datos[7] = crear_datos ("codg_pare","Parentesco",$_POST['codg_pare'],"1","11","numericos");
$datos[8] = crear_datos ("porc_benf","Porcentaje",$_POST['porc_benf'],"1","5","decimal");

//--> extraccion de porcentaje acumulado.
$sql="select SUM(porc_benf) AS porcentaje from socios_beneficiarios where cedu_soci=".$viene_val;
$reg=mysql_fetch_array(mysql_query($sql));
/////////////////////////////////////////

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_benf = $row["codg_benf"];
	    $cedu_soci = $row["cedu_soci"];
		$naci_benf = $row["naci_benf"];
	    $cedu_benf = $row["cedu_benf"];
		$stdc_benf = $row["stdc_benf"];
	    $nomb_benf = $row["nomb_benf"];
	    $apel_benf = $row["apel_benf"];
		$ciud_benf = $row["ciud_benf"];
	    $codg_pare = $row["codg_pare"];
		$porc_benf = $row["porc_benf"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
		if($datos[8][2]<0 OR $datos[8][2]>100){
			echo '<SCRIPT>alert("Error: El porcentaje no debe ser mayor a cien (100)");</SCRIPT>';
			$validacion = "";
		}else{
			if((($datos[8][2]-$porc_benf2)+$reg['porcentaje'])>100 OR (($datos[8][2]-$porc_benf2)+$reg['porcentaje'])<1){
				echo '<SCRIPT>alert("Error: La suma de porcentajes de los beneficiarios no debe ser mayor a cien (100) ni menor a uno (1)");</SCRIPT>';
				$validacion = "";
			}
		}
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_benf",$_POST["codg_benf"],$pagina2,'');
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
		if ($_POST['cedu_benf']){
			$boton = comp_exist($datos[2][0],$datos[2][2]."' AND cedu_soci = '".$datos[0][2],$tabla,$boton,'si',"Cedulas de Familiares de Ahorristas");
		}
		if($datos[8][2]<0 OR $datos[8][2]>100){
			echo '<SCRIPT>alert("Error: El porcentaje no debe ser mayor a cien (100)");</SCRIPT>';
			$boton = "Verificar";
		}else{
			if(($datos[8][2]+$reg['porcentaje'])>100 OR ($datos[8][2]+$reg['porcentaje'])<1){
				echo '<SCRIPT>alert("Error: La suma de porcentajes de los beneficiarios no debe ser mayor a cien (100) ni menor a uno (1)");</SCRIPT>';
				$boton = "Verificar";
			}
		}
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_benf"],"codg_benf",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"codg_benf","socios_beneficiarios",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Beneficiarios de 
                    	<?php $sql_socio = "SELECT * FROM socios WHERE cedu_soci=".$viene_val; 
                    	$res_socio = mysql_fetch_array(mysql_query($sql_socio));
                    	echo $res_socio[apel_soci].' '.$res_socio[nomb_soci];
                    	?>
                    </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      
					  <tr>
                        <td width="25%" class="etiquetas">C&eacute;dula:</td>
                        <td width="75%">
							<?php escribir_campo('codg_benf',$_POST["codg_benf"],$codg_benf,'readonly',12,15,'Codigo del Beneficiario',$boton,$existe,'','','oculto'); ?>
							<?php escribir_campo('cedu_soci',$viene_val,$cedu_soci,'readonly',12,15,'Cedula del Ahorrista',$boton,$existe,'','','oculto'); ?>
							<?php if ($boton != "Modificar") { echo '<select name="naci_benf" title="Nacionalidad">
									  <option>-</option>
									  <option value="V" '; if ($naci_benf == "V" || $_POST['naci_benf'] =="V") { echo 'selected'; } echo '>V-</option>
									  <option value="E" '; if ($naci_benf == "E" || $_POST['naci_benf'] =="E") { echo 'selected'; } echo '>E-</option>
									</select>'; } 
									else 
									{ 
										echo '<input type="hidden" name="naci_benf" id="naci_benf" value="'.$naci_benf.'" >'; 
										if ($naci_benf == "V") { echo 'V-'; } 
										if ($naci_benf == "E") { echo 'E-'; }
							}?>	
							<?php escribir_campo('cedu_benf',$_POST["cedu_benf"],$cedu_benf,'',12,15,'Cedula del Beneficiario',$boton,$existe,'','',''); ?>
						</td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Estado&nbsp;Civil:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="stdc_benf" title="Sexo">
                          <option>Seleccione...</option>
                          <option value="S" '; if ($stdc_benf == "S" || $_POST['stdc_benf'] =="S") { echo 'selected'; } echo '>Soltero(a)</option>
                          <option value="C" '; if ($stdc_benf == "C" || $_POST['stdc_benf'] =="C") { echo 'selected'; } echo '>Casado(a)</option>
						  <option value="D" '; if ($stdc_benf == "D" || $_POST['stdc_benf'] =="D") { echo 'selected'; } echo '>Divorciado(a)</option>
						  <option value="V" '; if ($stdc_benf == "V" || $_POST['stdc_benf'] =="V") { echo 'selected'; } echo '>Viudo(a)</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="stdc_benf" id="stdc_benf" value="'.$stdc_benf.'" >'; 
						    if ($stdc_benf == "S") { echo 'Soltero(a)'; } 
							if ($stdc_benf == "C") { echo 'Casado(a)'; }
							if ($stdc_benf == "D") { echo 'Divorciado(a)'; }
							if ($stdc_benf == "V") { echo 'Viudo(a)'; }
						}?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Nombre:</td>
                        <td>
							<?php escribir_campo('nomb_benf',$_POST["nomb_benf"],$nomb_benf,'',50,35,'Nombre del Beneficiario',$boton,$existe,'','',''); ?>
						</td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Apellido:</td>
                        <td>
							<?php escribir_campo('apel_benf',$_POST["apel_benf"],$apel_benf,'',50,35,'Apellido del Beneficiario',$boton,$existe,'','',''); ?>
						</td>
					  </tr>
                      <tr>
                        <td class="etiquetas">Ciudad:</td>
                        <td><?php escribir_campo('ciud_benf',$_POST["ciud_benf"],$ciud_benf,'',50,10,'Ciudad del Beneficiario',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>					  
                        <td class="etiquetas">Parentesco:</td>
                        <td>
							<?php combo('codg_pare', $codg_pare, 'parentescos', $link, 0, 0, 1, '', 'codg_pare', "", $boton, "", ""); ?>
						</td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Porcentaje:</td>
                        <td>
							<?php escribir_campo('porc_benf',$_POST["porc_benf"],$porc_benf,'',5,5,'Porcetaje de Beneficio',$boton,$existe,'','',''); ?>&nbsp;%&nbsp;<?PHP if($reg['porcentaje']!=""){ echo "Resta: ".redondear((100-$reg['porcentaje']), "2", ".", ",")."&nbsp;%"; } ?>
							<?php escribir_campo('porc_benf2',$_POST["porc_benf"],$porc_benf,'',5,5,'Porcetaje de Beneficio',$boton,$existe,'','','oculto'); ?>
						</td>
					  </tr>
		           </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_socios_beneficiarios.php'); ?></td>
                  </tr>
                 <?php if ($beneficiarios=='SI') { ?>
                 <tr>
                    <td align="center"><br><? abrir_ventana('../reportes/planilla_beneficiarios.php','v_Beneficiarios','Planilla Beneficiarios',"cedu_soci=".$cedu_soci."&seccion=".$_GET['seccion']);  ?></td>
                 </tr>
                 <?php } ?>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
