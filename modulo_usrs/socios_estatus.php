<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Estatus de Ahorristas</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cedu_soci'];
include ('../comunes/formularios_funciones.php');
$codg_stat=$_POST['codg_stat'];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'socios_estatus.php?cedu_soci='.$_GET["cedu_soci"].'&seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$pagina = 'socios_estatus.php?cedu_soci='.$_GET["cedu_soci"].'&seccion='.$_GET["seccion"];
$tabla = "socios_estado";
$ncampos = "8";
$datos[0] = crear_datos ("cedu_soci","C�dula",$_POST['cedu_soci'],"1","12","numericos");
$datos[1] = crear_datos ("codg_stat","Estatus",$_POST['codg_stat'],"1","1","numericos");
$datos[2] = crear_datos ("numr_estd","Nro de Solicitud",$_POST['numr_estd'],"1","11","numericos");
$datos[3] = crear_datos ("numr_acta","Nro de Acta",$_POST['numr_acta'],"1","20","alfanumericos");
$datos[4] = crear_datos ("fcha_estd","Fecha",$_POST['fcha_estd'],"1","10","fecha");
$datos[5] = crear_datos ("obse_estd","Observaciones",$_POST['obse_estd'],"0","255","alfanumericos");
$datos[6] = crear_datos ("codg_soci","Codigo de Socio",$_POST['codg_soci'],"0","20","alfanumericos");
$datos[7] = crear_datos ("codg_soci2","2do Codigo de Socio",$_POST['codg_soci2'],"0","20","alfanumericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_estd = $row["codg_estd"];
	    $cedu_soci = $row["cedu_soci"];
	    $codg_stat = $row["codg_stat"];
	    $numr_estd = $row["numr_estd"];
	    $numr_acta = $row["numr_acta"];
	    $fcha_estd = $row["fcha_estd"];
		$obse_estd = $row["obse_estd"];
		$codg_soci = $row["codg_soci"];
		$codg_soci2 = $row["codg_soci2"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_estd",$_POST["codg_estd"],$pagina2);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
		$boton = comp_exist($datos[1][0],$datos[1][2]."' AND cedu_soci = '".$datos[0][2],$tabla,$boton,'si',"Estatus de Ahorristas");
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_estd"],"codg_estd",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"codg_estd","socios_estado",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Estatus de 
                    	<?php $sql_socio = "SELECT * FROM socios WHERE cedu_soci=".$viene_val; 
                    	$res_socio = mysql_fetch_array(mysql_query($sql_socio));
                    	echo $res_socio[apel_soci].' '.$res_socio[nomb_soci];
                    	?>
                    </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Estatus:</td>
                        <td width="75%">
							<?php escribir_campo('codg_estd',$_POST["codg_estd"],$codg_estd,'readonly',12,15,'Codigo del Estado',$boton,$existe,'','','oculto'); ?>
							<?php escribir_campo('cedu_soci',$viene_val,$cedu_soci,'readonly',12,15,'Cedula del Ahorrista',$boton,$existe,'','','oculto'); ?>
							<?php combo('codg_stat', $codg_stat, 'tipos_status', $link, 0, 0, 1, '', 'codg_stat', "", $boton, "", ""); ?>
						</td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Numero&nbsp;de&nbsp;Solicitud:</td>
                        <td>
							<?php escribir_campo('numr_estd',$_POST["numr_estd"],$numr_estd,'',11,15,'Numero de Solicitud',$boton,$existe,'','',''); ?>
						</td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Numero&nbsp;de&nbsp;Acta:</td>
                        <td>
							<?php escribir_campo('numr_acta',$_POST["numr_acta"],$numr_acta,'',11,15,'Numero de Acta',$boton,$existe,'','',''); ?>
						</td>
					  </tr>
                      <tr>
                        <td class="etiquetas">Fecha&nbsp;de&nbsp;Acta:</td>
                        <td>
							<?php escribir_campo('fcha_estd',$_POST["fcha_estd"],$fcha_estd,'',11,15,'Fecha de Acta',$boton,$existe,'fecha','',''); ?>
						</td>
					  </tr>
                      <tr>
                        <td class="etiquetas">C&oacute;digo&nbsp;de&nbsp;Socio:</td>
                        <td>
							<?php escribir_campo('codg_soci',$_POST["codg_soci"],$codg_soci,'',11,15,'Codigo de Socio',$boton,$existe,'','',''); ?>
						</td>
					  </tr>
                      <tr>
                        <td class="etiquetas">2do C&oacute;digo&nbsp;de&nbsp;Socio:</td>
                        <td>
							<?php escribir_campo('codg_soci2',$_POST["codg_soci2"],$codg_soci2,'',11,15,'2do Codigo de Socio',$boton,$existe,'','',''); ?>
						</td>
					  </tr>
					  <tr>
                        <td class="etiquetas">Observaciones:</td>
                        <td><?php echo escribir_area('obse_estd',$_POST["obse_estd"],$obse_estd,'',30,3,'Observaciones sobre estatus',$boton,$existe,'')?></td>
                      </tr>
		           </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_socios_estatus.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
