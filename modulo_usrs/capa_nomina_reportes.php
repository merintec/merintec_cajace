<?php
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ////// esta capa debe recibir el codigo de la nomina (codg_nmna) 
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ////// si no trae el codigo de la nomina lo consulta lo recibe mediante POST[nomina];
   echo '<meta charset="ISO-8859-1">';
   include_once('../comunes/conexion_basedatos.php');
   include_once('../comunes/formularios_funciones.php');
   $numero_columnas = 6; 
   if ($codg_nmna == '') {
      $codg_nmna = $_POST[nomina];
   }
?>
<table border="0" cellpadding="0" cellspacing="0" width="80%" align="center">
   <tr >
      <td align="center" colspan="<?php echo $numero_columnas; ?>">
   	  <br><hr>
   	</td>            
   </tr>
   <tr>
      <td align="center" class="titulo" colspan="<?php echo $numero_columnas; ?>">
   	  REPORTES DE N�MINA
      </td>            
   </tr>
   <tr class="nomina_titulo">
      <td>Referencia</td>
   	<td>Aporte/Ahorros/Reintegros</td>
   	<td>Montepios</td>
      <td>Pr�stamos</td>
   	<td>Asientos<div id="asientos"></div></td>
   </tr>
   <?php
      /// para verificar si es una nomina de la dependencia interna ()
      $sql_quien = "SELECT * FROM nominas n, valores v WHERE n.codg_nmna =".$codg_nmna." AND n.codg_depn = v.val_val AND v.des_val='DEP_INT'";
      if ($res_quien = mysql_fetch_array(mysql_query($sql_quien))){
         $sql_reporte = "SELECT *,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Aporte') as monto_aportes, (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Retencion') as monto_retenciones,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Montepio') as monto_montepio,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Reintegro') as monto_reintegros,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Prestamo') as monto_prestamos FROM nominas_detalle nd WHERE nd.codg_nmna=".$codg_nmna." GROUP BY nd.codg_nmna";
         $bus_reporte = mysql_query($sql_reporte);
         $res_reporte = mysql_fetch_array($bus_reporte);
            echo '<tr align="center">
               <td>Comprobante de pago</td>
               <td>'; 
                  if ($res_reporte[monto_aportes] > 0 || $res_reporte[monto_retenciones] > 0 || $res_reporte[monto_reintegros] > 0){
                     abrir_ventana('../reportes/nmna_ahorros_retenciones.php','v_reporte','VER (Bs. '.redondear(($res_reporte[monto_aportes]+$res_reporte[monto_retenciones]+$res_reporte[monto_reintegros]),2,'.',',').')',"codg_nmna=".$codg_nmna."&seccion=".$_GET['seccion']);
                  }
               echo '</td>
               <td>'; 
                  if ($res_reporte[monto_montepio] > 0 ){
                     abrir_ventana('../reportes/nmna_montepio.php','v_reporte','VER (Bs. '.redondear(($res_reporte[monto_montepio]),2,'.',',').')',"codg_nmna=".$codg_nmna."&seccion=".$_GET['seccion']);
                  }
               echo '</td>
               <td>'; 
                  if ($res_reporte[monto_prestamos] > 0 ){
                     abrir_ventana('../reportes/nmna_prestamos.php','v_reporte','VER (Bs. '.redondear(($res_reporte[monto_prestamos]),2,'.',',').')',"codg_nmna=".$codg_nmna."&seccion=".$_GET['seccion']);
                  }
               echo '</td>
               <td>Comprobante de Pago</td>
            </tr>';          
      }
      else { 
         $sql_reporte = "SELECT *,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Aporte' AND codg_pago IS NULL) as monto_aportes, (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Retencion' AND codg_pago IS NULL) as monto_retenciones,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Montepio' AND codg_pago IS NULL) as monto_montepio,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Reintegro' AND codg_pago IS NULL) as monto_reintegros,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Prestamo' AND codg_pago IS NULL) as monto_prestamos FROM nominas_detalle nd WHERE nd.codg_nmna=".$codg_nmna." GROUP BY nd.codg_nmna";
         $bus_reporte = mysql_query($sql_reporte);
         while($res_reporte = mysql_fetch_array($bus_reporte)){
            echo '<tr align="center">
               <td>'.$res_reporte[numr_refe].'</td>
               <td>'; 
                  if ($res_reporte[monto_aportes] > 0 || $res_reporte[monto_retenciones] > 0 || $res_reporte[monto_reintegros] > 0){
                     abrir_ventana('../reportes/nmna_ahorros_retenciones.php','v_reporte','VER (Bs. '.redondear(($res_reporte[monto_aportes]+$res_reporte[monto_retenciones]+$res_reporte[monto_reintegros]),2,'.',',').')',"codg_nmna=".$codg_nmna."&codg_pago=".$res_reporte[codg_pago]."&seccion=".$_GET['seccion']);
                  }
               echo '</td>
               <td>'; 
                  if ($res_reporte[monto_montepio] > 0 ){
                     abrir_ventana('../reportes/nmna_montepio.php','v_reporte','VER (Bs. '.redondear(($res_reporte[monto_montepio]),2,'.',',').')',"codg_nmna=".$codg_nmna."&codg_pago=".$res_reporte[codg_pago]."&seccion=".$_GET['seccion']);
                  }
               echo '</td>
               <td>'; 
                  if ($res_reporte[monto_prestamos] > 0 ){
                     abrir_ventana('../reportes/nmna_prestamos.php','v_reporte','VER (Bs. '.redondear(($res_reporte[monto_prestamos]),2,'.',',').')',"codg_nmna=".$codg_nmna."&codg_pago=".$res_reporte[codg_pago]."&seccion=".$_GET['seccion']);
                  }
               echo '</td>
               <td>'; 
                  $asientos_mos = 'NO';
                  $sql_asientos = "SELECT * FROM asientos WHERE orgn_asien = 'Nomina' AND codg_rela = ".$codg_nmna." AND codg2_rela = ".$res_reporte[codg_pago]." AND codg3_rela = 0";
                  $bus_asientos = mysql_query($sql_asientos);
                  while ($res_asientos = mysql_fetch_array($bus_asientos)){
                     $asientos_mos = 'SI';
                     echo '<input type="hidden" id="asiento'.$res_reporte[codg_pago].'" name="asiento'.$res_reporte[codg_pago].'" value="'.$res_asientos[codg_asien].'">';
                     $parametros = "'eliminar','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','".$res_asientos[codg_asien]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Eliminar ('.$res_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
                     $parametros = "'imprimir','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','".$res_asientos[codg_asien]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Imprimir ('.$res_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
                  }
                  if ($asientos_mos == 'NO'){
                     $parametros = "'ver','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','0','".$res_reporte[codg_exce]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Ver" onclick="asientos('.$parametros.')">';
                     $parametros = "'generar','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','0','".$res_reporte[codg_exce]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Generar" onclick="asientos('.$parametros.')">';
                  }
               echo '</td>
            </tr>'; 
         } 
         $sql_reporte = "SELECT *,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Aporte' AND codg_pago=dp.codg_pago) as monto_aportes, (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Retencion' AND codg_pago=dp.codg_pago) as monto_retenciones,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Montepio' AND codg_pago=dp.codg_pago) as monto_montepio,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Reintegro' AND codg_pago=dp.codg_pago) as monto_reintegros,(SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Prestamo' AND codg_pago=dp.codg_pago) as monto_prestamos FROM nominas_detalle nd, dependencias_pagos dp WHERE nd.codg_nmna=".$codg_nmna." AND nd.codg_pago = dp.codg_pago GROUP BY nd.codg_nmna,nd.codg_pago";
         $bus_reporte = mysql_query($sql_reporte);
         while($res_reporte = mysql_fetch_array($bus_reporte)){
            echo '<tr align="center">
               <td>'.$res_reporte[numr_refe].'</td>
               <td>'; 
                  if ($res_reporte[monto_aportes] > 0 || $res_reporte[monto_retenciones] > 0 || $res_reporte[monto_reintegros] > 0){
                     abrir_ventana('../reportes/nmna_ahorros_retenciones.php','v_reporte','VER (Bs. '.redondear(($res_reporte[monto_aportes]+$res_reporte[monto_retenciones]+$res_reporte[monto_reintegros]),2,'.',',').')',"codg_nmna=".$codg_nmna."&codg_pago=".$res_reporte[codg_pago]."&seccion=".$_GET['seccion']);
                  }
               echo '</td>
               <td>'; 
                  if ($res_reporte[monto_montepio] > 0 ){
                     abrir_ventana('../reportes/nmna_montepio.php','v_reporte','VER (Bs. '.redondear(($res_reporte[monto_montepio]),2,'.',',').')',"codg_nmna=".$codg_nmna."&codg_pago=".$res_reporte[codg_pago]."&seccion=".$_GET['seccion']);
                  }
               echo '</td>
               <td>'; 
                  if ($res_reporte[monto_prestamos] > 0 ){
                     abrir_ventana('../reportes/nmna_prestamos.php','v_reporte','VER (Bs. '.redondear(($res_reporte[monto_prestamos]),2,'.',',').')',"codg_nmna=".$codg_nmna."&codg_pago=".$res_reporte[codg_pago]."&seccion=".$_GET['seccion']);
                  }
               echo '</td>
               <td>'; 
                  $asientos_mos = 'NO';
                  $sql_asientos = "SELECT * FROM asientos WHERE orgn_asien = 'Nomina' AND codg_rela = ".$codg_nmna." AND codg2_rela = ".$res_reporte[codg_pago]." AND codg3_rela = 0";
                  $bus_asientos = mysql_query($sql_asientos);
                  while ($res_asientos = mysql_fetch_array($bus_asientos)){
                     $asientos_mos = 'SI';
                     echo '<input type="hidden" id="asiento'.$res_reporte[codg_pago].'" name="asiento'.$res_reporte[codg_pago].'" value="'.$res_asientos[codg_asien].'">';
                     $parametros = "'eliminar','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','".$res_asientos[codg_asien]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Eliminar ('.$res_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
                     $parametros = "'imprimir','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','".$res_asientos[codg_asien]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Imprimir ('.$res_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
                  }
                  if ($asientos_mos == 'NO'){
                     $parametros = "'ver','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','0','".$res_reporte[codg_exce]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Ver" onclick="asientos('.$parametros.')">';
                     $parametros = "'generar','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','0','".$res_reporte[codg_exce]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Generar" onclick="asientos('.$parametros.')">';
                  }
               echo '</td>
            </tr>'; 
         }
         //// Verificar si hay cuentas por pagar
         $sql_reporte = "SELECT *, (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Reintegro' AND codg_pago='-1' AND codg_exce = nd.codg_exce) as monto_reintegros FROM nominas_detalle nd, dependencias_pagos_exce dpe, nominas n WHERE nd.codg_nmna=".$codg_nmna." AND nd.codg_pago = -1 AND nd.codg_exce=dpe.codg_exce AND dpe.codg_nmna=n.codg_nmna GROUP BY nd.codg_nmna,nd.codg_pago,nd.codg_exce";
         $bus_reporte = mysql_query($sql_reporte);
         while($res_reporte = mysql_fetch_array($bus_reporte)){
            switch ($res_reporte[prdo_nmna]) {
               case 8:
                  $porpagar_nom = 'Mes';
                  break;
               case 7:
                  $porpagar_nom = '2da Quincena';
                  break;
               case 6:
                  $porpagar_nom = '1era Quincena';
                  break;
               case ($res_reporte[prdo_nmna] > 10):
                  $porpagar_nom = 'Semana '.($res_reporte[prdo_nmna]-10).'�';
                  break;
               default:
                  # code...
                  break;
            }
            $res_reporte[numr_refe] = 'Cuenta por pagar<br>'.$porpagar_nom.' '.convertir_mes($res_reporte[mess_nmna]).' '.$res_reporte[anno_nmna];

            echo '<tr align="center">
               <td style="font-size:11px;">'.$res_reporte[numr_refe].'</td>
               <td>'; 
                  if ($res_reporte[monto_aportes] > 0 || $res_reporte[monto_retenciones] > 0 || $res_reporte[monto_reintegros] > 0){
                     abrir_ventana('../reportes/nmna_ahorros_retenciones.php','v_reporte','VER (Bs. '.redondear(($res_reporte[monto_aportes]+$res_reporte[monto_retenciones]+$res_reporte[monto_reintegros]),2,'.',',').')',"codg_nmna=".$codg_nmna."&codg_exce=".$res_reporte[codg_exce]."&seccion=".$_GET['seccion']);
                  }
               echo '</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>'; 
                  $asientos_mos = 'NO';
                  $sql_asientos = "SELECT * FROM asientos WHERE orgn_asien = 'Nomina' AND codg_rela = ".$codg_nmna." AND codg3_rela = ".$res_reporte[codg_exce];
                  $bus_asientos = mysql_query($sql_asientos);
                  while ($res_asientos = mysql_fetch_array($bus_asientos)){
                     $asientos_mos = 'SI';
                     echo '<input type="hidden" id="asiento'.$res_reporte[codg_pago].'" name="asiento'.$res_reporte[codg_pago].'" value="'.$res_asientos[codg_asien].'">';
                     $parametros = "'eliminar','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','".$res_asientos[codg_asien]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Eliminar ('.$res_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
                     $parametros = "'imprimir','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','".$res_asientos[codg_asien]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Imprimir ('.$res_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
                  }
                  if ($asientos_mos == 'NO'){
                     $parametros = "'ver','".$codg_nmna."','','".$_GET['seccion']."','0','".$res_reporte[codg_exce]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Ver" onclick="asientos('.$parametros.')">';
                     $parametros = "'generar','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','0','".$res_reporte[codg_exce]."'";
                     echo '<input type="button" name="v_reporte" id="v_reporte" value="Generar" onclick="asientos('.$parametros.')">';
                  }
               echo '</td>
            </tr>'; 
         }
      }

   ?>
</table>