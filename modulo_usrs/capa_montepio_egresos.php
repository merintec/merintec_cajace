<?php 
    //datos de la capa anterior
    $total_pagar_gen = $total_pagar;
    $total_pagado_gen = $total_pagado;
    ///
    $cuenta_datos = 0;
    $total_pagar = 0;
    $total_pagado = 0;
    $sql_capa0  = "SELECT * FROM montepio m, socios s WHERE codg_mpio=".$codg_mpio." AND m.cedu_soci=s.cedu_soci";
    $res_capa0 = mysql_query($sql_capa0);
    /// Si el montepio es por muerte del socio se consultan los datos de los beneficiarios con porcentaje mayor a 0
    if ($reg_capa0=mysql_fetch_array($res_capa0)) {
        $cedu_soci=$reg_capa0['cedu_soci'];
        if ($reg_capa0['tipo_mpio']=='S'){
            $sql_capa1="SELECT apel_benf,nomb_benf,porc_benf,cedu_benf FROM socios_beneficiarios WHERE cedu_soci=".$cedu_soci." AND porc_benf>0";
            $res_capa1=mysql_query($sql_capa1);
            while ($reg_capa1=mysql_fetch_array($res_capa1)){
                $datos1[$cuenta_datos][0]=$reg_capa1[0].' '.$reg_capa1[1]." (".$reg_capa1[2]."%)";
                $datos1[$cuenta_datos][1]=$total_pagar_gen*($reg_capa1[2]/100);
                // verificar si existe algun pago emitido a nombre de cada beneficiario
                $sql_capa2="SELECT SUM(ec.mnto_debe) as pago_benf FROM egresos e, egresos_conceptos ec WHERE e.codg_egre=ec.codg_egre AND codg_mpio=".$reg_capa0['codg_mpio']." AND codg_rela='".$reg_capa1[3]."' AND nomb_rela='".$reg_capa1[0]." ".$reg_capa1[1]."' GROUP BY codg_rela";
                $res_capa2=mysql_query($sql_capa2);
                $reg_capa2=mysql_fetch_array($res_capa2);
                $datos1[$cuenta_datos][2]=$reg_capa2[0];
                $total_pagar = redondear(($total_pagar+$datos1[$cuenta_datos][1]),2,"",".");
                $total_pagado = redondear(($total_pagado+$datos1[$cuenta_datos][2]),2,"",".");
                $cuenta_datos++;
            }        
        }
        if ($reg_capa0['tipo_mpio']=='F'){
             // verificar si existe algun pago emitido a nombre del beneficiario
                $sql_capa2="SELECT SUM(ec.mnto_debe) as pago_benf FROM egresos e, egresos_conceptos ec WHERE e.codg_egre=ec.codg_egre AND codg_mpio=".$reg_capa0['codg_mpio']." AND codg_rela='".$cedu_soci."' GROUP BY codg_rela";
                $res_capa2=mysql_query($sql_capa2);
                $reg_capa2=mysql_fetch_array($res_capa2);
                $datos1[$cuenta_datos][0]=$reg_capa0['apel_soci']." ".$reg_capa0['nomb_soci'];
                $datos1[$cuenta_datos][1]=$total_pagar_gen;
                $datos1[$cuenta_datos][2]=$reg_capa2[0];

                $total_pagar = redondear(($total_pagar+$datos1[$cuenta_datos][1]),2,"",".");
                $total_pagado = redondear(($total_pagado+$datos1[$cuenta_datos][2]),2,"",".");
                $cuenta_datos++;
        }
    }
?>
<html>
  <body>
    <table width="100%" border="1" cellpadding="0" cellspacing="0" align="center" id="tabla" name="tabla" > 
      <tr class="etiquetas">
        <td align="center" colspan="5" class="titulo"><b>DATOS DE LOS PAGOS EFECTUADOS A BENEFICIARIO(S)</b></td>
      </tr>
      <tr class="etiquetas">
        <td align="center"><b>N�</b></td>
        <td align="center"><b>BENFICIARIO</b></td>
        <td align="center" width="80px"><b>Monto a Pagar</b></td>
        <td align="center" width="80px"><b>Monto Pagado</b></td>
        <td align="center" width="30px"><b>% Pagado</b></td>
      </tr>
      <?php for ($i=0;$i<$cuenta_datos;$i++) { 
        if (($i % 2)==0) { $clase = 0; } else { $clase=1; }
      ?>    
        <tr class="<?php echo 'linea'.$clase; ?>">
          <td align="right"><?php echo $i+1; ?>&nbsp;</td>
          <td align="left">&nbsp;<?php echo $datos1[$i][0]; ?></td>
          <td align="right"><?php echo redondear($datos1[$i][1],2,".",","); ?>&nbsp;</td>
          <td align="right"><?php echo redondear($datos1[$i][2],2,".",","); ?>&nbsp;</td>
          <td align="right"><?php echo redondear(($datos1[$i][2]*100/$datos1[$i][1]),2,".",",").'%'; ?>&nbsp;</td>
        </tr>
      <?php }  
        if (($i % 2)==0) { $clase = 0; } else { $clase=1; } 
      ?> 
        <tr class="<?php echo 'linea'.$clase; ?>">
          <td align="right" colspan="2" class="etiquetas">TOTALES&nbsp;</td>
          <td align="right" class="etiquetas"><?php echo redondear($total_pagar,2,".",","); ?>&nbsp;</td>
          <td align="right" class="etiquetas"><?php echo redondear($total_pagado,2,".",","); ?>&nbsp;</td>
          <td align="right" class="etiquetas"><?php echo redondear(($total_pagado*100/$total_pagar),2,".",",").'%'; ?>&nbsp;</td>
        </tr>
    </table>
  </body>
</html>
