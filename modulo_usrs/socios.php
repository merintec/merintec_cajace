<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'socios.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "socios";	// nombre de la tabla
$ncampos = "16";	//numero de campos del formulario
$datos[0] = crear_datos ("cedu_soci","C�dula",$_POST['cedu_soci'],"1","12","numericos");
$datos[1] = crear_datos ("naci_soci","Nacionalidad",$_POST['naci_soci'],"1","1","alfabeticos");
$datos[2] = crear_datos ("nomb_soci","Nombre",$_POST['nomb_soci'],"1","50","alfabeticos");
$datos[3] = crear_datos ("apel_soci","Apellido",$_POST['apel_soci'],"1","50","alfabeticos");
$datos[4] = crear_datos ("sexo_soci","Sexo",$_POST['sexo_soci'],"0","1","alfabeticos");
$datos[5] = crear_datos ("fchn_soci","Fecha de Nacimiento",$_POST['fchn_soci'],"1","10","fecha");
$datos[6] = crear_datos ("titu_soci","Abreviacion del Titulo",$_POST['titu_soci'],"0","5","alfanumericos");
$datos[7] = crear_datos ("stdc_soci","Estado Civil",$_POST['stdc_soci'],"0","1","alfabeticos");
$datos[8] = crear_datos ("ciud_soci","Ciudad",$_POST['ciud_soci'],"1","50","alfanumericos");
$datos[9] = crear_datos ("dirh_soci","Direccion",$_POST['dirh_soci'],"0","255","alfanumericos");
$datos[10] = crear_datos ("mail_soci","E-mail",$_POST['mail_soci'],"0","50","email");
$datos[11] = crear_datos ("tlfn_soci","Tel�fono",$_POST['tlfn_soci'],"0","12","numericos");
$datos[12] = crear_datos ("tlfc_soci","Celular",$_POST['tlfc_soci'],"0","12","numericos");
$datos[13] = crear_datos ("mont_apor","Monto de Aportes",$_POST['mont_apor'],"0","11","decimal");
$datos[14] = crear_datos ("mont_rete","Monto de Retenciones",$_POST['mont_rete'],"0","11","decimal");
$datos[15] = crear_datos ("obsr_impr","Observaciones",$_POST['obsr_impr'],"0","255","alfanumericos");

$tabla2 = "socios_estado";
$ncampos2 = "7";
$datos2[0] = crear_datos ("cedu_soci","C�dula",$_POST['cedu_soci'],"1","12","numericos");
$datos2[1] = crear_datos ("codg_stat","Estatus",2,"1","1","numericos");
$datos2[2] = crear_datos ("numr_estd","Nro de Solicitud",0,"1","1","numericos");
$datos2[3] = crear_datos ("numr_acta","Nro de Acta",0,"1","1","alfanumericos");
$datos2[4] = crear_datos ("fcha_estd","Fecha",date("Y-m-d"),"1","10","fecha");
$datos2[5] = crear_datos ("obse_estd","Observaciones","Se Registra en la Caja de Ahorro","1","255","alfabeticos");
$datos2[6] = crear_datos ("codg_soci","Codigo de Socio","","0","1","alfanumericos");

$tabla3 = "socios_saldo";
$ncampos3 = "4";
$datos3[0] = crear_datos ("cedu_soci","C�dula",$_POST['cedu_soci'],"1","12","numericos");
$datos3[1] = crear_datos ("fcha_sald","Fecha",date("Y-m-d"),"1","10","fecha");
$datos3[2] = crear_datos ("apor_sald","Saldo de Aportes",0.00,"1","4","decimal");
$datos3[3] = crear_datos ("rete_sald","Saldo de Retenciones",0.00,"1","4","decimal");

if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="C�dula";
		$datos[0]="cedu_soci";
		$parametro[1]="Nombre";
		$datos[1]="nomb_soci";
		$parametro[2]="Apellido";
		$datos[2]="apel_soci";
		busqueda_varios(5,$buscando,$datos,$parametro,"cedu_soci");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cedu_soci = $row["cedu_soci"];
	    $naci_soci = $row["naci_soci"];
	    $nomb_soci = $row["nomb_soci"];
	    $apel_soci = $row["apel_soci"];
	    $sexo_soci = $row["sexo_soci"];
	    $fchn_soci = $row["fchn_soci"];
	    $titu_soci = $row["titu_soci"];
	    $stdc_soci = $row["stdc_soci"];
		$ciud_soci = $row["ciud_soci"];
	    $dirh_soci = $row["dirh_soci"];
	    $mail_soci = $row["mail_soci"];
	    $tlfn_soci = $row["tlfn_soci"];
	    $tlfc_soci = $row["tlfc_soci"];
	    $mont_apor = $row["mont_apor"];
	    $mont_rete = $row["mont_rete"];
		$obsr_impr = $row["obsr_impr"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cedu_soci",$_POST["cedu_soci2"],$pagina,"");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	insertar_func($ncampos2,$datos2,$tabla2,$pagina);
	insertar_func($ncampos3,$datos3,$tabla3,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cedu_soci"],"cedu_soci",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}


?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos del Ahorristas</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      		<tr>
                        <td width="25%" class="etiquetas">C&eacute;dula:</td>
                        <td width="75%">
						<?php if ($boton != "Modificar") { echo '<select name="naci_soci" title="Nacionalidad">
                          <option>-</option>
                          <option value="V" '; if ($naci_soci == "V" || $_POST['naci_soci'] =="V") { echo 'selected'; } echo '>V-</option>
                          <option value="E" '; if ($naci_soci == "E" || $_POST['naci_soci'] =="E") { echo 'selected'; } echo '>E-</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="naci_soci" id="naci_soci" value="'.$naci_soci.'" >'; 
						    if ($naci_soci == "V") { echo 'V-'; } 
							if ($naci_soci == "E") { echo 'E-'; }
						}?>						
                        <?php escribir_campo('cedu_soci',$_POST["cedu_soci"],$cedu_soci,'',12,15,'Cedula del Ahorrista',$boton,$existe,'','','')?></td>
								<?php escribir_campo('cedu_soci2',$_POST["cedu_soci2"],$cedu_soci,'',12,15,'Cedula del Ahorrista',$boton,$existe,'','','oculto')?>                      
                      </tr>
                      <tr>
                        <td class="etiquetas">Titulo:</td>
                        <td><?php escribir_campo('titu_soci',$_POST["titu_soci"],$titu_soci,'',12,10,'Titulo del Ahorrista',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Nombre:</td>
                        <td><?php escribir_campo('nomb_soci',$_POST["nomb_soci"],$nomb_soci,'',50,25,'Nombre del Ahorrista',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Apellido:</td>
                        <td width="75%"><?php escribir_campo('apel_soci',$_POST["apel_soci"],$apel_soci,'',50,25,'Apellido del Ahorrista',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Sexo:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="sexo_soci" title="Sexo">
                          <option>Seleccione...</option>
                          <option value="M" '; if ($sexo_soci == "M" || $_POST['sexo_soci'] =="M") { echo 'selected'; } echo '>Masculino</option>
                          <option value="F" '; if ($sexo_soci == "F" || $_POST['sexo_soci'] =="F") { echo 'selected'; } echo '>Femenino</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="sexo_soci" id="sexo_soci" value="'.$sexo_soci.'" >'; 
						    if ($sexo_soci == "M") { echo 'Masculino'; } 
							if ($sexo_soci == "F") { echo 'Femenino'; }
						}?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha&nbsp;de&nbsp;Nacimiento:</td>
                        <td><?php escribir_campo('fchn_soci',$_POST["fchn_soci"],$fchn_soci,'',12,15,'Fecha de Nacimiento del Ahorrista',$boton,$existe,'fecha','','')?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Estado&nbsp;Civil:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="stdc_soci" title="Sexo">
                          <option>Seleccione...</option>
                          <option value="S" '; if ($stdc_soci == "S" || $_POST['stdc_soci'] =="S") { echo 'selected'; } echo '>Soltero(a)</option>
                          <option value="C" '; if ($stdc_soci == "C" || $_POST['stdc_soci'] =="C") { echo 'selected'; } echo '>Casado(a)</option>
						  <option value="D" '; if ($stdc_soci == "D" || $_POST['stdc_soci'] =="D") { echo 'selected'; } echo '>Divorciado(a)</option>
						  <option value="V" '; if ($stdc_soci == "V" || $_POST['stdc_soci'] =="V") { echo 'selected'; } echo '>Viudo(a)</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="stdc_soci" id="stdc_soci" value="'.$stdc_soci.'" >'; 
						    if ($stdc_soci == "S") { echo 'Soltero(a)'; } 
							if ($stdc_soci == "C") { echo 'Casado(a)'; }
							if ($stdc_soci == "D") { echo 'Divorciado(a)'; }
							if ($stdc_soci == "V") { echo 'Viudo(a)'; }
						}?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Ciudad:</td>
                        <td><?php escribir_campo('ciud_soci',$_POST["ciud_soci"],$ciud_soci,'',50,10,'Ciudad del Ahorrista',$boton,$existe,'','','')?></td>
                      </tr>
					  <tr>
                        <td class="etiquetas">Direcci&oacute;n:</td>
                        <td><?php echo escribir_area('dirh_soci',$_POST["dirh_soci"],$dirh_soci,'',30,3,'Direcci&oacute;n de habitacion',$boton,$existe,'')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">e-mail:</td>
                        <td width="75%"><?php escribir_campo('mail_soci',$_POST["mail_soci"],$mail_soci,'',50,25,'Direcci�n de correo electr�nico',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Tel&eacute;fono:</td>
                        <td><?php escribir_campo('tlfn_soci',$_POST["tlfn_soci"],$tlfn_soci,'',12,15,'Tel�fono del Ahorrista',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Celular:</td>
                        <td><?php escribir_campo('tlfc_soci',$_POST["tlfc_soci"],$tlfc_soci,'',12,15,'Celular del Ahorrista',$boton,$existe,'','','')?></td>
                      </tr>
					  <tr>
                        <td class="etiquetas">Nota&nbsp;Importante:</td>
                        <td><?php echo escribir_area('obsr_impr',$_POST["obsr_impr"],$obsr_impr,'',30,3,'Observaciones o nota importante',$boton,$existe,'')?></td>
                      </tr>
 
              <tr><td colspan="2"><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
		      <?php  if ($boton == 'Modificar') { echo '<tr><td colspan="2" align="center"><hr></td></tr>'; ?>
		      <tr>
		        <td height="45" colspan="2" align="center" valign="top" class="etiquetas">
		        <table width="100%" align="center" cellspacing="10">
                  <tr>
				  	<td width="<?PHP echo (100/5)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('socios_estatus.php','v_estatus','Estatus',"cedu_soci=".$cedu_soci."&seccion=".$_GET['seccion']); } ?></td>
		    		<td width="<?PHP echo (100/5)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('socios_familiares.php','v_familiares','Datos Familiares',"cedu_soci=".$cedu_soci."&seccion=".$_GET['seccion']); } ?></td>
                    <td width="<?PHP echo (100/5)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('socios_saldo.php','v_saldo','Movimientos',"cedu_soci=".$cedu_soci."&seccion=".$_GET['seccion']); } ?></td>
                    <td width="<?PHP echo (100/5)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('socios_datos_lab.php','v_cuentas','Datos Laborales',"cedu_soci=".$cedu_soci."&seccion=".$_GET['seccion']); } ?></td> 
                    <td width="<?PHP echo (100/5)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('../reportes/socios_edocuenta.php','v_cuentas','Edo. Cuenta',"cedu_soci=".$cedu_soci."&seccion=".$_GET['seccion']); } ?></td> 
                  </tr>
                  </table>
                  <table width="100%" align="center" cellspacing="10">
                  <tr>
                    <td width="<?PHP echo (100/5)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('../reportes/solicitud_afiliacion.php','v_afiliaci�n','Planilla Afiliaci�n',"cedu_soci=".$cedu_soci."&seccion=".$_GET['seccion']); } ?></td>  
                    <td width="<?PHP echo (100/5)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('../reportes/solicitud_retiro.php','v_retiro','Solicitud de Retiro',"cedu_soci=".$cedu_soci."&seccion=".$_GET['seccion']); } ?></td>                
                  </tr>
                </table>		          
				</td>
		        </tr>
                    </table></td>
                  </tr>
		  <?php  } ?> 
                  <tr>
                    <td colspan="2" align="center">
					<?php 
						$ncriterios =3; 
						$criterios[0] = "C�dula"; 
						$campos[0] ="cedu_soci";
						$criterios[1] = "Nombre";
						$campos[1] = "nomb_soci";
						$criterios[2] = "Apellido";
						$campos[2] = "apel_soci";						
						if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                  
                  <tr>
                    <td></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
