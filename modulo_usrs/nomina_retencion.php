<meta charset="utf-8" />
<?php
	if (!$cedula){ $cedula = $_POST[cedula]; }
	if (!$porcien_ret){ $porcien_ret = $_POST[porcentaje]; }
	if (!$codg_nmna){ $codg_nmna = $_POST[nomina]; }
	$variable = 'retencion_'.$cedula;			// nombre de la variable del monto almacenado en la nomina
	$variable2 = 'retencion_'.$cedula.'_nom';	// nombre de la variable del monto según sueldo
	$variable3 = 'retencion_'.$cedula.'_mod'; 	// Para mostrar u ocultar editar
	$variable4 = "'retencion".$cedula."'"; 	// nombre de la caja del monto
	$variable5 = '$ico_reg'.$cedula; // nombre de la valiable para completar el nombre del icono del registro
	$variable6 = '$ico_pag'.$cedula; // nombre de la valiable para completar el nombre del icono del pago
	$color = '#000000';
	////// monto del retencion según nómina
	$sueldo = $reg_pers["suel_dlab"];
	if ($sueldo==0){
		$sueldo = $_POST[sueldo];
	}
	$$variable2 = redondear(($sueldo*$porcien_ret),2,"",".");
	////// consultar si hay un monto almacenado para la nomina seleccionada 
	$sql_reten = "SELECT nd.*, (if (nd.codg_pago > 0, 'on', 'off')) as pagado, 'on' as registrado FROM nominas_detalle nd WHERE nd.codg_nmna = ".$codg_nmna." AND nd.cedu_soci = ".$cedula." AND nd.moti_dlle = 'Retencion'";
	if ($res_reten = mysql_fetch_array(mysql_query($sql_reten))){
		$$variable = $res_reten[mnto_dlle];
		if ($$variable!=$$variable2) { $color = '#FF0000'; }
		$$variable3 = "display: none;";
		$$variable5 = $res_reten[registrado];
		$$variable6 = $res_reten[pagado];
		$accion_registrado = "procesar('eliminar','".$codg_nmna."','".$res_reten[codg_dlle]."', 'registrado', '".$cedula."', '".$sueldo."', 'retencion', '".$porcien_ret."','','','".$res_reten[codg_pago]."')";
		if ($$variable6=='on'){
			$accion_pagado = "procesar('eliminar','".$codg_nmna."','".$res_reten[codg_dlle]."', 'pagado', '".$cedula."', '".$sueldo."', 'retencion', '".$porcien_ret."','','','".$res_reten[codg_pago]."')";			
		}
		else {
			$accion_pagado = "procesar('guardar','".$codg_nmna."','".$res_reten[codg_dlle]."', 'pagado', '".$cedula."', '".$sueldo."', 'retencion', '".$porcien_ret."')";
		}
	}
	////// si no hay nada almacenado para la nomina mostramos el calculado en base a la nomina
	else {
		$$variable = $$variable2;
		$$variable5 = 'off';
		$$variable6 = 'off';
		$accion_pagado = "procesar('guardar','".$codg_nmna."','0', 'pagado', '".$cedula."', '".$sueldo."', 'retencion', '".$porcien_ret."')";
		$accion_registrado = "procesar('guardar','".$codg_nmna."','0', 'registrado', '".$cedula."', '".$sueldo."', 'retencion', '".$porcien_ret."')";
	}
	/// para verificar si es una nomina de la dependencia interna ()
	$sql_quien = "SELECT * FROM nominas n, valores v WHERE n.codg_nmna =".$codg_nmna." AND n.codg_depn = v.val_val AND v.des_val='DEP_INT'";
	if ($res_quien = mysql_fetch_array(mysql_query($sql_quien))){
		$nomina_interna = 'SI';
	}
	else { $nomina_interna = ''; }
	if ($$variable5=='on' && $nomina_interna != ''){
		$$variable6 = 'on';
		$accion_pagado = "procesar('eliminar','".$codg_nmna."','".$res_aporte[codg_dlle]."', 'registrado', '".$cedula."', '".$sueldo."', 'aporte', '".$porcien_apr."', '', '', '".$res_aporte[codg_pago]."')";
	}
	////// Mostrar en pantalla
	echo '<div style="height: 25px; padding-top:3px;text-align: right;" id="pago'.$res_reten["codg_pago"].'">';
		echo '<font color="'.$color.'">'.$$variable.'</font>&nbsp;';
		echo '<input style="display: none; border:0px solid #fff; text-align:right; font-weight:bold; width:50px;" type="text" name="retencion'.$cedula.'" id="retencion'.$cedula.'" value="'.$$variable.'" size="5" readOnly>';
		echo '&nbsp;<img style="cursor: pointer;'.$$variable3.'" src="../imagenes/editar.png" width="14px" id="editar" name="editar" value="editar" onclick="quita_readOnly('.$variable4.')" title="Click para editar el monto">'; 
		echo '&nbsp;<img style="cursor: pointer;" src="../imagenes/registrado_'.$$variable5.'.png" width="16px" id="editar" name="editar" value="editar" onclick="'.$accion_registrado.'" title="Click para registrar el monto">';
		echo '&nbsp;<img style="cursor: pointer;" src="../imagenes/pagado_'.$$variable6.'.png" width="16px" id="editar" name="editar" value="editar" onclick="'.$accion_pagado.'" title="Click para pagar el monto">'; 
	echo '&nbsp;</div>';
?>
