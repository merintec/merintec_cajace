<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php 
  if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$requerido = "<font color='red'><b>*</b>&nbsp;</font>";
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'proveedores.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "proveedores";		// nombre de la tabla
$ncampos = "29";		//numero de campos del formulario
$datos[0]  = crear_datos ("cod_pro","C�digo",$_POST['cod_pro'],"0","4","numericos");
$datos[1]  = crear_datos ("fch_pro","Fecha",$_POST['fch_pro'],"10","10","fecha");
$datos[2]  = crear_datos ("rif_pro","Rif",ucwords(strtoupper($_POST['rif_pro'])),"10","10","alfanumericos");
$datos[3]  = crear_datos ("nom_pro","Nombre",$_POST['nom_pro'],"1","50","alfabeticos");
$datos[4]  = crear_datos ("dir_pro","Direcci�n",$_POST['dir_pro'],"1","255","alfanumericos");
$datos[5]  = crear_datos ("act_pro","Actividad",$_POST['act_pro'],"1","50","alfabeticos");
$datos[6]  = crear_datos ("cap_pro","Capital Pagado",$_POST['cap_pro'],"0","12","decimal");
$datos[7]  = crear_datos ("cas_pro","Capital Suscrito",$_POST['cas_pro'],"0","12","decimal");
$datos[8]  = crear_datos ("tel_pro","Tel�fono",$_POST['tel_pro'],"11","11","numericos");
$datos[9]  = crear_datos ("fax_pro","Fax",$_POST['fax_pro'],"0","11","numericos");
$datos[10] = crear_datos ("per_pro","Personalidad",$_POST['per_pro'],"1","1","alfabeticos");
$datos[11] = crear_datos ("ofi_reg_pro","Oficina de Reg.",$_POST['ofi_reg_pro'],"0","50","alfanumericos");
$datos[12] = crear_datos ("num_reg_pro","Num Registro",$_POST['num_reg_pro'],"0","5","numericos");
$datos[13] = crear_datos ("tom_reg_pro","Tomo Registro",$_POST['tom_reg_pro'],"0","25","alfanumericos");
$datos[14] = crear_datos ("fol_reg_pro","Folio Registro",$_POST['fol_reg_pro'],"0","4","numericos");
$datos[15] = crear_datos ("fch_reg_pro","Fecha Registro",$_POST['fch_reg_pro'],"0","10","fecha");
$datos[16] = crear_datos ("num_mod_pro","Num Modificaci�n",$_POST['num_mod_pro'],"0","5","numericos");
$datos[17] = crear_datos ("tom_mod_pro","Tomo Modificaci�n",$_POST['tom_mod_pro'],"0","25","alfanumericos");
$datos[18] = crear_datos ("fol_mod_pro","Folio Modificaci�n",$_POST['fol_mod_pro'],"0","4","numericos");
$datos[19] = crear_datos ("fch_mod_pro","Fecha Modificaci�n",$_POST['fch_mod_pro'],"0","10","fecha");
$datos[20] = crear_datos ("pat_pro","Num. Patente",$_POST['pat_pro'],"0","25","alfanumericos");
$datos[21] = crear_datos ("ivss_pro","Num. IVSS",$_POST['ivss_pro'],"0","25","alfanumericos");
$datos[22] = crear_datos ("ince_pro","Num. INCES",$_POST['ince_pro'],"0","25","alfanumericos");
$datos[23] = crear_datos ("nom_rep_pro","Nombre Representante",$_POST['nom_rep_pro'],"1","50","alfabeticos");
$datos[24] = crear_datos ("ape_rep_pro","Apellido Representante",$_POST['ape_rep_pro'],"1","50","alfabeticos");
$datos[25] = crear_datos ("ced_rep_pro","C�dula Representante",$_POST['ced_rep_pro'],"8","8","numericos");
$datos[26] = crear_datos ("dir_rep_pro","Direcci�n Representante",$_POST['dir_rep_pro'],"0","255","alfanumericos");
$datos[27] = crear_datos ("tel_rep_pro","Tel�fono Representante",$_POST['tel_rep_pro'],"0","11","numericos");
$datos[28] = crear_datos ("car_rep_pro","Cargo Representante",$_POST['car_rep_pro'],"0","50","alfanumericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Rif";
		$datos[0]="rif_pro";
		$parametro[1]="Nombre";
		$datos[1]="nom_pro";
		busqueda_varios(4,$buscando,$datos,$parametro,"cod_pro");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_pro = $row["cod_pro"];
	    $fch_pro = $row["fch_pro"];
	    $rif_pro = $row["rif_pro"];
	    $nom_pro = $row["nom_pro"];
	    $dir_pro = $row["dir_pro"];
	    $act_pro = $row["act_pro"];
	    $cap_pro = $row["cap_pro"];
	    $cas_pro = $row["cas_pro"];
	    $tel_pro = $row["tel_pro"];
	    $fax_pro = $row["fax_pro"];
	    $per_pro = $row["per_pro"];
	    $ofi_reg_pro = $row["ofi_reg_pro"];
	    $num_reg_pro = $row["num_reg_pro"];
	    $tom_reg_pro = $row["tom_reg_pro"];
	    $fol_reg_pro = $row["fol_reg_pro"];
	    $fch_reg_pro = $row["fch_reg_pro"];
	    $num_mod_pro = $row["num_mod_pro"];
	    $tom_mod_pro = $row["tom_mod_pro"];
	    $fol_mod_pro = $row["fol_mod_pro"];
	    $fch_mod_pro = $row["fch_mod_pro"];
	    $pat_pro = $row["pat_pro"];
	    $ivss_pro = $row["ivss_pro"];
	    $ince_pro = $row["ince_pro"];
	    $nom_rep_pro = $row["nom_rep_pro"];
	    $ape_rep_pro = $row["ape_rep_pro"];
	    $ced_rep_pro = $row["ced_rep_pro"];
	    $dir_rep_pro = $row["dir_rep_pro"];
	    $tel_rep_pro = $row["tel_rep_pro"];
	    $car_rep_pro = $row["car_rep_pro"];

	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_pro",$_POST["cod_pro"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[2][0],$datos[2][2],$tabla,$boton,'si',$tabla);
	if ($boton=="Guardar"){ 
	$boton=comp_exist($datos[3][0],$datos[3][2],$tabla,$boton,'si',$tabla);}
}
if ($_POST["confirmar"]=="Guardar") 
{
	$sql = "SELECT MAX(cod_pro) AS cod_pro FROM proveedores";
    	$res = mysql_fetch_array(mysql_query($sql));
    	$cod_pro = $res['cod_pro'] + 1;
	$datos[0]  = crear_datos ("cod_pro","C�digo",$cod_pro,"1","4","numericos");
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_pro"],"cod_pro",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de Proveedores </td>
                  </tr>
                  <tr>
                    <td align="center" class="etiquetas">Los campos marcados con " <?php echo $requerido; ?>" son obligatorios</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">C�digo de Proveedor:</td>
                        <td width="75%">
			<input name="cod_pro" type="hidden" id="cod_pro" value="<?php if(! $existe) { echo $_POST['cod_pro']; } else { echo $cod_pro; } ?>" size="35" title="C�digo de Proveedor"><?php if ($boton != "Verificar" && $boton != "Guardar") { echo 'CMCE RP-'.$cod_pro.'/'.substr($fch_pro, 0, 4); } else echo 'Por Asignar...'; ?>
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas"><?php echo $requerido; ?>Fecha&nbsp;de&nbsp;Ingreso: <? //<a href="../comunes/pdf.php?phpdoc=http://localhost/siacon/modulo_usrs/proveedores.php">pdf</a>?> </td>
                        <td><input name="fch_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_pro" value="<?php if(! $existe) { echo $_POST["fch_pro"]; } else { echo $fch_pro; } ?>" size="20" title="Fecha de Ingreso" />
                          <?php if ($boton=='Modificar') { echo $fch_pro; } ?>
			<?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_pro,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas"><?php echo $requerido; ?>RIF:
                          </td>
			<td>
                        <input name="rif_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="rif_pro" value="<?php if(! $existe) { echo $_POST['rif_pro']; } else { echo $rif_pro; } ?>" size="15" title="Rif del Proveedor"> <?php if ($boton=='Modificar') { echo $rif_pro; } ?>
			<?php //////// Verificar RIF
			if ($boton=='Modificar') {
			   abrir_ventana('http://contribuyente.seniat.gov.ve/BuscaRif/BuscaRif.jsp','vrif','SENIAT','p_rif='.$rif_pro); 
			   abrir_ventana('http://rncenlinea.snc.gob.ve/reportes/resultado_busqueda','vrif','RNC','?p=1&rif='.$rif_pro.'&search=RIF');
			}?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas"><?php echo $requerido; ?>Nombre:
                          </td>
			<td>
                        <input name="nom_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_pro" value="<?php if(! $existe) { echo $_POST['nom_pro']; } else { echo $nom_pro; } ?>" size="35" title="Nombre de Proveedor">
                        <?php if ($boton=='Modificar') { echo $nom_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas"><?php echo $requerido; ?>Direcci�n:
                          </td>
			<td>
                        <input name="dir_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="dir_pro" value="<?php if(! $existe) { echo $_POST['dir_pro']; } else { echo $dir_pro; } ?>" size="35" title="Direcci�n de Proveedor">
                        <?php if ($boton=='Modificar') { echo $dir_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas"><?php echo $requerido; ?>Actividad:
                          </td>
			<td>
                        <input name="act_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="act_pro" value="<?php if(! $existe) { echo $_POST['act_pro']; } else { echo $act_pro; } ?>" size="35" title="Actividad de Proveedor">
                        <?php if ($boton=='Modificar') { echo $act_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Capital Pagado:
                          </td>
			<td>
                        <input name="cap_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="cap_pro" value="<?php if(! $existe) { echo $_POST['cap_pro']; } else { echo $cap_pro; } ?>" size="15" title="Capital Pagado de Proveedor">
                        <?php if ($boton=='Modificar') { echo $cap_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Capital Suscrito:
                          </td>
			<td>
                        <input name="cas_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="cas_pro" value="<?php if(! $existe) { echo $_POST['cas_pro']; } else { echo $cas_pro; } ?>" size="15" title="Capital Suscrito de Proveedor">
                        <?php if ($boton=='Modificar') { echo $cas_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas"><?php echo $requerido; ?>Tel�fono:
                          </td>
			<td>
                        <input name="tel_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="tel_pro" value="<?php if(! $existe) { echo $_POST['tel_pro']; } else { echo $tel_pro; } ?>" size="15" title="Tel&eacute;fono del Proveedor">
                        <?php if ($boton=='Modificar') { echo $tel_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fax:
                          </td>
			<td>
                        <input name="fax_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fax_pro" value="<?php if(! $existe) { echo $_POST['fax_pro']; } else { echo $fax_pro; } ?>" size="15" title="Fax del Proveedor">
                        <?php if ($boton=='Modificar') { echo $fax_pro; } ?></td>
                      </tr>
		      <tr>
                        <td class="etiquetas"><?php echo $requerido; ?>Personalidad:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="per_pro" title="Personalidad">
                          <option>Seleccione...</option>
                          <option value="P" '; if ($per_pro == "P" || $_POST['per_pro'] =="P") { echo 'selected'; } echo '>Natural</option>
                          <option value="J" '; if ($per_pro == "J" || $_POST['per_pro'] =="J") { echo 'selected'; } echo '>Jur&iacute;dica</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="per_pro" id="per_pro" value="'.$per_pro.'" >'; 
						    if ($per_pro == "P") { echo 'Natural'; } 
							if ($per_pro == "J") { echo 'Jur&iacute;dica'; }
						}?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Oficina de Registro:
                          </td>
			<td>
                        <input name="ofi_reg_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ofi_reg_pro" value="<?php if(! $existe) { echo $_POST['ofi_reg_pro']; } else { echo $ofi_reg_pro; } ?>" size="35" title="Oficina de Registro">
                        <?php if ($boton=='Modificar') { echo $ofi_reg_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N&uacute;mero de Registro:
                          </td>
			<td>
                        <input name="num_reg_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="num_reg_pro" value="<?php if(! $existe) { echo $_POST['num_reg_pro']; } else { echo $num_reg_pro; } ?>" size="10" title="N&uacute;mero de Registro">
                        <?php if ($boton=='Modificar') { echo $num_reg_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Tomo de Registro:
                          </td>
			<td>
                        <input name="tom_reg_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="tom_reg_pro" value="<?php if(! $existe) { echo $_POST['tom_reg_pro']; } else { echo $tom_reg_pro; } ?>" size="10" title="Tomo de Registro">
                        <?php if ($boton=='Modificar') { echo $tom_reg_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Folio de Registro:
                          </td>
			<td>
                        <input name="fol_reg_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fol_reg_pro" value="<?php if(! $existe) { echo $_POST['fol_reg_pro']; } else { echo $fol_reg_pro; } ?>" size="10" title="Folio de Registro">
                        <?php if ($boton=='Modificar') { echo $fol_reg_pro; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Registro: </td>
                        <td><input name="fch_reg_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_reg_pro" value="<?php if(! $existe) { echo $_POST["fch_reg_pro"]; } else { echo $fch_reg_pro; } ?>" size="20" title="Fecha de Registro" />
                          <?php if ($boton=='Modificar') { echo $fch_reg_pro; } ?>
			<?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_reg_pro,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N&uacute;mero de Registro de Modificaci&oacute;n:
                          </td>
			<td>
                        <input name="num_mod_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="num_mod_pro" value="<?php if(! $existe) { echo $_POST['num_mod_pro']; } else { echo $num_reg_pro; } ?>" size="10" title="N&uacute;mero de Registro de Modificaci&oacute;n">
                        <?php if ($boton=='Modificar') { echo $num_mod_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Tomo de Registro de Modificaci&oacute;n:
                          </td>
			<td>
                        <input name="tom_mod_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="tom_mod_pro" value="<?php if(! $existe) { echo $_POST['tom_mod_pro']; } else { echo $tom_reg_pro; } ?>" size="10" title="Tomo de Registro de Modificaci&oacute;n">
                        <?php if ($boton=='Modificar') { echo $tom_mod_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Folio de Registro de Modificaci&oacute;n:
                          </td>
			<td>
                        <input name="fol_mod_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fol_mod_pro" value="<?php if(! $existe) { echo $_POST['fol_mod_pro']; } else { echo $fol_reg_pro; } ?>" size="10" title="Folio de Registro de Modificaci&oacute;n">
                        <?php if ($boton=='Modificar') { echo $fol_mod_pro; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Registro de Modificiaci&oacute;n: </td>
                        <td><input name="fch_mod_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_mod_pro" value="<?php if(! $existe) { echo $_POST["fch_mod_pro"]; } else { echo $fch_mod_pro; } ?>" size="20" title="Fecha de Registro de Modificaci&oacute;n" />
                          <?php if ($boton=='Modificar') { echo $fch_mod_pro; } ?>
			<?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_mod_pro,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Num. Patente:
                          </td>
			<td>
                        <input name="pat_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="pat_pro" value="<?php if(! $existe) { echo $_POST['pat_pro']; } else { echo $pat_pro; } ?>" size="20" title="Num Patente de Proveedor">
                        <?php if ($boton=='Modificar') { echo $pat_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Num. IVSS:
                          </td>
			<td>
                        <input name="ivss_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ivss_pro" value="<?php if(! $existe) { echo $_POST['ivss_pro']; } else { echo $ivss_pro; } ?>" size="20" title="Num IVSS de Proveedor">
                        <?php if ($boton=='Modificar') { echo $ivss_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Num. INCES:
                          </td>
			<td>
                        <input name="ince_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ince_pro" value="<?php if(! $existe) { echo $_POST['ince_pro']; } else { echo $ince_pro; } ?>" size="20" title="Num INCES de Proveedor">
                        <?php if ($boton=='Modificar') { echo $ince_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas"><?php echo $requerido; ?>Nombre&nbsp;de&nbsp;Representante:
                          </td>
			<td>
                        <input name="nom_rep_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_rep_pro" value="<?php if(! $existe) { echo $_POST['nom_rep_pro']; } else { echo $nom_rep_pro; } ?>" size="35" title="Nombre de Representante de Proveedor">
                        <?php if ($boton=='Modificar') { echo $nom_rep_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas"><?php echo $requerido; ?>Apellido&nbsp;de&nbsp;Representante:
                          </td>
			<td>
                        <input name="ape_rep_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ape_rep_pro" value="<?php if(! $existe) { echo $_POST['ape_rep_pro']; } else { echo $ape_rep_pro; } ?>" size="35" title="Apellido de Representante de Proveedor">
                        <?php if ($boton=='Modificar') { echo $ape_rep_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas"><?php echo $requerido; ?>C&eacute;dula&nbsp;de&nbsp;Representante:
                          </td>
			<td>
                        <input name="ced_rep_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ced_rep_pro" value="<?php if(! $existe) { echo $_POST['ced_rep_pro']; } else { echo $ced_rep_pro; } ?>" size="15" title=">C&eacute;dula de Representante de Proveedor">
                        <?php if ($boton=='Modificar') { echo $ced_rep_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Direcci&oacute;n&nbsp;de&nbsp;Representante:
                          </td>
			<td>
                        <input name="dir_rep_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="dir_rep_pro" value="<?php if(! $existe) { echo $_POST['dir_rep_pro']; } else { echo $dir_rep_pro; } ?>" size="35" title="Direcci&oacute;n de Representante de Proveedor">
                        <?php if ($boton=='Modificar') { echo $dir_rep_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Tel&eacute;fono de Representante:
                          </td>
			<td>
                        <input name="tel_rep_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="tel_rep_pro" value="<?php if(! $existe) { echo $_POST['tel_rep_pro']; } else { echo $tel_rep_pro; } ?>" size="15" title="Tel&eacute;fono de Representante de Proveedor">
                        <?php if ($boton=='Modificar') { echo $tel_rep_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Cargo de Representante:
                          </td>
			<td>
                        <input name="car_rep_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="car_rep_pro" value="<?php if(! $existe) { echo $_POST['car_rep_pro']; } else { echo $car_rep_pro; } ?>" size="15" title="Cargo de Representante de Proveedor">
                        <?php if ($boton=='Modificar') { echo $car_rep_pro; } ?></td>
                      </tr>

                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$ncriterios =3; 
						$criterios[0] = "Rif"; 
						$campos[0] ="rif_pro";
						$criterios[1] = "Nombre";
						$campos[1] = "nom_pro";
						$criterios[2] = "Actividad";
						$campos[2] = "act_pro";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
