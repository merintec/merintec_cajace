<?php
	
	if (!$cedula){ $cedula = $_POST[cedula]; }
	if (!$porcien_apr){ $porcien_apr = $_POST[porcentaje]; }
	if (!$codg_nmna){ $codg_nmna = $_POST[nomina]; }
	////
	$variable = 'aporte_'.$cedula;			// nombre de la variable del monto almacenado en la nomina
	$variable2 = 'aporte_'.$cedula.'_nom';	// nombre de la variable del monto según sueldo
	$variable3 = 'aporte_'.$cedula.'_mod'; 	// Para mostrar u ocultar editar
	$variable4 = "'aporte".$cedula."'"; 	// nombre de la caja del monto
	$variable5 = '$ico_reg'.$cedula; // nombre de la valiable para completar el nombre del icono del registro
	$variable6 = '$ico_pag'.$cedula; // nombre de la valiable para completar el nombre del icono del pago
	$color = '#000000';
	////// monto del aporte según nómina
	$sueldo = $reg_pers["suel_dlab"];
	if ($sueldo==0){
		$sueldo = $_POST[sueldo];
	}
	$$variable2 = redondear(($sueldo*$porcien_apr),2,"",".");
	////// consultar si hay un monto almacenado para la nomina seleccionada 
	$sql_aporte = "SELECT nd.*, (if (nd.codg_pago > 0, 'on', 'off')) as pagado, 'on' as registrado FROM nominas_detalle nd WHERE nd.codg_nmna = ".$codg_nmna." AND nd.cedu_soci = ".$cedula." AND nd.moti_dlle = 'Aporte'";
	if ($res_aporte = mysql_fetch_array(mysql_query($sql_aporte))){
		$$variable = $res_aporte[mnto_dlle];
		if ($$variable!=$$variable2) { $color = '#FF0000'; }
		$$variable3 = "display: none;";
		$$variable5 = $res_aporte[registrado];
		$$variable6 = $res_aporte[pagado];
		$accion_registrado = "procesar('eliminar','".$codg_nmna."','".$res_aporte[codg_dlle]."', 'registrado', '".$cedula."', '".$sueldo."', 'aporte', '".$porcien_apr."', '', '', '".$res_aporte[codg_pago]."')";
		if ($$variable6=='on'){
			$accion_pagado = "procesar('eliminar','".$codg_nmna."','".$res_aporte[codg_dlle]."', 'pagado', '".$cedula."', '".$sueldo."', 'aporte', '".$porcien_apr."', '', '', '".$res_aporte[codg_pago]."')";			
		}
		else {
			$accion_pagado = "procesar('guardar','".$codg_nmna."','".$res_aporte[codg_dlle]."', 'pagado', '".$cedula."', '".$sueldo."', 'aporte', '".$porcien_apr."')";
		}
	}
	////// si no hay nada almacenado para la nomina mostramos el calculado en base a la nomina
	else {
		$$variable = $$variable2;
		$$variable5 = 'off';
		$$variable6 = 'off';
		$accion_pagado = "procesar('guardar','".$codg_nmna."','0', 'pagado', '".$cedula."', '".$sueldo."', 'aporte', '".$porcien_apr."')";
		$accion_registrado = "procesar('guardar','".$codg_nmna."','0', 'registrado', '".$cedula."', '".$sueldo."', 'aporte', '".$porcien_apr."')";
	}
	/// para verificar si es una nomina de la dependencia interna ()
	$sql_quien = "SELECT * FROM nominas n, valores v WHERE n.codg_nmna =".$codg_nmna." AND n.codg_depn = v.val_val AND v.des_val='DEP_INT'";
	if ($res_quien = mysql_fetch_array(mysql_query($sql_quien))){
		$nomina_interna = 'SI';
	}
	else { $nomina_interna = ''; }
	if ($$variable5=='on' && $nomina_interna != ''){
		$$variable6 = 'on';
		$accion_pagado = "procesar('eliminar','".$codg_nmna."','".$res_aporte[codg_dlle]."', 'registrado', '".$cedula."', '".$sueldo."', 'aporte', '".$porcien_apr."', '', '', '".$res_aporte[codg_pago]."')";
	}
	////// Mostrar en pantalla
	echo '<div style="height: 25px; padding-top:3px; text-align: right;" id="pago'.$res_aporte["codg_pago"].'">';
		echo '<font color="'.$color.'">'.$$variable.'</font>&nbsp;';
		echo '<input style="display: none; border:0px solid #fff; text-align:right; font-weight:bold; width:50px;" type="text" name="aporte'.$cedula.'" id="aporte'.$cedula.'" value="'.$$variable.'" size="5" readOnly>';
		echo '&nbsp;<img style="cursor: pointer;'.$$variable3.'" src="../imagenes/editar.png" width="14px" id="editar" name="editar" value="editar" onclick="quita_readOnly('.$variable4.')" title="Click para editar el monto">'; 
		echo '&nbsp;<img style="cursor: pointer;" src="../imagenes/registrado_'.$$variable5.'.png" width="16px" id="editar" name="editar" value="editar" onclick="'.$accion_registrado.'" title="Click para registrar el monto">';
		echo '&nbsp;<img style="cursor: pointer;" src="../imagenes/pagado_'.$$variable6.'.png" width="16px" id="editar" name="editar" value="editar" onclick="'.$accion_pagado.'" title="Click para pagar el monto">'; 
	echo '&nbsp;</div>';
?>
