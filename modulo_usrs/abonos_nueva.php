<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="../comunes/jquery.min.js"></script>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></link>
<script type="text/javascript" src="../comunes/calendar.js?"></script>
<script type="text/javascript">

function valida_envia(){
	if(document.form1.cedu_soci.selectedIndex==0){
		alert("Debe seleccionar un socio");
		document.form1.cedu_soci.focus();
		return 0;
	}
	if (document.form1.inter_ppp_h.value.length==0 || document.form1.total_ppp_h.value.length==0){
		alert("Debe primero indicar los meses de deuda y luego seleccionar un prestamo a pagar");
		document.form1.mess_deud1.focus();
		return 0;
	}
	if (document.form1.fcha_pago.value.length==0){
		alert("Debe colocar la fecha en que se realizo el pago");
		document.form1.fcha_pago.focus();
		return 0;
	}
	if (document.form1.mont_pago.value.length==0){
		alert("Debe colocar el monto que fue pagado");
		document.form1.mont_pago.focus();
		return 0;
	}
	if (document.form1.mont_pago.value <= 0){
		alert("Debe colocar un monto pagado mayor a cero");
		document.form1.mont_pago.focus();
		return 0;
	}
	if (document.form1.mont_pago.value > document.form1.total_ppp_h.value && document.form1.nmro_egre.value == ''){
		if(document.form1.exce_pago.value.length==0){
			alert("El monto pagado es mayor al total, debe elegir destino del excedente");
			document.form1.mont_pago.focus();
			return 0;                                                           
		}
	}
	if (document.form1.numr_refe.value.length==0 && (document.form1.nmro_egre.value.length==0 || document.form1.fcha_egre.value.length==0)){
		alert("Debe indicar el numero de referencia del pago");
		document.form1.numr_refe.focus();
		return 0;
	}
	if(document.form1.codg_cnta.selectedIndex==0 && (document.form1.nmro_egre.value.length==0 || document.form1.fcha_egre.value.length==0)){
		alert("Debe seleccionar la cuenta a la que se pago");
		document.form1.codg_cnta.focus();
		return 0;
	}

	document.form1.submit();
}
</script>

<?php 
include ('../comunes/formularios_funciones.php');
$existe = '';
$pagina = 'abonos_nueva.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$busqueda_varios=false;
if($_POST['boton']){ $boton=$_POST['boton']; }else{ $boton=$_GET['boton']; $busqueda=$_GET['busqueda']; }
$accion=$_POST['accion'];
if($_POST['codg_pago']){ $codg_pago=$_POST['codg_pago']; }else{ $codg_pago=$_GET['codg_pago']; }
$fcha_desd=$_POST['fcha_desd'];
$fcha_hast=$_POST['fcha_hast']; 
$fcha_regi=$_POST['fcha_regi'];
$cedu_soci=$_POST['cedu_soci']; 
$codg_prst=$_POST['codg_prst']; 
$capital_pagar=$_POST['capital_pagar']; 
$inter_ppp=$_POST['inter_ppp_h'];
$total_ppp=$_POST['total_ppp_h']; 
$fcha_pago=$_POST['fcha_pago']; 
$mont_pago=$_POST['mont_pago'];
$exce_pago=$_POST['exce_pago']; if(!$exce_pago){ $exce_pago='NULL'; }
if ($_POST['fcha_egre']!='' && $_POST[nmro_egre]!=''){
	$fcha_egre = "'".$_POST['fcha_egre']."'";
	$nmro_egre = "'".$_POST['nmro_egre']."'";
	$numr_refe = 'NULL';
	$codg_cnta = 'NULL';
}
else {
	if ($_POST['numr_refe']!='' && $_POST['codg_cnta']!=''){
		$fcha_egre = 'NULL';
		$nmro_egre = 'NULL';
		$numr_refe= "'".$_POST['numr_refe']."'"; 
		$codg_cnta= "'".$_POST['codg_cnta']."'"; 
	}
}
$obsr_pago=$_POST['obsr_pago']; 
$error=false;
if($boton=="Buscar" OR $boton=="Eliminar"){ $accion=""; }
if($accion=="Guardar"){
	mysql_query('AUTOCOMMIT=0');
	mysql_query('BEGIN');
	$mont_exce=$mont_pago-$total_ppp;
	if($mont_exce<0){ $mont_exce=0;}
	$capi_pago=$mont_pago-($inter_ppp+$mont_exce);
 	$sql_pres="INSERT INTO abonos(cedu_soci, codg_prst, fcha_regi, codg_cnta, numr_refe, fcha_pago, capi_pago, inte_pago, mont_pago, obsr_pago, exce_pago, fcha_egre, nmro_egre)
	VALUES ('$cedu_soci', '$codg_prst', '$fcha_regi', $codg_cnta, $numr_refe, '$fcha_pago', '$capi_pago', '$inter_ppp', '$mont_pago', '$obsr_pago', $exce_pago, $fcha_egre, $nmro_egre)";
	// echo $sql_pres; return;
	echo mysql_error();
	$nmro_egre = 'NULL';
	$numr_refe= "'".$_POST['numr_refe']."'"; 
	$codg_cnta= "'".$_POST['codg_cnta']."'"; 
	if(mysql_query($sql_pres)){
		$mont_pago_n = $capi_pago + $inter_ppp;
		$codg_pago=mysql_insert_id();
		$sql="insert into prestamos_mov(codg_prst, mnto_prtm, capi_prtm, inte_prtm, orgn_prtm, rela_prtm, freg_prtm, fpag_prtm) VALUES('$codg_prst', '$mont_pago_n', '$capi_pago', '$inter_ppp', 'Abono a prestamo', '$codg_pago', '$fcha_regi' , '$fcha_pago')";
		if(!mysql_query($sql)){
			$error=true;
		}
		if($exce_pago==1){
			$mont_exce=$mont_pago-$total_ppp;
			if($mont_exce>0){
				$sql="insert into socios_movimientos(cedu_soci, fcha_movi, conc_movi, tipo_movi, dest_movi, mont_movi, orig_movi, codg_dlle, codg_dlle_retr) VALUES('$cedu_soci', '$fcha_regi', 'Excedente por abono', 'I', 'I', $mont_exce, 'A' , '$codg_pago', 0)";
				if(!mysql_query($sql)){
					$error=true;
				}
			}
		}	
		if($exce_pago==2){
			$mont_exce=$mont_pago-$total_ppp;
			if($mont_exce>0){
				$val_apa = buscar_campo('val_val', 'valores', 'WHERE des_val="CNT_EPA"' );
				$sql="insert into cuentas_movimientos(codg_pcnta, fcha_cmov, conc_cmov, tipo_cmov, mont_cmov, orig_cmov, codg_dlle) VALUES('".$val_apa['val_val']."', '$fcha_regi', 'Excedente por abono', 'I', $mont_exce, 'A' , '$codg_pago')";
				if(!mysql_query($sql)){
					$error=true;
				}
			}
		}	
		if($capi_pago>=$capital_pagar){
			$sql="update prestamos set stat_prst='P' where codg_prst='$codg_prst'";
			if(!mysql_query($sql)){
				$error=true;
			}
		}
		if($error==true){ 
			mysql_query('ROLLBACK'); 
			echo '<script>alert("Los datos no pudieron ser guardados, vuelva a intentar la operacion");</script>';
			echo '<script>window.location="abonos_nueva.php"</script>';	
		}else{ 
			mysql_query('COMMIT'); 
			echo '<script>alert("Los datos del abono han sido Guardados con exito");</script>';
			echo '<script>window.location="abonos_nueva.php"</script>';
		}
	}else{
		mysql_query('ROLLBACK');
		echo '<script>alert("Los datos no pudieron ser guardados, vuelva a intentar la operacion. Es posible que est� utilizando un numero de dep�sito duplicado");</script>';
		//echo '<script>window.location="abonos_nueva.php"</script>';
	}
}
if ($fcha_egre == 'NULL' ) { $fcha_egre = '';}
if ($nmro_egre == 'NULL' ) { $nmro_egre = '';}
if ($numr_refe == 'NULL' ) { $numr_refe = '';}
if ($codg_cnta == 'NULL' ) { $codg_cnta = '';}

if($boton=="Buscar"){
	if($busqueda=="individual"){
		$sql="SELECT a.*, s.* FROM abonos a LEFT JOIN socios s ON s.cedu_soci=a.cedu_soci WHERE codg_pago='$codg_pago'";
	}else{
		$sql="SELECT a.*, s.* FROM abonos a LEFT JOIN socios s ON s.cedu_soci=a.cedu_soci WHERE fcha_regi>='$fcha_desd' AND fcha_regi<='$fcha_hast' order by fcha_regi desc";
		if($_POST['cedu_busq']){
			$sql="SELECT a.*, s.* FROM abonos a LEFT JOIN socios s ON s.cedu_soci=a.cedu_soci WHERE a.cedu_soci like '%".$_POST['cedu_busq']."%' order by fcha_regi desc";
		}			
	}
	
	$busq=mysql_query($sql);
	$reg=mysql_fetch_array($busq);
	mysql_num_rows($busq);
	if(mysql_num_rows($busq)>1){
		$busqueda_varios=true;
	}
	if(mysql_num_rows($busq)==0){
			echo '<script>alert("No existen resultados para la busqueda");</script>';
			
	}
	if(mysql_num_rows($busq)==1){
	
		$codg_pago=$reg['codg_pago']; 
		$fcha_regi=$reg['fcha_regi'];
		$cedu_soci=$reg['cedu_soci']; 
		$codg_prst=$reg['codg_prst']; 
		$capital_pagar=$reg['capital_pagar']; 
		$inter_ppp=$reg['inter_ppp'];
		$total_ppp=$reg['total_ppp']; 
		$fcha_pago=$reg['fcha_pago']; 
		$mont_pago=$reg['mont_pago'];
		$exce_pago=$reg['exce_pago'];
		$numr_refe=$reg['numr_refe']; 
		$codg_cnta=$reg['codg_cnta']; 
		$obsr_pago=$reg['obsr_pago']; 	
		$fcha_egre=$reg['fcha_egre']; 
		$nmro_egre=$reg['nmro_egre']; 	
	}
}

if($boton=="Eliminar"){
	$sql_abon='delete from abonos where codg_pago="'.$codg_pago.'"';
	mysql_query($sql_abon);
	$sql_abon='delete from prestamos_mov WHERE orgn_prtm="Abono a prestamo" AND rela_prtm="'.$codg_pago.'"';
	mysql_query($sql_abon);
	$sql_abon='delete from socios_movimientos WHERE conc_movi="Excedente por abono" AND codg_dlle="'.$codg_pago.'"';
	mysql_query($sql_abon);
	$sql_abon='delete from cuentas_movimientos WHERE conc_cmov="Excedente por abono" AND codg_dlle="'.$codg_pago.'"';
	mysql_query($sql_abon);
	echo '<script>alert("El registro fue eliminado con exito");</script>';
	echo '<script>window.location="abonos_nueva.php"</script>';
}
?>
<script>
	function asientos(accion,abono,codigo){
		if (accion == 'imprimir' ){
			window.open('../reportes/reporte_asientos.php?asiento2=' + codigo +'&status=ver','_blank','scrollbars=yes,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500');
		}
		if (accion == 'generar'){
			var parametros = {
	   			"codg_pago" : abono,
	   			"status": accion
	   		};
			$.ajax({
				data:  parametros,
			    url:   '../reportes/comprobante_asientos_abonos.php',
			    type:  'get',
			    beforeSend: function () {
		        	$("#asientos").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
		    	},
		    	success:  function (response) {
		    		$("#asientos").html(response);
		    		
		    	}
			});
		}
		if (accion == 'eliminar'){
			var parametros = {
	   			"codg_pago" : abono,
	   			"status": accion,
	   			"asiento": codigo
	   		};
			$.ajax({
				data:  parametros,
			    url:   '../reportes/comprobante_asientos_abonos.php',
			    type:  'get',
			    beforeSend: function () {
		        	$("#asientos").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
		    	},
		    	success:  function (response) {
		    		$("#asientos").html(response);
		    	}
			});
		}
	}
</script>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center"><?PHP if($busqueda_varios==false){ ?>
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td height="21" class="titulo"> Registro de Abonos </td>
                  </tr>
                  <tr>
                    <td width="550"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td class="etiquetas" width="26%">Buscar (Registro):</td>
                        <td>Desde:<?php escribir_campo('fcha_desd',$_POST["fcha_desd"],$fcha_desd,'',50,10,'Fecha desde donde se desea buscar','Verificar',$existe,'hoy','style="width:80px"','')?>&nbsp;hasta:<?php escribir_campo('fcha_hast',$_POST["fcha_hast"],$fcha_hast,'',50,10,'Fecha hasta donde se desea buscar','Verificar',$existe,'hoy','style="width:80px"','')?>                          </td>
                        <td width="10%" rowspan="2" valign="middle">
                          <input name="boton" id="boton" type="submit" value="Buscar" /></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Buscar (Cedula):</td>
                        <td><?php escribir_campo('cedu_busq','',$cedu_busq,'',50,10,'Buscar por cedula','Verificar',$cedu_busq,'','','')?></td>
                        </tr>
                      <tr>
                        <td colspan=3><hr></td>
                      </tr>
                      <tr>
                        <td width="26%" class="etiquetas">Fecha de Registro:<input name="codg_pago" id="codg_pago" type="hidden" value="<?PHP echo $codg_pago; ?>" /></td>
                        <td colspan="2"><?php escribir_campo('fcha_regi',$_POST["fcha_regi"],$fcha_regi,'',50,10,'Fecha de Registro del Pr�stamo','Verificar',$fcha_regi,'fecha_','','')?></td>
                      </tr>
                      <tr>
                        <td width="26%" class="etiquetas">Ahorrista&nbsp;Relacionado:
<script>
function prestamos_activos(cedu_soci){
        var parametros = {
                "cedu_soci" : cedu_soci,
				"tipo_accion" : 'abono'
        };
        $.ajax({
                data:  parametros,
                url:   'capa_prestamos_por_pagar.php',
                type:  'post',
                beforeSend: function () {
                        $("#prestamo_por_pagar").html("Procesando, espere por favor...");
                },
                success:  function (response) {	
                        $("#prestamo_por_pagar").html(response);
						actualizar_capa('ultimo_pago');
                }
        });
}
function ver_ultimo_pago(codg_prst,capital_pagar,inter_ppp,total_ppp){
        var parametros = {
                "codg_prst" : codg_prst,
				"capital_pagar" : capital_pagar,
				"inter_ppp" : inter_ppp,
				"total_ppp" : total_ppp
        };
        $.ajax({
                data:  parametros,
                url:   'capa_ultimo_pago_nomina.php',
                type:  'post',
                beforeSend: function () {
                        $("#ultimo_pago").html("Procesando, espere por favor...");
                },
                success:  function (response) {	
                        $("#ultimo_pago").html(response);
                }
        });
}
</script>						                         </td>
			            <td colspan="2"><?PHP if ($cedu_soci=='' && $_POST['cedu_soci']==''){ $vista='vista_socios_activos'; } else { $vista='vista_socios'; }  $param = "$('#cedu_soci').val()"; combo('cedu_soci', $cedu_soci, $vista, $link, 0, 0, 1, '', 'cedu_soci', 'href="javascript:;" onchange="prestamos_activos('.$param.');return false;"', $boton, "ORDER BY nomb_soci",''); ?>						</td>
                      </tr>
					  <?PHP if($boton!="Buscar"){ ?>
                      <tr>
					  	<td width="26%" class="etiquetas">Prestamos Activos:</td>
                        <td colspan="2" >&nbsp;<span id="prestamo_por_pagar"><?PHP include_once('capa_prestamos_por_pagar.php'); ?></span></td>
                      </tr>
                      <tr>
					  	<td width="26%" class="etiquetas">Ultimo pago:</td>
                        <td colspan="2" >&nbsp;<span id="ultimo_pago"><?PHP include_once('capa_ultimo_pago_nomina.php'); ?></span></td>
                      </tr>
					  <?PHP } ?>
					  <?PHP if(mysql_num_rows($busq)==1){ ?>
                      <tr>
                        <td colspan="3" class="etiquetas">
						<table width="100%" border="1" cellspacing="0" cellpadding="0" class="etiquetas">
						  <tr>
						    <td height="20" colspan="4" class="etiquetas_centradas">Detalle del prestamo y pago realizado</td>
						    </tr>
						  <tr>
							<td width="30%">Tipo&nbsp;de&nbsp;prestamo</td>
							<td width="28%">Capital&nbsp;pagado</td>
							<td width="18%">Intereses</td>
							<td width="15%">Total</td>
						  </tr>
							<?PHP 
								$sql_dpa="select pm.*, (select codg_tipo_pres from prestamos where codg_prst=pm.codg_prst) as codigo_tipo_pres, (select nomb_tipo_pres from tipo_prestamos where codg_tipo_pres=codigo_tipo_pres) as nomb_tipo_pres from prestamos_mov pm where orgn_prtm='Abono a prestamo' and rela_prtm=$codg_pago";
								$busq_dpa=mysql_query($sql_dpa);
								if($reg_dpa=mysql_fetch_array($busq_dpa)){ 							
							?>
							  <tr>
								<td><?PHP echo $reg_dpa['nomb_tipo_pres']; ?></td>
								<td><?PHP echo $reg_dpa['capi_prtm']; ?></td>
								<td><?PHP echo $reg_dpa['inte_prtm']; ?></td>
								<td><?PHP echo $reg_dpa['mnto_prtm']; ?></td>
							  </tr>
							<?PHP } ?>
						</table>						</td>
                      </tr>
					  <?PHP } ?>
                      <tr>
                        <td colspan="3" class="etiquetas"><hr></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="etiquetas_centradas">Detalles del pago</td>
                        </tr>
<script>
function actualizar_capa(contenedor){
	if(contenedor=='botonera'){
		var parametros = {
			"codg_pago" : $('#codg_pago').val(),
			"boton" : 'Buscar'
		};
		var url = 'abonos_botonera.php';
	}
	if(contenedor=='prestamos_por_pagar'){
		var parametros = {
			"cedu_soci" : $('#cedu_soci').val(),
			"tipo_accion" : 'abono'
		};
		var url = 'capa_prestamos_por_pagar.php';
	}
	if(contenedor=='ultimo_pago'){
        var parametros = {
                "codg_prst" : '',
				"capital_pagar" : '',
				"inter_ppp" : '',
				"total_ppp" : ''
        };
		var url = 'capa_ultimo_pago_nomina.php';
	}
        $.ajax({
                data:  parametros,
                url:   url,
                type:  'post',
                beforeSend: function () {
                        $('#' + contenedor).html("Procesando, espere por favor...");
                },
                success:  function (response) {	
                        $('#' + contenedor).html(response);
                }
        });
}
</script>			
					  <tr>
					  	<td colspan="2"><hr>Si el pago se efectu� mediante un comprobante de pago ya registrado:</td>
					  </tr>
					  <tr>
                        <td width="26%" class="etiquetas">Fecha&nbsp;del&nbsp;comprobante:</td>
                        <td colspan="2"><?php $add_fecha = 'onchange="valor_acampo(this.value,&#39;fcha_pago&#39;)"'; escribir_campo('fcha_egre',$_POST["fcha_egre"],$fcha_egre,'',50,10,'Fecha del comprobante con el que pag�','Verificar',$fcha_egre,'fecha',$add_fecha,'')?></td>
                      </tr>
                      <tr>
                        <td width="26%" class="etiquetas">N�mero de Comprobante:</td>
                        <td colspan="2"><input name="nmro_egre" id="nmro_egre" type="text" size="15" maxlength="11" value="<?PHP echo $nmro_egre; ?>" /></td>
                      </tr>
                      <tr>
					  	<td colspan="2"><hr></td>
					  </tr>	  
                      <tr>
                      <tr>
                        <td width="26%" class="etiquetas">Fecha&nbsp;de&nbsp;Pago:</td>
                        <td colspan="2"><?php escribir_campo('fcha_pago',$_POST["fcha_pago"],$fcha_pago,'',50,10,'Fecha de Registro del Pr&eacute;stamo','Verificar',$fcha_pago,'fecha','','')?></td>
                      </tr>
                      <tr>
                        <td width="26%" class="etiquetas">Monto Pagado:</td>
                        <td colspan="2"><input name="mont_pago" id="mont_pago" type="text" size="15" maxlength="11" value="<?PHP echo $mont_pago; ?>" /></td>
                      </tr>	  
                      <tr>
                        <td class="etiquetas">Si&nbsp;hay&nbsp;Excedente:</td>
                        <td colspan="2" class="etiquetas"><label>&nbsp;<input name="exce_pago" type="radio" <?PHP if($exce_pago=="1"){ echo 'checked="checked"'; } ?> value="1" />&nbsp;Ahorros&nbsp;de&nbsp;Socio</label>&nbsp;&nbsp;&nbsp;&nbsp;<label><input name="exce_pago" type="radio" <?PHP if($exce_pago=="2"){ echo 'checked="checked"'; } ?> value="2" />&nbsp;Otros&nbsp;Ingresos&nbsp;CAPREAMCE</label></td>
                      </tr>
                      <tr>
                        <td width="26%" class="etiquetas">N� de Referencia:</td>
                        <td colspan="2"><input name="numr_refe" id="numr_refe" type="text" size="15" maxlength="11" value="<?PHP echo $numr_refe; ?>" /></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Banco destino:</td>
                        <td colspan="2"><?PHP combo('codg_cnta', $codg_cnta, "banco_cuentas", $link, 0, 1, 2, '', 'codg_cnta', '', '', "ORDER BY bnco_cnta",''); ?></td>
                      </tr> 
                      <tr>
                        <td width="26%" class="etiquetas">Observaciones:</td>
                        <td colspan="2"><label>
                          <textarea name="obsr_pago" cols="30" rows="3"><?PHP echo $obsr_pago; ?></textarea>
                        </label></td>
                      </tr>
                      <tr>
                        <td colspan=3><hr></td>
                      </tr>
                      <tr>
                        <td colspan="3" align="center"><input name="accion" type="hidden" value="Guardar" />&nbsp;<input name="boton" id="boton" type="button" value="Guardar" <?PHP if($boton=="Buscar"){ echo 'disabled="disabled"'; } ?> onclick="valida_envia();" />&nbsp;
						  <span id="botonera"><?PHP include('abonos_botonera.php'); ?></span></td>
                      </tr>
					  <?PHP if($boton=="Buscar" and $codg_pago){ ?>
					  <tr>
						<td id="asientos" colspan="3" align="center">
							<?php 
								abrir_ventana('../reportes/recibo_abono.php','v_imprimir','Recibo de Abono',"codg_pago=".$codg_pago."&seccion=".$_GET['seccion']);
								$sql_asientos = "SELECT * FROM asientos WHERE orgn_asien = 'Comprobante Abono' AND codg_rela = ".$codg_pago;
								$bus_asientos = mysql_query($sql_asientos);
								if ($reg_asientos = mysql_fetch_array($bus_asientos)){				    			
									echo '<input type="hidden" id="asiento" name="asiento" value="'.$reg_asientos[codg_asien].'">';
									$parametros = "'eliminar','".$codg_pago."','".$reg_asientos[codg_asien]."'";
									echo '<input type="button" name="v_reporte" id="v_reporte" value="Eliminar Asiento ('.$reg_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
									$parametros = "'imprimir','".$codg_pago."','".$reg_asientos[codg_asien]."'";
									echo '<input type="button" name="v_reporte" id="v_reporte" value="Imprimir Asiento ('.$reg_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';

								}
								else{
									if ($nmro_egre==''){
										$parametros = "'generar','".$codg_pago."','0'";
										echo '<input type="button" name="v_reporte" id="v_reporte" value="Generar Asiento" onclick="asientos('.$parametros.')">';
									}
								}
							?>						</td>					  
					  </tr>
					  <?PHP } ?>
                    </table></td>
                  </tr>
                </table>
				  <?PHP
					}else{
				?>
                  <table width="550" border="0" cellspacing="0" cellpadding="0" class="etiquetas">
  <tr>
    <td colspan="6"  class="titulo">Resultados de la B&uacute;squeda</td>
  </tr>
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
  <tr>
    <td width="8%" height="20">Nro</td>
    <td width="14%">Fecha</td>
	<td width="15%">Cedula</td>
    <td width="37%">Socio</td>
    <td width="18%">Monto</td>
	<td width="8%">Accion</td>
  </tr>
  <tr>
    <td height="5" colspan="6"><hr></td>
  </tr>
  
  <?PHP $i=1; do{
  if ($linea==0) { $linea=1; } else { $linea = 0;}
   ?>
  <tr class='linea<?PHP echo $linea; ?>'>
    <td><?PHP echo $i; ?></td>
    <td><?PHP echo $reg['fcha_regi']; ?></td>
    <td><?PHP echo $reg['naci_soci']."-".$reg['cedu_soci']; ?></td>
	<td><?PHP echo $reg['nomb_soci']."&nbsp;".$reg['apel_soci']; ?></td>
    <td><?PHP echo $reg['mont_pago']; ?></td>
	<td><a href="abonos_nueva.php?<?PHP echo "codg_pago=".$reg['codg_pago']."&boton=Buscar&busqueda=individual"; ?>"><img src="../imagenes/buscar.png" width="24" height="24" border="0" /></a></td>
  </tr>
  <?PHP $i++; }while($reg=mysql_fetch_array($busq)); ?>
</table>				
				<?PHP
					}
				?>
                </div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
