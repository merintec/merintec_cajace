<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$codg_depn=$_POST['codg_depn'];
$codg_cnta=$_POST['codg_cnta'];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'dependencias_pagos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "dependencias_pagos";	// nombre de la tabla
$ncampos = "9";			//numero de campos del formulario
$datos[0] = crear_datos ("codg_depn","Codigo de la Dependencia",$_POST['codg_depn'],"1","11","numericos");
$datos[1] = crear_datos ("desc_pago","Descripcion del Pago",$_POST['desc_pago'],"1","50","alfanumericos");
$datos[2] = crear_datos ("codg_cnta","Cuenta Destino",$_POST['codg_cnta'],"1","11","numericos");
$datos[3] = crear_datos ("banc_orig","Banco Origen",$_POST['banc_orig'],"1","50","alfanumericos");
$datos[4] = crear_datos ("fcha_pago","Fecha de Pago",$_POST['fcha_pago'],"10","10","fecha");
$datos[5] = crear_datos ("numr_refe","Numero de Operacion",$_POST['numr_refe'],"1","11","numericos");
$datos[6] = crear_datos ("mont_pago","Monto del Pago",$_POST['mont_pago'],"1","50","decimal");
$datos[7] = crear_datos ("obsr_pago","Observaciones",$_POST['obsr_pago'],"0","255","alfanumericos");
$datos[8] = crear_datos ("retr_pago","Destinado a Retroactivo",$_POST['retr_pago'],"0","2","alfanumericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Fecha";
		$datos[0]="fcha_pago";
		$parametro[1]="Descripcion";
		$datos[1]="desc_pago";
		$parametro[2]="Referencia";
		$datos[2]="numr_refe";
		$parametro[3]="Monto";
		$datos[3]="mont_pago";
		$parametro[4]="Retroactivo";
		$datos[4]="retr_pago";
		busqueda_varios(7,$buscando,$datos,$parametro,"codg_pago");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_pago = $row["codg_pago"];
		$codg_depn = $row["codg_depn"];
		$desc_pago = $row["desc_pago"];
	    $codg_cnta = $row["codg_cnta"];
	    $banc_orig = $row["banc_orig"];
		$fcha_pago = $row["fcha_pago"];
	    $numr_refe = $row["numr_refe"];
	    $mont_pago = $row["mont_pago"];
	    $obsr_pago = $row["obsr_pago"];
	    $retr_pago = $row["retr_pago"];
	    
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_pago",$_POST["codg_pago"],$pagina,"");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_pago"],"codg_pago",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de los Pagos de las Dependencias</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Dependencia:<?php escribir_campo('codg_pago',$_POST["codg_pago"],$codg_pago,'',12,15,'Codigo del Pago',$boton,$existe,'','','oculto'); ?></td>
                        <td width="75%"><?php combo('codg_depn', $codg_depn, 'dependencias', $link, 0, 0, 1, '', 'codg_depn', " ", $boton, "ORDER BY nomb_depn ",''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Descripci&oacute;n:</td>
                        <td width="75%"><?php escribir_campo('desc_pago',$_POST["desc_pago"],$desc_pago,'',50,25,'Descripcion del pago',$boton,$existe,'','',''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Cuenta&nbsp;Destino:</td>
                        <td width="75%"><?php combo('codg_cnta', $codg_cnta, 'banco_cuentas', $link, 0, 0, 1, 2, 'codg_cnta', "", $boton, "",''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Banco&nbsp;Origen:</td>
                        <td width="75%"><?php escribir_campo('banc_orig',$_POST["banc_orig"],$banc_orig,'',50,25,'Banco Origen si es Cheque o Transferencia, de lo contrario colocar Deposito',$boton,$existe,'','',''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha&nbsp;de&nbsp;Pago:</td>
                        <td width="75%"><?php escribir_campo('fcha_pago',$_POST["fcha_pago"],$fcha_pago,'',10,10,'Fecha de registro del Pago',$boton,$existe,'fecha','',''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N�&nbsp;de&nbsp;Referencia:</td>
                        <td width="75%"><?php escribir_campo('numr_refe',$_POST["numr_refe"],$numr_refe,'',12,10,'Numero de Referencia',$boton,$existe,'','',''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto&nbsp;del&nbsp;Pago:</td>
						<td width="75%"><?php escribir_campo('mont_pago',$_POST["mont_pago"],$mont_pago,'',11,10,'Monto del Pago',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td width="75%"><?php echo escribir_area('obsr_pago',$_POST["obsr_pago"],$obsr_pago,'',30,3,'Observaciones adicionales sobre el Pago',$boton,$existe,''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Destinado para Retroactivo:</td>
                        <td width="75%"><input name="retr_pago" id="retr_pago" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$retr_pago.'" '; } else { echo 'type="checkbox" value="SI"'; } if($_POST["retr_pago"]=='SI' || $retr_pago == 'SI') { echo 'checked'; } ?> title="Destinado para Retroactivo">Retroactivo<?php if ($boton=='Modificar') { if ($retr_pago=='SI') {echo ': <b>SI</b>'; } else { echo ': <b>NO</b>'; } } ?> </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
					
                  <tr>
				  	<td>
					
					</td>
				  </tr>
				  <tr>
                    <td>
					<?php 
						$ncriterios =3;
						$criterios[0] = "Descripcion"; 
						$campos[0] ="desc_pago";
						$criterios[1] = "Fecha"; 
						$campos[1] ="fcha_pago";
						$criterios[2] = "Nro&nbsp;Referencia"; 
						$campos[2] ="numr_refe";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
