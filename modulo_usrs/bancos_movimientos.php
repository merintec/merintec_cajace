<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Movimientos Bancarios</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['codg_cnta'];
$tipo_movi = $_POST['tipo_movi'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'bancos_movimientos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"].'&codg_cnta='.$_GET["codg_cnta"];
$tabla = "banco_movimientos";	// nombre de la tabla
$ncampos = "8";			//numero de campos del formulario
$datos[0] = crear_datos ("codg_movi","Cod. de movimiento bancario ",$_POST['codg_movi'],"0","11","numericos");
$datos[1] = crear_datos ("codg_cnta","Banco",$_POST['codg_cnta'],"1","11","numericos");
$datos[2] = crear_datos ("fcha_movi","Fecha del Movimiento",$_POST['fcha_movi'],"10","10","fecha");
$datos[3] = crear_datos ("tipo_movi","Tipo de Movimiento",$_POST['tipo_movi'],"1","10","alfabeticos");
$datos[4] = crear_datos ("mont_movi","Monto del Movimiento",$_POST['mont_movi'],"1","12","decimal");
$datos[5] = crear_datos ("conc_movi","Concepto del Movimiento",$_POST['conc_movi'],"1","255","alfanumericos");
$datos[6] = crear_datos ("obsr_movi","Observaciones",$_POST['obsr_movi'],"0","255","alfanumericos");
$datos[7] = crear_datos ("refe_movi","Referencia",$_POST['refe_movi'],"1","50","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_movi = $row["codg_movi"];
	    $codg_cnta = $row["codg_cnta"];
	    $fcha_movi = $row["fcha_movi"];
	    $tipo_movi = $row["tipo_movi"];
	    $mont_movi = $row["mont_movi"];
	    $conc_movi = $row["conc_movi"];
	    $obsr_movi = $row["obsr_movi"]; 
	    $refe_movi = $row["refe_movi"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_movi",$_POST["codg_movi"],$pagina);
		auditoria_func ('modificar','',$_POST["ant"],$tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar',$ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_movi"],"codg_movi",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"codg_movi",$tabla,$pagina);
	return;
}
/// consultamos los datos del Banco
$sql_ban = "select * from banco_cuentas where codg_cnta =".$viene_val;
$bus_ban = mysql_query($sql_ban);
$res_ban = mysql_fetch_array($bus_ban);
$bnco_cnta=$res_ban['bnco_cnta'];
$nmro_cnta=$res_ban['nmro_cnta'];
$mnto_cnta=$res_ban['mnto_cnta'];
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Movimientos Bancarios</td>
                  </tr>
                  <tr>
							<td>
								<table width="100%">
                  			<tr align="center">
                  				<td class="etiquetas" width="33.33%">Banco</td>               		
                  				<td class="etiquetas" width="33.33%">Cuenta</td>		
                  				<td class="etiquetas" width="33.33%">Monto en Cuenta</td>
                  			</tr>
                  			<tr align="center">
                  				<td class="etiquetas"><?php echo $bnco_cnta; ?></td>               		
                  				<td class="etiquetas"><?php echo $nmro_cnta; ?></td>		
                  				<td class="etiquetas"><?php echo redondear($mnto_cnta,2,'.',','); ?></td>
                  			</tr>
                  		</table>
							</td>                  
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td class="etiquetas">Fecha:</td>
                        <td>
									<input name="codg_movi" type="hidden" id="codg_movi" value="<?php if(! $existe) { echo $_POST["codg_movi"]; } else { echo $codg_movi; } ?>" title="Codigo del Movimiento bancario">
									<input name="codg_cnta" type="hidden" id="codg_cnta" value="<?php if(! $existe) { echo $viene_val; } else { echo $codg_cnta; } ?>" title="Codigo de Banco">
                        	<?php escribir_campo('fcha_movi',$_POST["fcha_movi"],$fcha_movi,'',10,10,'Fecha del Movimiento Bancario',$boton,$existe,'fecha','',''); ?>
								</td>
                      </tr>
                      <tr>
                      	<td class="etiquetas">Tipo Movimiento:</td>
                        <td>
                            <?php if ($boton != "Modificar" && $boton != "Guardar") { echo '<select name="tipo_movi" title="Tipo de Movimiento">
                          <option>Seleccione...</option>
                          <option value="E" '; if ($tipo_movi == "E" || $_POST['tipo_movi'] =="E") { echo 'selected'; } echo '>Entrada</option>
                          <option value="S" '; if ($tipo_movi == "S" || $_POST['tipo_movi'] =="S") { echo 'selected'; } echo '>Salida</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tipo_movi" id="tipo_movi" value="'.$tipo_movi.'" >'; 
						    if ($tipo_movi == "E") { echo 'Entrada'; } 
							if ($tipo_movi == "S") { echo 'Salida'; }
						}?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Monto:</td>
                        <td width="75%">
                        	<?php escribir_campo('mont_movi',$_POST["mont_movi"],$mont_movi,'',12,12,'Monto del Movimiento Bancario',$boton,$existe,'','',''); ?>
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Referencia:</td>
                        <td width="75%">
                        	<?php escribir_campo('refe_movi',$_POST["refe_movi"],$refe_movi,'',50,12,'Referencia del Movimiento Bancario',$boton,$existe,'','',''); ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Concepto:</td>
                        <td width="75%"><?php echo escribir_area('conc_movi',$_POST["conc_movi"],$conc_movi,'',30,3,'Motivo por el cual se efect�a este movimiento',$boton,$existe,'')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td width="75%"><?php echo escribir_area('obsr_movi',$_POST["obsr_movi"],$obsr_movi,'',30,3,'Observaciones adicionales para el movimiento',$boton,$existe,'')?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('bancos_movimientos_capa.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
