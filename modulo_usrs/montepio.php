<?php include('../comunes/conexion_basedatos.php'); 
$tabla = "montepio";  // nombre de la tabla 
$ncampos = "15";      //numero de campos del formulario ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$cedu_soci=$_POST['cedu_soci'];
$tipo_mpio=$_POST['tipo_mpio'];
$codg_pcuen=$_POST['codg_pcuen'];
$codg_pcnta=$_POST['codg_pcnta'];
$datos[0] = crear_datos ("fcha_mpio","Fecha de Registro del MONTEPIO",$_POST['fcha_mpio'],"10","10","fecha");
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'montepio.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$datos[1] = crear_datos ("cedu_soci","Ahorrista relacionado al MONTEPIO",$_POST['cedu_soci'],"1","12","numericos");
$datos[2] = crear_datos ("tipo_mpio","Tipo de MONTEPIO",$_POST['tipo_mpio'],"1","1","alfabeticos");
$datos[3] = crear_datos ("anum_mpio","Num. Acta de Defunci�n",$_POST['anum_mpio'],"1","11","numericos");
$datos[4] = crear_datos ("afch_mpio","Fecha del Acta de Defunci�n",$_POST['afch_mpio'],"10","10","fecha");
$datos[5] = crear_datos ("aemi_mpio","Instituci�n que emite el Acta de Defunci�n",$_POST['aemi_mpio'],"1","100","alfanumericos");
$datos[6] = crear_datos ("aest_mpio","Estado en que se emite el Acta de Defunci�n",$_POST['aest_mpio'],"1","100","alfabeticos");
$datos[7] = crear_datos ("amun_mpio","Municipio en que se emite el Acta de Defunci�n",$_POST['amun_mpio'],"1","100","alfabeticos");
$datos[8] = crear_datos ("apar_mpio","Parroquia en que se emite el Acta de Defunci�n",$_POST['apar_mpio'],"1","100","alfabeticos");
$datos[9] = crear_datos ("obsr_mpio","Observaciones adicionales sobre el MONTEPIO",$_POST['obsr_mpio'],"0","255","alfanumericos");
$datos[10] = crear_datos ("nofi_mpio","Inicio de N� de Oficio:",$_POST['nofi_mpio'],"1","4","numericos");
$datos[11] = crear_datos ("codg_benf","Familiar",$_POST['codg_benf'],"0","12","alfanumericos");
$datos[12] = crear_datos ("codg_pcuen","Plan de cuentas:",$_POST['codg_pcuen'],"0","11","numericos");
$datos[13] = crear_datos ("codg_pcnta","Cuenta:",$_POST['codg_pcnta'],"0","11","numericos");
$datos[14] = crear_datos ("mont_mpio","Monto:",$_POST['mont_mpio'],"0","11","decimal");
if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="fecha";
		$datos[0]="fcha_mpio";
		$parametro[1]="Ahorrista";
		$datos[1]="cedu_soci";
		$parametro[2]="Acta N�";
		$datos[2]="anum_mpio";
		busqueda_varios(5,$buscando,$datos,$parametro,"codg_mpio");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_mpio = $row["codg_mpio"];
	    $fcha_mpio = $row["fcha_mpio"];
	    $cedu_soci = $row["cedu_soci"];
	    $tipo_mpio = $row["tipo_mpio"];
	    $anum_mpio = $row["anum_mpio"];
	    $afch_mpio = $row["afch_mpio"];
	    $aemi_mpio = $row["aemi_mpio"];
	    $aest_mpio = $row["aest_mpio"];
	    $amun_mpio = $row["amun_mpio"];
	    $apar_mpio = $row["apar_mpio"];
	    $obsr_mpio = $row["obsr_mpio"];
	    $nofi_mpio = $row["nofi_mpio"];
      $codg_benf = $row["codg_benf"];
      $codg_pcuen= $row["codg_pcuen"];
      $codg_pcnta = $row["codg_pcnta"];
      $mont_mpio = $row["mont_mpio"];
	    if ($codg_benf==''){ $codg_benf='NULL'; }
	    
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}

if ($_POST["cedu_soci"]=='' && $_POST["tipo_mpio"]=='F' && ($_POST["bton_ante"]=="Verificar" || $_POST["bton_ante"]=="Actualizar" )) 
{
    echo "<script>alert('Debe seleccionar un ahorrista antes de continuar');</script>";
    $boton = $_POST["confirmar"];
    $_POST["confirmar"]='';
    $_POST["codg_benf"]='NULL';
    $_POST["tipo_mpio"]='';
    $tipo_mpio='';
    $beneficiario='';
}
if ($_POST["codg_benf"]=='NULL' && $_POST["tipo_mpio"]=='F' && ($_POST["confirmar"]=="Verificar" || $_POST["confirmar"]=="Actualizar" )) 
{
    echo "<script>alert('Debe seleccionar un Familiar');</script>";
    $boton = $_POST["confirmar"];
    $_POST["confirmar"]='';
} 
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_mpio",$_POST["codg_mpio"],$pagina,"");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
  $monto_mpio = 0;
	$montepio = insertar_func_return($ncampos,$datos,$tabla);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_mpio"],"codg_mpio",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["bton_ante"]!="") 
{
	$boton = $_POST["bton_ante"];
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo"> Registro de MONTEPIO </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Fecha de Registro:<?php escribir_campo('codg_mpio',$_POST["codg_mpio"],$codg_mpio,'',12,15,'Codigo del parentesco',$boton,$existe,'','','oculto')?></td>
                        <td width="75%"><?php escribir_campo('fcha_mpio',$_POST["fcha_mpio"],$fcha_mpio,'',50,10,'Fecha de Registro del MONTEPIO',$boton,$existe,'fecha','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Ahorrista&nbsp;Relacionado:                          </td>
			            <td>
                        <?php 
                        $func_cambiar_socio = "OnChange=valor_acampo('".$boton."','bton_ante');valor_acampo('NULL','codg_benf');submit();"; 
                        if ($cedu_soci=='' && $_POST['cedu_soci']==''){ $vista='vista_socios_activos'; } else { $vista='vista_socios'; } combo('cedu_soci', $cedu_soci, $vista, $link, 0, 0, 1, '', 'cedu_soci', $func_cambiar_socio, $boton, "ORDER BY nomb_soci",''); ?></td>
                      </tr>
                      <tr>
                      	<td class="etiquetas">Tipo de MONTEPIO:</td>
                        <td>
                            <?php 
                            $func_cambiar_tipo = "OnChange=valor_acampo('".$boton."','bton_ante');valor_acampo('NULL','codg_benf');submit();"; 
 						  if ($boton != "Modificar" && $boton != "Guardar") { echo '<select name="tipo_mpio" title="Tipo de MONTEPIO" '.$func_cambiar_tipo.' '; echo '>
                          <option>Seleccione...</option>
                          <option value="S" '; if ($tipo_mpio == "S" || $_POST['tipo_mpio'] =="S") { echo 'selected'; } echo '>Fallecimiento del Afiliado</option>
                          <option value="F" '; if ($tipo_mpio == "F" || $_POST['tipo_mpio'] =="F") { echo 'selected'; } echo '>Fallecimiento de un Familiar</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tipo_mpio" id="tipo_mpio" value="'.$tipo_mpio.'" >'; 
						    if ($tipo_mpio == "S") { echo 'Fallecimiento del Afiliado'; } 
							if ($tipo_mpio == "F") { echo 'Fallecimiento de un Familiar'; }
						}?>
						<?php escribir_campo('codg_benf',$_POST["codg_benf"],$codg_benf,'',11,50,'C�digo del familiar',$boton,$existe,'',' readonly ','oculto')?>
                        <input type="hidden" name="bton_ante" id="bton_ante" value="">
						</td>
                      </tr>
                      <?php 
                      //// si Fallece un familiar del socio se buscan los beneficiarios que tengan porcentaje mayor a 0
                        if($_POST["tipo_mpio"]=='F' && $_POST["cedu_soci"]!='' && $boton!='Modificar'){
                            $sql_benef = "SELECT * FROM socios s, socios_beneficiarios sf, parentescos par WHERE s.cedu_soci=".$_POST[cedu_soci]." AND s.cedu_soci=sf.cedu_soci AND sf.codg_pare=par.codg_pare ";
                            $res_benef = mysql_query($sql_benef);
                            echo '<tr><td>&nbsp;</td><td class="etiquetas">Seleccione el Familiar:</td></tr>';
                            echo '<tr><td>&nbsp;</td><td><table cellpadding="0" cellspacing="0" border="1" width="100%">';
                            echo '<tr class="etiquetas" align="center"><td>Sel.</td><td>Apellidos y Nombres</td></tr>';
                            while ($reg_benef=mysql_fetch_array($res_benef)){
                                $func_cambiar = ";valor_acampo('".$reg_benef['codg_benf']."','codg_benf');";     
                                echo '<tr>
                                    <td align="center"><input type="radio" name="benficiario" '; if ($_POST["codg_benf"]==$reg_benef["codg_benf"]){ echo 'checked'; } echo ' value="'.$reg_mpio["codg_mpio"].'" OnClick="'.$func_cambiar.'" title="Haga Click para seleccionar"></td>
                                    <td>&nbsp;'.$reg_benef["apel_benf"].'&nbsp;'.$reg_benef["nomb_benf"].'</td>
                                    </tr>';
                            }
                            echo '</table></td></tr>';
                        }
                        ?>
                      <tr>
                        <td width="25%" class="etiquetas">N� de Acta Defuncion:</td>
                        <td width="75%"><?php escribir_campo('anum_mpio',$_POST["anum_mpio"],$anum_mpio,'',11,10,'N�mero del Acta de Defunci�n',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha&nbsp;de&nbsp;Acta&nbsp;Defuncion:</td>
                        <td width="75%"><?php escribir_campo('afch_mpio',$_POST["afch_mpio"],$afch_mpio,'',50,10,'Fecha de Acta Defuncion',$boton,$existe,'fecha','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Instituci�n&nbsp;que&nbsp;emite&nbsp;el&nbsp;Acta:</td>
                        <td width="75%"><?php escribir_campo('aemi_mpio',$_POST["aemi_mpio"],$aemi_mpio,'',100,37,'Instituci�n que emite el Acta de defunci�n',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Estado:</td>
                        <td width="75%"><?php escribir_campo('aest_mpio',$_POST["aest_mpio"],$aest_mpio,'',100,37,'Estado o Entidad en el que se emite el Acta de defunci�n',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Municipio:</td>
                        <td width="75%"><?php escribir_campo('amun_mpio',$_POST["amun_mpio"],$amun_mpio,'',100,37,'Municipio en el que se emite el Acta de defunci�n',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Parroquia:</td>
                        <td width="75%"><?php escribir_campo('apar_mpio',$_POST["apar_mpio"],$apar_mpio,'',100,37,'Parroquia en el que se emite el Acta de defunci�n',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td width="75%"><?php echo escribir_area('obsr_mpio',$_POST["obsr_mpio"],$obsr_mpio,'',30,3,'Observaciones adicionales sobre el MONTEPIO',$boton,$existe,'')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Inicio de N� de Oficio:</td>
                        <td width="75%"><?php escribir_campo('nofi_mpio',$_POST["nofi_mpio"],$nofi_mpio,'',4,10,'N�mero de Oficio en el que inicia la impresi�n',$boton,$existe,'','','')?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Plan de cuenta:</td>
                        <td width="75%">
                          <?php 
                            combo('codg_pcuen', $codg_pcuen, 'plan_cuentas', $link, 0, 0, 1, '', 'codg_pcuen', '', $boton, "WHERE stat_pcuen='Activo'",''); 
                          ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Pagar con Cuenta:</td>
                        <td width="75%">
                        <?php 
                          $sql_valores = " WHERE v.val_val=c.codg_pcnta AND ("; 
                          for ($i=1;$i<=12;$i++){
                            $sql_valores .= "v.des_val = 'CNT_MP".$i."' OR ";
                          }
                          $sql_valores .= ',,';
                          $sql_valores = str_replace(' OR ,,', '', $sql_valores);
                          $sql_valores .= " OR v.des_val = 'CNT_TERCEROS')";
                          if ($codg_pcnta || $_POST[codg_pcnta]){
                            $sql_valores .= " AND (c.codg_pcnta NOT IN (SELECT codg_pcnta FROM montepio) OR c.codg_pcnta=".$codg_pcnta.") ";
                          }else{
                            $sql_valores .= " AND c.codg_pcnta NOT IN (SELECT codg_pcnta FROM montepio)";
                          }
                          combo('codg_pcnta', $codg_pcnta, 'valores v, cuentas c', $link, 4, 5, 6, '', 'codg_pcnta', '', $boton, $sql_valores,''); 
                          
                          // consultamos los datos del montepio a pagar
                          $sql_mpio = "SELECT * FROM montepio WHERE codg_mpio=".$codg_mpio;
                          $reg_mpio = mysql_fetch_array(mysql_query($sql_mpio));
                          // consultamos el monto total a pagar por este montepio
                          if ($reg_mpio[mont_mpio]>0){
                            $monto_pagar = $reg_mpio[mont_mpio];
                          }
                          else{
                            $sql_moti = "SELECT SUM(haber_movi) as mont_pagar from movimientos_contables WHERE codg_pcnta = ".$reg_mpio[codg_pcnta];
                            $res_moti = mysql_fetch_array(mysql_query($sql_moti));
                            $monto_pagar = $res_moti['mont_pagar'];
                          }
                          // si es por muerte del asociado consultar el porcentaje que corresponde al beneficiario seleccionado
                          if ($reg_mpio['tipo_mpio']=='S'){
                                    $sql_por_benef = "SELECT * FROM socios_beneficiarios where cedu_soci=".$reg_mpio['cedu_soci']." AND cedu_benf='".$codg_rela."' AND CONCAT(apel_benf, ' ' ,nomb_benf)='".$nomb_rela."'";
                              $res_por_benef = mysql_fetch_array(mysql_query($sql_por_benef));
                              $monto_pagar = redondear(($monto_pagar*$res_por_benef['porc_benf']/100),2,'','.');          
                          }
                          //** consultamos los montos que ya se han pagado al beneficiario seleccionado
                          $sql_pagado = "SELECT SUM(mnto_debe) as monto FROM egresos e, egresos_conceptos ec WHERE e.moti_egre='MONTEPIO' AND e.codg_mpio=".$codg_mpio." AND e.codg_egre=ec.codg_egre GROUP BY e.codg_mpio,codg_rela";
                          $res_pagado = mysql_fetch_array(mysql_query($sql_pagado));
                          // restamos el monto que ya se ha pagado
                          $monto_pagar = $monto_pagar - $res_pagado['monto'];
                          echo '<font color="#FF0000"><br><b>'.redondear($monto_pagar,2,'.',',')."</b><br>";
                          ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto:</td>
                        <td width="75%">
                          <?php escribir_campo('mont_mpio',$_POST["mont_mpio"],$mont_mpio,'',4,10,'Si deseas puedes asignar un monto',$boton,$existe,'','','');
                            echo '<br>Si el monto ya no est� disponible mediante una cuenta. <br>Utilize la cuenta de fondo de terceros y asigne un monto.';
                          ?>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
                  <tr>
                    <td align="center">
                    		<?php if($boton=='Modificar'){ abrir_ventana('../reportes/montepio_oficios.php','v_imprimir','Imprimir Oficios',"codg_mpio=".$codg_mpio."&seccion=".$_GET['seccion']); } ?>
                    </td>
                  </tr>
				  <tr>
					<td>
					<?php 
						$ncriterios =2;
						$criterios[0] = "Fecha"; 
						$campos[0] ="fcha_mpio";
						$criterios[1] = "C�dula"; 
						$campos[1] ="cedu_soci";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     $funcion_combo = '"criterio1.checked=true; valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
                                             echo '<center>Buscar C�dula: '; 
                                             combo('cedu_soci2', $cedu_soci, 'vista_socios', $link, 0, 0, 1, '', 'cedu_soci', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nomb_soci",'');
                                             echo '</center>'; 
				   	   }  ?>
				   	   </td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
