<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<script type='text/javascript' src='../comunes/jquery.min.js'></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$prm[1]='';
$boton = "Verificar";
$existe = '';
$pagina = 'dividendos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "dividendos";  // nombre de la tabla
$ncampos = "4";     //numero de campos del formulario

$ano_divi=$_POST['ano_divi'];
$ano_dividendo=$_POST['ano_dividendo'];

$datos[0] = crear_datos ("fecha_divi","Fecha del dividendo",$_POST['fecha_divi'],"10","10","fecha");
$datos[1] = crear_datos ("ano_divi","Año de dividendos",$_POST['ano_divi'],"1","255","numericos");
$datos[2] = crear_datos ("detalle_divi","Detalle de dividendos",$_POST['detalle_divi'],"1","255","alfanumericos");
$datos[3] = crear_datos ("mont_divi","Monto de dividendos",$_POST['mont_divi'],"1","255","decimal");


$tabla2 = "egresos";  // nombre de la tabla
$ncampos2 = "14";     //numero de campos del formulario

 
$motivo_egreso='Dividendo';
$nombre_relacion='Nombre dividendos';
 
$nmro_cheq=0;
$dedu_egre=0;
$obsr_egre='Pago de dividendos ano '.$_POST['ano_divi'].' Con un monto '.$_POST['mont_divi'].'Bs.';
$x=$_POST['codg_cnta'];
$datos2[0] = crear_datos ("fcha_egre","Fecha del dividendo",$_POST['fecha_divi'],"10","10","fecha");
$datos2[1] = crear_datos ("moti_egre","Motivo egreso",$motivo_egreso,"1","255","alfanumericos");
$datos2[2] = crear_datos ("nomb_rela","Nombre relacion",$nombre_relacion,"1","255","alfanumericos");
$datos2[3] = crear_datos ("codg_cnta","Cuenta Bancaria",$x,"1","255","numericos");
$datos2[4] = crear_datos ("nmro_cheq","Numero Cheque",$nmro_cheq,"1","255","alfanumericos");
$datos2[5] = crear_datos ("elab_egre","Elaborado",$_POST['elab_egre'],"1","255","alfanumericos");
$datos2[6] = crear_datos ("revi_egre","Revisado",$_POST['revi_egre'],"1","255","alfanumericos");
$datos2[7] = crear_datos ("autr_egre","Autorizado",$_POST['autr_egre'],"1","255","alfanumericos");
$datos2[8] = crear_datos ("cntb_egre","Contabilizado",$_POST['cntb_egre'],"1","255","alfanumericos");
$datos2[9] = crear_datos ("dedu_egre","Deducciones",$dedu_egre,"1","255","numericos");
$datos2[10] = crear_datos ("obsr_egre","Observaciones",$obsr_egre,"1","255","alfanumericos");
$datos2[11] = crear_datos ("ano_divi","Año dividendo",$_POST['ano_divi'],"1","255","alfanumericos");


if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
  if ($_POST["Buscar"]) { $tipo = "general"; }
  if ($_POST["BuscarInd"]) { $tipo = "individual"; }
  $buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
  if (mysql_num_rows($buscando) > 1)
  {
    include ('../comunes/busqueda_varios.php');
    $parametro[0]="A&ntilde;o";
    $datos[0]="ano_divi";
    $parametro[1]="Descripci&oacute;n";
    $datos[1]="detalle_divi";
    $parametro[2]="Fecha";
    $datos[2]="fecha_divi";
    $parametro[3]="Monto";
    $datos[3]="mont_divi";

    busqueda_varios(8,$buscando,$datos,$parametro,"id_divi");
    return;
  }
  while ($row=@mysql_fetch_array($buscando))
  {
      $existe = 'SI';
      $id_divi = $row["id_divi"];
      $mont_divi = $row["mont_divi"];
      $fecha_divi=$row["fecha_divi"];
      $detalle_divi = $row["detalle_divi"];
      $ano_divi = $row["ano_divi"];
      $boton = "Modificar";
      // No modificar, datos necesarios para auditoria
      $n_ant = mysql_num_fields($buscando);
      for ($i = 0; $i < $n_ant; $i++) 
      { 
          $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
      }
      ///
  }
}
if ($_POST["confirmar"]=="Actualizar") 
{
  $validacion = validando_campos ($ncampos,$datos);
  

  if ($validacion) 
  {
       modificar_func($ncampos,$datos,$tabla,"id_divi",$_POST["id_divi"],$pagina,"");
      auditoria_func ('modificar', '', $_POST["ant"], $tabla);
      return;    

  }
    
    
  else{
    $boton = "Actualizar";
  }
}


if ($_POST["confirmar"]=="Modificar") 
{
  $boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
  $validacion = validando_campos ($ncampos,$datos);
  if ($validacion) { $boton = "Guardar"; }
  

}
if ($_POST["confirmar"]=="Guardar") 
{

    $consulta=mysql_query("SELECT * FROM dividendos WHERE ano_divi='$_POST[ano_divi]'");
    $con_con=mysql_fetch_assoc($consulta);



    if ($con_con[id_divi]==NULL)
    {

      insertar_func($ncampos,$datos,$tabla,$pagina);
      //consultar el ultimo dividendo para poder guardar la relacion en egreso

      $consulta_dividendos=mysql_query("SELECT MAX(id_divi) as id_divi FROM dividendos ");
      $con_div2=mysql_fetch_assoc($consulta_dividendos);
      $consulta_egresos=mysql_query("Select nmro_egre from egresos where codg_egre in (SELECT MAX(codg_egre) FROM egresos) ");
      $con_egre2=mysql_fetch_assoc($consulta_egresos);
      $numero_egre=intval($con_egre2['nmro_egre'])+1;
      //echo $con_div2['id_divi'];

      $datos2[12] = crear_datos ("nmro_egre","Numero egreso",$numero_egre,"1","255","numericos");
      $datos2[13] = crear_datos ("codg_rela","Codigo de relacion",$con_div2[id_divi],"1","255","alfanumericos");
      insertar_func($ncampos2,$datos2,$tabla2,$pagina);
      $consulta_cuenta2=mysql_query("Select * from valores, cuentas where valores.des_val='CNT_DIV' and cuentas.codg_pcnta=valores.val_val");
      $con_cue2=mysql_fetch_assoc($consulta_cuenta2);

      $consulta_egresos2=mysql_query("SELECT MAX(codg_egre) as codg_egre FROM egresos ");
      $con_egre22=mysql_fetch_assoc($consulta_egresos2);

      $conc_egre='dividendos ano '.$_POST['ano_divi'];

      $monto=$_POST['mont_divi'];

      $insertar_egresos_conceptos_haber=mysql_query("INSERT into egresos_conceptos (codg_egre, codg_ctab, conc_egre, mnto_debe,mnto_hber) VALUES ('$con_egre22[codg_egre]','$con_cue[nmro_cnta]','$conc_egre','0','$monto')");
      $insertar_egresos_conceptos_debe=mysql_query("INSERT into egresos_conceptos (codg_egre, codg_ctab, conc_egre, mnto_debe,mnto_hber) VALUES ('$con_egre22[codg_egre]','$con_cue2[nmro_cnta]','$conc_egre','$monto','0')");
     

      auditoria_func ('insertar', $ncampos, $datos, $tabla);
      return;
    }
    else
    {
      ?>
      <script type="text/javascript">
          alert("Error... el dividendo ya existe ");
      </script>

      <?php 
    }
}
if ($_POST["confirmar"]=="Eliminar") 
{
  //consultar si hay un asiento
 $consultaa=mysql_query("SELECT * FROM asientos where orgn_asien='Comprobante Pago' and  codg_rela='$_POST[id_divi]'");
  $cona=mysql_fetch_array($consultaa);

  echo "SELECT * FROM asientos where orgn_asien='Comprobante Pago' and  codg_rela='$_POST[id_divi]'";

  echo $cona[codg_asien]; 
  if ($cona[codg_asien]==NULL)
  {
        eliminar_func($_POST["id_divi"],"id_divi",$tabla,$pagina);

        $eliminar_detalle_dividendos=mysql_query("DELETE from detalle_dividendos where id_divi='$_POST[id_divi]'");
        
        $consultae=mysql_query("SELECT * FROM egresos where codg_rela='$_POST[id_divi]'");
         $cone=mysql_fetch_array($consultae);
        $codg_egre=$cone['codg_egre'];
        echo "codigo:".$codg_egre;

        $eliminar_egresos_conceptos=mysql_query("DELETE from egresos_conceptos where codg_egre='$codg_egre' ");
        echo "DELETE from egresos_conceptos where codg_egre='$codg_egre'";

        $eliminar_egreso=mysql_query("DELETE from egresos where codg_rela='$_POST[id_divi]' ");
        echo "DELETE from egresos where codg_rela='$_POST[id_divi]' ";

        $elimanr_cheque=mysql_query("DELETE from dividendo_cheques where id_egreso='$codg_egre' ");
        echo "DELETE from dividendo_cheques where id_egreso='$codg_egre'";

        auditoria_func ('eliminar', $ncampos, $datos, $tabla);  
 }
   else
  {
    ?>
      <script type="text/javascript">
          alert("Error... el dividendo no puede ser eliminado, tiene un asiento contable");
      </script>

      <?php 

  }
  return;
}


//aca empieza la otra parte

$consulta_dividendos=mysql_query("SELECT * FROM dividendos ");

$boton2=$_POST['boton2'];
$ca=$_POST['ca'];
$id_divi1=$_POST['id_divi1'];
//almacenar todas las tuplas correspondientes a los dividendos de los ahorritas


if ($boton2)
{

    for ($j=1; $j<=$ca;$j++)
    {


      $cedu_soci[$j]=$_POST['cedu_soci'.$j];
      $id_divi[$j]=$_POST['id_divi'.$j];
      $mont_det_div[$j]=$_POST['mont_det_div'.$j];
  
      $insertar=mysql_query("INSERT INTO detalle_dividendos (id_divi,mont_det_div,cedu_soci) VALUES ('$id_divi[$j]','$mont_det_div[$j]','$cedu_soci[$j]') ");
      
    }

     $Actualizar=mysql_query("UPDATE dividendos SET status_divi='pagado' where id_divi='$id_divi1' ");

    ?>


      <script type="text/javascript">
       alert("Los datos se han procesado con exito...");


      </script>
      <?php 
    


}











?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Dividendos</td>
                  </tr>

                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                    <tr>
                     <td width="25%" class="etiquetas">Fecha:</td>
                     <td width="75%"><?php 
                     escribir_campo('id_divi',$_POST["id_divi"],$id_divi,'',12,15,'Codigo del dividendo',$boton,$existe,'','','oculto')?>

                     <?php escribir_campo('fecha_divi',$_POST["fecha_divi"],$fecha_divi,'',50,10,'Fecha del Dividendo',$boton,$existe,'fecha','','')?></td>
                   </tr>

                   <tr>


                        <td width="25%" class="etiquetas">A&ntilde;o del Dividendo:</td>
                        <td width="75%">

                        <?php

                          $ano_actual=date("Y");
                         

                          echo '<select name="ano_divi" id="ano_divi" >';
                          if ($ano_divi==NULL)
                          { 
                            echo ' <option value="" selected disabled style="display:none;">Seleccionar el A&ntilde;o del Dividendo</option>';
                          }
                          else
                          {
                              echo '<option value='.$ano_divi.'>'.$ano_divi.'</option>';
                          }  
                          for ($i=$ano_actual-5; $i<=$ano_actual;$i++)
                          {

                               echo '<option value='.$i.'>'.$i.'</option>';



                          }
                          
                           
            
                          echo '</select></td>';


                         

                          ?>

                        </td>


                  

                   </tr>

                    <tr>
                        <td width="25%" class="etiquetas">Monto:</td>
                        <td width="75%"><?php escribir_campo('mont_divi',$_POST["mont_divi"],$mont_divi,'',255,30,'Monto del Dividendo',$boton,$existe,'','','')?></td>
                    </tr>
                       
                    <tr>
                        <td width="25%" class="etiquetas">Detalle:</td>
                        <td width="75%"><?php escribir_campo('detalle_divi',$_POST["detalle_divi"],$detalle_divi,'',255,30,'Detalles',$boton,$existe,'','','')?></td>
                    </tr>
 
                    <tr>
                        <td width="25%" class="etiquetas">Elaborado:</td>
                        <td width="75%"><?php escribir_campo('elab_egre',$_POST["elab_egre"],$elab_egre,'',255,30,'Elaborado por',$boton,$existe,'','','')?></td>
                    </tr>

                    <tr>
                        <td width="25%" class="etiquetas">Revisado:</td>
                        <td width="75%"><?php escribir_campo('revi_egre',$_POST["revi_egre"],$revi_egre,'',255,30,'Revisado por',$boton,$existe,'','','')?></td>
                    </tr>

                    <tr>
                        <td width="25%" class="etiquetas">Autorizado:</td>
                        <td width="75%"><?php escribir_campo('autr_egre',$_POST["autr_egre"],$autr_egre,'',255,30,'Autorizado por',$boton,$existe,'','','')?></td>
                    </tr>

                    <tr>
                        <td width="25%" class="etiquetas">Contabilizado:</td>
                        <td width="75%"><?php escribir_campo('cntb_egre',$_POST["cntb_egre"],$cntb_egre,'',255,30,'Contabilizado por',$boton,$existe,'','','')?></td>
                    </tr>

                    <tr>
                        <td width="25%" class="etiquetas">Cuenta Bancaria:</td>
                        <td width="75%">
                          
                      <?php 
                      $consulta_bancos=mysql_query("SELECT * FROM banco_cuentas ");

                   //muestra la consulta de los dividendos no pagados
                  

                           echo '<select name="codg_cnta" id="codg_cnta"  >';
                            if ($_POST[codg_cnta]==NULL)
                            { 
                              echo ' <option value="" selected disabled style="display:none;">Seleccionar la Cuenta Bancaria</option>';
                            }
                             else 
                             {
                              echo' <option    value="'.$_POST[codg_cnta].'"  >'.$_POST[codg_cnta].'</option> ';
                             }
                            while($fila=mysql_fetch_array($consulta_bancos))
                            {
                                echo '<option value='.$fila[codg_cnta].'>'.$fila[bnco_cnta].'->'.$fila[nmro_cnta].'</option>';
                            }
                
                           echo '</select></td></tr>';
                   ?>

                      
              
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
          
                  <tr>
            <td>
          
          </td>
          </tr>
          <tr>
                    <td>
          <?php 
            $ncriterios =2;
            $criterios[0] = "Descripci&oacute;n"; 
            $campos[0] ="desc_asien";
            $criterios[1] = "Fecha"; 
            $campos[1] ="fech_asien";
 
            if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
            crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>

                  <!--//// funcion para guardar mediante JQuery -->
                  <script>
                    function realizaProceso(num_cheq,id_div_det,id_div_che,id_egreso,cedu_soci,ca,accion){
                        var parametros = {
                          "num_cheq" : num_cheq,
                          "ca" : ca,
                          "id_div_det" : id_div_det,
                          "id_div_che" : id_div_che,
                          "id_egreso" : id_egreso,
                          "cedu_soci":cedu_soci,
                          "accion" : accion
               
               
                        };



                        if (num_cheq)
                        {
                          $.ajax({
                            data:  parametros,
                              url:   'guardar_cheques_dividendos.php',
                              type:  'post',
                              beforeSend: function () 
                              {
                                  $("#resultante" + ca).html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
                              },
                              success:  function (response) 
                              {
                                $("#resultante" + ca).html(response);
                              }
                          });
                        }
                        else { alert('Debe Indicar el Numero de cheque'); }
                    }
                  
                  </script>
                  <!--/////////////////////////////////////////-->


                  <tr class="titulo"><td colspan="2"> Pago Dividendos</td></tr>
                  <tr><td> &nbsp;</td> </tr>
                  <tr>
                  <td class="etiquetas">A&ntilde;o Dividendo: 

                   <?php 


                   //muestra la consulta de los dividendos no pagados
                  

                       echo '<select name="ano_dividendo" id="ano_dividendo" onchange="this.form.submit()" >';
                      if ($ano_dividendo==NULL)
                      { 
                        echo ' <option value="" selected disabled style="display:none;">Seleccionar el Asiento</option>';
                      }
                     else 
                      {
                        echo' <option    value="'.$ano_dividendo.'"  >'.$ano_dividendo.'</option> ';
                       }
                        while($fila=mysql_fetch_array($consulta_dividendos))
                        {
                            echo '<option value='.$fila[ano_divi].'>'.$fila[ano_divi].'</option>';
                        }
            
                      echo '</select></td></tr>';

                 //empezamos a consultar para mostrar el resultados de los socios que tienen seis meses cotizando     
                $boton3=$_POST[boton3];
                 if ($ano_dividendo!=NULL)
                {
                  if ($boton3=="===CALCULAR===")
                  {

                    $Actualizar=mysql_query("UPDATE dividendos SET status_divi='calculando' where ano_divi='$ano_dividendo' ");

                 
                  }
                  //consulta del dividendo
                  $consulta_dividendo=mysql_query("SELECT * FROM dividendos where ano_divi='$ano_dividendo'  ");
                  $con_div=mysql_fetch_assoc($consulta_dividendo);

                          echo '<table>';
                          echo '<tr><td colspan="8" align="center"><h4>Monto Dividendo:'.redondear($con_div[mont_divi],2,".",",").'Bs. </h4></td></tr>';
                          // consulta la depèndencia 
                            $consulta_dependencias=mysql_query("SELECT * FROM dependencias  ");
                            $ca=0;
                            $sumatoria_dividendos=0;
                            $sumatoria_ahorros=0;
                            $sumatoria_aporte=0;
                            $sumatoria_saldo_ahorrado=0;
                            $proporciont=0;
                            $sumatoria_retiro=0;
                            $proporciont=$_POST[proporcion];
                            while($fila=mysql_fetch_array($consulta_dependencias))
                            {

                                $id_depen=$fila[codg_depn];
                                //busca al socio en la dependencia seleccionada
                                echo '<tr"><td class="titulo" colspan="8">'.$fila[nomb_depn]. '</td> </tr>';
                                echo '<tr class="titulo"> <td>N&deg;</td> <td>C&eacute;dula</td> <td>Nombres y Apellidos</td> <td> Ultimo Sueldo</td> <td> Saldo Ahorrado</td> <td>Saldo Aporte</td> <td> Retiros</td> <td> Total Ahorro</td>  <td> Dividendo</td> <td colspan="2"> Cheque</td></tr>';
                                $consulta_socios_laboral=mysql_query("SELECT * FROM socios as s, socios_dat_lab as sdl where sdl.codg_depn='$id_depen' and s.cedu_soci=sdl.cedu_soci GROUP BY sdl.cedu_soci");
                                $i=0;
                                while ($fila2=mysql_fetch_array($consulta_socios_laboral))
                                {
                                   
                                  
                                  $cedu_soci=$fila2[cedu_soci]; 
                                  //busca al socio por la ultima fecha laboral
                                  
                                  $consulta_socios_laboral2=mysql_query("SELECT * FROM socios as s, socios_dat_lab as sdl where  sdl.cedu_soci='$cedu_soci' and s.cedu_soci=sdl.cedu_soci order by sdl.fchi_dlab desc limit 1");
                                  while ($fila3=mysql_fetch_array($consulta_socios_laboral2))
                                  {
                                    $i++;
                                    
                                    //** PARA DARLE COLORCITO A LA LINEAS 
                                      $filacolor=$i%2;
                                       if ($filacolor==1)
                                      {
                                  
                                          $estilo_fila='filaimpar';
                                      }
                                       else
                                      {
                                       $estilo_fila='filapar';
                                      }
                                      //consulta para evaluar si ha cotizado por 6 meses o no para poder cancelar dividendo
                                        $cantidad_mes=0;
                                        $saldo_ahorrado=0;
                                        $saldo_aporte=0;
                                        $total_ahorrado=0;
                                        $dividendo_per=0;
                                        $retiro_per=0;
                                        $consulta_detalle_nomina=mysql_query("SELECT * FROM nominas_detalle AS nd, nominas AS n WHERE nd.cedu_soci = '$fila3[cedu_soci]' AND n.codg_nmna = nd.codg_nmna AND  n.anno_nmna = '$ano_dividendo' GROUP by nd.codg_nmna");
                                       // echo "SELECT *FROM nominas_detalle AS nd, nominas AS n WHERE nd.cedu_soci = '$fila3[cedu_soci]' AND n.codg_nmna = nd.codg_nmna AND  n.anno_nmna = '$ano_dividendo'";
                                        while ($fila4=mysql_fetch_array($consulta_detalle_nomina))
                                        {
                                          if ($fila4[prdo_nmna]==8)
                                          {
                                            $cantidad_mes=$cantidad_mes+1;
                                          }
                                          if ($fila4[prdo_nmna]==6 or $fila4[prdo_nmna]==7 )
                                          {
                                            $cantidad_mes=$cantidad_mes+0.50;
                                          }
                                          if ($fila4[prdo_nmna]>=11)
                                          {
                                            $cantidad_mes=$cantidad_mes+0.25;
                                          }

                                                                             

                                          

                                        }
                                        //consulta para calculos
                                     // $consulta_detalle_suma=mysql_query("SELECT * FROM nominas_detalle AS nd, nominas AS n WHERE nd.cedu_soci = '$fila3[cedu_soci]' AND n.codg_nmna = nd.codg_nmna AND  n.anno_nmna <= '$ano_dividendo' ");
                                      $consulta_detalle_suma=mysql_query("SELECT * FROM nominas_detalle AS nd, nominas AS n WHERE nd.cedu_soci = '$fila3[cedu_soci]' AND n.codg_nmna = nd.codg_nmna AND  n.anno_nmna <= '$ano_dividendo' ");

                                      while ($fila5=mysql_fetch_array($consulta_detalle_suma))
                                      {

                                            



                                        if ($fila5[codg_pago]!='')
                                        {

                                            $consulta_dep_pag=mysql_query("SELECT * FROM dependencias_pagos AS dp  WHERE dp.codg_pago = '$fila5[codg_pago]' ");
                                            $con_dep_pag=mysql_fetch_assoc($consulta_dep_pag);
                                            $fecha_pag=substr($con_dep_pag[fcha_pago], 0, 4);
                                        }
                                        if ($fila5[codg_exce]!='')
                                        {
                                            $consulta_exce=mysql_query("SELECT * FROM dependencias_pagos AS dp,  dependencias_pagos_exce AS de  WHERE de.codg_exce = '$fila5[codg_exce]' and dp.codg_pago=de.codg_pago ");
                                            $con_exce=mysql_fetch_assoc($consulta_exce);
                                            $fecha_pag=substr($con_exce[fcha_pago], 0, 4);

                                        }

                                        if ($fecha_pag<=$ano_dividendo)
                                        {

                                           




                                                if ($fila5[moti_dlle]=='Aporte')
                                                {
                                                  $saldo_aporte=$saldo_aporte+$fila5[mnto_dlle];
                                                }
                                                $moti_detalle=$fila5[moti_dlle];
                                                $moti_detalle = substr($moti_detalle, 0,7);

                                                if ($moti_detalle=='Retenci')                                  
                                                {
                                                  
                                                  $saldo_ahorrado=$saldo_ahorrado+$fila5[mnto_dlle];
                                                }
                                               
                                               if ($fila5[moti_dlle]=='Reintegro')                                  
                                                {
                                                  $saldo_ahorrado=$saldo_ahorrado+$fila5[mnto_dlle];
                                                }



                                        }

                                          
                                              


                                      }

                                            $consulta_retiros=mysql_query("SELECT SUM(mont_reti) as suma_retiro  FROM retiros WHERE cedu_soci='$fila3[cedu_soci]' and year(fcha_reti)<=$ano_dividendo");
                                            $con_ret=mysql_fetch_assoc($consulta_retiros);
                                            $retiro_per=$con_ret[suma_retiro];

                                            $consulta_socio_movimientos=mysql_query("SELECT * FROM socios_movimientos  WHERE orig_movi='M' and cedu_soci='$fila3[cedu_soci]' and year(fcha_movi)<=$ano_dividendo ");
                                            while ($con_soc_mov=mysql_fetch_array($consulta_socio_movimientos))
                                            {

                                                //consulta  a socios_movimientos para sumar o restar los movimientos manuales y aparte restar los retiros 

                                                if($con_soc_mov[dest_movi]=='A' and $con_soc_mov[tipo_movi]=='I')
                                                {
                                                    $saldo_aporte=$saldo_aporte+$con_soc_mov[mont_movi];
                                                }

                                                if($con_soc_mov[dest_movi]=='A' and $con_soc_mov[tipo_movi]=='E')
                                                {
                                                    $saldo_aporte=$saldo_aporte-$con_soc_mov[mont_movi];
                                                }
                                                if($con_soc_mov[dest_movi]=='R' and $con_soc_mov[tipo_movi]=='I')
                                                {
                                                    $saldo_ahorrado=$saldo_ahorrado+$con_soc_mov[mont_movi];
                                                }
                                                if($con_soc_mov[dest_movi]=='R' and $con_soc_mov[tipo_movi]=='E')
                                                {
                                                    $saldo_ahorrado=$saldo_ahorrado-$con_soc_mov[mont_movi];
                                                }
                                                if($con_soc_mov[dest_movi]=='I' and $con_soc_mov[tipo_movi]=='I')
                                                {
                                                    $saldo_ahorrado=$saldo_ahorrado+$con_soc_mov[mont_movi];
                                                }
                                                if($con_soc_mov[dest_movi]=='I' and $con_soc_mov[tipo_movi]=='E')
                                                {
                                                    $saldo_ahorrado=$saldo_ahorrado-$con_soc_mov[mont_movi];
                                                }
                                           
                                            }


                                             
                                      $total_ahorrado=($saldo_aporte+$saldo_ahorrado)-$retiro_per;
                                      $dividendo_per=$proporciont*$total_ahorrado;

                                      

                                    if ($fila3[codg_depn]==$id_depen and $cantidad_mes>=1)
                                    {
                                      $ca++;
                                      $sumatoria_ahorros=$sumatoria_ahorros+$total_ahorrado;
                                      $sumatoria_aporte=$sumatoria_aporte+$saldo_aporte;
                                      $sumatoria_saldo_ahorrado=$sumatoria_saldo_ahorrado+$saldo_ahorrado;
                                      $sumatoria_retiro=$sumatoria_retiro+$retiro_per; 
                                      echo '<INPUT type="hidden"   name="cedu_soci'.$ca.'" id="cedu_soci'.$ca.'" value="'.$fila3[cedu_soci].'" >';
                                      echo '<INPUT type="hidden"   name="id_divi'.$ca.'" id="id_divi'.$ca.'" value="'.$con_div[id_divi].'" >';

                                      echo '<tr class="'.$estilo_fila.'"><td align="left">'.$ca.'</td><td>'.$fila3[cedu_soci]. '</td><td align="left">'.$fila3[nomb_soci].' '.$fila3[apel_soci].'</td><td align="right">'.redondear($fila3[suel_dlab],2,".",","). '</td><td align="right">'.redondear($saldo_ahorrado,2,".",","). '</td><td align="right">'.redondear($saldo_aporte,2,".",","). '</td> <td align="right">'.redondear($retiro_per,2,".",","). '</td> <td align="right">'.redondear($total_ahorrado,2,".",","). '</td>';

                                       //consulta del monto del dividendo a cancelar
                                        $consulta_detalle_dividendos=mysql_query("SELECT * FROM detalle_dividendos where id_divi='$con_div[id_divi]' and cedu_soci='$fila3[cedu_soci]' ");
                                        $con_det_div=mysql_fetch_assoc($consulta_detalle_dividendos);

                                        $consulta_egresos=mysql_query("SELECT * FROM egresos where ano_divi='$con_div[ano_divi]' ");
                                        $con_egre=mysql_fetch_assoc($consulta_egresos);
                                       
                                        $sumatoria_dividendos=$con_det_div[mont_det_div]+$sumatoria_dividendos;

                                      if ($con_div[status_divi]!='pagado' and $boton3=="===CALCULAR===")
                                      {

                                          echo '<td align="right"> <INPUT type="text" readonly   size="8" name="mont_det_div'.$ca.'" id="mont_det_div'.$ca.'" value="'.redondear($dividendo_per,2,'','.').'" title="Dividendo"></td></tr>';
                                      }
                                      else
                                      {
                                        echo '<td align="right">'.$con_det_div[mont_det_div]. '</td>';
                                        if ($con_div[status_divi]=='pagado')
                                        {
                                          echo '<td align="right" colspan="2" width="120px">'; include ('aux_dividendos.php');    echo '</td>';
                                        }
                                        echo '</tr>';
                                      }
                                    }
                                  }

                                }

                            }
                               $proporcion= $con_div[mont_divi]/$sumatoria_ahorros;


                          echo '<tr> <td colspan="4" class="titulo"> TOTALES </td><td class="filaimpar" align="right"> '.redondear($sumatoria_saldo_ahorrado,2,".",",").' </td><td class="filaimpar" align="right"> '.redondear($sumatoria_aporte,2,".",",").' </td> <td class="filaimpar" align="right"> '.redondear($sumatoria_retiro,2,".",",").' </td> <td class="filaimpar" align="right"> '.redondear($sumatoria_ahorros,2,".",",").' </td> <td class="filaimpar"  align="right">'.redondear($sumatoria_dividendos,2,".",",").' </td></tr>';
                          echo '<tr> <td colspan="8" align="center"> &nbsp; </td></tr>';

                           echo '<tr> <td colspan="8" align="left"><h5> Proporci&oacute;n:'.redondear($proporcion,10,'.',',').'</h5> </td></tr>';
                           echo '<INPUT type="hidden"   name="proporcion" id="proporcion" value="'.redondear($proporcion,10,'','.').'" >';

                           echo '<tr> <td colspan="8" align="center"> &nbsp; </td></tr>';
                         

                           echo '<INPUT type="hidden"   name="ca" id="ca" value="'.$ca.'" >';
                          echo '<INPUT type="hidden"   name="id_divi1" id="id_divi1" value="'.$con_div[id_divi].'" >';

                          if ($con_div[status_divi]!='pagado' and $boton3=="===CALCULAR===" )
                          {
                            echo '<tr> <td colspan="8" align="center"><input type="SUBMIT" value="PROCESAR" id="boton2" name="boton2"> </td></tr>';
                          }
                          if ($con_div[status_divi]!='pagado' and $con_div[status_divi]!='calculando' )
                          {
                            echo '<tr> <td colspan="8" align="center"><input type="SUBMIT" value="===CALCULAR===" id="boton3" name="boton3"> </td></tr>';

                          }
                           

                          if ($con_div[status_divi]=='pagado')
                          {
                            echo '<tr class="filaimpar"> 
                            <td colspan="3 align="center"> <a href="../reportes/generar_cheque_dividendos.php?id_divi='.$con_div[id_divi].'"> <img src="../imagenes/imprimir.png" title="Generar Cheques" style="cursor:pointer;" /> </a> </td>';
                            echo '<td colspan="3" align="center"> <a href="../reportes/listado_dividendos.php?id_divi='.$con_div[id_divi].'"> <img src="../imagenes/listado.png" title="Generar Listado" style="cursor:pointer;" id="btn_listado" name="btn_listado"/> </a> </td>';
                              echo '<td colspan="3 align="center"> <a href="../reportes/recibo_dividendos.php?id_divi='.$con_div[id_divi].'"><img src="../imagenes/recibo.png" title="Generar recibos" style="cursor:pointer;" id="btn_recibo" name="btn_recibo"/> </a></td>';
                            
                            echo '</tr>';

                          }
                         



                          echo '</table>';


                }
              
                 


           ?>

                  </td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>