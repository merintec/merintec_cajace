<?PHP 
echo '<meta charset="ISO-8859-1">';
	include_once('../comunes/conexion_basedatos.php');
	include_once('../comunes/formularios_funciones.php');
	$fcha_reti = $_POST[fcha_reti];
	$cedu_soci = $_POST[cedu_soci];
	$tipo_reti = $_POST[tipo_reti];
	$numr_acta = $_POST[numr_acta];
	$fcha_acta = $_POST[fcha_acta];
	$obsr_reti = utf8_decode($_POST[obsr_reti]);
	$mont_reti = $_POST[mont_cheq];
	$mont_ret_ret = $_POST[mont_ret_ret];
	$mont_ret_apr = $_POST[mont_ret_apr];
	$mont_ret_rei = $_POST[mont_ret_rei];
	//// para generar numero se reincia cada a�o
	$aux = date('Y', strtotime($fcha_acta));
	$sql_max = "SELECT MAX(nmro_reti) as ultimo FROM retiros WHERE YEAR(fcha_acta) = ".$aux;
	$sql_max = mysql_query($sql_max);
	$sql_max = mysql_fetch_array($sql_max);
	$nmro_reti = ($sql_max[ultimo]+1);
	if (!$nmro_reti || $nmro_reti == 0){
		$nmro_reti = 1;
	}

	$tabla = "retiros";	// nombre de la tabla
	$ncampos = "12";			//numero de campos del formulario
	$datos[0] = crear_datos ("fcha_reti","Fecha de Solicitud",$fcha_reti,"10","10","fecha");
	$datos[1] = crear_datos ("cedu_soci","Ahorrista",$cedu_soci,"1","12","numericos");
	$datos[2] = crear_datos ("tipo_reti","Tipo de Retiro",$tipo_reti,"1","12","alfabeticos");
	$datos[3] = crear_datos ("stat_reti","Estado de Retiro",'A',"1","11","alfabeticos");
	$datos[4] = crear_datos ("numr_acta","Numero de Acta",$numr_acta,"1","20","alfanumericos");
	$datos[5] = crear_datos ("fcha_acta","Fecha del Acta",$fcha_acta,"1","10","fecha");
	$datos[6] = crear_datos ("obsr_reti","Observaciones",$obsr_reti,"0","255","alfanumericos");
	$datos[7] = crear_datos ("mont_reti","Monto del Retiro",$mont_reti,"0","12","decimal");
	$datos[8] = crear_datos ("apr_reti","Monto de Aportes",$mont_ret_apr,"0","12","decimal");
	$datos[9] = crear_datos ("ret_reti","Monto de Retenci�n",$mont_ret_ret,"0","12","decimal");
	$datos[10] = crear_datos ("rei_reti","Monto de Reintegro",$mont_ret_rei,"0","12","decimal");
	$datos[11] = crear_datos ("nmro_reti","Numero",$nmro_reti,"0","12","numericos");
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion){
		$retiro = insertar_func_nomina($ncampos,$datos,$tabla);
		if ($retiro){	
			// movimientos de saldos por retiro
			if($mont_ret_apr > 0){
				$sql_insertar = "INSERT INTO socios_movimientos (cedu_soci,fcha_movi,conc_movi,tipo_movi,dest_movi,mont_movi,orig_movi,codg_dlle,codg_dlle_retr) VALUES (".$cedu_soci.",'".$fcha_acta."','Retiro (".$tipo_reti.")','E','A',".$mont_ret_apr.",'A',".$retiro.",0)";
				mysql_query($sql_insertar);
			}
			if($mont_ret_ret > 0){
				$sql_insertar = "INSERT INTO socios_movimientos (cedu_soci,fcha_movi,conc_movi,tipo_movi,dest_movi,mont_movi,orig_movi,codg_dlle,codg_dlle_retr) VALUES (".$cedu_soci.",'".$fcha_acta."','Retiro (".$tipo_reti.")','E','R',".$mont_ret_ret.",'A',".$retiro.",0)";
				mysql_query($sql_insertar);
			}
			if($mont_ret_rei > 0){
				$sql_insertar = "INSERT INTO socios_movimientos (cedu_soci,fcha_movi,conc_movi,tipo_movi,dest_movi,mont_movi,orig_movi,codg_dlle,codg_dlle_retr) VALUES (".$cedu_soci.",'".$fcha_acta."','Retiro (".$tipo_reti.")','E','I',".$mont_ret_rei.",'A',".$retiro.",0)";
				mysql_query($sql_insertar);
			}
			// Movimiento para prestamos pagados
			for ($i=1; $i<=$_POST[n_ppp]; $i++){
				if ($_POST['pagar_ppp'.$i]){
					$sql_insertar = "INSERT INTO prestamos_mov (codg_prst,mnto_prtm,capi_prtm,inte_prtm,orgn_prtm,rela_prtm,freg_prtm,fpag_prtm) VALUES (".$_POST['codg_prst_ppp'.$i].",".$_POST['total_ppp'.$i].",".redondear(($_POST['total_ppp'.$i]-$_POST['inter_ppp'.$i]),2,'','.').",".$_POST['inter_ppp'.$i].",'Retiro',".$retiro.",'".$fcha_acta."','".$fcha_acta."')";
					mysql_query($sql_insertar);
				}
			}
			// prestamos respaldados
			for ($i=1; $i<=$_POST[num_fia]; $i++){
				if (!$_POST['fia'.$i]) {
					$sql_insertar = "INSERT INTO socios_movimientos (cedu_soci,fcha_movi,conc_movi,tipo_movi,dest_movi,mont_movi,orig_movi,codg_dlle,codg_dlle_retr) VALUES (".$cedu_soci.",'".$fcha_acta."','Retenci�n de fianza por Retiro (Pr�stamo ".$_POST['cod_gar'.$i].")','I','I',".$_POST['mont_gar'.$i].",'A',".$retiro.",0)";
					mysql_query($sql_insertar);
				}
				else{
					$sql_update = "UPDATE prestamos_fia SET codg_reti = ".$retiro.", libre_fian = 'Liberada el: ".ordenar_fecha($fcha_acta).": Retiro (".$tipo_reti.")' WHERE cedu_soci = ".$cedu_soci." AND codg_prst = ".$_POST['cod_gar'.$i];
					mysql_query($sql_update);
				}
			}
		}
	echo ':::::'.$retiro;
	}
?>
