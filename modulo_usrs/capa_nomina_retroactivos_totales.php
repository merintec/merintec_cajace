<?php 
	echo '<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1" />';
	include_once('../comunes/conexion_basedatos.php');
	include_once ('../comunes/formularios_funciones.php');
	if (!$codg_retr){
		$codg_retr = $_POST[codg_retr];
	}
	if ($codg_retr) {
		$sql_totales = "select SUM(mont_nr_dlle) as total, (SELECT SUM(mont_nr_dlle) FROM nominas_retroactivos_detalles WHERE codg_retr=".$codg_retr." AND dest_nr_dlle='Aporte' GROUP BY dest_nr_dlle) as aporte, (SELECT SUM(mont_nr_dlle) FROM nominas_retroactivos_detalles WHERE codg_retr=".$codg_retr." AND dest_nr_dlle='Retencion' GROUP BY dest_nr_dlle) as retencion, (SELECT SUM(mont_nr_dlle) FROM nominas_retroactivos_detalles WHERE codg_retr=".$codg_retr." AND dest_nr_dlle='Montepio' GROUP BY dest_nr_dlle) as montepio  from nominas_retroactivos_detalles where codg_retr = ".$codg_retr." GROUP BY codg_retr";
		$res_totales = mysql_fetch_array(mysql_query($sql_totales));
		echo '<table style="border-collapse:collapse;" border="1" bordercolor="#000000" class="nomina" cellspacing="0" cellpadding="0" align="center" width="90%">';
			echo '<tr class="nomina_titulo" style="line-height: 30px; color: #FFFFFF;" bgcolor="#67BABA" id="tr_totales">';
				echo '<td align="right" colspan="3">TOTAL &nbsp;</td>
				<td align="right" id="total_Aporte" width="120px">'.redondear($res_totales[aporte],2,'.',',').'&nbsp;</td>
				<td align="right" id="total_Retencion" width="120px">'.redondear($res_totales[retencion],2,'.',',').'&nbsp;</td>
				<td align="right" id="total_Montepio" width="120px">'.redondear($res_totales[montepio],2,'.',',').'&nbsp;</td>
				<td align="right" id="total_total" width="120px">'.redondear($res_totales[total],2,'.',',').'&nbsp;</td>';
			echo '</tr>';
		echo '</table>';
		echo '<br>';
		echo '<table style="border-collapse:collapse;" border="0" bordercolor="#000000" class="nomina" cellspacing="0" cellpadding="0" align="center" width="80%">';
		echo '<tr>
      			<td align="center" class="titulo" colspan="3">
   	  				REPORTES DE RETROACTIVO
      			</td>            
   			</tr>';
		echo '<tr class="nomina_titulo">
				<td>Aporte/Ahorros</td>
				<td>Montepios</td>
				<td>Asiento</td>
			</tr>';
         echo '<tr align="center">
            <td>';
               $total_ar = $res_totales[aporte] + $res_totales[retencion]; 
               if ($total_ar > 0){
                  abrir_ventana('../reportes/nmna_retr_ahorros_retenciones.php','v_reporte','VER REPORTE (Bs. '.redondear($total_ar,2,'.',',').')',"codg_retr=".$codg_retr."&seccion=".$_GET['seccion']);
               }
            echo '</td>
            <td>'; 
               if ($res_totales[montepio] > 0 ){
                  abrir_ventana('../reportes/nmna_retr_montepio.php','v_reporte','VER REPORTE (Bs. '.redondear(($res_totales[montepio]),2,'.',',').')',"codg_retr=".$codg_retr."&seccion=".$_GET['seccion']);
               }
            echo '<td id="asientos">'; 
               $asientos_mos = 'NO';
               $sql_asientos = "SELECT * FROM asientos WHERE orgn_asien = 'Retroactivo' AND codg_rela = ".$codg_retr;
               $bus_asientos = mysql_query($sql_asientos);
               while ($res_asientos = mysql_fetch_array($bus_asientos)){
                  $asientos_mos = 'SI';
                  echo '<input type="hidden" id="asiento" name="asiento" value="'.$res_asientos[codg_asien].'">';
                  $parametros = "'eliminar','".$codg_retr."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','".$res_asientos[codg_asien]."'";
                  echo '<input type="button" name="v_reporte" id="v_reporte" value="Eliminar ('.$res_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
                  $parametros = "'imprimir','".$codg_nmna."','".$res_reporte[codg_pago]."','".$_GET['seccion']."','".$res_asientos[codg_asien]."'";
                  echo '<input type="button" name="v_reporte" id="v_reporte" value="Imprimir ('.$res_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
               }
               if ($asientos_mos == 'NO'){
                  $parametros = "'ver','".$codg_retr."','".$codg_pago."','".$_GET['seccion']."','0'";
                  echo '<input type="button" name="v_reporte" id="v_reporte" value="Ver" onclick="asientos('.$parametros.')">';
                  $parametros = "'generar','".$codg_retr."','".$codg_pago."','".$_GET['seccion']."','0'";
                  echo '<input type="button" name="v_reporte" id="v_reporte" value="Generar" onclick="asientos('.$parametros.')">';
               }
            echo '</td>
         </tr>'; 
		echo '</table><br>';
	}
?>