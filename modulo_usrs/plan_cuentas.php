<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'plan_cuentas.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "plan_cuentas";	// nombre de la tabla
$ncampos = "3";			//numero de campos del formulario

$datos[0] = crear_datos ("desc_pcuen","Descripción del Plan ",$_POST['desc_pcuen'],"1","255","alfanumericos");
$datos[1] = crear_datos ("fech_pcuen","Fecha del Plan",$_POST['fech_pcuen'],"10","10","fecha");
$datos[2] = crear_datos ("stat_pcuen","Status del Plan",$_POST['stat_pcuen'],"1","255","alfanumericos");
 

if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Descripción";
		$datos[0]="desc_pcuen";
		$parametro[1]="Fecha";
		$datos[1]="fech_pcuen";
		$parametro[2]="Status";
		$datos[2]="stat_pcuen";
		busqueda_varios(5,$buscando,$datos,$parametro,"codg_pcuen");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_pcuen = $row["codg_pcuen"];
	    $desc_pcuen = $row["desc_pcuen"];
	    $fech_pcuen = $row["fech_pcuen"];
	    $stat_pcuen = $row["stat_pcuen"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	


	if ($validacion) {

		$valida=mysql_query("SELECT * FROM plan_cuentas WHERE desc_pcuen='$_POST[desc_pcuen]' and codg_pcuen!='$_POST[codg_pcuen]' ");
		$valida2=mysql_fetch_array($valida);

		

		if ($valida2)
		{
			$modificando='no';
			?>
			<script type="text/javascript">
				alert("la Descripción del Plan de Cuenta debe ser distinto");
			</script>
			<?php
			$boton = "Actualizar";

		}
		if ($_POST['stat_pcuen']=='Activo')
		{
			$valida1=mysql_query("SELECT * FROM plan_cuentas WHERE stat_pcuen='Activo' and codg_pcuen!='$_POST[codg_pcuen]' ");
		    $valida3=mysql_fetch_array($valida1);

		}
		
	    if ($valida3)
		{
			$modificando='no';
			?>
			<script type="text/javascript">
				alert("Hay otro Plan de Cuenta Activo debe Inactivarlo Primero");
			</script>
			<?php
			$boton = "Actualizar";

		}
		
 		 if ($modificando!='no')
		{
			modificar_func($ncampos,$datos,$tabla,"codg_pcuen",$_POST["codg_pcuen"],$pagina,"");
			auditoria_func ('modificar', '', $_POST["ant"], $tabla);
			return;			
		}

	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
 	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si',$_GET["nom_sec"]);
	if ($datos[2][2]=='Activo')
	{
			$boton=comp_exist($datos[2][0],$datos[2][2],$tabla,$boton,'si',$_GET["nom_sec"].' Se encuentra un Activo');

	}

}
if ($_POST["confirmar"]=="Guardar") 
{
	/*$consultar="SELECT * FROM plan_cuentas WHERE desc_pcuen=$datos[1]";
	$con1=mysql_query($consultar);
	$con1=mysql_fetch_array($con1);*/

	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	$validar=mysql_query("SELECT * FROM cuentas where codg_pcuen='$_POST[codg_pcuen]'");
  	$val=mysql_fetch_array($validar);
  	if ($val[codg_pcnta]==NULL)
  	{

		eliminar_func($_POST["codg_pcuen"],"codg_pcuen",$tabla,$pagina);
		auditoria_func ('eliminar', $ncampos, $datos, $tabla);
		return;
	}
	else
	{
		?>
		<script type="text/javascript">

			alert("Error... no se ha podido eliminar, posee relación con cuentas");

		</script>

		<?php 
	}
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Plan de Cuentas</td>
                  </tr>

                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                    <tr>
                  	 <td width="25%" class="etiquetas">Fecha:</td>
                     <td width="75%"><?php 
                     escribir_campo('codg_pcuen',$_POST["codg_pcuen"],$codg_pcuen,'',12,15,'Codigo del plan',$boton,$existe,'','','oculto')?>

                     <?php escribir_campo('fech_pcuen',$_POST["fech_pcuen"],$fech_pcuen,'',50,10,'Fecha de Inicio del Plan de Cuenta',$boton,$existe,'fecha','','')?></td>
                   </tr>
 
                      <tr>
                        <td width="25%" class="etiquetas">Descripci&oacute;n del Plan:</td>
                        <td width="75%"><?php escribir_campo('desc_pcuen',$_POST["desc_pcuen"],$desc_pcuen,'',255,30,'Descripción del Plan de Cuenta',$boton,$existe,'','','')?></td>
                      </tr>
                       <tr>
                        <td class="etiquetas">Status del Plan:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="stat_pcuen" title="Status del Plan">
                          <option>Seleccione...</option>
                          <option value="Activo" '; if ($stat_pcuen == "Activo" || $_POST['stat_pcuen'] =="Activo") { echo 'selected'; } echo '>Activo</option>
                          <option value="Inactivo" '; if ($stat_pcuen == "Inactivo" || $_POST['stat_pcuen'] =="Inactivo") { echo 'selected'; } echo '>Inactivo</option>
                        </select>'; } 
						else 
						{ 

						    echo '<input type="hidden" name="stat_pcuen" id="stat_pcuen" value="'.$stat_pcuen.'" >'; 
						    if ($stat_pcuen == "Activo") { echo 'Activo'; } 
							if ($stat_pcuen == "Inactivo") { echo 'Inactivo'; }
						}?></td>
                      </tr>
              
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
					
                  <tr>
				  	<td>
					
					</td>
				  </tr>
				  <tr>
                    <td>
					<?php 
						$ncriterios =3;
						$criterios[0] = "Descripci&oacute;n"; 
						$campos[0] ="desc_pcuen";
						$criterios[1] = "Fecha"; 
						$campos[1] ="fech_pcuen";
						$criterios[2] = "Status"; 
						$campos[2] ="stat_pcuen";

					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
