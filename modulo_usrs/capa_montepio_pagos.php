<?php 
    $sql_capa = "SELECT d.nomb_depn, ";
    // Monto por pagar
    $sql_capa .= "(SELECT SUM(mnto_mpio) FROM montepio_socios where codg_mpio=ms.codg_mpio AND codg_depn=d.codg_depn) as pagar, "; 
    // Monto pagado
    $sql_capa .= "(SELECT SUM(mnto_mpio) FROM montepio_socios, nominas_detalle where codg_mpio=ms.codg_mpio AND codg_depn=d.codg_depn AND montepio_socios.codg_mpsc=nominas_detalle.rela_dlle AND codg_pago!='') as pagado ";    
    $sql_capa .= "FROM dependencias d, montepio_socios ms WHERE ms.codg_depn=d.codg_depn and ms.codg_mpio=".$codg_mpio." GROUP BY ms.codg_depn,ms.codg_mpio"; 
    $res_capa = mysql_query($sql_capa);
    $cuenta_datos = 0;
    $total_pagar = 0;
    $total_pagado = 0;
    while ($reg_capa=mysql_fetch_array($res_capa)){
        $datos[$cuenta_datos][0]=$reg_capa[0];
        $datos[$cuenta_datos][1]=$reg_capa[1];
        $datos[$cuenta_datos][2]=$reg_capa[2];
        $total_pagar = redondear(($total_pagar+$reg_capa[1]),2,"",".");
        $total_pagado = redondear(($total_pagado+$reg_capa[2]),2,"",".");
        $cuenta_datos++;
    }
?>
<html>
  <body>
    <table width="100%" border="1" cellpadding="0" cellspacing="0" align="center" id="tabla" name="tabla" > 
      <tr class="etiquetas">
        <td align="center" colspan="5" class="titulo"><b>DATOS DE LOS PAGOS DE LAS DEPENDENCIAS</b></td>
      </tr>
      <tr class="etiquetas">
        <td align="center"><b>N�</b></td>
        <td align="center"><b>Dependencia</b></td>
        <td align="center" width="80px"><b>Monto a Pagar</b></td>
        <td align="center" width="80px"><b>Monto Pagado</b></td>
        <td align="center" width="30px"><b>% Pagado</b></td>
      </tr>
      <?php for ($i=0;$i<$cuenta_datos;$i++) { 
        if (($i % 2)==0) { $clase = 0; } else { $clase=1; }
      ?>    
        <tr class="<?php echo 'linea'.$clase; ?>">
          <td align="right"><?php echo $i+1; ?>&nbsp;</td>
          <td align="left">&nbsp;<?php echo $datos[$i][0]; ?></td>
          <td align="right"><?php echo redondear($datos[$i][1],2,".",","); ?>&nbsp;</td>
          <td align="right"><?php echo redondear($datos[$i][2],2,".",","); ?>&nbsp;</td>
          <td align="right"><?php echo redondear(($datos[$i][2]*100/$datos[$i][1]),2,".",",").'%'; ?>&nbsp;</td>
        </tr>
      <?php }  
        if (($i % 2)==0) { $clase = 0; } else { $clase=1; } 
      ?> 
        <tr class="<?php echo 'linea'.$clase; ?>">
          <td align="right" colspan="2" class="etiquetas">TOTALES&nbsp;</td>
          <td align="right" class="etiquetas"><?php echo redondear($total_pagar,2,".",","); ?>&nbsp;</td>
          <td align="right" class="etiquetas"><?php echo redondear($total_pagado,2,".",","); ?>&nbsp;</td>
          <td align="right" class="etiquetas"><?php echo redondear(($total_pagado*100/$total_pagar),2,".",",").'%'; ?>&nbsp;</td>
        </tr>
    </table>
  </body>
</html>
