<?PHP 
	include_once('../comunes/conexion_basedatos.php');
	include_once('../comunes/formularios_funciones.php');
	$codg_prst=$_POST['codg_prst'];
	$capital_pagar=$_POST['capital_pagar'];
	$inter_ppp=$_POST['inter_ppp'];
	$total_ppp=$_POST['total_ppp'];
	
	if($codg_prst){
		$pres_detalle = buscar_registro('nominas_detalle nd, nominas n', 'nd.codg_dlle, nd.mnto_dlle, n.anno_nmna, n.mess_nmna, n.prdo_nmna', ' WHERE moti_dlle = "Pr�stamo" AND n.codg_nmna = nd.codg_nmna AND rela_dlle = '.$codg_prst.' ORDER BY n.anno_nmna DESC, n.mess_nmna DESC, n.prdo_nmna DESC LIMIT 1', 'registro'); 
		if($pres_detalle){
			$prestamos_mov = buscar_registro('prestamos_mov pm', '*', ' WHERE pm.orgn_prtm LIKE "Registro de N%" AND pm.rela_prtm = '.$pres_detalle['codg_dlle'].'', 'registro'); 
			?>
			<table width="100%" border="1" cellspacing="0" cellpadding="0">
			  <tr class="etiquetas">
				<td width="20%">Nomina</td>
				<td width="20%">Capital&nbsp;pagado</td>
				<td width="20%">Interes&nbsp;pagado</td>
				<td width="20%">Total</td>
			  </tr>
			  <tr class="etiquetas">
				<td><?php if($pres_detalle['prdo_nmna']>10){ echo 'Semana-'.($pres_detalle['prdo_nmna']-10); }elseif($pres_detalle['prdo_nmna']==8){ escribir_mes($pres_detalle['mess_nmna']); echo '-'.$pres_detalle['anno_nmna']; }else{ echo 'Quincena '.($pres_detalle['prdo_nmna']-5).' de '; escribir_mes($pres_detalle['mess_nmna']); echo '-'.$pres_detalle['anno_nmna']; } ?></td>
				<td align="right"><?php echo redondear($prestamos_mov['capi_prtm'],2,".",","); ?>&nbsp;</td>
				<td align="right"><?php echo redondear($prestamos_mov['inte_prtm'],2,".",","); ?>&nbsp;</td>
				<td align="right"><?php echo redondear($prestamos_mov['capi_prtm']+$prestamos_mov['inte_prtm'],2,".",","); ?></td>
			  </tr>
			</table>
	<?php }else{ ?>
			<table width="100%" border="1" cellspacing="0" cellpadding="0">
			  <tr class="etiquetas">
				<td width="20%">Nomina</td>
				<td width="20%">Capital&nbsp;pagado</td>
				<td width="20%">Interes&nbsp;pagado</td>
				<td width="20%">Total</td>
			  </tr>
			  <tr class="etiquetas">
				<td align="center" colspan="5">No existen detalles asociados a este prestamo</td>
			</table>		
	<?php } ?>
<?php } ?><input name="capital_pagar" id="capital_pagar" type="hidden" value="<?PHP echo $capital_pagar; ?>" /><input name="inter_ppp_h" id="inter_ppp_h" type="hidden" value="<?PHP echo $inter_ppp; ?>" /><input name="total_ppp_h" id="total_ppp_h" type="hidden" value="<?PHP echo $total_ppp; ?>" /><input name="codg_prst" id="codg_prst" type="hidden" value="<?PHP echo $codg_prst; ?>" />
