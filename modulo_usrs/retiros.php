<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$cedu_soci=$_POST['cedu_soci'];
$codg_benf=$_POST['codg_benf'];
$tipo_reti=$_POST['tipo_reti'];
$max_reti=$_POST['max_reti'];
$saldo=$_POST['sum_saldo'];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'retiros.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "retiros";	// nombre de la tabla
$ncampos = "10";			//numero de campos del formulario
$datos[0] = crear_datos ("fcha_reti","Fecha de Solicitud",$_POST['fcha_reti'],"10","10","fecha");
$datos[1] = crear_datos ("cedu_soci","Ahorrista",$_POST['cedu_soci'],"1","12","numericos");
$datos[2] = crear_datos ("tipo_reti","Tipo de Retiro",$_POST['tipo_reti'],"1","12","alfabeticos");
$datos[3] = crear_datos ("codg_benf","Cedula de Beneficiario",$_POST['codg_benf'],"0","11","alfanumericos");
$datos[4] = crear_datos ("stat_reti","Estado de Retiro",$_POST['stat_reti'],"1","11","alfabeticos");
$datos[5] = crear_datos ("numr_acta","Numero de Acta",$_POST['numr_acta'],"1","20","numericos");
$datos[6] = crear_datos ("fcha_acta","Fecha del Acta",$_POST['fcha_acta'],"0","10","fecha");
$datos[7] = crear_datos ("obsr_reti","Observaciones",$_POST['obsr_reti'],"0","255","alfanumericos");
$datos[8] = crear_datos ("mont_reti","Monto del Retiro",$_POST['mont_reti'],"0","12","decimal");
$datos[9] = crear_datos ("bloq_reti","Bloquear Retiro",$_POST['bloq_reti'],"0","1","numericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Fecha";
		$datos[0]="fcha_reti";
		$parametro[1]="Ahorrista";
		$datos[1]="cedu_soci";
		$parametro[2]="Tipo de Retiro";
		$datos[2]="tipo_reti";
		$parametro[3]="Estatus";
		$datos[3]="stat_reti";
		busqueda_varios(6,$buscando,$datos,$parametro,"codg_reti");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_reti = $row["codg_reti"];
	    $cedu_soci = $row["cedu_soci"];
	    $tipo_reti = $row["tipo_reti"];
	    $codg_benf = $row["codg_benf"];
	    $stat_reti = $row["stat_reti"];
		$numr_acta = $row["numr_acta"];
		$fcha_acta = $row["fcha_acta"];
		$fcha_reti = $row["fcha_reti"];
	    $obsr_reti = $row["obsr_reti"];
	    $mont_reti = $row["mont_reti"];
		$bloq_reti = $row["bloq_reti"];
	    
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);

	if ($validacion) {	
		modificar_func($ncampos,$datos,$tabla,"codg_reti",$_POST["codg_reti"],$pagina,"");
		if($datos[9][2]=="0"){
			////////Actualizar estado del socio si el retiro es total o por muerte
			if($datos[4][2]=="A" && ($datos[2][2]=="T" || $datos[2][2]=="M")){
				if($datos[2][2]=="T"){ $retiro="retiro total voluntario"; }else{ $retiro="retiro por fallecimiento"; }
				$sql_soci="INSERT INTO socios_estado(cedu_soci,codg_stat,numr_estd,numr_acta,fcha_estd,obse_estd,codg_soci) values('".$datos[1][2]."','4','0','".$datos[5][2]."','".$datos[6][2]."','Se realiza el $retiro del socio', '0')";
				mysql_query($sql_soci);
			}
			/////////Actualizar saldo si el retiro total o percial fue aprobado.
			if($datos[4][2]=="A"){
				$saldo=buscar_campo('apor_sald, rete_sald', 'socios_saldo', 'WHERE cedu_soci='.$datos[1][2]);
				$apor_sald=$saldo['apor_sald']; $rete_sald=$saldo['rete_sald'];
				$saldo_paga=$apor_sald+$rete_sald;
				$monto_reti = (($datos[8][2])/2);
				$apor_sald=$apor_sald-$monto_reti;
				$rete_sald=$rete_sald-$monto_reti;
				if($datos[2][2]=="T"){ $retiro="retiro total voluntario"; }elseif($datos[2][2]=="M"){ $retiro="retiro por fallecimiento"; }else{ $retiro="retiro parcial de ahorros"; }
				$sql_movi="INSERT INTO socios_movimientos(cedu_soci,fcha_movi,conc_movi,tipo_movi,dest_movi,mont_movi,orig_movi) values('".$datos[1][2]."','".$datos[6][2]."','Se realiza el $retiro del socio','E','A','$monto_reti','A')";
				mysql_query($sql_movi);
				$sql_movi="INSERT INTO socios_movimientos(cedu_soci,fcha_movi,conc_movi,tipo_movi,dest_movi,mont_movi,orig_movi) values('".$datos[1][2]."','".$datos[6][2]."','Se realiza el $retiro del socio','E','R','$monto_reti','A')";	
				mysql_query($sql_movi);
				$sql_reti="UPDATE socios_saldo SET apor_sald='$apor_sald', rete_sald='$rete_sald', fcha_sald='".$datos[6][2]."' WHERE cedu_soci=".$datos[1][2]."";
				mysql_query($sql_reti);
				$sql_bloq="UPDATE retiros SET bloq_reti='1' WHERE codg_reti=".$_POST["codg_reti"];
				mysql_query($sql_bloq);
			}
		}
		
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	////// validar monto maximo para retiros
	/* if($max_reti<$datos[8][2]){
		echo "<script>alert('No puede exceder el maximo de retiro estipulado')</script>";
		$boton = "Verificar";
	}*/
	////////////////////////////////////////
}
if ($_POST["confirmar"]=="Guardar")
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_reti"],"codg_reti",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
//echo $boton;
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo"> Registro de Retiros</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Fecha de Registro:<?php escribir_campo('codg_reti',$_POST["codg_reti"],$codg_reti,'',12,15,'Codigo del retiro',$boton,$existe,'','','oculto'); ?></td>
                        <td width="75%"><?php escribir_campo('fcha_reti',$_POST["fcha_reti"],$fcha_reti,'',10,10,'Fecha de Registro del Retiro',$boton,$existe,'fecha','',''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Ahorrista&nbsp;Relacionado:                          </td>
			            <td>
                        <?php if ($cedu_soci=='' && $_POST['cedu_soci']==''){ $vista='vista_socios_activos'; } else { $vista='vista_socios'; } combo('cedu_soci', $cedu_soci, $vista, $link, 0, 0, 1, '', 'cedu_soci', " onchange='submit();' ", $boton, "ORDER BY nomb_soci",''); ?></td>
                      </tr>
                      <tr>
                      	<td class="etiquetas">Tipo de Retiro:</td>
                        <td>
						 <?php if($boton == "Actualizar"){ 
						 		echo '<input type="hidden" name="tipo_reti" id="tipo_reti" value="'.$_POST['tipo_reti'].'" >'; if($_POST['tipo_reti'] == "T"){ echo "Retiro Total"; } if($_POST['tipo_reti'] == "P"){ echo "Retiro Parcial"; } if($_POST['tipo_reti'] == "M"){ echo "Retiro por Fallecimiento"; }
						 	}else{
						 ?> 
							 <?php if ($boton != "Modificar") { echo '<select name="tipo_reti" title="Tipo de Retiro" onchange="submit();">
							  <option>Seleccione...</option>
							  <option value="T" '; if ($tipo_reti == "T" || $_POST['tipo_reti'] =="T") { echo 'selected'; } echo '>Retiro Total</option>
							  <option value="P" '; if ($tipo_reti == "P" || $_POST['tipo_reti'] =="P") { echo 'selected'; } echo '>Retiro Parcial</option>
							  <option value="M" '; if ($tipo_reti == "M" || $_POST['tipo_reti'] =="M") { echo 'selected'; } echo '>Retiro por Fallecimiento</option>
							</select>'; } 
							else 
							{ 
								echo '<input type="hidden" name="tipo_reti" id="tipo_reti" value="'.$tipo_reti.'" >'; 
								if ($tipo_reti == "T") { echo 'Retiro Total'; } 
								if ($tipo_reti == "P") { echo 'Retiro Parcial'; }
								if ($tipo_reti == "M") { echo 'Retiro por Fallecimiento'; }
							}
						}?>
						</td>
					  </tr>
					  <?PHP if($tipo_reti == "M" || $_POST['tipo_reti'] =="M"){ ?>
                      <tr>
                        <td width="25%" class="etiquetas">Solicitante:</td>
			            <td>
                        <?php combo('codg_benf', $codg_benf, 'socios_beneficiarios', $link, 0, 0, 6, 5, 'codg_benf', "", $boton, " WHERE cedu_soci=$cedu_soci ORDER BY apel_benf",''); ?></td>
                      </tr>
					  <?PHP }else{ ?>
						<input name="codg_benf" type="hidden" value="NULL" />
					  <?PHP } ?>
                      <tr>
                      	<td class="etiquetas">Estatus del Retiro:</td>
                        <td>
						<?php if($boton=="Verificar" OR $boton=="Guardar"){ 
							echo '<input type="hidden" name="stat_reti" id="stat_reti" value="P" >'; echo "Pendiente";
						}elseif($boton=="Actualizar" AND $_POST["bloq_reti"]=='1'){
							echo '<input type="hidden" name="stat_reti" id="stat_reti" value="'.$_POST['stat_reti'].'" >'; if($_POST['stat_reti'] == "P"){ echo "Pendiente"; } if($_POST['stat_reti'] == "A"){ echo "Aprobado"; } if($_POST['stat_reti'] == "N"){ echo "Negado"; }
						}else{ ?>
                            <?php if ($boton != "Modificar" && $boton != "Guardar") { echo '<select name="stat_reti" title="Estatus del Retiro">
                          <option value="">Seleccione...</option>
						  <option value="P" '; if ($stat_reti == "P" || $_POST['stat_reti'] =="P") { echo 'selected'; } echo '>Pendiente</option>
                          <option value="A" '; if ($stat_reti == "A" || $_POST['stat_reti'] =="A") { echo 'selected'; } echo '>Aprobado</option>
                          <option value="N" '; if ($stat_reti == "N" || $_POST['stat_reti'] =="N") { echo 'selected'; } echo '>Negado</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="stat_reti" id="stat_reti" value="'.$stat_reti.'" >'; 
							if ($stat_reti == "P") { echo 'Pendiente'; }
						    if ($stat_reti == "A") { echo 'Aprobado'; } 
							if ($stat_reti == "N") { echo 'Negado'; }
						}?>
						<?php } ?>
					  </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N� de Acta:</td>
										<?php if($boton=="Verificar" OR $boton=="Guardar"){ $oculto_na="oculto"; $_POST["numr_acta"]=0; $numr_acta=0; $cero_na=0; }else{ $oculto_na=""; $cero_na=""; } ?>
                        <td width="75%"><?php escribir_campo('numr_acta',$_POST["numr_acta"],$numr_acta,'',12,10,'Numero de Acta',$boton,$existe,'','',$oculto_na); echo $cero_na; ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha&nbsp;de&nbsp;Acta:</td>
										<?php if($boton=="Verificar" OR $boton=="Guardar"){ $oculto_fa="oculto"; $_POST["fcha_acta"]=0; $fcha_acta=0; $cero_fa="0000-00-00"; }else{ $oculto_fa=""; $cero_fa=""; } ?>
										<?php if($fcha_acta=="0000-00-00"){ $fcha_acta=""; } ?>
                        <td width="75%"><?php escribir_campo('fcha_acta',$_POST["fcha_acta"],$fcha_acta,'',10,10,'Fecha de Acta',$boton,$existe,'fecha','',$oculto_fa); echo $cero_fa; ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td width="75%"><?php echo escribir_area('obsr_reti',$_POST["obsr_reti"],$obsr_reti,'',30,3,'Observaciones adicionales sobre el MONTEPIO',$boton,$existe,''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto del Retiro:</td>
						<?PHP $val_prm = buscar_campo('val_val', 'valores', 'WHERE des_val="PRM"' ); ?>
						<?PHP $sum_saldo = buscar_campo('(apor_sald + rete_sald + apor_comp + rete_comp) AS sum_saldo ', 'socios_saldo', 'WHERE cedu_soci='.$cedu_soci);  if($boton!="Actualizar" AND $boton!="Guardar" AND $saldo!="" AND ($tipo_reti=="T" OR $tipo_reti=="M")){ $mont_reti=redondear($saldo,2,"","."); $_POST["mont_reti"]=redondear($saldo,2,"","."); }elseif($boton!="Actualizar" AND $boton!="Guardar" AND $saldo!="" AND $tipo_reti=="P"){ $mont_reti=redondear(($saldo*$val_prm['val_val']),2,"","."); $_POST["mont_reti"]=redondear(($saldo*$val_prm['val_val']),2,"","."); $readonly=''; }else{ $readonly=''; } ?>
						<?PHP if($saldo!="" AND ($tipo_reti=="T" OR $tipo_reti=="M")){ $max_reti=$saldo; }elseif($saldo!="" AND $tipo_reti=="P"){ $max_reti=($saldo*$val_prm['val_val']); } ?>
						<input name="sum_saldo" type="hidden" value="<?PHP echo $sum_saldo['sum_saldo']; ?>" />
						<input name="max_reti" type="hidden" value="<?PHP echo $max_reti; ?>" />
                        <td width="75%"><?php escribir_campo('mont_reti',$_POST["mont_reti"],$mont_reti,'',11,10,'Monto de Retiro',$boton,$existe,'',$readonly,''); ?><?PHP  if($saldo!="" AND ($tipo_reti=="P")){ echo "&nbsp;Maximo: ".redondear(($saldo*$val_prm['val_val']),2,"","."); } ?></td>
                      	<?php  escribir_campo('bloq_reti',$_POST["bloq_reti"],$bloq_reti,'',11,10,'Monto de Retiro',$boton,$existe,'','','oculto'); ?>
					  </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
				  <tr>
					<td>
					<?php 
						$ncriterios =2;
						$criterios[0] = "Fecha"; 
						$campos[0] ="fcha_reti";
						$criterios[1] = "C�dula"; 
						$campos[1] ="cedu_soci";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     $funcion_combo = '"criterio1.checked=true; valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
                                             echo '<center>Buscar C�dula: '; 
                                             combo('cedu_soci2', $cedu_soci, 'vista_socios', $link, 0, 0, 1, '', 'cedu_soci', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nomb_soci",'');
                                             echo '</center>'; 
				   	   }  ?>
				   	   </td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
