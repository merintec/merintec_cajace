<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$codg_pago=$_POST['codg_pago'];
$cedu_soci=$_POST['cedu_soci'];
$codg_prst=$_POST['codg_prst'];
$codg_cnta=$_POST['codg_cnta'];
$numr_cuot=$_POST['numr_cuot'];
$mess_deud=$_POST['mess_deud'];
$dest_exce=$_POST['dest_exce'];
$mont_exce=$_POST['mont_exce'];

$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'abonos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "abonos";	// nombre de la tabla
$ncampos = "11";			//numero de campos del formulario
$datos[0] = crear_datos ("codg_prst","Codigo de Prestamo",$_POST['codg_prst'],"1","11","numericos");
$datos[1] = crear_datos ("cedu_soci","Ahorrista",$_POST['cedu_soci'],"1","12","numericos");
$datos[2] = crear_datos ("fcha_regi","Fecha de Registro",$_POST['fcha_regi'],"10","10","fecha");
$datos[3] = crear_datos ("codg_cnta","Cuenta Destino",$_POST['codg_cnta'],"0","11","numericos");
$datos[4] = crear_datos ("desc_pago","Descripcion de Pago",$_POST['desc_pago'],"1","255","alfanumericos");
$datos[5] = crear_datos ("numr_refe","Numero de Operacion",$_POST['numr_refe'],"1","11","numericos");
$datos[6] = crear_datos ("fcha_pago","Fecha de Pago",$_POST['fcha_pago'],"10","10","fecha");
$datos[7] = crear_datos ("mess_deud","Meses de Deuda",$_POST['mess_deud'],"1","11","numericos");
$datos[8] = crear_datos ("dest_exce","Destino excedente",$_POST['dest_exce'],"1","1","numericos");
$datos[9] = crear_datos ("mont_pago","Monto del pago",$_POST['mont_pago'],"0","12","decimal");
$datos[10] = crear_datos ("obsr_pago","Observaciones",$_POST['obsr_pago'],"0","255","alfanumericos");


if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Fecha";
		$datos[0]="fcha_pago";
		$parametro[1]="Ahorrista";
		$datos[1]="cedu_soci";
		$parametro[2]="Nro de Cuotas";
		$datos[2]="numr_cuot";
		$parametro[3]="Monto de las Cuotas";
		$datos[3]="mont_cuot";
		busqueda_varios(6,$buscando,$datos,$parametro,"codg_pago");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_pago = $row["codg_pago"];
		$codg_prst = $row["codg_prst"];
	    $cedu_soci = $row["cedu_soci"];
	    $codg_cnta = $row["codg_cnta"];
	    $banc_orig = $row["banc_orig"];
	    $numr_refe = $row["numr_refe"];
		$fcha_pago = $row["fcha_pago"];
		$numr_cuot = $row["numr_cuot"];
		$mont_cuot = $row["mont_cuot"];
		$mont_adic = $row["mont_adic"];
	    $obsr_pago = $row["obsr_pago"];
		$fcha_regi = $row["fcha_regi"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion){
		modificar_func($ncampos,$datos,$tabla,"codg_pago",$_POST["codg_pago"],$pagina,"1");
		$plaz_prst = buscar_campo('plaz_prst', 'prestamos', 'WHERE codg_prst='.$datos[0][2]);  
		$tipo_cuot = buscar_campo('tipo_cuot', 'prestamos', 'WHERE codg_prst='.$datos[0][2]); 
		if($tipo_cuot['tipo_cuot']=="S"){ $cant=$plaz_prst['plaz_prst']*4; }elseif($tipo_cuot['tipo_cuot']=="Q"){ $cant=$plaz_prst['plaz_prst']*2; }else{ $cant=$plaz_prst['plaz_prst']; }
			
		$cont_nomin = buscar_campo('COUNT(rela_dlle) AS cont_nomin ', 'nominas_detalle', 'WHERE moti_dlle="Pr�stamo" AND (rela_dlle='.$datos[0][2].' OR rela_dlle='.$_POST['codg_prst'].')');
		$cont_abono = buscar_campo('SUM(numr_cuot) AS cont_abono ', 'abonos', 'WHERE codg_prst='.$datos[0][2].' OR codg_prst='.$_POST['codg_prst'].' GROUP BY codg_prst');
		$sum_cont=($cont_nomin['cont_nomin']+$cont_abono['cont_abono']);
		
		if($cant==$sum_cont){
			$sql="update prestamos set stat_prst='P' WHERE codg_prst=".$datos[0][2];
			mysql_query($sql);	
		}else{
			$sql="update prestamos set stat_prst='A' WHERE codg_prst=".$datos[0][2];
			mysql_query($sql);			
		}
                // Actualizamos el Movimiento de Prestamos
	// CONSULTAMOS EL SALDO INICIAL DEL PRESTAMO
	    $sql_prestamo_mov = "SELECT * FROM vista_prestamos WHERE codg_prst=".$_POST['codg_prst'].";";
	    $row_prst = mysql_fetch_array(mysql_query($sql_prestamo_mov));
	    $saldo_inicial = $row_prst["mont_apro"];
	        $interes = $row_prst["mont_intr"];
	        $tipo_cuot = $row_prst["tipo_cuot"];
	        switch ($tipo_cuot):
                    case "Q":
                        $interes = $interes/2;
                        break;
                    case "S":
                        $interes = $interes/4;
                        break;
                    default:
                        $interes = 1;
                endswitch;
	    // CONSULTAMOS LOS MOVIMIENTOS DE PRESTAMO PARA OBTENER EL SALDO PAGADO
	    $sql_prestamo_mov = "SELECT SUM(capi_prtm) as prestamo_pagado from prestamos_mov WHERE codg_prst=".$_POST['codg_prst']." AND (rela_prtm!=".$_POST['codg_pago']." AND orgn_prtm='Abono de Pr�stamos');";
	    $row_pag = mysql_fetch_array(mysql_query($sql_prestamo_mov));
	    $prestamo_pagado = $row_pag["prestamo_pagado"];
	    
	    // calculamos el saldo restando el inicial - el pagado
	    $prestamos_saldo = redondear(($saldo_inicial-$prestamo_pagado),2,'','.');
	    for ($i=1;$i<=$_POST['numr_cuot'];$i++){
	        $monto_interes_ind = redondear(($prestamos_saldo*($interes/12/100)),2,'','.');
	        $monto_capital_ind = redondear(($_POST["mont_cuot"]-$monto_interes_ind),2,'','.');
	        $monto_interes += $monto_interes_ind;
	        $monto_capital += $monto_capital_ind;
	        $monto_total += $_POST["mont_cuot"];
	        $prestamos_saldo -= $monto_capital_ind; 
            }
            if ($_POST['mont_adic']!=''){
                $monto_capital += $_POST['mont_adic'];
                $monto_total += $_POST['mont_adic'];
            }
	    // verificamos si el saldo es menor al monto de la amortizacion de capital capital se suma la diferencia a interes
	    if ($prestamos_saldo<$monto_capital_ind){
	        $monto_excedente = $monto_capital_ind - $prestamos_saldo;
	        $monto_capital = $prestamos_saldo;
	        $monto_interes += $monto_excedente;
	    }
	    $sql_prestamo_mov = "UPDATE prestamos_mov SET mnto_prtm=".$monto_total.", capi_prtm=".$monto_capital.",inte_prtm=".$monto_interes."  WHERE rela_prtm=".$_POST['codg_pago']." AND orgn_prtm='Abono de Pr�stamos';";
	    mysql_query($sql_prestamo_mov);
	    auditoria_func ('modificar', '', $_POST["ant"], $tabla);
	    echo '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	$id = insertar_func_return($ncampos,$datos,$tabla);
	$id_asie = insertar_func_nomina($ncampos2,$datos2,$tabla2);
	if($dest_exce=='1' && $mont_exce>0){
		$sql_exce = "insert into socios_movimientos (cedu_soci,fcha_movi,conc_movi,tipo_movi,dest_movi,mont_movi,orig_movi) values ('".$_POST['cedu_soci']."','".$_POST['fcha_regi']."','Excedente por abono de prestamo','I','A','".$mont_exce."','A')";
		mysql_query($sql_exce);
		$socio_saldo = buscar_registro('socios_saldo', '*', ' WHERE cedu_soci='.$_POST['cedu_soci'].'', 'registro');
		if($socio_saldo){
			$saldo_nuevo=redondear(($socio_saldo['rete_sald']+$mont_exce),2,'','.');
			$sql_sald="UPDATE socios_saldo SET fcha_sald='".date('Y-m-d')."', rete_sald='".$saldo_nuevo."' WHERE cedu_soci='".$_POST['cedu_soci']."'";
		}else{
			$sql_sald='insert into socios_saldo(cedu_soci,fcha_sald,apor_sald,rete_sald,rein_sald,apor_comp,rete_comp,rein_comp) values("'.$_POST['cedu_soci'].'","'.date('Y-m-d').'","0.00","'.$mont_exce.'","0.00","0.00","0.00","0.00")';
		}
		mysql_query($sql_sald);
	}
	
	if($dest_exce=='2' && $mont_exce>0){
		$valores = buscar_registro('valores','val_val'," WHERE des_val='OTR_ING' AND con_val='Otros Ingresos'", 'registro');
		$cuenta = buscar_registro('cuentas','nmro_cnta, nomb_cnta'," WHERE codg_pcnta='".redondear($valores['val_val'],0,'','')."'", 'registro');
		$sql_exce = "insert into cuentas_movimientos (nmro_cnta,nomb_cnta,debe_movi,habe_movi,orig_movi,rela_movi) values ('".$cuenta['nmro_cnta']."','".$cuenta['nomb_cnta']."','','".$mont_exce."','Excedente por abono de prestamo','".$id."')";
		mysql_query($sql_exce);
	}	
	
	
	$plaz_prst = buscar_campo('plaz_prst', 'prestamos', 'WHERE codg_prst='.$datos[0][2]);  
	$tipo_cuot = buscar_campo('tipo_cuot', 'prestamos', 'WHERE codg_prst='.$datos[0][2]); 
	if($tipo_cuot['tipo_cuot']=="S"){ $cant=$plaz_prst['plaz_prst']*4; }elseif($tipo_cuot['tipo_cuot']=="Q"){ $cant=$plaz_prst['plaz_prst']*2; }else{ $cant=$plaz_prst['plaz_prst']; }
		
	$cont_nomin = buscar_campo('COUNT(rela_dlle) AS cont_nomin ', 'nominas_detalle', 'WHERE moti_dlle="Pr�stamo" AND (rela_dlle='.$datos[0][2].' OR rela_dlle='.$_POST['codg_prst'].')');
	$cont_abono = buscar_campo('SUM(numr_cuot) AS cont_abono ', 'abonos', 'WHERE codg_prst='.$datos[0][2].' OR codg_prst='.$_POST['codg_prst'].' GROUP BY codg_prst');
	$sum_cont=($cont_nomin['cont_nomin']+$cont_abono['cont_abono']);
	
	if($cant==$sum_cont){
		$sql="update prestamos set stat_prst='P' WHERE codg_prst=".$datos[0][2];
		mysql_query($sql);	
	}else{
		$sql="update prestamos set stat_prst='A' WHERE codg_prst=".$datos[0][2];
		mysql_query($sql);			
	}
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	
	
	// Registrar Movimiento de Prestamos
	// CONSULTAMOS EL SALDO INICIAL DEL PRESTAMO
	    $sql_prestamo_mov = "SELECT * FROM vista_prestamos WHERE codg_prst=".$_POST['codg_prst'].";";
	    $row_prst = mysql_fetch_array(mysql_query($sql_prestamo_mov));
	    $saldo_inicial = $row_prst["mont_apro"];
	        $interes = $row_prst["mont_intr"];
	        $tipo_cuot = $row_prst["tipo_cuot"];
	        switch ($tipo_cuot):
                    case "Q":
                        $interes = $interes/2;
                        break;
                    case "S":
                        $interes = $interes/4;
                        break;
                    default:
                        $interes = 1;
                endswitch;
	    // CONSULTAMOS LOS MOVIMIENTOS DE PRESTAMO PARA OBTENER EL SALDO PAGADO
	    $sql_prestamo_mov = "SELECT SUM(capi_prtm) as prestamo_pagado from prestamos_mov WHERE codg_prst=".$_POST['codg_prst'].";";
	    $row_pag = mysql_fetch_array(mysql_query($sql_prestamo_mov));
	    $prestamo_pagado = $row_pag["prestamo_pagado"];
	    
	    // calculamos el saldo restando el inicial - el pagado
	    $prestamos_saldo = redondear(($saldo_inicial-$prestamo_pagado),2,'','.');
	    for ($i=1;$i<=$_POST['numr_cuot'];$i++){
	        $monto_interes_ind = redondear(($prestamos_saldo*($interes/12/100)),2,'','.');
	        $monto_capital_ind = redondear(($_POST["mont_cuot"]-$monto_interes_ind),2,'','.');
	        $monto_interes += $monto_interes_ind;
	        $monto_capital += $monto_capital_ind;
	        $monto_total += $_POST["mont_cuot"];
	        $prestamos_saldo -= $monto_capital_ind; 
            }
            if ($_POST['mont_adic']!=''){
                $monto_capital += $_POST['mont_adic'];
                $monto_total += $_POST['mont_adic'];
            }
	    // verificamos si el saldo es menor al monto de la amortizacion de capital capital se suma la diferencia a interes
	    if ($prestamos_saldo<$monto_capital_ind){
	        $monto_excedente = $monto_capital_ind - $prestamos_saldo;
	        $monto_capital = $prestamos_saldo;
	        $monto_interes += $monto_excedente;
	    }
	    $sql_prestamo_mov = "INSERT INTO prestamos_mov (codg_prst,mnto_prtm,capi_prtm,inte_prtm,orgn_prtm,rela_prtm,freg_prtm,fpag_prtm) ";
	    $sql_prestamo_mov .= "VALUES(".$_POST['codg_prst'].",".$monto_total.",".$monto_capital.",".$monto_interes.",'Abono de Pr�stamos',".$id.",'".$_POST['fcha_pago']."','".$_POST['fcha_pago']."')"; 
	    mysql_query($sql_prestamo_mov);
	    echo '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_pago"],"codg_pago",$tabla,$pagina);
	/// eliminamos el movimiento de prestamo
	$sql_movimiento = "DELETE FROM prestamos_mov WHERE rela_prtm=".$_POST["codg_pago"]." AND orgn_prtm='Abono de Pr�stamos';";
        mysql_query($sql_movimiento);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}

	$plaz_prst = buscar_campo('plaz_prst', 'prestamos', 'WHERE codg_prst='.$codg_prst);  
	$tipo_cuot = buscar_campo('tipo_cuot', 'prestamos', 'WHERE codg_prst='.$codg_prst); 
	if($tipo_cuot['tipo_cuot']=="S"){ $cant_cuot=$plaz_prst['plaz_prst']*4; }elseif($tipo_cuot['tipo_cuot']=="Q"){ $cant_cuot=$plaz_prst['plaz_prst']*2; }else{ $cant_cuot=$plaz_prst['plaz_prst']; }
								
	$cont_nomin = buscar_campo('COUNT(rela_dlle) AS cont_nomin ', 'nominas_detalle', 'WHERE moti_dlle="Pr�stamo" AND (rela_dlle='.$codg_prst.')');
	$cont_abono = buscar_campo('SUM(numr_cuot) AS cont_abono ', 'abonos', 'WHERE codg_prst='.$codg_prst.' GROUP BY codg_prst');
	$cant_paga=($cont_nomin['cont_nomin']+$cont_abono['cont_abono']);
	if($boton=="Modificar" AND $cant_cuot==$cant_paga){
		echo  "<script>alert('El Prestamo ya esta pagado, si lo modifica, volvera a estar por pagar');</script>";
		$boton="Modificar";
	}
        if($codg_pago!="" AND ($boton!="Verificar" OR $boton!="Guardar")){
	$boton="Actualizar";
        }
        if ($buscando){
	    $boton = "Modificar";
	}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Registro de Abonos</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Hoy:</td>
                        <td width="75%"><?php escribir_campo('fcha_regi',$_POST["fcha_regi"],$fcha_regi,'',10,10,'Fecha de registro del Abono',$boton,$existe,'fecha_','readonly',''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Ahorrista&nbsp;Relacionado:<?php escribir_campo('codg_pago',$_POST["codg_pago"],$codg_pago,'',12,15,'Codigo del Abono',$boton,$existe,'','','oculto'); ?></td>
                        <td width="75%"><?php if ($cedu_soci=='' && $_POST['cedu_soci']==''){ $vista='vista_socios_activos'; } else { $vista='vista_socios'; } combo('cedu_soci', $cedu_soci, $vista, $link, 0, 0, 1, '', 'cedu_soci', " onchange='submit();' ", $boton, "ORDER BY nomb_soci",''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Prestamos&nbsp;del&nbsp;Ahorrista:</td><?php if($boton!="Actualizar"){ $comp_comb="AND stat_prst<>'P'"; }else{ $comp_comb="AND (stat_prst='P' OR stat_prst='A')"; $_POST["cedu_soci"]=$cedu_soci; } ?>
                        <td width="75%">
						<?php
							if($_POST["cedu_soci"]){ $prestamos = buscar_registro('vista_prestamos', '*', ' WHERE cedu_soci='.$_POST['cedu_soci'].' '.$comp_comb.'', 'cantidad'); 
							if ($prestamos == 0){ echo "<script>alert('No posee prestamos activos');</script>"; } }
							combo('codg_prst', $codg_prst, 'vista_prestamos', $link, 0, 12, 20, 15, 'codg_prst', " onchange='submit();' ", $boton, " WHERE cedu_soci=".$_POST["cedu_soci"]." ".$comp_comb." ORDER BY codg_prst DESC",''); 
						?></td>
                      </tr>
						<?php 
							if($_POST["codg_prst"]){ $prestamo = buscar_registro('vista_prestamos', '*', ' WHERE codg_prst='.$_POST['codg_prst'].'', 'registro'); } 
							if ($prestamo){ 
						?>
                      <tr>
                        <td colspan="2"><table width="100%" border="1" cellspacing="0" cellpadding="0">
										  <tr class="etiquetas">
											<td width="25%">Tipo&nbsp;de&nbsp;Prestamo</td>
											<td width="25%">Capital&nbsp;por&nbsp;pagar</td>
											<td width="25%">Interes&nbsp;de&nbsp;un&nbsp;mes</td>
											<td width="25%">Total</td>
										  </tr>
										  <tr class="etiquetas">
											<td><?php echo $prestamo['nomb_tipo_pres']; ?></td>
											<td align="right"><?php $capital_pagar=($prestamo['mont_apro']-$prestamo['capi_prtm']); echo redondear($capital_pagar,2,".",","); ?>&nbsp;</td>
											<td align="right"><?php $interes_mes=($capital_pagar*(($prestamo['mont_intr']/100)/360*30)); echo redondear($interes_mes,2,".",","); ?>&nbsp;</td>
											<td align="right"><?php echo redondear($capital_pagar+$interes_mes,2,".",","); ?></td>
										  </tr>
										</table><br>
										<?php 
											$pres_detalle = buscar_registro('nominas_detalle nd, nominas n', 'nd.codg_dlle, nd.mnto_dlle, n.anno_nmna, n.mess_nmna, n.prdo_nmna', ' WHERE moti_dlle = "Pr�stamo" AND n.codg_nmna = nd.codg_nmna AND rela_dlle = '.$_POST['codg_prst'].' ORDER BY n.anno_nmna DESC, n.mess_nmna DESC, n.prdo_nmna DESC LIMIT 1', 'registro'); 
											if($pres_detalle){
												$prestamos_mov = buscar_registro('prestamos_mov pm', '*', ' WHERE pm.orgn_prtm = "Registro de N�mina" AND pm.rela_prtm = '.$pres_detalle['codg_dlle'].'', 'registro'); 
										?>
										<table width="100%" border="1" cellspacing="0" cellpadding="0">
										  <tr class="etiquetas">
											<td width="20%">Nomina</td>
											<td width="20%">Capital&nbsp;pagado</td>
											<td width="20%">Interes&nbsp;pagado</td>
											<td width="20%">Total</td>
										  </tr>
										  <tr class="etiquetas">
											<td><?php if($pres_detalle['prdo_nmna']>10){ echo 'Semana-'.($pres_detalle['prdo_nmna']-10); }elseif($pres_detalle['prdo_nmna']==8){ escribir_mes($pres_detalle['mess_nmna']); echo '-'.$pres_detalle['anno_nmna']; }else{ echo 'Quincena '.($pres_detalle['prdo_nmna']-5).' de '; escribir_mes($pres_detalle['mess_nmna']); echo '-'.$pres_detalle['anno_nmna']; } ?></td>
											<td align="right"><?php echo redondear($prestamos_mov['capi_prtm'],2,".",","); ?>&nbsp;</td>
											<td align="right"><?php echo redondear($prestamos_mov['inte_prtm'],2,".",","); ?>&nbsp;</td>
											<td align="right"><?php echo redondear($prestamos_mov['capi_prtm']+$prestamos_mov['inte_prtm'],2,".",","); ?></td>
										  </tr>
										</table>
									<?php }else{ ?>
										<table width="100%" border="1" cellspacing="0" cellpadding="0">
										  <tr class="etiquetas">
											<td width="20%">Nomina</td>
											<td width="20%">Capital&nbsp;pagado</td>
											<td width="20%">Interes&nbsp;pagado</td>
											<td width="20%">Total</td>
										  </tr>
										  <tr class="etiquetas">
											<td align="center" colspan="5">No existen detalles asociados a este prestamo</td>
										</table>									
									<?php } ?>
						</td>
                      </tr>						
						<?php } ?>
                      <tr>
                        <td width="25%" class="etiquetas">Meses Adeudados:</td>
                        <td width="75%"><select name="mess_deud" onchange="submit();">
											<option value="">Seleccione...</option>
											<?php 
												for($i=1;$i<=12;$i++){
													echo '<option value="'.($i).'" '; if($mess_deud==$i){ echo 'selected'; } echo' >'.($i).'</option>';
												}
											?>
										</select></td>
                      </tr>
					  <?php if($mess_deud){ ?>
                      <tr>
                        <td colspan="2"><table width="100%" border="1" cellspacing="0" cellpadding="0">
										  <tr class="etiquetas">
											<td width="25%">Capital&nbsp;que&nbsp;debe</td>
											<td width="25%">Interes x Meses</td>
											<td width="25%">Total</td>
										  </tr>
										  <tr class="etiquetas">
											<td align="right"><?php $capital_pagar=($prestamo['mont_apro']-$prestamo['capi_prtm']); echo redondear($capital_pagar,2,".",","); ?>&nbsp;</td>
											<td align="right"><?php $interes_mes=($capital_pagar*(($prestamo['mont_intr']/100)/360*30)); echo redondear($interes_mes*$mess_deud,2,".",","); ?>&nbsp;</td>
											<td align="right"><?php echo redondear($capital_pagar+($interes_mes*$mess_deud),2,".",","); $deuda=redondear($capital_pagar+($interes_mes*$mess_deud),2,"","."); ?></td>
										  </tr>
										</table>
						</td>
                      </tr>
					  <?php } ?>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha&nbsp;de&nbsp;Abono:</td>
                        <td width="75%"><?php escribir_campo('fcha_pago',$_POST["fcha_pago"],$fcha_pago,'',10,10,'Fecha del deposito o transferencia',$boton,$existe,'hoy','',''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto&nbsp;del&nbsp;pago:</td>
                        <td width="75%"><?php escribir_campo('mont_pago',$_POST["mont_pago"],$mont_pago,'',12,10,'Monto del pago',$boton,$existe,'','onchange="submit();"',''); ?></b></td>
                      </tr>
					  <?php if($_POST["mont_pago"]){ ?>
                      <tr>
                        <td colspan="2"><table width="100%" border="1" cellspacing="0" cellpadding="0">
										  <tr class="etiquetas">
											<td width="25%">Capital</td>
											<td width="25%">Interes <?php echo $mess_deud; ?> Meses</td>
											<td width="25%">Total</td>
										  </tr>
										  <tr class="etiquetas">
											<td align="right"><?php $capital=redondear($_POST["mont_pago"]-($interes_mes*$mess_deud),2,"","."); echo redondear($capital,2,'.',','); ?>&nbsp;</td>
											<td align="right"><?php echo redondear($interes_mes*$mess_deud,2,".",","); ?>&nbsp;</td>
											<td align="right"><?php echo redondear($capital+($interes_mes*$mess_deud),2,".",","); ?></td>
										  </tr>
										</table>
						</td>
                      </tr>
					  <?php }
					  		if($capital<0){
								echo "<script>alert('No se puede guardar. Capital es menor a Cero (0)')</script>";
								$prm[1]='';
							}
					  ?>
					  <?php 
					  		if($_POST["mont_pago"]>$deuda){ 
								$execedente=$_POST["mont_pago"]-$deuda;
					  			if(!$dest_exce){ echo "<script>alert('Monto del pago es mayor a la deuda. Excedente=".$execedente.", indique a donde desea enviarlo')</script>"; }					  
					  ?>
                      <tr>
                        <td colspan="2"><table width="100%" border="1" cellspacing="0" cellpadding="0">
										  <tr class="etiquetas">
											<td width="80%">&nbsp;Destino&nbsp;de&nbsp;excedente (<?php echo $execedente; ?>) <input type="hidden" name="mont_exce" value="<?php echo $execedente; ?>"></td>
											<td width="20%" align="center">Seleccionar</td>
										  </tr>
										  <tr class="etiquetas">
											<td align="left">&nbsp;Ahorros del Asociado</td>
											<td align="right"><input name="dest_exce" type="radio" value="1" <?php if(!$dest_exce){ echo 'checked="checked"'; } if($dest_exce=='1'){ echo 'checked="checked"'; } ?> />&nbsp;</td>
										  </tr>
										  <tr class="etiquetas">
											<td align="left">&nbsp;Otros ingresos de la Caja de Ahorro</td>
											<td align="right"><input name="dest_exce" type="radio" value="2" <?php if($dest_exce=='2'){ echo 'checked="checked"'; } ?> />&nbsp;</td>
										  </tr>
										</table>
						</td>
                      </tr>
					  <?php } ?>
                      <tr>
                      <tr>
                        <td width="25%" class="etiquetas">N�&nbsp;de&nbsp;Referencia:</td>
                        <td width="75%"><?php escribir_campo('numr_refe',$_POST["numr_refe"],$numr_refe,'',12,10,'Numero de Referencia',$boton,$existe,'','',''); ?></td>
                      </tr>
                        <td width="25%" class="etiquetas">Cuenta&nbsp;Destino:</td>
                        <td width="75%"><?php combo('codg_cnta', $codg_cnta, 'banco_cuentas', $link, 0, 0, 1, 2, 'codg_cnta', " onchange='submit();' ", $boton, "",''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Descripcion&nbsp;del&nbsp;Pago:</td>
                        <td width="75%"><?php escribir_campo('desc_pago',$_POST["desc_pago"],$banc_orig,'',255,50,'Banco Origen si es Cheque o Transferencia, de lo contrario colocar Deposito',$boton,$existe,'','',''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td width="75%"><?php echo escribir_area('obsr_pago',$_POST["obsr_pago"],$obsr_pago,'',30,3,'Observaciones adicionales sobre el Abono',$boton,$existe,'');  ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
                    <tr> 
                    <td width="<?PHP echo (100/5)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('../reportes/comprobante_ingresos.php','v_comprobante_ingresos','Comprobante Ingresos',"cedu_soci=".$cedu_soci."codg_pago=".$codg_pago."&seccion=".$_GET['seccion']); } ?></td>  


                  </tr>
                </table>		  </td> </tr>
				  <tr>
					<td>
					<?php 
						$ncriterios =2;
						$criterios[0] = "Fecha"; 
						$campos[0] ="fcha_reti";
						$criterios[1] = "C�dula"; 
						$campos[1] ="cedu_soci";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     $funcion_combo = '"criterio1.checked=true; valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
                                             echo '<center>Buscar C�dula: '; 
                                             combo('cedu_soci2', $cedu_soci, 'vista_socios', $link, 0, 0, 1, '', 'cedu_soci', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nomb_soci",'');
                                             echo '</center>'; 
				   	   }  ?>
				   	   </td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
