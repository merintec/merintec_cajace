<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="../comunes/jquery.min.js"></script>
<?php 
include ('../comunes/formularios_funciones.php');
$cedu_soci=$_POST['cedu_soci'];
$codg_benf=$_POST['codg_benf'];
$tipo_reti=$_POST['tipo_reti'];
$max_reti=$_POST['max_reti'];
$saldo=$_POST['sum_saldo'];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'retiros.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "retiros";	// nombre de la tabla
$ncampos = "10";			//numero de campos del formulario
$datos[0] = crear_datos ("fcha_reti","Fecha de Solicitud",$_POST['fcha_reti'],"10","10","fecha");
$datos[1] = crear_datos ("cedu_soci","Ahorrista",$_POST['cedu_soci'],"1","12","numericos");
$datos[2] = crear_datos ("tipo_reti","Tipo de Retiro",$_POST['tipo_reti'],"1","12","alfabeticos");
$datos[3] = crear_datos ("codg_benf","Cedula de Beneficiario",$_POST['codg_benf'],"0","11","alfanumericos");
$datos[4] = crear_datos ("stat_reti","Estado de Retiro",$_POST['stat_reti'],"1","11","alfabeticos");
$datos[5] = crear_datos ("numr_acta","Numero de Acta",$_POST['numr_acta'],"1","20","numericos");
$datos[6] = crear_datos ("fcha_acta","Fecha del Acta",$_POST['fcha_acta'],"0","10","fecha");
$datos[7] = crear_datos ("obsr_reti","Observaciones",$_POST['obsr_reti'],"0","255","alfanumericos");
$datos[8] = crear_datos ("mont_reti","Monto del Retiro",$_POST['mont_reti'],"0","12","decimal");
$datos[9] = crear_datos ("bloq_reti","Bloquear Retiro",$_POST['bloq_reti'],"0","1","numericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Fecha";
		$datos[0]="fcha_reti";
		$parametro[1]="Ahorrista";
		$datos[1]="cedu_soci";
		$parametro[2]="Tipo de Retiro";
		$datos[2]="tipo_reti";
		$parametro[3]="Estatus";
		$datos[3]="stat_reti";
		busqueda_varios(6,$buscando,$datos,$parametro,"codg_reti");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_reti = $row["codg_reti"];
	    $cedu_soci = $row["cedu_soci"];
	    $tipo_reti = $row["tipo_reti"];
	    $codg_benf = $row["codg_benf"];
	    $stat_reti = $row["stat_reti"];
		$numr_acta = $row["numr_acta"];
		$fcha_acta = $row["fcha_acta"];
		$fcha_reti = $row["fcha_reti"];
	    $obsr_reti = $row["obsr_reti"];
	    $mont_reti = $row["mont_reti"];
		$bloq_reti = $row["bloq_reti"];
	    
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);

	if ($validacion) {	
		modificar_func($ncampos,$datos,$tabla,"codg_reti",$_POST["codg_reti"],$pagina,"");
		if($datos[9][2]=="0"){
			////////Actualizar estado del socio si el retiro es total o por muerte
			if($datos[4][2]=="A" && ($datos[2][2]=="T" || $datos[2][2]=="M")){
				if($datos[2][2]=="T"){ $retiro="retiro total voluntario"; }else{ $retiro="retiro por fallecimiento"; }
				$sql_soci="INSERT INTO socios_estado(cedu_soci,codg_stat,numr_estd,numr_acta,fcha_estd,obse_estd,codg_soci) values('".$datos[1][2]."','4','0','".$datos[5][2]."','".$datos[6][2]."','Se realiza el $retiro del socio', '0')";
				mysql_query($sql_soci);
			}
			/////////Actualizar saldo si el retiro total o percial fue aprobado.
			if($datos[4][2]=="A"){
				$saldo=buscar_campo('apor_sald, rete_sald', 'socios_saldo', 'WHERE cedu_soci='.$datos[1][2]);
				$apor_sald=$saldo['apor_sald']; $rete_sald=$saldo['rete_sald'];
				$saldo_paga=$apor_sald+$rete_sald;
				$monto_reti = (($datos[8][2])/2);
				$apor_sald=$apor_sald-$monto_reti;
				$rete_sald=$rete_sald-$monto_reti;
				if($datos[2][2]=="T"){ $retiro="retiro total voluntario"; }elseif($datos[2][2]=="M"){ $retiro="retiro por fallecimiento"; }else{ $retiro="retiro parcial de ahorros"; }
				$sql_movi="INSERT INTO socios_movimientos(cedu_soci,fcha_movi,conc_movi,tipo_movi,dest_movi,mont_movi,orig_movi) values('".$datos[1][2]."','".$datos[6][2]."','Se realiza el $retiro del socio','E','A','$monto_reti','A')";
				mysql_query($sql_movi);
				$sql_movi="INSERT INTO socios_movimientos(cedu_soci,fcha_movi,conc_movi,tipo_movi,dest_movi,mont_movi,orig_movi) values('".$datos[1][2]."','".$datos[6][2]."','Se realiza el $retiro del socio','E','R','$monto_reti','A')";	
				mysql_query($sql_movi);
				$sql_reti="UPDATE socios_saldo SET apor_sald='$apor_sald', rete_sald='$rete_sald', fcha_sald='".$datos[6][2]."' WHERE cedu_soci=".$datos[1][2]."";
				mysql_query($sql_reti);
				$sql_bloq="UPDATE retiros SET bloq_reti='1' WHERE codg_reti=".$_POST["codg_reti"];
				mysql_query($sql_bloq);
			}
		}
		
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	////// validar monto maximo para retiros
	/* if($max_reti<$datos[8][2]){
		echo "<script>alert('No puede exceder el maximo de retiro estipulado')</script>";
		$boton = "Verificar";
	}*/
	////////////////////////////////////////
}
if ($_POST["confirmar"]=="Guardar")
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_reti"],"codg_reti",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
//echo $boton;
?>

<script type="text/javascript">
	function recalcular() {
		var total_prestamo = 0;
		var total_retirar = 0;
		var garantia=0;
		if ($('#mont_ret_tot').val()){
			var total_retirar = Number($('#mont_ret_tot').val());
		}
		if ($('#suma_prestamos').val()){
			total_prestamo = Number($('#suma_prestamos').val());
		}
		var totalizar = Number(total_retirar - total_prestamo);
		totalizar = totalizar.toFixed(2);
		if ($('#total_garantia').val()){
			garantia = Number($('#total_garantia').val());
		}
		totalizar = Number(totalizar - garantia);
		totalizar = totalizar.toFixed(2);
    	$('#mont_cheq').val(totalizar);
    }
</script>
<script>
function actualizar_capa(contenedor){
	if (contenedor == 'montos_disponibles'){
		var parametros = {
			"cedu_soci" : $('#cedu_soci').val(),			
			"tipo_reti" : $('#tipo_reti').val()
		};
		var url = 'capa_saldosdisponibles_retiro.php';
		if ($('#cedu_soci').val() && $('#tipo_reti').val()){
	       	$('#contenedor_montos_disponibles').css("display","");	
		}
		else{
			$('#contenedor_montos_disponibles').css("display","none");	
		}
	    $.ajax({
		    data:  parametros,
	        url:   url,
	        type:  'post',
	        beforeSend: function () {
	    	    $('#' + contenedor).html("Procesando, espere por favor...");
	        },
	        success:  function (response) {
	        	$('#' + contenedor).html(response);
				actualizar_capa('prestamos_por_pagar');
				actualizar_capa('prestamos_por_pagar_fia');
				recalcular();
	        }
	    });
	}
	if(contenedor=='ultimo_retiro'){
		var parametros = {
			"fecha" : $('#fcha_reti').val(),
			"cedu_soci" : $('#cedu_soci').val()
		};
		var url = 'capa_ultimo_retiro.php';
		if ($('#cedu_soci').val()){
	       	$('#contenedor_ultimo_retiro').css("display","");	
		}
		else{
			$('#contenedor_ultimo_retiro').css("display","none");	
		}
		$('#tipo_reti').val('');
	    $.ajax({
		    data:  parametros,
	        url:   url,
	        type:  'post',
	        beforeSend: function () {
	    	    $('#' + contenedor).html("Procesando, espere por favor...");
	        },
	        success:  function (response) {
	        	$('#' + contenedor).html(response);
				actualizar_capa('montos_disponibles');
	        }
	    });
	}
	if(contenedor=='prestamos_por_pagar_fia'){
		var parametros = {
			"cedu_soci" : $('#cedu_soci').val()
		};
		var url = 'capa_prestamos_fia_retiro.php';
		if ($('#cedu_soci').val() && ($('#tipo_reti').val()=='T' || $('#tipo_reti').val()=='M'  || $('#tipo_reti').val()=='R' )){
	       	$('#contenedor_prestamos_fia').css("display","");
		}
		else{
			$('#contenedor_prestamos_fia').css("display","none");	
		}
	    $.ajax({
		    data:  parametros,
	        url:   url,
	        type:  'post',
	        beforeSend: function () {
	    	    $('#' + contenedor).html("Procesando, espere por favor...");
	        },
	        success:  function (response) {
	        	$('#' + contenedor).html(response);
	        }
	    });
	}
	if(contenedor=='prestamos_por_pagar'){
		$('#suma_prestamos').val('');
		$('#label_suma_prestamos').html('');	
		var parametros = {
			"cedu_soci" : $('#cedu_soci').val(),
			"tipo_accion" : 'prestamo',
			"tipo_reti" : $('#tipo_reti').val()
		};
		var url = 'capa_prestamos_por_pagar_retiro.php';
		if ($('#cedu_soci').val() && $('#tipo_reti').val()!='P' && $('#tipo_reti').val()!=''){
	       	$('#contenedor_prestamos').css("display","");
			$('#suma_prestamos').val('');
			$('#label_suma_prestamos').val('');	
		}
		else{
			$('#contenedor_prestamos').css("display","none");	
		}

	    $.ajax({
		    data:  parametros,
	        url:   url,
	        type:  'post',
	        beforeSend: function () {
	    	    $('#' + contenedor).html("Procesando, espere por favor...");
	        },
	        success:  function (response) {
	        	$('#' + contenedor).html(response);
	        }
	    });
	}
}
</script>

<script>
	function sumar_fianza(origen,monto){
		var estado = $(origen).attr('checked');
		var nuevo_max =  Number($('#total_garantia').val());
		monto = Number(monto);
		if (estado){ 
			nuevo_max = nuevo_max - monto;
		}else{
			nuevo_max = nuevo_max + monto;
		}
		nuevo_max = nuevo_max.toFixed(2);
		$('#total_garantia').val(nuevo_max);
		$('#total_garantia_label').html(nuevo_max);
		recalcular();		
	}

	function suma_prestamo_pagado(origen,monto,destino){
		var indice = $(origen).attr("name");
		indice = indice.split('pagar_ppp');
		indice = indice[1];
		if (!monto){
			alert('Debe especificar cantidad de meses para efectuar calculo de inter�s');
			$(origen).attr('checked', false);
		}
		else {
			var estado = $(origen).attr('checked');
			if (destino=='pagado'){
				var nuevo_max =  Number($('#suma_prestamos').val());
			}
			monto = Number(monto);
			if (estado){ 
				$('#mess_deud' + indice).prop('disabled', 'disabled');
				nuevo_max = nuevo_max + monto;
			}else{
				nuevo_max = nuevo_max - monto;
				$('#mess_deud'  + indice).removeAttr('disabled');
			}
			nuevo_max = nuevo_max.toFixed(2);
			if (destino=='pagado'){
				$('#suma_prestamos').val(nuevo_max);
				$('#label_suma_prestamos').html(nuevo_max);
			}
		}
		recalcular();		
	}
</script>
<script>
	function guardar_retiro(){
		var insertado;
		var url = 'retiro_guardar.php';
	    $.ajax({
            data: $("#form1").serialize(),
	        url:   url,
	        type:  'post',
	        beforeSend: function () {
	    	    $('#Resultado').html("Procesando, espere por favor...");
	        },
	        success:  function (response) {
	        	var respuesta = response;
	        	respuesta = respuesta.split(':::::');
	        	if (respuesta[1]){
	        		insertado = respuesta[1];
	        		$('#Resultado').html('');
	        		buscar_retiro(insertado);
	        	}
	        	else{
	        		$('#Resultado').html(respuesta[0]);
	        	}
	        }
	    });	
	}
	function buscar_retiro(codg_reti){
		var parametros2 = {
			"codg_reti" : codg_reti
		};
		var url2 = 'buscar_retiro.php'
		$.ajax({
		    data:  parametros2,
	        url:   url2,
	        type:  'post',
	        beforeSend: function () {
	    	    $('#Resultado').html("Procesando, espere por favor...");
	        },
	        success:  function (response) {
	        	$('#Resultado').html('');
	        	$('#formulario').html(response);
	        }
	    });
	}
	function eliminar_retiro(codg_reti){
		var parametros2 = {
			"codg_reti" : codg_reti
		};
		var url2 = 'eliminar_retiro.php'
		$.ajax({
		    data:  parametros2,
	        url:   url2,
	        type:  'post',
	        beforeSend: function () {
	    	    $('#Resultado').html("Procesando, espere por favor...");
	        },
	        success:  function (response) {
	        	console.log(response);
	        	$('#Resultado').html('');
	        	alert('Retiro eliminado exitosamente');
	        	window.location=('http://localhost/merintec_cajace_new/modulo_usrs/retiros_nuevas.php?seccion=14&nom_sec=Retiros&rollback=rollback');
	        }
	    });
	}
</script>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo"> Registro de Retiros</td>
                  </tr>
                  <tr>
                    <td width="526">
	                    <div id="formulario">
	                    	<div id="Resultado"></div>
	                    	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
	                      <tr>
	                        <td width="25%" class="etiquetas">Fecha de Registro:<?php escribir_campo('codg_reti',$_POST["codg_reti"],$codg_reti,'',12,15,'Codigo del retiro',$boton,$existe,'','','oculto'); ?></td>
	                        <td width="75%"><?php escribir_campo('fcha_reti',$_POST["fcha_reti"],$fcha_reti,'',10,10,'Fecha de Registro del Retiro',$boton,$existe,'fecha','',''); ?></td>
	                      </tr>
	                      <tr>
	                        <td width="25%" class="etiquetas">Ahorrista&nbsp;Relacionado:                          </td>
				            <td>
	                        <?php if ($cedu_soci=='' && $_POST['cedu_soci']==''){ $vista='vista_socios_activos'; } else { $vista='vista_socios'; } combo('cedu_soci', $cedu_soci, $vista, $link, 0, 0, 1, '', 'cedu_soci', 'href="javascript:;" onchange="actualizar_capa(\'ultimo_retiro\');"', $boton, "ORDER BY nomb_soci",''); ?></td>
	                      </tr>
	                      <tr id="contenedor_ultimo_retiro" style="display:none;" class="etiquetas">
	                      	<td>&nbsp;</td><td>�ltimo Retiro: <span id="ultimo_retiro"></span></td>	
	                      </tr>
	                      <tr>
	                      	<td class="etiquetas">Tipo de Retiro:</td>
	                        <td>
							 <?php if($boton == "Actualizar"){ 
							 		echo '<input type="hidden" name="tipo_reti" id="tipo_reti" value="'.$_POST['tipo_reti'].'" >'; if($_POST['tipo_reti'] == "T"){ echo "Retiro Total"; } if($_POST['tipo_reti'] == "P"){ echo "Retiro Parcial"; } if($_POST['tipo_reti'] == "M"){ echo "Retiro por Fallecimiento"; }
							 	}else{
							 ?> 
								 <?php if ($boton != "Modificar") { echo '<select name="tipo_reti" id="tipo_reti" title="Tipo de Retiro" onchange="actualizar_capa(\'montos_disponibles\');">
								  <option value="">Seleccione...</option>
								  <option id="ret_tot" value="T" '; if ($tipo_reti == "T" || $_POST['tipo_reti'] =="T") { echo 'selected'; } echo '>Retiro Total</option>
								  <option id="ret_par" value="P" '; if ($tipo_reti == "P" || $_POST['tipo_reti'] =="P") { echo 'selected'; } echo '>Retiro Parcial</option>
								  <option value="R" '; if ($tipo_reti == "R" || $_POST['tipo_reti'] =="R") { echo 'selected'; } echo '>Retiro Reintegros</option>
								  <option id="ret_mue" value="M" '; if ($tipo_reti == "M" || $_POST['tipo_reti'] =="M") { echo 'selected'; } echo '>Retiro por Fallecimiento</option>
								</select>'; } 
								else 
								{ 
									echo '<input type="hidden" name="tipo_reti" id="tipo_reti" value="'.$tipo_reti.'" >'; 
									if ($tipo_reti == "T") { echo 'Retiro Total'; } 
									if ($tipo_reti == "P") { echo 'Retiro Parcial'; }
									if ($tipo_reti == "R") { echo 'Retiro Reintegros'; }
									if ($tipo_reti == "M") { echo 'Retiro por Fallecimiento'; }
								}
							}?>
							</td>
						  </tr>
						  <?PHP if($tipo_reti == "M" || $_POST['tipo_reti'] =="M"){ ?>
		                      <tr>
		                        <td width="25%" class="etiquetas">Solicitante:</td>
					            <td>
		                        <?php combo('codg_benf', $codg_benf, 'socios_beneficiarios', $link, 0, 0, 6, 5, 'codg_benf', "", $boton, " WHERE cedu_soci=$cedu_soci ORDER BY apel_benf",''); ?></td>
		                      </tr>
						  <?PHP }else{ ?>
								<input name="codg_benf" type="hidden" value="NULL" />
						  <?PHP } ?>
	 					  <tr id="contenedor_montos_disponibles" style="display: none;">
	                        <td width="25%" class="etiquetas">&nbsp;</td>
	                        <td id="montos_disponibles">&nbsp;</td>
	 					  </tr>
	 					  <tr id="contenedor_prestamos" style="display:none;">
						  	<td>&nbsp;</td>
						  	<td align="center" class="etiquetas">
						  		<table border="0" cellpadding="0" cellspacing="0" class="etiquetas" style="width: 100%; text-align: center;">
						  			<tr>
						  				<td id="prestamos_por_pagar"></td>
						  			</tr>
						  		</table>
						  		<br>
						  		<div align="left">Total de prestamos por pagar: <input type="hidden" id="suma_prestamos"><span id="label_suma_prestamos"></span> Bs.</div>
						  	</td>
						  </tr>
	 					  <tr id="contenedor_prestamos_fia" style="display:none;">
						  	<td>&nbsp;</td>
						  	<td align="center" class="etiquetas">
						  		<table border="0" cellpadding="0" cellspacing="0" class="etiquetas" style="width: 100%; text-align: center;">
						  			<tr>
						  				<td id="prestamos_por_pagar_fia"></td>
						  			</tr>
						  		</table>
						  		<br>
						  	</td>
						  </tr>
	                      <tr>
	                        <td width="25%" class="etiquetas">N� de Acta:</td>							
	                        <td width="75%"><?php escribir_campo('numr_acta',$_POST["numr_acta"],$numr_acta,'',12,10,'Numero de Acta',$boton,$existe,'','','');  ?></td>
	                      </tr>
	                      <tr>
	                        <td width="25%" class="etiquetas">Fecha&nbsp;de&nbsp;Acta:</td>
	                        <td width="75%"><?php escribir_campo('fcha_acta',$_POST["fcha_acta"],$fcha_acta,'',10,10,'Fecha de Acta',$boton,$existe,'fecha','',''); ?></td>
	                      </tr>
	                      <tr>
	                        <td width="25%" class="etiquetas">Observaciones:</td>
	                        <td width="75%"><?php echo escribir_area('obsr_reti',$_POST["obsr_reti"],$obsr_reti,'',30,3,'Observaciones adicionales sobre el MONTEPIO',$boton,$existe,''); ?></td>
	                      </tr>
	                      <tr>
	                        <td width="25%" class="etiquetas">Monto del Cheque:</td>
	                        <td width="75%"><?php escribir_campo('mont_cheq',$_POST["mont_cheq"],$mont_cheq,'',11,10,'Monto de Cheque',$boton,$existe,'','readonly=readonly(true)',''); ?>
						  </tr>
		                  	<tr>
		                    	<td colspan="2" align="center">
		                    		<button onclick="guardar_retiro();return(false)">Guardar</button>
		                    	</td>
		                  	</tr>
	                    </table>
                    </div>
                    </td>
                  </tr>
				  <tr>
					<td>
					<?php
						if ($_POST["BuscarInd"]) {
							echo '<script>buscar_retiro('.$_POST[buscar_a].')</script>'; 
						}
						$ncriterios =2;
						$criterios[0] = "Fecha"; 
						$campos[0] ="fcha_reti";
						$criterios[1] = "C�dula"; 
						$campos[1] ="cedu_soci";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     $funcion_combo = '"criterio1.checked=true; valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
                                             echo '<center>Buscar C�dula: '; 
                                             combo('cedu_soci2', $cedu_soci, 'vista_socios', $link, 0, 0, 1, '', 'cedu_soci', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nomb_soci",'');
                                             echo '</center>'; 
				   	   }  ?>
				   	   </td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>