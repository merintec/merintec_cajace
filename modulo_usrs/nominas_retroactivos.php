<?php 
	echo '<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1" />';
	include('../comunes/conexion_basedatos.php');
	include ('../comunes/comprobar_inactividad.php');
	include ('../comunes/titulos.php');
	include ('../comunes/mensajes.php');
	$boton="Verificar";
	if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } 
?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../comunes/jquery.min.js'></script>
<style type="text/css">
<!--
.nomina {
    font-family: arial;
    font-size: 10px;
}
.nomina_titulo {
    font-weight: bold;
    text-align: center;
    font-size: 12px;
}
-->
</style>
<script type="text/javascript">
    function bloqueada (estado) {
        if (estado=='S'){
            document.getElementById("bloqueo_img_no").style.display="none";
            document.getElementById("bloqueo_img_si").style.display="";
        }
        if (estado==''){
            document.getElementById("bloqueo_img_no").style.display="";
            document.getElementById("bloqueo_img_si").style.display="none";
        }
    }     
</script>
<script type="text/javascript">
	function actualizar_detalles_gen(codigo){
			var fecha = $('#fcha_retr').val();
			var descripcion = $('#desc_retr').val();
			var mes_ini = $('#mini_retr').val();
			var ano_ini = $('#aini_retr').val();
			var mes_fin = $('#mfin_retr').val();
			var ano_fin = $('#afin_retr').val();
			var dependencia = $('#codg_depn').val();
			var pago = $('#codg_pago').val();

			parametros = {
				"codg_retr" : codigo,
				"fcha_retr" : fecha,
				"desc_retr" : descripcion, 
				"mini_retr" : mes_ini,
				"aini_retr" : ano_ini,
				"mfin_retr" : mes_fin,
				"afin_retr" : ano_fin,
				"codg_depn" : dependencia,
				"codg_pago" : pago
			};
			$.ajax({
				data:  parametros,
			    url:   'capa_nomina_retroactivos_detalles.php',
			    type:  'post',
			    beforeSend: function () {
			       	$("#detalles").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
			   	},
			   	success:  function (response) {
			   		$("#detalles").html(response);
			   	}
			});
	}
	function eliminar_retroactivo(codigo){
		var dependencia = $('#codg_depn').val();
		var pago = $('#codg_pago').val();
		parametros = {
			"codg_retr" : codigo,
			"codg_depn" : dependencia,
			"codg_pago" : pago
		};
		$.ajax({
			data:  parametros,
		    url:   'nominas_retroactivos_eliminar.php',
		    type:  'post',
		    beforeSend: function () {
		       	$("#td_registrar").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
		   	},
		   	success:  function (response) {
		   		$("#td_registrar").html(response);
		   		actualizar_detalles_gen(0);
		   	}
		});
	}
	function actualizar_pagos(){
		var dependencia = $('#codg_depn').val();
		var pago = $('#codg_pago').val();
		var codg_retr = $('#codg_retr_new').val();
		parametros = {
			"codg_depn" : dependencia,
			"codg_retr" : codg_retr,
			"codg_pago" : pago
		};
		$.ajax({
			data:  parametros,
		    url:   'capa_nomina_retroactivos_pagos.php',
		    type:  'post',
		    beforeSend: function () {
		       	$("#td_pago").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
		   	},
		   	success:  function (response) {
		   		$("#td_pago").html(response);
		   	}
		});
	}

	function actualizar_detalles(codg_retr,cedu_soci){
		parametros = {
			"codg_retr" : codg_retr
		};
		$.ajax({
			data:  parametros,
		    url:   'capa_nomina_retroactivos_totales.php',
		    type:  'post',
		    beforeSend: function () {
		       	$("#div_totales").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
		   	},
		   	success:  function (response) {
		   		$("#div_totales").html(response);
		   	}
		});
		if (cedu_soci){
			parametros2 = {
				"codg_retr" : codg_retr,
				"cedu_soci" : cedu_soci,
				"dest_nr_dlle" : 'Total'
			};
			$.ajax({
				data:  parametros2,
			    url:   'capa_nomina_retroactivos_total.php',
			    type:  'post',
			    beforeSend: function () {
			       	$("#total_"+ cedu_soci).html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
			   	},
			   	success:  function (response) {
			   		$("#total_"+ cedu_soci).html(response);
			   	}
			});
		}

	}
	function procesar_detalle(destino,cedula,accion,codigo){
		var codg_asiento = $('#asiento').val();
		var codg_retr = $('#codg_retr_new').val();
		var disponible = $('#monto_disponible').val();
		var mont_nr_dlle = $('#' + destino + cedula).val();
		var diferencia = disponible - mont_nr_dlle;
		if (codg_asiento){
			alert ('NO puedes efectuar cambios en el reintegro porque ya hay un asiento. Debes Eliminarlo para seguir');
		}else{
			if ((mont_nr_dlle>0 && accion == 'guardar') || accion == 'eliminar'){
				if (mont_nr_dlle>0 && accion == 'guardar' && diferencia<0){
					alert ("El monto a procesar es mayor al disponible. \n\nIntente nuevamente con un monto distinto"); 
				}else{
					parametros = {
						"codg_retr" : codg_retr,
						"cedu_soci" : cedula,
						"dest_nr_dlle" : destino,
						"mont_nr_dlle" : mont_nr_dlle,
						"accion": accion,
						"codg_nr_codg": codigo
					};
					$.ajax({
						data:  parametros,
					    url:   'nominas_retroactivos_detalles_procesar.php',
					    type:  'post',
					    beforeSend: function () {
					       	$("#" + destino + "_" + cedula).html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
					   	},
					   	success:  function (response) {
					   		$("#" + destino + "_" + cedula).html(response);
					   		actualizar_detalles(codg_retr,cedula);
					   		actualizar_pagos();
					   	}
					});
				}
			}else{
				alert ('Debe indicar un monto a registrar');
			}
		}
	}
	function registrar_retroactivo(){
		var fecha = $('#fcha_retr').val();
		var descripcion = $('#desc_retr').val();
		var mes_ini = $('#mini_retr').val();
		var ano_ini = $('#aini_retr').val();
		var mes_fin = $('#mfin_retr').val();
		var ano_fin = $('#afin_retr').val();
		var dependencia = $('#codg_depn').val();
		var pago = $('#codg_pago').val();
		if (fecha=='' || descripcion == '' || mes_ini == '' || ano_ini == '' || mes_fin == '' || ano_fin == '' || dependencia == '' || pago == ''){
			alert ('Todos los datos son necesarios para procesar un retroactivo');
		}
		else{
			mes_fin = Number(mes_fin);
			mes_ini = Number(mes_ini);
			if (ano_ini == ano_fin && mes_fin < mes_ini){
				alert ('EL mes de inicio debe ser menor o igual al de finalizacion');
			}else{
				parametros = {
					"fcha_retr" : fecha,
					"desc_retr" : descripcion, 
					"mini_retr" : mes_ini,
					"aini_retr" : ano_ini,
					"mfin_retr" : mes_fin,
					"afin_retr" : ano_fin,
					"codg_depn" : dependencia,
					"codg_pago" : pago
			   	};
				$.ajax({
					data:  parametros,
				    url:   'nominas_retroactivos_guardar.php',
				    type:  'post',
				    beforeSend: function () {
				       	$("#td_registrar").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
				   	},
				   	success:  function (response) {
				   		$("#td_registrar").html(response);
				   		actualizar_detalles_gen($('#codg_retr_new').val());
				   	}
				});
			}
		}
	} 
	function asientos(accion,retroactivo,pago,seccion,codigo){
		if (accion == 'ver' ){
			window.open('../reportes/nmna_retr_asientos.php?codg_retr=' + retroactivo + '&codg_pago='+ pago +'&seccion='+ seccion +'&status=ver','_blank','scrollbars=yes,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500');
		}
		if (accion == 'imprimir' ){
			window.open('../reportes/reporte_asientos.php?asiento2=' + codigo +'&status=ver','_blank','scrollbars=yes,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500');
		}
		if (accion == 'generar'){
			var parametros = {
	   			"codg_retr" : retroactivo,
	   			"codg_pago" : pago,
	   			"seccion" : seccion,
	   			"status": accion
	   		};
			$.ajax({
				data:  parametros,
			    url:   '../reportes/nmna_retr_asientos.php',
			    type:  'get',
			    beforeSend: function () {
		        	$("#asientos").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
		    	},
		    	success:  function (response) {
		    		$("#asientos").html(response);
		    		actualizar_detalles(retroactivo,'');
		    	}
			});
		}
		if (accion == 'eliminar'){
			var parametros = {
	   			"codg_retr" : retroactivo,
	   			"codg_pago" : pago,
	   			"seccion" : seccion,
	   			"status": accion,
	   			"asiento": codigo
	   		};
			$.ajax({
				data:  parametros,
			    url:   '../reportes/nmna_retr_asientos.php',
			    type:  'get',
			    beforeSend: function () {
		        	$("#asientos").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/cargando_gray.gif' width='30px'></td>");
		    	},
		    	success:  function (response) {
		    		actualizar_detalles(retroactivo,'');
		    	}
			});
		}
	}
</script>
<?php 
	include ('../comunes/formularios_funciones.php');
	$prm = llamar_permisos ($_GET["seccion"]);
	include ('nominas_retroactivos_boton.php');
?>
<form id="form1" name="form1" method="post" action="">
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	   	<tr><td>&nbsp;</td></tr>
	   	<tr>
	      	<td class="titulo" colspan="2">
	      		REGISTRO DE N�MINAS DE RETROACTIVOS	
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td class="etiquetas">Fecha de Registro:</td>
						<td>
							<?php escribir_campo('fcha_retr',$_POST["fcha_retr"],$fcha_retr,'',50,10,'Fecha de Registro del Reintegro',$boton,$existe,'fecha','onchange="submit();"','') ?>
						</td>
					</tr>
					<tr>
						<td class="etiquetas">Descripci�n:</td>
						<td>
							<?php escribir_campo('desc_retr',$_POST["desc_retr"],$desc_retr,'',100,45,'Descripci�n o motivo del Reintegro',$boton,$existe,'','onchange="submit();"',''); ?>
						</td>
					</tr>
					<tr>
						<td class="etiquetas">Inicio: </td>
						<td>
							<span class="etiquetas">A�o:</span>
                        <?php if (! $_POST['aini_retr']) { $aini_retr=date("Y"); } else {$aini_retr = $_POST['aini_retr'];} ?>
            			<select name="aini_retr" id="aini_retr"  title="A&ntilde;o de nomina a procesar" onchange='submit();'>
						  	<?php
								$ano_act=date("Y");
								for($i=$ano_act;$i>2010;$i--){
									echo"<option value='$i'";
										if($i==$aini_retr){ echo "selected";}
									echo">A�o: $i</option>";
							} ?>
                        </select>
                        <span class="etiquetas">Mes:</span>
			            <?php if (!$_POST['mini_retr']) { $mini_retr=date("m"); } else {$mini_retr = $_POST['mini_retr'];} ?>
                        <select name="mini_retr" id="mini_retr" title="Mes de nomina a procesar" onchange='submit();'>
                            <?php 
                                for ($i=1;$i<=12;$i++){
                                    echo '<option value="'.$i.'" '; if($mini_retr==$i){ echo 'selected'; } echo '>'.convertir_mes($i).'</option>';
                                }
                            ?>
                        </select>
						</td>
					</tr>
					<tr>
						<td class="etiquetas">Fin: </td>
						<td>
							<span class="etiquetas">A�o:</span>
                        <?php if (! $_POST['afin_retr']) { $afin_retr=date("Y"); } else { $afin_retr = $_POST['afin_retr'];} ?>
            			<select name="afin_retr" id="afin_retr"  title="A&ntilde;o de nomina a procesar" onchange='submit();'>
						  	<?php
								$ano_act=date("Y");
								for($i=$ano_act;$i>2010;$i--){
									echo"<option value='$i'";
										if($i==$afin_retr){ echo "selected";}
									echo">A�o: $i</option>";
							} ?>
                        </select>
                        <span class="etiquetas">Mes:</span>
			            <?php if (! $_POST['mfin_retr']) { $mfin_retr=date("m"); } else {$mfin_retr = $_POST['mfin_retr'];} ?>
                        <select name="mfin_retr" id="mfin_retr" title="Mes de nomina a procesar" onchange='submit();'>
                            <?php 
                                for ($i=1;$i<=12;$i++){
                                    echo '<option value="'.$i.'" '; if($mfin_retr==$i){ echo 'selected'; } echo '>'.convertir_mes($i).'</option>';
                                }
                            ?>
                        </select>
						</td>
					</tr>
					<tr>
						<td class="etiquetas">Dependencia: </td>
						<td>
							<span class="etiquetas"></span><?php combo('codg_depn', $codg_depn, 'dependencias', $link, 0, 0, 1, '', 'codg_depn', " title='Seleccione la Dependencia que desea procesar' OnChange='codg_pago.value=0;submit();' ", $boton, "", ""); ?>
						</td>
					</tr>
					<tr>
						<td class="etiquetas">Seleccione un pago: </td>
						<td id="td_pago">
			                <?php  include ('capa_nomina_retroactivos_pagos.php')?><br>
						</td>
					</tr>
					<?php 
						echo $boton2;
					?>	 
					</tr>
				</table>
				<br>
				<?php include ('capa_nomina_retroactivos_busquedas.php'); ?>
			</td>
		</tr>
	</table>
	<br>
	<div id='detalles'>
		<?php include ('capa_nomina_retroactivos_detalles.php'); ?>
	</div>
</form>
