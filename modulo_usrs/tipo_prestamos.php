<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
  <link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$codg_pcnta = $_POST['codg_pcnta'];
$codg_pcnta_int = $_POST['codg_pcnta_int'];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'tipo_prestamos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "tipo_prestamos";	// nombre de la tabla
$ncampos = "7";			//numero de campos del formulario
 
$datos[0] = crear_datos ("codg_tipo_pres","Codigo del tipo del prestamo",$_POST['codg_tipo_pres'],"0","11","numericos");
$datos[1] = crear_datos ("nomb_tipo_pres","Nombre del tipo de prestamo",$_POST['nomb_tipo_pres'],"1","50","alfanumericos");
$datos[2] = crear_datos ("desc_tipo_pres","Descripción del tipo de prestamo",$_POST['desc_tipo_pres'],"1","50","alfanumericos");
$datos[3] = crear_datos ("valormin_tipo_pres","Valor minimo de tipo de prestamo",$_POST['valormin_tipo_pres'],"0","2","numericos");
$datos[4] = crear_datos ("valormax_tipo_pres","Valor maximo de tipo de prestamo",$_POST['valormax_tipo_pres'],"0","2","numericos");
$datos[5] = crear_datos ("codg_pcnta","Cuentas",$_POST['codg_pcnta'],"1","30","alfanumericos");
$datos[6] = crear_datos ("codg_pcnta_int","Cuentas para interes",$_POST['codg_pcnta_int'],"0","30","alfanumericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"])
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Nombre";
		$datos[0]="nomb_tipo_pres";
		$parametro[1]="Descripción";
		$datos[1]="desc_tipo_pres";

		busqueda_varios(6,$buscando,$datos,$parametro,"codg_tipo_pres");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $codg_tipo_pres= $row["codg_tipo_pres"];
		$nomb_tipo_pres= $row["nomb_tipo_pres"];
		$desc_tipo_pres= $row["desc_tipo_pres"];
	    $valormin_tipo_pres = $row["valormin_tipo_pres"];
	    $valormax_tipo_pres 	 = $row["valormax_tipo_pres"];
		$codg_pcnta = $row["codg_pcnta"];
		$codg_pcnta_int = $row["codg_pcnta_int"];

	    
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"codg_tipo_pres",$_POST["codg_tipo_pres"],$pagina,"");
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["codg_tipo_pres"],"codg_tipo_pres",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Tipos de Prestamos</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
						<?php  escribir_campo('codg_tipo_pres',$_POST["codg_tipo_pres"],$codg_tipo_pres,'',12,15,'Codigo del tipo de prestamo',$boton,$existe,'','','oculto'); ?>
                      <tr>
                        <td width="25%" class="etiquetas">Nombre:</td>
                        <td width="75%"><?php escribir_campo('nomb_tipo_pres',$_POST["nomb_tipo_pres"],$nomb_tipo_pres,'',50,25,'Nombre del tipo de prestamo',$boton,$existe,'','',''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Descripción:</td>
                        <td width="75%"><?php escribir_campo('desc_tipo_pres',$_POST["desc_tipo_pres"],$desc_tipo_pres,'',100,25,'Descripción del tipo de prestamo',$boton,$existe,'','',''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Tiempo Minimo:  </td>
                        <td width="75%"><?php escribir_campo('valormin_tipo_pres',$_POST["valormin_tipo_pres"],$valormin_tipo_pres,'',2,5,'Tiempo minimo del credito',$boton,$existe,'','',''); ?></td>
							</tr>
							 <tr>
                        <td width="25%" class="etiquetas">Tiempo Maximo:  </td>
                        <td width="75%"><?php escribir_campo('valormax_tipo_pres',$_POST["valormax_tipo_pres"],$valormax_tipo_pres,'',2,5,'Tiempo maximo del credito',$boton,$existe,'','',''); ?></td>
                      </tr>      
                      <tr>
                        <td width="25%" class="etiquetas">Cuenta:  </td>
                        <td width="75%"><?php combo('codg_pcnta', $codg_pcnta, 'cuentas c, plan_cuentas pc', $link, 0, 1, 2, '', 'codg_pcnta', " ", $boton, "WHERE pc.stat_pcuen='Activo' AND pc.codg_pcuen=c.codg_pcuen AND c.movi_cnta='S' ORDER BY c.nmro_cnta ",''); ?></td>
                      </tr>                 
                      <tr>
                        <td width="25%" class="etiquetas">Cuenta para intereses:  </td>
                        <td width="75%"><?php combo('codg_pcnta_int', $codg_pcnta_int, 'cuentas c, plan_cuentas pc', $link, 0, 1, 2, '', 'codg_pcnta_int', " ", $boton, "WHERE pc.stat_pcuen='Activo' AND pc.codg_pcuen=c.codg_pcuen AND c.movi_cnta='S' ORDER BY c.nmro_cnta",'codg_pcnta'); ?></td>
                      </tr>                 
                     
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
					
                  <tr>
				  	<td>
					
					</td>
				  </tr>
				  <tr>
                    <td>
					<?php 
						$ncriterios =2;
						$criterios[0] = "Nombre"; 
						$campos[0] ="nomb_tipo_pres";
						$criterios[1] = "Descripción"; 
						$campos[1] ="desc_tipo_pres";

					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
