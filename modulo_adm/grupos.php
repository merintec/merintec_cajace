<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE["usnombre"]||$_COOKIE["uspriv"]!=1) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$boton = "Verificar";
$existe = '';
$pagina = "grupos.php";
$tabla = "grupos";	// nombre de la tabla
$ncampos = "2";		//numero de campos del formulario
$datos[0] = crear_datos ("nom_grp","Nombre",$_POST['nom_grp'],"1","20","alfabeticos");
$datos[1] = crear_datos ("des_grp","Descripción",$_POST['des_grp'],"1","255","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Nombre";
		$datos[0]="nom_grp";
		busqueda_varios(3,$buscando,$datos,$parametro,"cod_grp");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_grp = $row["cod_grp"];
	    $nom_grp = $row["nom_grp"];
	    $des_grp = $row["des_grp"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_grp",$_POST["cod_grp"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si','Grupos');
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_grp"],"cod_grp",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos del Grupo</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Nombre:
                          </td>
                        <td width="75%"><input name="cod_grp" type="hidden" id="cod_grp" value="<?php if(! $existe) { echo $_POST["cod_grp"]; } else { echo $cod_grp; } ?>" size="35" />
                        <input name="nom_grp" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_grp" value="<?php if(! $existe) { echo $_POST["nom_grp"]; } else { echo $nom_grp; } ?>" size="35" title="Nombre del Grupo">
                        <?php if ($boton=='Modificar') { echo $nom_grp; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Descripcion:</td>
                        <td>
						<?php if ($boton!='Modificar') { echo '<textarea name="des_grp" cols="32" rows="2" id="des_grp" title="Descripción del Grupo">';  if(! $existe) { echo $_POST["des_grp"]; } else { echo $des_grp; } echo '</textarea>'; } ?>
                          <?php  if ($boton=='Modificar') { echo '<input name="des_grp" type="hidden" id="des_grp" value="'; if(!$existe) { echo $_POST['des_grp']; } else { echo $des_grp; } echo '">'.$des_grp; } ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$ncriterios =2; 
						$criterios[0] = "Nombre"; 
						$campos[0] ="nom_grp";
						$criterios[1] = "Descripción";
						$campos[1] = "des_grp";
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton) ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
