<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); 
include ('../reportes/generarpdf.php');
$idprint=$_GET['codg_nmna']; 
$idprint2=$_GET['codg_pago']; 
    //////////////// Datos de la nomina
    $sql_nmna = "SELECT * FROM nominas nmna, dependencias dp WHERE codg_nmna=".$idprint." AND nmna.codg_depn=dp.codg_depn";
    $res_nmna = mysql_query($sql_nmna);
    $reg_nmna = mysql_fetch_array($res_nmna);
    $dependencia = $reg_nmna['nomb_depn'];
    if ($reg_nmna['prdo_nmna']<=5){ $nomina = $reg_nmna['prdo_nmna'].'� Semana '; }
    if ($reg_nmna['prdo_nmna']>5 && $reg_nmna['prdo_nmna']<8){ $nomina = ($reg_nmna['prdo_nmna']-5).'� Quincena '; }
    if ($reg_nmna['prdo_nmna']==8){ $nomina = 'Mes '; }
    $nomina .= "de ".convertir_mes($reg_nmna['mess_nmna'])." del ".$reg_nmna['anno_nmna'];
    //////////////// Datos del Pago
    $sql_pago = "SELECT * from dependencias_pagos dp, banco_cuentas bc WHERE dp.codg_pago=".$idprint2." AND dp.codg_cnta=bc.codg_cnta";
    $res_pago = mysql_query($sql_pago);
    $reg_pago = mysql_fetch_array($res_pago);
    $reg_pago["fcha_pago"] = strtotime($reg_pago["fcha_pago"]);    
    //////////////// Datos de los Detalles de la Nomina
    $cuenta_datos = 0;
    $sql_dtll = "SELECT mp.tipo_mpio, sc.cedu_soci, sc.cedu_soci, CONCAT(sc.nomb_soci, ' ', sc.apel_soci) as nombre, mp.fcha_mpio, SUM(nd.mnto_dlle) as monto_pagado FROM nominas_detalle nd, montepio_socios mps, montepio mp, socios sc WHERE nd.codg_nmna=".$idprint." AND nd.codg_pago=".$idprint2." AND nd.moti_dlle='MONTEPIO' AND nd.rela_dlle=mps.codg_mpsc AND mps.codg_mpio=mp.codg_mpio AND mp.cedu_soci=sc.cedu_soci GROUP BY mp.codg_mpio";
    $res_dtll = mysql_query($sql_dtll);
    $totales_general = array();
    while ($reg_dtll = mysql_fetch_array($res_dtll)){
        $cuenta_datos += 1;
        if ($reg_dtll['tipo_mpio']=="S"){
           $detalles[$cuenta_datos][0]= "Muerte de socio: ".$reg_dtll['nombre'].' ('.redondear($reg_dtll['cedu_soci'],0,".",",").')';
        }
        if ($reg_dtll['tipo_mpio']=="F"){
           $detalles[$cuenta_datos][0]= "Muerte de familiar de: ".$reg_dtll['nombre'].' ('.redondear($reg_dtll['cedu_soci'],0,".",",").')';
        }
        $detalles[$cuenta_datos][1]=  strtotime($reg_dtll['fcha_mpio']);
        $detalles[$cuenta_datos][2]=  $reg_dtll['monto_pagado'];
        $totales_general["total"]+=$reg_dtll['monto_pagado'];
    }
    $acum_cuentas = 1;
    /// cargamos el la cuenta de Montepio
    $sql_val="select * from valores WHERE des_val='CNT_MP'";
    $res_val = mysql_query($sql_val);
    while ($row_val = mysql_fetch_array($res_val))
    {
        // el nombre de la variable es $CNT_MP
        $$row_val['des_val'] = $row_val['val_val'];
    }
    // buscar la cuenta Montepio
    $sql_part = "SELECT * FROM cuentas WHERE codg_pcnta=".$CNT_MP;
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $res_cnta["montepio"]["nmro_cnta"] = $res_part["nmro_cnta"];
    $res_cnta["montepio"]["nomb_cnta"] = $res_part["nomb_cnta"];
    //// datos del pago
    $sql_part = "SELECT * FROM dependencias_pagos dpago, banco_cuentas bcnta, cuentas WHERE dpago.codg_pago =".$idprint2." AND dpago.codg_cnta=bcnta.codg_cnta AND bcnta.codg_pcnta=cuentas.codg_pcnta";
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_part['nmro_cnta'];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_part['nomb_cnta'];
    $detalle_cuentas[$acum_cuentas]["debe"] = $totales_general["total"];
    $acum_cuentas += 1;
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["montepio"]["nmro_cnta"];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["montepio"]["nomb_cnta"];
    $detalle_cuentas[$acum_cuentas]["haber"] = $totales_general["total"];
    $acum_cuentas += 1;
    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'. 
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
$html='<html>
<head>
    <base target="_blank">
    <title>Reportes de Nomina - Aportes y Ahorros</title>
<style type="text/css">
    .reporte
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .reporte a
    {
        font-weight: bold;
        color: #0000FF;
    }
    .titulo {
        font-family: arial; 
        font-size: 13pt; 
        font-weight: bold; 
        color: #000000; 
        background-color: #67BABA; 
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 12px;
        font-weight: bold;
    }    
    .tablanomina {
        font-family: Arial; 
        font-size: 9px; 
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 120px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
#header,
#footer {
    position: fixed;  
    left: 0;
    right: 0;
	font-size: 0.9em;
}
#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
</style>
</head>
<body>
<div id="header">
    <table cellspacing="0" cellpadding="0" border="0 align="center" class="reporte" >
    <tr height="1%">
        <td width="210px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td>
            <div align="center"><h3>'.$dependencia.'<BR>PAGO DE MONTEPIO EN N�MINA<BR>('.$nomina.')</h2></div>
        </td>
        <td width="210px">
            <div align="right">Lugar y Fecha de Impresi�n: <br>Ejido, '.date(d).' de '.convertir_mes(date(m)).' de '.redondear(date(Y),0,".","").'&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    </table>
</div>
<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Datos del Pago</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas">Banco Origen</td>
                    <td class="etiquetas">Banco / Cuenta Destino</td>
                    <td class="etiquetas">Referencia</td>
                    <td class="etiquetas">Fecha</td>
                </tr>
                <tr align="center">
                    <td>'.$reg_pago["banc_orig"].'</td>
                    <td>'.$reg_pago["bnco_cnta"].': '.$reg_pago["nmro_cnta"].'</td>
                    <td>'.$reg_pago["numr_refe"].'</td>
                    <td>'.date("d-m-Y",$reg_pago["fcha_pago"]).'</td>
                </tr>
            </table>
            <br>
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Montos Acreditados</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas" width="30px">N�</td>
                    <td class="etiquetas">Descripci�n de Montepio</td>
                    <td class="etiquetas" width="100px">Fecha</td>
                    <td class="etiquetas" width="80px">Monto</td>
                </tr>';
                for ($i=1;$i<=$cuenta_datos;$i++){
                        $html .= '<tr>
                            <td align="right">'.$i.'&nbsp;</td>
                            <td>&nbsp;'.$detalles[$i]["0"].'&nbsp;</td>
                            <td align="center">'.date('d-m-Y', $detalles[$i]["1"]).'</td>
                            <td align="right">'.Redondear($detalles[$i]["2"],2,".",",").'&nbsp;</td>
                        </tr>';
                }
$html .= '<tr align="center" class="titulo">
                    <td colspan="3" align="right">T O T A L E S&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["total"],2,'.',',').'&nbsp;</td>
                </tr>
             </table>';
$html .= '</body></html>';
echo $html;
//generar_pdf($html,'Aportes_y_Ahorros.pdf','letter','portrait');
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');?>
