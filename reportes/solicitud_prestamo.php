<?php 
include('../comunes/conexion_basedatos.php'); 
include ('../comunes/comprobar_inactividad.php');
$print_pdf='SI';
include ('../comunes/formularios_funciones.php');
include ('generarpdf.php');

$cedu_soci=$_GET['cedu_soci'];
$codg_prst=$_GET['codg_prst'];

//consulta datos personales
$consulta= mysql_query("SELECT   * from socios where cedu_soci='$cedu_soci'  ");
$con=mysql_fetch_assoc($consulta);

//consulta el maximo en datos laborales
$consulta_max= mysql_query("SELECT   max(codg_dlab) as codg_dlab  from socios_dat_lab where cedu_soci='$cedu_soci'  ");
$conmax=mysql_fetch_assoc($consulta_max);

$id_maxlab=$conmax['codg_dlab'];

//consulta datos laborales y dependencia
$consulta_dp= mysql_query("SELECT   * from socios_dat_lab as s, dependencias as d, tipo_cargo as t  where s.codg_dlab='$id_maxlab' and d.codg_depn=s.codg_depn and t.codg_tcrg=s.codg_tcrg   ");
$con_dp=mysql_fetch_assoc($consulta_dp);

/// traer fecha de ingreso (primer status activo en orden desc)
$sql_fechaing = "SELECT * FROM socios_estado se, tipos_status ts  WHERE se.cedu_soci = ".$cedu_soci." AND se.codg_stat = ts.codg_stat AND ts.nomb_stat = 'Activo' ORDER BY se.fcha_estd DESC LIMIT 1";
$bus_fechaing = mysql_query($sql_fechaing);
$con_fechaing = mysql_fetch_assoc($bus_fechaing);

//evaluar tipo de sueldos

if ($con_dp['peri_suel']=='S')
{
	$sueldo='SEMANAL';
}
if ($con_dp['peri_suel']=='Q')
{
	$sueldo='QUINCENAL';
}
if ($con_dp['peri_suel']=='M')
{
	$sueldo='MENSUAL';
}

//consulta de % para credito

$con_val= mysql_query("SELECT   * from valores where des_val='PPM'  ");
$conv=mysql_fetch_assoc($con_val);
$pormax_prestamo=$conv['val_val'];

$con_sal= mysql_query("SELECT   * from socios_saldo where cedu_soci='$cedu_soci'");
$cons=mysql_fetch_assoc($con_sal);

$sum_sal=$cons['apor_sald']+$cons['rete_sald']+$cons['apor_comp']+$cons['rete_comp'];
$valor_maximo=$pormax_prestamo*$sum_sal;
if ($con_dp[cedu_soci]==NULL)
{
   ?>
   <script type="text/javascript" >
   	alert("Debe especificar los datos laborales de funcionario");
   	window.close();

   </script>
   <?
	
}

//convertir a fecha DE NAC

$convertir_fecha=strtotime($con['fchn_soci']);
$dia=date('d',$convertir_fecha);
$mes=date('m',$convertir_fecha);
$ano=date('Y',$convertir_fecha);

//convertir fecha de ingreso
$fecha_ingreso=strtotime($con_fechaing[fcha_estd]); 
$diai=date('d',$fecha_ingreso);
$mesi=date('m',$fecha_ingreso);
$anoi=date('Y',$fecha_ingreso);

//edad
$edad=edad($con['fchn_soci'],'');
//estado civil
$estado_civil=estado_civil($con['stdc_soci']);
//estado civil

// datos del prestamo
$sql = "SELECT *,(SELECT carg_dlab FROM socios_dat_lab WHERE cedu_soci = s.cedu_soci AND fchi_dlab <= p.fcha_acta ORDER BY fchi_dlab DESC LIMIT 1) as cargo, (SELECT nomb_tipo_pres FROM tipo_prestamos WHERE codg_tipo_pres = p.codg_tipo_pres) as nomb_tipo_pres, (SELECT nomb_depn FROM socios_dat_lab, dependencias WHERE cedu_soci = s.cedu_soci AND fchi_dlab <= p.fcha_acta AND socios_dat_lab.codg_depn=dependencias.codg_depn ORDER BY fchi_dlab DESC LIMIT 1) as dependencia from socios as s, prestamos as p where p.codg_prst='$codg_prst' and s.cedu_soci=p.cedu_soci";
$consulta= mysql_query($sql);
$prestamo=mysql_fetch_assoc($consulta);


// para verificar si cuenta con fiadores
$sql_fia = "SELECT * FROM prestamos_fia pf, socios s WHERE pf.codg_prst = ".$codg_prst. " AND pf.orig_fian = 'fiador' AND pf.cedu_soci = s.cedu_soci";
$bus_fia = mysql_query($sql_fia);
$cuenta_fiador = 0;
while($reg_fia = mysql_fetch_array($bus_fia)){
	$cuenta_fiador+=1;
	$detalle_fiador[$cuenta_fiador][nombre] = $reg_fia[apel_soci].'&nbsp;'.$reg_fia[nomb_soci];
	$detalle_fiador[$cuenta_fiador][cedula] = $reg_fia[cedu_soci];
	$detalle_fiador[$cuenta_fiador][monto] = $reg_fia[mont_fian];
}
// para verificar si cuenta con fianza
$sql_fia = "SELECT * FROM prestamos_fia pf WHERE pf.codg_prst = ".$codg_prst. " AND pf.orig_fian = 'fianza'";
$bus_fia = mysql_query($sql_fia);
$cuenta_fianza = 0;
while($reg_fia = mysql_fetch_array($bus_fia)){
	$cuenta_fianza+=1;
	$detalle_fianza[$cuenta_fianza][nombre] = $reg_fia[desc_fian];
	$detalle_fianza[$cuenta_fianza][monto] = $reg_fia[mont_fian];
}
/// Para verificar si paga otros prestamos
$sql_ppp='SELECT m.*, p.*, (select nomb_tipo_pres from tipo_prestamos where codg_tipo_pres=p.codg_tipo_pres) as nomb_tipo_pres FROM prestamos_mov m LEFT JOIN prestamos p ON p.codg_prst=m.codg_prst WHERE m.orgn_prtm="Pagado por prestamo" AND m.rela_prtm="'.$codg_prst.'"';
$busq_ppp=mysql_query($sql_ppp);
$cuenta_ppp = 0;
while ($reg_ppp = mysql_fetch_array($busq_ppp)){
	$cuenta_ppp += 1;
	$ppp[$cuenta_ppp][nompre] = $reg_ppp[nomb_tipo_pres];
	$ppp[$cuenta_ppp][fecha] = $reg_ppp[fcha_acta];
	$ppp[$cuenta_ppp][monto] = $reg_ppp[mnto_prtm];
	$ppp[$cuenta_ppp][capital] = $reg_ppp[capi_prtm];
	$ppp[$cuenta_ppp][interes] = $reg_ppp[inte_prtm];
}
$tama�o = 6;
$numero = $prestamo[codg_prst];
$actual = strlen($numero);
for ($g=1;$g<=($tama�o-$actual);$g++){
	$prenum .= '0';
}
$numero_prestamo = "N&deg; S-".$prestamo[numr_acta]."-".$prenum.$numero;
$fecha_solicitud = ordenar_fecha($prestamo[fcha_acta]);

$html="<html>
<style type='text/css' >
<!--
.etiqueta {
	color: #000000;
	font-size: 11px;
	font-weight: bold;
}
.resultado {
	font-size: 12px;
	color: #000000;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid;
	border-left:none;
	text-align: center;
}
.adicionales {
	font-size: 12px;
	color: #000000;
}

.titulo {
	font-size: 18px;
	font-weight: bold;
	
	
body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
}
-->
</style>
<head> </head>
<body>
</br>
</br>
<table width='100%' > 
	<tr> 
		<td align='left'> 
			<img height='60px' src='../imagenes/logo_report.jpg'>  
		</td>
		<td align='right' class='titulo'> SOLICITUD DE PRESTAMO<br><span style='color:#FF0000'>".$numero_prestamo."</span><br><span style='font-size: 12px'>FECHA: <u>&nbsp;&nbsp;".$fecha_solicitud."&nbsp;&nbsp;</u></span></td> 
	</tr>
</table>
<br>
<table width='100%' align='center'><tr height='1px'> <td align='left' width='20%' class='etiqueta'>&nbsp;APELLIDOS&nbsp;Y&nbsp;NOMBRES:&nbsp;</td> <td width='70%'  align='left' class='resultado'  >&nbsp;".$con['apel_soci']."&nbsp;".$con['nomb_soci']."  </td>  </tr></table>
<table width='100%' align='center'><tr height='1px'> <td align='left' width='5%' class='etiqueta'>&nbsp;C.I.:&nbsp;</td> <td   align='left' class='resultado'>&nbsp;".redondear($con['cedu_soci'],0,'.',',')."&nbsp;   </td> <td align='left' width='5%' class='etiqueta'>&nbsp;F.N.:&nbsp;</td> <td   align='left'  class='resultado'>&nbsp;".$dia."/".$mes."/".$ano."&nbsp; </td>   <td align='left' width='8%' class='etiqueta'>&nbsp;EDAD:&nbsp;</td> <td   align='left'  class='resultado'>&nbsp;".$edad."&nbsp; </td>    <td align='left' width='15%' class='etiqueta'>&nbsp;ESTADO&nbsp;CIVIL:&nbsp;</td> <td   align='left'  class='resultado'>&nbsp;".$estado_civil."&nbsp; </td>    </tr></table>
<table width='100%' align='center'><tr height='1px'> <td align='left' width='18%' class='etiqueta'>&nbsp;NACIONALIDAD:&nbsp;</td> <td   align='left' width='5%' class='resultado'>&nbsp;".$con['naci_soci']."&nbsp;  </td> <td align='left' width='30%' class='etiqueta'>&nbsp;DIRECCI�N&nbsp;DE&nbsp;HABITACI�N:&nbsp;</td> <td   align='left' width='50%' class='resultado'>&nbsp;".$con['dirh_soci']."&nbsp;  </td>   </tr></table>
<table width='100%' align='center'><tr height='1px'> <td align='left' width='13%' class='etiqueta'>&nbsp;TEL�FONO:&nbsp;</td> <td   align='left' class='resultado'>&nbsp;".$con['tlfn_soci']."&nbsp;  </td> <td align='left' width='13%' class='etiqueta'>&nbsp;CELULAR:&nbsp;</td> <td   align='left' class='resultado'>&nbsp;".$con['tlfc_soci']."&nbsp;  </td>   </tr></table>
<table width='100%' align='center'><tr height='1px'> <td align='left' width='28%' class='etiqueta'>&nbsp;DEPENDENCIA&nbsp;DONDE&nbsp;LABORA:&nbsp;</td> <td   align='left'  class='resultado'>&nbsp;".$con_dp['nomb_depn']."&nbsp;  </td> </tr></table>
<table width='100%' align='center'><tr height='1px'> <td align='left' width='28%'class='etiqueta'>&nbsp;DIRECCI�N&nbsp;DE&nbsp;TRABAJO:&nbsp;</td> <td   align='left'  class='resultado'>&nbsp;".$con_dp['dirc_depn']."&nbsp;  </td>   </tr></table>
<table width='100%' align='center'><tr height='1px'> <td align='left' width='15%' class='etiqueta'>&nbsp;TIPO&nbsp;DE&nbsp;CARGO:&nbsp;</td> <td  width='25%' align='left' class='resultado'>&nbsp;".$con_dp['desc_tcrg']."&nbsp;  </td> <td align='left' width='18%' class='etiqueta'>&nbsp;CARGO&nbsp;FUNCIONAL:&nbsp;</td> <td width='25%'   align='left' class='resultado'>&nbsp;".$con_dp['carg_dlab']."&nbsp;  </td>   </tr></table>
<table width='100%' align='center'><tr height='1px'> <td align='left' width='13%' class='etiqueta'>&nbsp;FECHA&nbsp;DE&nbsp;INGRESO:&nbsp;</td> <td width='20%'  align='left' class='resultado'>&nbsp;".$diai."/".$mesi."/".$anoi."&nbsp; </td> <td align='left' width='13%' class='etiqueta'>&nbsp;SUELDO&nbsp;".$sueldo.":&nbsp;</td> <td width='20%'   align='left' class='resultado'>&nbsp;".$con_dp['suel_dlab']."&nbsp;  </td>   </tr></table>
<table width='100%' align='center'>
	<tr height='1px'> 
		<td align='left' class='etiqueta' colspan='5'><center>PRESTAMO SOLICITADO</center></td>
	</tr>
	<tr height='1px' align='center'> 
		<td class='etiqueta' width='140px' >MODALIDAD</td>
		<td class='etiqueta' width='45px'>PLAZO:</td>
		<td class='etiqueta' width='70px'>% INTER�S:</td>
		<td class='etiqueta' width='120px'>MONTO SOLICITADO:</td>  
		<td class='etiqueta' width='90px'>MONTO CUOTA:</td>
	</tr>
	<tr height='1px'> 
		<td class='resultado'>".$prestamo[nomb_tipo_pres]."</td>
		<td class='resultado'>".$prestamo[plaz_prst]." meses</td>
		<td class='resultado'>".$prestamo[mont_intr]."% anual</td>  
		<td class='resultado'>".redondear($prestamo[mont_prst],2,'.',',')."</td>
		<td class='resultado'>".redondear($prestamo[mont_cuot],2,'.',',')."</td>
	</tr>
</table>
<table width='100%' align='center'  border='1' bordercolor='#000000' style='border-collapse:collapse'><tr> <td align='left' width='20%' class='etiqueta'>&nbsp;NOTAS&nbsp;IMPORTANTES:&nbsp;</td> <td   align='left' class='resultado'>&nbsp; </td>  </tr>
	<tr class='adicionales'>
		<td colspan='2'>";

if ($cuenta_fiador > 0){
	$html .= "<b>Sirve(n) de fiador(es):</b>";
	for($i=1;$i<=$cuenta_fiador;$i++){
		$html.="<u>".$detalle_fiador[$i][nombre]."</u>, titular de la c�dula de identidad <u>".redondear($detalle_fiador[$i][cedula],0,'.','')."</u> por un monto de <u>Bs. ".redondear($detalle_fiador[$i][monto],2,'.',',')."</u>";
		if ($i < $cuenta_fiador){
			$html .= "; ";
		}
		else {
			$html.= ". ";
		}
	}
}
if ($cuenta_fianza > 0){
	$html .= "<br><b>Se recibe(n) como garant�a(s): </b>";
	for($i=1;$i<=$cuenta_fianza;$i++){
		$html.="<u>".$detalle_fianza[$i][nombre]."</u>, por un monto de Bs. <u>".redondear($detalle_fianza[$i][monto],2,'.',',')."</u>";
		if ($i < $cuenta_fianza){
			$html .= "; ";
		}
		else {
			$html.= ". ";
		}
	}
}
if ($cuenta_ppp > 0){
	$total_ppp = 0;
	$html .= "<br><b>Del monto otorgado se cancela(n) deuda(s) de </b>";
	for($i=1;$i<=$cuenta_ppp;$i++){
		$html.= " Bs. ".redondear($ppp[$i][monto],2,'.',',')." (Bs. ".redondear($ppp[$i][capital],2,'.',',')." de Capital y Bs. ".redondear($ppp[$i][interes],2,'.',',')." de Inter�s) por ".$ppp[$i][nompre].", otorgado el ".ordenar_fecha($ppp[$i][fecha]);
		$total_ppp += $ppp[$i][monto];
		if ($i < $cuenta_ppp){
			$html .= "; ";
		}
		else {
			$html.= ". ";
		}
	}
}

$html.="
		</td> 
	</tr>
</table>

<table width='100%' align='center'> 
<tr> <td colspan='4' align='center'>&nbsp; </td>  </tr></table><table width='100%' align='center' border='1' bordercolor='#000000' style='border-collapse:collapse'> 
	<tr> 
		<td>
			<table width='100%' align='center'> <tr> <td colspan='4' align='center'>&nbsp; </td>  </tr> </table>
			<table width='100%' align='center'> <tr> <td align='left' width='25%' class='etiqueta'>&nbsp;FIRMA&nbsp;DEL&nbsp;SOLICITANTE:&nbsp;</td> <td   align='left' class='resultado'>&nbsp;</td> <td align='left' width='5%' class='etiqueta'>&nbsp;C.I.&nbsp;</td> <td   align='left' class='resultado'>&nbsp;</td>   </tr> </table> ";
			if ($cuenta_fiador > 0){
				for($i=1;$i<=$cuenta_fiador;$i++){
					$html.="<table width='100%' align='center'> <tr> <td align='left' width='25%' class='etiqueta'>&nbsp;FIRMA&nbsp;DEL&nbsp;".$i."�&nbsp;FIADOR&nbsp;</td> <td   align='left' class='resultado'>&nbsp;</td> <td align='left' width='5%' class='etiqueta'>&nbsp;C.I.&nbsp;</td> <td   align='left' class='resultado'>&nbsp;</td>   </tr> </table> ";
				}
			}
   			$html .= "<table width='100%' align='center'> <tr> <td colspan='4' align='center'>&nbsp; </td>  </tr> </table>
   		</td>  
   	</tr> 
</table> 
<br>
<table width='60%' align='right'><tr> <td align='left' width='20%' class='etiqueta'>&nbsp;FECHA&nbsp;DE&nbsp;RECIBIDO:&nbsp;</td> <td align='left' class='resultado' width='10%'>&nbsp;</td> <td align='left' width='12%' class='etiqueta'>&nbsp;FIRMA:&nbsp;</td> <td   align='left' width='20%' class='resultado'>&nbsp;  </td>   </tr></table>";

/*
<table width='100%' align='center'><tr> <td align='center' width='1%' class='etiqueta'>&nbsp;NO&nbsp;ESCRIBA&nbsp;EN&nbsp;ESTE&nbsp;ESPACIO&nbsp;</td>  </tr></table>
<table width='100%' align='center' border='1' bordercolor='#000000' style='border-collapse:collapse'> 
<tr> <td>
<table width='100%' align='left'><tr> <td align='left' width='15%' class='etiqueta'>&nbsp;SOLICITUD&nbsp;N�:&nbsp;</td> <td  align='left' class='resultado'>&nbsp; </td> <td align='left' width='20%' class='etiqueta'>&nbsp;TIPO&nbsp;DE&nbsp;PRESTAMO:&nbsp;</td> <td  align='left' class='resultado'>&nbsp; </td> <td align='left' width='8%' class='etiqueta'>&nbsp;PLAZO:&nbsp;</td> <td    align='left' class='resultado'>&nbsp; </td></tr></table>
<br>
<table width='100%' align='left'><tr> <td align='left' width='20%' class='etiqueta'>&nbsp;OBSERVACIONES:&nbsp;</td> <td width='78%'  align='left' class='resultado'>&nbsp; </td>  </tr></table>
<br>
<table width='50%' align='left'><tr> <td align='left' width='15%' class='etiqueta'>&nbsp;APROBADO:&nbsp;</td>  <td width='13%'   align='left' class='resultado'>&nbsp;  </td> <td align='left' width='13%' class='etiqueta'>&nbsp;NEGADO:&nbsp;</td> <td   align='left' width='13%'  class='resultado'>&nbsp;  </td>   </tr></table> <br>
<table width='100%' align='center'><tr> <td align='left' width='18%' class='etiqueta'>&nbsp;SEG�N&nbsp;ACTA&nbsp;N�:&nbsp;</td>  <td   align='left' class='resultado'>&nbsp;  </td> <td align='left' width='10%' class='etiqueta'>&nbsp;FECHA:&nbsp;</td> <td   align='left' class='resultado'>&nbsp;  </td> <td align='left' width='20%' class='etiqueta'>&nbsp;MONTO&nbsp;OTORGADO:&nbsp;</td> <td   align='left' class='resultado'>&nbsp;  </td>   </tr></table>
<table width='100%' align='left'><tr> <td align='left' width='10%' class='etiqueta'>&nbsp;CHEQUE:&nbsp;</td> <td  align='left' class='resultado'>&nbsp; </td> <td align='left' width='10%' class='etiqueta'>&nbsp;BANCO:&nbsp;</td> <td  align='left' class='resultado'>&nbsp; </td> <td align='left' width='25%' class='etiqueta'>&nbsp;MONTO&nbsp;BENEFICIARIO:&nbsp;</td> <td  align='left' class='resultado'>&nbsp; </td></tr></table><BR>
<table width='100%' align='left'><tr> <td align='left' width='20%' class='etiqueta'>&nbsp;CUOTA&nbsp;MENSUAL:</td>  <td    align='left' class='resultado'>&nbsp;  </td> <td width='10%'>&nbsp; </td> <td align='left' width='20%' class='etiqueta'>&nbsp;CUOTA&nbsp;QUINCENAL:&nbsp;</td> <td   align='left'    class='resultado'>&nbsp;  </td>   </tr></table> <BR>
<table width='50%' align='left'><tr> <td align='left' width='35%' class='etiqueta'>&nbsp;CUOTA&nbsp;SEMANAL:</td> <td   align='left'    class='resultado'>&nbsp;  </td>   </tr></table> 
<BR>
<BR>
<table width='100%' align='center'> 
<tr> <td colspan='4' align='center'>&nbsp; </td>  </tr></table>
<table width='100%' align='center'><tr> <td widht='15%'>&nbsp; </td> <td   align='left' class='resultado'>&nbsp;  </td> <td widht='15%'>&nbsp; </td> <td   align='left' class='resultado'>&nbsp;  </td> <td widht='15%'>&nbsp; </td>   </tr>
<tr> <td widht='10%'>&nbsp; </td> <td   align='center' class='etiqueta'> PRESIDENTE  </td> <td widht='10%'>&nbsp; </td> <td   align='center' class='etiqueta'> TESORERO </td> <td widht='10%'>&nbsp; </td>   </tr></table>
</tr> </td> </table>*/


$html.= "</body> </html>";


 


//echo $html;
generar_pdf($html,'solicitud_prestamo.pdf','letter','portrait');
?>

