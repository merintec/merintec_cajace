<?php 
echo '<meta charset="ISO-8859-1">';
include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); 
include ('../reportes/generarpdf.php');
$idprint=$_GET['codg_nmna'];
$idprint2=$_GET['codg_pago'];
$idprint3=$_GET['exce'];
    //////////////// Datos de la nomina
    $sql_nmna = "SELECT * FROM nominas nmna, dependencias dp WHERE codg_nmna=".$idprint." AND nmna.codg_depn=dp.codg_depn";
    $res_nmna = mysql_query($sql_nmna);
    $reg_nmna = mysql_fetch_array($res_nmna);
    $dependencia = $reg_nmna['nomb_depn'];
    if ($reg_nmna['prdo_nmna']>=11){ $nomina = ($reg_nmna['prdo_nmna']-10).'� Semana '; }
    if ($reg_nmna['prdo_nmna']>5 && $reg_nmna['prdo_nmna']<8){ $nomina = ($reg_nmna['prdo_nmna']-5).'� Quincena '; }
    if ($reg_nmna['prdo_nmna']==8){ $nomina = 'Mes '; }
    $nomina .= "de ".convertir_mes($reg_nmna['mess_nmna'])." del ".$reg_nmna['anno_nmna'];
    //////////////// Datos del Pago
    if(!$idprint3){    
        $sql_pago = "SELECT * from dependencias_pagos dp, banco_cuentas bc WHERE dp.codg_pago=".$idprint2." AND dp.codg_cnta=bc.codg_cnta";
        $res_pago = mysql_query($sql_pago);
        $reg_pago = mysql_fetch_array($res_pago);
        $reg_pago["fcha_pago"] = strtotime($reg_pago["fcha_pago"]);
    }
    /////////////// Si es pago de cuenta por pagar 
    $sql_porpagar = "SELECT * FROM dependencias_pagos_exce dpe, nominas n WHERE dpe.codg_exce=".$idprint3." AND dpe.codg_nmna = n.codg_nmna";
    $bus_porpagar = mysql_query($sql_porpagar);
    $res_porpagar = mysql_fetch_array($bus_porpagar);
    //////////////// TOTALES de los Datos de los Detalles de la Nomina
    $sql_dtll = "SELECT nd.codg_nmna,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Aporte' AND codg_pago=".$idprint2.") as aportes,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Retenci�n' AND codg_pago=".$idprint2.") as retenciones,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Montepio' AND codg_pago=".$idprint2.") as montepios,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Reintegro' AND codg_pago=".$idprint2.") as reintegros,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Prestamo' AND codg_pago=".$idprint2.") as prestamos,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND codg_pago=".$idprint2.") as total,";
    $sql_dtll .= " (SELECT SUM(nd.mont_exce) FROM dependencias_pagos_exce nd WHERE codg_nmna=".$idprint." AND nd.codg_pago=".$idprint2.") AS exceso";
    $sql_dtll .= " from nominas_detalle nd WHERE nd.codg_nmna=".$idprint." AND nd.codg_pago=".$idprint2." GROUP BY nd.codg_pago";
    if (!$idprint2){
        $sql_dtll = "SELECT nd.codg_nmna,";
        $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Aporte' AND codg_pago IS NULL) as aportes,";
        $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Retenci�n' AND codg_pago IS NULL) as retenciones,";
        $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Montepio' AND codg_pago IS NULL) as montepios,";
        $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Reintegro' AND codg_pago IS NULL) as reintegros,";
        $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Prestamo' AND codg_pago IS NULL) as prestamos,";
        $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND codg_pago IS NULL) as total,";
        $sql_dtll .= " (SELECT SUM(nd.mont_exce) FROM dependencias_pagos_exce nd WHERE codg_nmna=".$idprint." AND nd.codg_pago IS NULL) AS exceso";
        $sql_dtll .= " from nominas_detalle nd WHERE nd.codg_nmna=".$idprint." AND nd.codg_pago IS NULL GROUP BY nd.codg_pago";        
    }
    if ($idprint3){
        $sql_dtll = "SELECT nd.codg_nmna,";
        $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Reintegro' AND codg_exce = ".$idprint3.") as reintegros,";
        $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND codg_exce = ".$idprint3.") as total";
        $sql_dtll .= " from nominas_detalle nd WHERE nd.codg_nmna=".$idprint." AND nd.codg_exce = ".$idprint3." GROUP BY nd.codg_pago";
    }
    $res_dtll = mysql_query($sql_dtll);
    while ($reg_dtll = mysql_fetch_array($res_dtll)){
        $detalles=$reg_dtll;
    }
    /// cargamos el valor de las Cuentas para retenci�n, aportes y montepio
    $sql_val="select * from valores WHERE des_val='CNT_AH' OR des_val='CNT_AP' OR des_val='CNT_MP".$reg_nmna['mess_nmna']."' OR des_val='CNT_PORPAGAR'";
    $res_val = mysql_query($sql_val);
    while ($row_val = mysql_fetch_array($res_val))
    {
        // el nombre de la variable es $CNT_AH y $CNT_AP
        $$row_val['des_val'] = $row_val['val_val'];
    }

    // buscar la cuenta Ahorros
    $sql_part = "SELECT * FROM cuentas c, plan_cuentas p WHERE c.codg_pcnta=".$CNT_AH." AND c.codg_pcuen=p.codg_pcuen";
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $res_cnta["ahorros"]["codg_pcnta"] = $res_part["codg_pcnta"];
    $res_cnta["ahorros"]["nmro_cnta"] = $res_part["nmro_cnta"];
    $res_cnta["ahorros"]["nomb_cnta"] = $res_part["nomb_cnta"];
    if ($res_part["stat_pcuen"]!='Activo'){
        $msg .= "Retenciones, ";
        break;
    }
    // buscar la cuenta Aporte
    $sql_part = "SELECT * FROM cuentas c, plan_cuentas p WHERE c.codg_pcnta=".$CNT_AP." AND c.codg_pcuen=p.codg_pcuen";
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $res_cnta["aporte"]["codg_pcnta"] = $res_part["codg_pcnta"];
    $res_cnta["aporte"]["nmro_cnta"] = $res_part["nmro_cnta"];
    $res_cnta["aporte"]["nomb_cnta"] = $res_part["nomb_cnta"];
    if ($res_part["stat_pcuen"]!='Activo'){
        $msg .= "Aportes, ";
    }
    // buscar la cuenta MONTEPIO
    $var_aux = "CNT_MP".$reg_nmna['mess_nmna'];
    $sql_part = "SELECT * FROM cuentas c, plan_cuentas p WHERE c.codg_pcnta=".$$var_aux." AND c.codg_pcuen=p.codg_pcuen";
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $res_cnta["montepio"]["codg_pcnta"] = $res_part["codg_pcnta"];
    $res_cnta["montepio"]["nmro_cnta"] = $res_part["nmro_cnta"];
    $res_cnta["montepio"]["nomb_cnta"] = $res_part["nomb_cnta"];
    if ($res_part["stat_pcuen"]!='Activo'){
        $msg .= "Montepio ".convertir_mes($reg_nmna['mess_nmna']).", ";
    }

    // buscar cuenta por pagar
    if ($idprint3){    
        $sql_part = "SELECT * FROM cuentas c, plan_cuentas p WHERE c.codg_pcnta=".$CNT_PORPAGAR." AND c.codg_pcuen=p.codg_pcuen";
        $res_part = mysql_fetch_array(mysql_query($sql_part));
        $res_cnta["porpagar"]["codg_pcnta"] = $res_part["codg_pcnta"];
        $res_cnta["porpagar"]["nmro_cnta"] = $res_part["nmro_cnta"];
        $res_cnta["porpagar"]["nomb_cnta"] = $res_part["nomb_cnta"];
        if ($res_part["stat_pcuen"]!='Activo'){
            $msg .= "Por pagar, ";
        }
    }
    
    //// datos del pago
    if ($idprint2){
        $sql_part = "SELECT * FROM dependencias_pagos dpago, banco_cuentas bcnta, cuentas, plan_cuentas pc WHERE dpago.codg_pago =".$idprint2." AND dpago.codg_cnta=bcnta.codg_cnta AND bcnta.codg_pcnta=cuentas.codg_pcnta AND cuentas.codg_pcuen=pc.codg_pcuen";
        $res_part = mysql_fetch_array(mysql_query($sql_part));
        if ($res_part["stat_pcuen"]!='Activo'){
            $msg .= "Banco ".$res_part["bnco_cnta"].", ";
        }
    }

    ///// si hay exceso
    if ($detalles[exceso]>0){ 
        $sql_exce = "SELECT * FROM dependencias_pagos_exce WHERE codg_nmna = ".$idprint." AND codg_pago = ".$idprint2;
        $res_exce = mysql_fetch_array(mysql_query($sql_exce));
        $sql_cntas = "SELECT * FROM cuentas WHERE codg_pcnta = ".$res_exce[codg_pcnta];
        $res_cntas = mysql_fetch_array(mysql_query($sql_cntas));
        $res_cnta["exce"]["codg_pcnta"] = $res_cntas["codg_pcnta"];
        $res_cnta["exce"]["nmro_cnta"] = $res_cntas["nmro_cnta"];
        $res_cnta["exce"]["nomb_cnta"] = $res_cntas["nomb_cnta"];
    }


    ////// Preparando los datos de las cuentas contables para mostrar
    if (!$idprint3){    
        $acum_cuentas = 1;
        $detalle_cuentas[$acum_cuentas]["id"] = $res_part['codg_pcnta'];
        $detalle_cuentas[$acum_cuentas]["codigo"] = $res_part['nmro_cnta'];
        $detalle_cuentas[$acum_cuentas]["concepto"] = $res_part['nomb_cnta'];
        $detalle_cuentas[$acum_cuentas]["debe"] = $detalles["total"]+$detalles[exceso];
        $acum_cuentas += 1;
        $detalle_cuentas[$acum_cuentas]["id"] = $res_cnta["ahorros"]["codg_pcnta"];
        $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["ahorros"]["nmro_cnta"];
        $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["ahorros"]["nomb_cnta"];
        $detalle_cuentas[$acum_cuentas]["haber"] = $detalles["retenciones"];
        $acum_cuentas += 1;
        $detalle_cuentas[$acum_cuentas]["id"] = $res_cnta["aporte"]["codg_pcnta"];
        $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["aporte"]["nmro_cnta"];
        $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["aporte"]["nomb_cnta"];
        $detalle_cuentas[$acum_cuentas]["haber"] = $detalles["aportes"];
        $acum_cuentas += 1;
        $detalle_cuentas[$acum_cuentas]["id"] = $res_cnta["montepio"]["codg_pcnta"];
        $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["montepio"]["nmro_cnta"];
        $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["montepio"]["nomb_cnta"];
        $detalle_cuentas[$acum_cuentas]["haber"] = $detalles["montepios"];
        $acum_cuentas += 1;
        $detalle_cuentas[$acum_cuentas]["id"] = $res_cnta["ahorros"]["codg_pcnta"];
        $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["ahorros"]["nmro_cnta"];
        $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["ahorros"]["nomb_cnta"].' (Reintegros)';
        $detalle_cuentas[$acum_cuentas]["haber"] = $detalles["reintegros"];
        $acum_cuentas += 1;
        if ($detalles[exceso]>0){ 
        $detalle_cuentas[$acum_cuentas]["id"] = $res_cnta["exce"]["codg_pcnta"];
        $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["exce"]["nmro_cnta"];
        $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["exce"]["nomb_cnta"];
        $detalle_cuentas[$acum_cuentas]["haber"] = $detalles["exceso"];
        $acum_cuentas += 1;
        }
    }
    else{
        $acum_cuentas = 1;
        $detalle_cuentas[$acum_cuentas]["id"] = $res_cnta["porpagar"]["codg_pcnta"];
        $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["porpagar"]["nmro_cnta"];
        $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["porpagar"]["nomb_cnta"];
        $detalle_cuentas[$acum_cuentas]["debe"] = $detalles["reintegros"];
        $acum_cuentas += 1;
        $detalle_cuentas[$acum_cuentas]["id"] = $res_cnta["ahorros"]["codg_pcnta"];
        $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["ahorros"]["nmro_cnta"];
        $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["ahorros"]["nomb_cnta"].' (Reintegros)';
        $detalle_cuentas[$acum_cuentas]["haber"] = $detalles["reintegros"];
        $acum_cuentas += 1;
    }

    ///////////////// PRESTAMOS
    $sql_prestamos = "SELECT SUM(pm.mnto_prtm) as sum_tot, SUM(pm.capi_prtm) as sum_capi, SUM(inte_prtm) as sum_inte, tpp.codg_tipo_pres as codg_tipo FROM nominas_detalle nd, prestamos_mov pm, prestamos prt, tipo_prestamos tpp WHERE nd.codg_nmna=".$idprint." AND nd.moti_dlle='Pr�stamo' AND nd.codg_pago=".$idprint2." AND nd.codg_dlle=pm.rela_prtm AND pm.codg_prst=prt.codg_prst AND prt.codg_tipo_pres=tpp.codg_tipo_pres GROUP BY tpp.codg_tipo_pres ORDER BY tpp.codg_tipo_pres";
    if(!$idprint2){        
       $sql_prestamos = "SELECT SUM(pm.mnto_prtm) as sum_tot, SUM(pm.capi_prtm) as sum_capi, SUM(inte_prtm) as sum_inte, tpp.codg_tipo_pres as codg_tipo FROM nominas_detalle nd, prestamos_mov pm, prestamos prt, tipo_prestamos tpp WHERE nd.codg_nmna=".$idprint." AND nd.moti_dlle='Pr�stamo' AND nd.codg_pago IS NULL AND nd.codg_dlle=pm.rela_prtm AND pm.codg_prst=prt.codg_prst AND prt.codg_tipo_pres=tpp.codg_tipo_pres GROUP BY tpp.codg_tipo_pres ORDER BY tpp.codg_tipo_pres";
    }
    $bus_prestamos = mysql_query($sql_prestamos);
    while ($res = mysql_fetch_array($bus_prestamos)){
        $detalle_prestamo[$res['codg_tipo']]['capital'] = $res['sum_capi'];
        $detalle_prestamo[$res['codg_tipo']]['intereses'] = $res['sum_inte'];
        $detalle_prestamo[$res['codg_tipo']]['total'] = $res['sum_tot'];    
    }
    //////////////// Preparamos los datos de la partidas de los prestamos
    $sql_part_print = "SELECT * FROM tipo_prestamos";
    $bus_part_print = mysql_query($sql_part_print);
    while ($res_part_print = mysql_fetch_array($bus_part_print)){
        // si el tipo de prestamo tiene algun monto en esta nomina 
        if ($detalle_prestamo[$res_part_print['codg_tipo_pres']]['total']>0){
          // consultamos los datos de la cuenta para capital
          $sql_part = "SELECT * FROM cuentas c, plan_cuentas p WHERE c.codg_pcnta=".$res_part_print['codg_pcnta']." AND c.codg_pcuen=p.codg_pcuen";  
          $res_part = mysql_fetch_array(mysql_query($sql_part));
          if ($res_part["stat_pcuen"]!='Activo'){
            $msg .= 'Capital de '.$res_part_print[nomb_tipo_pres].', ';
          }
          $detalle_cuentas[$acum_cuentas]["id"] = $res_part['codg_pcnta'];
          $detalle_cuentas[$acum_cuentas]["codigo"] = $res_part['nmro_cnta'];
          $detalle_cuentas[$acum_cuentas]["concepto"] = $res_part['nomb_cnta'];
          $detalle_cuentas[$acum_cuentas]["haber"] = $detalle_prestamo[$res_part_print['codg_tipo_pres']]['capital'];
          $acum_cuentas += 1;
          // consultamos los datos de la cuenta para los intereses
          $sql_part = "SELECT * FROM cuentas c, plan_cuentas p WHERE c.codg_pcnta=".$res_part_print['codg_pcnta_int']." AND c.codg_pcuen=p.codg_pcuen";  
          $res_part = mysql_fetch_array(mysql_query($sql_part));
          if ($res_part["stat_pcuen"]!='Activo'){
             $msg .= 'Intereses de '.$res_part_print[nomb_tipo_pres].', ';
          }
          $detalle_cuentas[$acum_cuentas]["id"] = $res_part['codg_pcnta'];
          $detalle_cuentas[$acum_cuentas]["codigo"] = $res_part['nmro_cnta'];
          $detalle_cuentas[$acum_cuentas]["concepto"] = $res_part['nomb_cnta'];
          $detalle_cuentas[$acum_cuentas]["haber"] = $detalle_prestamo[$res_part_print['codg_tipo_pres']]['intereses'];
          $acum_cuentas += 1;
        }
    }  
    
    
    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'. 
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
$html='<html>
<head>
    <base target="_blank">
    <title>Reportes de Nomina - Asientos Contables</title>
<style type="text/css">
    .reporte
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .reporte a
    {
        font-weight: bold;
        color: #0000FF;
    }
    .titulo {
        font-family: arial; 
        font-size: 13pt; 
        font-weight: bold; 
        color: #000000; 
        background-color: #67BABA; 
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 12px;
        font-weight: bold;
    }    
    .tablanomina {
        font-family: Arial; 
        font-size: 9px; 
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 120px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
#header,
#footer {
    position: fixed;  
    left: 0;
    right: 0;
	font-size: 0.9em;
}
#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
</style>
</head>
<body>
<div id="header">
    <table cellspacing="0" cellpadding="0" border="0 align="center" class="reporte" >
    <tr height="1%">
        <td width="210px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td>
            <div align="center"><h3>'.$dependencia.'<BR>ASIENTOS CONTABLES DE NOMINA<BR>('.$nomina.')</h2></div>
        </td>
        <td width="210px">
            <div align="right">Lugar y Fecha de Impresi�n: <br>Ejido, '.date(d).' de '.convertir_mes(date(m)).' de '.redondear(date(Y),0,".","").'&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    </table>
</div>
<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>';
if ($nomina_interna!='SI'){
    if($idprint2){        
    $html.='
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Datos del Pago</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas">Banco Origen</td>
                    <td class="etiquetas">Banco / Cuenta Destino</td>
                    <td class="etiquetas">Referencia</td>
                    <td class="etiquetas">Fecha</td>
                </tr>
                <tr align="center">
                    <td>'.$reg_pago["banc_orig"].'</td>
                    <td>'.$reg_pago["bnco_cnta"].': '.$reg_pago["nmro_cnta"].'</td>
                    <td>'.$reg_pago["numr_refe"].'</td>
                    <td>'.date("d-m-Y",$reg_pago["fcha_pago"]).'</td>
                </tr>
            </table>
            <br>';
    }
    if($idprint3){        
        switch ($res_porpagar[prdo_nmna]) {
           case 8:
              $porpagar_nom = 'Mes';
              break;
           case 7:
              $porpagar_nom = '2da Quincena';
              break;
           case 6:
              $porpagar_nom = '1era Quincena';
              break;
           case ($res_porpagar[prdo_nmna] > 10):
              $porpagar_nom = 'Semana '.($res_porpagar[prdo_nmna]-10).'�';
              break;
           default:
              # code...
              break;
        }
        $num_cuenta = $res_porpagar[nmro_cnta];
        $nom_cuenta = $res_porpagar[nomb_cnta];
        $referencia = $porpagar_nom.' '.convertir_mes($res_porpagar[mess_nmna]).' '.$res_porpagar[anno_nmna];
        $html.='
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Origen del Pago por Cuenta por pagar</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas">C�digo de Cuenta</td>
                    <td class="etiquetas">Nombre de Cuenta</td>
                    <td class="etiquetas">N�mina</td>
                </tr>
                <tr align="center">
                    <td>'.$num_cuenta.'</td>
                    <td>'.$nom_cuenta.'</td>
                    <td>'.$referencia.'</td>
                </tr>
            </table>
            <br>';
    }
}

              $html.='<br><table width="90%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo"><td colspan="4">Datos de las Cuentas</td></tr>
               <tr class="etiquetas" align="center"><td width="100px">C�DIGO</td><td>CONCEPTO</td><td width="100px">DEBE</td><td width="100px">HABER</td></tr>
                 '; 
                 for ($k=1;$k<$acum_cuentas;$k++){
                    if ($detalle_cuentas[$k]["debe"]>0 || $detalle_cuentas[$k]["haber"]>0) {
                         $html.='<tr><td>&nbsp;'.$detalle_cuentas[$k]["codigo"].'</td><td>&nbsp;'.$detalle_cuentas[$k]["concepto"].'</td><td align="right">'; if ($detalle_cuentas[$k]["debe"]>0) { $html.=redondear($detalle_cuentas[$k]["debe"],2,".",","); } $html.='&nbsp;</td><td align="right">'; if ($detalle_cuentas[$k]["haber"]>0) { $html.= redondear($detalle_cuentas[$k]["haber"],2,".",","); } $html.='&nbsp;</td></tr>';
                         $suma_debe += $detalle_cuentas[$k]["debe"];
                         $suma_haber += $detalle_cuentas[$k]["haber"];
                    }
                 }
                 $html.='<tr class="etiquetas"><td colspan="2" align="right">TOTALES (SUMAS IGUALES)&nbsp;</td><td align="right">'.redondear($suma_debe,2,".",",").'&nbsp;</td><td align="right">'.redondear($suma_haber,2,".",",").'&nbsp;</td></tr>';
                 $html.='</table>';
$html .= '</body></html>';

if (!$msg) {   
    if ($_GET['status']=='ver'){
        echo $html;
     //   generar_pdf($html,'Aportes_y_Ahorros.pdf','letter','portrait');
    }
    if ($_GET['status']=='generar'){
        $fech_asien = date("Y-m-d",$reg_pago["fcha_pago"]);
        $desc_asien = 'Nomina '.$dependencia.' ('.$nomina.')';
        $stat_asien = 'Bloqueado';
        $orgn_asien = 'Nomina';
        $codg_rela = $idprint;
        $codg2_rela = $idprint2;
        if ($idprint3>0){
            $fech_asien = date("Y-m-d");
            $codg2_rela = 0;
            $codg3_rela = $idprint3;
        }
        $sql_insert = "INSERT INTO asientos (fech_asien, desc_asien, stat_asien, orgn_asien, codg_rela, codg2_rela, codg3_rela) VALUES ('".$fech_asien."', '".$desc_asien."', '".$stat_asien."', '".$orgn_asien."', '".$codg_rela."', '".$codg2_rela."', '".$codg3_rela."');";
        mysql_query($sql_insert);
        $id = mysql_insert_id();
        $sql_insert = "INSERT INTO movimientos_contables (codg_asien, codg_pcnta, nmro_pcnta, nomb_pcnta, debe_movi, haber_movi) VALUES ";
        for ($k=1;$k<$acum_cuentas;$k++){
            if ($detalle_cuentas[$k]["debe"]>0 || $detalle_cuentas[$k]["haber"]>0) {
                $sql_insert2 .= "('".$id."', '".$detalle_cuentas[$k]["id"]."', '".$detalle_cuentas[$k]["codigo"]."', '".$detalle_cuentas[$k]["concepto"]."', '".$detalle_cuentas[$k]["debe"]."', '".$detalle_cuentas[$k]["haber"]."'),";
            }
        }
        $sql_insert2 .= ',';
        $sql_insert2 = str_replace(',,', ';', $sql_insert2);
        $sql_insert = $sql_insert.' '.$sql_insert2;
        mysql_query($sql_insert);
        echo  '<SCRIPT LANGUAGE="javascript">location.href = "../reportes/reporte_asientos.php?asiento2='.$id.'";</SCRIPT>';
    }
    if ($_GET['status']=='eliminar' && $_GET['asiento']){
        $sql_eliminar = "DELETE FROM movimientos_contables WHERE codg_asien=".$_GET['asiento'].";";
        $sql_eliminar = "DELETE FROM asientos WHERE codg_asien=".$_GET['asiento'].";";
        mysql_query($sql_eliminar);
    }
}
else {
    $msg = "Las cuentas ".$msg." deben ser actuallizadas";
    echo '<script>alert("'.$msg.'")</script>';
    if ($_GET['status']=='ver') {
        echo '<script>window.close();</script>';   
    }
}
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');?>