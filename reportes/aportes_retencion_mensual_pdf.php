<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); 
include ('../reportes/generarpdf.php');
$idprint=$_GET['codg_depn']; 
$idprint2=$_GET['mes']; 
$idprint3=$_GET['anno']; 
    //////////////// datos de la dependencia
    if ($idprint!=''){
      $sql_dep = "SELECT nomb_depn FROM dependencias WHERE codg_depn=".$idprint;
      $dependencia=mysql_fetch_array(mysql_query($sql_dep));
      $dependencia=$dependencia[0];
    }
    else{
      $dependencia="Todas las Dependencias";
    }
    //// si la dependencia es la misma interna
        /// cargamos el valor del codigo interno de la dependencia interna
        $sql_val="select * from valores WHERE des_val='DEP_INT'";
        $res_val = mysql_query($sql_val);
        while ($row_val = mysql_fetch_array($res_val))
        {
                // el nombre de la variable es $DEP_INT
                $$row_val['des_val'] = $row_val['val_val'];
        }
        if ($idprint==$DEP_INT){
	       $interna = 1;
	       $mensaje_interna="Esta dependencia no efectua Dep�sitos para el pago de n�mina debido a que es la Propia Caja de Ahorro";
	}
	    
    //////////////// Datos de los Detalles de la Nomina
    $cuenta_datos = 0;
    $sql_dtll = "SELECT sc.cedu_soci, CONCAT(apel_soci, ' ', nomb_soci) as nomb_soci,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle n_d, dependencias_pagos dp_pg WHERE n_d.codg_pago=dp_pg.codg_pago AND n_d.moti_dlle='Aporte' AND n_d.cedu_soci=sc.cedu_soci AND MONTH(dp_pg.fcha_pago)=".$idprint2." AND YEAR(dp_pg.fcha_pago)=".$idprint3.") as aporte,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle n_d, dependencias_pagos dp_pg WHERE n_d.codg_pago=dp_pg.codg_pago AND n_d.moti_dlle='Retenci�n' AND n_d.cedu_soci=sc.cedu_soci AND MONTH(dp_pg.fcha_pago)=".$idprint2." AND YEAR(dp_pg.fcha_pago)=".$idprint3.") as retencion,";
    $sql_dtll .= " depn.nomb_depn as dependencia";
    $sql_dtll .= " from nominas_detalle nd, socios sc, dependencias_pagos depn_pg, dependencias depn";
    $sql_dtll .= " WHERE nd.cedu_soci=sc.cedu_soci AND nd.codg_pago=depn_pg.codg_pago AND MONTH(depn_pg.fcha_pago)=".$idprint2." AND YEAR(depn_pg.fcha_pago)=".$idprint3;
      $sql_dtll .= " AND depn_pg.codg_depn=depn.codg_depn";
    if ($idprint){
      $sql_dtll .= " AND depn_pg.codg_depn=depn.codg_depn AND depn_pg.codg_depn=".$idprint;
    }
    $sql_dtll .= " GROUP BY nd.cedu_soci ORDER BY depn.nomb_depn,sc.apel_soci, sc.nomb_soci";
    $res_dtll = mysql_query($sql_dtll);
    $totales_general = array();
    $totales_dep = array();
    $cuenta_dep = 0;
    while ($reg_dtll = mysql_fetch_array($res_dtll)){
        $cuenta_datos += 1;
        $detalles[$cuenta_datos]=$reg_dtll;
        $totales[$cuenta_datos]= Redondear(($detalles[$cuenta_datos]["aporte"]+$detalles[$cuenta_datos]["retencion"]),2,"",".");
        $totales_general["aporte"]+=$detalles[$cuenta_datos]["aporte"];
        $totales_general["retencion"]+=$detalles[$cuenta_datos]["retencion"];
        $totales_general["total"]+=$totales[$cuenta_datos];
        if ($detalles[$cuenta_datos]["dependencia"]!=$detalles[$cuenta_datos-1]["dependencia"]){
          $cuenta_dep += 1;
        }
        $totales_dep[$cuenta_dep]["aporte"]+=$detalles[$cuenta_datos]["aporte"];
        $totales_dep[$cuenta_dep]["retencion"]+=$detalles[$cuenta_datos]["retencion"];
        $totales_dep[$cuenta_dep]["total"]+=$totales[$cuenta_datos];
    }
    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'. 
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
$html='<html>
<head>
    <base target="_blank">
    <title>Reportes de Nomina - Aportes y Ahorros</title>
<style type="text/css">
    .reporte
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .reporte a
    {
        font-weight: bold;
        color: #0000FF;
    }
    .titulo {
        font-family: arial; 
        font-size: 13pt; 
        font-weight: bold; 
        color: #000000; 
        background-color: #67BABA; 
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 12px;
        font-weight: bold;
    }    
    .tablanomina {
        font-family: Arial; 
        font-size: 9px; 
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 120px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
#header,
#footer {
    position: fixed;  
    left: 0;
    right: 0;
	font-size: 0.9em;
}
#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
</style>
</head>
<body>
<div id="header">
    <table cellspacing="0" cellpadding="0" border="0 align="center" class="reporte" >
    <tr height="1%">
        <td width="210px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td>
            <div align="center"><h3>'.$dependencia.'<BR>APORTES Y AHORROS EN N�MINA<BR>(Depositados en '.convertir_mes($idprint2).' de '.redondear($idprint3,0,'.','').')</h2></div>
        </td>
        <td width="210px">
            <div align="right">Lugar y Fecha de Impresi�n: <br>Ejido, '.date(d).' de '.convertir_mes(date(m)).' de '.redondear(date(Y),0,".","").'&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    </table>
</div>
<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>
            <center><h2>'.$mensaje_interna.'</h2></center>
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="6">Montos Acreditados</td>
                </tr>';
                if ($idprint){
                $html.= '<tr align="center">
                    <td class="etiquetas" width="30px">N�</td>
                    <td class="etiquetas" width="100px">C�dula</td>
                    <td class="etiquetas">Apellidos y Nombres</td>
                    <td class="etiquetas" width="80px">Aporte</td>
                    <td class="etiquetas" width="80px">Ahorro</td>
                    <td class="etiquetas" width="80px">Total</td>
                </tr>';
                }
                $cuenta_dep_print=0;
                for ($i=1;$i<=$cuenta_datos;$i++){
                        if (($detalles[$i]["dependencia"]!=$detalles[$i-1]["dependencia"]) && !$idprint){
                          $html .= '<tr>
                            <td align="center" colspan="6"><b>'.$detalles[$i]["dependencia"].'</b></td>
                          </tr>
                          <tr align="center">
                              <td class="etiquetas" width="30px">N�</td>
                              <td class="etiquetas" width="100px">C�dula</td>
                              <td class="etiquetas">Apellidos y Nombres</td>
                              <td class="etiquetas" width="80px">Aporte</td>
                              <td class="etiquetas" width="80px">Ahorro</td>
                              <td class="etiquetas" width="80px">Total</td>
                          </tr>';
                        }
                        $html .= '<tr>
                            <td align="right">'.$i.'&nbsp;</td>
                            <td align="right">'.Redondear($detalles[$i]["cedu_soci"],0,".","").'&nbsp;</td>
                            <td>&nbsp;'.$detalles[$i]["nomb_soci"].'</td>
                            <td align="right">'.Redondear($detalles[$i]["aporte"],2,".",",").'&nbsp;</td>
                            <td align="right">'.Redondear($detalles[$i]["retencion"],2,".",",").'&nbsp;</td>
                            <td align="right">'.Redondear($totales[$i],2,".",",").'&nbsp;</td>
                        </tr>';
                        if (!$idprint && (!$detalles[$i+1]["dependencia"] || ($detalles[$i]["dependencia"]!=$detalles[$i+1]["dependencia"]) && $i>1)){
                            $cuenta_dep_print += 1;
                            $html .= '<tr class="etiquetas">
                              <td align="right" colspan="3">SUB-TOTALES&nbsp;</td>
                              <td align="right">'.Redondear($totales_dep[$cuenta_dep_print]["aporte"],2,".",",").'&nbsp;</td>
                              <td align="right">'.Redondear($totales_dep[$cuenta_dep_print]["retencion"],2,".",",").'&nbsp;</td>
                              <td align="right">'.Redondear($totales_dep[$cuenta_dep_print]["total"],2,".",",").'&nbsp;</td>
                            </tr>';
                        }
                }
$html .= '<tr align="center" class="titulo">
                    <td colspan="3" align="right">T O T A L E S&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["aporte"],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["retencion"],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["total"],2,'.',',').'&nbsp;</td>
                </tr>
             </table>';
$html .= '</body></html>';
echo $html;
//generar_pdf($html,'Aportes_y_Ahorros.pdf','letter','portrait');
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');?>
