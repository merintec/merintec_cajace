<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); 
include ('../reportes/generarpdf.php');
$cedula = $_GET[cedu_soci];

/// cargamos el valor de los porcentajes de retenci�n, aportes 
    $sql_val="select * from valores WHERE des_val='PR' OR des_val='PA' OR des_val ='PRM' OR des_val ='PPM'";
    $res_val = mysql_query($sql_val);
    while ($row_val = mysql_fetch_array($res_val))
    {
        // el nombre de la variable es $SUSTRAC_ISRL
        $$row_val['des_val'] = $row_val['val_val'];
    }

////// Datos del Socio
$sql_socio = "SELECT * FROM socios WHERE cedu_soci = ".$cedula;
$reg_socio = mysql_fetch_array(mysql_query($sql_socio));
if ($reg_socio[cedu_soci]){
    ////// Datos del primer Estatus
    $sql_status1 = "SELECT * from socios_estado se, tipos_status ts WHERE se.cedu_soci = ".$cedula." AND se.codg_stat=ts.codg_stat ORDER BY se.fcha_estd ASC LIMIT 1";
    $reg_status1 = mysql_fetch_array(mysql_query($sql_status1));
    ////// Datos del �ltimo Estatus
    $sql_status2 = "SELECT * from socios_estado se, tipos_status ts WHERE se.cedu_soci = ".$cedula." AND se.codg_stat=ts.codg_stat ORDER BY se.fcha_estd DESC LIMIT 1";
    $reg_status2 = mysql_fetch_array(mysql_query($sql_status2));
    ////// Datos Laborales
    $sql_lab = "SELECT * FROM socios_dat_lab sl, dependencias d WHERE sl.cedu_soci = ".$cedula." AND sl.codg_depn=d.codg_depn ORDER BY sl.fchi_dlab DESC LIMIT 1"; 
    $reg_lab = mysql_fetch_array(mysql_query($sql_lab));
    if ($reg_lab[aprt_dlab]==0){
        $reg_lab[aprt_dlab] = $PA;
    }
    if ($reg_lab[retn_dlab]==0){
        $reg_lab[retn_dlab] = $PR;
    }
    ////// ULTIMA N�MINA PROCESADA EN LA DEPENDENCIA ACTUAL
    //echo $sql_last = "SELECT n.anno_nmna, n.mess_nmna, n.prdo_nmna, d.nomb_depn, (SELECT mnto_dlle FROM nominas_detalle WHERE codg_nmna = n.codg_nmna AND cedu_soci = nd.cedu_soci AND moti_dlle = 'Prestamo') as prestamo, (SELECT mnto_dlle FROM nominas_detalle WHERE codg_nmna = n.codg_nmna AND cedu_soci = nd.cedu_soci AND moti_dlle = 'Montepio') as montepio, (SELECT mnto_dlle FROM nominas_detalle WHERE codg_nmna = n.codg_nmna AND cedu_soci = nd.cedu_soci AND moti_dlle = 'Retencion') as retencion, (SELECT mnto_dlle FROM nominas_detalle WHERE codg_nmna = n.codg_nmna AND cedu_soci = nd.cedu_soci AND moti_dlle = 'Aporte') as aporte, (SELECT mnto_dlle FROM nominas_detalle WHERE codg_nmna = n.codg_nmna AND cedu_soci = nd.cedu_soci AND moti_dlle = 'Reintegro') as reintegro FROM nominas n, nominas_detalle nd, dependencias d WHERE n.codg_nmna = nd.codg_nmna AND n.codg_depn = d.codg_depn AND nd.cedu_soci = ".$cedula." ORDER BY n.anno_nmna DESC, n.mess_nmna DESC, n.prdo_nmna DESC LIMIT 1";
    $sql_last = "SELECT n.anno_nmna, n.mess_nmna, n.prdo_nmna, d.nomb_depn, (SELECT mnto_dlle FROM nominas_detalle WHERE codg_nmna = n.codg_nmna AND cedu_soci = nd.cedu_soci AND moti_dlle = 'Montepio' LIMIT 1) as montepio, (SELECT mnto_dlle FROM nominas_detalle WHERE codg_nmna = n.codg_nmna AND cedu_soci = nd.cedu_soci AND moti_dlle = 'Retencion' LIMIT 1) as retencion, (SELECT mnto_dlle FROM nominas_detalle WHERE codg_nmna = n.codg_nmna AND cedu_soci = nd.cedu_soci AND moti_dlle = 'Aporte' LIMIT 1) as aporte FROM nominas n, nominas_detalle nd, dependencias d WHERE n.codg_nmna = nd.codg_nmna AND n.codg_depn = d.codg_depn AND nd.cedu_soci = ".$cedula." ORDER BY n.anno_nmna DESC, n.mess_nmna DESC, n.prdo_nmna DESC LIMIT 1";
    $res_last = mysql_fetch_array(mysql_query($sql_last));
    if ($res_last[aporte]>0) { $ultimo_aporte = redondear($res_last[aporte],2,'.',','); } else { $ultimo_aporte = '0,00'; }
    if ($res_last[retencion]>0) { $ultimo_retencion = redondear($res_last[retencion],2,'.',','); } else { $ultimo_retencion = '0,00'; }
    if ($res_last[montepio]>0) { $ultimo_montepio = redondear($res_last[montepio],2,'.',','); } else { $ultimo_montepio = '0,00'; }
    if ($res_last[prestamo]>0) { $ultimo_prestamo = redondear($res_last[prestamo],2,'.',','); } else { $ultimo_prestamo = '0,00'; }
    if ($res_last[reintegro]>0) { $ultimo_reintegro = redondear($res_last[reintegro],2,'.',','); } else { $ultimo_reintegro = '0,00'; }
    $ultimo_dependencia = $res_last[nomb_depn];
    $ultimo_anno = redondear($res_last[anno_nmna],0,'.','');
    $ultimo_mess = convertir_mes ($res_last[mess_nmna]);
    switch ($res_last[prdo_nmna]) {
         case 6:
             $ultimo_periodo = '1era Quincena';
             break;
         case 7:
             $ultimo_periodo = '2da Quincena';
             break;
         case 8:
             $ultimo_periodo = 'Mes';
             break;
         case ($res_last[prdo_nmna] >= 11):
             $ultimo_periodo = ($res_last[prdo_nmna]-10).' Semana';
             break;         
         default:
             $ultimo_periodo = 'Error';
             break;
    } 
    ////// SALDOS A LA FECHA ACTUAL
        $sql = "SELECT cedu_soci, ";
        $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_ing, ";
        $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_egr, "; 
        $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_ing, ";
        $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_egr, "; 
        $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_ing, ";
        $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_egr, ";
        $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='AP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_p_ing, ";
        $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='RP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_p_ing, ";
        $sql .= "(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='IP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_p_ing ";
        $sql .= "FROM socios_movimientos sm WHERE cedu_soci='".$cedula."' GROUP BY sm.cedu_soci ORDER BY codg_movi";
        $res = mysql_fetch_array(mysql_query($sql));
        $saldo_apr = $res['apr_ing']-$res['apr_egr'];
        $saldo_ret = $res['ret_ing']-$res['ret_egr'];
        $saldo_rei = $res['rei_ing']-$res['rei_egr'];
        $saldo_apr_p = $res['apr_p_ing'];
        $saldo_ret_p = $res['ret_p_ing'];
        $saldo_rei_p = $res['rei_p_ing'];

        if ($saldo_apr==NULL){ $saldo_apr = 0; }
        if ($saldo_ret==NULL){ $saldo_ret = 0; }
        if ($saldo_rei==NULL){ $saldo_rei = 0; }
        if ($saldo_apr_p==NULL){ $saldo_apr_p = 0; }
        if ($saldo_ret_p==NULL){ $saldo_ret_p = 0; }
        if ($saldo_rei_p==NULL){ $saldo_rei_p = 0; }

        $saldo_tot = $saldo_apr + $saldo_ret + $saldo_rei;

        // actualizamos los saldos
        $sql_insert = "UPDATE socios_saldo SET fcha_sald='".date('Y-m-d')."', apor_sald = ".$saldo_apr.", rete_sald = ".$saldo_ret.", rein_sald = ".$saldo_rei.", apor_comp = ".$saldo_apr_p.", rete_comp = ".$saldo_ret_p.", rein_comp = ".$saldo_rei_p." WHERE cedu_soci=".$cedula."";
        mysql_query ($sql_insert);
        mysql_error ();

    ////// Saldo disponible para retiros
    $sql_ret = "SELECT *";
    $sql_ret .= ", (SELECT SUM(mont_movi) AS apr_ing FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='I' AND cedu_soci=r.cedu_soci AND fcha_movi > r.fcha_acta GROUP BY cedu_soci ) as apr_ing, "; 
    $sql_ret .= "(SELECT SUM(mont_movi) AS apr_ing FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='R' AND cedu_soci=r.cedu_soci AND fcha_movi > r.fcha_acta GROUP BY cedu_soci ) as apr_egr, "; 
    $sql_ret .= "(SELECT SUM(mont_movi) AS apr_ing FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='I' AND cedu_soci=r.cedu_soci AND fcha_movi > r.fcha_acta GROUP BY cedu_soci ) as ret_ing, ";
    $sql_ret .= "(SELECT SUM(mont_movi) AS apr_ing FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='E' AND cedu_soci=r.cedu_soci AND fcha_movi > r.fcha_acta GROUP BY cedu_soci ) as ret_egr, "; 
    $sql_ret .= "(SELECT SUM(mont_movi) AS apr_ing FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='I' AND cedu_soci=r.cedu_soci AND fcha_movi > r.fcha_acta GROUP BY cedu_soci ) as rei_ing, ";
    $sql_ret .= "(SELECT SUM(mont_movi) AS apr_ing FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='E' AND cedu_soci=r.cedu_soci AND fcha_movi > r.fcha_acta GROUP BY cedu_soci ) as rei_egr, ";
    $sql_ret .= "(SELECT SUM(mont_movi) AS apr_ing FROM socios_movimientos WHERE dest_movi='AP' AND tipo_movi='I' AND cedu_soci=r.cedu_soci AND fcha_movi > r.fcha_acta GROUP BY cedu_soci ) as apr_p_ing, ";
    $sql_ret .= "(SELECT SUM(mont_movi) AS apr_ing FROM socios_movimientos WHERE dest_movi='RP' AND tipo_movi='I' AND cedu_soci=r.cedu_soci AND fcha_movi > r.fcha_acta GROUP BY cedu_soci ) as ret_p_ing, ";
    $sql_ret .= "(SELECT SUM(mont_movi) AS apr_ing FROM socios_movimientos WHERE dest_movi='IP' AND tipo_movi='I' AND cedu_soci=r.cedu_soci AND fcha_movi > r.fcha_acta GROUP BY cedu_soci ) as rei_p_ing ";
    $sql_ret .=" FROM retiros r WHERE r.cedu_soci = ".$cedula." AND r.stat_reti='A' ORDER BY r.fcha_acta DESC LIMIT 1";
    if ($reg_ret = mysql_fetch_array(mysql_query($sql_ret))){
        $saldo_apr_rp = $reg_ret['apr_ing']-$reg_ret['apr_egr'];
        $saldo_ret_rp = $reg_ret['ret_ing']-$reg_ret['ret_egr'];
        $saldo_rei_rp = $reg_ret['rei_ing']-$reg_ret['rei_egr'];
        $saldo_apr_p_rp = $reg_ret['apr_p_ing'];
        $saldo_ret_p_rp = $reg_ret['ret_p_ing'];
        $saldo_rei_p_rp = $reg_ret['rei_p_ing'];

        if ($saldo_apr_rp==NULL){ $saldo_apr_rp = 0; }
        if ($saldo_ret_rp==NULL){ $saldo_ret_rp = 0; }
        if ($saldo_rei_rp==NULL){ $saldo_rei_rp = 0; }
        if ($saldo_apr_p_rp==NULL){ $saldo_apr_p_rp = 0; }
        if ($saldo_ret_p_rp==NULL){ $saldo_ret_p_rp = 0; }
        if ($saldo_rei_p_rp==NULL){ $saldo_rei_p_rp = 0; }

        $saldo_tot_rp = $saldo_apr_rp + $saldo_ret_rp + $saldo_rei_rp;
    }
    else {
        $saldo_tot_rp = $saldo_tot;   
    }
    ////// Prestamos pendientes por pagar
    $fecha_max = date('Y-m-d');
    $cuenta_p = 1;
        $sql_prestamos = "SELECT *, IF ((SELECT SUM(capi_prtm) from prestamos_mov WHERE codg_prst = p.codg_prst AND freg_prtm <= '".$fecha_max."' GROUP BY codg_prst),(SELECT SUM(capi_prtm) from prestamos_mov WHERE codg_prst = p.codg_prst AND freg_prtm <= '".$fecha_max."' GROUP BY codg_prst),0) AS pagado FROM prestamos p, tipo_prestamos tp WHERE p.codg_tipo_pres = tp.codg_tipo_pres AND p.cedu_soci = '".$cedula."' AND p.fcha_acta < '".$fecha_max."'  GROUP BY p.codg_prst HAVING (pagado) <= p.mont_apro";
        $bus_prestamos = mysql_query($sql_prestamos);
        while($res_prestamos = mysql_fetch_array($bus_prestamos)){
            $prestamos[$cuenta_p] = $res_prestamos;
            if ($res_prestamos[sald_inic_real] > 0){
                $prestamos[$cuenta_p][mont_apro] = $res_prestamos[sald_inic_real];
                $prestamos[$cuenta_p][mont_cuot] = $res_prestamos[mont_cuot_real];
            }
            $sql_fianza = "SELECT * FROM prestamos_fia WHERE codg_prst = ".$res_prestamos[codg_prst];
            $bus_fianza = mysql_query($sql_fianza);
            $fianza = '<ul>';
            while ($res_fianza = mysql_fetch_array($bus_fianza)) {
                $fianza .= '<li>';
                if ($res_fianza[desc_fian]) { 
                    $fianza .= $res_fianza[desc_fian].'.'; 
                } 
                else { 
                    $sql_fiador = "SELECT * FROM socios WHERE cedu_soci = ".$res_fianza[cedu_soci];
                    $bus_fiador = mysql_query($sql_fiador);
                    if ($res_fiador = mysql_fetch_array($bus_fiador)){
                        $fianza .= $res_fiador[apel_soci].' '.$res_fiador[nomb_soci].'.<br>C.I.&nbsp;'.redondear($res_fianza[cedu_soci],0,'.',',');    
                    }
                }
                $fianza .= '<br><b>Monto Bs:&nbsp;'.redondear($res_fianza[mont_fian],2,'.',',').'</b>';
                $fianza .= '<br><font color="#ff0000"><b>'.$res_fianza[libre_fian].'</b></font>';
                $fianza .= '</li>';
            }
            $fianza .= '</ul>';
            $prestamos_fianza[$cuenta_p] = $fianza;
            $total_prestamos += redondear(($prestamos[$cuenta_p][mont_apro] - $prestamos[$cuenta_p][pagado]),2,'','.'); 
            $cuenta_p += 1;
        }

    ////// Prestamos pendientes por pagar en los que sirvi� de fiador
    $fecha_max = date('Y-m-d');
    $cuenta_f = 1;
        $sql_prestamos = "SELECT *, IF ((SELECT SUM(capi_prtm) from prestamos_mov WHERE codg_prst = p.codg_prst GROUP BY codg_prst),(SELECT SUM(capi_prtm) from prestamos_mov WHERE codg_prst = p.codg_prst GROUP BY codg_prst),0) AS pagado FROM prestamos p, prestamos_fia pf, tipo_prestamos pt, socios s WHERE pf.cedu_soci = ".$cedula." AND pf.codg_prst=p.codg_prst AND p.codg_tipo_pres=pt.codg_tipo_pres AND pf.cedu_soli = s.cedu_soci GROUP BY pf.codg_prst HAVING (pagado) < p.mont_apro";
        $bus_prestamos = mysql_query($sql_prestamos);
        while($res_prestamos = mysql_fetch_array($bus_prestamos)){
            $prestamosf[$cuenta_f] = $res_prestamos;
            if ($res_prestamos[sald_inic_real] > 0){
                $prestamosf[$cuenta_f][mont_apro] = $res_prestamos[sald_inic_real];
                $prestamosf[$cuenta_f][mont_cuot] = $res_prestamos[mont_cuot_real];
            }
            if ($res_prestamos[libre_fian]==''){
                $total_fianzas += redondear(($prestamosf[$cuenta_f][mont_fian]),2,'','.'); 
            }
            $cuenta_f += 1;
        }
}

        $print_aportes = redondear(($saldo_apr + $saldo_rei),2,'.',','); 
        $print_ahorros = redondear($saldo_ret,2,'.',',');
        $print_capital = redondear($saldo_tot,2,'.',',');
        $print_bloq_p  = redondear(($total_prestamos + $total_fianzas),2,'.',',');
        $print_bloq_r  = redondear(($saldo_tot - $saldo_tot_rp),2,'.',',');
        /////// calculando monto disponible para prestamos
        $bloq_p = redondear(($total_prestamos + $total_fianzas),2,'','.'); 
        $bloq_r = redondear(($saldo_tot - $saldo_tot_rp),2,'','.'); 
        $dis_prest = redondear(($saldo_tot - $bloq_p),2,'','.'); 
        $dis_prest = redondear(($dis_prest * $PPM),2,'','.');
        if ($dis_prest > 0 ) { 
            $print_dis_prest = redondear(($dis_prest),2,'.',','); 
        } 
        else { 
            $print_dis_prest =  '0,00';
        }
        ////// Calculando monto disponible para Retiro
        $dis_retir = redondear(($saldo_tot * $PRM),2,'','.');
        $dis_retir = redondear(($dis_retir - $bloq_p),2,'','.');
        $print_dis_retir = redondear(($dis_retir),2,'.',',');
        if ($dis_retir <= 0.00 ){
            $print_dis_retir = '0,00';           
        }


        /////// retiros efectuados
//echo        $sql_retiros = "SELECT * FROM retiros WHERE cedu_soci = ".$cedula;

    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'. 
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
$html='<html>
<head>
    <base target="_blank">
    <title>Reportes de Estado de Cuenta</title>
<style type="text/css">
    .reporte
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .reporte a
    {
        font-weight: bold;
        color: #0000FF;
    }
    .titulo {
        font-family: arial; 
        font-size: 11pt; 
        font-weight: bold; 
        color: #000000; 
        background-color: #67BABA; 
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 11px;
        font-weight: bold;
    }    
    .resultados {
        color: #000000;
        font-size: 10px;
    }    
    .tablanomina {
        font-family: Arial; 
        font-size: 9px; 
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 120px;
        margin-right: 0px;
        margin-bottom: 0px;
    }

ul {
    margin-top: 0;
    margin-left: 10;
    margin-bottom: 0;
    padding-left: 0;
}
ul li {
    margin-left: 1em;
}

#header,
#footer {
    position: fixed;  
    left: 0;
    right: 0;
	font-size: 0.9em;
}
#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
#tabla_buscar th{
  background-color: #67BABA;
  color: #000000;
  font-weight: bold;
  font-family:Arial;
}
#tabla_buscar tr:nth-child(odd){
    background-color: #f9f9f9;
    color: #000000;
    font-family:Arial;
}
#tabla_buscar tr:nth-child(even){
    background: #d8e1ef;
    color: #000000;
    font-family:Arial;
}
</style>
</head>
<body>
<div id="header">
    <table cellspacing="0" cellpadding="0" border="0 align="center" class="reporte" >
    <tr height="1%">
        <td width="210px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td>
            <div align="center"><h3>Estado de Cuenta del Ahorrista</h3></div>
        </td>
        <td width="210px">
            <div align="right">Lugar y Fecha de Impresi�n: <br>Ejido, '.date(d).' de '.convertir_mes(date(m)).' de '.redondear(date(Y),0,".","").'&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    </table>
</div>
<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>
<table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
    <tr align="center" class="titulo">
        <td colspan="4" >
            DATOS DEL SOCIO
        </td>
    </tr>
    <tr class="etiquetas" align="center">
        <td>C�DULA</td><td>APELLIDOS Y NOMBRES</td><td>FECHA INGRESO</td><td>ESTATUS</td>
    </tr>
    <tr align="center" class="resultados">
        <td>'.redondear($reg_socio[cedu_soci],0,'.','').'</td><td>'.$reg_socio[apel_soci].' '.$reg_socio[nomb_soci].'</td><td>'.date_format(date_create($reg_status1[fcha_estd]),"d-m-Y").'</td><td>'.$reg_status2[nomb_stat].'</td>
    </tr>
</table>
<br>
<table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
    <tr align="center" class="titulo">
        <td colspan="5" >
            DATOS DE LA �LTIMA N�MINA REGISTRADA
        </td>
    </tr>
    <tr class="etiquetas" align="center">
        <td>DEPENDENCIA</td><td>NOMINA</td><td>APORTE</td><td>RETENCION</td><td>MONTEPIO</td>
    </tr>
    <tr align="center" class="resultados">
        <td>'.$ultimo_dependencia.'</td><td>'.$ultimo_periodo.'&nbsp;de&nbsp;'.$ultimo_mess.'&nbsp;de&nbsp;'.$ultimo_anno.'</td><td>'.$ultimo_aporte.'</td><td>'.$ultimo_retencion.'</td><td>'.$ultimo_montepio.'</td>
    </tr>
</table>
<br>
<table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
    <tr align="center" class="titulo">
        <td colspan="5" >
            DATOS LABORALES ACTUALES
        </td>
    </tr>
    <tr class="etiquetas" align="center">
        <td>DEPENDENCIA</td><td>CARGO</td><td>SUELDO</td><td>% APORTE</td><td>% RETENCI�N</td>
    </tr>
    <tr align="center" class="resultados">
        <td>'.$reg_lab[nomb_depn].'</td><td>'.$reg_lab[carg_dlab].'</td><td>'.redondear($reg_lab[suel_dlab],2,'.',',').' ('.$reg_lab[peri_suel].')</td><td>'.($reg_lab[aprt_dlab]*100).'%</td><td>'.($reg_lab[retn_dlab]*100).'%</td>
    </tr>
</table>
<br>
<table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
    <tr align="center" class="titulo">
        <td colspan="7" >
            DATOS FINANCIEROS
        </td>
    </tr>
    <tr align="center" class="etiquetas">
        <td width="100px" rowspan="2">Ahorros</td>
        <td width="100px" rowspan="2">Aportes</td>
        <td width="100px" rowspan="2">Capital Total</td>
        <td width="100px" rowspan="2">Bloqueado<br>por Pr�stamos</td>
        <td width="100px" rowspan="2">Bloqueado<br>por Retiros</td>
        <td width="200px" colspan="2">Disponible</td>
    </tr>
    <tr align="center" class="etiquetas">
        <td width="100px">Prestamos ('.($PPM*100).'%)</td>
        <td width="100px">Retiro Parcial ('.($PRM * 100).'%)</td>
    </tr>
    <tr align="right" class="resultados">
        <td>'.$print_ahorros.'&nbsp;</td>
        <td>'.$print_aportes.'&nbsp;</td>
        <td>'.$print_capital.'&nbsp;</td>
        <td>'.$print_bloq_p.'&nbsp;</td>
        <td>'.$print_bloq_r.'&nbsp;</td>
        <td>'.$print_dis_prest.'&nbsp;</td>
        <td>'.$print_dis_retir.'&nbsp;</td>
    </tr>
</table>
<br>
<table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center" id="tabla_buscar">
    <tr align="center" class="titulo">
        <th colspan="9" >
            DATOS DE PRESTAMOS OTORGADOS
        </td>
    </tr>
    <tr align="center" class="etiquetas">
        <td width="20px">N�</td><td width="70px">OTORGADO</td><td width="140px">TIPO</td><td width="60px">MONTO</td><td width="50px">PLAZO</td><td width="50px">TASA %</td><td width="50px">CUOTA</td><td width="60px">SALDO</td><td>GARANT�AS</td>
    </tr>';
    $total_desc = 0;
    for ($i=1; $i<$cuenta_p;$i++){
        if (redondear(($prestamos[$i][mont_apro] - $prestamos[$i][pagado]),2,'.',',') > 0){
            $total_desc += $prestamos[$i][mont_cuot];
            $total_desc = redondear($total_desc,2,'','.');
        }        
        $html.= '<tr align="center" class="resultados">
            <td align="right">'.$i.'&nbsp;</td><td>'.date_format(date_create($prestamos[$i][fcha_acta]),"d-m-Y").'</td><td>'.$prestamos[$i][nomb_tipo_pres].'</td><td align="right">'.redondear($prestamos[$i][mont_apro],2,'.',',').'&nbsp;</td><td>'.$prestamos[$i][plaz_prst].' meses</td><td>'.$prestamos[$i][mont_intr].'%</td><td align="right">'.redondear($prestamos[$i][mont_cuot],2,'.',',').'&nbsp;</td><td align="right">'.redondear(($prestamos[$i][mont_apro] - $prestamos[$i][pagado]),2,'.',',').'&nbsp;</td><td align="left">'.$prestamos_fianza[$i].'</td>
        </tr>';
    }
$html .='<tr class="etiquetas"><td colspan="6" align="right">T O T A L&nbsp;&nbsp;</td><td align="right">'.redondear($total_desc,2,'.','.').'&nbsp;</td><td align="right">'.redondear($total_prestamos,2,'.','.').'&nbsp;</td><td>&nbsp;</td></tr>
</table>
<br>
<table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center" id="tabla_buscar" >
    <tr align="center" class="titulo">
        <th colspan="10" >
            DATOS DE PRESTAMOS RESPALDADOS
        </td>
    </tr>
        <tr align="center" class="etiquetas">
        <td width="20px">N�</td><td width="70px">OTORGADO</td><td width="140px">TIPO</td><td width="60px">MONTO</td><td width="50px">PLAZO</td><td width="50px">TASA %</td><td width="50px">CUOTA</td><td width="60px">SALDO</td><td width="60px">GARANT�A</td><td>BENEFICIARIO</td>
    </tr>';
    for ($i=1; $i<$cuenta_f;$i++){
        $html.= '<tr align="center" class="resultados">
            <td align="right">'.$i.'&nbsp;</td><td>'.date_format(date_create($prestamosf[$i][fcha_acta]),"d-m-Y").'</td><td>'.$prestamosf[$i][nomb_tipo_pres].'</td><td align="right">'.redondear($prestamosf[$i][mont_apro],2,'.',',').'&nbsp;</td><td>'.$prestamosf[$i][plaz_prst].' meses</td><td>'.$prestamosf[$i][mont_intr].'%</td><td align="right">'.redondear($prestamosf[$i][mont_cuot],2,'.',',').'&nbsp;</td><td align="right">'.redondear(($prestamosf[$i][mont_apro] - $prestamosf[$i][pagado]),2,'.',',').'&nbsp;</td><td align="right">'.redondear(($prestamosf[$i][mont_fian]),2,'.',',').'&nbsp;</td><td>'.$prestamosf[$i][apel_soci].'&nbsp;'.$prestamosf[$i][nomb_soci].'<br>C.I:&nbsp;'.redondear($prestamosf[$i][cedu_soci],0,'.',''); if ($prestamosf[$i][libre_fian]) { $html .= '<br><font color="#ff0000"><b>'.$prestamosf[$i][libre_fian].'</b></font>'; } $html .= '</td>
        </tr>';
    }
$html .='
    <tr class="etiquetas"><td colspan="8" align="right">T O T A L&nbsp;&nbsp;</td><td align="right">'.redondear($total_fianzas,2,'.','.').'&nbsp;</td><td>&nbsp;</td></tr>
</table>';
$html .= '</body></html>';
//echo $html;
generar_pdf($html,'Edo Cuenta '.$cedula.' '.date('d-m-Y').'.pdf','letter','portrait');
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');?>
