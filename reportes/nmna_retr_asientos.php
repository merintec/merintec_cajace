<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); 
include ('../reportes/generarpdf.php');
$idprint=$_GET['codg_retr']; 
    //////////////// Datos de la nomina
    $sql_nmna = "SELECT * FROM nominas_retroactivos nmna_retr, dependencias dp WHERE codg_retr=".$idprint." AND nmna_retr.codg_depn=dp.codg_depn";    
    $res_nmna = mysql_query($sql_nmna);
    $reg_nmna = mysql_fetch_array($res_nmna);    
    $dependencia = $reg_nmna['nomb_depn'];    
    $codg_pago = $reg_nmna['codg_pago'];
    $fecha_max = $reg_nmna['fcha_retr'];
    $nomina = 'Del '.$reg_nmna['mini_retr'].'/'.$reg_nmna['aini_retr'].' al '.$reg_nmna['mfin_retr'].'/'.$reg_nmna['afin_retr'].'';
    //////////////// Datos del Pago
    $sql_pago = "SELECT * from dependencias_pagos dp, banco_cuentas bc WHERE dp.codg_pago=".$codg_pago." AND dp.codg_cnta=bc.codg_cnta";
    $res_pago = mysql_query($sql_pago);
    $reg_pago = mysql_fetch_array($res_pago);
    $reg_pago["fcha_pago"] = strtotime($reg_pago["fcha_pago"]);
    //////////////// TOTALES de los Datos de los Detalles de la Nomina    
    $sql_dtll = "SELECT nd.codg_retr,";
    $sql_dtll .= " (SELECT SUM(mont_nr_dlle) FROM nominas_retroactivos_detalles WHERE codg_retr=nd.codg_retr AND dest_nr_dlle='Aporte') as aportes,";
    $sql_dtll .= " (SELECT SUM(mont_nr_dlle) FROM nominas_retroactivos_detalles WHERE codg_retr=nd.codg_retr AND dest_nr_dlle='Retencion') as retenciones,"; 
    $sql_dtll .= " (SELECT SUM(mont_nr_dlle) FROM nominas_retroactivos_detalles WHERE codg_retr=nd.codg_retr AND dest_nr_dlle='Montepio') as montepios,";   
    $sql_dtll .= " (SELECT SUM(mont_nr_dlle) FROM nominas_retroactivos_detalles WHERE codg_retr=nd.codg_retr) as total";
    $sql_dtll .= " from nominas_retroactivos_detalles nd WHERE nd.codg_retr=".$idprint." GROUP BY codg_retr";
    $res_dtll = mysql_query($sql_dtll);
    while ($reg_dtll = mysql_fetch_array($res_dtll)){
        $detalles=$reg_dtll;
    }
    /// cargamos el valor de las Cuentas para retenci�n, aportes y montepio
    $sql_val="select * from valores WHERE des_val='CNT_AH' OR des_val='CNT_AP' OR des_val='CNT_MP".$reg_nmna['mess_nmna']."'";
    $res_val = mysql_query($sql_val);
    while ($row_val = mysql_fetch_array($res_val))
    {
        // el nombre de la variable es $CNT_AH y $CNT_AP
        $$row_val['des_val'] = $row_val['val_val'];
    }
    // buscar la cuenta Ahorros
    $sql_part = "SELECT * FROM cuentas c, plan_cuentas p WHERE c.codg_pcnta=".$CNT_AH." AND c.codg_pcuen=p.codg_pcuen";
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $res_cnta["ahorros"]["codg_pcnta"] = $res_part["codg_pcnta"];
    $res_cnta["ahorros"]["nmro_cnta"] = $res_part["nmro_cnta"];
    $res_cnta["ahorros"]["nomb_cnta"] = $res_part["nomb_cnta"];
    if ($res_part["stat_pcuen"]!='Activo'){
        $msg .= "Aportes, ";
    }
    // buscar la cuenta Aporte
    $sql_part = "SELECT * FROM cuentas c, plan_cuentas p WHERE c.codg_pcnta=".$CNT_AP." AND c.codg_pcuen=p.codg_pcuen";
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $res_cnta["aporte"]["codg_pcnta"] = $res_part["codg_pcnta"];
    $res_cnta["aporte"]["nmro_cnta"] = $res_part["nmro_cnta"];
    $res_cnta["aporte"]["nomb_cnta"] = $res_part["nomb_cnta"];
    if ($res_part["stat_pcuen"]!='Activo'){
        $msg .= "Retenciones, ";
    }

    // buscar las cuentas los MONTEPIOS de los meses correspondientes
    $fechainicial = $reg_nmna['aini_retr']."-".$reg_nmna['mini_retr']."-01";
    $fechainicial = strtotime($fechainicial);
    $fechafinal = $reg_nmna['afin_retr']."-".$reg_nmna['mfin_retr']."-01";
    $fechafinal = strtotime($fechafinal);
    $nuevafecha = $fechainicial;
    $meses = 0;
    do {
        $sql_val="select * from valores WHERE des_val='CNT_MP".date('n',$nuevafecha)."'";
        $res_val = mysql_query($sql_val);
        while ($row_val = mysql_fetch_array($res_val))
        {
            // el nombre de la variable es $CNT_AH y $CNT_AP
            $$row_val['des_val'] = $row_val['val_val'];
        }
        $var_aux = "CNT_MP".date('n',$nuevafecha);
        $sql_part = "SELECT * FROM cuentas c, plan_cuentas p WHERE c.codg_pcnta=".$$var_aux." AND c.codg_pcuen=p.codg_pcuen";
        $res_part = mysql_fetch_array(mysql_query($sql_part));
        $res_cnta["montepio"][date('n',$nuevafecha)]["codg_pcnta"] = $res_part["codg_pcnta"];
        $res_cnta["montepio"][date('n',$nuevafecha)]["nmro_cnta"] = $res_part["nmro_cnta"];
        $res_cnta["montepio"][date('n',$nuevafecha)]["nomb_cnta"] = $res_part["nomb_cnta"];
        if ($res_part["stat_pcuen"]!='Activo'){
            $msg .= "Montepio ".convertir_mes(date('n',$nuevafecha)).", ";
        }
        $nuevafecha = strtotime ( '+1 month' , $nuevafecha ) ;
        $meses = $meses + 1;
    } while ($nuevafecha <= $fechafinal);
    //// datos del pago
     $sql_part = "SELECT * FROM dependencias_pagos dpago, banco_cuentas bcnta, cuentas WHERE dpago.codg_pago =".$codg_pago." AND dpago.codg_cnta=bcnta.codg_cnta AND bcnta.codg_pcnta=cuentas.codg_pcnta";
    $res_part = mysql_fetch_array(mysql_query($sql_part));

    ////// Preparando los datos de las cuentas contables para mostrar
    $acum_cuentas = 1;
    $detalle_cuentas[$acum_cuentas]["id"] = $res_part['codg_pcnta'];
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_part['nmro_cnta'];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_part['nomb_cnta'];
    $detalle_cuentas[$acum_cuentas]["debe"] = $detalles["total"];
    $acum_cuentas += 1;
    $detalle_cuentas[$acum_cuentas]["id"] = $res_cnta["ahorros"]["codg_pcnta"];
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["ahorros"]["nmro_cnta"];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["ahorros"]["nomb_cnta"];
    $detalle_cuentas[$acum_cuentas]["haber"] = $detalles["retenciones"];
    $total_acum = $detalles["retenciones"];
    $acum_cuentas += 1;
    $detalle_cuentas[$acum_cuentas]["id"] = $res_cnta["aporte"]["codg_pcnta"];
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["aporte"]["nmro_cnta"];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["aporte"]["nomb_cnta"];
    $detalle_cuentas[$acum_cuentas]["haber"] = $detalles["aportes"];
    $total_acum += $detalles["aportes"];
    $acum_cuentas += 1;
    ///// asignacion de montos de los montepios
    $monto_mes_montepio = redondear($detalles["montepios"]/$meses,2,'','.');
    $fechainicial = $reg_nmna['aini_retr']."-".$reg_nmna['mini_retr']."-01";
    $fechainicial = strtotime($fechainicial);
    $fechafinal = $reg_nmna['afin_retr']."-".$reg_nmna['mfin_retr']."-01";
    $fechafinal = strtotime($fechafinal);
    $nuevafecha = $fechainicial;
    $meses = 0;
    do {
        $var_aux = "CNT_MP".date('n',$nuevafecha);
        $detalle_cuentas[$acum_cuentas]["id"] = $res_cnta["montepio"][date('n',$nuevafecha)]["codg_pcnta"];
        $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["montepio"][date('n',$nuevafecha)]["nmro_cnta"];
        $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["montepio"][date('n',$nuevafecha)]["nomb_cnta"];
        $detalle_cuentas[$acum_cuentas]["haber"] = $monto_mes_montepio;
        $nuevafecha = strtotime ( '+1 month' , $nuevafecha ) ;
        $total_acum += $monto_mes_montepio;
        $meses = $meses + 1;
        $acum_cuentas += 1;
    } while ($nuevafecha <= $fechafinal);
    
    if ($total_acum<$detalles["total"]){
        $diferencia = redondear($detalles["total"]-$total_acum,2,'','.');
        $detalle_cuentas[$acum_cuentas-1]["haber"]+=$diferencia;
    }
    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'. 
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
$html='<html>
<head>
    <base target="_blank">
    <title>Reportes de Nomina - Asientos Contables</title>
<style type="text/css">
    .reporte
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .reporte a
    {
        font-weight: bold;
        color: #0000FF;
    }
    .titulo {
        font-family: arial; 
        font-size: 13pt; 
        font-weight: bold; 
        color: #000000; 
        background-color: #67BABA; 
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 12px;
        font-weight: bold;
    }    
    .tablanomina {
        font-family: Arial; 
        font-size: 9px; 
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 120px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
#header,
#footer {
    position: fixed;  
    left: 0;
    right: 0;
	font-size: 0.9em;
}
#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
</style>
</head>
<body>
<div id="header">
    <table cellspacing="0" cellpadding="0" border="0 align="center" class="reporte" >
    <tr height="1%">
        <td width="210px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td>
            <div align="center"><h3>'.$dependencia.'<BR>ASIENTOS CONTABLES DE NOMINA<BR>('.$nomina.')</h2></div>
        </td>
        <td width="210px">
            <div align="right">Lugar y Fecha de Impresi�n: <br>Ejido, '.date(d).' de '.convertir_mes(date(m)).' de '.redondear(date(Y),0,".","").'&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    </table>
</div>
<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Datos del Pago</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas">Banco Origen</td>
                    <td class="etiquetas">Banco / Cuenta Destino</td>
                    <td class="etiquetas">Referencia</td>
                    <td class="etiquetas">Fecha</td>
                </tr>
                <tr align="center">
                    <td>'.$reg_pago["banc_orig"].'</td>
                    <td>'.$reg_pago["bnco_cnta"].': '.$reg_pago["nmro_cnta"].'</td>
                    <td>'.$reg_pago["numr_refe"].'</td>
                    <td>'.date("d-m-Y",$reg_pago["fcha_pago"]).'</td>
                </tr>
            </table>';
              $html.='<br><table width="90%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo"><td colspan="4">Datos de las Cuentas</td></tr>
               <tr class="etiquetas" align="center"><td width="100px">C�DIGO</td><td>CONCEPTO</td><td width="100px">DEBE</td><td width="100px">HABER</td></tr>
                 '; 
                 for ($k=1;$k<$acum_cuentas;$k++){
                     if ($detalle_cuentas[$k]["debe"]>0 || $detalle_cuentas[$k]["haber"]>0) {
                         $html.='<tr><td>&nbsp;'.$detalle_cuentas[$k]["codigo"].'</td><td>&nbsp;'.$detalle_cuentas[$k]["concepto"].'</td><td align="right">'; if ($detalle_cuentas[$k]["debe"]>0) { $html.=redondear($detalle_cuentas[$k]["debe"],2,".",","); } $html.='&nbsp;</td><td align="right">'; if ($detalle_cuentas[$k]["haber"]>0) { $html.= redondear($detalle_cuentas[$k]["haber"],2,".",","); } $html.='&nbsp;</td></tr>';
                         $suma_debe += $detalle_cuentas[$k]["debe"];
                         $suma_haber += $detalle_cuentas[$k]["haber"];
                     }  
                 }
                 $html.='<tr class="etiquetas"><td colspan="2" align="right">TOTALES (SUMAS IGUALES)&nbsp;</td><td align="right">'.redondear($suma_debe,2,".",",").'&nbsp;</td><td align="right">'.redondear($suma_haber,2,".",",").'&nbsp;</td></tr>';
                 $html.='</table>';
$html .= '</body></html>';
//echo $html;
if (!$msg) {   
    if ($_GET['status']=='ver'){
       generar_pdf($html,'Aportes_y_Ahorros_Montepio en Retroactivo.pdf','letter','portrait');
    }
    if ($_GET['status']=='generar'){
        $fech_asien = $fecha_max;
        $desc_asien = 'Retroactivo '.$dependencia.' ('.$nomina.')';
        $stat_asien = 'Bloqueado';
        $orgn_asien = 'Retroactivo';
        $codg_rela = $idprint;
        $sql_insert = "INSERT INTO asientos (fech_asien, desc_asien, stat_asien, orgn_asien, codg_rela) VALUES ('".$fech_asien."', '".$desc_asien."', '".$stat_asien."', '".$orgn_asien."', '".$codg_rela."');";
        mysql_query($sql_insert);
        $id = mysql_insert_id();
        $sql_insert = "INSERT INTO movimientos_contables (codg_asien, codg_pcnta, nmro_pcnta, nomb_pcnta, debe_movi, haber_movi) VALUES ";
        for ($k=1;$k<$acum_cuentas;$k++){
            if ($detalle_cuentas[$k]["debe"]>0 || $detalle_cuentas[$k]["haber"]>0){
                $sql_insert2 .= "('".$id."', '".$detalle_cuentas[$k]["id"]."', '".$detalle_cuentas[$k]["codigo"]."', '".$detalle_cuentas[$k]["concepto"]."', '".$detalle_cuentas[$k]["debe"]."', '".$detalle_cuentas[$k]["haber"]."'),";
            }
        }
        $sql_insert2 .= ',';
        $sql_insert2 = str_replace(',,', ';', $sql_insert2);
        $sql_insert = $sql_insert.' '.$sql_insert2;
        mysql_query($sql_insert);
        //echo  '<SCRIPT LANGUAGE="javascript">location.href = "../reportes/reporte_asientos.php?asiento2='.$id.'";</SCRIPT>';
    }
    if ($_GET['status']=='eliminar' && $_GET['asiento']){
        $sql_eliminar = "DELETE FROM movimientos_contables WHERE codg_asien=".$_GET['asiento'].";";
        $sql_eliminar = "DELETE FROM asientos WHERE codg_asien=".$_GET['asiento'].";";
        mysql_query($sql_eliminar);
    }
}   
else {
    $msg = "Las cuentas ".$msg." deben ser actuallizadas";
    echo '<script>alert("'.$msg.'")</script>';
    if ($_GET['status']=='ver') {
        echo '<script>window.close();</script>';   
    }
}
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');?>