<?php
    include('../dompdf/dompdf_config.inc.php');
    function generar_pdf($contenido,$nom_pdf_out,$papel,$orientacion) 
    {
        $dompdf =  new DOMPDF();
        $dompdf -> set_paper($papel,$orientacion);
        $dompdf -> load_html($contenido);
        $dompdf -> render();
        $dompdf->stream($nom_pdf_out); 
    }
    //generar_pdf('texto','texto.pdf','letter','portrait');
?>
