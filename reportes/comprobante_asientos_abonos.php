<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); 
include ('../reportes/generarpdf.php');
$idprint=$_GET['codg_pago']; 
    //////////////// Datos de la nomina
	// consultar aca los datos del abono y generar al menos dos asientos uno por interes, uno por capital y uno por exceso si existe
    $acum_cuentas = 1;
	
	$sql_pago = "SELECT * FROM abonos a, prestamos p, tipo_prestamos tp, socios s WHERE a.codg_pago=".$idprint." AND p.codg_prst=a.codg_prst AND tp.codg_tipo_pres=p.codg_tipo_pres AND p.cedu_soci=s.cedu_soci";
	$res_pago = mysql_query($sql_pago);
    if ($reg_pago = mysql_fetch_array($res_pago)){
		$codg_tipo_pres=$reg_pago["codg_tipo_pres"];
		$reg_tpre = buscar_campo('*', 'tipo_prestamos', 'WHERE codg_tipo_pres="'.$codg_tipo_pres.'"' );	
		$ccue_ban = buscar_campo('codg_pcnta', 'banco_cuentas', 'WHERE codg_cnta="'.$reg_pago["codg_cnta"].'"' );	
        $fech_asien = $reg_pago["fcha_pago"];
        $desc_asien = 'Comprobante de Abono a prestamo de '.$reg_pago["nomb_soci"].' '.$reg_pago["apel_soci"];
        $stat_asien = 'Bloqueado';
        $orgn_asien = 'Comprobante Abono';
        $codg_rela = $idprint;
    } 

$error_asiento=false;
if ($_GET['status']=='generar'){
	mysql_query('AUTOCOMMIT=0');
	mysql_query('BEGIN');
    $sql_insert = "INSERT INTO asientos (fech_asien, desc_asien, stat_asien, orgn_asien, codg_rela, codg2_rela) VALUES ('".$fech_asien."', '".$desc_asien."', '".$stat_asien."', '".$orgn_asien."', '".$codg_rela."', 0);";
    mysql_query($sql_insert);
    $id = mysql_insert_id();
	if($cue_ban = buscar_campo('*', 'cuentas, plan_cuentas pc', 'WHERE codg_pcnta="'.$ccue_ban['codg_pcnta'].'" AND c.codg_pcuen = pc.codg_pcuen AND pc.stat_pcuen = "Activo"' )){
		$sql_insert = "INSERT INTO movimientos_contables(codg_asien, codg_pcnta, nmro_pcnta, nomb_pcnta, debe_movi, haber_movi) VALUES ('".$id."', '".$cue_ban['codg_pcnta']."', '".$cue_ban['nmro_cnta']."', '".$cue_ban['nomb_cnta']."', ".$reg_pago['mont_pago'].", 0.00)";
    	mysql_query($sql_insert);
	}else{
		$error_asiento=true;
	}

	if($cue_int = buscar_campo('*', 'cuentas, plan_cuentas pc', 'WHERE codg_pcnta="'.$reg_tpre['codg_pcnta_int'].'" AND c.codg_pcuen = pc.codg_pcuen AND pc.stat_pcuen = "Activo"' )){
		$sql_insert = "INSERT INTO movimientos_contables(codg_asien, codg_pcnta, nmro_pcnta, nomb_pcnta, debe_movi, haber_movi) VALUES ('".$id."', '".$cue_int['codg_pcnta']."', '".$cue_int['nmro_cnta']."', '".$cue_int['nomb_cnta']."', 0.00, ".$reg_pago['inte_pago'].")";
    	mysql_query($sql_insert);
	}else{
		$error_asiento=true;
	}
	if($cue_pre = buscar_campo('*', 'cuentas, plan_cuentas pc', 'WHERE codg_pcnta="'.$reg_tpre['codg_pcnta'].'" AND c.codg_pcuen = pc.codg_pcuen AND pc.stat_pcuen = "Activo"' )){
		$sql_insert = "INSERT INTO movimientos_contables(codg_asien, codg_pcnta, nmro_pcnta, nomb_pcnta, debe_movi, haber_movi) VALUES ('".$id."', '".$cue_pre['codg_pcnta']."', '".$cue_pre['nmro_cnta']."', '".$cue_pre['nomb_cnta']."', 0.00, ".$reg_pago['capi_pago'].")";
   		mysql_query($sql_insert);
	}else{
		$error_asiento=true;
	}
	if($reg_cuemov = buscar_campo('*', 'cuentas_movimientos', 'WHERE conc_cmov="Excedente por abono" AND codg_dlle="'.$idprint.'"' )){
		if($cue_exc = buscar_campo('*', 'cuentas, plan_cuentas pc', 'WHERE codg_pcnta="'.$reg_cuemov['codg_pcnta'].'" AND c.codg_pcuen = pc.codg_pcuen AND pc.stat_pcuen = "Activo"' )){
			$sql_insert = "INSERT INTO movimientos_contables(codg_asien, codg_pcnta, nmro_pcnta, nomb_pcnta, debe_movi, haber_movi) VALUES ('".$id."', '".$cue_exc['codg_pcnta']."', '".$cue_exc['nmro_cnta']."', '".$cue_exc['nomb_cnta']."', 0.00, ".$reg_cuemov['mont_cmov'].")";
			mysql_query($sql_insert);
		}else{
			$error_asiento=true;
		}	
	}
	if($reg_socmov = buscar_campo('*', 'socios_movimientos', 'WHERE conc_movi="Excedente por abono" AND codg_dlle="'.$idprint.'"' )){
		$ccue_exc = buscar_campo('val_val', 'valores', 'WHERE des_val="CNT_AH"' );
		if($cue_exc = buscar_campo('*', 'cuentas, plan_cuentas pc', 'WHERE codg_pcnta="'.intval($ccue_exc['val_val']).'" AND c.codg_pcuen = pc.codg_pcuen AND pc.stat_pcuen = "Activo"' )){
			$sql_insert = "INSERT INTO movimientos_contables(codg_asien, codg_pcnta, nmro_pcnta, nomb_pcnta, debe_movi, haber_movi) VALUES ('".$id."', '".$cue_exc['codg_pcnta']."', '".$cue_exc['nmro_cnta']."', '".$cue_exc['nomb_cnta']."', 0.00, ".$reg_socmov['mont_movi'].")";
			mysql_query($sql_insert);
		}else{
			$error_asiento=true;
		}		
	}
	if($error_asiento==true){ 
		mysql_query('ROLLBACK'); 
		echo '<script>alert("Debe actualizar el plan de cuentas");</script>';		
	}else{ 
		mysql_query('COMMIT'); 
	}
}
	/*
    echo  '<SCRIPT LANGUAGE="javascript">location.href = "../reportes/reporte_asientos.php?asiento2='.$id.'";</SCRIPT>';
	*/
if ($_GET['status']=='eliminar' && $_GET['asiento']){
    $sql_eliminar = "DELETE FROM movimientos_contables WHERE codg_asien=".$_GET['asiento'].";";
    mysql_query($sql_eliminar);
    echo mysql_error();
    $sql_eliminar = " DELETE FROM asientos WHERE codg_asien=".$_GET['asiento'].";";
    mysql_query($sql_eliminar);
    echo mysql_error();
}
/////////////// volver a mostrar botones
$sql_asientos = "SELECT * FROM asientos WHERE orgn_asien = 'Comprobante Abono' AND codg_rela = ".$idprint;
$bus_asientos = mysql_query($sql_asientos);
if ($res_asientos = mysql_fetch_array($bus_asientos)){                              
    echo '<input type="hidden" id="asiento" name="asiento" value="'.$res_asientos[codg_asien].'">';
    $parametros = "'eliminar','".$idprint."','".$res_asientos[codg_asien]."'";
    echo '<input type="button" name="v_reporte" id="v_reporte" value="Eliminar Asiento('.$res_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
    $parametros = "'imprimir','".$idprint."','".$res_asientos[codg_asien]."'";
    echo '<input type="button" name="v_reporte" id="v_reporte" value="Imprimir Asiento('.$res_asientos[codg_asien].')" onclick="asientos('.$parametros.')">';
}else{
    $parametros = "'generar','".$idprint."','0'";
    echo '<input type="button" name="v_reporte" id="v_reporte" value="Generar Asiento" onclick="asientos('.$parametros.')">';
}

/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');
?>
<script>
	actualizar_capa('botonera');
</script>
