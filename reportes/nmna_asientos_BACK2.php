<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php');
include ('../reportes/generarpdf.php');
$idprint=$_GET['codg_nmna'];
$idprint2=$_GET['codg_pago'];
    //////////////// Datos de la nomina
    $sql_nmna = "SELECT * FROM nominas nmna, dependencias dp WHERE codg_nmna=".$idprint." AND nmna.codg_depn=dp.codg_depn";
    $res_nmna = mysql_query($sql_nmna);
    $reg_nmna = mysql_fetch_array($res_nmna);
    $dependencia = $reg_nmna['nomb_depn'];
    if ($reg_nmna['prdo_nmna']<=5){ $nomina = $reg_nmna['prdo_nmna'].'� Semana '; }
    if ($reg_nmna['prdo_nmna']>5 && $reg_nmna['prdo_nmna']<8){ $nomina = ($reg_nmna['prdo_nmna']-5).'� Quincena '; }
    if ($reg_nmna['prdo_nmna']==8){ $nomina = 'Mes '; }
    $nomina .= "de ".convertir_mes($reg_nmna['mess_nmna'])." del ".$reg_nmna['anno_nmna'];
    //////////////// Datos del Pago
    $sql_pago = "SELECT * from dependencias_pagos dp, banco_cuentas bc WHERE dp.codg_pago=".$idprint2." AND dp.codg_cnta=bc.codg_cnta";
    $res_pago = mysql_query($sql_pago);
    $reg_pago = mysql_fetch_array($res_pago);
    $fecha_filtro = $reg_pago["fcha_pago"];
    $reg_pago["fcha_pago"] = strtotime($reg_pago["fcha_pago"]);
    //////////////// Datos de los Detalles de la Nomina
    $cuenta_datos = 0;
    $sql_dtll = "SELECT sc.cedu_soci, CONCAT(apel_soci, ' ', nomb_soci) as nomb_soci, COUNT(rela_dlle) as cant_pres";
    $sql_dtll .= " from nominas_detalle nd, socios sc WHERE nd.codg_nmna=".$idprint." AND nd.codg_pago=".$idprint2." AND nd.cedu_soci=sc.cedu_soci AND nd.moti_dlle='Pr�stamo' GROUP BY nd.cedu_soci";
    $res_dtll = mysql_query($sql_dtll);
    $totales_general = array();
    $totales_partidas = array();
    while ($reg_dtll = mysql_fetch_array($res_dtll)){
        $cuenta_datos += 1;
        $detalles[$cuenta_datos]=$reg_dtll;
        $sql_prestamos="SELECT nd.*, prt.*, (SELECT COUNT(codg_dlle) FROM nominas_detalle WHERE rela_dlle=nd.rela_dlle AND moti_dlle='Pr�stamo' AND nominas_detalle.codg_nmna<=nd.codg_nmna GROUP BY rela_dlle) as pagadas, (SELECT SUM(numr_cuot) FROM abonos WHERE codg_prst=prt.codg_prst AND fcha_pago<'".$fecha_filtro."') AS abonadas from nominas_detalle nd, prestamos prt WHERE moti_dlle='Pr�stamo' AND nd.cedu_soci=".$detalles[$cuenta_datos]['cedu_soci']." AND nd.codg_nmna=".$idprint." AND nd.codg_pago=".$idprint2." AND nd.rela_dlle=prt.codg_prst";
        $res_prestamos=mysql_query($sql_prestamos);
        $cuenta_prestamo=0;
        while ($reg_prestamos=mysql_fetch_array($res_prestamos)){
          $cuenta_prestamo += 1;
          $detalles_prestamos[$cuenta_datos][$cuenta_prestamo] = $reg_prestamos;
          ////// C�lculo de los montos
	    $mont_prst = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["mont_prst"];
	    $tipo_prst = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["tipo_prst"];
	    $plaz_prst = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["plaz_prst"];
	    $mont_apro = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["mont_apro"];
	    $tipo_cuot = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["tipo_cuot"];
	    $mont_cuot = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["mont_cuot"];
	    $mont_intr = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["mont_intr"];
	    $sald_real = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["sald_inic_real"];
 	    $cuot_real = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["mont_cuot_real"];
 	    $codg_tipo_pres = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["codg_tipo_pres"];
            $pagadas = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["pagadas"];
            $abonadas = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["abonadas"];
            $pagadas = $pagadas + $abonadas;	    
            $monto_mensual=0;
	    $monto_cuota=0;
	    $deuda = $mont_apro;
	    $interes_calculo=redondear(($mont_intr/12),2,"",".");
	    if ($tipo_cuot=='Q'){ 
	      $cuenta_for = $plaz_prst * 2;
	      $interes = ($interes_calculo/100)/2;
	    } 
            if ($tipo_cuot=='S'){
	      $cuenta_for = $plaz_prst * 4;
	      $interes = ($interes_calculo/100)/4;
	    }
	    if ($tipo_cuot=='M'){
	      $cuenta_for = $plaz_prst;
	      $interes = ($interes_calculo/100);
	    }
	    $monto_mensual = $mont_apro*(($interes*pow(1+$interes,$cuenta_for))/(pow(1+$interes,$cuenta_for)-1));
	    $monto_mensual = redondear($monto_mensual,2,"",".");
            $monto_cuota = $monto_mensual;
            if ($sald_real!='0.00'){
	        $deuda = $sald_real;
	        $monto_cuota = $cuot_real;
	    }
            for ($i=0;$i<$pagadas;$i++){
                /// monto Interes
                $monto_cuota_interes=$deuda*$interes;
                $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["interes"]=$monto_cuota_interes;
                /// monto capital
                $monto_cuota_capital=$monto_cuota-$monto_cuota_interes;
                $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["capital"]=$monto_cuota_capital;
                $capital_acum=redondear(($capital_acum+$monto_cuota_capital),2,"",".");
                $interes_acum=redondear(($interes_acum+$monto_cuota_interes),2,"",".");
                $total_acum=redondear(($interes_acum+$capital_acum),2,"",".");
                $deuda_print=redondear($deuda,2,".",",");
                $monto_cuota_interes_print=redondear($monto_cuota_interes,2,".",",");
                $monto_cuota_capital_print=redondear($monto_cuota_capital,2,".",",");
                /// monto capital + Interes
                $monto_cuota_print=redondear($monto_cuota,2,".",",");
                $capital_acum_print=redondear($capital_acum,2,".",",");
                $interes_acum_print=redondear($interes_acum,2,".",",");
                $total_acum_print=redondear($total_acum,2,".",",");
                $deuda = $deuda -$monto_cuota_capital;
	    }
	    $totales_general['capital']+=$monto_cuota_capital;
	    $totales_general['intereses']+=$monto_cuota_interes;
	    $totales_general['total']+=$monto_cuota_capital+$monto_cuota_interes;
	    //// totales por partida
	    $id_prst_part = $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["codg_tipo_pres"];
	    $totales_partidas[$id_prst_part]['capital']+=redondear($monto_cuota_capital,2,'','.');
	    $totales_partidas[$id_prst_part]['intereses']+=redondear($monto_cuota_interes,2,'','.');
	    $totales_partidas[$id_prst_part]['total']+= redondear($monto_cuota_capital,2,'','.') + redondear($monto_cuota_interes,2,'','.');
	    $totales_total += $totales_partidas[$id_prst_part]['total'];
        }
    }
    //////////////// Preparamos los datos de la partidas
    $sql_part_print = "SELECT * FROM tipo_prestamos";
    $bus_part_print = mysql_query($sql_part_print);
    $acum_cuentas = 1;
    //// datos del pago
    $sql_part = "SELECT * FROM dependencias_pagos dpago, banco_cuentas bcnta, cuentas WHERE dpago.codg_pago =".$idprint2." AND dpago.codg_cnta=bcnta.codg_cnta AND bcnta.codg_pcnta=cuentas.codg_pcnta";
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_part['nmro_cnta'];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_part['nomb_cnta'];
    $detalle_cuentas[$acum_cuentas]["debe"] = $totales_total;
    $acum_cuentas += 1;      
    while ($res_part_print = mysql_fetch_array($bus_part_print)){
        $totales_partidas[$res_part_print['codg_tipo_pres']]['capital'];
        $totales_partidas[$res_part_print['codg_tipo_pres']]['intereses'];
        $totales_partidas[$res_part_print['codg_tipo_pres']]['total'];
        // si el tipo de prestamo tiene algun monto en esta nomina 
        if ($totales_partidas[$res_part_print['codg_tipo_pres']]['capital']){
          // consultamos los datos de la cuenta para capital
          $sql_part = "Select * FROM cuentas WHERE codg_pcnta=".$res_part_print['codg_pcnta'];
          $res_part = mysql_fetch_array(mysql_query($sql_part));
          $detalle_cuentas[$acum_cuentas]["codigo"] = $res_part['nmro_cnta'];
          $detalle_cuentas[$acum_cuentas]["concepto"] = $res_part['nomb_cnta'];
          $detalle_cuentas[$acum_cuentas]["haber"] = $totales_partidas[$res_part_print['codg_tipo_pres']]['capital'];
          $acum_cuentas += 1;
          $sql_part = "Select * FROM cuentas WHERE codg_pcnta=".$res_part_print['codg_pcnta_int'];
          $res_part = mysql_fetch_array(mysql_query($sql_part));
          $detalle_cuentas[$acum_cuentas]["codigo"] = $res_part['nmro_cnta'];
          $detalle_cuentas[$acum_cuentas]["concepto"] = $res_part['nomb_cnta'];
          $detalle_cuentas[$acum_cuentas]["haber"] = $totales_partidas[$res_part_print['codg_tipo_pres']]['intereses'];
          $acum_cuentas += 1;
        }
    }  
    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'.
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
$html='<html>
<head>
    <base target="_blank">
    <title>Reportes de Nomina - Pr�stamos</title>
<style type="text/css">
    .reporte
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .reporte a
    {
        font-weight: bold;
        color: #0000FF;
    }
    .titulo {
        font-family: arial;
        font-size: 13pt; 
        font-weight: bold;
        color: #000000;
        background-color: #67BABA;
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 12px;
        font-weight: bold;
    }    
    .tablanomina {
        font-family: Arial;
        font-size: 9px;
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 120px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
#header,
#footer {
    position: fixed;
    left: 0;
    right: 0;
	font-size: 0.9em;
}
#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
</style>
</head>
<body>
<div id="header">
    <table cellspacing="0" cellpadding="0" border="0 align="center" class="reporte" >
    <tr height="1%">
        <td width="210px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td>
            <div align="center"><h3>'.$dependencia.'<BR>PR�STAMOS EN N�MINA<BR>('.$nomina.')</h2></div>
        </td>
        <td width="210px">
            <div align="right">Lugar y Fecha de Impresi�n: <br>Ejido, '.date(d).' de '.convertir_mes(date(m)).' de '.redondear(date(Y),0,".","").'&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    </table>
</div>
<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Datos del Pago</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas">Banco Origen</td>
                    <td class="etiquetas">Banco / Cuenta Destino</td>
                    <td class="etiquetas">Referencia</td>
                    <td class="etiquetas">Fecha</td>
                </tr>
                <tr align="center">
                    <td>'.$reg_pago["banc_orig"].'</td>
                    <td>'.$reg_pago["bnco_cnta"].': '.$reg_pago["nmro_cnta"].'</td>
                    <td>'.$reg_pago["numr_refe"].'</td>
                    <td>'.date("d-m-Y",$reg_pago["fcha_pago"]).'</td>
                </tr>
            </table>
            <br>
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">
                    <td colspan="6">Montos Acreditados</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas" width="30px">N�</td>
                    <td class="etiquetas" width="100px">C�dula</td>
                    <td class="etiquetas">Apellidos y Nombres</td>
                    <td class="etiquetas" width="80px">Capital</td>
                    <td class="etiquetas" width="80px">Inter�s</td>
                    <td class="etiquetas" width="80px">Total</td>
                </tr>';
                for ($i=1;$i<=$cuenta_datos;$i++){
                        $html .= '<tr>
                            <td align="right" rowspan="'.$detalles[$i]["cant_pres"].'">'.$i.'&nbsp;</td>
                            <td align="right" rowspan="'.$detalles[$i]["cant_pres"].'">'.Redondear($detalles[$i]["cedu_soci"],0,".","").'&nbsp;</td>
                            <td rowspan="'.$detalles[$i]["cant_pres"].'">&nbsp;'.$detalles[$i]["nomb_soci"].'</td>';
                            for ($j=1;$j<=$detalles[$i]["cant_pres"];$j++){
                              $html.='
                              <td align="right">'.Redondear($detalles_prestamos[$i][$j]["capital"],2,".",",").'&nbsp;</td>
                              <td align="right">'.Redondear($detalles_prestamos[$i][$j]["interes"],2,".",",").'&nbsp;</td>
                              <td align="right">'.Redondear($detalles_prestamos[$i][$j]['mnto_dlle'],2,".",",").'&nbsp;</td>
                              </tr>';
                              if ($j<$detalles[$i]["cant_pres"]){
                                $html.='<tr>';
                              }
                            }
                }
$html .= '<tr align="center" class="titulo">
                    <td colspan="3" align="right">T O T A L E S&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["capital"],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["intereses"],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["total"],2,'.',',').'&nbsp;</td>
                </tr>
             </table>
             <br>
             <table width="90%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo"><td colspan="4">Datos de las Cuentas</td></tr>
               <tr class="etiquetas" align="center"><td width="100px">C�DIGO</td><td>CONCEPTO</td><td width="100px">DEBE</td><td width="100px">HABER</td></tr>
                 '; 
                 for ($k=1;$k<$acum_cuentas;$k++){
                     $html.='<tr><td align="center">'.$detalle_cuentas[$k]["codigo"].'</td><td>&nbsp;'.$detalle_cuentas[$k]["concepto"].'</td><td align="right">'.redondear($detalle_cuentas[$k]["debe"],2,".",",").'&nbsp;</td><td align="right">'.redondear($detalle_cuentas[$k]["haber"],2,".",",").'&nbsp;</td></tr>';
                     $suma_debe += $detalle_cuentas[$k]["debe"];
                     $suma_haber += $detalle_cuentas[$k]["haber"];
                 }
                 $html.='<tr class="etiquetas"><td colspan="2" align="right">TOTALES (SUMAS IGUALES)&nbsp;</td><td align="right">'.redondear($suma_debe,2,".",",").'&nbsp;</td><td align="right">'.redondear($suma_haber,2,".",",").'&nbsp;</td></tr>';
                 $html.='</table>';
$html .= '</body></html>';
echo $html;
//generar_pdf($html,'prestamos.pdf','letter','portrait');
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');?>
