<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php');
include ('../reportes/generarpdf.php');
$idprint=$_GET['codg_nmna'];
$idprint2=$_GET['codg_pago'];
    //////////////// Datos de la nomina
    $sql_nmna = "SELECT * FROM nominas nmna, dependencias dp WHERE codg_nmna=".$idprint." AND nmna.codg_depn=dp.codg_depn";
    $res_nmna = mysql_query($sql_nmna);
    $reg_nmna = mysql_fetch_array($res_nmna);
    $dependencia = $reg_nmna['nomb_depn'];
    if ($reg_nmna['prdo_nmna']>=11){ 
        $nomina = ($reg_nmna['prdo_nmna']-10).'� Semana '; 
        $semana = semana($reg_nmna["anno_nmna"],($reg_nmna["prdo_nmna"]-10));
        $fecha_max = $semana['ultimo_dia'];
    }
    if ($reg_nmna['prdo_nmna']==6){ 
        $nomina = ($reg_nmna['prdo_nmna']-5).'� Quincena '; 
        $fecha_max = $reg_nmna["anno_nmna"].'-'.$reg_nmna["mess_nmna"].'-15';
    }
    if ($reg_nmna['prdo_nmna']==7){ 
        $nomina = ($reg_nmna['prdo_nmna']-5).'� Quincena '; 
        $fecha_max = $reg_nmna["anno_nmna"].'-'.$reg_nmna["mess_nmna"].'-'.dias_mes ($reg_nmna["mess_nmna"],$reg_nmna["anno_nmna"]);
    }
    if ($reg_nmna['prdo_nmna']==8){ 
        $nomina = 'Mes '; 
        $fecha_max = $reg_nmna["anno_nmna"].'-'.$reg_nmna["mess_nmna"].'-'.dias_mes ($reg_nmna["mess_nmna"],$reg_nmna["anno_nmna"]);
    }
    $nomina .= "de ".convertir_mes($reg_nmna['mess_nmna'])." del ".$reg_nmna['anno_nmna'];
    //////////////// Datos del Pago
    $sql_pago = "SELECT * from dependencias_pagos dp, banco_cuentas bc WHERE dp.codg_pago=".$idprint2." AND dp.codg_cnta=bc.codg_cnta";
    $res_pago = mysql_query($sql_pago);
    $reg_pago = mysql_fetch_array($res_pago);
    $fecha_filtro = $reg_pago["fcha_pago"];
    $reg_pago["fcha_pago"] = strtotime($reg_pago["fcha_pago"]);
    //////////////// Datos de los Detalles de la Nomina

    /// para verificar si es una nomina de la dependencia interna ()
    $sql_quien = "SELECT * FROM nominas n, valores v WHERE n.codg_nmna =".$idprint." AND n.codg_depn = v.val_val AND v.des_val='DEP_INT'";
    if ($res_quien = mysql_fetch_array(mysql_query($sql_quien))){
        $nomina_interna = 'SI';
    }
    $sql_dtll = "SELECT sc.cedu_soci, CONCAT(apel_soci, ' ', nomb_soci) as nomb_soci, COUNT(rela_dlle) as cant_pres, tp.nomb_tipo_pres as tipo_prestamo, codg_dlle, p.mont_apro, p.sald_inic_real";
    $sql_dtll .= " from nominas_detalle nd, socios sc, prestamos p, tipo_prestamos tp WHERE nd.rela_dlle = p.codg_prst AND p.codg_tipo_pres = tp.codg_tipo_pres AND nd.codg_nmna=".$idprint;
    if ($nomina_interna!='SI'){
        $sql_dtll .= " AND nd.codg_pago=".$idprint2;
    } 
    $sql_dtll .= " AND nd.cedu_soci=sc.cedu_soci AND nd.moti_dlle='Pr�stamo' GROUP BY p.codg_prst ORDER BY tp.codg_tipo_pres, sc.apel_soci,sc.nomb_soci";
    //echo $sql_dtll;
    $cuenta_datos = 0;
    $res_dtll = mysql_query($sql_dtll);
    $totales_general = array();
    $totales_partidas = array();
    while ($reg_dtll = mysql_fetch_array($res_dtll)){
        $cuenta_datos += 1;
        $detalles[$cuenta_datos]=$reg_dtll;
        $sql_prestamos="SELECT nd.*, prt.*, (SELECT COUNT(codg_dlle) FROM nominas_detalle WHERE rela_dlle=nd.rela_dlle AND moti_dlle='Pr�stamo' AND nominas_detalle.codg_nmna<=nd.codg_nmna GROUP BY rela_dlle) as pagadas from nominas_detalle nd, prestamos prt WHERE moti_dlle='Pr�stamo' AND nd.cedu_soci=".$detalles[$cuenta_datos]['cedu_soci']." AND nd.codg_nmna=".$idprint;
        if ($nomina_interna!='SI'){
            $sql_prestamos .= " AND nd.codg_pago=".$idprint2;
        }
        $sql_prestamos .=" AND nd.rela_dlle=prt.codg_prst AND nd.codg_dlle=".$reg_dtll[codg_dlle];
        $res_prestamos=mysql_query($sql_prestamos);
        //echo mysql_error();
        $cuenta_prestamo=0;
        while ($reg_prestamos=mysql_fetch_array($res_prestamos)){
          $cuenta_prestamo += 1;
          $detalles_prestamos[$cuenta_datos][$cuenta_prestamo] = $reg_prestamos;
          //// traemos los montos capital interes y total de la tabla de movimientos de prestamos
          $sql_prst_mov = "SELECT pm.*, (SELECT SUM(capi_prtm) from prestamos_mov WHERE codg_prst=pm.codg_prst AND freg_prtm <= '".$fecha_max."' GROUP BY codg_prst) AS pagado FROM prestamos_mov pm WHERE pm.rela_prtm = ".$detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["codg_dlle"];
          $res_prst_mov = mysql_query($sql_prst_mov);
          $reg_prst_mov=mysql_fetch_array($res_prst_mov);
          $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["tipo"]=str_replace('Pr�stamo a ', '', $reg_dtll['tipo_prestamo']);          
          $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["capital"]=$reg_prst_mov['capi_prtm'];
          $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["interes"]=$reg_prst_mov['inte_prtm'];
          $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["mnto_dlle"]=$reg_prst_mov['mnto_prtm'];
          $saldo = 0;
          if ($detalles[$cuenta_datos][sald_inic_real]>0) { 
            $saldo = $detalles[$cuenta_datos][sald_inic_real] - $reg_prst_mov['pagado']; 
          } else { 
            $saldo = $detalles[$cuenta_datos][mont_apro] - $reg_prst_mov['pagado'];
          } 
          $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["saldo"]=$saldo;
          $detalles_prestamos[$cuenta_datos][$cuenta_prestamo]["saldoini"]=redondear(($saldo+$reg_prst_mov['capi_prtm']),2,'','.');
        }
    }
    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'.
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
$html='<html>
<head>
    <base target="_blank">
    <title>Reportes de Nomina - Pr�stamos</title>
<style type="text/css">
    .reporte
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .reporte a
    {
        font-weight: bold;
        color: #0000FF;
    }
    .detalles
    {
        font-family: Arial; 
        font-size: 9pt;
    }
    .titulo {
        font-family: arial;
        font-size: 13pt; 
        font-weight: bold;
        color: #000000;
        background-color: #67BABA;
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 12px;
        font-weight: bold;
    }    
    .tablanomina {
        font-family: Arial;
        font-size: 9px;
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 120px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
#header,
#footer {
    position: fixed;
    left: 0;
    right: 0;
	font-size: 0.9em;
}
#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
</style>
</head>
<body>
<div id="header">
    <table cellspacing="0" cellpadding="0" border="0 align="center" class="reporte" >
    <tr height="1%">
        <td width="210px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td>
            <div align="center"><h3>'.$dependencia.'<BR>PR�STAMOS EN N�MINA<BR>('.$nomina.')</h2></div>
        </td>
        <td width="210px">
            <div align="right">Lugar y Fecha de Impresi�n: <br>Ejido, '.date(d).' de '.convertir_mes(date(m)).' de '.redondear(date(Y),0,".","").'&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    </table>
</div>
<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>';
if ($nomina_interna!='SI'){
    $html.='
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Datos del Pago</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas">Banco Origen</td>
                    <td class="etiquetas">Banco / Cuenta Destino</td>
                    <td class="etiquetas">Referencia</td>
                    <td class="etiquetas">Fecha</td>
                </tr>
                <tr align="center">
                    <td>'.$reg_pago["banc_orig"].'</td>
                    <td>'.$reg_pago["bnco_cnta"].': '.$reg_pago["nmro_cnta"].'</td>
                    <td>'.$reg_pago["numr_refe"].'</td>
                    <td>'.date("d-m-Y",$reg_pago["fcha_pago"]).'</td>
                </tr>
            </table>
            <br>';
}
            $html.='<table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">
                    <td colspan="9">Montos Acreditados</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas" width="30px">N�</td>
                    <td class="etiquetas" width="60px">C�dula</td>
                    <td class="etiquetas">Apellidos y Nombres</td>
                    <td class="etiquetas" width="120px">Tipo</td>
                    <td class="etiquetas" width="60px">S.Inicial</td>
                    <td class="etiquetas" width="60px">Capital</td>
                    <td class="etiquetas" width="60px">Inter�s</td>
                    <td class="etiquetas" width="60px">Total</td>
                    <td class="etiquetas" width="60px">Saldo</td>
                </tr>';
                for ($i=1;$i<=$cuenta_datos;$i++){
                          $html .= '<tr class="detalles">
                            <td align="right" rowspan="'.$detalles[$i]["cant_pres"].'">'.$i.'&nbsp;</td>
                            <td align="right" rowspan="'.$detalles[$i]["cant_pres"].'">'.Redondear($detalles[$i]["cedu_soci"],0,".","").'&nbsp;</td>
                            <td rowspan="'.$detalles[$i]["cant_pres"].'">&nbsp;'.$detalles[$i]["nomb_soci"].'</td>';
                            for ($j=1;$j<=$detalles[$i]["cant_pres"];$j++){
                              $html.='
                              <td align="center">'.$detalles_prestamos[$i][$j]["tipo"].'&nbsp;</td>
			      <td align="right">'.Redondear($detalles_prestamos[$i][$j]["saldoini"],2,".",",").'&nbsp;</td>
                              <td align="right">'.Redondear($detalles_prestamos[$i][$j]["capital"],2,".",",").'&nbsp;</td>
                              <td align="right">'.Redondear($detalles_prestamos[$i][$j]["interes"],2,".",",").'&nbsp;</td>
                              <td align="right">'.Redondear($detalles_prestamos[$i][$j]['mnto_dlle'],2,".",",").'&nbsp;</td>
                              <td align="right">'.Redondear($detalles_prestamos[$i][$j]['saldo'],2,".",",").'&nbsp;</td>
                              </tr>';
                              $total_prestamos[$detalles_prestamos[$i][$j]["tipo"]]["capital"]+=$detalles_prestamos[$i][$j]["capital"];
                              $total_prestamos[$detalles_prestamos[$i][$j]["tipo"]]["interes"]+=$detalles_prestamos[$i][$j]["interes"];
                              $total_prestamos[$detalles_prestamos[$i][$j]["tipo"]]["total"]+=$detalles_prestamos[$i][$j]["mnto_dlle"];

                              $total_prestamos[$detalles_prestamos[$i][$j]["tipo"]]["saldo"]+=$detalles_prestamos[$i][$j]["saldo"];

                              $total_prestamos[$detalles_prestamos[$i][$j]["tipo"]]["saldoini"]+=$detalles_prestamos[$i][$j]["saldoini"];

                              $totales_general['capital']+=$detalles_prestamos[$i][$j]["capital"];
                              $totales_general['intereses']+=$detalles_prestamos[$i][$j]["interes"];
                              $totales_general['total']+=$detalles_prestamos[$i][$j]["mnto_dlle"];

                              $totales_general['saldo']+=$detalles_prestamos[$i][$j]["saldo"];
                              $totales_general['saldoini']+=$detalles_prestamos[$i][$j]["saldoini"];


                              if ($j<$detalles[$i]["cant_pres"]){
                                $html.='<tr>';
                              }
                            }
                        if ($detalles_prestamos[$i][1]["tipo"] != $detalles_prestamos[$i+1][1]["tipo"]){
                            $html.= '<tr style="font-weight: bold;font-size: 11px;"><td colspan="4" align="right">SUB - TOTAL&nbsp;&nbsp;</td>
                                <td align="right">'.Redondear($total_prestamos[$detalles_prestamos[$i][1]["tipo"]]['saldoini'],2,".",",").'&nbsp;</td>
                                <td align="right">'.Redondear($total_prestamos[$detalles_prestamos[$i][1]["tipo"]]["capital"],2,".",",").'&nbsp;</td>
                                <td align="right">'.Redondear($total_prestamos[$detalles_prestamos[$i][1]["tipo"]]["interes"],2,".",",").'&nbsp;</td>
                                <td align="right">'.Redondear($total_prestamos[$detalles_prestamos[$i][1]["tipo"]]['total'],2,".",",").'&nbsp;</td>
                                <td align="right">'.Redondear($total_prestamos[$detalles_prestamos[$i][1]["tipo"]]['saldo'],2,".",",").'&nbsp;</td>
                            </tr>';
                        }
                }
$html .= '<tr align="center" class="titulo" style="font-size: 11px;">
                    <td colspan="4" align="right">T O T A L E S&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["saldoini"],2,'.',',').'&nbsp;</</td>
                    <td align="right">'.Redondear($totales_general["capital"],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["intereses"],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["total"],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["saldo"],2,'.',',').'&nbsp;</</td>
                </tr>
             </table>';
$html .= '</body></html>';
//echo $html;
generar_pdf($html,$dependencia.' Prestamos ('.$nomina.').pdf','letter','portrait');
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');?>
