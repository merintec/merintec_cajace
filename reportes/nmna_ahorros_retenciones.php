<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); 
include ('../reportes/generarpdf.php');
$idprint=$_GET['codg_nmna']; 
$idprint2=$_GET['codg_pago']; 
$idprint3=$_GET['codg_exce'];
    //////////////// Datos de la nomina
    $sql_nmna = "SELECT * FROM nominas nmna, dependencias dp WHERE codg_nmna=".$idprint." AND nmna.codg_depn=dp.codg_depn";
    $res_nmna = mysql_query($sql_nmna);
    $reg_nmna = mysql_fetch_array($res_nmna);
    $dependencia = $reg_nmna['nomb_depn'];
    if ($reg_nmna['prdo_nmna']>=11){ 
        $nomina = ($reg_nmna['prdo_nmna']-10).'� Semana '; 
        $semana = semana($reg_nmna["anno_nmna"],($reg_nmna["prdo_nmna"]-10));
        $fecha_max = $semana['ultimo_dia'];
    }
    if ($reg_nmna['prdo_nmna']==6){ 
        $nomina = ($reg_nmna['prdo_nmna']-5).'� Quincena '; 
        $fecha_max = $reg_nmna["anno_nmna"].'-'.$reg_nmna["mess_nmna"].'-15';
    }
    if ($reg_nmna['prdo_nmna']==7){ 
        $nomina = ($reg_nmna['prdo_nmna']-5).'� Quincena '; 
        $fecha_max = $reg_nmna["anno_nmna"].'-'.$reg_nmna["mess_nmna"].'-'.dias_mes ($reg_nmna["mess_nmna"],$reg_nmna["anno_nmna"]);
    }
    if ($reg_nmna['prdo_nmna']==8){ 
        $nomina = 'Mes '; 
        $fecha_max = $reg_nmna["anno_nmna"].'-'.$reg_nmna["mess_nmna"].'-'.dias_mes ($reg_nmna["mess_nmna"],$reg_nmna["anno_nmna"]);
    }
    $nomina .= "de ".convertir_mes($reg_nmna['mess_nmna'])." del ".$reg_nmna['anno_nmna'];
    //////////////// Datos del Pago
    $sql_pago = "SELECT * from dependencias_pagos dp, banco_cuentas bc WHERE dp.codg_pago=".$idprint2." AND dp.codg_cnta=bc.codg_cnta";
    $res_pago = mysql_query($sql_pago);
    $reg_pago = mysql_fetch_array($res_pago);
    $reg_pago["fcha_pago"] = strtotime($reg_pago["fcha_pago"]);
    /////////////// Si es pago de cuenta por pagar 
    $sql_porpagar = "SELECT * FROM dependencias_pagos_exce dpe, nominas n WHERE dpe.codg_exce=".$_GET[codg_exce]." AND dpe.codg_nmna = n.codg_nmna";
    $bus_porpagar = mysql_query($sql_porpagar);
    $res_porpagar = mysql_fetch_array($bus_porpagar);
    //////////////// Datos de los Detalles de la Nomina
    /// para verificar si es una nomina de la dependencia interna ()
    $sql_quien = "SELECT * FROM nominas n, valores v WHERE n.codg_nmna =".$idprint." AND n.codg_depn = v.val_val AND v.des_val='DEP_INT'";
    if ($res_quien = mysql_fetch_array(mysql_query($sql_quien))){
        $nomina_interna = 'SI';
    }
    $cuenta_datos = 0;
    $sql_dtll = "SELECT sc.cedu_soci, CONCAT(apel_soci, ' ', nomb_soci) as nomb_soci,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Aporte' AND cedu_soci=sc.cedu_soci";
    if ($nomina_interna!='SI'){
        $sql_dtll .= " AND codg_pago=".$idprint2;
    } 
    $sql_dtll .=" ) as aporte,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Retenci�n' AND cedu_soci=sc.cedu_soci";
    if ($nomina_interna!='SI'){
        $sql_dtll .= " AND codg_pago=".$idprint2;
    } 
    $sql_dtll .=" ) as retencion,";
    $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Reintegro' AND cedu_soci=sc.cedu_soci ";
    if ($nomina_interna!='SI'){
        $sql_dtll .= " AND codg_pago=".$idprint2;
    } 
    $sql_dtll .=" ) as reintegro,";
    $sql_dtll .= " (SELECT SUM(mont_movi) FROM socios_movimientos WHERE cedu_soci=sc.cedu_soci AND tipo_movi='I' AND fcha_movi<='".$fecha_max."') as ingresos,";
    $sql_dtll .= " (SELECT SUM(mont_movi) FROM socios_movimientos WHERE cedu_soci=sc.cedu_soci AND tipo_movi='E' AND fcha_movi<='".$fecha_max."') as egresos";
    $sql_dtll .= " from nominas_detalle nd, socios sc WHERE nd.codg_nmna=".$idprint;
    if ($nomina_interna!='SI'){
        $sql_dtll .= " AND codg_pago=".$idprint2;
    } 
    $sql_dtll .=" AND nd.cedu_soci=sc.cedu_soci GROUP BY sc.apel_soci,sc.nomb_soci";
    if ($_GET[codg_exce]>0){
        $sql_dtll = "SELECT sc.cedu_soci, CONCAT(apel_soci, ' ', nomb_soci) as nomb_soci,";
        $sql_dtll .= " '0' as aporte, ";
        $sql_dtll .= " '0' as retencion, ";
        $sql_dtll .= " (SELECT SUM(mnto_dlle) FROM nominas_detalle WHERE codg_nmna=nd.codg_nmna AND moti_dlle='Reintegro' AND cedu_soci=sc.cedu_soci AND codg_exce=".$_GET[codg_exce].") as reintegro,";
        $sql_dtll .= " (SELECT SUM(mont_movi) FROM socios_movimientos WHERE cedu_soci=sc.cedu_soci AND tipo_movi='I' AND fcha_movi<='".$fecha_max."') as ingresos,";
        $sql_dtll .= " (SELECT SUM(mont_movi) FROM socios_movimientos WHERE cedu_soci=sc.cedu_soci AND tipo_movi='E' AND fcha_movi<='".$fecha_max."') as egresos";
        $sql_dtll .= " from nominas_detalle nd, socios sc WHERE nd.codg_nmna=".$idprint." AND codg_exce=".$_GET[codg_exce]; 
        $sql_dtll .=" AND nd.cedu_soci=sc.cedu_soci GROUP BY sc.apel_soci,sc.nomb_soci";
    }
    //echo $sql_dtll;
    $res_dtll = mysql_query($sql_dtll);
    $totales_general = array();
    while ($reg_dtll = mysql_fetch_array($res_dtll)){
        $cuenta_datos += 1;
        $detalles[$cuenta_datos]=$reg_dtll;
        $detalles[$cuenta_datos]['saldo'] = $detalles[$cuenta_datos]["ingresos"]-$detalles[$cuenta_datos]["egresos"];
        $totales[$cuenta_datos]= Redondear(($detalles[$cuenta_datos]["aporte"]+$detalles[$cuenta_datos]["retencion"]+$detalles[$cuenta_datos]["reintegro"]),2,"",".");
        $totales_general["aporte"]+=$detalles[$cuenta_datos]["aporte"];
        $totales_general["retencion"]+=$detalles[$cuenta_datos]["retencion"];
        $totales_general["reintegro"]+=$detalles[$cuenta_datos]["reintegro"];
        $totales_general["saldo"]+=$detalles[$cuenta_datos]["ingresos"]-$detalles[$cuenta_datos]["egresos"];
        $totales_general["total"]+=$totales[$cuenta_datos];
    }
    $acum_cuentas = 1;
    /// cargamos el valor de los porcentajes de retenci�n, aportes y montepio
    $sql_val="select * from valores WHERE des_val='CNT_AH' OR des_val='CNT_AP'";
    $res_val = mysql_query($sql_val);
    while ($row_val = mysql_fetch_array($res_val))
    {
        // el nombre de la variable es $CNT_AH y $CNT_AP
        $$row_val['des_val'] = $row_val['val_val'];
    }
    // buscar la cuenta Ahorros
    $sql_part = "SELECT * FROM cuentas WHERE codg_pcnta=".$CNT_AH;
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $res_cnta["ahorros"]["nmro_cnta"] = $res_part["nmro_cnta"];
    $res_cnta["ahorros"]["nomb_cnta"] = $res_part["nomb_cnta"];
    // buscar la cuenta Aporte
    $sql_part = "SELECT * FROM cuentas WHERE codg_pcnta=".$CNT_AP;
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $res_cnta["aporte"]["nmro_cnta"] = $res_part["nmro_cnta"];
    $res_cnta["aporte"]["nomb_cnta"] = $res_part["nomb_cnta"];
    //// datos del pago
    $sql_part = "SELECT * FROM dependencias_pagos dpago, banco_cuentas bcnta, cuentas WHERE dpago.codg_pago =".$idprint2." AND dpago.codg_cnta=bcnta.codg_cnta AND bcnta.codg_pcnta=cuentas.codg_pcnta";
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_part['nmro_cnta'];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_part['nomb_cnta'];
    $detalle_cuentas[$acum_cuentas]["debe"] = $totales_general["total"];
    $acum_cuentas += 1;
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["ahorros"]["nmro_cnta"];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["ahorros"]["nomb_cnta"];
    $detalle_cuentas[$acum_cuentas]["haber"] = $totales_general["retencion"];
    $acum_cuentas += 1;
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["aporte"]["nmro_cnta"];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["aporte"]["nomb_cnta"];
    $detalle_cuentas[$acum_cuentas]["haber"] = $totales_general["aporte"];
    $acum_cuentas += 1;
    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'. 
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
$html='<html>
<head>
    <base target="_blank">
    <title>Reportes de Nomina - Aportes, Ahorros y Reintegros</title>
<style type="text/css">
    .reporte
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .detalles
    {
        font-family: Arial; 
        font-size: 9pt;
    }
    .reporte a
    {
        font-weight: bold;
        color: #0000FF;
    }
    .titulo {
        font-family: arial; 
        font-size: 13pt; 
        font-weight: bold; 
        color: #000000; 
        background-color: #67BABA; 
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 12px;
        font-weight: bold;
    }    
    .tablanomina {
        font-family: Arial; 
        font-size: 9px; 
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 120px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
#header,
#footer {
    position: fixed;  
    left: 0;
    right: 0;
	font-size: 0.9em;
}
#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
</style>
</head>
<body>
<div id="header">
    <table cellspacing="0" cellpadding="0" border="0 align="center" class="reporte" >
    <tr height="1%">
        <td width="210px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td>
            <div align="center"><h3>'.$dependencia.'<BR>APORTES, AHORROS Y REINTEGROS EN N�MINA<BR>('.$nomina.')</h2></div>
        </td>
        <td width="210px">
            <div align="right">Lugar y Fecha de Impresi�n: <br>Ejido, '.date(d).' de '.convertir_mes(date(m)).' de '.redondear(date(Y),0,".","").'&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    </table>
</div>
<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>';
if ($nomina_interna!='SI'){
    if($idprint2){        
    $html.='
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Datos del Pago</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas">Banco Origen</td>
                    <td class="etiquetas">Banco / Cuenta Destino</td>
                    <td class="etiquetas">Referencia</td>
                    <td class="etiquetas">Fecha</td>
                </tr>
                <tr align="center">
                    <td>'.$reg_pago["banc_orig"].'</td>
                    <td>'.$reg_pago["bnco_cnta"].': '.$reg_pago["nmro_cnta"].'</td>
                    <td>'.$reg_pago["numr_refe"].'</td>
                    <td>'.date("d-m-Y",$reg_pago["fcha_pago"]).'</td>
                </tr>
            </table>
            <br>';
    }
    if($idprint3){        
        switch ($res_porpagar[prdo_nmna]) {
           case 8:
              $porpagar_nom = 'Mes';
              break;
           case 7:
              $porpagar_nom = '2da Quincena';
              break;
           case 6:
              $porpagar_nom = '1era Quincena';
              break;
           case ($res_porpagar[prdo_nmna] > 10):
              $porpagar_nom = 'Semana '.($res_porpagar[prdo_nmna]-10).'�';
              break;
           default:
              # code...
              break;
        }
        $num_cuenta = $res_porpagar[nmro_cnta];
        $nom_cuenta = $res_porpagar[nomb_cnta];
        $referencia = $porpagar_nom.' '.convertir_mes($res_porpagar[mess_nmna]).' '.$res_porpagar[anno_nmna];
        $html.='
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Origen del Pago por Cuenta por pagar</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas">C�digo de Cuenta</td>
                    <td class="etiquetas">Nombre de Cuenta</td>
                    <td class="etiquetas">N�mina</td>
                </tr>
                <tr align="center">
                    <td>'.$num_cuenta.'</td>
                    <td>'.$nom_cuenta.'</td>
                    <td>'.$referencia.'</td>
                </tr>
            </table>
            <br>';
    }
}
            $html.='<table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="8">Montos Acreditados</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas" width="30px">N�</td>
                    <td class="etiquetas" width="80px">C�dula</td>
                    <td class="etiquetas">Apellidos y Nombres</td>
                    <td class="etiquetas" width="80px">Aporte</td>
                    <td class="etiquetas" width="80px">Ahorro</td>
                    <td class="etiquetas" width="80px">Reintegro</td>
                    <td class="etiquetas" width="80px">Total</td>
                    <td class="etiquetas" width="80px">Saldo</td>
                </tr>';
                for ($i=1;$i<=$cuenta_datos;$i++){
                        $html .= '<tr class="detalles">
                            <td align="right">'.$i.'&nbsp;</td>
                            <td align="right">'.Redondear($detalles[$i]["cedu_soci"],0,".","").'&nbsp;</td>
                            <td>&nbsp;'.$detalles[$i]["nomb_soci"].'</td>
                            <td align="right">'.Redondear($detalles[$i]["aporte"],2,".",",").'&nbsp;</td>
                            <td align="right">'.Redondear($detalles[$i]["retencion"],2,".",",").'&nbsp;</td>
                            <td align="right">'.Redondear($detalles[$i]["reintegro"],2,".",",").'&nbsp;</td>
                            <td align="right">'.Redondear($totales[$i],2,".",",").'&nbsp;</td>
                            <td align="right">'.Redondear($detalles[$i]["saldo"],2,".",",").'&nbsp;</td>
                        </tr>';
                }
$html .= '<tr align="center" class="titulo">
                    <td colspan="3" align="right">T O T A L E S&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["aporte"],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["retencion"],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["reintegro"],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["total"],2,'.',',').'&nbsp;</td>
                    <td align="right">&nbsp;</td>
                </tr>
             </table>';
              /* <br><table width="90%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo"><td colspan="4">Datos de las Cuentas</td></tr>
               <tr class="etiquetas" align="center"><td width="100px">C�DIGO</td><td>CONCEPTO</td><td width="100px">DEBE</td><td width="100px">HABER</td></tr>
                 '; 
                 for ($k=1;$k<$acum_cuentas;$k++){
                     $html.='<tr><td align="center">'.$detalle_cuentas[$k]["codigo"].'</td><td>&nbsp;'.$detalle_cuentas[$k]["concepto"].'</td><td align="right">'.redondear($detalle_cuentas[$k]["debe"],2,".",",").'&nbsp;</td><td align="right">'.redondear($detalle_cuentas[$k]["haber"],2,".",",").'&nbsp;</td></tr>';
                     $suma_debe += $detalle_cuentas[$k]["debe"];
                     $suma_haber += $detalle_cuentas[$k]["haber"];
                 }
                 $html.='<tr class="etiquetas"><td colspan="2" align="right">TOTALES (SUMAS IGUALES)&nbsp;</td><td align="right">'.redondear($suma_debe,2,".",",").'&nbsp;</td><td align="right">'.redondear($suma_haber,2,".",",").'&nbsp;</td></tr>';
                 $html.='</table>';*/
$html.= '<font size="2px">NOTA: El saldo de cada socio corresponde al saldo a la fecha de generaci�n del reporte, �ste cambiar� si lo genera nuevamente en el futuro</font>';
$html .= '</body></html>';
//echo $html;
generar_pdf($html,$dependencia.' Aportes, Ahorros y Reintegros ('.$nomina.').pdf','letter','portrait');
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');?>
