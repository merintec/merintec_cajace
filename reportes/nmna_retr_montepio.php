<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); 
include ('../reportes/generarpdf.php');
$idprint=$_GET['codg_retr']; 
    //////////////// Datos de la nomina
    $sql_nmna = "SELECT * FROM nominas_retroactivos nmna_retr, dependencias dp WHERE codg_retr=".$idprint." AND nmna_retr.codg_depn=dp.codg_depn";    
    $res_nmna = mysql_query($sql_nmna);
    $reg_nmna = mysql_fetch_array($res_nmna);
    $dependencia = $reg_nmna['nomb_depn'];    $codg_pago = $reg_nmna['codg_pago'];
    $fecha_max = $reg_nmna['fcha_retr'];
    $nomina = 'Del '.$reg_nmna['mini_retr'].'/'.$reg_nmna['aini_retr'].' al '.$reg_nmna['mfin_retr'].'/'.$reg_nmna['afin_retr'].'';
    //////////////// Datos del Pago
    $sql_pago = "SELECT * from dependencias_pagos dp, banco_cuentas bc WHERE dp.codg_pago=".$codg_pago." AND dp.codg_cnta=bc.codg_cnta";
    $res_pago = mysql_query($sql_pago);
    $reg_pago = mysql_fetch_array($res_pago);
    $reg_pago["fcha_pago"] = strtotime($reg_pago["fcha_pago"]);    
    //////////////// Datos de los Detalles de la Nomina
    $cuenta_datos = 0;
    $sql_dtll = "SELECT sc.cedu_soci, CONCAT(apel_soci, ' ', nomb_soci) as nomb_soci,";
    $sql_dtll .= " (SELECT SUM(mont_nr_dlle) FROM nominas_retroactivos_detalles WHERE codg_retr=nd.codg_retr AND dest_nr_dlle='Montepio' AND cedu_soci=sc.cedu_soci) as montepio";
    $sql_dtll .= " from nominas_retroactivos_detalles nd, socios sc WHERE nd.codg_retr=".$idprint." AND nd.cedu_soci=sc.cedu_soci AND dest_nr_dlle='Montepio' GROUP BY nd.cedu_soci";
    $res_dtll = mysql_query($sql_dtll);
    $totales_general = array();
    while ($reg_dtll = mysql_fetch_array($res_dtll)){
        $cuenta_datos += 1;
        $detalles[$cuenta_datos]=$reg_dtll;
        $totales[$cuenta_datos]= Redondear(($detalles[$cuenta_datos]["montepio"]),2,"",".");
        $totales_general["montepio"]+=$detalles[$cuenta_datos]["montepio"];
    }
        $acum_cuentas = 1;
    /// cargamos el valor de los porcentajes de retenci�n, aportes y montepio
    $sql_val="select * from valores WHERE des_val='CNT_AH' OR des_val='CNT_AP'";
    $res_val = mysql_query($sql_val);
    while ($row_val = mysql_fetch_array($res_val))
    {
        // el nombre de la variable es $CNT_AH y $CNT_AP
        $$row_val['des_val'] = $row_val['val_val'];
    }
    // buscar la cuenta Ahorros
    $sql_part = "SELECT * FROM cuentas WHERE codg_pcnta=".$CNT_AH;
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $res_cnta["ahorros"]["nmro_cnta"] = $res_part["nmro_cnta"];
    $res_cnta["ahorros"]["nomb_cnta"] = $res_part["nomb_cnta"];
    // buscar la cuenta Aporte
    $sql_part = "SELECT * FROM cuentas WHERE codg_pcnta=".$CNT_AP;
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $res_cnta["aporte"]["nmro_cnta"] = $res_part["nmro_cnta"];
    $res_cnta["aporte"]["nomb_cnta"] = $res_part["nomb_cnta"];
    //// datos del pago
    $sql_part = "SELECT * FROM dependencias_pagos dpago, banco_cuentas bcnta, cuentas WHERE dpago.codg_pago =".$idprint2." AND dpago.codg_cnta=bcnta.codg_cnta AND bcnta.codg_pcnta=cuentas.codg_pcnta";
    $res_part = mysql_fetch_array(mysql_query($sql_part));
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_part['nmro_cnta'];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_part['nomb_cnta'];
    $detalle_cuentas[$acum_cuentas]["debe"] = $totales_general["total"];
    $acum_cuentas += 1;
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["ahorros"]["nmro_cnta"];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["ahorros"]["nomb_cnta"];
    $detalle_cuentas[$acum_cuentas]["haber"] = $totales_general["retencion"];
    $acum_cuentas += 1;
    $detalle_cuentas[$acum_cuentas]["codigo"] = $res_cnta["aporte"]["nmro_cnta"];
    $detalle_cuentas[$acum_cuentas]["concepto"] = $res_cnta["aporte"]["nomb_cnta"];
    $detalle_cuentas[$acum_cuentas]["haber"] = $totales_general["aporte"];
    $acum_cuentas += 1;
    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'. 
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
$html='<html>
<head>
    <base target="_blank">
    <title>Reportes de Nomina - Montepio</title>
<style type="text/css">
    .reporte
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .reporte a
    {
        font-weight: bold;
        color: #0000FF;
    }
    .detalles
    {
        font-family: Arial; 
        font-size: 9pt;
    }
    .titulo {
        font-family: arial; 
        font-size: 13pt; 
        font-weight: bold; 
        color: #000000; 
        background-color: #67BABA; 
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 12px;
        font-weight: bold;
    }    
    .tablanomina {
        font-family: Arial; 
        font-size: 9px; 
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 120px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
#header,
#footer {
    position: fixed;  
    left: 0;
    right: 0;
	font-size: 0.9em;
}
#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
</style>
</head>
<body>
<div id="header">
    <table cellspacing="0" cellpadding="0" border="0 align="center" class="reporte" >
    <tr height="1%">
        <td width="210px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td>
            <div align="center"><h3>'.$dependencia.'<BR>MONTEP�O EN N�MINA DE RETROACTIVOS<BR>('.$nomina.')</h2></div>
        </td>
        <td width="210px">
            <div align="right">Lugar y Fecha de Impresi�n: <br>Ejido, '.date(d).' de '.convertir_mes(date(m)).' de '.redondear(date(Y),0,".","").'&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    </table>
</div>
<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Datos del Pago</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas">Banco Origen</td>
                    <td class="etiquetas">Banco / Cuenta Destino</td>
                    <td class="etiquetas">Referencia</td>
                    <td class="etiquetas">Fecha</td>
                </tr>
                <tr align="center">
                    <td>'.$reg_pago["banc_orig"].'</td>
                    <td>'.$reg_pago["bnco_cnta"].': '.$reg_pago["nmro_cnta"].'</td>
                    <td>'.$reg_pago["numr_refe"].'</td>
                    <td>'.date("d-m-Y",$reg_pago["fcha_pago"]).'</td>
                </tr>
            </table>
            <br>
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="4">Montos Acreditados</td>
                </tr>
                <tr align="center">
                    <td class="etiquetas" width="30px">N�</td>
                    <td class="etiquetas" width="100px">C�dula</td>
                    <td class="etiquetas">Apellidos y Nombres</td>
                    <td class="etiquetas" width="80px">Monto</td>
                </tr>';
                for ($i=1;$i<=$cuenta_datos;$i++){
                        $html .= '<tr class="detalles">
                            <td align="right">'.$i.'&nbsp;</td>
                            <td align="right">'.Redondear($detalles[$i]["cedu_soci"],0,".","").'&nbsp;</td>
                            <td>&nbsp;'.$detalles[$i]["nomb_soci"].'</td>
                            <td align="right">'.Redondear($detalles[$i]["montepio"],2,".",",").'&nbsp;</td>
                        </tr>';
                }
$html .= '<tr align="center" class="titulo">
                    <td colspan="3" align="right">T O T A L E S&nbsp;</td>
                    <td align="right">'.Redondear($totales_general["montepio"],2,'.',',').'&nbsp;</td>
                </tr>
             </table>';
$html .= '</body></html>';
//echo $html;
generar_pdf($html,$dependencia.' Montepios ('.$nomina.').pdf','letter','portrait');
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');?>
