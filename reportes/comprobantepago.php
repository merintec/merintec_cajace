<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); 
include ('../reportes/generarpdf.php'); 
include('../comunes/numerosaletras.php');?>
<?php
    //// tomar los valores pasados por URL 
    $idprint=$_GET['codg_egre']; 
    $chequeprint=$_GET['cheque']; 
    ///////////////// traemos los datos de la empresa
    $consulta_empresa= mysql_query("SELECT   * from emp_empresa  ");
    $con_emp=mysql_fetch_assoc($consulta_empresa);
    $encabezado_empresa = $con_emp[nomb_empr]." ".$con_emp[regi_empr].", RIF ".$con_emp[nrif_empr]." Tel�fono: ".$con_emp[tlfn_empr];
    ///////////////// Datos del Egreso
    $result=mysql_query("SELECT * FROM egresos eg, banco_cuentas bc WHERE eg.codg_egre=".$idprint." AND eg.codg_cnta=bc.codg_cnta");
	if ($row=mysql_fetch_array($result))
	{
	    $existe = 'SI';
   	    $codg_egre = $row["codg_egre"];
	    $fcha_egre = $row["fcha_egre"];
	    $fecha_print = strtotime($fcha_egre);
	    $nmro_egre = $row["nmro_egre"];
	    $nmero_comprobante = date(m,$fecha_print).date(y,$fecha_print).'-'.$row["nmro_egre"];
	    $moti_egre = $row["moti_egre"];
	    $nomb_rela = $row["nomb_rela"];
	    $codg_rela = $row["codg_rela"];
	    $codg_mpio = $row["codg_mpio"];
	    if ($codg_mpio=='') { $codg_mpio='NULL'; }
	    else {
	        $sql_socio = "SELECT * FROM socios where cedu_soci=".$codg_rela;
	        $bus_socio = mysql_query($sql_socio);
	        if ($reg_socio = mysql_fetch_array($bus_socio)){
	            $beneficiario[0] = redondear($reg_socio["cedu_soci"],0,".",",");
	            $beneficiario[1] = $reg_socio["apel_soci"]."&nbsp;".$reg_socio["nomb_soci"];
	            $beneficiario[2] = $reg_socio["dirh_soci"];
	        }
	        else {
	            $beneficiario[0] = redondear($codg_rela,0,".",",");
	            $beneficiario[1] = $nomb_rela;
	            $beneficiario[2] = '';
	        }
	    }
	    $cod_pro = $row["cod_pro"];
	    if ($cod_pro=='') { $cod_pro='NULL'; }
	    else {
	        $sql_prov = "SELECT * FROM proveedores where cod_pro=".$cod_pro;
	        $bus_prov = mysql_query($sql_prov);
	        $reg_prov = mysql_fetch_array($bus_prov);
            $beneficiario[0] = redondear($reg_prov["rif_pro"],0,".",",");
	        $beneficiario[1] = $reg_prov["nom_pro"];
	        $beneficiario[2] = $reg_prov["dir_pro"];
	    }
	    $codg_prst = $row["codg_prst"];
	    if ($codg_prst=='') { $codg_prst='NULL'; }
	    else {
	        $sql_socio = "SELECT * FROM socios where cedu_soci=".$codg_rela;
	        $bus_socio = mysql_query($sql_socio);
	        $reg_socio = mysql_fetch_array($bus_socio);
            $beneficiario[0] = redondear($reg_socio["cedu_soci"],0,".",",");
	        $beneficiario[1] = $reg_socio["apel_soci"]."&nbsp;".$reg_socio["nomb_soci"];
	        $beneficiario[2] = $reg_socio["dirh_soci"];
	    }
	    $codg_reti = $row["codg_reti"];
	    if ($codg_reti=='') { $codg_reti='NULL'; }
	    else {
	        $sql_socio = "SELECT * FROM socios where cedu_soci=".$codg_rela;
	        $bus_socio = mysql_query($sql_socio);
	        if ($reg_socio = mysql_fetch_array($bus_socio)){
	            $beneficiario[0] = redondear($reg_socio["cedu_soci"],0,".",",");
	            $beneficiario[1] = $reg_socio["apel_soci"]."&nbsp;".$reg_socio["nomb_soci"];
	            $beneficiario[2] = $reg_socio["dirh_soci"];
	        }
	        else {
	            $beneficiario[0] = redondear($codg_rela,0,".",",");
	            $beneficiario[1] = $nomb_rela;
	            $beneficiario[2] = '';
	        }
	    }
	    if ($moti_egre=='Reimpresi�n'){
              $beneficiario[0] = redondear($codg_rela,0,".",",");
	        $beneficiario[1] = $nomb_rela;
          }
	    $codg_cnta = $row["codg_cnta"];
	    $nmro_cheq = $row["nmro_cheq"];
	    $elab_egre = $row["elab_egre"];
	    $revi_egre = $row["revi_egre"];
	    $autr_egre = $row["autr_egre"];
	    $cntb_egre = $row["cntb_egre"];
	    $anul_egre = $row["anul_egre"];
	    $anul_obsr = $row["anul_obsr"];
	    $bnco_cnta = $row["bnco_cnta"];
	    $dedu_egre = $row["dedu_egre"];
	    $obsr_egre = $row["obsr_egre"];
	}
	//////////////// DATOS DE LOS CONCEPTOS
	$total_debe = 0;
	$total_haber = 0;
	$sql_conceptos = "SELECT * FROM egresos_conceptos WHERE codg_egre=".$codg_egre;
	$bus_conceptos = mysql_query($sql_conceptos);
	$i=0;
	while ($reg_conceptos = mysql_fetch_array($bus_conceptos)){
	    $conceptos[$i][0]=$reg_conceptos[2];
	    $conceptos[$i][1]=$reg_conceptos[3];
	    $conceptos[$i][2]=$reg_conceptos[4];
	    $conceptos[$i][3]=$reg_conceptos[5];
	    $total_debe += $reg_conceptos[4];
	    $total_haber += $reg_conceptos[5];
	    $i++;
	}
	$ttotal_debe = $total_debe;
	$ttotal_haber = $total_haber;
	$ttotal_haber = redondear(($ttotal_haber-$dedu_egre),2,'','.');
        $monto_pago_letras = convertir_a_letras($ttotal_haber,'');
        $monto_pago_letras = ucwords(strtolower($monto_pago_letras));
        //$sql_monto_ch = "SELECT egc.mnto_hber as monto_cheque FROM egresos eg, banco_cuentas bnc, cuentas cnta, egresos_conceptos egc WHERE eg.codg_egre=".$idprint." AND eg.codg_cnta=bnc.codg_cnta AND bnc.codg_pcnta=cnta.codg_pcnta AND eg.codg_egre=egc.codg_egre AND cnta.nmro_cnta=egc.codg_ctab ORDER BY monto_cheque DESC LIMIT 1";
        $sql_monto_ch = "SELECT egc.mnto_hber as monto_cheque FROM egresos eg, banco_cuentas bnc, cuentas cnta, egresos_conceptos egc WHERE eg.codg_egre=".$idprint." AND eg.codg_cnta=bnc.codg_cnta AND bnc.codg_pcnta=cnta.codg_pcnta AND eg.codg_egre=egc.codg_egre ORDER BY monto_cheque DESC LIMIT 1";
        $res_monto_ch = mysql_fetch_array(mysql_query($sql_monto_ch));
	$ttotal_haber = $res_monto_ch['monto_cheque'];
	$monto_pago_letras = convertir_a_letras($ttotal_haber,'');
    $monto_pago_letras = ucwords(strtolower($monto_pago_letras));
    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tabla_print" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'. 
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
?>
<?php 
	$html='<html>
<head>
    <base target="_blank">
    <title>Comprobante de Pago</title>
    <style type="text/css">
    .encabezado
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .titulo {
        font-family: arial; 
        font-size: 13pt; 
        font-weight: bold; 
        color: #000000; 
        background-color: #67BABA; 
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .div_cheque {
        border:solid 1px ';
        if ($chequeprint!='') {
            $html.='#FFFFFF;';
        }
        else { $html.='#000000;'; }
        if ($chequeprint!='') {
            $html.='margin-top: -175px;';
        }
    $html.='
        width: 680px;
        margin-left: auto;
        margin-right: auto;
        color: #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 13px;
        font-weight: bold;';
        if ($chequeprint!='') {
            $html.='color: #FFFFFF';
        }
    $html.='
    }    
    .tabla_print {
        font-family: Arial; 
        font-size: 9px; 
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 130px;
        margin-right: 0px;
        margin-bottom: 0px;';
        if ($chequeprint!='') {
            $html.='color: #FFFFFF';
        }
    $html.='
    }
    .conceptos {
        font-size: 10px;
    }
    #header,
    #footer {
        position: fixed;  
        left: 0;
        right: 0;
    	font-size: 0.9em;
    	';
        if ($chequeprint!='') {
            $html.='visibility: hidden';
        }
    $html.='
    }
    #header {
        top: 0;
	    border-bottom: 0.1pt solid #aaa;
    }
    #footer {
        bottom: 0;
        border-top: 0.1pt solid #aaa;
    }
    </style>
</head>
<body>
<div id="header">
    <div style="text-align: right; position:absolute; right: 0px; top: -10px; width: 200px; "><font size="18" color="#FF0000"><b>N&ordm; '.$nmero_comprobante.'</b></font></div>
    <table cellspacing="0" cellpadding="0" border="0" align="center" class="encabezado" border="1">
    <tr height="1%">
        <td width="1px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td align="center">
            <div align="center" style="width: 400px; margin-left: auto; margin-right: auto;">
                '.$encabezado_empresa.'<br>
            </div>
        </td>
    </tr>
    <tr height="1%">
        <td colspan="2" align="center">
            <font size="14"><b>COMPROBANTE DE PAGO</b></font>
        </td>
    </tr>

    </table>
</div>

<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>';
$html .= '
    <div align="center" class="div_cheque" style="height: 308px">
        <div style="position: relative; margin-left: auto; margin-right: 20px; top: 20px; width: 150px; border: 1px solid #FFFFFF;" >
            '.redondear($ttotal_haber,2,".",",").'
        </div>
        <div style="position: relative; margin-left: auto; margin-right: 20px; top: 55px; width: 500px; border: 1px solid #FFFFFF;" >
            '.$beneficiario[1].'
        </div>
        <div style="text-align:left; position: relative; margin-left: auto; margin-right: 20px; top: 64px; width: 600px; height:40px; border: 1px solid #FFFFFF;" >
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$monto_pago_letras.'
        </div>
        <div style="text-align=left; position: relative; margin-left: auto; margin-right: 440px; top: 69px; width: 200px; border: 1px solid #FFFFFF;" >
            Ejido,  '.date(d,$fecha_print).' / '.date(m,$fecha_print).'
        </div>
        <div style="position: relative; margin-left: auto; margin-right: 330px; top: 48px; width: 50px; border: 1px solid #FFFFFF;" >
            '.date(y,$fecha_print).'
        </div>
        <div style="position: relative; margin-left: auto; margin-right: 330px; top: 115px; width: 250px; border: 1px solid #FFFFFF;" >
            <font size="12"><b>CADUCA A LOS 30 D�AS</b></font>
        </div>
    </div>';
if ($chequeprint==''){
$html.= '
<table width="680px" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;" border="1" bordercolor="'; if ($chequeprint!=''){ $html.='#FFFFFF'; } else { $html.='#000000'; } $html.='">
    <tr>
        <td class="etiquetas" align="left" width="30%"> 
            &nbsp;NOMBRE O RAZ�N SOCIAL:&nbsp;&nbsp;
        </td>
        <td class="etiquetas" align="center"> 
            '.$beneficiario[1].'
        </td>
    </tr>
    <tr>
        <td class="etiquetas" align="left" width="30%"> 
            &nbsp;RIF O C�DULA:&nbsp;&nbsp;
        </td>
        <td class="etiquetas" align="center"> 
            '.$beneficiario[0].'
        </td>
    </tr>
    <tr>
        <td class="etiquetas" align="left" width="30%"> 
            &nbsp;DOMICILIO FISCAL:&nbsp;&nbsp;
        </td>
        <td class="etiquetas" align="center"> 
            '.$beneficiario[2].'
        </td>
    </tr>
</table>
';
$html.= '
<table width="680px" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;" border="1" bordercolor="'; if ($chequeprint!=''){ $html.='#FFFFFF'; } else { $html.='#000000'; } $html.='">
    <tr>
        <td class="etiquetas" align="center" width="20%"> 
            C�DIGO
        </td>
        <td class="etiquetas" align="center"> 
            CONCEPTO
        </td>
        <td class="etiquetas" align="center" width="15%"> 
            DEBE
        </td>
        <td class="etiquetas" align="center" width="15%"> 
            HABER
        </td>
    </tr>';
    for ($i=0;$i<=10;$i++){
        if ($conceptos[$i][2]!=0.00) {
            $debe = redondear($conceptos[$i][2],2,".",",");
        } else { $debe= ''; }
        if ($conceptos[$i][3]!=0.00) {
            $haber = redondear($conceptos[$i][3],2,".",",");
        } else { $haber= ''; }
        $html .= '<tr class="conceptos">
            <td>'.$conceptos[$i][0].'</td><td align="left">'.$conceptos[$i][1].'</td><td align="right">'.$debe.'&nbsp;</td><td align="right">'.$haber.'&nbsp;</td>
        </tr>';
    }
$html.='</table>';
$html.= '
<table width="680px" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;" border="1" bordercolor="'; if ($chequeprint!=''){ $html.='#FFFFFF'; } else { $html.='#000000'; } $html.='">
    <tr>
        <td class="etiquetas" rowspan="2" align="left" width="50%"> 
            Observaciones:<br>'.$obsr_egre.'
        </td>
        <td class="etiquetas" align="right"> 
            Sub-Total&nbsp;&nbsp;
        </td>
        <td class="etiquetas" align="right" width="15%"> 
            '.redondear($total_debe,2,".",",").'&nbsp;
        </td>
        <td class="etiquetas" align="right" width="15%"> 
            '.redondear($total_haber,2,".",",").'&nbsp;
        </td>
    </tr>
    <tr>
        <td class="etiquetas" align="right"> 
            TOTAL A PAGAR&nbsp;&nbsp;
        </td>
        <td class="etiquetas" align="right" colspan="2"> 
            '.redondear($ttotal_haber,2,".",",").'&nbsp;
        </td>
    </tr>
</table>';
$html.= '
<table width="680px" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;" border="1" bordercolor="'; if ($chequeprint!=''){ $html.='#FFFFFF'; } else { $html.='#000000'; } $html.='">
    <tr>
        <td class="etiquetas" align="left" width="20%" height="40px"> 
            &nbsp;SON BOL�VARES:&nbsp;&nbsp;
        </td>
        <td class="etiquetas" align="center"> 
            '.$monto_pago_letras.'
        </td>
    </tr>
</table>
';
$html.= '
<table width="680px" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;" border="1" bordercolor="'; if ($chequeprint!=''){ $html.='#FFFFFF'; } else { $html.='#000000'; } $html.='">
    <tr>
        <td class="etiquetas" align="left" width="50%"> 
            &nbsp;BANCO:&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$bnco_cnta.'
        </td>
        <td class="etiquetas" align="left"> 
            &nbsp;CHEQUE / TRANSFERENCIA:&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$nmro_cheq.'
        </td>
    </tr>
</table>
';
$html.= '
<table width="680px" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;" border="1" bordercolor="'; if ($chequeprint!=''){ $html.='#FFFFFF'; } else { $html.='#000000'; } $html.='">
    <tr>
        <td colspan="4" class="etiquetas" align="center">
            POR LA CAJA DE AHORRO
        </td>
    </tr>
    <tr>
        <td valign="top" class="etiquetas" align="left" width="25%" height="45px"> 
            &nbsp;ELABORADO POR:&nbsp;<br>&nbsp;'.$elab_egre.'
        </td>
        <td valign="top" class="etiquetas" align="left" width="25%"> 
            &nbsp;REVISADO POR:&nbsp;<br>&nbsp;'.$revi_egre.'
        </td>
        <td valign="top" class="etiquetas" align="left" width="25%"> 
            &nbsp;AUTORIZADO POR:&nbsp;<br>&nbsp;'.$autr_egre.'
        </td>
        <td valign="top" class="etiquetas" align="left" width="25%"> 
            &nbsp;CONTABILIZADO POR:&nbsp;<br>&nbsp;'.$cntb_egre.'
        </td>
    </tr>
</table>
';
$html.='
<table width="680px" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;" border="1" bordercolor="'; if ($chequeprint!=''){ $html.='#FFFFFF'; } else { $html.='#000000'; } $html.='">
    <tr>
        <td colspan="4" class="etiquetas" align="center">
            RECIBIDO POR
        </td>
    </tr>
    <tr>
        <td valign="top" class="etiquetas" align="left" width="25%" height="55px"> 
            &nbsp;NOMBRE Y APELLIDO:&nbsp;<br>&nbsp;
        </td>
        <td valign="top" class="etiquetas" align="left"> 
            &nbsp;FIRMA:&nbsp;<br>&nbsp;
        </td>
        <td valign="top" class="etiquetas" align="left" width="15%"> 
            &nbsp;C.I.:&nbsp;<br>&nbsp;
        </td>
        <td valign="top" class="etiquetas" align="left" width="10%"> 
            &nbsp;FECHA:&nbsp;<br>&nbsp;
        </td>
    </tr>
</table>
';
}
$html .= '</body></html>';
//echo $html;
$archivo_salida = 'comprobante_pago';
if ($chequeprint){
   $archivo_salida = 'cheque';
}
$archivo_salida .= '_'.$nmero_comprobante.'.pdf';
generar_pdf($html,$archivo_salida,'letter','portrait');
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php'); ?>
