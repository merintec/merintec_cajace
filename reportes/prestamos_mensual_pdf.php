<?php include('../comunes/conexion_basedatos.php'); 
$print_pdf="SI";
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); 
include ('../reportes/generarpdf.php');
$idprint=$_GET['codg_depn']; 
$idprint2=$_GET['mes']; 
$idprint3=$_GET['anno']; 
    //////////////// datos de la dependencia
    if ($idprint!=''){
      $sql_dep = "SELECT nomb_depn FROM dependencias WHERE codg_depn=".$idprint;
      $dependencia=mysql_fetch_array(mysql_query($sql_dep));
      $dependencia=$dependencia[0];
    }
    else{
      $dependencia="Todas las Dependencias";
    }
    //////////////// Datos de los PRESTAMOS
    $cuenta_datos = 0;
    $fecha_max = $fecha_max = $idprint3.'-'.$idprint2.'-'.dias_mes ($idprint2,$idprint3);
        /// buscar prestamos otorgados en el mes
        $sql_prst_asig = "SELECT dp.codg_depn, dp.nomb_depn, tprt.codg_tipo_pres, tprt.nomb_tipo_pres, SUM(prt.mont_apro+prt.sald_inic_real) as otorgados  FROM prestamos prt, socios sc, socios_dat_lab sl, dependencias dp, tipo_prestamos tprt WHERE prt.stat_prst='A' AND prt.codg_tipo_pres=tprt.codg_tipo_pres AND YEAR(prt.fcha_acta)='".$idprint3."' AND MONTH(prt.fcha_acta)=".$idprint2." AND prt.cedu_soci=sc.cedu_soci AND sc.cedu_soci=sl.cedu_soci AND sl.codg_dlab IN (SELECT MAX(codg_dlab) FROM socios_dat_lab WHERE cedu_soci=sc.cedu_soci AND fchi_dlab<='".$fecha_max."') AND sl.codg_depn=dp.codg_depn";
        if ($idprint){ $sql_prst_asig .= " AND dp.codg_depn=".$idprint; }
        $sql_prst_asig .= " GROUP BY dp.codg_depn, tprt.codg_tipo_pres ORDER BY dp.codg_depn, tprt.codg_tipo_pres";
        $bus_prst_asig = mysql_query($sql_prst_asig);
        while ($res_prt_asig=mysql_fetch_array($bus_prst_asig)){
           $detalles[$res_prt_asig['codg_depn']][$res_prt_asig['codg_tipo_pres']]['otorgados']['capital']=$res_prt_asig['otorgados'];
        }
        /// Buscamos los movimientos de los prestamos del mes
        $sql_prst_mov = "SELECT dp.codg_depn, dp.nomb_depn, tprt.codg_tipo_pres, tprt.nomb_tipo_pres, prtm.orgn_prtm, SUM(capi_prtm) AS capital, SUM(inte_prtm) AS interes, SUM(mnto_prtm) as total FROM prestamos_mov prtm, prestamos prt, tipo_prestamos tprt, socios sc, socios_dat_lab sl, dependencias dp WHERE YEAR(prtm.fpag_prtm)='".$idprint3."' AND MONTH(prtm.fpag_prtm)='".$idprint2."' AND prtm.codg_prst=prt.codg_prst AND prt.codg_tipo_pres=tprt.codg_tipo_pres AND prt.cedu_soci=sc.cedu_soci AND sc.cedu_soci=sl.cedu_soci AND sl.codg_dlab IN (SELECT MAX(codg_dlab) FROM socios_dat_lab WHERE cedu_soci=sc.cedu_soci AND fchi_dlab<='".$fecha_max."') AND sl.codg_depn=dp.codg_depn";
        if ($idprint){ $sql_prst_mov .= " AND dp.codg_depn=".$idprint; }
        $sql_prst_mov .= " GROUP BY dp.codg_depn, tprt.codg_tipo_pres, prtm.orgn_prtm ORDER BY dp.codg_depn, tprt.codg_tipo_pres, prtm.orgn_prtm";
        $bus_prst_mov = mysql_query($sql_prst_mov);
        while ($res_prt_mov=mysql_fetch_array($bus_prst_mov)){
           $detalles[$res_prt_mov['codg_depn']][$res_prt_mov['codg_tipo_pres']][$res_prt_mov['orgn_prtm']]['capital']=$res_prt_mov['capital'];
           $detalles[$res_prt_mov['codg_depn']][$res_prt_mov['codg_tipo_pres']][$res_prt_mov['orgn_prtm']]['interes']=$res_prt_mov['interes'];
        }
        
    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'. 
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////
$html='<html>
<head>
    <base target="_blank">
    <title>Resumen mensual de pr�stamos</title>
<style type="text/css">
    .reporte
    {
        font-family: Arial; 
        font-size: 10pt;
        text-align:justify;
        border-collapse:collapse;
        border:solid 0px #FFFFFF;
        width: 100%;
    }
    .reporte a
    {
        font-weight: bold;
        color: #0000FF;
    }
    .titulo {
        font-family: arial; 
        font-size: 13pt; 
        font-weight: bold; 
        color: #000000; 
        background-color: #67BABA; 
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .total_prest{
        font-family: arial; 
        font-size: 10pt; 
        font-weight: bold; 
        color: #000000; 
        background-color: #67BABA; 
        text-align: center
        border-collapse:collapse;
        border:solid 1px #000000;
    }
    .etiquetas {
        color: #000000;
        font-size: 12px;
        font-weight: bold;
    }    
    .tablanomina {
        font-family: Arial; 
        font-size: 10px; 
    }
    body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 120px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
#header,
#footer {
    position: fixed;  
    left: 0;
    right: 0;
	font-size: 0.9em;
}
#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}
#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}
</style>
</head>
<body>
<div id="header">
    <table cellspacing="0" cellpadding="0" border="0 align="center" class="reporte" >
    <tr height="1%">
        <td width="210px">
            <img src="../imagenes/logo_report.jpg">
        </td>
        <td>
            <div align="center"><h3>'.$dependencia.'<BR>RESUMEN DE PR�STAMOS <BR>DEL MES '.convertir_mes($idprint2).' de '.redondear($idprint3,0,'.','').')</h2></div>
        </td>
        <td width="210px">
            <div align="right">Lugar y Fecha de Impresi�n: <br>Ejido, '.date(d).' de '.convertir_mes(date(m)).' de '.redondear(date(Y),0,".","").'&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    </table>
</div>
<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>
            <center><h2>'.$mensaje_interna.'</h2></center>
            <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center">
                <tr height="1%" align="center" class="titulo">    
                    <td colspan="10">Montos de Pr�stamos</td>
                </tr>';
                if ($idprint){
                $html.= '<tr align="center" class="etiquetas">
                    <td rowspan="2">Tipo de Pr�stamo</td>
                    <td width="70px">Otorgados</td>
                    <td width="120px" colspan="2">Pagados</td>
                    <td width="120px" colspan="2">Abonados</td>
                    <td width="120px" colspan="2">En N�mina</td>
                    <td width="120px" colspan="2">Total</td>
                </tr>
                <tr class="etiquetas" align="center"><td>Capital</td><td>Capital</td><td>Inter�s</td><td>Capital</td><td>Inter�s</td><td>Capital</td><td>Inter�s</td><td>Capital</td><td>Inter�s</td>
                </tr>';
                }
                    /// Pasamos por todas las Dependencias
                    $sql_dep_print = "SELECT * FROM dependencias";
                    if ($idprint){ $sql_dep_print .= " WHERE codg_depn=".$idprint; }
                    $bus_dep_print = mysql_query($sql_dep_print);
                    while ($res_dep_print = mysql_fetch_array($bus_dep_print)){
                        /// Pasamos por todos los prestamos
                        $sql_prest_print = "SELECT * FROM tipo_prestamos";
                        $bus_prest_print = mysql_query($sql_prest_print);
                        while ($res_prest_print = mysql_fetch_array($bus_prest_print)){
                            if ($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['otorgados']['capital']|| $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de N�mina']['capital']|| $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de N�mina']['interes']|| $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Abono de Pr�stamos']['capital']|| $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Abono de Pr�stamos']['interes']|| $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de Pr�stamo']['capital']|| $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de Pr�stamo']['interes']){
                                if ($dep_ant!=$res_dep_print['codg_depn'] && !$idprint){
                                   if ($dep_ant){
                                      $cuenta_dep_print += 1;
                                      $html .= '<tr class="etiquetas">
                                              <td align="right">SUB-TOTALES&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['otorgados']['capital'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['pagados']['capital'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['pagados']['interes'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['abonados']['capital'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['abonados']['interes'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['nomina']['capital'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['nomina']['interes'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['total']['capital'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['total']['interes'],2,".",",").'&nbsp;</td>
                                            </tr>';
                                   }
                                $html .= '<tr>
                                            <td align="center" colspan="10"><b>'.$res_dep_print['nomb_depn'].'</b></td>
                                          </tr>
                                          <tr align="center" class="etiquetas">
                                            <td rowspan="2">Tipo de Pr�stamo</td>
                                            <td width="70px">Otorgados</td>
                                            <td width="120px" colspan="2">Pagados</td>
                                            <td width="120px" colspan="2">Abonados</td>
                                            <td width="120px" colspan="2">En N�mina</td>
                                            <td width="120px" colspan="2">Total</td>
                                          </tr>
                                          <tr class="etiquetas" align="center"><td>Capital</td><td>Capital</td><td>Inter�s</td><td>Capital</td><td>Inter�s</td><td>Capital</td><td>Inter�s</td><td>Capital</td><td>Inter�s</td>
                                          </tr>';
                                }
                                /// total linea
                                $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['capital']=$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['otorgados']['capital'];
                                $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['capital']=Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de Pr�stamo']['capital']),2,'','.');
                                $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['capital']=Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Abono de Pr�stamos']['capital']),2,'','.');
                                $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['capital']=Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de N�mina']['capital']),2,'','.');
                                $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['interes']=Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de Pr�stamo']['interes']),2,'','.');
                                $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['interes']=Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['interes']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Abono de Pr�stamos']['interes']),2,'','.');
                                $detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['interes']=Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['interes']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de N�mina']['interes']),2,'','.');
                                /// Sub-Totales Departamentos
                                $totales_dep[$res_dep_print['codg_depn']]['otorgados']['capital']=Redondear(($totales_dep[$res_dep_print['codg_depn']]['otorgados']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['otorgados']['capital']),2,'','.');
                                $totales_dep[$res_dep_print['codg_depn']]['pagados']['capital']=Redondear(($totales_dep[$res_dep_print['codg_depn']]['pagados']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de Pr�stamo']['capital']),2,'','.');
                                $totales_dep[$res_dep_print['codg_depn']]['pagados']['interes']=Redondear(($totales_dep[$res_dep_print['codg_depn']]['pagados']['interes']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de Pr�stamo']['interes']),2,'','.');
                                $totales_dep[$res_dep_print['codg_depn']]['abonados']['capital']=Redondear(($totales_dep[$res_dep_print['codg_depn']]['abonados']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Abono de Pr�stamos']['capital']),2,'','.');
                                $totales_dep[$res_dep_print['codg_depn']]['abonados']['interes']=Redondear(($totales_dep[$res_dep_print['codg_depn']]['abonados']['interes']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Abono de Pr�stamos']['interes']),2,'','.');
                                $totales_dep[$res_dep_print['codg_depn']]['nomina']['capital']=Redondear(($totales_dep[$res_dep_print['codg_depn']]['nomina']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de N�mina']['capital']),2,'','.');
                                $totales_dep[$res_dep_print['codg_depn']]['nomina']['interes']=Redondear(($totales_dep[$res_dep_print['codg_depn']]['nominas']['interes']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de N�mina']['interes']),2,'','.');
                                $totales_dep[$res_dep_print['codg_depn']]['total']['capital']=Redondear(($totales_dep[$res_dep_print['codg_depn']]['total']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['capital']),2,'','.');
                                $totales_dep[$res_dep_print['codg_depn']]['total']['interes']=Redondear(($totales_dep[$res_dep_print['codg_depn']]['total']['interes']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['interes']),2,'','.');
                                /// Totales Generales
                                $totales_gen['otorgados']['capital']=Redondear(($totales_gen['otorgados']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['otorgados']['capital']),2,'','.');
                                $totales_gen['pagados']['capital']=Redondear(($totales_gen['pagados']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de Pr�stamo']['capital']),2,'','.');
                                $totales_gen['pagados']['interes']=Redondear(($totales_gen['pagados']['interes']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de Pr�stamo']['interes']),2,'','.');
                                $totales_gen['abonados']['capital']=Redondear(($totales_gen['abonados']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Abono de Pr�stamos']['capital']),2,'','.');
                                $totales_gen['abonados']['interes']=Redondear(($totales_gen['abonados']['interes']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Abono de Pr�stamos']['interes']),2,'','.');
                                $totales_gen['nomina']['capital']=Redondear(($totales_gen['nomina']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de N�mina']['capital']),2,'','.');
                                $totales_gen['nomina']['interes']=Redondear(($totales_gen['nominas']['interes']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de N�mina']['interes']),2,'','.');
                                $totales_gen['total']['capital']=Redondear(($totales_gen['total']['capital']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['capital']),2,'','.');
                                $totales_gen['total']['interes']=Redondear(($totales_gen['total']['interes']+$detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['interes']),2,'','.');

                                $html .= '<tr class="tablanomina ">
                                    <td align="left" class="etiquetas">&nbsp;'.$res_prest_print['nomb_tipo_pres'].'</td>
                                    <td align="right">'.Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['otorgados']['capital']),2,".",",").'&nbsp;</td>
                                    <td align="right">'.Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de Pr�stamo']['capital']),2,".",",").'&nbsp;</td>
                                    <td align="right">'.Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de Pr�stamo']['interes']),2,".",",").'&nbsp;</td>
                                    <td align="right">'.Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Abono de Pr�stamos']['capital']),2,".",",").'&nbsp;</td>
                                    <td align="right">'.Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Abono de Pr�stamos']['interes']),2,".",",").'&nbsp;</td>
                                    <td align="right">'.Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de N�mina']['capital']),2,".",",").'&nbsp;</td>
                                    <td align="right">'.Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Registro de N�mina']['interes']),2,".",",").'&nbsp;</td>
                                    <td align="right">'.Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['capital']),2,".",",").'&nbsp;</td>
                                    <td align="right">'.Redondear(($detalles[$res_dep_print['codg_depn']][$res_prest_print['codg_tipo_pres']]['Total']['interes']),2,".",",").'&nbsp;</td>
                                </tr>';
                                $dep_ant = $res_dep_print['codg_depn'];
                            }
                        }
                    }
$html .= '<tr class="etiquetas">
                                              <td align="right">SUB-TOTALES&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['otorgados']['capital'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['pagados']['capital'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['pagados']['interes'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['abonados']['capital'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['abonados']['interes'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['nomina']['capital'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['nomina']['interes'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['total']['capital'],2,".",",").'&nbsp;</td>
                                              <td align="right">'.Redondear($totales_dep[$dep_ant]['total']['interes'],2,".",",").'&nbsp;</td>
                                            </tr>';
$html .= '<tr align="center" class="total_prest" >
                    <td align="right">T O T A L E S&nbsp;</td>
                    <td align="right">'.Redondear($totales_gen['otorgados']['capital'],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_gen['pagados']['capital'],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_gen['pagados']['interes'],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_gen['abonados']['capital'],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_gen['abonados']['interes'],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_gen['nomina']['capital'],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_gen['nomina']['interes'],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_gen['total']['capital'],2,'.',',').'&nbsp;</td>
                    <td align="right">'.Redondear($totales_gen['total']['interes'],2,'.',',').'&nbsp;</td>
                </tr>
             </table>';
$html .= '</body></html>';
//echo $html;
generar_pdf($html,'Prestamos_'.$idprint3.'-'.$idprint2.'.pdf','letter','portrait');
/////// boton de imprimir
//$ancho_div_boton = "50%";
//include('../comunes/imprimir.php');?>
