<?php 
include('../comunes/conexion_basedatos.php'); 
include ('../comunes/comprobar_inactividad.php');
$print_pdf='SI';
include ('../comunes/formularios_funciones.php');
include ('../comunes/numerosaletras.php');
include ('generarpdf.php');
 
$codg_pago=$_GET['codg_pago'];

$consulta_empresa= mysql_query("SELECT   * from emp_empresa  ");
$con_emp=mysql_fetch_assoc($consulta_empresa);

//// consultar el abono
$sql_abono = "SELECT * FROM abonos WHERE codg_pago = ".$codg_pago;
$bus_abono = mysql_query($sql_abono);
$con_abono = mysql_fetch_assoc($bus_abono);
$monto_abono = $con_abono[mont_pago];
$montoletras=convertir_a_letras($monto_abono,'');
$montoletras=strtoupper($montoletras);

//// Consultar los datos del prestamo
$sql_prst = "SELECT * FROM prestamos p, tipo_prestamos tp WHERE codg_prst = ".$con_abono[codg_prst]." AND p.codg_tipo_pres=tp.codg_tipo_pres";
$bus_prst = mysql_query($sql_prst);
$con_prst = mysql_fetch_assoc($bus_prst);

//// consultar el saldo del prestamo para determinar si fue parcial o total
$sql_sal = "SELECT *, SUM(capi_prtm) as pagado FROM  prestamos_mov WHERE  codg_prst = ".$con_prst[codg_prst]." AND freg_prtm<='".$con_abono[fcha_pago]."'";
$bus_sal = mysql_query($sql_sal);
$con_sal = mysql_fetch_assoc($bus_sal);
$saldo = $con_prst[mont_apro] - $con_sal[pagado];
if ($saldo > 0 ){
	$tipo_abono = 'parcial';
}else{
	$tipo_abono = 'total';
}

//// consultar los montos de capital y prestamo que se pagan con el abono
$sql_mov = "SELECT * FROM prestamos_mov WHERE codg_prst=".$con_prst[codg_prst]." AND orgn_prtm='Abono a prestamo' AND rela_prtm=".$codg_pago;
$bus_mov = mysql_query($sql_mov);
$con_mov = mysql_fetch_assoc($bus_mov);
$monto_capital = $con_mov[capi_prtm];
$capitalletras=convertir_a_letras($monto_capital,'');
$capitalletras=strtoupper($capitalletras);
$monto_interes = $con_mov[inte_prtm];
$interesletras=convertir_a_letras($monto_interes,'');
$interesletras=strtoupper($interesletras);

//// consultar los datos del socio
$sql_pers = "SELECT * FROM socios WHERE cedu_soci = ".$con_abono['cedu_soci'];
$bus_pers = mysql_query($sql_pers);
$con_pers = mysql_fetch_assoc($bus_pers);
$nacionalidad=$con_pers[naci_soci];
if ($nacionalidad=='V')
{
	$naci='Venezolano(a)';
}
else 
{
	$naci='Extrajero(a)';
}

$consulta_presi= mysql_query("SELECT   * from vista_miembros_junta_direct  WHERE codg_tmie='1' and fchi_junt<='$fecha' and fchf_junt>='$fecha'  ");
$con_pre=mysql_fetch_assoc($consulta_presi);


$consulta_tesorero= mysql_query("SELECT   * from vista_miembros_junta_direct  WHERE codg_tmie='2' and fchi_junt<='$fecha' and fchf_junt>='$fecha' ");
$con_tes=mysql_fetch_assoc($consulta_tesorero);



//evaluar desempe�o si es jubilido o no para establecer el cargo

if ($con['codg_stat']==3)
{
	$cargo='Jubilado(a)';	
}
else 
{
  $cargo=$con[carg_dlab];
}

//fecha
$dia=date(d);
$mes=date(m);
$mesa=date(m);
$ano=date(Y);
$mes=intval($mes);

// para verificar si cuenta con fiadores
$sql_fia = "SELECT * FROM prestamos_fia pf, socios s WHERE pf.codg_prst = ".$codg_prst. " AND pf.orig_fian = 'fiador' AND pf.cedu_soci = s.cedu_soci";
$bus_fia = mysql_query($sql_fia);
$cuenta_fiador = 0;
while($reg_fia = mysql_fetch_array($bus_fia)){
	$cuenta_fiador+=1;
	$detalle_fiador[$cuenta_fiador][nombre] = $reg_fia[apel_soci].'&nbsp;'.$reg_fia[nomb_soci];
	$detalle_fiador[$cuenta_fiador][cedula] = $reg_fia[cedu_soci];
	$detalle_fiador[$cuenta_fiador][monto] = $reg_fia[mont_fian];
}

// para verificar si cuenta con fianza
$sql_fia = "SELECT * FROM prestamos_fia pf WHERE pf.codg_prst = ".$codg_prst. " AND pf.orig_fian = 'fianza'";
$bus_fia = mysql_query($sql_fia);
$cuenta_fianza = 0;
while($reg_fia = mysql_fetch_array($bus_fia)){
	$cuenta_fianza+=1;
	$detalle_fianza[$cuenta_fianza][nombre] = $reg_fia[desc_fian];
	$detalle_fianza[$cuenta_fianza][monto] = $reg_fia[mont_fian];
}

/// Para verificar si paga otros prestamos
$sql_ppp='SELECT m.*, p.*, (select nomb_tipo_pres from tipo_prestamos where codg_tipo_pres=p.codg_tipo_pres) as nomb_tipo_pres FROM prestamos_mov m LEFT JOIN prestamos p ON p.codg_prst=m.codg_prst WHERE m.orgn_prtm="Pagado por prestamo" AND m.rela_prtm="'.$codg_prst.'"';
$busq_ppp=mysql_query($sql_ppp);
$cuenta_ppp = 0;
while ($reg_ppp = mysql_fetch_array($busq_ppp)){
	$cuenta_ppp += 1;
	$ppp[$cuenta_ppp][nompre] = $reg_ppp[nomb_tipo_pres];
	$ppp[$cuenta_ppp][fecha] = $reg_ppp[fcha_acta];
	$ppp[$cuenta_ppp][monto] = $reg_ppp[mnto_prtm];
	$ppp[$cuenta_ppp][capital] = $reg_ppp[capi_prtm];
	$ppp[$cuenta_ppp][interes] = $reg_ppp[inte_prtm];
}

    //////////////// Preparamos el pie de reporte
    $sql_pie = "SELECT * FROM emp_empresa";
    $row_pie = mysql_fetch_array(mysql_query($sql_pie));
    $direccion = $row_pie['dirc_empr'];
    $telefono = $row_pie['tlfn_empr'];
    $celular = $row_pie['celu_empr'];
    $mail = $row_pie['mail_empr'];
    $twit = $row_pie['twit_empr'];
    if ($telefono!='') { $telefono= '<b>Tel�fono:</b> '.$telefono; }
    if ($celular!='') { $celular= '<b>Celular:</b> '.$celular; }
    if ($twit!='') { $twit= '<b>Twitter:</b> <a href="http://twitter.com/#!/'.$twit.'">'.$twit; }
    if ($mail!='') { $mail= '<b>e-mail:</b> <a href="mailto:'.$mail.'">'.$mail.'</a>'; }
    $msg_pie_carta ='
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablanomina" bordercolor="#FFFFFF">
        <tr>
            <td align="center"><hr></td>
        </tr>
        <tr>
            <td align="center">
                <b>Direcci�n:</b> '.$direccion.'<br>'.$telefono.' '.$celular.' '.$mail.' '.$twit.'. 
            </td>
        </tr>
    </table>';
    /////////////////////////////////////////////////


$html="<html>
<style type='text/css' >
<!--
.etiqueta {
	color: #000000;
	font-size: 14px;
	 
}
.resultado {
	font-size: 14px;
	color: #000000;
	font-weight: bold;
	
}

.resultado_titulo {
	font-size: 10px;
	color: #000000;
	font-weight: bold;
	 
	
}

.titulo {
	font-size: 18px;
	font-weight: bold;
	
	
body{
        background-color: #FFFFFF;
        font-family: arial;
        margin-left: 0px;
        margin-top: 170px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
}
#header,
#footer {
    position: fixed;  
    left: 0;
    right: 0;
}

#header {
    top: 0;
	border-bottom: 0.1pt solid #aaa;
}

#footer {
  font-size: 12px;
  bottom: 10px;
  border-top: 0.1pt solid #aaa;
}

-->
</style>
<head> </head>
<body>
</br>
</br>
<table width='100%' > 
	<tr> 
		<td width='250px' align='left' > 
			<img src='../imagenes/logo_report.jpg'>  
		</td> 
	  	<td align='left' class='resultado_titulo'>".$con_emp[nomb_empr] ." ".$con_emp[regi_empr] .", Rif N� ".$con_emp[nrif_empr]." Tel�fono: ".$con_emp[tlfn_empr]." </td> 
	</tr> 
</table>

<table width='100%'  align='center' >  
<tr> <td colspan='3' align='justify'  > La ".$con_emp[nomb_empr]."
 ".$con_emp[dimi_empr].", ".$con_emp[temp_empr]." con domicilio en ".$con_emp[ciud_empr].", ".$con_emp[regi_empr].", HACE CONSTAR, que ha recibido del ciudadano(a) <span class='resultado'>".$con_pers[nomb_soci]." ".$con_pers[apel_soci].", </span> mayor de edad,
<span class='resultado'>".$naci." </span> , domiciliado(a) en <span class='resultado'> ".$con_pers[ciud_soci]." </span> y titular de la C�dula de Identidad N�mero
<span class='resultado'>".redondear($con_pers[cedu_soci],0,'.','')."</span>; la cantidad de: <span class='resultado'>".$montoletras." (Bs. ".redondear($con_abono[mont_pago],2,'.',',').")</span>, como <span class='resultado'>abono ".$tipo_abono."</span> del <span class='resultado'>".$con_prst[nomb_tipo_pres]."</span>, otorgado el <span class='resultado'>".ordenar_fecha($con_prst[fcha_acta])."</span>. Este abono representa ".$capitalletras." (Bs. ".redondear($monto_capital,2,'.',',').") de Capital y ".$interesletras." (Bs. ".redondear($monto_interes,2,'.',',').") de Inter�s.";

if ($cuenta_fiador > 0 || $cuenta_fianza > 0){
	$html.= "En el otorgamiento del presente prestamo ";
}
if ($cuenta_fiador > 0){
	$html .= "Sirve(n) de fiador(es):";
	for($i=1;$i<=$cuenta_fiador;$i++){
		$html.=$detalle_fiador[$i][nombre].", titular de la c�dula de identidad ".redondear($detalle_fiador[$i][cedula],0,'.','')." por un monto de Bs. ".redondear($detalle_fiador[$i][monto],2,'.',',');
		if ($i < $cuenta_fiador){
			$html .= "; ";
		}
		else {
			$html.= ". ";
		}
	}
}
if ($cuenta_fianza > 0){
	$html .= "Se recibe(n) como garant�a(s): ";
	for($i=1;$i<=$cuenta_fianza;$i++){
		$html.=$detalle_fianza[$i][nombre].", por un monto de Bs. ".redondear($detalle_fianza[$i][monto],2,'.',',');
		if ($i < $cuenta_fianza){
			$html .= "; ";
		}
		else {
			$html.= ". ";
		}
	}
}
if ($cuenta_ppp > 0){
	$total_ppp = 0;
	$html .= "Del monto otorgado se cancela(n) deuda(s) de ";
	for($i=1;$i<=$cuenta_ppp;$i++){
		$html.= " Bs. ".redondear($ppp[$i][monto],2,'.',',')." (Bs. ".redondear($ppp[$i][capital],2,'.',',')." de Capital y Bs. ".redondear($ppp[$i][interes],2,'.',',')." de Inter�s) por ".$ppp[$i][nompre].", otorgado el ".ordenar_fecha($ppp[$i][fecha]);
		$total_ppp += $ppp[$i][monto];
		if ($i < $cuenta_ppp){
			$html .= "; ";
		}
		else {
			$html.= ". ";
		}
	}
	$html .= 'Lo que representa un total de Bs. '.redondear($total_ppp,2,".",".").' en deduciones. ';
	$html .= 'Luego de las deducciones el cheque ser� emitido por la cantidad de: ';
	$monto_final = redondear($mont_apro-$total_ppp,2,'','.');
	$nuevomontoletras=convertir_a_letras($monto_final,'');
	$html .= '<b>'.strtoupper($nuevomontoletras);
	$html .= ' (Bs. '.redondear($monto_final,2,".",",").')</b>.';
}
$html.="</td>  
</tr>
</table>";
if ($con[obsr_prst]) {
	$html.="<table width='100%'  align='center'>
	<tr> <td>  Observaciones: <span class='resultado'>".$con[obsr_prst]." <span> </td> </tr>
	</table>
	<table width='98%'  align='center'>
	<tr> <td> As� lo decidimos y firmamos a los <span class='resultado'>".strtoupper(decena($dia))."</span> d�as del mes de <span class='resultado'>".strtoupper(convertir_mes($mes))."</span> de <span class='resultado'>".strtoupper(mil($ano))." </span> (".$dia."/".$mesa."/".$ano.").  </td> </tr>
	</table>
	<br>";
}
$html.="<br><table width='100%'  align='center'>
<tr> <td widht='10%'>&nbsp; </td> <td   align='center'><u>".$con[titu_soci]." ".$con[nomb_soci]." ".$con[apel_soci]."</u>   </td> <td widht='10%'>&nbsp; </td> <td   align='center' > <u> ".$con_pre[titu_soci]." ".$con_pre[nomb_soci]." ".$con_pre[apel_soci]."</u></td> <td widht='10%'>&nbsp; </td>   </tr>
<tr> <td widht='10%'>&nbsp; </td> <td align='center' > BENEFICIARIO </td> <td widht='10%'>&nbsp; </td> <td   align='center' > PRESIDENTE </td> <td widht='10%'>&nbsp; </td>   </tr></table>
<table width='53%'  align='left'><tr> <td widht='10%'>&nbsp; </td> <td align='center'> C�dula N�:  </td> <td widht='10%'>&nbsp; </td> <td> &nbsp; </td> <td widht='10%'>&nbsp; </td>   </tr> </table>
<br>
<br>
<table width='100%'  align='center'>  
<tr><td align='center'><u> ".$con_tes[titu_soci]." ".$con_tes[nomb_soci]." ".$con_tes[apel_soci]." </u></td> </tr>
<tr><td align='center'>TESORERO </td> </tr>
</table>
";
if ($cuenta_fiador > 0){
	$html .= '<br><b><u>Fiadores:</u></b><br>';
	for($i=1;$i<=$cuenta_fiador;$i++){
		$html.=$detalle_fizador[$i][nombre]."<br> C.I. ".redondear($detalle_fiador[$i][cedula],0,'.','')."<BR><BR>";
	}
}
$html.= '<div id="footer">
  <div class="page-number">'.$msg_pie_carta.'</div>
</div>'; 
//echo $html;
generar_pdf($html,'acta_prestamo.pdf','letter','portrait');
?>
