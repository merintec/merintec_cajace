DELIMITER $$
CREATE TRIGGER `actualizar_detalle` AFTER UPDATE ON `nominas_detalle`
 FOR EACH ROW BEGIN
	IF (NEW.moti_dlle = 'Prestamo') THEN
		IF (NEW.codg_pago) THEN
			SET @fecha_pago = (SELECT fcha_pago FROM dependencias_pagos WHERE codg_pago = NEW.codg_pago);
		ELSE 
			SET @fecha_pago = '0000-00-00';
		END IF;
	UPDATE prestamos_mov SET fpag_prtm = @fecha_pago WHERE orgn_prtm = 'Registro de Nómina' AND rela_prtm = OLD.codg_dlle;
	END IF;

	IF (NEW.moti_dlle = 'Aporte' OR NEW.moti_dlle = 'Retencion' OR NEW.moti_dlle = 'Reintegro') THEN
		IF (NEW.moti_dlle = 'Aporte') THEN SET @tipo = 'A'; END IF;
 		IF (NEW.moti_dlle = 'Retencion') THEN SET @tipo = 'R'; END IF;
 		IF (NEW.moti_dlle = 'Reintegro') THEN SET @tipo = 'I'; END IF;
		IF (NEW.codg_pago) THEN
			SET @test = ''; 
			ELSE 
				SET @tipo = (SELECT CONCAT(@tipo,'P'));
		END IF;
		UPDATE socios_movimientos SET dest_movi = @tipo WHERE codg_dlle = NEW.codg_dlle;
	END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `eliminar_detalle` AFTER DELETE ON `nominas_detalle`
 FOR EACH ROW BEGIN
	IF (OLD.moti_dlle = 'Prestamo') THEN
		DELETE FROM prestamos_mov WHERE orgn_prtm = 'Registro de Nómina' AND rela_prtm = OLD.codg_dlle;
	END IF;
	IF (OLD.moti_dlle = 'Aporte' OR OLD.moti_dlle = 'Retencion' OR OLD.moti_dlle = 'Reintegro') THEN
		DELETE FROM socios_movimientos WHERE codg_dlle = OLD.codg_dlle;
	END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insertar_detalle` AFTER INSERT ON `nominas_detalle`
 FOR EACH ROW BEGIN
	IF (NEW.moti_dlle = 'Prestamo') THEN
		SET @codg_prst = NEW.rela_dlle;
		SET @mnto_prtm = NEW.mnto_dlle;
		SET @tipo_nomina = (SELECT prdo_nmna FROM nominas WHERE codg_nmna = NEW.codg_nmna);
		SET @periodo = (SELECT prdo_nmna as semana FROM nominas WHERE codg_nmna = NEW.codg_nmna);
		SET @anno = (SELECT anno_nmna as semana FROM nominas WHERE codg_nmna = NEW.codg_nmna);
		SET @mees = (SELECT mess_nmna as semana FROM nominas WHERE codg_nmna = NEW.codg_nmna);
		SET @fecha = (SELECT CONCAT(@anno,'-',@mees,'-15'));
		IF (@tipo_nomina = 6) THEN
			SET @factor = 2;
			SET @fecha_registro = (SELECT CONCAT(@anno,'-',@mees,'-15'));
		END IF;
		IF (@tipo_nomina = 7) THEN
			SET @factor = 2;
			SET @fecha_registro = (Select LAST_DAY(@fecha));
		END IF;
		IF (@tipo_nomina = 8) THEN
			SET @factor = 1;
			SET @fecha_registro = (SELECT LAST_DAY(@fecha));
		END IF;
		IF (@tipo_nomina >= 11) THEN
			SET @factor = 4;
			SET @dia = (SELECT CONCAT(@anno,'-01-01'));
			SET @periodo = @periodo - 10;
			SET @fecha_registro = (SELECT DATE_ADD(@dia, INTERVAL (7-DAYOFWEEK(@dia)) DAY) AS "ultimoDiaSemana");
			SET @fecha_registro = (SELECT DATE_ADD(@fecha_registro, INTERVAL -1 DAY));
			SET @fecha_registro = (SELECT DATE_ADD(@fecha_registro, INTERVAL (@periodo-1) WEEK));
		END IF;
		set @fecha_registro = cast(@fecha_registro as datetime);
		SET @pagado = (SELECT IF (SUM(capi_prtm)>0, SUM(capi_prtm), 0) FROM prestamos_mov WHERE codg_prst = @codg_prst AND freg_prtm <= @fecha_registro);
		SET @interes = (SELECT mont_intr FROM prestamos WHERE codg_prst = @codg_prst);
		SET @otorgado = (SELECT mont_apro FROM prestamos WHERE codg_prst = @codg_prst);
		SET @monto_deuda = @otorgado - @pagado;

		SET @monto_interes = ((@monto_deuda * (@interes/360*30)/100)/@factor);
		SET @inte_prtm = @monto_interes;
		SET @capi_prtm = @mnto_prtm - @inte_prtm;
		SET @orgn_prtm = 'Registro de Nómina';
		SET @rela_prtm = NEW.codg_dlle;
		SET @freg_prtm = @fecha_registro;
		SET @fecha_pago = '0000-00-00';
		IF (NEW.codg_pago) THEN
			SET @fecha_pago = (SELECT fcha_pago FROM dependencias_pagos WHERE codg_pago = NEW.codg_pago);
		END IF;
		SET @fpag_prtm = @fecha_pago;
		INSERT INTO prestamos_mov (codg_prtm, codg_prst, mnto_prtm, capi_prtm, inte_prtm, orgn_prtm, rela_prtm, freg_prtm, fpag_prtm) VALUES (NULL, @codg_prst, @mnto_prtm, @capi_prtm, @inte_prtm, @orgn_prtm, @rela_prtm, @freg_prtm, @fpag_prtm);
	END IF;

		IF (NEW.moti_dlle = 'Aporte' OR NEW.moti_dlle = 'Retencion' OR NEW.moti_dlle = 'Reintegro') THEN
		IF (NEW.moti_dlle = 'Aporte') THEN SET @tipo = 'A'; END IF;
 		IF (NEW.moti_dlle = 'Retencion') THEN SET @tipo = 'R'; END IF;
 		IF (NEW.moti_dlle = 'Reintegro') THEN SET @tipo = 'I'; END IF;
		IF (NEW.codg_pago) THEN
			SET @test = ''; 
			ELSE 
				SET @tipo = (SELECT CONCAT(@tipo,'P'));
		END IF;
		SET @tipo_nomina = (SELECT prdo_nmna FROM nominas WHERE codg_nmna = NEW.codg_nmna);
		SET @periodo = (SELECT prdo_nmna as semana FROM nominas WHERE codg_nmna = NEW.codg_nmna);
		SET @anno = (SELECT anno_nmna as semana FROM nominas WHERE codg_nmna = NEW.codg_nmna);
		SET @mees = (SELECT mess_nmna as semana FROM nominas WHERE codg_nmna = NEW.codg_nmna);
		SET @fecha = (SELECT CONCAT(@anno,'-',@mees,'-15'));
		IF (@tipo_nomina = 6) THEN
			SET @fecha_registro = (SELECT CONCAT(@anno,'-',@mees,'-15'));
			SET @concepto = (SELECT CONCAT('Nómina ', @mees,'/',@anno, '(1º Quincena)'));
		END IF;
		IF (@tipo_nomina = 7) THEN
			SET @fecha_registro = (Select LAST_DAY(@fecha));
			SET @concepto = (SELECT CONCAT('Nómina ', @mees,'/',@anno, '(2º Quincena)'));
		END IF;
		IF (@tipo_nomina = 8) THEN
			SET @fecha_registro = (SELECT LAST_DAY(@fecha));
			SET @concepto = (SELECT CONCAT('Nómina ', @mees,'/',@anno));
		END IF;
		IF (@tipo_nomina >= 11) THEN
			SET @semana = @periodo - 10;
			SET @fecha = (SELECT CONCAT(@anno,'-07-01'));
			SET @fecha_registro = (SELECT TIMESTAMPADD(DAY,(@periodo-WEEKDAY(MAKEDATE(YEAR(@fecha),(@periodo*7)))),MAKEDATE(YEAR(@fecha),(@periodo*7))) as UltimoDiaSemana);
			SET @concepto = (SELECT CONCAT('Nómina ', @mees,'/',@anno, '(',@semana,'º Semana)'));
		END IF;
		SET @cedu_soci = NEW.cedu_soci;
		SET @fcha_movi = @fecha_registro;
		SET @conc_movi = @concepto;
		SET @tipo_movi = 'I';
		SET @dest_movi = @tipo;
		SET @mont_movi = NEW.mnto_dlle;
		SET @orig_movi = 'A';
		SET @codg_dlle = NEW.codg_dlle;
		INSERT INTO socios_movimientos (cedu_soci, fcha_movi, conc_movi, tipo_movi, dest_movi, mont_movi, orig_movi, codg_dlle) VALUES (@cedu_soci, @fcha_movi, @conc_movi, @tipo_movi, @dest_movi, @mont_movi, @orig_movi, @codg_dlle);
	END IF;
END
$$
DELIMITER ;





DELIMITER $$
CREATE TRIGGER `eliminar_detalle_retroactivos` AFTER DELETE ON `nominas_retroactivos_detalles`
 FOR EACH ROW BEGIN
	IF (OLD.dest_nr_dlle = 'Aporte' OR OLD.dest_nr_dlle = 'Retencion') THEN
		DELETE FROM socios_movimientos WHERE codg_dlle_retr = OLD.codg_nr_dlle;
	END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insertar_detalle_retroactivos` AFTER INSERT ON `nominas_retroactivos_detalles`
 FOR EACH ROW BEGIN
IF (NEW.dest_nr_dlle = 'Aporte' OR NEW.dest_nr_dlle = 'Retencion') THEN
		SET @fecha_registro = (SELECT fcha_retr FROM nominas_retroactivos WHERE codg_retr = NEW.codg_retr);

		SET @concepto = (SELECT CONCAT ('Retroactivo: ',desc_retr) FROM nominas_retroactivos WHERE codg_retr = NEW.codg_retr);

		IF (NEW.dest_nr_dlle = 'Aporte') THEN SET @tipo = 'A'; END IF;
 		IF (NEW.dest_nr_dlle = 'Retencion') THEN SET @tipo = 'R'; END IF;
		
		SET @cedu_soci = NEW.cedu_soci;
		SET @fcha_movi = @fecha_registro;
		SET @conc_movi = @concepto;
		SET @tipo_movi = 'I';
		SET @dest_movi = @tipo;
		SET @mont_movi = NEW.mont_nr_dlle;
		SET @orig_movi = 'A';
		SET @codg_dlle_retr = NEW.codg_nr_dlle;
		INSERT INTO socios_movimientos (cedu_soci, fcha_movi, conc_movi, tipo_movi, dest_movi, mont_movi, orig_movi, codg_dlle_retr) VALUES (@cedu_soci, @fcha_movi, @conc_movi, @tipo_movi, @dest_movi, @mont_movi, @orig_movi, @codg_dlle_retr);
	END IF;
END
$$
DELIMITER ;


-- Triggers `socios_movimientos`
--
DELIMITER $$
CREATE TRIGGER `Actualizar_movimiento` AFTER UPDATE ON `socios_movimientos`
 FOR EACH ROW BEGIN
	IF (NEW.cedu_soci <> OLD.cedu_soci) THEN
		SELECT cedu_soci, 
			(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_ing, 
			(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_egr, 
			(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_ing, 
			(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_egr, 
			(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_ing, 
			(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_egr, 
			(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='AP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_p_ing,
			(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='RP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_p_ing, 
			(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='IP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_p_ing 
			INTO @a, @b, @c, @d, @e, @f, @g, @h, @i, @j FROM socios_movimientos sm WHERE cedu_soci=NEW.cedu_soci GROUP BY sm.cedu_soci ORDER BY codg_movi;

		SET @fcha_sald = (SELECT date_format(NOW(),'%Y/%m/%d'));
		SET @cedu_soci = @a;
		SET @apor_sald = IFNULL(@b,0) - IFNULL (@c,0);
		SET @rete_sald = IFNULL(@d,0) - IFNULL (@e,0);
		SET @rein_sald = IFNULL(@f,0) - IFNULL (@g,0);
		SET @apor_comp = IFNULL(@h,0);
		SET @rete_comp = IFNULL(@i,0);
		SET @rein_comp = IFNULL(@j,0); 
		UPDATE socios_saldo SET fcha_sald = '@fcha_sald', apor_sald = '@apor_sald', rete_sald = '@rete_sald', rein_sald = '@rein_sald', apor_comp = '@apor_comp', rete_comp = '@rete_comp', rein_comp = '@rein_comp' WHERE cedu_soci = '@cedu_soci';
	END IF;
	SELECT cedu_soci, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_egr, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_egr, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_egr, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='AP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_p_ing,
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='RP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_p_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='IP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_p_ing 
		INTO @a, @b, @c, @d, @e, @f, @g, @h, @i, @j FROM socios_movimientos sm WHERE cedu_soci=OLD.cedu_soci GROUP BY sm.cedu_soci ORDER BY codg_movi;

	SET @fcha_sald = (SELECT date_format(NOW(),'%Y/%m/%d'));
	SET @cedu_soci = @a;
	SET @apor_sald = IFNULL(@b,0) - IFNULL (@c,0);
	SET @rete_sald = IFNULL(@d,0) - IFNULL (@e,0);
	SET @rein_sald = IFNULL(@f,0) - IFNULL (@g,0);
	SET @apor_comp = IFNULL(@h,0);
	SET @rete_comp = IFNULL(@i,0);
	SET @rein_comp = IFNULL(@j,0); 
	UPDATE socios_saldo SET fcha_sald = '@fcha_sald', apor_sald = '@apor_sald', rete_sald = '@rete_sald', rein_sald = '@rein_sald', apor_comp = '@apor_comp', rete_comp = '@rete_comp', rein_comp = '@rein_comp' WHERE cedu_soci = '@cedu_soci';
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `eliminar_movimiento` AFTER DELETE ON `socios_movimientos`
 FOR EACH ROW BEGIN
	SELECT cedu_soci, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_egr, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_egr, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_egr, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='AP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_p_ing,
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='RP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_p_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='IP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_p_ing 
		INTO @a, @b, @c, @d, @e, @f, @g, @h, @i, @j FROM socios_movimientos sm WHERE cedu_soci=OLD.cedu_soci GROUP BY sm.cedu_soci ORDER BY codg_movi;

	SET @fcha_sald = (SELECT date_format(NOW(),'%Y/%m/%d'));
	SET @cedu_soci = @a;
	SET @apor_sald = IFNULL(@b,0) - IFNULL (@c,0);
	SET @rete_sald = IFNULL(@d,0) - IFNULL (@e,0);
	SET @rein_sald = IFNULL(@f,0) - IFNULL (@g,0);
	SET @apor_comp = IFNULL(@h,0);
	SET @rete_comp = IFNULL(@i,0);
	SET @rein_comp = IFNULL(@j,0); 
	UPDATE socios_saldo SET fcha_sald = '@fcha_sald', apor_sald = '@apor_sald', rete_sald = '@rete_sald', rein_sald = '@rein_sald', apor_comp = '@apor_comp', rete_comp = '@rete_comp', rein_comp = '@rein_comp' WHERE cedu_soci = '@cedu_soci';
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insertar_movimiento` AFTER INSERT ON `socios_movimientos`
 FOR EACH ROW BEGIN
	SELECT cedu_soci, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='A' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_egr, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='R' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_egr, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='I' AND tipo_movi='E' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_egr, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='AP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as apr_p_ing,
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='RP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as ret_p_ing, 
		(SELECT SUM(mont_movi) FROM socios_movimientos WHERE dest_movi='IP' AND tipo_movi='I' AND cedu_soci=sm.cedu_soci GROUP BY cedu_soci ) as rei_p_ing 
		INTO @a, @b, @c, @d, @e, @f, @g, @h, @i, @j FROM socios_movimientos sm WHERE cedu_soci=NEW.cedu_soci GROUP BY sm.cedu_soci ORDER BY codg_movi;

	SET @fcha_sald = (SELECT date_format(NOW(),'%Y/%m/%d'));
	SET @cedu_soci = @a;
	SET @apor_sald = IFNULL(@b,0) - IFNULL (@c,0);
	SET @rete_sald = IFNULL(@d,0) - IFNULL (@e,0);
	SET @rein_sald = IFNULL(@f,0) - IFNULL (@g,0);
	SET @apor_comp = IFNULL(@h,0);
	SET @rete_comp = IFNULL(@i,0);
	SET @rein_comp = IFNULL(@j,0); 

	UPDATE socios_saldo SET fcha_sald = '@fcha_sald', apor_sald = '@apor_sald', rete_sald = '@rete_sald', rein_sald = '@rein_sald', apor_comp = '@apor_comp', rete_comp = '@rete_comp', rein_comp = '@rein_comp' WHERE cedu_soci = '@cedu_soci';

END
$$
DELIMITER ;

