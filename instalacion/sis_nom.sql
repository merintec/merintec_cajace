-- MySQL dump 10.13  Distrib 5.1.49, for debian-linux-gnu (i486)
--
-- Host: localhost    Database: cont_sis_nom_cont
-- ------------------------------------------------------
-- Server version	5.1.49-3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actividades`
--

DROP TABLE IF EXISTS `actividades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actividades` (
  `cod_act` varchar(5) NOT NULL COMMENT 'codigo de la Actividad',
  `nom_act` varchar(150) NOT NULL COMMENT 'nombre de la actividad',
  PRIMARY KEY (`cod_act`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actividades`
--

LOCK TABLES `actividades` WRITE;
/*!40000 ALTER TABLE `actividades` DISABLE KEYS */;
/*!40000 ALTER TABLE `actividades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anular_form`
--

DROP TABLE IF EXISTS `anular_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anular_form` (
  `cod_anl` int(11) NOT NULL AUTO_INCREMENT,
  `fch_anl` date NOT NULL,
  `tip_anl` varchar(1) NOT NULL,
  `num_anl` int(5) unsigned zerofill NOT NULL,
  `mot_anl` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_anl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los Formularios Anulados';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anular_form`
--

LOCK TABLES `anular_form` WRITE;
/*!40000 ALTER TABLE `anular_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `anular_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignaciones`
--

DROP TABLE IF EXISTS `asignaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignaciones` (
  `cod_cnp` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la asignacion',
  `con_cnp` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `ncuo_cnp` int(3) NOT NULL COMMENT 'Numero de cuotas de la asignacion',
  `per_cnp` varchar(1) NOT NULL COMMENT 'Permanencia de la asignacion',
  `mcuo_cnp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la asignacion',
  `por_cnp` int(3) NOT NULL COMMENT 'Porcentaje de asignacion',
  `bpor_cnp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_con` int(11) NOT NULL COMMENT 'Codigo del tipo de concesion',
  `ced_per` varchar(12) NOT NULL COMMENT 'La cedula de la persona',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_cnp` date NOT NULL COMMENT 'Fecha de Registro de la asignacion',
  `ncp_cnp` int(3) NOT NULL COMMENT 'N�mero de cuotas pagadas',
  `des_cnp` varchar(1) NOT NULL COMMENT 'Aplica para Descuentos de ley',
  PRIMARY KEY (`cod_cnp`),
  KEY `cod_con` (`cod_con`),
  KEY `ced_per` (`ced_per`),
  CONSTRAINT `asignaciones_ibfk_1` FOREIGN KEY (`cod_con`) REFERENCES `concesiones` (`cod_con`) ON UPDATE CASCADE,
  CONSTRAINT `asignaciones_ibfk_2` FOREIGN KEY (`ced_per`) REFERENCES `personal` (`ced_per`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignaciones`
--

LOCK TABLES `asignaciones` WRITE;
/*!40000 ALTER TABLE `asignaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `asignaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auditoria`
--

DROP TABLE IF EXISTS `auditoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditoria` (
  `cod_aud` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C�digo de Auditor�a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C�digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci�n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci�n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu� la acci�n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci�n',
  `hora` time NOT NULL COMMENT 'hora de la acci�n',
  PRIMARY KEY (`cod_aud`),
  KEY `cod_usr` (`cod_usr`),
  CONSTRAINT `auditoria_ibfk_1` FOREIGN KEY (`cod_usr`) REFERENCES `usuarios` (`cod_usr`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=484 DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditoria`
--

LOCK TABLES `auditoria` WRITE;
/*!40000 ALTER TABLE `auditoria` DISABLE KEYS */;
INSERT INTO `auditoria` VALUES (1,'12345678','insertar','cod_usr=6343267;nom_usr=Luz Marina ;ape_usr=Salcedo ;freg_usr=2011-01-01;log_usr=luzmarina;pas_usr=luzmari;cod_grp=2;fcam_usr=2011-04-18;sts_usr=A','usuarios','2011-04-18','14:22:00'),(2,'12345678','insertar','cod_usr=15031097;nom_usr=Luis Felipe;ape_usr=Márquez Briceño;freg_usr=2011-01-01;log_usr=felix;pas_usr=C0ntr4;cod_grp=2;fcam_usr=2011-04-18;sts_usr=A','usuarios','2011-04-18','14:39:00'),(3,'12345678','insertar','nom_grp=Pasantes;des_grp=Privilegios Mínimos','grupos','2011-04-18','14:40:00'),(4,'15031097','insertar','cod_dep=DSC;nom_dep=Despacho del Contralor;cod_act=;cod_pro=','dependencias','2011-04-18','14:41:00'),(5,'15031097','insertar','cod_dep=ADM;nom_dep=Administración;cod_act=;cod_pro=','dependencias','2011-04-18','14:42:00'),(6,'15031097','insertar','cod_dep=UAD;nom_dep=Unidad de Auditoría;cod_act=;cod_pro=','dependencias','2011-04-18','14:43:00'),(7,'15031097','insertar','cod_dep=SCR;nom_dep=Secretaría;cod_act=;cod_pro=','dependencias','2011-04-18','14:43:00'),(8,'15031097','insertar','cod_dep=SLT;nom_dep=Sala Técnica;cod_act=;cod_pro=','dependencias','2011-04-18','14:45:00'),(9,'15031097','insertar','cod_dep=SRJ;nom_dep=SERVICIO JURÍDICO;cod_act=;cod_pro=','dependencias','2011-04-18','14:45:00'),(10,'15031097','insertar','cod_dep=CBN;nom_dep=Control de Bienes;cod_act=;cod_pro=','dependencias','2011-04-18','14:47:00'),(11,'15031097','insertar','mon_sue=6119.45;des_sue=Contralor','sueldos','2011-04-18','14:50:00'),(12,'15031097','insertar','mon_sue=2251.70;des_sue=Auditor II','sueldos','2011-04-18','14:50:00'),(13,'15031097','insertar','mon_sue=2588.65;des_sue=Auditor  II','sueldos','2011-04-18','14:51:00'),(14,'15031097','insertar','mon_sue=2100.00;des_sue=Inspector de Obras II','sueldos','2011-04-18','14:51:00'),(15,'15031097','insertar','mon_sue=1658.42;des_sue=Secretaria Ejecutiva','sueldos','2011-04-18','14:52:00'),(16,'15031097','insertar','mon_sue=3231.07;des_sue=Administrador(a)','sueldos','2011-04-18','14:52:00'),(17,'15031097','insertar','mon_sue=2937.34;des_sue=Jefe Sala Técnica','sueldos','2011-04-18','14:53:00'),(18,'15031097','insertar','mon_sue=2157.40;des_sue=Registrador de Bienes','sueldos','2011-04-18','14:53:00'),(19,'15031097','insertar','mon_sue=2937.34;des_sue=Jefe de Servicio Jurídico','sueldos','2011-04-18','14:54:00'),(20,'15031097','insertar','mon_sue=1548.22;des_sue=Asistente de Control Fiscal','sueldos','2011-04-18','14:54:00'),(21,'15031097','insertar','mon_sue=2063.60;des_sue=Auditor I','sueldos','2011-04-18','14:55:00'),(22,'15031097','insertar','mon_sue=1690.00;des_sue=Auxiliar de Auditoría','sueldos','2011-04-18','14:55:00'),(23,'15031097','insertar','mon_sue=1690.00;des_sue=Técnico Inspector de Obras Civiles','sueldos','2011-04-18','14:56:00'),(24,'15031097','insertar','mon_sue=1223.89;des_sue=Aseadora','sueldos','2011-04-18','14:56:00'),(25,'15031097','insertar','mon_sue=1690;des_sue=Operadora de Equipos de Computación','sueldos','2011-04-18','14:56:00'),(26,'15031097','insertar','mon_sue=1800;des_sue=Abogado','sueldos','2011-04-18','14:57:00'),(27,'15031097','modificar','cod_sue=12; mon_sue=1690.00; des_sue=Auxiliar de Auditoría; ','sueldos','2011-04-18','14:57:00'),(28,'15031097','modificar','cod_sue=13; mon_sue=1690.00; des_sue=Técnico Inspector de Obras Civiles; ','sueldos','2011-04-18','14:57:00'),(29,'15031097','modificar','cod_sue=14; mon_sue=1223.89; des_sue=Aseadora; ','sueldos','2011-04-18','14:58:00'),(30,'15031097','modificar','cod_sue=14; mon_sue=1223.89; des_sue=Aseadora (Contratado); ','sueldos','2011-04-18','14:58:00'),(31,'15031097','modificar','cod_sue=15; mon_sue=1690.00; des_sue=Operadora de Equipos de Computación; ','sueldos','2011-04-18','14:58:00'),(32,'15031097','modificar','cod_sue=16; mon_sue=1800.00; des_sue=Abogado; ','sueldos','2011-04-18','14:58:00'),(33,'15031097','insertar','ced_per=15031097;nac_per=V;nom_per=Luis Felipe;ape_per=Márquez Briceño;sex_per=M;fnac_per=1981-03-09;lnac_per=Mérida;cor_per=felixpe09@gmail.com;pro_per=TSU Informática;dir_per=Asoprieto, Calle 4A, #242.;tel_per=02742217980;cel_per=04266736316;fch_reg=2011-04-18;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-18','15:00:00'),(34,'15031097','insertar','num_car=1;nom_car=Operador de Equipos de Computación;cod_sue=15;est_car=;cod_tcar=2;cod_dep=ADM','cargos','2011-04-18','15:08:00'),(35,'15031097','modificar','cod_sue=15; mon_sue=1690.00; des_sue=Operadora de Equipos de Computación (Contratado); ','sueldos','2011-04-18','15:08:00'),(36,'12345678','insertar','cod_usr=21184968;nom_usr=Yaquelin;ape_usr=Rivas Vera;freg_usr=2011-04-25;log_usr=yaquelin;pas_usr=yaquerivas;cod_grp=4;fcam_usr=2011-04-25;sts_usr=A','usuarios','2011-04-25','08:22:00'),(37,'21184968','insertar','ced_per=15031097;nac_per=V;nom_per=Luis Felipe;ape_per=Márquez Briceño;sex_per=M;fnac_per=1981-03-09;lnac_per=Mérida;cor_per=felixpe09@hotmail.com;pro_per=TSU INFORMATICA;dir_per=ASOPRIETO CALLE 4A #242, EJIDO;tel_per=02742217980;cel_per=04266736316;fch_reg=2011-04-25;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-25','08:35:00'),(38,'15031097','insertar','num_car=1;nom_car=Contralor Municipal;cod_sue=1;est_car=;cod_tcar=1;cod_dep=DSC','cargos','2011-04-25','08:45:00'),(39,'15031097','insertar','num_car=1;nom_car=Administrador(a);cod_sue=6;est_car=;cod_tcar=1;cod_dep=ADM','cargos','2011-04-25','08:46:00'),(40,'15031097','insertar','num_car=1;nom_car=Secretaria Ejecutiva;cod_sue=5;est_car=;cod_tcar=1;cod_dep=SCR','cargos','2011-04-25','08:46:00'),(41,'15031097','insertar','num_car=1;nom_car=Jefe de Servicios Jurídicos;cod_sue=9;est_car=;cod_tcar=1;cod_dep=SRJ','cargos','2011-04-25','08:49:00'),(42,'15031097','insertar','num_car=1;nom_car=Jefe de Sala Técnica;cod_sue=7;est_car=;cod_tcar=1;cod_dep=SLT','cargos','2011-04-25','08:49:00'),(43,'15031097','insertar','num_car=1;nom_car=Auditor II;cod_sue=2;est_car=;cod_tcar=1;cod_dep=UAD','cargos','2011-04-25','08:50:00'),(44,'15031097','insertar','num_car=2;nom_car=Auditor II ;cod_sue=3;est_car=;cod_tcar=1;cod_dep=UAD','cargos','2011-04-25','08:51:00'),(45,'15031097','insertar','num_car=2;nom_car=Inspector de Obras II;cod_sue=4;est_car=;cod_tcar=1;cod_dep=SLT','cargos','2011-04-25','08:51:00'),(46,'15031097','insertar','num_car=1;nom_car=Registrador de Bienes;cod_sue=8;est_car=;cod_tcar=1;cod_dep=CBN','cargos','2011-04-25','08:52:00'),(47,'15031097','insertar','num_car=2;nom_car=Asistente de Control y Fiscalización;cod_sue=10;est_car=;cod_tcar=1;cod_dep=CBN','cargos','2011-04-25','08:53:00'),(48,'15031097','insertar','num_car=3;nom_car=Auditor I;cod_sue=11;est_car=;cod_tcar=1;cod_dep=UAD','cargos','2011-04-25','08:54:00'),(49,'15031097','modificar','cod_car=4; num_car=001; nom_car=Secretaria Ejecutiva; cod_sue=5; est_car=D; cod_tcar=1; cod_dep=SCR; ced_per=; fch_asg=0000-00-00; des_car=; ','cargos','2011-04-25','08:59:00'),(50,'15031097','modificar','cod_car=7; num_car=001; nom_car=Auditor II; cod_sue=2; est_car=D; cod_tcar=1; cod_dep=UAD; ced_per=; fch_asg=0000-00-00; des_car=; ','cargos','2011-04-25','08:59:00'),(51,'15031097','modificar','cod_car=8; num_car=002; nom_car=Auditor II ; cod_sue=3; est_car=D; cod_tcar=1; cod_dep=UAD; ced_per=; fch_asg=0000-00-00; des_car=; ','cargos','2011-04-25','08:59:00'),(52,'15031097','modificar','cod_car=9; num_car=002; nom_car=Inspector de Obras II; cod_sue=4; est_car=D; cod_tcar=1; cod_dep=SLT; ced_per=; fch_asg=0000-00-00; des_car=; ','cargos','2011-04-25','08:59:00'),(53,'15031097','modificar','cod_car=10; num_car=001; nom_car=Registrador de Bienes; cod_sue=8; est_car=D; cod_tcar=1; cod_dep=CBN; ced_per=; fch_asg=0000-00-00; des_car=; ','cargos','2011-04-25','09:00:00'),(54,'15031097','modificar','cod_car=11; num_car=002; nom_car=Asistente de Control y Fiscalización; cod_sue=10; est_car=D; cod_tcar=1; cod_dep=CBN; ced_per=; fch_asg=0000-00-00; des_car=; ','cargos','2011-04-25','09:00:00'),(55,'15031097','modificar','cod_car=12; num_car=003; nom_car=Auditor I; cod_sue=11; est_car=D; cod_tcar=1; cod_dep=UAD; ced_per=; fch_asg=0000-00-00; des_car=; ','cargos','2011-04-25','09:00:00'),(56,'15031097','insertar','num_car=4;nom_car=Auxiliar de Auditoría;cod_sue=12;est_car=;cod_tcar=3;cod_dep=UAD','cargos','2011-04-25','09:01:00'),(57,'15031097','insertar','num_car=3;nom_car=Técnico Inspector de Obras Civiles;cod_sue=13;est_car=;cod_tcar=3;cod_dep=SLT','cargos','2011-04-25','09:02:00'),(58,'15031097','modificar','cod_sue=13; mon_sue=1690.00; des_sue=Técnico Inspector de Obras Civiles (Contratado); ','sueldos','2011-04-25','09:02:00'),(59,'15031097','modificar','cod_sue=12; mon_sue=1690.00; des_sue=Auxiliar de Auditoría (Contratado); ','sueldos','2011-04-25','09:02:00'),(60,'15031097','modificar','cod_sue=14; mon_sue=1223.89; des_sue=Aseadora (Contratada); ','sueldos','2011-04-25','09:03:00'),(61,'15031097','modificar','cod_sue=15; mon_sue=1690.00; des_sue=Operador de Equipos de Computación (Contratado); ','sueldos','2011-04-25','09:03:00'),(62,'15031097','modificar','cod_sue=16; mon_sue=1800.00; des_sue=Abogado (Contratado); ','sueldos','2011-04-25','09:03:00'),(63,'15031097','insertar','num_car=2;nom_car=Abogado;cod_sue=16;est_car=;cod_tcar=3;cod_dep=SRJ','cargos','2011-04-25','09:06:00'),(64,'21184968','insertar','ced_per=16933715;nac_per=V;nom_per=Alejandro Antonio;ape_per=Sayago Gonzales ;sex_per=M;fnac_per=1984-12-19;lnac_per=Merida edo Merida;cor_per=alejandro_s1@hotmail.com;pro_per=Abogado;dir_per=AV 16 de Septiembre casa nro 1-12\r\nSan Jose de Obrero  zona postal;tel_per=;cel_per=04147086961;fch_reg=2011-04-25;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-25','09:06:00'),(65,'15031097','insertar','num_car=2;nom_car=Aseadora;cod_sue=14;est_car=;cod_tcar=3;cod_dep=ADM','cargos','2011-04-25','09:07:00'),(66,'15031097','insertar','num_car=3;nom_car=Operador de Equipos de Computación;cod_sue=15;est_car=;cod_tcar=3;cod_dep=ADM','cargos','2011-04-25','09:08:00'),(67,'21184968','insertar','ced_per=4264264;nac_per=V;nom_per=Carlos Julio ;ape_per=Puentes Uzcatequi ;sex_per=M;fnac_per=1955-12-04;lnac_per=La Azulita edo Merida ;cor_per=puentesuzca@hotmail.com;pro_per=Arquitecto;dir_per=urb.j.j Osuna Rodriquez bloque 20apt 00-04;tel_per=02744172160;cel_per=04168002030;fch_reg=2011-04-25;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-25','09:33:00'),(68,'21184968','insertar','ced_per=8036119;nac_per=V;nom_per=Elda Soraya;ape_per=Hill Davila;sex_per=F;fnac_per=1965-10-29;lnac_per=Merida edo Merida;cor_per=servijdeos@hotmail.com;pro_per=Abogada;dir_per=chamita calle las Acasias Nº1-278 Merida ;tel_per=02742665325;cel_per=04165778653;fch_reg=2011-04-25;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-25','10:21:00'),(69,'15031097','eliminar','fch_frd=2011-04-20;des_frd=Feriado por decreto','feriados','2011-04-25','10:52:00'),(70,'15031097','modificar','ced_per=16933715; nac_per=V; nom_per=Alejandro Antonio; ape_per=Sayago Gonzales ; sex_per=M; fnac_per=1984-12-19; lnac_per=Merida edo Merida; cor_per=alejandro_s1@hotmail.com; pro_per=Abogado; dir_per=AV 16 de Septiembre casa nro 1-12\r\nSan Jose de Obrero  zona postal; tel_per=; cel_per=04147086961; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-04-25','11:06:00'),(71,'15031097','modificar','ced_per=4264264; nac_per=V; nom_per=Carlos Julio ; ape_per=Puentes Uzcatequi ; sex_per=M; fnac_per=1955-12-04; lnac_per=La Azulita edo Merida ; cor_per=puentesuzca@hotmail.com; pro_per=Arquitecto; dir_per=urb.j.j Osuna Rodriquez bloque 20apt 00-04; tel_per=02744172160; cel_per=04168002030; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-04-25','11:11:00'),(72,'21184968','insertar','ced_per=9028505;nac_per=V;nom_per=Ramon Antonio;ape_per=Fernandez Roja;sex_per=M;fnac_per=1962-09-01;lnac_per=vigia ;cor_per=ramonactividad@hotmail.com;pro_per=ing Mecanico;dir_per=palmo calle 5 de julio casa Nº 0-13;tel_per=02742212574;cel_per=04166265746;fch_reg=2011-04-25;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-25','11:14:00'),(73,'15031097','modificar','ced_per=9028505; nac_per=V; nom_per=Ramon Antonio; ape_per=Fernandez Roja; sex_per=M; fnac_per=1962-09-01; lnac_per=vigia ; cor_per=ramonactividad@hotmail.com; pro_per=ing Mecanico; dir_per=palmo calle 5 de julio casa Nº 0-13; tel_per=02742212574; cel_per=04166265746; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-04-25','11:43:00'),(74,'21184968','insertar','ced_per=15621190;nac_per=V;nom_per=Yohana;ape_per=Perez de Valero;sex_per=F;fnac_per=1981-09-06;lnac_per=Merida edo Merida;cor_per=yoha19perez@hotmail.com;pro_per=lic Contaduria Publica;dir_per=Sector bella vista calle lara casa Nº54 ejido;tel_per=02742210478;cel_per=04163791718;fch_reg=2011-04-25;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-25','11:47:00'),(75,'15031097','modificar','ced_per=8036119; nac_per=V; nom_per=Elda Soraya; ape_per=Hill Davila; sex_per=F; fnac_per=1965-10-29; lnac_per=Merida edo Merida; cor_per=servijdeos@hotmail.com; pro_per=Abogada; dir_per=chamita calle las Acasias Nº1-278 Merida ; tel_per=02742665325; cel_per=04165778653; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-04-25','11:50:00'),(76,'21184968','insertar','ced_per=10715628;nac_per=V;nom_per=Yoly Josefina;ape_per=Avendaño Meza;sex_per=F;fnac_per=1970-06-02;lnac_per=Merida edo Merida;cor_per=;pro_per=Contador publico;dir_per=Santa elena calle 9 Nº10-45c;tel_per=02742630667;cel_per=04268283389;fch_reg=2011-04-25;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-25','12:09:00'),(77,'15031097','modificar','ced_per=15621190; nac_per=V; nom_per=Yohana; ape_per=Perez de Valero; sex_per=F; fnac_per=1981-09-06; lnac_per=Merida edo Merida; cor_per=yoha19perez@hotmail.com; pro_per=lic Contaduria Publica; dir_per=Sector bella vista calle lara casa Nº54 ejido; tel_per=02742210478; cel_per=04163791718; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-04-25','12:55:00'),(78,'15031097','modificar','ced_per=10715628; nac_per=V; nom_per=Yoly Josefina; ape_per=Avendaño Meza; sex_per=F; fnac_per=1970-06-02; lnac_per=Merida edo Merida; cor_per=; pro_per=Contador publico; dir_per=Santa elena calle 9 Nº10-45c; tel_per=02742630667; cel_per=04268283389; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-04-25','12:56:00'),(79,'21184968','insertar','ced_per=12346028;nac_per=V;nom_per=Jose de Jesus ;ape_per=Corredor Alarcon;sex_per=M;fnac_per=1974-01-30;lnac_per=Merida edo Merida;cor_per=tscorredor@hotmail.com;pro_per=inspector de obras;dir_per=Manzano Bajo calle urdaneta Nº 32ejido edo merida;tel_per=02744165181;cel_per=04147484178;fch_reg=2011-04-25;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-25','13:18:00'),(80,'15031097','insertar','nom_con=CAPREAMCE;des_con=Préstamo a Caja de Ahorro','concesiones','2011-04-25','13:36:00'),(81,'15031097','modificar','cod_con=1; nom_con=CAPREAMCE; des_con=Préstamo a Caja de Ahorro; ','concesiones','2011-04-25','13:37:00'),(82,'15031097','insertar','nom_des=Préstamo CAPREAMCE;des_des=Préstamo a Caja de Ahorros','descuentos','2011-04-25','13:38:00'),(83,'15031097','modificar','ced_per=12346028; nac_per=V; nom_per=Jose de Jesus ; ape_per=Corredor Alarcon; sex_per=M; fnac_per=1974-01-30; lnac_per=Merida edo Merida; cor_per=tscorredor@hotmail.com; pro_per=inspector de obras; dir_per=Manzano Bajo calle urdaneta Nº 32ejido edo merida; tel_per=02744165181; cel_per=04147484178; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-04-25','13:41:00'),(84,'21184968','insertar','ced_per=11960592;nac_per=V;nom_per=Jesus Ramòn;ape_per=Silva Osuna;sex_per=M;fnac_per=1975-03-23;lnac_per=Merida edo Merida;cor_per=je2375@hotmail.com;pro_per= tsu construcion civil;dir_per=calle camejo,ejido.RES El trigal EDF APTO 2-3;tel_per=02742213223;cel_per=04149785308;fch_reg=2011-04-25;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-25','13:42:00'),(85,'15031097','insertar','con_dsp=Préstamo Caja de Ahorros;ncuo_dsp=24;per_dsp=;mcuo_dsp=133.27;por_dsp=;bpor_dsp=;cod_des=1;ced_per=4264264;cod_car=6;fch_dsp=2010-05-01;ncp_dsp=','deducciones','2011-04-25','13:57:00'),(86,'21184968','insertar','ced_per=8023210;nac_per=V;nom_per=Jose Andres ;ape_per=Briceño Valero;sex_per=M;fnac_per=1961-11-10;lnac_per=Merida edo Merida;cor_per=josemeridave@hotmail.com;pro_per=Abogado;dir_per= URB J.J OSUMA RODRIGUEZ VEREDA 17 .Nº07 MERIDA;tel_per=02742714008;cel_per=04162749656;fch_reg=2011-04-25;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-04-25','15:09:00'),(87,'15031097','modificar','ced_per=11960592; nac_per=V; nom_per=Jesus Ramòn; ape_per=Silva Osuna; sex_per=M; fnac_per=1975-03-23; lnac_per=Merida edo Merida; cor_per=je2375@hotmail.com; pro_per= tsu construcion civil; dir_per=calle camejo,ejido.RES El trigal EDF APTO 2-3; tel_per=02742213223; cel_per=04149785308; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-04-26','15:03:00'),(88,'15031097','modificar','ced_per=11960592; nac_per=V; nom_per=Jesus Ramón; ape_per=Silva Osuna; sex_per=M; fnac_per=1975-03-23; lnac_per=Merida edo Merida; cor_per=je2375@hotmail.com; pro_per= tsu construcion civil; dir_per=calle camejo,ejido.RES El trigal EDF APTO 2-3; tel_per=02742213223; cel_per=04149785308; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-04-26','15:04:00'),(89,'12345678','insertar','cod_usr=8023210;nom_usr=José Andres;ape_usr=Briceño Valero;freg_usr=2011-04-29;log_usr=josemeridave;pas_usr=jose_1961;cod_grp=1;fcam_usr=2011-04-29;sts_usr=A','usuarios','2011-04-29','10:49:00'),(90,'8023210','insertar','fch_frd=2011-04-20;des_frd=feriado por decreto n| 213123','feriados','2011-04-29','10:56:00'),(91,'8023210','insertar','ced_per=16933715;fch_ina=2011-04-29;tip_ina=Remunerada;des_ina=permiso medico','inasistencias','2011-04-29','11:00:00'),(92,'12345678','eliminar','ced_per=16933715;fch_ina=2011-04-29;tip_ina=Remunerada;des_ina=permiso medico','inasistencias','2011-04-29','11:16:00'),(93,'15031097','eliminar','fch_frd=2011-04-20;des_frd=feriado por decreto n| 213123','feriados','2011-04-29','12:08:00'),(94,'15031097','insertar','fch_frd=2011-05-01;des_frd=Día Internacional del Trabajador','feriados','2011-04-29','12:47:00'),(95,'15031097','insertar','ced_per=16933715;fch_ina=2011-04-28;tip_ina=Remunerada;des_ina=prueba','inasistencias','2011-04-29','12:53:00'),(96,'21184968','insertar','ced_per=14304657;nac_per=V;nom_per=leidy josefina;ape_per=pereira ochoa;sex_per=F;fnac_per=1979-04-07;lnac_per=Bejuma edo carabobo;cor_per=ladypereirao@hotmail.com;pro_per=tecnico superior en contaduria;dir_per=av centenario, residencia el molino torre 10 apto 1-4;tel_per=02744168377;cel_per=04147458942;fch_reg=2011-05-03;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-05-03','08:47:00'),(97,'21184968','insertar','ced_per=6343267;nac_per=V;nom_per=Luz Marina;ape_per=Salcedo Pernia;sex_per=F;fnac_per=1970-08-27;lnac_per=caracas;cor_per=luzmarsal@hotmail.com;pro_per=lic contaduria  publica;dir_per=URB Hacienda zumba calle 4-B Araguaney 298;tel_per=02745118815;cel_per=04166657051;fch_reg=2011-05-03;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-05-03','09:08:00'),(98,'21184968','insertar','ced_per=16657924;nac_per=V;nom_per=Maria Alejandra ;ape_per=Perez;sex_per=F;fnac_per=1985-02-23;lnac_per=Merida;cor_per=MARIAP-23@hotmail.com;pro_per=Bachiller;dir_per=Calle el porvenir N° 18;tel_per=;cel_per=04165780214;fch_reg=2011-05-03;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-05-03','09:39:00'),(99,'21184968','insertar','ced_per=10109577;nac_per=V;nom_per=Maria Isabel;ape_per=Rivas de Osuna;sex_per=F;fnac_per=1967-11-05;lnac_per=Merida;cor_per=u_auditoriaint@hotmail.com;pro_per=lic contaduria  publica;dir_per=urb zumba calle 4 A N°556;tel_per=2212896;cel_per=04141413561;fch_reg=2011-05-03;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-05-03','10:16:00'),(100,'21184968','insertar','ced_per=8029735;nac_per=V;nom_per=Nora Coromoto;ape_per=Cerrada;sex_per=F;fnac_per=1961-04-19;lnac_per=Merida;cor_per=Secrenor@hotmail.com;pro_per=Secretaria ;dir_per=Manzano alto via la panamericana casa n°031;tel_per=02748085555;cel_per=04169760118;fch_reg=2011-05-03;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-05-03','10:42:00'),(101,'21184968','insertar','ced_per=8019652;nac_per=V;nom_per=Oswaldo Ramon ;ape_per=Ramos;sex_per=M;fnac_per=1962-02-06;lnac_per=Barquisimeto Edo Lara;cor_per=oswalramra@hotmail.com;pro_per=analistas y programador desistema de computacionn ;dir_per=urb la mata serrania casa clb , torre13-0-c ;tel_per=02745111050;cel_per=04166742323;fch_reg=2011-05-03;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=','personal','2011-05-03','11:01:00'),(102,'21184968','insertar','cod_pro=1;fch_pro=2011-01-01;rif_pro=J298994940;nom_pro=Asociacion Cooperativa Manely;dir_pro=AV. Principal casa N°1-1Sector Zumba;act_pro=comerciante;cap_pro=2500.00;cas_pro=2500.00;tel_pro=02742710374;fax_pro=;per_pro=J;ofi_reg_pro=Registro Mercantil Primero del edo Merida;num_reg_pro=29;tom_reg_pro=Noveno;fol_reg_pro=241;fch_reg_pro=2010-04-28;num_mod_pro=;tom_mod_pro=;fol_mod_pro=;fch_mod_pro=;pat_pro=;ivss_pro=01931016;ince_pro=;nom_rep_pro=Henry de Jesus;ape_rep_pro=Angulo;ced_rep_pro=08019717;dir_rep_pro=Sector Zumba Casa N°1-1 AV Principal;tel_rep_pro=04161770759;car_rep_pro=Presidente','proveedores','2011-05-04','09:11:00'),(103,'21184968','insertar','cod_pro=2;fch_pro=2011-01-01;rif_pro=J294469752;nom_pro=Asociacion cooperativa coofiandina;dir_pro=Calle 16 Entre Av 5y6 N°5 - 72;act_pro=Compra y venta de reparacion de equipos de oficina;cap_pro=50.000;cas_pro=50.000;tel_pro=02742528046;fax_pro=02742528046;per_pro=J;ofi_reg_pro=Registro Mercantil Primero del edo Merida;num_reg_pro=41;tom_reg_pro=26;fol_reg_pro=305;fch_reg_pro=2007-05-15;num_mod_pro=;tom_mod_pro=;fol_mod_pro=;fch_mod_pro=;pat_pro=;ivss_pro=;ince_pro=;nom_rep_pro=Hugo Ali ;ape_rep_pro=Sulbaran ;ced_rep_pro=08020874;dir_rep_pro=calle 16 entre Av 5y6 N°572;tel_rep_pro=04147455104;car_rep_pro=Cordinador General ','proveedores','2011-05-04','09:57:00'),(104,'21184968','insertar','cod_pro=3;fch_pro=2011-01-01;rif_pro=J296861234;nom_pro=supermercado plaza montalban ;dir_pro=Av Bolivar local N°196 -17 sector montalban;act_pro=comerciante;cap_pro=300.000;cas_pro=300.000;tel_pro=02742214682;fax_pro=;per_pro=J;ofi_reg_pro=Registro Mercantil Primero del edo Merida;num_reg_pro=379;tom_reg_pro=16;fol_reg_pro=16;fch_reg_pro=2008-10-23;num_mod_pro=;tom_mod_pro=;fol_mod_pro=;fch_mod_pro=;pat_pro=;ivss_pro=;ince_pro=;nom_rep_pro=Yhon Isaac;ape_rep_pro=Areas Barrios;ced_rep_pro=08025049;dir_rep_pro=Av Bolivar Sector Montalban;tel_rep_pro=02742214682;car_rep_pro=GERENTE PERSONAL','proveedores','2011-05-04','10:08:00'),(105,'21184968','insertar','cod_pro=4;fch_pro=2011-01-01;rif_pro=J300180174;nom_pro=Sodexho pass venzuela ;dir_pro=con Av los chaguaramos torre corp banca piso 16 la castellar ;act_pro=Emision, comercializacion;cap_pro=2250000;cas_pro=2250000;tel_pro=02122065625;fax_pro=02122065620;per_pro=J;ofi_reg_pro=Registro mercantil segundo  del esdo miranda;num_reg_pro=75;tom_reg_pro=154-A;fol_reg_pro=5;fch_reg_pro=2007-07-07;num_mod_pro=;tom_mod_pro=;fol_mod_pro=;fch_mod_pro=;pat_pro=;ivss_pro=;ince_pro=;nom_rep_pro=nicolas ;ape_rep_pro=vaga;ced_rep_pro=06972690;dir_rep_pro=;tel_rep_pro=;car_rep_pro=gerente general','proveedores','2011-05-04','11:36:00'),(106,'15031097','insertar','fch_frd=2011-05-04;des_frd=feliz','feriados','2011-05-04','11:48:00'),(107,'15031097','insertar','fch_frd=2012-05-010;des_frd=preuba','feriados','2011-05-04','11:48:00'),(108,'15031097','eliminar','fch_frd=2012-05-10;des_frd=preuba','feriados','2011-05-04','11:50:00'),(109,'21184968','insertar','cod_pro=5;fch_pro=2011-01-01;rif_pro=V123536335;nom_pro=Distrubuiora Alexwenp;dir_pro=Av Bolivar N°231 local 02 ejido;act_pro=Compra y venta de papeleria ANT de oficios  ;cap_pro=6000;cas_pro=6000;tel_pro=02742210571;fax_pro=;per_pro=J;ofi_reg_pro=registro mercantil primero del estado merida ;num_reg_pro=91;tom_reg_pro=5;fol_reg_pro=5;fch_reg_pro=2007-08-16;num_mod_pro=;tom_mod_pro=;fol_mod_pro=;fch_mod_pro=;pat_pro=;ivss_pro=;ince_pro=;nom_rep_pro=Amilcar Alexander ;ape_rep_pro=Nieto Contreras ;ced_rep_pro=12353633;dir_rep_pro=Av Bolivar N°231 Local de ejido;tel_rep_pro=04165222227;car_rep_pro=propietario','proveedores','2011-05-04','11:53:00'),(110,'15031097','eliminar','fch_frd=2011-05-04;des_frd=feliz','feriados','2011-05-04','13:03:00'),(111,'12345678','insertar','cod_usr=4264264;nom_usr=Carlos Julio;ape_usr=Puentes Uzcátegui;freg_usr=2011-05-04;log_usr=carlosp;pas_usr=slt1975;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','13:07:00'),(112,'12345678','insertar','cod_usr=16933715;nom_usr=Alejandro Antonio;ape_usr=Sayago González;freg_usr=2011-05-04;log_usr=alejandros;pas_usr=servju;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:25:00'),(113,'12345678','insertar','cod_usr=8036119;nom_usr=Elda Soraya;ape_usr=Hill Dávila;freg_usr=2011-05-04;log_usr=sorayah;pas_usr=servju;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:30:00'),(114,'12345678','insertar','cod_usr=8019652;nom_usr=Oswaldo Ramón;ape_usr=ramos;freg_usr=2011-05-04;log_usr=oswaldor;pas_usr=ctrlbn;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:31:00'),(115,'12345678','insertar','cod_usr=9028505;nom_usr=Ramón Antonio;ape_usr=Fernández Rojas;freg_usr=2011-05-04;log_usr=ramonf;pas_usr=ctrlbn;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:32:00'),(116,'12345678','insertar','cod_usr=11960592;nom_usr=Jesus Ramón;ape_usr=Silva Osuna;freg_usr=2011-05-04;log_usr=jesuss;pas_usr=slt1975;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:33:00'),(117,'12345678','insertar','cod_usr=12346028;nom_usr=José de Jesús;ape_usr=Corredor Alarcón;freg_usr=2011-05-04;log_usr=josec;pas_usr=slt1975;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:34:00'),(118,'21184968','insertar','cod_pro=6;fch_pro=2011-01-01;rif_pro=J298778377;nom_pro=Asociacion cooperativa ofimatica;dir_pro=oficina matriz Av 25 de noviembre urbanizacion el trapiche bloque 4 edif 1 apto 3-3;act_pro=Comerciante;cap_pro=;cas_pro=;tel_pro=02744174697;fax_pro=;per_pro=J;ofi_reg_pro=Registro Publico del Municipio Campo Elias;num_reg_pro=21;tom_reg_pro=2;fol_reg_pro=132;fch_reg_pro=2010-05-26;num_mod_pro=;tom_mod_pro=;fol_mod_pro=;fch_mod_pro=;pat_pro=;ivss_pro=;ince_pro=;nom_rep_pro=Jose Felix;ape_rep_pro=Rivas Gomez;ced_rep_pro=13524339;dir_rep_pro=URB El trapiche bloque 4 edf 1 pisos 3;tel_rep_pro=04247577644;car_rep_pro=gerente','proveedores','2011-05-04','14:34:00'),(119,'12345678','insertar','cod_usr=10715628;nom_usr=Yoly Josefina;ape_usr=Avendaño Meza;freg_usr=2011-05-04;log_usr=yoly;pas_usr=adt123;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:37:00'),(120,'12345678','insertar','cod_usr=15621190;nom_usr=Yohana;ape_usr=Pérez de Valero;freg_usr=2011-05-04;log_usr=yohanap;pas_usr=adt123;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:38:00'),(121,'12345678','modificar','','usuarios','2011-05-04','14:38:00'),(122,'12345678','insertar','cod_usr=8029735;nom_usr=Nora Coromoto;ape_usr=Cerrada;freg_usr=2011-05-04;log_usr=norac;pas_usr=secejec;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:44:00'),(123,'12345678','insertar','cod_usr=10109577;nom_usr=María Isabel;ape_usr=Rivas de Osuna;freg_usr=2011-05-04;log_usr=isabelr;pas_usr=adt123;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:45:00'),(124,'12345678','insertar','cod_usr=14304657;nom_usr=Leidy Josefina;ape_usr=Pereira Ochoa;freg_usr=2011-05-04;log_usr=leidyp;pas_usr=audt123;cod_grp=3;fcam_usr=2011-05-04;sts_usr=A','usuarios','2011-05-04','14:46:00'),(125,'12345678','modificar','','usuarios','2011-05-04','15:19:00'),(126,'21184968','insertar','cod_pro=7;fch_pro=2011-01-01;rif_pro=J037653213;nom_pro=Prove Oficina Sosa;dir_pro=Av Don tulio febres cordero entre calle 29y30merida ;act_pro=comerciante;cap_pro=10.000.000;cas_pro=10.000.000;tel_pro=02742529551;fax_pro=;per_pro=J;ofi_reg_pro=Megistro mercantil primero del estado merida;num_reg_pro=64;tom_reg_pro=5;fol_reg_pro=5;fch_reg_pro=2007-06-26;num_mod_pro=;tom_mod_pro=;fol_mod_pro=;fch_mod_pro=;pat_pro=;ivss_pro=;ince_pro=;nom_rep_pro=Alexis;ape_rep_pro=Sosa;ced_rep_pro=03765321;dir_rep_pro=Av don tulio febres cordero entre calle 29y30 merida;tel_rep_pro=04147742018;car_rep_pro=representante legal','proveedores','2011-05-04','15:30:00'),(127,'21184968','insertar','cod_pro=8;fch_pro=2011-01-01;rif_pro=V080232108;nom_pro=Jose Andres Briceño;dir_pro=urb. JJ osuna Rodriguez Vereda 17 N°07 Merida;act_pro=contralor;cap_pro=000;cas_pro=000;tel_pro=02742714008;fax_pro=;per_pro=P;ofi_reg_pro=;num_reg_pro=0;tom_reg_pro=0;fol_reg_pro=0;fch_reg_pro=2011-01-01;num_mod_pro=;tom_mod_pro=;fol_mod_pro=;fch_mod_pro=;pat_pro=;ivss_pro=;ince_pro=;nom_rep_pro=Jose Andres;ape_rep_pro=Briceño;ced_rep_pro=08023210;dir_rep_pro=urb jj Osuna rodriguez;tel_rep_pro=04162749656;car_rep_pro=contralor','proveedores','2011-05-05','11:32:00'),(128,'21184968','insertar','cod_pro=9;fch_pro=2011-01-01;rif_pro=V080297358;nom_pro=Nora Cerrada;dir_pro=EL Manzano Alto Via Jaji Panamericana;act_pro=Secretaria;cap_pro=0002;cas_pro=0001;tel_pro=04267032172;fax_pro=;per_pro=P;ofi_reg_pro=S/N;num_reg_pro=000;tom_reg_pro=0;fol_reg_pro=009;fch_reg_pro=2011-01-01;num_mod_pro=0004;tom_mod_pro=S/N;fol_mod_pro=0001;fch_mod_pro=2011-01-01;pat_pro=0;ivss_pro=0;ince_pro=0;nom_rep_pro=Nora Coromoto;ape_rep_pro=Cerrada;ced_rep_pro=08029735;dir_rep_pro=El Manzano Alto via jaji panamericana;tel_rep_pro=04167032172;car_rep_pro=Secretaria','proveedores','2011-05-05','13:45:00'),(129,'21184968','insertar','cod_pro=10;fch_pro=2011-01-01;rif_pro=J029506233;nom_pro=Cadafe;dir_pro=Cento Electronico Nacional ;act_pro=comerciante;cap_pro=0000000;cas_pro=0000000;tel_pro=02432722711;fax_pro=;per_pro=J;ofi_reg_pro=N/R;num_reg_pro=000;tom_reg_pro=0009;fol_reg_pro=0009;fch_reg_pro=2011-01-01;num_mod_pro=0;tom_mod_pro=0;fol_mod_pro=0;fch_mod_pro=2011-01-01;pat_pro=0;ivss_pro=0;ince_pro=0;nom_rep_pro=S/R;ape_rep_pro=S/R;ced_rep_pro=00000000;dir_rep_pro=;tel_rep_pro=;car_rep_pro=S/R','proveedores','2011-05-05','14:09:00'),(130,'21184968','insertar','cod_pro=11;fch_pro=2011-01-01;rif_pro=J090370196;nom_pro=Sovenpfa;dir_pro=Av 4 calle 21edif Don Atilio Nivel Mezanina;act_pro=Comerciante;cap_pro=0000000000;cas_pro=0000000000;tel_pro=02742510444;fax_pro=;per_pro=J;ofi_reg_pro=N/R;num_reg_pro=01;tom_reg_pro=0000005;fol_reg_pro=980;fch_reg_pro=2011-01-01;num_mod_pro=0;tom_mod_pro=0;fol_mod_pro=0001;fch_mod_pro=2011-01-01;pat_pro=;ivss_pro=;ince_pro=;nom_rep_pro=S/R;ape_rep_pro=N/R;ced_rep_pro=88888888;dir_rep_pro=Av 4 con Calle 21 edif Don Atilio Nivel Mezanina;tel_rep_pro=02742510444;car_rep_pro=N/R','proveedores','2011-05-05','14:24:00'),(131,'21184968','insertar','cod_pro=12;fch_pro=2011-01-01;rif_pro=J001241345;nom_pro=CANTV;dir_pro=Final Av libertador edif CNT Caracas;act_pro=Cervicio Telefonoco;cap_pro=00001;cas_pro=00001;tel_pro=02125001111;fax_pro=;per_pro=J;ofi_reg_pro=S/N;num_reg_pro=008;tom_reg_pro=0000005;fol_reg_pro=5436;fch_reg_pro=2011-01-01;num_mod_pro=789;tom_mod_pro=S/N;fol_mod_pro=070;fch_mod_pro=2011-01-01;pat_pro=0;ivss_pro=0;ince_pro=0;nom_rep_pro=CANTV;ape_rep_pro=J;ced_rep_pro=01241345;dir_rep_pro=Final Av Libertedor edif CNT Caracas;tel_rep_pro=02125001111;car_rep_pro=Propietario','proveedores','2011-05-06','09:13:00'),(132,'21184968','insertar','cod_pro=13;fch_pro=2011-01-01;rif_pro=V127803591;nom_pro=Multiservicios del centro ;dir_pro=Av Bolivar Casa N° 162 Ejido edo Merida;act_pro=Puesto del Estacionamiento;cap_pro=00001;cas_pro=00001;tel_pro=02744168266;fax_pro=;per_pro=J;ofi_reg_pro=S/N;num_reg_pro=999;tom_reg_pro=0;fol_reg_pro=465;fch_reg_pro=2011-01-01;num_mod_pro=999;tom_mod_pro=S/N;fol_mod_pro=465;fch_mod_pro=2011-01-01;pat_pro=0;ivss_pro=0;ince_pro=0;nom_rep_pro=Ramon A ;ape_rep_pro=Ruiz Fernandez;ced_rep_pro=12780359;dir_rep_pro=Av Bolivar Casa N°162 Ejido edo Merida;tel_rep_pro=02744168266;car_rep_pro=Representante Legal','proveedores','2011-05-06','09:45:00'),(133,'21184968','insertar','cod_pro=14;fch_pro=2011-01-01;rif_pro=J000389233;nom_pro=Seguros Caracas;dir_pro=Av francisco de Miranda CC el Parque Torre Seguros Caracas Nivel C-4 los Palos Grandes Caracas ;act_pro=Seguros Caracas;cap_pro=00001;cas_pro=00001;tel_pro=02122099556;fax_pro=;per_pro=J;ofi_reg_pro=S/N;num_reg_pro=0;tom_reg_pro=0;fol_reg_pro=0;fch_reg_pro=2011-01-01;num_mod_pro=009;tom_mod_pro=S/N;fol_mod_pro=008;fch_mod_pro=2011-01-01;pat_pro=0;ivss_pro=0;ince_pro=0;nom_rep_pro=Seguros ;ape_rep_pro=Caracas;ced_rep_pro=00389233;dir_rep_pro=Av Francisco de Miranda CC el ParqueTorre Seguros Caracas Nivel c -4 Con Palos Grandes Caracas;tel_rep_pro=02122099556;car_rep_pro=Representante Legal','proveedores','2011-05-06','10:20:00'),(134,'21184968','insertar','cod_pro=15;fch_pro=2011-01-01;rif_pro=J000268401;nom_pro=Seguros Nuevo Mundo;dir_pro=Av luis roche con carrera trasversal edif nuevo mundo Sector Altamira;act_pro=Seguros Nuevo Mundo;cap_pro=00001;cas_pro=00001;tel_pro=02122011111;fax_pro=02122011157;per_pro=J;ofi_reg_pro=S/N;num_reg_pro=0008;tom_reg_pro=0;fol_reg_pro=0;fch_reg_pro=2011-01-01;num_mod_pro=0008;tom_mod_pro=S/N;fol_mod_pro=0;fch_mod_pro=2011-01-01;pat_pro=0;ivss_pro=0;ince_pro=0;nom_rep_pro=Seguros;ape_rep_pro=nuevo mundo;ced_rep_pro=00026840;dir_rep_pro=Av Luis Rochec/3 Torre Nuevo Mundo Sector Altamira;tel_rep_pro=02122011111;car_rep_pro=Representante Legal','proveedores','2011-05-06','10:44:00'),(135,'21184968','insertar','cod_pro=16;fch_pro=2011-01-01;rif_pro=J070017376;nom_pro=Seguros los Andes;dir_pro=Av las Pilas, urb . Santa Ines edif Seguros los Andes;act_pro=Seguros lo Andes;cap_pro=0001;cas_pro=0001;tel_pro=02763402611;fax_pro=;per_pro=J;ofi_reg_pro=S/N;num_reg_pro=009;tom_reg_pro=0;fol_reg_pro=0;fch_reg_pro=2011-01-01;num_mod_pro=008;tom_mod_pro=S/N;fol_mod_pro=008;fch_mod_pro=2011-01-01;pat_pro=0;ivss_pro=0;ince_pro=0;nom_rep_pro=Seguros;ape_rep_pro= Andes;ced_rep_pro=70017376;dir_rep_pro=Av las Pilas urb Santa Ines edif Seguros los Andes;tel_rep_pro=02763402611;car_rep_pro=Representante Legal','proveedores','2011-05-06','10:56:00'),(136,'12345678','modificar','','usuarios','2011-05-06','11:05:00'),(137,'21184968','insertar','cod_pro=17;fch_pro=2011-01-01;rif_pro=J311801596;nom_pro=Condomineo  Centro Comercial Centenario ;dir_pro=Av Centenario. Centro Comercial Centenrio Nivel PR Oficina Administrador;act_pro=Condomineo;cap_pro=0001;cas_pro=0001;tel_pro=02744158866;fax_pro=;per_pro=J;ofi_reg_pro=S/N;num_reg_pro=0006;tom_reg_pro=0;fol_reg_pro=0;fch_reg_pro=2011-01-01;num_mod_pro=0006;tom_mod_pro=S/N;fol_mod_pro=0007;fch_mod_pro=2011-01-01;pat_pro=0;ivss_pro=0;ince_pro=0;nom_rep_pro=condomineo;ape_rep_pro=centenario;ced_rep_pro=31180159;dir_rep_pro=Av centenario . centro comercial ;tel_rep_pro=02744158866;car_rep_pro=administrador','proveedores','2011-05-06','11:18:00'),(138,'21184968','insertar','cod_pro=18;fch_pro=2011-01-01;rif_pro=J305877432;nom_pro=Capreamce;dir_pro=Ejido edo Merida;act_pro=Caja De Ahorro;cap_pro=0001;cas_pro=0001;tel_pro=02742214982;fax_pro=;per_pro=J;ofi_reg_pro=S/N;num_reg_pro=0009;tom_reg_pro=0;fol_reg_pro=0;fch_reg_pro=2011-01-01;num_mod_pro=0009;tom_mod_pro=S/N;fol_mod_pro=0006;fch_mod_pro=2011-01-01;pat_pro=0;ivss_pro=0;ince_pro=0;nom_rep_pro=Adonaida ;ape_rep_pro=peña ;ced_rep_pro=11460606;dir_rep_pro=ejido edo merida;tel_rep_pro=02742214982;car_rep_pro=Representante Legal','proveedores','2011-05-06','11:35:00'),(139,'21184968','insertar','cod_pro=19;fch_pro=2011-01-01;rif_pro=G200061944;nom_pro=Aguas de Ejido;dir_pro=Av Centenario .Nucleo Norte ,Nivel 1 Local 62;act_pro=Servicio de la Comunidad;cap_pro=0001;cas_pro=0001;tel_pro=02742215774;fax_pro=;per_pro=J;ofi_reg_pro=S/N;num_reg_pro=008;tom_reg_pro=0;fol_reg_pro=0;fch_reg_pro=2011-01-01;num_mod_pro=008;tom_mod_pro=S/N;fol_mod_pro=007;fch_mod_pro=2011-01-01;pat_pro=0;ivss_pro=0;ince_pro=0;nom_rep_pro=AGUAS;ape_rep_pro=EJIDO;ced_rep_pro=20006194;dir_rep_pro=Av centenario . centro comercial centenario nucleo norte nivel 1 local 62;tel_rep_pro=02742215774;car_rep_pro=Representante Legal','proveedores','2011-05-06','11:49:00'),(140,'21184968','insertar','cod_pro=20;fch_pro=2011-01-01;rif_pro=J299956066;nom_pro=Asociacion Cooperativa  andimed rl;dir_pro=Edif el Ramiral Piso Planta Baja Sagrado Calle 26;act_pro=Comercio, Compras, Ventas, Y Exportacion;cap_pro=0001;cas_pro=0001;tel_pro=02742526096;fax_pro=;per_pro=J;ofi_reg_pro=publico d el municipio libertedor estado merida;num_reg_pro=38;tom_reg_pro=22;fol_reg_pro=222;fch_reg_pro=2010-10-21;num_mod_pro=38;tom_mod_pro=22;fol_mod_pro=222;fch_mod_pro=2010-10-21;pat_pro=;ivss_pro=;ince_pro=;nom_rep_pro=Aurimar Aurora ;ape_rep_pro=Angulo Escalante;ced_rep_pro=10717555;dir_rep_pro=edif el Ramiral Piso Planta Baja Sagrado calle 26;tel_rep_pro=04166740194;car_rep_pro=Representante Legal','proveedores','2011-05-06','12:06:00'),(141,'15031097','insertar','fch_frd=2011-05-20;des_frd=Dia libre para todos','feriados','2011-05-07','09:01:00'),(142,'15031097','insertar','cod_egr=;fch_egr=2011-05-02;nor_egr=1;rif_pro=J298994940;aut_egr=;cod_ban=3;chq_egr=13423123;fac_egr=SN;tip_egr=;ref_egr=S/N;con_egr=Traslado;mon_egr=50000;mon_iva_egr=;ret_iva_egr=;ret_isrl_egr=;npr_egr=;ela_egr=T.S.U. Luis Márquez Briceño ;rev_egr=Lcda. Luz Marina Salcedo;apr_egr=Abog. José Andrés Briceño Valero;cont_egr=Lcdo. Gilberto Uzcátegui;obs_egr=','egresos','2011-05-19','10:15:00'),(143,'15031097','modificar','cod_egr=1; fch_egr=2011-05-02; nor_egr=0001; rif_pro=J298994940; aut_egr=; cod_ban=3; chq_egr=13423123; fac_egr=SN; tip_egr=; ref_egr=S/N; con_egr=Traslado; mon_egr=50000.00; mon_iva_egr=0.00; ret_iva_egr=0; ret_isrl_egr=0; npr_egr=0; ela_egr=T.S.U. Luis Márquez Briceño ; rev_egr=Lcda. Luz Marina Salcedo; apr_egr=Abog. José Andrés Briceño Valero; cont_egr=Lcdo. Gilberto Uzcátegui; obs_egr=; ','egresos','2011-05-19','10:18:00'),(144,'15031097','modificar','cod_egr=1; fch_egr=2011-05-02; nor_egr=0001; rif_pro=J298994940; aut_egr=; cod_ban=3; chq_egr=13423123; fac_egr=SN; tip_egr=; ref_egr=S/N; con_egr=Traslado; mon_egr=500.00; mon_iva_egr=0.00; ret_iva_egr=0; ret_isrl_egr=0; npr_egr=0; ela_egr=T.S.U. Luis Márquez Briceño ; rev_egr=Lcda. Luz Marina Salcedo; apr_egr=Abog. José Andrés Briceño Valero; cont_egr=Lcdo. Gilberto Uzcátegui; obs_egr=; ','egresos','2011-05-19','10:18:00'),(145,'15031097','modificar','cod_egr=1; fch_egr=2011-05-02; nor_egr=0001; rif_pro=J298994940; aut_egr=; cod_ban=3; chq_egr=13423123; fac_egr=SN; tip_egr=; ref_egr=S/N; con_egr=Traslado; mon_egr=49999.99; mon_iva_egr=0.00; ret_iva_egr=0; ret_isrl_egr=0; npr_egr=0; ela_egr=T.S.U. Luis Márquez Briceño ; rev_egr=Lcda. Luz Marina Salcedo; apr_egr=Abog. José Andrés Briceño Valero; cont_egr=Lcdo. Gilberto Uzcátegui; obs_egr=; ','egresos','2011-05-19','10:19:00'),(146,'15031097','modificar','cod_egr=1; fch_egr=2011-05-02; nor_egr=0001; rif_pro=J298994940; aut_egr=; cod_ban=3; chq_egr=13423123; fac_egr=SN; tip_egr=; ref_egr=S/N; con_egr=Traslado; mon_egr=50001.00; mon_iva_egr=0.00; ret_iva_egr=0; ret_isrl_egr=0; npr_egr=0; ela_egr=T.S.U. Luis Márquez Briceño ; rev_egr=Lcda. Luz Marina Salcedo; apr_egr=Abog. José Andrés Briceño Valero; cont_egr=Lcdo. Gilberto Uzcátegui; obs_egr=; ','egresos','2011-05-19','10:20:00'),(147,'15031097','modificar','cod_egr=1; fch_egr=2011-05-02; nor_egr=0001; rif_pro=J298994940; aut_egr=; cod_ban=3; chq_egr=13423123; fac_egr=SN; tip_egr=; ref_egr=S/N; con_egr=Traslado; mon_egr=50000.00; mon_iva_egr=0.00; ret_iva_egr=0; ret_isrl_egr=0; npr_egr=0; ela_egr=T.S.U. Luis Márquez Briceño ; rev_egr=Lcda. Luz Marina Salcedo; apr_egr=Abog. José Andrés Briceño Valero; cont_egr=Lcdo. Gilberto Uzcátegui; obs_egr=; ','egresos','2011-05-19','10:23:00'),(148,'15031097','eliminar','fch_frd=2011-05-20;des_frd=Dia libre para todos','feriados','2011-05-23','20:05:00'),(149,'15031097','modificar','ced_per=4264264; nac_per=V; nom_per=Carlos Julio ; ape_per=Puentes Uzcatequi ; sex_per=M; fnac_per=1955-12-04; lnac_per=La Azulita edo Merida ; cor_per=puentesuzca@hotmail.com; pro_per=Arquitecto; dir_per=urb.j.j Osuna Rodriquez bloque 20apt 00-04; tel_per=02744172160; cel_per=04168002030; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ced_per=8036119; nac_per=V; nom_per=Elda Soraya; ape_per=Hill Davila; sex_per=F; fnac_per=1965-10-29; lnac_per=Merida edo Merida; cor_per=servijdeos@hotmail.com; pro_per=Abogada; dir_per=chamita calle las Acasias Nº1-278 Merida ; tel_per=02742665325; cel_per=04165778653; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-05-30','14:59:00'),(150,'15031097','insertar','cod_egr=;fch_egr=2011-05-03;nor_egr=2;rif_pro=J300180174;aut_egr=;cod_ban=3;chq_egr=11004859;fac_egr=69578;tip_egr=Servicio;ref_egr=00178;con_egr=Cancelación de Bono de Alimentación al Personal Fijo y Contratado correspondiente al mes de Abril;mon_egr=10027.62;mon_iva_egr=0;ret_iva_egr=75;ret_isrl_egr=2;npr_egr=;ela_egr=T.S.U. Luis Márquez Briceño ;rev_egr=Lcda. Luz Marina Salcedo;apr_egr=Abog. José Andrés Briceño Valero;cont_egr=Lcdo. Gilberto Uzcátegui;obs_egr=','egresos','2011-06-23','11:58:00'),(151,'15031097','insertar','cod_com=;fch_com=2011-05-03;nor_com=1;sol_com=S/N;tip_com=Servicio;for_com=Contado;adl_com=0;fre_com=Bono de Alimentación;rif_pro=J300180174;npr_com=;iva_com=12;ela_com=TSU. Luis Márquez;rev_com=Lcda. Luz Marina Salcedo;obs_com=','compras','2011-06-23','12:00:00'),(152,'15031097','modificar','cod_pro_egr=1; cod_egr=2; cod_par=1; isrl_pro_egr=; mon_pro_egr=32.10; ','productos_egresos','2011-06-23','12:11:00'),(153,'15031097','modificar','cod_egr=2; fch_egr=2011-05-03; nor_egr=0002; rif_pro=J300180174; aut_egr=; cod_ban=3; chq_egr=11004859; fac_egr=69578; tip_egr=Servicio; ref_egr=00178; con_egr=Cancelación de Bono de Alimentación al Personal Fijo y Contratado correspondiente al mes de Abril; mon_egr=10027.62; mon_iva_egr=0.00; ret_iva_egr=75; ret_isrl_egr=2; npr_egr=0; ela_egr=T.S.U. Luis Márquez Briceño ; rev_egr=Lcda. Luz Marina Salcedo; apr_egr=Abog. José Andrés Briceño Valero; cont_egr=Lcdo. Gilberto Uzcátegui; obs_egr=; ','egresos','2011-06-23','12:12:00'),(154,'15031097','modificar','cod_pro_egr=4; cod_egr=2; cod_par=13; isrl_pro_egr=; mon_pro_egr=2736.00; ','productos_egresos','2011-06-23','12:36:00'),(155,'15031097','eliminar','cod_egr=2;fch_egr=2011-05-03;nor_egr=0002;rif_pro=J300180174;aut_egr=;cod_ban=3;chq_egr=11004859;fac_egr=69578;tip_egr=Servicio;ref_egr=00178;con_egr=Cancelación de Bono de Alimentación al Personal Fijo y Contratado correspondiente al mes de Abril;mon_egr=10027.62;mon_iva_egr=32.10;ret_iva_egr=75;ret_isrl_egr=2;npr_egr=;ela_egr=T.S.U. Luis Márquez Briceño ;rev_egr=Lcda. Luz Marina Salcedo;apr_egr=Abog. José Andrés Briceño Valero;cont_egr=Lcdo. Gilberto Uzcátegui;obs_egr=','egresos','2011-06-23','12:37:00'),(156,'15031097','insertar','cod_egr=;fch_egr=2011-06-01;nor_egr=1;rif_pro=J300180174;aut_egr=;cod_ban=3;chq_egr=11004859;fac_egr=S/N;tip_egr=Servicio;ref_egr=00178;con_egr=Cancelación de Bono de Alimentación al Personal Fijo y Contratado correspondiente al mes de Abril;mon_egr=10027.62;mon_iva_egr=32.10;ret_iva_egr=75;ret_isrl_egr=2;npr_egr=;ela_egr=T.S.U. Luis Márquez Briceño ;rev_egr=Lcda. Luz Marina Salcedo;apr_egr=Abog. José Andrés Briceño Valero;cont_egr=Lcdo. Gilberto Uzcátegui;obs_egr=','egresos','2011-06-23','12:40:00'),(157,'15031097','insertar','cod_egr=;fch_egr=2011-06-01;nor_egr=1;rif_pro=J300180174;aut_egr=;cod_ban=3;chq_egr=11004859;fac_egr=S/N;tip_egr=Servicio;ref_egr=00178;con_egr=Cancelación de Bono de Alimentación al Personal Fijo y Contratado correspondiente al mes de Abril;mon_egr=10027.62;mon_iva_egr=32.10;ret_iva_egr=75;ret_isrl_egr=2;npr_egr=;ela_egr=T.S.U. Luis Márquez Briceño ;rev_egr=Lcda. Luz Marina Salcedo;apr_egr=Abog. José Andrés Briceño Valero;cont_egr=Lcdo. Gilberto Uzcátegui;obs_egr=','egresos','2011-06-23','12:46:00'),(158,'15031097','insertar','cod_egr=;fch_egr=2011-06-01;nor_egr=2;rif_pro=J300180174;aut_egr=;cod_ban=3;chq_egr=11004859;fac_egr=S/N;tip_egr=Servicio;ref_egr=00178;con_egr=Cancelación de Bono de Alimentación al Personal Fijo y Contratado correspondiente al mes de Abril;mon_egr=10027.62;mon_iva_egr=32.10;ret_iva_egr=75;ret_isrl_egr=2;npr_egr=;ela_egr=T.S.U. Luis Márquez Briceño ;rev_egr=Lcda. Luz Marina Salcedo;apr_egr=Abog. José Andrés Briceño Valero;cont_egr=Lcdo. Gilberto Uzcátegui;obs_egr=','egresos','2011-06-23','12:46:00'),(159,'15031097','insertar','cod_egr=;fch_egr=2011-06-01;nor_egr=1;rif_pro=J300180174;aut_egr=;cod_ban=3;chq_egr=11004859;fac_egr=S/N;tip_egr=Servicio;ref_egr=00178;con_egr=Cancelación de Bono de Alimentación al Personal Fijo y Contratado correspondiente al mes de Abril;mon_egr=10027.62;mon_iva_egr=32.10;ret_iva_egr=75;ret_isrl_egr=2;npr_egr=;ela_egr=T.S.U. Luis Márquez Briceño ;rev_egr=Lcda. Luz Marina Salcedo;apr_egr=Abog. José Andrés Briceño Valero;cont_egr=Lcdo. Gilberto Uzcátegui;obs_egr=','egresos','2011-06-23','12:49:00'),(160,'15031097','insertar','cod_egr=;fch_egr=2011-06-01;nor_egr=1;rif_pro=J300180174;aut_egr=;cod_ban=3;chq_egr=11004859;fac_egr=S/N;tip_egr=Servicio;ref_egr=00178;con_egr=Cancelación de Bono de Alimentación al Personal Fijo y Contratado correspondiente al mes de Abril;mon_egr=10027.62;mon_iva_egr=32.10;ret_iva_egr=75;ret_isrl_egr=2;npr_egr=;ela_egr=T.S.U. Luis Márquez Briceño ;rev_egr=Lcda. Luz Marina Salcedo;apr_egr=Abog. José Andrés Briceño Valero;cont_egr=Lcdo. Gilberto Uzcátegui;obs_egr=','egresos','2011-06-23','12:51:00'),(161,'15031097','insertar','cod_egr=;fch_egr=2011-05-01;nor_egr=2;rif_pro=J300180174;aut_egr=;cod_ban=3;chq_egr=11004859;fac_egr=S/N;tip_egr=Servicio;ref_egr=00178;con_egr=Cancelación de Bono de Alimentación al Personal Fijo y Contratado correspondiente al mes de Abril;mon_egr=10027.62;mon_iva_egr=32.10;ret_iva_egr=75;ret_isrl_egr=2;npr_egr=;ela_egr=T.S.U. Luis Márquez Briceño ;rev_egr=Lcda. Luz Marina Salcedo;apr_egr=Abog. José Andrés Briceño Valero;cont_egr=Lcdo. Gilberto Uzcátegui;obs_egr=','egresos','2011-06-23','12:52:00'),(162,'15031097','modificar','cod_pro=0004; fch_pro=2011-01-01; rif_pro=J300180174; nom_pro=Sodexho pass venzuela ; dir_pro=con Av los chaguaramos torre corp banca piso 16 la castellar ; act_pro=Emision, comercializacion; cap_pro=2250000.00; cas_pro=2250000.00; tel_pro=02122065625; fax_pro=02122065620; per_pro=J; ofi_reg_pro=Registro mercantil segundo  del esdo miranda; num_reg_pro=75; tom_reg_pro=154-A; fol_reg_pro=5; fch_reg_pro=2007-07-07; num_mod_pro=0; tom_mod_pro=; fol_mod_pro=; fch_mod_pro=0000-00-00; pat_pro=; ivss_pro=; ince_pro=; nom_rep_pro=nicolas ; ape_rep_pro=vaga; ced_rep_pro=06972690; dir_rep_pro=; tel_rep_pro=; car_rep_pro=gerente general; ','proveedores','2011-06-23','12:54:00'),(163,'15031097','insertar','cod_egr=;fch_egr=2011-05-03;nor_egr=1;rif_pro=J300180174;aut_egr=;cod_ban=3;chq_egr=11004859;fac_egr=69578;tip_egr=Servicio;ref_egr=00178;con_egr=Cancelación de bono de alimentacion al personal contratado y fijo correspondiente al mes de abril;mon_egr=10027.62;mon_iva_egr=32.10;ret_iva_egr=75;ret_isrl_egr=2;npr_egr=;ela_egr=T.S.U. LUIS MARQUEZ;rev_egr=LCDA LUZ SALCEDO;apr_egr=ABOG. JOSE A. BRICEÑO;cont_egr=LCDO. JOSE G. UZCATEGUI;obs_egr=','egresos','2011-06-23','14:16:00'),(164,'15031097','insertar','cod_com=;fch_com=2011-05-03;nor_com=2;sol_com=S/N;tip_com=Servicio;for_com=Contado;adl_com=0;fre_com=Bono de Alimentación;rif_pro=J300180174;npr_com=;iva_com=12;ela_com=TSU Luis Márquez;rev_com=Lcda. Luz Marina Salcedo;obs_com=','compras','2011-06-23','14:25:00'),(165,'4264264','insertar','num_ofi=1;fch_ofi=2011-06-23;rdp_ofi=Sala Técnica;rpr_ofi=Carlos Julio Puentes Uzcátegui;ddp_ofi=Alcaldia;dpr_ofi=Pedro Alvarez;des_ofi=Solicitud de Info de gestion 2010;nds_ofi=3;fen_ofi=;ftr_ofi=;frs_ofi=;ref_ofi=;obs_ofi=;est_ofi=A','oficios_enviados','2011-06-23','14:32:00'),(166,'15031097','modificar','cod_ofi=1; num_ofi=0001; fch_ofi=2011-06-23; rdp_ofi=Sala Técnica; rpr_ofi=Carlos Julio Puentes Uzcátegui; ddp_ofi=Alcaldia; dpr_ofi=Pedro Alvarez; des_ofi=Solicitud de Info de gestion 2010; nds_ofi=3; fen_ofi=0000-00-00; ftr_ofi=0000-00-00; frs_ofi=0000-00-00; ref_ofi=; obs_ofi=; est_ofi=A; ','oficios_enviados','2011-06-23','14:34:00'),(167,'15031097','modificar','cod_ofi=1; num_ofi=0001; fch_ofi=2011-06-23; rdp_ofi=Sala Técnica; rpr_ofi=Carlos Julio Puentes Uzcátegui; ddp_ofi=Alcaldia; dpr_ofi=Pedro Alvarez; des_ofi=Solicitud de Info de gestion 2010; nds_ofi=3; fen_ofi=2011-06-23; ftr_ofi=2011-06-28; frs_ofi=0000-00-00; ref_ofi=; obs_ofi=; est_ofi=A; ','oficios_enviados','2011-06-23','14:36:00'),(168,'15031097','modificar','cod_ofi=1; num_ofi=0001; fch_ofi=2011-06-23; rdp_ofi=Sala Técnica; rpr_ofi=Carlos Julio Puentes Uzcátegui; ddp_ofi=Alcaldia; dpr_ofi=Pedro Alvarez; des_ofi=Solicitud de Info de gestion 2010; nds_ofi=3; fen_ofi=2011-06-20; ftr_ofi=2011-06-23; frs_ofi=0000-00-00; ref_ofi=; obs_ofi=; est_ofi=A; cod_ofi=1; num_ofi=0001; fch_ofi=2011-06-23; rdp_ofi=Sala Técnica; rpr_ofi=Carlos Julio Puentes Uzcátegui; ddp_ofi=Alcaldia; dpr_ofi=Pedro Alvarez; des_ofi=Solicitud de Info de gestion 2010; nds_ofi=3; fen_ofi=2011-06-20; ftr_ofi=2011-06-23; frs_ofi=0000-00-00; ref_ofi=; obs_ofi=; est_ofi=A; ','oficios_enviados','2011-06-23','14:41:00'),(169,'15031097','insertar','fch_frd=2011-06-24;des_frd=Dia de San Juan / Batalla de Carabobo','feriados','2011-06-23','14:42:00'),(170,'15031097','insertar','ced_per=4264264;fch_ina=2011-06-23;tip_ina=No Remunerada;des_ina=Permiso medico','inasistencias','2011-06-23','14:44:00'),(171,'4264264','modificar','cod_ofi=1; num_ofi=0001; fch_ofi=2011-06-23; rdp_ofi=Sala Técnica; rpr_ofi=Carlos Julio Puentes Uzcátegui; ddp_ofi=Alcaldia; dpr_ofi=Pedro Alvarez; des_ofi=Solicitud de Info de gestion 2010; nds_ofi=3; fen_ofi=2011-06-20; ftr_ofi=2011-06-23; frs_ofi=0000-00-00; ref_ofi=; obs_ofi=; est_ofi=P; ','oficios_enviados','2011-06-23','14:59:00'),(172,'15031097','modificar','cod_pro=0007; fch_pro=2011-01-01; rif_pro=J037653213; nom_pro=Prove Oficina Sosa; dir_pro=Av Don tulio febres cordero entre calle 29y30merida ; act_pro=comerciante; cap_pro=10.00; cas_pro=10.00; tel_pro=02742529551; fax_pro=; per_pro=J; ofi_reg_pro=Megistro mercantil primero del estado merida; num_reg_pro=64; tom_reg_pro=5; fol_reg_pro=5; fch_reg_pro=2007-06-26; num_mod_pro=0; tom_mod_pro=; fol_mod_pro=; fch_mod_pro=0000-00-00; pat_pro=; ivss_pro=; ince_pro=; nom_rep_pro=Alexis; ape_rep_pro=Sosa; ced_rep_pro=03765321; dir_rep_pro=Av don tulio febres cordero entre calle 29y30 merida; tel_rep_pro=04147742018; car_rep_pro=representante legal; ','proveedores','2011-06-23','15:26:00'),(173,'15031097','modificar','ced_per=6343267; nac_per=V; nom_per=Luz Marina; ape_per=Salcedo Pernia; sex_per=F; fnac_per=1970-08-27; lnac_per=caracas; cor_per=luzmarsal@hotmail.com; pro_per=lic contaduria  publica; dir_per=URB Hacienda zumba calle 4-B Araguaney 298; tel_per=02745118815; cel_per=04166657051; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-05-03; ','personal','2011-06-27','08:40:00'),(174,'15031097','modificar','ced_per=8029735; nac_per=V; nom_per=Nora Coromoto; ape_per=Cerrada; sex_per=F; fnac_per=1961-04-19; lnac_per=Merida; cor_per=Secrenor@hotmail.com; pro_per=Secretaria ; dir_per=Manzano alto via la panamericana casa n°031; tel_per=02748085555; cel_per=04169760118; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-05-03; ','personal','2011-06-27','08:41:00'),(175,'15031097','modificar','ced_per=10109577; nac_per=V; nom_per=Maria Isabel; ape_per=Rivas de Osuna; sex_per=F; fnac_per=1967-11-05; lnac_per=Merida; cor_per=u_auditoriaint@hotmail.com; pro_per=lic contaduria  publica; dir_per=urb zumba calle 4 A N°556; tel_per=2212896; cel_per=04141413561; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-05-03; ','personal','2011-06-27','08:42:00'),(176,'15031097','modificar','ced_per=14304657; nac_per=V; nom_per=leidy josefina; ape_per=pereira ochoa; sex_per=F; fnac_per=1979-04-07; lnac_per=Bejuma edo carabobo; cor_per=ladypereirao@hotmail.com; pro_per=tecnico superior en contaduria; dir_per=av centenario, residencia el molino torre 10 apto 1-4; tel_per=02744168377; cel_per=04147458942; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-05-03; ','personal','2011-06-27','08:43:00'),(177,'15031097','modificar','ced_per=16933715; nac_per=V; nom_per=Alejandro Antonio; ape_per=Sayago Gonzales ; sex_per=M; fnac_per=1984-12-19; lnac_per=Merida edo Merida; cor_per=alejandro_s1@hotmail.com; pro_per=Abogado; dir_per=AV 16 de Septiembre casa nro 1-12\r\nSan Jose de Obrero  zona postal; tel_per=; cel_per=04147086961; lph_des=A; spf_des=A; sso_des=A; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-06-27','08:44:00'),(178,'15031097','modificar','ced_per=8036119; nac_per=V; nom_per=Elda Soraya; ape_per=Hill Davila; sex_per=F; fnac_per=1965-10-29; lnac_per=Merida edo Merida; cor_per=servijdcos@hotmail.com; pro_per=Abogada; dir_per=chamita calle las Acasias Nº1-278 Merida ; tel_per=02742665325; cel_per=04165778653; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-06-27','08:44:00'),(179,'15031097','modificar','ced_per=8019652; nac_per=V; nom_per=Oswaldo Ramon ; ape_per=Ramos; sex_per=M; fnac_per=1962-02-06; lnac_per=Barquisimeto Edo Lara; cor_per=oswalramra@hotmail.com; pro_per=analistas y programador desistema de computacionn ; dir_per=urb la mata serrania casa clb , torre13-0-c ; tel_per=02745111050; cel_per=04166742323; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-05-03; ','personal','2011-06-27','08:45:00'),(180,'15031097','modificar','ced_per=9028505; nac_per=V; nom_per=Ramón Antonio; ape_per=Fernández Roja; sex_per=M; fnac_per=1962-09-01; lnac_per=vigia ; cor_per=ramonactividad@hotmail.com; pro_per=ing Mecanico; dir_per=palmo calle 5 de julio casa Nº 0-13; tel_per=02742212574; cel_per=04166265746; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-06-27','08:45:00'),(181,'15031097','modificar','ced_per=12346028; nac_per=V; nom_per=Jose de Jesus ; ape_per=Corredor Alarcon; sex_per=M; fnac_per=1974-01-30; lnac_per=Merida edo Merida; cor_per=tscorredor@hotmail.com; pro_per=inspector de obras; dir_per=Manzano Bajo calle urdaneta Nº 32ejido edo merida; tel_per=02744165181; cel_per=04147484178; lph_des=A; spf_des=A; sso_des=A; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-06-27','08:46:00'),(182,'15031097','modificar','ced_per=11960592; nac_per=V; nom_per=Jesus Ramón; ape_per=Silva Osuna; sex_per=M; fnac_per=1975-03-23; lnac_per=Merida edo Merida; cor_per=je2375@hotmail.com; pro_per= tsu construcion civil; dir_per=calle camejo,ejido.RES El trigal EDF APTO 2-3; tel_per=02742213223; cel_per=04149785308; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-06-27','08:47:00'),(183,'15031097','modificar','ced_per=4264264; nac_per=V; nom_per=Carlos Julio ; ape_per=Puentes Uzcatequi ; sex_per=M; fnac_per=1955-12-04; lnac_per=La Azulita edo Merida ; cor_per=puentesuzca@hotmail.com; pro_per=Arquitecto; dir_per=urb.j.j Osuna Rodriquez bloque 20apt 00-04; tel_per=02744172160; cel_per=04168002030; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-06-27','08:48:00'),(184,'15031097','modificar','ced_per=10715628; nac_per=V; nom_per=Yoly Josefina; ape_per=Avendaño Meza; sex_per=F; fnac_per=1970-06-02; lnac_per=Merida edo Merida; cor_per=; pro_per=Contador publico; dir_per=Santa elena calle 9 Nº10-45c; tel_per=02742630667; cel_per=04268283389; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; fch_reg=2011-04-25; ','personal','2011-06-27','08:49:00'),(185,'15031097','modificar','ced_per=15031097; nac_per=V; nom_per=Luis Felipe; ape_per=Márquez Briceño; sex_per=M; fnac_per=1981-03-09; lnac_per=Mérida; cor_per=felixpe09@hotmail.com; pro_per=TSU INFORMATICA; dir_per=ASOPRIETO CALLE 4A #242, EJIDO; tel_per=02742217980; cel_per=04266736316; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-06-28','08:08:00'),(186,'15031097','modificar','ced_per=15031097; nac_per=V; nom_per=Luis Felipe; ape_per=Márquez Briceño; sex_per=M; fnac_per=1981-03-09; lnac_per=Mérida; cor_per=felixpe09@hotmail.com; pro_per=TSU INFORMATICA; dir_per=ASOPRIETO CALLE 4A #242, EJIDO; tel_per=02742217980; cel_per=04266736316; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=A; jer_asg=A; otr_asg=A; fch_reg=2011-04-25; ','personal','2011-06-28','08:10:00'),(187,'15031097','modificar','ced_per=15031097; nac_per=V; nom_per=Luis Felipe; ape_per=Márquez Briceño; sex_per=M; fnac_per=1981-03-09; lnac_per=Mérida; cor_per=felixpe09@hotmail.com; pro_per=TSU INFORMATICA; dir_per=ASOPRIETO CALLE 4A #242, EJIDO; tel_per=02742217980; cel_per=04266736316; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=A; jer_asg=A; otr_asg=A; fch_reg=2011-04-25; ','personal','2011-06-28','08:11:00'),(188,'15031097','modificar','ced_per=4264264; nac_per=V; nom_per=Carlos Julio ; ape_per=Puentes Uzcatequi ; sex_per=M; fnac_per=1955-12-04; lnac_per=La Azulita edo Merida ; cor_per=puentesuzca@hotmail.com; pro_per=Arquitecto; dir_per=urb.j.j Osuna Rodriquez bloque 20apt 00-04; tel_per=02744172160; cel_per=04168002030; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-06-28','08:17:00'),(189,'15031097','modificar','ced_per=4264264; num_cue=01050747200747054045; tip_cue=2; fcam_cue=2011-04-25; ban_cue=Mercantil; ','cuentas','2011-06-28','08:33:00'),(190,'15031097','modificar','ced_per=8023210; nac_per=V; nom_per=Jose Andres ; ape_per=Briceño Valero; sex_per=M; fnac_per=1961-11-10; lnac_per=Merida edo Merida; cor_per=josemeridave@hotmail.com; pro_per=Abogado; dir_per= URB J.J OSUMA RODRIGUEZ VEREDA 17 .Nº07 MERIDA; tel_per=02742714008; cel_per=04162749656; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-06-30','13:47:00'),(191,'15031097','modificar','ced_per=6343267; nac_per=V; nom_per=Luz Marina; ape_per=Salcedo Pernia; sex_per=F; fnac_per=1970-08-27; lnac_per=caracas; cor_per=luzmarsal@hotmail.com; pro_per=lic contaduria  publica; dir_per=URB Hacienda zumba calle 4-B Araguaney 298; tel_per=02745118815; cel_per=04166657051; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ','personal','2011-06-30','13:48:00'),(192,'15031097','modificar','ced_per=6343267; nac_per=V; nom_per=Luz Marina; ape_per=Salcedo Pernia; sex_per=F; fnac_per=1970-08-27; lnac_per=caracas; cor_per=luzmarsal@hotmail.com; pro_per=lic contaduria  publica; dir_per=URB Hacienda zumba calle 4-B Araguaney 298; tel_per=02745118815; cel_per=04166657051; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=A; otr_asg=A; fch_reg=2011-05-03; ','personal','2011-06-30','15:19:00'),(193,'15031097','insertar','fch_frd=2011-07-04;des_frd=Decreto Presidencial como júbilo','feriados','2011-07-05','16:59:00'),(194,'15031097','insertar','fch_frd=2011-07-05;des_frd=Día de la Independencia Venezolana','feriados','2011-07-05','16:59:00'),(195,'15031097','insertar','fch_frd=2011-07-24;des_frd=Día de la Muerte del Libertador Simón Bolívar','feriados','2011-07-05','17:00:00'),(196,'15031097','modificar','ced_per=10109577; nac_per=V; nom_per=Maria Isabel; ape_per=Rivas de Osuna; sex_per=F; fnac_per=1967-11-05; lnac_per=Merida; cor_per=u_auditoriaint@hotmail.com; pro_per=lic contaduria  publica; dir_per=urb zumba calle 4 A N°556; tel_per=2212896; cel_per=04141413561; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ','personal','2011-07-05','17:19:00'),(197,'15031097','modificar','cod_fam=28; ced_per=15031097; ced_fam=; nom_fam=Felipe Gabriel; ape_fam=Márquez Dávila; sex_fam=M; fnac_fam=1990-10-25; par_fam=H; est_fam=S; obs_fam=; ','familiares','2011-07-05','17:25:00'),(198,'15031097','modificar','cod_fam=28; ced_per=15031097; ced_fam=; nom_fam=Felipe Gabriel; ape_fam=Márquez Dávila; sex_fam=M; fnac_fam=1992-10-25; par_fam=H; est_fam=S; obs_fam=; ','familiares','2011-07-05','17:25:00'),(199,'15031097','modificar','cod_fam=28; ced_per=15031097; ced_fam=; nom_fam=Felipe Gabriel; ape_fam=Márquez Dávila; sex_fam=M; fnac_fam=1991-10-25; par_fam=H; est_fam=S; obs_fam=; ','familiares','2011-07-05','17:25:00'),(200,'15031097','modificar','ced_per=15031097; nac_per=V; nom_per=Luis Felipe; ape_per=Márquez Briceño; sex_per=M; fnac_per=1981-03-09; lnac_per=Mérida; cor_per=felixpe09@hotmail.com; pro_per=TSU INFORMATICA; dir_per=ASOPRIETO CALLE 4A #242, EJIDO; tel_per=02742217980; cel_per=04266736316; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ced_per=6343267; nac_per=V; nom_per=Luz Marina; ape_per=Salcedo Pernia; sex_per=F; fnac_per=1970-08-27; lnac_per=caracas; cor_per=luzmarsal@hotmail.com; pro_per=lic contaduria  publica; dir_per=URB Hacienda zumba calle 4-B Araguaney 298; tel_per=02745118815; cel_per=04166657051; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=A; otr_asg=A; fch_reg=2011-05-03; ','personal','2011-07-05','17:26:00'),(201,'15031097','modificar','ced_per=10715628; nac_per=V; nom_per=Yoly Josefina; ape_per=Avendaño Meza; sex_per=F; fnac_per=1970-06-02; lnac_per=Merida edo Merida; cor_per=; pro_per=Contador publico; dir_per=Santa elena calle 9 Nº10-45c; tel_per=02742630667; cel_per=04268283389; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-07-05','17:38:00'),(202,'15031097','modificar','ced_per=15621190; nac_per=V; nom_per=Yohana; ape_per=Perez de Valero; sex_per=F; fnac_per=1981-09-06; lnac_per=Merida edo Merida; cor_per=yoha19perez@hotmail.com; pro_per=lic Contaduria Publica; dir_per=Sector bella vista calle lara casa Nº54 ejido; tel_per=02742210478; cel_per=04163791718; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=A; hij_asg=A; ant_asg=; pro_asg=A; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-07-05','17:57:00'),(203,'15031097','insertar','con_dsp=Préstamo de Caja de Ahorros;ncuo_dsp=6;per_dsp=;mcuo_dsp=132.12;por_dsp=;bpor_dsp=;cod_des=1;ced_per=6343267;cod_car=3;fch_dsp=2011-01-01;ncp_dsp=','deducciones','2011-07-06','22:40:00'),(204,'15031097','insertar','con_dsp=Préstamo de Caja de Ahorros;ncuo_dsp=5;per_dsp=;mcuo_dsp=524.66;por_dsp=;bpor_dsp=;cod_des=1;ced_per=8023210;cod_car=2;fch_dsp=2011-01-01;ncp_dsp=','deducciones','2011-07-06','22:56:00'),(205,'15031097','modificar','cod_dsp=1; con_dsp=Préstamo Caja de Ahorros; ncuo_dsp=24; per_dsp=; mcuo_dsp=133.27; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=4264264; cod_car=6; fch_dsp=2010-05-01; ncp_dsp=16; ','deducciones','2011-07-06','23:08:00'),(206,'15031097','modificar','ced_per=4264264; nac_per=V; nom_per=Carlos Julio ; ape_per=Puentes Uzcatequi ; sex_per=M; fnac_per=1955-12-04; lnac_per=La Azulita edo Merida ; cor_per=puentesuzca@hotmail.com; pro_per=Arquitecto; dir_per=Calle Ppal Boticario, 5-54. Ejido; tel_per=02744172160; cel_per=04168002030; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=A; otr_asg=; fch_reg=2011-04-25; ced_per=8036119; nac_per=V; nom_per=Elda Soraya; ape_per=Hill Davila; sex_per=F; fnac_per=1965-10-29; lnac_per=Merida edo Merida; cor_per=servijdcos@hotmail.com; pro_per=Abogada; dir_per=chamita calle las Acasias Nº1-278 Merida ; tel_per=02742665325; cel_per=04165778653; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-07-06','23:19:00'),(207,'15031097','insertar','con_dsp=Préstamo de Caja de Ahorros;ncuo_dsp=5;per_dsp=;mcuo_dsp=138.27;por_dsp=;bpor_dsp=;cod_des=1;ced_per=9028505;cod_car=10;fch_dsp=2011-01-01;ncp_dsp=','deducciones','2011-07-06','23:24:00'),(208,'15031097','insertar','ced_per=15031097;tit_edu=Informática;niv_edu=TecS;ins_edu=Instituto Universitario Tecnológico de Ejido;fch_edu=2007-12-12;reg_edu=S','educacion','2011-07-07','15:03:00'),(209,'15031097','modificar','cod_edu=1; ced_per=15031097; tit_edu=Informática; niv_edu=TecS; ins_edu=Instituto Universitario Tecnológico de Ejido; fch_edu=2007-12-12; reg_edu=S; ','educacion','2011-07-07','15:40:00'),(210,'15031097','modificar','ced_per=15031097; nac_per=V; nom_per=Luis Felipe; ape_per=Márquez Briceño; sex_per=M; fnac_per=1981-03-09; lnac_per=Mérida; cor_per=felixpe09@hotmail.com; pro_per=TSU INFORMATICA; dir_per=ASOPRIETO CALLE 4A #242, EJIDO; tel_per=02742217980; cel_per=04266736316; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-07-08','08:54:00'),(211,'15031097','insertar','ced_per=15031097;tit_edu=Ingeniero Informática;niv_edu=Univ;ins_edu=IUTE;fch_edu=2007-12-12;reg_edu=S','educacion','2011-07-08','09:12:00'),(212,'15031097','modificar','cod_edu=2; ced_per=15031097; tit_edu=Ingeniero Informática; niv_edu=Univ; ins_edu=IUTE; fch_edu=2007-12-12; reg_edu=S; ','educacion','2011-07-08','09:12:00'),(213,'15031097','modificar','cod_edu=2; ced_per=15031097; tit_edu=Ingeniero Informática; niv_edu=Esp; ins_edu=IUTE; fch_edu=2007-12-12; reg_edu=S; ','educacion','2011-07-08','09:13:00'),(214,'15031097','modificar','ced_per=15031097; nac_per=V; nom_per=Luis Felipe; ape_per=Márquez Briceño; sex_per=M; fnac_per=1981-03-09; lnac_per=Mérida; cor_per=felixpe09@hotmail.com; pro_per=TSU INFORMATICA; dir_per=ASOPRIETO CALLE 4A #242, EJIDO; tel_per=02742217980; cel_per=04266736316; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=A; hje_asg=; jer_asg=; otr_asg=A; fch_reg=2011-04-25; ','personal','2011-07-08','09:14:00'),(215,'15031097','insertar','ced_per=6343267;tit_edu=Licenciada Contaduría Pública;niv_edu=Univ;ins_edu=Universidad de Los Andes (ULA);fch_edu=1995-07-21;reg_edu=S','educacion','2011-07-08','11:48:00'),(216,'15031097','insertar','ced_per=6343267;tit_edu=Gerencia Mención Finanzas;niv_edu=Msc;ins_edu=Universidad Bicentenaria de Aragua;fch_edu=2002-12-12;reg_edu=S','educacion','2011-07-08','11:49:00'),(217,'12345678','insertar','nom_grp=BienesP;des_grp=Grupo de Pasantes','grupos','2011-07-08','12:59:00'),(218,'12345678','insertar','cod_usr=19422787;nom_usr=Luzmar;ape_usr=Contreras S;freg_usr=2011-07-08;log_usr=luzmar;pas_usr=123456;cod_grp=5;fcam_usr=2011-07-08;sts_usr=A','usuarios','2011-07-08','13:00:00'),(219,'15031097','modificar','cod_sue=1; mon_sue=6119.45; des_sue=Contralor; ','sueldos','2011-07-08','13:47:00'),(220,'15031097','modificar','cod_dep=ADM; nom_dep=Administración; cod_act=; cod_pro=; ','dependencias','2011-07-13','00:49:00'),(221,'15031097','insertar','cod_dep=PRU;nom_dep=PRUEBA;cod_act=;cod_pro=','dependencias','2011-07-13','00:52:00'),(222,'15031097','modificar','cod_dep=PRU; nom_dep=PRUEBA; cod_act=; cod_pro=; ','dependencias','2011-07-13','00:53:00'),(223,'15031097','eliminar','cod_dep=PRU;nom_dep=PRUEBAS;cod_act=;cod_pro=','dependencias','2011-07-13','00:53:00'),(224,'15031097','insertar','cod_dep=PRU;nom_dep=PRUEBA;cod_act=;cod_pro=','dependencias','2011-07-13','00:57:00'),(225,'15031097','modificar','cod_dep=PRU; nom_dep=PRUEBA; cod_act=; cod_pro=; ','dependencias','2011-07-13','00:57:00'),(226,'15031097','modificar','cod_dep=PRU; nom_dep=PRUEBAs; cod_act=; cod_pro=; ','dependencias','2011-07-13','00:57:00'),(227,'15031097','modificar','cod_dep=PRU; nom_dep=PRUEBA; cod_act=; cod_pro=; ','dependencias','2011-07-13','00:58:00'),(228,'15031097','eliminar','cod_dep=PRU;nom_dep=PRUEBA;cod_act=;cod_pro=','dependencias','2011-07-13','00:59:00'),(229,'15031097','insertar','fch_frd=2011-07-13;des_frd=hoy','feriados','2011-07-13','01:29:00'),(230,'15031097','modificar','fch_frd=2011-07-13; des_frd=hoy; ','feriados','2011-07-13','01:29:00'),(231,'15031097','modificar','fch_frd=2011-07-13; des_frd=hoys; ','feriados','2011-07-13','01:30:00'),(232,'15031097','eliminar','fch_frd=2011-07-13;des_frd=hoy','feriados','2011-07-13','01:30:00'),(233,'15031097','insertar','fch_frd=2011-07-13;des_frd=hoy','feriados','2011-07-13','01:32:00'),(234,'15031097','modificar','fch_frd=2011-07-13; des_frd=hoy; ','feriados','2011-07-13','01:33:00'),(235,'15031097','eliminar','fch_frd=2011-07-13;des_frd=hoy','feriados','2011-07-13','01:33:00'),(236,'15031097','modificar','fch_frd=2011-06-24; des_frd=Dia de San Juan / Batalla de Carabobo; ','feriados','2011-07-13','01:49:00'),(237,'15031097','insertar','fch_frd=2011-07-13;des_frd=hoy','feriados','2011-07-13','01:50:00'),(238,'15031097','modificar','fch_frd=2011-07-13; des_frd=hoy; ','feriados','2011-07-13','01:50:00'),(239,'15031097','modificar','fch_frd=2011-07-13; des_frd=hoy; ','feriados','2011-07-13','01:50:00'),(240,'15031097','modificar','fch_frd=2011-07-13; des_frd=hoy dia; ','feriados','2011-07-13','01:51:00'),(241,'15031097','eliminar','fch_frd=2011-07-13;des_frd=hoy dia','feriados','2011-07-13','01:51:00'),(242,'15031097','insertar','cod_egr=;fch_egr=2011-07-15;nor_egr=2;rif_pro=J298994940;aut_egr=;cod_ban=1;chq_egr=guigh;fac_egr=jhghjk;tip_egr=Compra;ref_egr=kjh;con_egr=jbhkjh;mon_egr=1156.68;mon_iva_egr=123.93;ret_iva_egr=75;ret_isrl_egr=5;npr_egr=;ela_egr=T.S.U. Luis Márquez Briceño ;rev_egr=Lcda. Luz Marina Salcedo;apr_egr=Abog. José Andrés Briceño Valero;cont_egr=Lcdo. Gilberto Uzcátegui;obs_egr=','egresos','2011-07-15','12:09:00'),(243,'15031097','insertar','ced_per=10109577;tit_edu=Lic. Administración de Empresas;niv_edu=Univ;ins_edu=Universidad de Los Andes (ULA);fch_edu=1995-01-20;reg_edu=S','educacion','2011-07-15','13:07:00'),(244,'15031097','insertar','ced_per=10109577;tit_edu=Licenciada Contaduría Pública;niv_edu=Univ;ins_edu=Universidad Nacional Abierta (UNA);fch_edu=2005-01-15;reg_edu=S','educacion','2011-07-15','13:09:00'),(245,'15031097','insertar','ced_per=14304657;tit_edu=TSU en Contaduría Pública;niv_edu=TecS;ins_edu=Instituto Universitario Tecnológico de Ejido;fch_edu=2005-04-22;reg_edu=S','educacion','2011-07-18','08:51:00'),(246,'15031097','insertar','con_dsp=Préstamo de Caja de Ahorros;ncuo_dsp=48;per_dsp=;mcuo_dsp=135.38;por_dsp=;bpor_dsp=;cod_des=1;ced_per=8029735;cod_car=4;fch_dsp=2010-07-12;ncp_dsp=','deducciones','2011-07-18','08:56:00'),(247,'15031097','insertar','ced_per=8036119;tit_edu=Abogado;niv_edu=Univ;ins_edu=Universidad de Los Andes (ULA);fch_edu=1990-04-20;reg_edu=S','educacion','2011-07-18','09:01:00'),(248,'15031097','insertar','ced_per=16933715;tit_edu=Abogado;niv_edu=Univ;ins_edu=Universidad de Los Andes (ULA);fch_edu=2007-12-07;reg_edu=S','educacion','2011-07-18','09:05:00'),(249,'15031097','insertar','ced_per=4264264;tit_edu=Arquitecto;niv_edu=Univ;ins_edu=Universidad de Los Andes (ULA);fch_edu=1989-07-27;reg_edu=S','educacion','2011-07-18','10:46:00'),(250,'15031097','insertar','ced_per=11960597;tit_edu=TSU en Construcción Civil;niv_edu=TecS;ins_edu=Instituto Universitario Tecnológico de Ejido;fch_edu=1996-06-14;reg_edu=S','educacion','2011-07-18','10:57:00'),(251,'15031097','insertar','con_dsp=Préstamo de Caja de Ahorros;ncuo_dsp=36;per_dsp=;mcuo_dsp=132.10;por_dsp=;bpor_dsp=;cod_des=1;ced_per=11960597;cod_car=14;fch_dsp=2011-05-17;ncp_dsp=','deducciones','2011-07-18','10:59:00'),(252,'15031097','insertar','ced_per=12346028;tit_edu=TSU en Construcción Civil;niv_edu=TecS;ins_edu=Instituto Universitario Tecnológico de Ejido;fch_edu=1996-06-14;reg_edu=S','educacion','2011-07-18','11:04:00'),(253,'15031097','insertar','ced_per=10715628;tit_edu=Licenciada Contaduría Pública;niv_edu=Univ;ins_edu=Universidad de Los Andes (ULA);fch_edu=1993-10-22;reg_edu=S','educacion','2011-07-18','11:09:00'),(254,'15031097','insertar','ced_per=10715628;tit_edu=Especialista en Ciencias Contables: Mención Tribut;niv_edu=Esp;ins_edu=Universidad de Los Andes (ULA);fch_edu=2001-05-02;reg_edu=S','educacion','2011-07-18','11:12:00'),(255,'15031097','modificar','cod_edu=13; ced_per=10715628; tit_edu=Especialista en Ciencias Contables: Mención Tribut; niv_edu=Esp; ins_edu=Universidad de Los Andes (ULA); fch_edu=2001-05-02; reg_edu=S; ','educacion','2011-07-18','11:14:00'),(256,'15031097','modificar','cod_fam=33; ced_per=15621190; ced_fam=; nom_fam=Diego Josue; ape_fam=Valero Pérez; sex_fam=M; fnac_fam=2001-03-15; par_fam=H; est_fam=; obs_fam=; ','familiares','2011-07-18','11:22:00'),(257,'15031097','modificar','cod_edu=13; ced_per=10715628; tit_edu=Especialista en Ciencias Contables Mención Tributo; niv_edu=Esp; ins_edu=Universidad de Los Andes (ULA); fch_edu=2001-05-02; reg_edu=S; ','educacion','2011-07-18','11:22:00'),(258,'15031097','insertar','con_dsp=Préstamo de Caja de Ahorros;ncuo_dsp=48;per_dsp=;mcuo_dsp=316.57;por_dsp=;bpor_dsp=;cod_des=1;ced_per=10715628;cod_car=7;fch_dsp=2011-05-30;ncp_dsp=','deducciones','2011-07-18','11:26:00'),(259,'15031097','insertar','con_dsp=Préstamo de Caja de Ahorros;ncuo_dsp=36;per_dsp=;mcuo_dsp=168.79;por_dsp=;bpor_dsp=;cod_des=1;ced_per=15621190;cod_car=12;fch_dsp=2011-05-30;ncp_dsp=','deducciones','2011-07-18','11:27:00'),(260,'15031097','modificar','cod_dsp=2; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=6; per_dsp=; mcuo_dsp=132.12; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=6343267; cod_car=3; fch_dsp=2011-01-01; ncp_dsp=0; ','deducciones','2011-07-18','11:29:00'),(261,'15031097','modificar','cod_edu=1; ced_per=15031097; tit_edu=En Informática; niv_edu=TecS; ins_edu=Instituto Universitario Tecnológico de Ejido; fch_edu=2007-12-12; reg_edu=S; ','educacion','2011-07-18','11:30:00'),(262,'15031097','insertar','ced_per=8023210;tit_edu=Abogado;niv_edu=Univ;ins_edu=Universidad de Los Andes (ULA);fch_edu=1989-07-21;reg_edu=S','educacion','2011-07-18','11:34:00'),(263,'15031097','insertar','ced_per=8023210;tit_edu=Especialista en Gerencia Municipal;niv_edu=Esp;ins_edu=Universidad Santa María;fch_edu=1998-10-09;reg_edu=S','educacion','2011-07-18','11:36:00'),(264,'15031097','insertar','ced_per=8023210;tit_edu=Especialista en Derecho Administrativo;niv_edu=Esp;ins_edu=Universidad de Los Andes (ULA);fch_edu=2009-10-23;reg_edu=S','educacion','2011-07-18','11:43:00'),(265,'15031097','modificar','cod_dsp=3; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=5; per_dsp=; mcuo_dsp=524.66; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=8023210; cod_car=2; fch_dsp=2011-01-01; ncp_dsp=0; ','deducciones','2011-07-18','11:50:00'),(266,'15031097','insertar','ced_per=4264264;fch_ina=2011-04-14;tip_ina=No Remunerada;des_ina=Permiso medico','inasistencias','2011-07-18','14:06:00'),(267,'15031097','eliminar','ced_per=4264264;fch_ina=2011-04-23;tip_ina=No Remunerada;des_ina=Permiso medico','inasistencias','2011-07-18','14:36:00'),(268,'15031097','eliminar','ced_per=4264264;fch_ina=2011-04-14;tip_ina=No Remunerada;des_ina=Permiso medico','inasistencias','2011-07-18','14:37:00'),(269,'15031097','modificar','ced_per=4264264; nac_per=V; nom_per=Carlos Julio ; ape_per=Puentes Uzcatequi ; sex_per=M; fnac_per=1955-12-04; lnac_per=La Azulita edo Merida ; cor_per=puentesuzca@hotmail.com; pro_per=Arquitecto; dir_per=Calle Ppal Boticario, 5-54. Ejido; tel_per=02744172160; cel_per=04168002030; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=A; otr_asg=; fch_reg=2011-04-25; ','personal','2011-07-18','14:37:00'),(270,'15031097','insertar','ced_per=6343267;fch_ina=2011-04-05;tip_ina=No Remunerada;des_ina=Permiso','inasistencias','2011-07-18','14:42:00'),(271,'15031097','modificar','cod_ina=3; ced_per=6343267; fch_ina=2011-04-05; tip_ina=No Remunerada; des_ina=Permiso; ','inasistencias','2011-07-18','14:44:00'),(272,'15031097','insertar','ced_per=11960597;fch_ina=2011-04-13;tip_ina=No Remunerada;des_ina=Permiso','inasistencias','2011-07-18','14:50:00'),(273,'15031097','modificar','ced_per=11960597; nac_per=V; nom_per=Jesus Ramón; ape_per=Silva Osuna; sex_per=M; fnac_per=1975-03-23; lnac_per=Merida edo Merida; cor_per=je2375@hotmail.com; pro_per= tsu construcion civil; dir_per=calle camejo,ejido.RES El trigal EDF APTO 2-3; tel_per=02742213223; cel_per=04149785308; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-07-18','14:53:00'),(274,'15031097','modificar','cod_dsp=6; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=36; per_dsp=; mcuo_dsp=132.10; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=11960597; cod_car=14; fch_dsp=2011-05-17; ncp_dsp=0; ','deducciones','2011-07-18','14:57:00'),(275,'15031097','modificar','ced_per=6343267; num_cue=01050065600065576721; tip_cue=2; fcam_cue=2011-05-03; ban_cue=mercantil; ','cuentas','2011-07-25','10:24:00'),(276,'15031097','modificar','ced_per=8036119; num_cue=01050747020074705404; tip_cue=2; fcam_cue=2011-04-25; ban_cue=Mercantil; ','cuentas','2011-07-25','10:28:00'),(277,'15031097','modificar','ced_per=15031097; num_cue=01050092310092239498; tip_cue=2; fcam_cue=2011-05-03; ban_cue=mercantil; ','cuentas','2011-07-25','10:30:00'),(278,'15031097','modificar','ced_per=14304657; num_cue=01050672757672055038; tip_cue=2; fcam_cue=2011-05-03; ban_cue=mercantil; ','cuentas','2011-07-25','10:32:00'),(279,'15031097','modificar','ced_per=10109577; nac_per=V; nom_per=Maria Isabel; ape_per=Rivas de Osuna; sex_per=F; fnac_per=1967-11-05; lnac_per=Merida; cor_per=u_auditoriaint@hotmail.com; pro_per=lic contaduria  publica; dir_per=urb zumba calle 4 A N°556; tel_per=2212896; cel_per=04141413561; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ','personal','2011-07-25','10:47:00'),(280,'15031097','modificar','ced_per=8029735; nac_per=V; nom_per=Nora Coromoto; ape_per=Cerrada; sex_per=F; fnac_per=1961-04-19; lnac_per=Merida; cor_per=Secrenor@hotmail.com; pro_per=Secretaria ; dir_per=Manzano alto via la panamericana casa n°031; tel_per=02748085555; cel_per=04169760118; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ','personal','2011-07-25','10:49:00'),(281,'15031097','modificar','cod_fam=5; ced_per=4264264; ced_fam=4264264; nom_fam=Lindres Valetina ; ape_fam=Puentes Rodriquez; sex_fam=F; fnac_fam=2002-07-14; par_fam=H; est_fam=; obs_fam=; ','familiares','2011-07-25','10:56:00'),(282,'15031097','modificar','ced_per=8019652; nac_per=V; nom_per=Oswaldo Ramon ; ape_per=Ramos; sex_per=M; fnac_per=1962-02-06; lnac_per=Barquisimeto Edo Lara; cor_per=oswalramra@hotmail.com; pro_per=analistas y programador desistema de computacionn ; dir_per=urb la mata serrania casa clb , torre13-0-c ; tel_per=02745111050; cel_per=04166742323; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ','personal','2011-07-25','10:58:00'),(283,'15031097','modificar','ced_per=9028505; nac_per=V; nom_per=Ramón Antonio; ape_per=Fernández Roja; sex_per=M; fnac_per=1962-09-01; lnac_per=vigia ; cor_per=ramonactividad@hotmail.com; pro_per=ing Mecanico; dir_per=palmo calle 5 de julio casa Nº 0-13; tel_per=02742212574; cel_per=04166265746; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-07-25','11:00:00'),(284,'15031097','modificar','ced_per=4264264; nac_per=V; nom_per=Carlos Julio ; ape_per=Puentes Uzcategui ; sex_per=M; fnac_per=1955-12-04; lnac_per=La Azulita edo Merida ; cor_per=puentesuzca@hotmail.com; pro_per=Arquitecto; dir_per=Calle Ppal Boticario, 5-54. Ejido; tel_per=02744172160; cel_per=04168002030; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=A; otr_asg=; fch_reg=2011-04-25; ','personal','2011-07-25','11:01:00'),(285,'15031097','insertar','ced_per=15621190;tit_edu=Licenciada Contaduría Pública;niv_edu=Univ;ins_edu=Universidad de Los Andes (ULA);fch_edu=2008-07-11;reg_edu=S','educacion','2011-07-27','08:20:00'),(286,'15031097','modificar','ced_per=16657924; nac_per=V; nom_per=Maria Alejandra ; ape_per=Perez; sex_per=F; fnac_per=1985-02-23; lnac_per=Merida; cor_per=MARIAP-23@hotmail.com; pro_per=Bachiller; dir_per=Calle el porvenir N° 18; tel_per=; cel_per=04165780214; lph_des=; spf_des=; sso_des=; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ','personal','2011-07-28','19:49:00'),(287,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-01;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:56:00'),(288,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-04;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:56:00'),(289,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-05;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:56:00'),(290,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-06;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:56:00'),(291,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-07;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:57:00'),(292,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-08;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:57:00'),(293,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-11;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:57:00'),(294,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-12;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:57:00'),(295,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-13;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:57:00'),(296,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-14;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:57:00'),(297,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-15;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:57:00'),(298,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-18;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:57:00'),(299,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-19;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:57:00'),(300,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-20;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:58:00'),(301,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-21;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:58:00'),(302,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-22;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:58:00'),(303,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-25;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','10:59:00'),(304,'15031097','insertar','fch_frd=2011-04-19;des_frd=Firma Acta de Independencia','feriados','2011-08-01','11:05:00'),(305,'15031097','eliminar','ced_per=15621190;fch_ina=2011-04-25;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','11:10:00'),(306,'15031097','insertar','fch_frd=2011-04-21;des_frd=Semana Santa','feriados','2011-08-01','11:12:00'),(307,'15031097','insertar','fch_frd=2011-04-22;des_frd=Semana Santa','feriados','2011-08-01','11:12:00'),(308,'15031097','eliminar','ced_per=11960597;fch_ina=2011-04-05;tip_ina=No Remunerada;des_ina=Permiso','inasistencias','2011-08-01','11:13:00'),(309,'15031097','eliminar','ced_per=11960597;fch_ina=2011-04-05;tip_ina=No Remunerada;des_ina=Permiso','inasistencias','2011-08-01','11:13:00'),(310,'15031097','eliminar','ced_per=11960597;fch_ina=2011-04-13;tip_ina=No Remunerada;des_ina=Permiso','inasistencias','2011-08-01','11:14:00'),(311,'15031097','eliminar','ced_per=15621190;fch_ina=2011-04-19;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','11:15:00'),(312,'15031097','eliminar','ced_per=15621190;fch_ina=2011-04-21;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','11:15:00'),(313,'15031097','eliminar','ced_per=15621190;fch_ina=2011-04-22;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','11:16:00'),(314,'15031097','insertar','ced_per=15621190;fch_ina=2011-04-25;tip_ina=Remunerada;des_ina=Post-Natal','inasistencias','2011-08-01','11:16:00'),(315,'15031097','modificar','ced_per=9028505; nac_per=V; nom_per=Ramón Antonio; ape_per=Fernández Roja; sex_per=M; fnac_per=1962-09-01; lnac_per=El Vigía ; cor_per=ramonactividad@hotmail.com; pro_per=Ing. Mecánico; dir_per=Palmo Calle 5 de Julio casa Nº 0-13; tel_per=02742212574; cel_per=04166265746; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-01','11:25:00'),(316,'15031097','modificar','ced_per=10109577; nac_per=V; nom_per=Maria Isabel; ape_per=Rivas de Osuna; sex_per=F; fnac_per=1967-11-05; lnac_per=Merida; cor_per=u_auditoriaint@hotmail.com; pro_per=lic contaduria  publica; dir_per=urb zumba calle 4 A N°556; tel_per=2212896; cel_per=04141413561; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ','personal','2011-08-01','11:25:00'),(317,'15031097','modificar','ced_per=14304657; nac_per=V; nom_per=Leidy Josefina; ape_per=Pereira Ochoa; sex_per=F; fnac_per=1979-04-07; lnac_per=Bejuma edo carabobo; cor_per=ladypereirao@hotmail.com; pro_per=tecnico superior en contaduria; dir_per=av centenario, residencia el molino torre 10 apto 1-4; tel_per=02744168377; cel_per=04147458942; lph_des=A; spf_des=A; sso_des=A; cah_des=; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ced_per=8036119; nac_per=V; nom_per=Elda Soraya; ape_per=Hill Davila; sex_per=F; fnac_per=1965-10-29; lnac_per=Merida edo Merida; cor_per=servijdcos@hotmail.com; pro_per=Abogada; dir_per=chamita calle las Acasias Nº1-278 Merida ; tel_per=02742665325; cel_per=04165778653; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=A; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-01','11:26:00'),(318,'15031097','modificar','ced_per=8029735; nac_per=V; nom_per=Nora Coromoto; ape_per=Cerrada; sex_per=F; fnac_per=1961-04-19; lnac_per=Merida; cor_per=Secrenor@hotmail.com; pro_per=Secretaria ; dir_per=Manzano alto via la panamericana casa n°031; tel_per=02748085555; cel_per=04169760118; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ced_per=16933715; nac_per=V; nom_per=Alejandro Antonio; ape_per=Sayago Gonzales ; sex_per=M; fnac_per=1984-12-19; lnac_per=Merida edo Merida; cor_per=alejandro_s1@hotmail.com; pro_per=Abogado; dir_per=AV 16 de Septiembre casa nro 1-12\r\nSan Jose de Obrero  zona postal; tel_per=; cel_per=04147086961; lph_des=A; spf_des=A; sso_des=A; cah_des=; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-01','11:26:00'),(319,'15031097','modificar','ced_per=8019652; nac_per=V; nom_per=Oswaldo Ramon ; ape_per=Ramos; sex_per=M; fnac_per=1962-02-06; lnac_per=Barquisimeto Edo Lara; cor_per=oswalramra@hotmail.com; pro_per=analistas y programador desistema de computacionn ; dir_per=urb la mata serrania casa clb , torre13-0-c ; tel_per=02745111050; cel_per=04166742323; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ','personal','2011-08-01','11:26:00'),(320,'15031097','modificar','ced_per=9028505; nac_per=V; nom_per=Ramón Antonio; ape_per=Fernández Rojas; sex_per=M; fnac_per=1962-09-01; lnac_per=El Vigía ; cor_per=ramonactividad@hotmail.com; pro_per=Ing. Mecánico; dir_per=Palmo Calle 5 de Julio casa Nº 0-13; tel_per=02742212574; cel_per=04166265746; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-01','11:27:00'),(321,'15031097','modificar','ced_per=16933715; nac_per=V; nom_per=Alejandro Antonio; ape_per=Sayago Gonzáles ; sex_per=M; fnac_per=1984-12-19; lnac_per=Merida edo Merida; cor_per=alejandro_s1@hotmail.com; pro_per=Abogado; dir_per=AV 16 de Septiembre casa nro 1-12\r\nSan Jose de Obrero  zona postal; tel_per=; cel_per=04147086961; lph_des=A; spf_des=A; sso_des=A; cah_des=; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-01','11:27:00'),(322,'15031097','modificar','ced_per=4264264; nac_per=V; nom_per=Carlos Julio ; ape_per=Puentes Uzcategui ; sex_per=M; fnac_per=1955-12-04; lnac_per=La Azulita Edo Merida ; cor_per=puentesuzca@hotmail.com; pro_per=Arquitecto; dir_per=Calle Ppal Boticario, 5-54. Ejido; tel_per=02744172160; cel_per=04168002030; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=A; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-01','11:27:00'),(323,'15031097','modificar','ced_per=11960597; nac_per=V; nom_per=Jesus Ramón; ape_per=Silva Osuna; sex_per=M; fnac_per=1975-03-23; lnac_per=Merida edo Merida; cor_per=je2375@hotmail.com; pro_per= tsu construcion civil; dir_per=calle camejo,ejido.RES El trigal EDF APTO 2-3; tel_per=02742213223; cel_per=04149785308; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-01','11:28:00'),(324,'15031097','modificar','ced_per=12346028; nac_per=V; nom_per=Jose de Jesus ; ape_per=Corredor Alarcon; sex_per=M; fnac_per=1974-01-30; lnac_per=Merida edo Merida; cor_per=tscorredor@hotmail.com; pro_per=inspector de obras; dir_per=Manzano Bajo calle urdaneta Nº 32ejido edo merida; tel_per=02744165181; cel_per=04147484178; lph_des=A; spf_des=A; sso_des=A; cah_des=; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-01','11:28:00'),(325,'15031097','modificar','ced_per=15621190; nac_per=V; nom_per=Yohana; ape_per=Perez de Valero; sex_per=F; fnac_per=1981-09-06; lnac_per=Merida edo Merida; cor_per=yoha19perez@hotmail.com; pro_per=lic Contaduria Publica; dir_per=Sector bella vista calle lara casa Nº54 ejido; tel_per=02742210478; cel_per=04163791718; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-01','11:28:00'),(326,'15031097','modificar','ced_per=10715628; nac_per=V; nom_per=Yoly Josefina; ape_per=Avendaño Meza; sex_per=F; fnac_per=1970-06-02; lnac_per=Merida edo Merida; cor_per=; pro_per=Contador publico; dir_per=Santa elena calle 9 Nº10-45c; tel_per=02742630667; cel_per=04268283389; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=; otr_asg=A; fch_reg=2011-04-25; ced_per=6343267; nac_per=V; nom_per=Luz Marina; ape_per=Salcedo Pernia; sex_per=F; fnac_per=1970-08-27; lnac_per=caracas; cor_per=luzmarsal@hotmail.com; pro_per=lic contaduria  publica; dir_per=URB Hacienda zumba calle 4-B Araguaney 298; tel_per=02745118815; cel_per=04166657051; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=A; otr_asg=A; fch_reg=2011-05-03; ','personal','2011-08-01','11:29:00'),(327,'15031097','modificar','ced_per=8023210; nac_per=V; nom_per=Jose Andres ; ape_per=Briceño Valero; sex_per=M; fnac_per=1961-11-10; lnac_per=Merida edo Merida; cor_per=josemeridave@hotmail.com; pro_per=Abogado; dir_per= URB J.J OSUMA RODRIGUEZ VEREDA 17 .Nº07 MERIDA; tel_per=02742714008; cel_per=04162749656; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-01','11:29:00'),(328,'15031097','modificar','ced_per=16657924; nac_per=V; nom_per=Maria Alejandra ; ape_per=Perez; sex_per=F; fnac_per=1985-02-23; lnac_per=Merida; cor_per=MARIAP-23@hotmail.com; pro_per=Bachiller; dir_per=Calle el porvenir N° 18; tel_per=; cel_per=04165780214; lph_des=A; spf_des=A; sso_des=A; cah_des=; sfu_des=; hog_asg=; hij_asg=; ant_asg=; pro_asg=; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ','personal','2011-08-01','11:29:00'),(329,'15031097','insertar','fch_frd=2011-03-07;des_frd=Carnaval','feriados','2011-08-01','11:46:00'),(330,'15031097','insertar','fch_frd=2011-03-08;des_frd=Carnaval','feriados','2011-08-01','11:46:00'),(331,'15031097','insertar','ced_per=8019652;fch_ina=2011-03-01;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:13:00'),(332,'15031097','insertar','ced_per=8019652;fch_ina=2011-03-02;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:13:00'),(333,'15031097','insertar','ced_per=8019652;fch_ina=2011-03-03;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:13:00'),(334,'15031097','insertar','ced_per=8019652;fch_ina=2011-03-04;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:13:00'),(335,'15031097','insertar','ced_per=8019652;fch_ina=2011-03-07;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:13:00'),(336,'15031097','insertar','ced_per=8019652;fch_ina=2011-03-08;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:14:00'),(337,'15031097','insertar','ced_per=8019652;fch_ina=2011-03-09;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:14:00'),(338,'15031097','insertar','ced_per=8019652;fch_ina=2011-03-10;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:14:00'),(339,'15031097','insertar','ced_per=8019652;fch_ina=2011-03-11;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:14:00'),(340,'15031097','eliminar','ced_per=8019652;fch_ina=2011-03-07;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:17:00'),(341,'15031097','eliminar','ced_per=8019652;fch_ina=2011-03-08;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:17:00'),(342,'15031097','insertar','ced_per=10715628;fch_ina=2011-03-01;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:25:00'),(343,'15031097','insertar','ced_per=10715628;fch_ina=2011-03-02;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:25:00'),(344,'15031097','insertar','ced_per=10715628;fch_ina=2011-03-03;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:25:00'),(345,'15031097','insertar','ced_per=10715628;fch_ina=2011-03-04;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:25:00'),(346,'15031097','insertar','ced_per=10715628;fch_ina=2011-03-09;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:26:00'),(347,'15031097','insertar','ced_per=8029735;fch_ina=2011-03-17;tip_ina=Remunerada;des_ina=Permiso','inasistencias','2011-08-01','12:33:00'),(348,'15031097','insertar','ced_per=8029735;fch_ina=2011-03-18;tip_ina=Remunerada;des_ina=Permiso','inasistencias','2011-08-01','12:33:00'),(349,'15031097','insertar','ced_per=8036119;fch_ina=2011-03-21;tip_ina=Remunerada;des_ina=Permiso (Esposo en Clínica)','inasistencias','2011-08-01','12:38:00'),(350,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-01;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:42:00'),(351,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-02;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:42:00'),(352,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-03;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:42:00'),(353,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-04;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:43:00'),(354,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-09;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:43:00'),(355,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-10;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:43:00'),(356,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-11;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:43:00'),(357,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-14;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:44:00'),(358,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-15;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:44:00'),(359,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-16;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:45:00'),(360,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-17;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:45:00'),(361,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-18;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:45:00'),(362,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-19;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:45:00'),(363,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-21;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:47:00'),(364,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-22;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:47:00'),(365,'15031097','insertar','ced_per=4264264;fch_ina=2011-03-23;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:47:00'),(366,'15031097','eliminar','ced_per=4264264;fch_ina=2011-03-19;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-01','12:48:00'),(367,'15031097','insertar','ced_per=6343267;fch_ina=2011-03-18;tip_ina=Remunerada;des_ina=Permiso (Vacaciones)','inasistencias','2011-08-01','12:58:00'),(368,'15031097','insertar','ced_per=11960597;fch_ina=2011-03-09;tip_ina=Remunerada;des_ina=Permiso','inasistencias','2011-08-01','13:03:00'),(369,'15031097','insertar','ced_per=16657924;fch_ina=2011-02-03;tip_ina=Remunerada;des_ina=Estudios','inasistencias','2011-08-02','08:12:00'),(370,'15031097','insertar','ced_per=8019652;fch_ina=2011-02-28;tip_ina=Remunerada;des_ina=Vacaciones','inasistencias','2011-08-02','08:27:00'),(371,'15031097','insertar','ced_per=10109577;fch_ina=2011-02-04;tip_ina=Remunerada;des_ina=No Firmó','inasistencias','2011-08-02','08:32:00'),(372,'15031097','insertar','ced_per=11960597;fch_ina=2011-02-07;tip_ina=No Remunerada;des_ina=No asistió','inasistencias','2011-08-02','08:36:00'),(373,'15031097','insertar','ced_per=11960597;fch_ina=2011-02-24;tip_ina=No Remunerada;des_ina=No asistió','inasistencias','2011-08-02','08:36:00'),(374,'15031097','eliminar','fch_frd=2011-07-04;des_frd=Decreto Presidencial como júbilo','feriados','2011-08-02','09:46:00'),(375,'15031097','eliminar','fch_frd=2011-07-05;des_frd=Día de la Independencia Venezolana','feriados','2011-08-02','09:46:00'),(376,'15031097','insertar','fch_frd=2011-07-05;des_frd=Día de la Independencia de Venezuela','feriados','2011-08-02','09:47:00'),(377,'15031097','insertar','mon_sue=1407.47;des_sue=Auxiliar Administrativo','sueldos','2011-08-02','13:01:00'),(378,'15031097','insertar','num_car=5;nom_car=Auxiliar Administrativo;cod_sue=17;est_car=;cod_tcar=3;cod_dep=UAD','cargos','2011-08-02','13:02:00'),(379,'15031097','insertar','ced_per=17130852;nac_per=V;nom_per=Irene;ape_per=Guillén;sex_per=F;fnac_per=2011-05-04;lnac_per=;cor_per=;pro_per=;dir_per=;tel_per=;cel_per=;fch_reg=2011-08-02;lph_des=;spf_des=;sso_des=;cah_des=;sfu_des=;hog_asg=;hij_asg=;ant_asg=;pro_asg=;hje_asg=;jer_asg=;otr_asg=','personal','2011-08-02','13:03:00'),(380,'15031097','insertar','ced_per=17130852;fch_ina=2011-07-01;tip_ina=No Remunerada;des_ina=No había Ingresado','inasistencias','2011-08-02','13:05:00'),(381,'15031097','insertar','ced_per=17130852;fch_ina=2011-07-04;tip_ina=No Remunerada;des_ina=No había Ingresado','inasistencias','2011-08-02','13:05:00'),(382,'15031097','insertar','ced_per=17130852;fch_ina=2011-07-05;tip_ina=No Remunerada;des_ina=No había Ingresado','inasistencias','2011-08-02','13:06:00'),(383,'15031097','insertar','ced_per=17130852;fch_ina=2011-07-06;tip_ina=No Remunerada;des_ina=No había Ingresado','inasistencias','2011-08-02','13:06:00'),(384,'15031097','insertar','ced_per=17130852;fch_ina=2011-07-07;tip_ina=No Remunerada;des_ina=No había Ingresado','inasistencias','2011-08-02','13:06:00'),(385,'15031097','insertar','ced_per=17130852;fch_ina=2011-07-08;tip_ina=No Remunerada;des_ina=No había Ingresado','inasistencias','2011-08-02','13:06:00'),(386,'15031097','insertar','ced_per=17130852;fch_ina=2011-07-11;tip_ina=No Remunerada;des_ina=No había Ingresado','inasistencias','2011-08-02','13:06:00'),(387,'15031097','eliminar','ced_per=17130852;fch_ina=2011-07-05;tip_ina=No Remunerada;des_ina=No había Ingresado','inasistencias','2011-08-02','13:07:00'),(388,'15031097','modificar','ced_per=15621190; nac_per=V; nom_per=Yohana; ape_per=Peréz de Valero; sex_per=F; fnac_per=1981-09-06; lnac_per=Merida edo Merida; cor_per=yoha19perez@hotmail.com; pro_per=lic Contaduria Publica; dir_per=Sector bella vista calle lara casa Nº54 ejido; tel_per=02742210478; cel_per=04163791718; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-04-25; ','personal','2011-08-05','21:58:00'),(389,'15031097','modificar','ced_per=10109577; nac_per=V; nom_per=María Isabel; ape_per=Rivas de Osuna; sex_per=F; fnac_per=1967-11-05; lnac_per=Merida; cor_per=u_auditoriaint@hotmail.com; pro_per=lic contaduria  publica; dir_per=urb zumba calle 4 A N°556; tel_per=2212896; cel_per=04141413561; lph_des=A; spf_des=A; sso_des=A; cah_des=A; sfu_des=A; hog_asg=A; hij_asg=A; ant_asg=A; pro_asg=A; hje_asg=; jer_asg=; otr_asg=; fch_reg=2011-05-03; ','personal','2011-08-05','22:30:00'),(390,'15031097','eliminar','nom_con=CAPREAMCE;des_con=Caja de Ahorro ','concesiones','2011-08-07','11:48:00'),(391,'15031097','insertar','nom_con=Bono de Productividad;des_con=Bono que se cancela de acuerdo con la evaluación de desempeño','concesiones','2011-08-07','11:54:00'),(392,'15031097','insertar','con_cnp=Bono de Productividad con puntaje de 90 pts.;ncuo_cnp=1;per_cnp=;mcuo_cnp=450;por_cnp=;bpor_cnp=;cod_con=1;ced_per=15031097;cod_car=17;fch_cnp=2011-08-05;ncp_cnp=;des_cnp=','asignaciones','2011-08-07','11:57:00'),(393,'15031097','insertar','nom_con=Devolución;des_con=Devolución por cobro indebido','concesiones','2011-08-07','12:30:00'),(394,'15031097','insertar','con_cnp=Devolución de cuota de CAPREAMCE;ncuo_cnp=1;per_cnp=;mcuo_cnp=150;por_cnp=;bpor_cnp=;cod_con=2;ced_per=15031097;cod_car=17;fch_cnp=2011-07-01;ncp_cnp=;des_cnp=','asignaciones','2011-08-07','12:31:00'),(395,'15031097','insertar','nom_des=Monto Pendiente;des_des=Monto Pendiente por descontar','descuentos','2011-08-07','22:14:00'),(396,'15031097','insertar','con_dsp=Pendiente de mes anterior por día no laborado;ncuo_dsp=1;per_dsp=;mcuo_dsp=200;por_dsp=;bpor_dsp=;cod_des=2;ced_per=6343267;cod_car=3;fch_dsp=2011-05-02;ncp_dsp=','deducciones','2011-08-07','22:15:00'),(397,'12345678','modificar','cod_val=1; des_val=BV; val_val=40.000; con_val=Días de Bono Vacacional para personal Contratado; ','valores','2011-08-07','23:53:00'),(398,'12345678','modificar','cod_val=11; des_val=PBV; val_val=0.280; con_val=; cod_val=3; des_val=UT; val_val=76.000; con_val=; ','valores','2011-08-07','23:55:00'),(399,'12345678','modificar','cod_val=4; des_val=SSO; val_val=0.040; con_val=; ','valores','2011-08-07','23:56:00'),(400,'12345678','modificar','cod_val=10; des_val=SMMSSO; val_val=5.000; con_val=; ','valores','2011-08-07','23:57:00'),(401,'12345678','modificar','cod_val=12; des_val=APSSO; val_val=0.090; con_val=; ','valores','2011-08-07','23:57:00'),(402,'12345678','modificar','cod_val=6; des_val=SPF; val_val=0.005; con_val=; ','valores','2011-08-07','23:58:00'),(403,'12345678','modificar','cod_val=13; des_val=APSPF; val_val=0.020; con_val=; ','valores','2011-08-07','23:58:00'),(404,'12345678','modificar','cod_val=13; des_val=APSPF; val_val=0.020; con_val=Porcentaje de Aporte de Pérdida Involuntaria de Empleo (PIE); ','valores','2011-08-07','23:59:00'),(405,'12345678','modificar','cod_val=5; des_val=LPH; val_val=0.010; con_val=; ','valores','2011-08-08','00:00:00'),(406,'12345678','modificar','cod_val=22; des_val=APLPH; val_val=0.100; con_val=; ','valores','2011-08-08','00:00:00'),(407,'12345678','modificar','cod_val=37; des_val=APLPH; val_val=0.020; con_val=; ','valores','2011-08-08','00:01:00'),(408,'12345678','eliminar','cod_val=37;des_val=APLPH;val_val=0.020;con_val=Porcentaje de Aporte Fondo Ahorro Obligatorio para Vivienda','valores','2011-08-08','00:04:00'),(409,'12345678','modificar','cod_val=5; des_val=LPH; val_val=0.010; con_val=Porcentaje de Retención Fondo Ahorro Obligatorio para Vivienda; cod_val=8; des_val=SFU; val_val=40.000; con_val=; ','valores','2011-08-08','00:06:00'),(410,'12345678','modificar','cod_val=7; des_val=CAH; val_val=0.080; con_val=; ','valores','2011-08-08','00:07:00'),(411,'12345678','modificar','cod_val=14; des_val=APCAH; val_val=0.080; con_val=; ','valores','2011-08-08','00:08:00'),(412,'12345678','eliminar','cod_val=11;des_val=PBV;val_val=0.280;con_val=','valores','2011-08-08','00:09:00'),(413,'12345678','modificar','cod_val=15; des_val=PRMHOG; val_val=2.500; con_val=; ','valores','2011-08-08','00:10:00'),(414,'12345678','modificar','cod_val=16; des_val=PRMHIJ; val_val=0.500; con_val=; ','valores','2011-08-08','00:10:00'),(415,'12345678','modificar','cod_val=15; des_val=PRMHOG; val_val=2.500; con_val=Cantidad de Unidades Tributarias Prima Hogar; ','valores','2011-08-08','00:11:00'),(416,'12345678','modificar','cod_val=17; des_val=PRMANT1; val_val=1.000; con_val=; ','valores','2011-08-08','00:12:00'),(417,'12345678','modificar','cod_val=18; des_val=PRMPRO1; val_val=1.500; con_val=; ','valores','2011-08-08','00:13:00'),(418,'12345678','modificar','cod_val=19; des_val=PRMJER; val_val=300.000; con_val=; ','valores','2011-08-08','00:13:00'),(419,'12345678','modificar','cod_val=21; des_val=PRMOTR1; val_val=2.500; con_val=; ','valores','2011-08-08','00:14:00'),(420,'12345678','modificar','cod_val=20; des_val=PRMHJE; val_val=0.000; con_val=; ','valores','2011-08-08','00:15:00'),(421,'12345678','modificar','cod_val=20; des_val=PRMHJE; val_val=0.000; con_val=Cantidad de Unidades Tributarias Prima por Hijos Excepcionales; ','valores','2011-08-08','00:15:00'),(422,'12345678','modificar','cod_val=23; des_val=PRMANT2; val_val=1.500; con_val=; ','valores','2011-08-08','00:16:00'),(423,'12345678','modificar','cod_val=24; des_val=PRMANT3; val_val=2.000; con_val=; ','valores','2011-08-08','00:16:00'),(424,'12345678','modificar','cod_val=25; des_val=PRMANT4; val_val=2.500; con_val=; ','valores','2011-08-08','00:17:00'),(425,'12345678','modificar','cod_val=26; des_val=PRMANT5; val_val=3.000; con_val=; ','valores','2011-08-08','00:17:00'),(426,'12345678','modificar','cod_val=27; des_val=VACPAG1; val_val=45.000; con_val=; ','valores','2011-08-08','00:18:00'),(427,'12345678','modificar','cod_val=28; des_val=VACPAG2; val_val=50.000; con_val=; ','valores','2011-08-08','00:18:00'),(428,'12345678','modificar','cod_val=29; des_val=VACPAG3; val_val=55.000; con_val=; ','valores','2011-08-08','00:19:00'),(429,'12345678','modificar','cod_val=30; des_val=VACPAG4; val_val=60.000; con_val=; ','valores','2011-08-08','00:19:00'),(430,'12345678','modificar','cod_val=31; des_val=VACPAG5; val_val=65.000; con_val=; ','valores','2011-08-08','00:19:00'),(431,'12345678','modificar','cod_val=38; des_val=DESPIO; val_val=0.001; con_val=; ','valores','2011-08-08','00:20:00'),(432,'12345678','modificar','cod_val=39; des_val=PRMPRO2; val_val=2.000; con_val=; ','valores','2011-08-08','00:21:00'),(433,'12345678','modificar','cod_val=40; des_val=PRMOTR2; val_val=3.000; con_val=; ','valores','2011-08-08','00:22:00'),(434,'12345678','modificar','cod_val=9; des_val=SM; val_val=1223.890; con_val=; ','valores','2011-08-08','00:23:00'),(435,'12345678','modificar','cod_val=32; des_val=VACDIF1; val_val=20.000; con_val=; ','valores','2011-08-08','00:24:00'),(436,'12345678','modificar','cod_val=34; des_val=VACDIF3; val_val=25.000; con_val=; ','valores','2011-08-08','00:25:00'),(437,'12345678','modificar','cod_val=33; des_val=VACDIF2; val_val=23.000; con_val=; ','valores','2011-08-08','00:25:00'),(438,'12345678','modificar','cod_val=35; des_val=VACDIF4; val_val=28.000; con_val=; ','valores','2011-08-08','00:25:00'),(439,'12345678','modificar','cod_val=36; des_val=VACDIF5; val_val=30.000; con_val=; ','valores','2011-08-08','00:25:00'),(440,'12345678','modificar','cod_val=38; des_val=DESPIO; val_val=0.001; con_val=Porcentaje de Monte Pio; ','valores','2011-08-08','00:26:00'),(441,'15031097','modificar','cod_fam=21; ced_per=6343267; ced_fam=2286846; nom_fam=Ana ; ape_fam=Pernia; sex_fam=F; fnac_fam=1939-07-26; par_fam=C; est_fam=; obs_fam=; ','familiares','2011-08-09','12:29:00'),(442,'12345678','modificar','cod_val=38; des_val=DESPIO; val_val=0.000; con_val=Porcentaje de Monte Pio; ','valores','2011-08-09','12:33:00'),(443,'15031097','modificar','cod_val=38; des_val=DESPIO; val_val=0.001; con_val=Porcentaje de Monte Pio; ','valores','2011-08-09','12:35:00'),(444,'15031097','insertar','nom_des=Alilente;des_des=Descuento de Alitente','descuentos','2011-08-09','12:37:00'),(445,'15031097','insertar','ced_per=11960597;fch_ina=2011-08-09;tip_ina=No Remunerada;des_ina=Cita en Tribunal','inasistencias','2011-08-09','12:44:00'),(446,'15031097','insertar','con_dsp=Compra de lentes correctivos;ncuo_dsp=1;per_dsp=;mcuo_dsp=100;por_dsp=;bpor_dsp=;cod_des=3;ced_per=15031097;cod_car=17;fch_dsp=2011-08-09;ncp_dsp=','deducciones','2011-08-09','12:46:00'),(447,'6343267','modificar','cod_val=38; des_val=DESPIO; val_val=0.010; con_val=Porcentaje de Monte Pio; ','valores','2011-08-09','13:42:00'),(448,'6343267','insertar','cod_pro=21;fch_pro=2011-01-03;rif_pro=G200000031;nom_pro=Tesoro Nacional;dir_pro=Caracas;act_pro=Gubernamental;cap_pro=;cas_pro=;tel_pro=02120000000;fax_pro=;per_pro=J;ofi_reg_pro=0;num_reg_pro=;tom_reg_pro=;fol_reg_pro=;fch_reg_pro=2011-01-03;num_mod_pro=;tom_mod_pro=;fol_mod_pro=;fch_mod_pro=;pat_pro=;ivss_pro=;ince_pro=;nom_rep_pro=Tesoro Nacional;ape_rep_pro=Tesoro Nacional;ced_rep_pro=00000000;dir_rep_pro=;tel_rep_pro=;car_rep_pro=Tesoro Nacional','proveedores','2011-08-09','14:03:00'),(449,'6343267','insertar','cod_egr=;fch_egr=2011-08-04;nor_egr=184;rif_pro=G200000031;aut_egr=;cod_ban=2;chq_egr=00238303;fac_egr=3845678;tip_egr=Servicio;ref_egr=S/N;con_egr=Cancelación de retenciones de IVA correspondientes a la segunda quincena del mes de Julio;mon_egr=276.02;mon_iva_egr=0;ret_iva_egr=0;ret_isrl_egr=0;npr_egr=;ela_egr=T.S.U. LUIS MARQUEZ;rev_egr=LCDA LUZ SALCEDO;apr_egr=ABOG. JOSE A. BRICEÑO;cont_egr=LCDO. JOSE G. UZCATEGUI;obs_egr=','egresos','2011-08-09','14:06:00'),(450,'6343267','insertar','cod_com=;fch_com=2011-08-09;nor_com=76;sol_com=S/N;tip_com=Servicio;for_com=Contado;adl_com=0;fre_com= ;rif_pro=J311801596;npr_com=;iva_com=12;ela_com=LCDA. LUZ MARINA SALCEDO;rev_com=ABOG. JOSÉ ANDRÉS BRICEÑO VALERO;obs_com=','compras','2011-08-09','14:12:00'),(451,'6343267','modificar','cod_pro=0017; fch_pro=2011-01-01; rif_pro=J311801596; nom_pro=Condomineo  Centro Comercial Centenario ; dir_pro=Av Centenario. Centro Comercial Centenrio Nivel PR Oficina Administrador; act_pro=Condomineo; cap_pro=1.00; cas_pro=1.00; tel_pro=02744158866; fax_pro=; per_pro=J; ofi_reg_pro=S/N; num_reg_pro=6; tom_reg_pro=0; fol_reg_pro=0; fch_reg_pro=2011-01-01; num_mod_pro=6; tom_mod_pro=S/N; fol_mod_pro=0007; fch_mod_pro=2011-01-01; pat_pro=0; ivss_pro=0; ince_pro=0; nom_rep_pro=condomineo; ape_rep_pro=centenario; ced_rep_pro=31180159; dir_rep_pro=Av centenario . centro comercial ; tel_rep_pro=02744158866; car_rep_pro=administrador; ','proveedores','2011-08-09','14:13:00'),(452,'15031097','modificar','cod_com=2; fch_com=2011-08-09; nor_com=0076; sol_com=S/N; tip_com=Servicio; for_com=Contado; adl_com=0.00; fre_com= ; rif_pro=J311801596; npr_com=0; iva_com=12; ela_com=LCDA. LUZ MARINA SALCEDO; rev_com=ABOG. JOSÉ ANDRÉS BRICEÑO VALERO; obs_com=; ','compras','2011-08-09','14:16:00'),(453,'15031097','modificar','cod_pro_com=1; cod_com=2; cnt_pro_com=1; uni_pro_com=1; con_pro_com=Sirvase prestar el servicio de Condominio del local Nº 54 correspondiente al mes de Julio; pun_pro_com=430.74; exc_pro_com=SI; ','productos_compras','2011-08-09','14:16:00'),(454,'15031097','modificar','cod_pro_com=1; cod_com=2; cnt_pro_com=1; uni_pro_com=1; con_pro_com=Sirvase prestar el servicio de Condominio del local Nº 54 correspondiente al mes de Julio; pun_pro_com=430.74; exc_pro_com=; ','productos_compras','2011-08-09','14:17:00'),(455,'6343267','insertar','cod_egr=;fch_egr=2011-08-09;nor_egr=185;rif_pro=J311801596;aut_egr=;cod_ban=2;chq_egr=07385083;fac_egr=009970-0;tip_egr=Servicio;ref_egr=08110076;con_egr=Cancelación de Servicio de Condominio del local Nº 54. Correspondiente al mes de Julio;mon_egr=430.74;mon_iva_egr=0;ret_iva_egr=0;ret_isrl_egr=0;npr_egr=;ela_egr=T.S.U. LUIS MARQUEZ;rev_egr=LCDA LUZ SALCEDO;apr_egr=ABOG. JOSE A. BRICEÑO;cont_egr=LCDO. JOSE G. UZCATEGUI;obs_egr=','egresos','2011-08-09','14:30:00'),(456,'6343267','modificar','cod_egr=3; fch_egr=2011-08-09; nor_egr=0185; rif_pro=J311801596; aut_egr=; cod_ban=2; chq_egr=07385083; fac_egr=009970-0; tip_egr=Servicio; ref_egr=08110076; con_egr=Cancelación de Servicio de Condominio del local Nº 54. Correspondiente al mes de Julio; mon_egr=430.74; mon_iva_egr=0.00; ret_iva_egr=0; ret_isrl_egr=0; npr_egr=0; ela_egr=T.S.U. LUIS MARQUEZ; rev_egr=LCDA LUZ SALCEDO; apr_egr=ABOG. JOSE A. BRICEÑO; cont_egr=LCDO. JOSE G. UZCATEGUI; obs_egr=; ','egresos','2011-08-09','14:31:00'),(457,'15031097','modificar','cod_pro_egr=1; cod_egr=3; cod_par=54; isrl_pro_egr=S; mon_pro_egr=430.74; ','productos_egresos','2011-08-09','14:34:00'),(458,'6343267','modificar','cod_egr=3; fch_egr=2011-08-09; nor_egr=0185; rif_pro=J311801596; aut_egr=; cod_ban=2; chq_egr=07385083; fac_egr=009970-0; tip_egr=Servicio; ref_egr=08110076; con_egr=Cancelación de Servicio de Condominio del local Nº 54. Correspondiente al mes de Julio; mon_egr=430.74; mon_iva_egr=0.00; ret_iva_egr=0; ret_isrl_egr=2; npr_egr=0; ela_egr=T.S.U. LUIS MARQUEZ; rev_egr=LCDA LUZ SALCEDO; apr_egr=ABOG. JOSE A. BRICEÑO; cont_egr=LCDO. JOSE G. UZCATEGUI; obs_egr=; ','egresos','2011-08-09','14:34:00'),(459,'15031097','insertar','con_cnp=dsfasdf;ncuo_cnp=1;per_cnp=;mcuo_cnp=1;por_cnp=;bpor_cnp=;cod_con=1;ced_per=15031097;cod_car=17;fch_cnp=2011-08-09;ncp_cnp=;des_cnp=','asignaciones','2011-08-09','21:37:00'),(460,'15031097','eliminar','con_cnp=dsfasdf;ncuo_cnp=1;per_cnp=;mcuo_cnp=1.00;por_cnp=0;bpor_cnp=;cod_con=1;ced_per=15031097;cod_car=17;fch_cnp=2011-08-09;ncp_cnp=0;des_cnp=','asignaciones','2011-08-09','21:37:00'),(461,'15031097','eliminar','ced_per=15031097;num_cue=01050092310092239498;tip_cue=2;ban_cue=Mercantil;fcam_cue=2011-08-09','cuentas','2011-08-09','21:41:00'),(462,'15031097','eliminar','ced_per=15031097;num_cue=01050092310092239498;tip_cue=2;ban_cue=Mercantil;fcam_cue=2011-08-09','cuentas','2011-08-09','21:41:00'),(463,'15031097','eliminar','ced_per=15031097;num_cue=01050092310092239498;tip_cue=2;ban_cue=Mercantil;fcam_cue=2011-08-09','cuentas','2011-08-09','21:45:00'),(464,'15031097','eliminar','ced_per=15031097;num_cue=01050092310092239498;tip_cue=2;ban_cue=Mercantil;fcam_cue=2011-08-09','cuentas','2011-08-09','21:52:00'),(465,'15031097','eliminar','ced_per=15031097;num_cue=01050092310092239498;tip_cue=2;ban_cue=Mercantil;fcam_cue=2011-08-09','cuentas','2011-08-09','21:54:00'),(466,'15031097','eliminar','ced_per=15031097;num_cue=01050092310092239498;tip_cue=2;ban_cue=Mercantil;fcam_cue=2011-08-09','cuentas','2011-08-09','21:57:00'),(467,'15031097','eliminar','ced_per=15031097;num_cue=01050092310092239498;tip_cue=2;ban_cue=Mercantil;fcam_cue=2011-08-09','cuentas','2011-08-09','21:59:00'),(468,'15031097','eliminar','ced_per=15031097;num_cue=01050092310092239498;tip_cue=2;ban_cue=Mercantil;fcam_cue=2011-08-09','cuentas','2011-08-09','22:00:00'),(469,'15031097','eliminar','ced_per=15031097;num_cue=01050092310092239498;tip_cue=2;ban_cue=Mercantil;fcam_cue=2011-08-09','cuentas','2011-08-09','22:01:00'),(470,'15031097','modificar','cod_dsp=7; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=48; per_dsp=; mcuo_dsp=316.57; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=10715628; cod_car=7; fch_dsp=2011-05-30; ncp_dsp=0; ','deducciones','2011-08-10','15:14:00'),(471,'15031097','modificar','cod_dsp=8; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=36; per_dsp=; mcuo_dsp=168.79; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=15621190; cod_car=12; fch_dsp=2011-05-30; ncp_dsp=0; ','deducciones','2011-08-10','15:16:00'),(472,'15031097','modificar','cod_dsp=1; con_dsp=Préstamo Caja de Ahorros; ncuo_dsp=24; per_dsp=; mcuo_dsp=243.90; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=4264264; cod_car=6; fch_dsp=2010-05-01; ncp_dsp=16; ','deducciones','2011-08-10','15:20:00'),(473,'15031097','modificar','cod_dsp=6; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=36; per_dsp=; mcuo_dsp=132.10; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=11960597; cod_car=9; fch_dsp=2011-05-17; ncp_dsp=0; ','deducciones','2011-08-10','15:21:00'),(474,'15031097','modificar','cod_dsp=2; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=48; per_dsp=; mcuo_dsp=132.12; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=6343267; cod_car=3; fch_dsp=2010-09-09; ncp_dsp=0; ','deducciones','2011-08-10','15:30:00'),(475,'15031097','modificar','cod_dsp=4; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=5; per_dsp=; mcuo_dsp=138.27; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=9028505; cod_car=10; fch_dsp=2011-01-01; ncp_dsp=0; ','deducciones','2011-08-10','15:31:00'),(476,'15031097','modificar','cod_dsp=4; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=5; per_dsp=; mcuo_dsp=138.27; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=9028505; cod_car=10; fch_dsp=2010-09-09; ncp_dsp=22; ','deducciones','2011-08-10','15:31:00'),(477,'15031097','modificar','cod_dsp=4; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=48; per_dsp=; mcuo_dsp=138.27; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=9028505; cod_car=10; fch_dsp=2010-09-09; ncp_dsp=22; ','deducciones','2011-08-10','15:32:00'),(478,'15031097','insertar','con_dsp=Préstamo de Caja de Ahorros;ncuo_dsp=24;per_dsp=;mcuo_dsp=66.64;por_dsp=;bpor_dsp=;cod_des=1;ced_per=8019652;cod_car=11;fch_dsp=2011-08-16;ncp_dsp=','deducciones','2011-08-10','15:35:00'),(479,'15031097','modificar','cod_dsp=10; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=24; per_dsp=; mcuo_dsp=66.64; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=8019652; cod_car=11; fch_dsp=2011-08-16; ncp_dsp=0; ','deducciones','2011-08-10','15:37:00'),(480,'15031097','modificar','cod_dsp=10; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=24; per_dsp=; mcuo_dsp=66.64; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=8019652; cod_car=11; fch_dsp=2011-08-16; ncp_dsp=23; ','deducciones','2011-08-10','15:37:00'),(481,'15031097','modificar','cod_dsp=3; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=48; per_dsp=; mcuo_dsp=524.66; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=8023210; cod_car=2; fch_dsp=2010-06-26; ncp_dsp=0; ','deducciones','2011-08-10','15:38:00'),(482,'15031097','modificar','cod_dsp=3; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=48; per_dsp=; mcuo_dsp=524.66; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=8023210; cod_car=2; fch_dsp=2010-07-26; ncp_dsp=0; ','deducciones','2011-08-10','15:40:00'),(483,'15031097','modificar','cod_dsp=5; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=48; per_dsp=; mcuo_dsp=135.38; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=8029735; cod_car=4; fch_dsp=2010-07-12; ncp_dsp=0; cod_dsp=5; con_dsp=Préstamo de Caja de Ahorros; ncuo_dsp=48; per_dsp=; mcuo_dsp=135.38; por_dsp=0; bpor_dsp=; cod_des=1; ced_per=8029735; cod_car=4; fch_dsp=2010-07-12; ncp_dsp=0; ','deducciones','2011-08-10','15:44:00');
/*!40000 ALTER TABLE `auditoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auditoria_backup`
--

DROP TABLE IF EXISTS `auditoria_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditoria_backup` (
  `cod_aud` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C�digo de Auditor�a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C�digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci�n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci�n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu� la acci�n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci�n',
  `hora` time NOT NULL COMMENT 'hora de la acci�n',
  PRIMARY KEY (`cod_aud`),
  KEY `cod_usr` (`cod_usr`),
  CONSTRAINT `auditoria_backup_ibfk_1` FOREIGN KEY (`cod_usr`) REFERENCES `usuarios` (`cod_usr`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditoria_backup`
--

LOCK TABLES `auditoria_backup` WRITE;
/*!40000 ALTER TABLE `auditoria_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `auditoria_backup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banco`
--

DROP TABLE IF EXISTS `banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banco` (
  `cod_ban` int(11) NOT NULL AUTO_INCREMENT,
  `nom_ban` varchar(25) DEFAULT NULL,
  `cue_ban` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`cod_ban`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banco`
--

LOCK TABLES `banco` WRITE;
/*!40000 ALTER TABLE `banco` DISABLE KEYS */;
INSERT INTO `banco` VALUES (1,'Mercantil','01050747271747019259'),(2,'Sofitasa','01370032020000015661'),(3,'Venezuela','01020441170000104281');
/*!40000 ALTER TABLE `banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargos`
--

DROP TABLE IF EXISTS `cargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargos` (
  `cod_car` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo del cargo',
  `num_car` int(3) unsigned zerofill NOT NULL COMMENT 'numero del cargo',
  `nom_car` varchar(100) NOT NULL COMMENT 'nombre del cargo',
  `cod_sue` int(2) NOT NULL COMMENT 'codigo del sueldo',
  `est_car` varchar(1) NOT NULL COMMENT 'estado del cargo',
  `cod_tcar` int(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `ced_per` varchar(12) NOT NULL,
  `fch_asg` date NOT NULL COMMENT 'Fecha de asignacion  del cargo',
  `des_car` varchar(1) NOT NULL COMMENT 'Se efectuan los descuentos de ley por este Cargo',
  `fch_vac` date NOT NULL,
  PRIMARY KEY (`cod_car`),
  UNIQUE KEY `num_car` (`num_car`,`cod_dep`),
  KEY `cod_sue` (`cod_sue`),
  KEY `cod_dep` (`cod_dep`),
  CONSTRAINT `cargos_ibfk_15` FOREIGN KEY (`cod_sue`) REFERENCES `sueldos` (`cod_sue`) ON UPDATE CASCADE,
  CONSTRAINT `cargos_ibfk_16` FOREIGN KEY (`cod_dep`) REFERENCES `dependencias` (`cod_dep`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargos`
--

LOCK TABLES `cargos` WRITE;
/*!40000 ALTER TABLE `cargos` DISABLE KEYS */;
INSERT INTO `cargos` VALUES (2,001,'Contralor Municipal',1,'A',1,'DSC','8023210','2006-12-22','X','1993-09-01'),(3,001,'Administrador(a)',6,'A',1,'ADM','6343267','2008-04-22','X','1996-05-06'),(4,001,'Secretaria Ejecutiva',5,'A',2,'SCR','8029735','1991-07-19','X','1991-07-19'),(5,001,'Jefe de Servicios Jurídicos',9,'A',1,'SRJ','8036119','2007-03-01','X','2007-03-01'),(6,001,'Jefe de Sala Técnica',7,'A',1,'SLT','4264264','2007-03-01','X','2004-07-12'),(7,001,'Auditor II',2,'A',2,'UAD','10715628','2001-11-12','X','1999-11-15'),(8,002,'Auditor II ',3,'A',2,'UAD','10109577','1996-07-01','X','1996-07-01'),(9,002,'Inspector de Obras II',4,'A',2,'SLT','11960597','1999-11-02','X','1999-11-02'),(10,001,'Registrador de Bienes',8,'A',2,'CBN','9028505','2006-01-09','X','2006-01-09'),(11,002,'Asistente de Control y Fiscalización',10,'A',2,'CBN','8019652','2007-02-15','X','1995-02-08'),(12,003,'Auditor I',11,'A',2,'UAD','15621190','2008-08-18','X','2008-08-18'),(13,004,'Auxiliar de Auditoría',12,'A',3,'UAD','14304657','2010-04-22','X','2010-04-22'),(14,003,'Técnico Inspector de Obras Civiles',13,'A',3,'SLT','12346028','2010-04-21','X','2010-04-21'),(15,002,'Abogado',16,'A',3,'SRJ','16933715','2011-01-01','X','2011-01-01'),(16,002,'Aseadora',14,'A',3,'ADM','16657924','2010-02-01','X','2010-02-01'),(17,003,'Operador de Equipos de Computación',15,'A',3,'ADM','15031097','2011-01-01','','2011-01-01'),(18,005,'Auxiliar Administrativo',17,'',3,'UAD','','0000-00-00','','0000-00-00');
/*!40000 ALTER TABLE `cargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compras`
--

DROP TABLE IF EXISTS `compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compras` (
  `cod_com` int(11) NOT NULL AUTO_INCREMENT,
  `fch_com` date NOT NULL,
  `nor_com` int(4) unsigned zerofill NOT NULL,
  `sol_com` varchar(20) DEFAULT NULL,
  `tip_com` varchar(15) NOT NULL,
  `for_com` varchar(15) DEFAULT NULL,
  `adl_com` double(9,2) DEFAULT NULL,
  `fre_com` varchar(50) DEFAULT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `npr_com` int(2) NOT NULL,
  `iva_com` int(2) NOT NULL,
  `ela_com` varchar(50) NOT NULL,
  `rev_com` varchar(50) NOT NULL,
  `obs_com` varchar(255) DEFAULT NULL,
  KEY `id` (`cod_com`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compras`
--

LOCK TABLES `compras` WRITE;
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
INSERT INTO `compras` VALUES (1,'2011-01-01',0075,NULL,'',NULL,NULL,NULL,'',0,0,'','',NULL),(2,'2011-08-09',0076,'S/N','Servicio','Contado',0.00,' ','J311801596',0,12,'LCDA. LUZ MARINA SALCEDO','ABOG. JOSÉ ANDRÉS BRICEÑO VALERO','');
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `concesiones`
--

DROP TABLE IF EXISTS `concesiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `concesiones` (
  `cod_con` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de concesion',
  `nom_con` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'Nombre del tipo de concesion',
  `des_con` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de concesion',
  PRIMARY KEY (`cod_con`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `concesiones`
--

LOCK TABLES `concesiones` WRITE;
/*!40000 ALTER TABLE `concesiones` DISABLE KEYS */;
INSERT INTO `concesiones` VALUES (1,'Bono de Productividad','Bono que se cancela de acuerdo con la evaluación de desempeño'),(2,'Devolución','Devolución por cobro indebido');
/*!40000 ALTER TABLE `concesiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuentas`
--

DROP TABLE IF EXISTS `cuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentas` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `num_cue` varchar(20) NOT NULL COMMENT 'numero de cuenta de la persona',
  `tip_cue` varchar(1) NOT NULL COMMENT 'tipo de la cuenta de la persona',
  `fcam_cue` date NOT NULL COMMENT 'fecha de cambio del numero de cuenta',
  `ban_cue` varchar(50) NOT NULL,
  PRIMARY KEY (`ced_per`),
  UNIQUE KEY `num_cue` (`num_cue`),
  CONSTRAINT `cuentas_ibfk_1` FOREIGN KEY (`ced_per`) REFERENCES `personal` (`ced_per`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuentas`
--

LOCK TABLES `cuentas` WRITE;
/*!40000 ALTER TABLE `cuentas` DISABLE KEYS */;
INSERT INTO `cuentas` VALUES ('10109577','01050747220747049661','2','2011-05-03','Mercantil'),('10715628','01050747210747049807','2','2011-04-25','Mercantil'),('11960597','01050184940184025907','2','2011-04-25','Mercantil'),('12346028','01050672717672028529','2','2011-04-25','Mercantil'),('14304657','01050672757672055038','2','2011-07-25','Mercantil'),('15031097','01050092310092239498','2','2011-08-09','Mercantil'),('15621190','01050747290747049548','2','2011-04-25','Mercantil'),('16657924','01050747270747054010','2','2011-05-03','Mercantil'),('16933715','01050747210747059888','2','2011-04-25','Mercantil'),('4264264','01020681210000033640','1','2011-06-28','Venezuela'),('6343267','01020441140100028505','2','2011-07-25','Venezuela'),('8019652','01050747210747049483','2','2011-05-03','Mercantil'),('8023210','01020441100100028502','2','2011-07-25','Venezuela'),('8029735','01050747290747054037','2','2011-05-03','Mercantil'),('8036119','01020681240000032968','2','2011-07-25','Venezuela'),('9028505','01050747220747049076','2','2011-04-25','Mercantil');
/*!40000 ALTER TABLE `cuentas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deducciones`
--

DROP TABLE IF EXISTS `deducciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deducciones` (
  `cod_dsp` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la Deduccion',
  `con_dsp` varchar(100) NOT NULL COMMENT 'Concepto de la Deduccion',
  `ncuo_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas de la deduccion',
  `per_dsp` varchar(1) NOT NULL COMMENT 'Permanencia de la Deduccion',
  `mcuo_dsp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la deduccion',
  `por_dsp` int(3) NOT NULL COMMENT 'Porcentaje de deduccion',
  `bpor_dsp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_des` int(11) NOT NULL COMMENT 'Codigo del Descuento',
  `ced_per` varchar(12) NOT NULL COMMENT 'Cedula del Personal',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_dsp` date NOT NULL COMMENT 'Fecha de Registro de la Deduccion',
  `ncp_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas pagadas',
  PRIMARY KEY (`cod_dsp`),
  KEY `cod_des` (`cod_des`),
  KEY `cod_per` (`ced_per`),
  CONSTRAINT `deducciones_ibfk_1` FOREIGN KEY (`cod_des`) REFERENCES `descuentos` (`cod_des`) ON UPDATE CASCADE,
  CONSTRAINT `deducciones_ibfk_2` FOREIGN KEY (`ced_per`) REFERENCES `personal` (`ced_per`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deducciones`
--

LOCK TABLES `deducciones` WRITE;
/*!40000 ALTER TABLE `deducciones` DISABLE KEYS */;
INSERT INTO `deducciones` VALUES (1,'Préstamo Caja de Ahorros',36,'',243.90,0,'',1,'4264264',6,'2010-05-17',5),(2,'Préstamo de Caja de Ahorros',48,'',132.12,0,'',1,'6343267',3,'2010-09-09',22),(3,'Préstamo de Caja de Ahorros',48,'',524.66,0,'',1,'8023210',2,'2010-07-26',24),(4,'Préstamo de Caja de Ahorros',36,'',138.27,0,'',1,'9028505',10,'2010-09-09',22),(5,'Préstamo de Caja de Ahorros',48,'',135.38,0,'',1,'8029735',4,'2010-07-12',26),(6,'Préstamo de Caja de Ahorros',36,'',132.10,0,'',1,'11960597',9,'2011-05-17',5),(7,'Préstamo de Caja de Ahorros',48,'',316.57,0,'',1,'10715628',7,'2011-05-30',4),(8,'Préstamo de Caja de Ahorros',36,'',168.79,0,'',1,'15621190',12,'2011-05-30',4),(9,'Compra de lentes correctivos',1,'',100.00,0,'',3,'15031097',17,'2011-08-09',0),(10,'Préstamo de Caja de Ahorros',24,'',66.64,0,'',1,'8019652',11,'2011-08-16',22);
/*!40000 ALTER TABLE `deducciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dependencias`
--

DROP TABLE IF EXISTS `dependencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependencias` (
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `nom_dep` varchar(150) NOT NULL COMMENT 'nombre de la dependencia',
  `cod_act` varchar(5) NOT NULL COMMENT 'actividad de la dependencia',
  `cod_pro` varchar(5) NOT NULL COMMENT 'programa de la dependencia',
  PRIMARY KEY (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependencias`
--

LOCK TABLES `dependencias` WRITE;
/*!40000 ALTER TABLE `dependencias` DISABLE KEYS */;
INSERT INTO `dependencias` VALUES ('ADM','Administración','',''),('CBN','Control de Bienes','',''),('DSC','Despacho del Contralor','',''),('SCR','Secretaría','',''),('SLT','Sala Técnica','',''),('SRJ','Servicio Jurídico','',''),('UAD','Unidad de Auditoría','','');
/*!40000 ALTER TABLE `dependencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `descuentos`
--

DROP TABLE IF EXISTS `descuentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descuentos` (
  `cod_des` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de descuento',
  `nom_des` varchar(50) NOT NULL COMMENT 'Nombre del tipo de descuento',
  `des_des` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de descuento',
  PRIMARY KEY (`cod_des`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `descuentos`
--

LOCK TABLES `descuentos` WRITE;
/*!40000 ALTER TABLE `descuentos` DISABLE KEYS */;
INSERT INTO `descuentos` VALUES (1,'Préstamo CAPREAMCE','Préstamo a Caja de Ahorros'),(2,'Monto Pendiente','Monto Pendiente por descontar'),(3,'Alilente','Descuento de Alitente');
/*!40000 ALTER TABLE `descuentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `educacion`
--

DROP TABLE IF EXISTS `educacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educacion` (
  `cod_edu` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `niv_edu` varchar(50) NOT NULL,
  `ins_edu` varchar(100) NOT NULL,
  `fch_edu` date NOT NULL,
  `reg_edu` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_edu`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COMMENT='Datos de los Estudios Realizados';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `educacion`
--

LOCK TABLES `educacion` WRITE;
/*!40000 ALTER TABLE `educacion` DISABLE KEYS */;
INSERT INTO `educacion` VALUES (1,'15031097','TSU en Informática','TecS','Instituto Universitario Tecnológico de Ejido','2007-12-12','S'),(2,'6343267','Licenciada Contaduría Pública','Univ','Universidad de Los Andes (ULA)','1995-07-21','S'),(3,'6343267','Gerencia Mención Finanzas','Msc','Universidad Bicentenaria de Aragua','2002-12-12','S'),(4,'10109577','Lic. Administración de Empresas','Univ','Universidad de Los Andes (ULA)','1995-01-20','S'),(5,'10109577','Licenciada Contaduría Pública','Univ','Universidad Nacional Abierta (UNA)','2005-01-15','S'),(6,'14304657','TSU en Contaduría Pública','TecS','Instituto Universitario Tecnológico de Ejido','2005-04-22','S'),(7,'8036119','Abogado','Univ','Universidad de Los Andes (ULA)','1990-04-20','S'),(8,'16933715','Abogado','Univ','Universidad de Los Andes (ULA)','2007-12-07','S'),(9,'4264264','Arquitecto','Univ','Universidad de Los Andes (ULA)','1989-07-27','S'),(10,'11960597','TSU en Construcción Civil','TecS','Instituto Universitario Tecnológico de Ejido','1996-06-14','S'),(11,'12346028','TSU en Construcción Civil','TecS','Instituto Universitario Tecnológico de Ejido','1996-06-14','S'),(12,'10715628','Licenciada Contaduría Pública','Univ','Universidad de Los Andes (ULA)','1993-10-22','S'),(13,'10715628','Especialista en Ciencias Contables Mención Tributo','Esp','Universidad de Los Andes (ULA)','2003-12-12','S'),(14,'8023210','Abogado','Univ','Universidad de Los Andes (ULA)','1989-07-21','S'),(15,'8023210','Especialista en Gerencia Municipal','Esp','Universidad Santa María','1998-10-09','S'),(16,'8023210','Especialista en Derecho Administrativo','Esp','Universidad de Los Andes (ULA)','2009-10-23','S'),(17,'15621190','Licenciada Contaduría Pública','Univ','Universidad de Los Andes (ULA)','2008-07-11','S');
/*!40000 ALTER TABLE `educacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `egresos`
--

DROP TABLE IF EXISTS `egresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `egresos` (
  `cod_egr` int(11) NOT NULL AUTO_INCREMENT,
  `fch_egr` date NOT NULL,
  `nor_egr` int(4) unsigned zerofill NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `aut_egr` varchar(25) DEFAULT NULL,
  `cod_ban` int(11) NOT NULL,
  `chq_egr` varchar(25) NOT NULL,
  `fac_egr` varchar(15) NOT NULL,
  `tip_egr` varchar(15) NOT NULL,
  `ref_egr` varchar(15) DEFAULT NULL,
  `con_egr` varchar(255) NOT NULL,
  `mon_egr` double(9,2) NOT NULL,
  `mon_iva_egr` double(9,2) NOT NULL,
  `ret_iva_egr` int(3) NOT NULL,
  `ret_isrl_egr` int(2) NOT NULL,
  `npr_egr` int(2) DEFAULT NULL,
  `ela_egr` varchar(50) NOT NULL,
  `rev_egr` varchar(50) NOT NULL,
  `apr_egr` varchar(50) NOT NULL,
  `cont_egr` varchar(50) NOT NULL,
  `obs_egr` varchar(255) DEFAULT NULL,
  KEY `id` (`cod_egr`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `egresos`
--

LOCK TABLES `egresos` WRITE;
/*!40000 ALTER TABLE `egresos` DISABLE KEYS */;
INSERT INTO `egresos` VALUES (1,'2011-01-01',0183,'',NULL,0,'','','',NULL,'',0.00,0.00,0,0,NULL,'','','','',NULL),(2,'2011-08-04',0184,'G200000031','',2,'00238303','3845678','Servicio','S/N','Cancelación de retenciones de IVA correspondientes a la segunda quincena del mes de Julio',276.02,0.00,0,0,0,'T.S.U. LUIS MARQUEZ','LCDA LUZ SALCEDO','ABOG. JOSE A. BRICEÑO','LCDO. JOSE G. UZCATEGUI',''),(3,'2011-08-09',0185,'J311801596','',2,'07385083','009970-0','Servicio','08110076','Cancelación de Servicio de Condominio del local Nº 54. Correspondiente al mes de Julio',430.74,0.00,0,0,0,'T.S.U. LUIS MARQUEZ','LCDA LUZ SALCEDO','ABOG. JOSE A. BRICEÑO','LCDO. JOSE G. UZCATEGUI','');
/*!40000 ALTER TABLE `egresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entradas`
--

DROP TABLE IF EXISTS `entradas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entradas` (
  `cod_ntr` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C�digo de la Entrada',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C�digo del usuario',
  `fecha_ntr` date NOT NULL COMMENT 'fecha de la entrada',
  `hora_ntr` char(15) NOT NULL COMMENT 'Hora de la Entrada',
  PRIMARY KEY (`cod_ntr`),
  KEY `cod_usr` (`cod_usr`),
  CONSTRAINT `entradas_ibfk_1` FOREIGN KEY (`cod_usr`) REFERENCES `usuarios` (`cod_usr`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=latin1 COMMENT='Entradas de los Usuarios al Sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entradas`
--

LOCK TABLES `entradas` WRITE;
/*!40000 ALTER TABLE `entradas` DISABLE KEYS */;
INSERT INTO `entradas` VALUES (1,'12345678','2011-04-18','02:14:37 pm'),(2,'12345678','2011-04-18','02:18:02 pm'),(3,'15031097','2011-04-18','02:41:19 pm'),(4,'15031097','2011-04-18','03:18:10 pm'),(5,'15031097','2011-04-20','08:51:34 am'),(6,'15031097','2011-04-25','08:19:12 am'),(7,'12345678','2011-04-25','08:19:37 am'),(8,'21184968','2011-04-25','08:22:37 am'),(9,'15031097','2011-04-25','08:23:11 am'),(10,'12345678','2011-04-25','08:23:21 am'),(11,'21184968','2011-04-25','08:24:15 am'),(12,'15031097','2011-04-25','08:27:58 am'),(13,'21184968','2011-04-25','08:29:35 am'),(14,'21184968','2011-04-25','08:30:39 am'),(15,'15031097','2011-04-25','08:43:24 am'),(16,'21184968','2011-04-25','08:48:52 am'),(17,'15031097','2011-04-25','08:58:39 am'),(18,'15031097','2011-04-25','10:11:12 am'),(19,'12345678','2011-04-25','10:17:13 am'),(20,'15031097','2011-04-25','10:21:47 am'),(21,'12345678','2011-04-25','10:23:10 am'),(22,'12345678','2011-04-25','10:29:28 am'),(23,'12345678','2011-04-25','10:30:31 am'),(24,'15031097','2011-04-25','10:37:50 am'),(25,'15031097','2011-04-25','10:51:44 am'),(26,'21184968','2011-04-25','11:39:09 am'),(27,'15031097','2011-04-25','11:59:16 am'),(28,'15031097','2011-04-25','12:52:31 pm'),(29,'15031097','2011-04-25','12:53:57 pm'),(30,'21184968','2011-04-25','01:04:41 pm'),(31,'15031097','2011-04-25','01:32:53 pm'),(32,'15031097','2011-04-25','01:34:04 pm'),(33,'15031097','2011-04-25','02:37:01 pm'),(34,'21184968','2011-04-25','02:38:57 pm'),(35,'15031097','2011-04-25','02:39:41 pm'),(36,'15031097','2011-04-25','03:02:54 pm'),(37,'15031097','2011-04-26','02:57:21 pm'),(38,'12345678','2011-04-29','09:38:14 am'),(39,'12345678','2011-04-29','09:45:11 am'),(40,'15031097','2011-04-29','10:38:49 am'),(41,'12345678','2011-04-29','10:49:01 am'),(42,'8023210','2011-04-29','10:50:09 am'),(43,'8023210','2011-04-29','10:50:52 am'),(44,'12345678','2011-04-29','10:58:56 am'),(45,'8023210','2011-04-29','11:18:24 am'),(46,'15031097','2011-04-29','11:49:04 am'),(47,'8023210','2011-04-29','11:50:19 am'),(48,'15031097','2011-04-29','12:09:18 pm'),(49,'15031097','2011-04-29','12:45:37 pm'),(50,'15031097','2011-04-29','01:13:26 pm'),(51,'15031097','2011-04-29','01:37:33 pm'),(52,'15031097','2011-05-03','08:27:39 am'),(53,'21184968','2011-05-03','08:30:50 am'),(54,'21184968','2011-05-03','08:37:09 am'),(55,'21184968','2011-05-03','08:40:26 am'),(56,'21184968','2011-05-03','10:05:03 am'),(57,'21184968','2011-05-03','11:43:36 am'),(58,'21184968','2011-05-03','12:04:33 pm'),(59,'21184968','2011-05-03','12:44:01 pm'),(60,'21184968','2011-05-03','12:51:49 pm'),(61,'21184968','2011-05-03','01:12:35 pm'),(62,'21184968','2011-05-04','08:47:57 am'),(63,'15031097','2011-05-04','09:01:56 am'),(64,'15031097','2011-05-04','10:01:30 am'),(65,'12345678','2011-05-04','10:18:31 am'),(66,'15031097','2011-05-04','10:43:37 am'),(67,'21184968','2011-05-04','10:48:41 am'),(68,'21184968','2011-05-04','11:16:01 am'),(69,'21184968','2011-05-04','01:00:05 pm'),(70,'15031097','2011-05-04','01:02:55 pm'),(71,'12345678','2011-05-04','01:04:54 pm'),(72,'12345678','2011-05-04','01:06:25 pm'),(73,'4264264','2011-05-04','01:07:40 pm'),(74,'4264264','2011-05-04','01:11:08 pm'),(75,'15031097','2011-05-04','01:21:42 pm'),(76,'4264264','2011-05-04','01:24:53 pm'),(77,'15031097','2011-05-04','01:35:56 pm'),(78,'4264264','2011-05-04','01:36:48 pm'),(79,'12345678','2011-05-04','01:43:44 pm'),(80,'4264264','2011-05-04','01:44:08 pm'),(81,'15031097','2011-05-04','01:45:06 pm'),(82,'4264264','2011-05-04','01:45:19 pm'),(83,'12345678','2011-05-04','01:46:01 pm'),(84,'21184968','2011-05-04','01:48:55 pm'),(85,'4264264','2011-05-04','01:51:04 pm'),(86,'15031097','2011-05-04','01:51:23 pm'),(87,'4264264','2011-05-04','01:52:00 pm'),(88,'21184968','2011-05-04','02:15:58 pm'),(89,'15031097','2011-05-04','02:21:42 pm'),(90,'15031097','2011-05-04','02:22:34 pm'),(91,'12345678','2011-05-04','02:22:44 pm'),(92,'15031097','2011-05-04','02:23:38 pm'),(93,'12345678','2011-05-04','02:25:48 pm'),(94,'16933715','2011-05-04','02:26:17 pm'),(95,'12345678','2011-05-04','02:28:56 pm'),(96,'10715628','2011-05-04','02:38:37 pm'),(97,'12345678','2011-05-04','02:38:42 pm'),(98,'12345678','2011-05-04','02:41:32 pm'),(99,'15031097','2011-05-04','02:42:57 pm'),(100,'11960592','2011-05-04','02:50:26 pm'),(101,'12345678','2011-05-04','03:19:13 pm'),(102,'21184968','2011-05-04','03:19:51 pm'),(103,'15031097','2011-05-05','08:17:00 am'),(104,'21184968','2011-05-05','08:20:37 am'),(105,'21184968','2011-05-05','08:58:28 am'),(106,'21184968','2011-05-05','09:05:12 am'),(107,'15031097','2011-05-05','09:05:31 am'),(108,'21184968','2011-05-05','10:06:35 am'),(109,'21184968','2011-05-05','10:35:23 am'),(110,'21184968','2011-05-05','11:24:16 am'),(111,'21184968','2011-05-05','12:57:56 pm'),(112,'21184968','2011-05-05','01:25:05 pm'),(113,'21184968','2011-05-05','02:32:56 pm'),(114,'21184968','2011-05-06','08:21:16 am'),(115,'21184968','2011-05-06','08:43:30 am'),(116,'21184968','2011-05-06','09:05:20 am'),(117,'21184968','2011-05-06','09:28:59 am'),(118,'21184968','2011-05-06','10:04:49 am'),(119,'12345678','2011-05-06','11:04:45 am'),(120,'21184968','2011-05-06','11:05:29 am'),(121,'21184968','2011-05-06','11:10:04 am'),(122,'21184968','2011-05-06','12:19:20 pm'),(123,'15031097','2011-05-07','08:02:25 am'),(124,'15031097','2011-05-07','08:50:12 am'),(125,'4264264','2011-05-07','09:04:06 am'),(126,'15031097','2011-05-07','04:24:48 pm'),(127,'15031097','2011-05-19','10:11:19 am'),(128,'15031097','2011-05-23','08:05:06 pm'),(129,'15031097','2011-05-23','08:21:31 pm'),(130,'4264264','2011-05-30','01:58:59 pm'),(131,'4264264','2011-05-30','02:00:47 pm'),(132,'15031097','2011-05-30','02:02:12 pm'),(133,'15031097','2011-05-30','02:57:29 pm'),(134,'15031097','2011-05-31','08:15:05 pm'),(135,'15031097','2011-06-16','10:09:01 am'),(136,'15031097','2011-06-16','10:14:17 am'),(137,'15031097','2011-06-23','11:52:16 am'),(138,'15031097','2011-06-23','02:06:58 pm'),(139,'4264264','2011-06-23','02:30:03 pm'),(140,'15031097','2011-06-23','02:33:32 pm'),(141,'15031097','2011-06-23','02:47:46 pm'),(142,'4264264','2011-06-23','02:58:36 pm'),(143,'4264264','2011-06-23','03:00:58 pm'),(144,'15031097','2011-06-23','03:01:47 pm'),(145,'15031097','2011-06-27','08:28:43 am'),(146,'15031097','2011-06-27','10:20:01 am'),(147,'15031097','2011-06-27','10:51:09 am'),(148,'15031097','2011-06-27','11:45:59 am'),(149,'15031097','2011-06-27','03:19:30 pm'),(150,'15031097','2011-06-28','08:07:28 am'),(151,'15031097','2011-06-28','09:00:29 am'),(152,'15031097','2011-06-28','01:39:34 pm'),(153,'4264264','2011-06-28','01:40:47 pm'),(154,'15031097','2011-06-28','02:55:17 pm'),(155,'15031097','2011-06-28','03:21:12 pm'),(156,'15031097','2011-06-30','12:23:47 pm'),(157,'15031097','2011-06-30','12:27:17 pm'),(158,'15031097','2011-06-30','01:15:55 pm'),(159,'15031097','2011-06-30','02:16:46 pm'),(160,'15031097','2011-07-01','08:46:02 am'),(161,'15031097','2011-07-01','09:17:53 am'),(162,'15031097','2011-07-01','10:34:21 am'),(163,'15031097','2011-07-02','09:09:49 pm'),(164,'15031097','2011-07-02','09:19:25 pm'),(165,'15031097','2011-07-02','10:28:31 pm'),(166,'15031097','2011-07-05','04:58:19 pm'),(167,'15031097','2011-07-05','07:34:21 pm'),(168,'15031097','2011-07-06','08:29:58 am'),(169,'15031097','2011-07-06','12:46:29 pm'),(170,'15031097','2011-07-06','08:00:02 pm'),(171,'15031097','2011-07-06','09:18:55 pm'),(172,'15031097','2011-07-07','09:00:40 am'),(173,'15031097','2011-07-07','09:02:11 am'),(174,'15031097','2011-07-07','10:14:28 am'),(175,'15031097','2011-07-07','10:18:51 am'),(176,'15031097','2011-07-07','01:57:02 pm'),(177,'15031097','2011-07-07','02:20:21 pm'),(178,'4264264','2011-07-07','02:26:21 pm'),(179,'15031097','2011-07-07','02:41:16 pm'),(180,'15031097','2011-07-08','08:24:54 am'),(181,'15031097','2011-07-08','11:46:17 am'),(182,'15031097','2011-07-08','12:56:29 pm'),(183,'12345678','2011-07-08','12:57:55 pm'),(184,'19422787','2011-07-08','01:01:10 pm'),(185,'12345678','2011-07-08','01:03:27 pm'),(186,'4264264','2011-07-08','01:04:08 pm'),(187,'15031097','2011-07-08','01:05:55 pm'),(188,'15031097','2011-07-08','01:44:36 pm'),(189,'12345678','2011-07-08','01:48:00 pm'),(190,'15031097','2011-07-09','07:33:57 am'),(191,'15031097','2011-07-12','03:21:24 pm'),(192,'15031097','2011-07-12','05:23:08 pm'),(193,'19422787','2011-07-12','05:34:07 pm'),(194,'19422787','2011-07-12','07:19:37 pm'),(195,'19422787','2011-07-12','09:45:28 pm'),(196,'15031097','2011-07-13','12:42:57 am'),(197,'15031097','2011-07-13','02:39:10 pm'),(198,'15031097','2011-07-15','10:38:14 am'),(199,'15031097','2011-07-15','10:39:50 am'),(200,'4264264','2011-07-15','10:44:41 am'),(201,'15031097','2011-07-15','10:47:38 am'),(202,'15031097','2011-07-15','12:08:17 pm'),(203,'15031097','2011-07-15','12:35:07 pm'),(204,'15031097','2011-07-15','12:59:14 pm'),(205,'15031097','2011-07-18','08:43:36 am'),(206,'15031097','2011-07-18','10:43:22 am'),(207,'15031097','2011-07-18','10:54:24 am'),(208,'12345678','2011-07-18','12:29:21 pm'),(209,'15031097','2011-07-18','12:29:59 pm'),(210,'15031097','2011-07-18','01:03:40 pm'),(211,'15031097','2011-07-18','02:36:14 pm'),(212,'15031097','2011-07-25','08:18:12 am'),(213,'15031097','2011-07-25','08:59:06 am'),(214,'15031097','2011-07-25','09:51:25 am'),(215,'15031097','2011-07-25','01:01:44 pm'),(216,'15031097','2011-07-27','08:17:36 am'),(217,'15031097','2011-07-28','09:50:45 am'),(218,'15031097','2011-07-28','06:48:55 pm'),(219,'15031097','2011-07-28','07:52:20 pm'),(220,'15031097','2011-07-28','08:13:40 pm'),(221,'15031097','2011-08-01','08:37:53 am'),(222,'15031097','2011-08-01','08:38:34 am'),(223,'15031097','2011-08-01','09:49:50 am'),(224,'15031097','2011-08-01','09:52:46 am'),(225,'15031097','2011-08-01','09:54:55 am'),(226,'15031097','2011-08-01','09:59:36 am'),(227,'15031097','2011-08-01','10:26:23 am'),(228,'15031097','2011-08-01','10:29:32 am'),(229,'15031097','2011-08-01','10:41:28 am'),(230,'15031097','2011-08-01','10:43:16 am'),(231,'15031097','2011-08-01','10:51:39 am'),(232,'15031097','2011-08-01','11:51:26 am'),(233,'15031097','2011-08-01','11:52:03 am'),(234,'15031097','2011-08-01','12:03:57 pm'),(235,'15031097','2011-08-01','12:29:35 pm'),(236,'15031097','2011-08-01','12:51:07 pm'),(237,'15031097','2011-08-01','01:08:28 pm'),(238,'15031097','2011-08-02','08:04:33 am'),(239,'15031097','2011-08-02','08:26:04 am'),(240,'15031097','2011-08-02','08:57:56 am'),(241,'15031097','2011-08-02','09:31:59 am'),(242,'15031097','2011-08-02','09:38:15 am'),(243,'15031097','2011-08-02','01:00:44 pm'),(244,'15031097','2011-08-02','01:09:53 pm'),(245,'15031097','2011-08-03','09:05:25 am'),(246,'15031097','2011-08-03','09:13:24 am'),(247,'15031097','2011-08-03','09:17:57 am'),(248,'12345678','2011-08-03','09:19:08 am'),(249,'15031097','2011-08-04','10:37:52 am'),(250,'15031097','2011-08-04','12:01:38 pm'),(251,'15031097','2011-08-04','02:25:04 pm'),(252,'15031097','2011-08-05','09:27:33 am'),(253,'15031097','2011-08-05','11:43:52 am'),(254,'15031097','2011-08-05','07:28:08 pm'),(255,'15031097','2011-08-06','11:04:32 am'),(256,'15031097','2011-08-06','12:52:16 pm'),(257,'15031097','2011-08-06','04:04:27 pm'),(258,'15031097','2011-08-07','11:46:57 am'),(259,'15031097','2011-08-07','11:53:52 am'),(260,'15031097','2011-08-07','05:37:06 pm'),(261,'15031097','2011-08-07','07:12:58 pm'),(262,'12345678','2011-08-07','11:17:37 pm'),(263,'15031097','2011-08-08','07:46:58 am'),(264,'15031097','2011-08-08','09:35:37 am'),(265,'15031097','2011-08-08','09:46:03 am'),(266,'15031097','2011-08-08','12:59:06 pm'),(267,'15031097','2011-08-08','01:15:11 pm'),(268,'15031097','2011-08-08','01:15:55 pm'),(269,'15031097','2011-08-09','08:17:45 am'),(270,'15031097','2011-08-09','08:24:34 am'),(271,'15031097','2011-08-09','08:34:27 am'),(272,'15031097','2011-08-09','08:40:01 am'),(273,'15031097','2011-08-09','09:09:10 am'),(274,'15031097','2011-08-09','09:48:54 am'),(275,'15031097','2011-08-09','10:28:33 am'),(276,'15031097','2011-08-09','10:51:14 am'),(277,'15031097','2011-08-09','11:11:51 am'),(278,'15031097','2011-08-09','11:37:31 am'),(279,'15031097','2011-08-09','12:05:44 pm'),(280,'15031097','2011-08-09','12:14:38 pm'),(281,'15031097','2011-08-09','12:21:52 pm'),(282,'15031097','2011-08-09','12:28:37 pm'),(283,'12345678','2011-08-09','12:33:03 pm'),(284,'15031097','2011-08-09','12:33:54 pm'),(285,'12345678','2011-08-09','12:34:54 pm'),(286,'15031097','2011-08-09','12:35:21 pm'),(287,'6343267','2011-08-09','01:41:50 pm'),(288,'15031097','2011-08-09','02:16:06 pm'),(289,'15031097','2011-08-09','02:45:33 pm'),(290,'15031097','2011-08-09','02:48:18 pm'),(291,'15031097','2011-08-09','09:03:43 pm'),(292,'15031097','2011-08-09','09:44:30 pm'),(293,'15031097','2011-08-09','10:00:54 pm'),(294,'15031097','2011-08-09','11:43:35 pm'),(295,'15031097','2011-08-10','12:03:07 am'),(296,'15031097','2011-08-10','07:55:43 am'),(297,'12345678','2011-08-10','07:57:01 am'),(298,'15031097','2011-08-10','08:04:33 am'),(299,'15031097','2011-08-10','08:04:40 am'),(300,'12345678','2011-08-10','08:04:51 am'),(301,'15031097','2011-08-10','08:19:15 am'),(302,'6343267','2011-08-10','08:20:42 am'),(303,'12345678','2011-08-10','08:20:53 am'),(304,'6343267','2011-08-10','08:21:23 am'),(305,'15031097','2011-08-10','03:01:38 pm'),(306,'15031097','2011-08-10','03:07:50 pm'),(307,'15031097','2011-08-11','08:29:57 am'),(308,'15031097','2011-08-11','08:34:05 am'),(309,'15031097','2011-08-11','08:41:42 am');
/*!40000 ALTER TABLE `entradas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familiares`
--

DROP TABLE IF EXISTS `familiares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familiares` (
  `cod_fam` int(2) NOT NULL AUTO_INCREMENT COMMENT 'C�digo del Familiar',
  `ced_per` varchar(12) COLLATE latin1_spanish_ci NOT NULL COMMENT 'C�dula del Personal',
  `ced_fam` varchar(12) COLLATE latin1_spanish_ci NOT NULL COMMENT 'C�dula del  del Familiar',
  `nom_fam` varchar(50) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Nombre  del Familiar',
  `ape_fam` varchar(50) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Apellido  del Familiar',
  `sex_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Sexo del Familiar',
  `fnac_fam` date NOT NULL COMMENT 'Fecha de Nacimiento del Familiar',
  `par_fam` varchar(25) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Parentesco  del Familiar',
  `est_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Estudia el Familiar?',
  `obs_fam` varchar(255) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Observaciones acerca del Familiar',
  PRIMARY KEY (`cod_fam`),
  UNIQUE KEY `ced_per` (`ced_per`,`nom_fam`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Datos de los Familiares del Personal';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familiares`
--

LOCK TABLES `familiares` WRITE;
/*!40000 ALTER TABLE `familiares` DISABLE KEYS */;
INSERT INTO `familiares` VALUES (2,'15031097','14832319','Zulay ','Dávila de Márquez','F','1979-04-12','C','S',''),(3,'16933715','16933715','   Dorian Alejandro','Sayago Vivas ','M','2010-07-28','H','',''),(4,'4264264','17364706','Yusbely M.','Rodriguez ','F','1985-11-13','C','',''),(5,'4264264','4264264','Rodrisbel Valetina ','Linares Rodríquez','F','2002-07-14','H','S',''),(6,'4264264','','Carlos Manuel','Puentes Rodriquez','M','2007-07-27','H','',''),(7,'8036119','80318800','José Euclides','Torres Rondó','M','1962-05-11','C','',''),(8,'8036119','25151778','Ana Paula ','Torres Hill','F','1996-08-26','H','S',''),(9,'8036119','27241447','Maria Laura','Torres Hill','F','2000-01-19','H','S',''),(10,'9028505','10240086','Neris Ysolina','Ramirez Contreras ','F','1969-04-28','C','',''),(11,'9028505','21184774','Maryury del valle ','Fernandez Ramirez ','F','1992-03-16','H','S',''),(12,'9028505','20431370','Marly del Valle','Fernandez Ramirez','F','1990-12-15','H','S',''),(13,'15621190','12779485','Alvaro Emir ','valero pernia ','M','1978-02-19','C','',''),(14,'15621190','15621190','Alvani Isamar ','Valero Perez','F','2003-09-20','H','',''),(15,'12346028','8015981','Ana ','Alarcon de Corredor ','F','1932-03-12','C','',''),(16,'12346028','12346028','Eduard ',' Corredor Quintero','M','2010-07-12','H','',''),(17,'12346028','18124667','Ana Maria','Quintero Suarez','F','1984-10-05','C','',''),(18,'11960597','','Andrea V','Silva Agüero','F','2004-06-13','H','S',''),(19,'11960597','','Cesar O.','Silva Agüero','M','2007-10-24','H','',''),(20,'14304657','14304657','Jose Antonio ','Ramirez Pereira','M','1998-04-23','H','S',''),(21,'6343267','2286846','Ana ','Pernia','F','1941-07-26','C','',''),(22,'6343267','19422787','luzmar','Contreras ','F','1990-05-04','H','S',''),(23,'6343267','19422788','wilfredo','Contreas','M','1989-01-31','H','S',''),(24,'16657924','8014836','Rafaela del Carmen ','Perez','F','1956-10-25','C','',''),(25,'10109577','9473331','andres eloy  ','osorio dugarte','M','1968-11-05','C','',''),(26,'10715628','3766590','Udilia ','Avendaño','F','1944-09-04','C','',''),(27,'10715628','','Jesus Manuel','Nava Avendaño','M','2010-11-10','H','',''),(28,'15031097','','Felipe Gabriel','Márquez Dávila','M','2002-10-25','H','S',''),(29,'15031097','','Luis Daniel','Márquez Dávila','M','2004-02-14','H','S',''),(30,'15031097','','Luis Ángel','Márquez Dávila','M','2008-05-20','H','',''),(31,'10109577','','Andrés E.','Osorio Rivas','M','1998-03-19','H','S',''),(32,'10109577','','Eduard Andrés','Osorio Rivas','M','2001-07-20','H','S',''),(33,'15621190','','Diego Josue','Valero Pérez','M','2011-03-15','H','','');
/*!40000 ALTER TABLE `familiares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feriados`
--

DROP TABLE IF EXISTS `feriados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feriados` (
  `fch_frd` date NOT NULL,
  `des_frd` varchar(50) NOT NULL,
  PRIMARY KEY (`fch_frd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de dias Feridos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feriados`
--

LOCK TABLES `feriados` WRITE;
/*!40000 ALTER TABLE `feriados` DISABLE KEYS */;
INSERT INTO `feriados` VALUES ('2011-03-07','Carnaval'),('2011-03-08','Carnaval'),('2011-04-19','Firma Acta de Independencia'),('2011-04-21','Semana Santa'),('2011-04-22','Semana Santa'),('2011-06-24','Dia de San Juan / Batalla de Carabobo'),('2011-07-05','Día de la Independencia de Venezuela'),('2011-07-24','Día de la Muerte del Libertador Simón Bolívar');
/*!40000 ALTER TABLE `feriados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupos`
--

DROP TABLE IF EXISTS `grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupos` (
  `cod_grp` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del grupo',
  `nom_grp` varchar(20) NOT NULL COMMENT 'nombre del grupo',
  `des_grp` varchar(255) NOT NULL COMMENT 'descripcion del grupo',
  PRIMARY KEY (`cod_grp`),
  UNIQUE KEY `nom_grp` (`nom_grp`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupos`
--

LOCK TABLES `grupos` WRITE;
/*!40000 ALTER TABLE `grupos` DISABLE KEYS */;
INSERT INTO `grupos` VALUES (1,'Administradores','Con todos los privilegios'),(2,'Administración','Grupo con todos los privilegios del área de Usuarios'),(3,'Personal','Grupo para el personal adscrito a la institución'),(4,'Pasantes','Privilegios Mínimos'),(5,'BienesP','Grupo de Pasantes');
/*!40000 ALTER TABLE `grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inasistencias`
--

DROP TABLE IF EXISTS `inasistencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inasistencias` (
  `cod_ina` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `fch_ina` date NOT NULL,
  `tip_ina` varchar(30) NOT NULL,
  `des_ina` varchar(50) NOT NULL,
  PRIMARY KEY (`cod_ina`),
  UNIQUE KEY `ced_per` (`ced_per`,`fch_ina`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1 COMMENT='Registro de Inasistencias del personal';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inasistencias`
--

LOCK TABLES `inasistencias` WRITE;
/*!40000 ALTER TABLE `inasistencias` DISABLE KEYS */;
INSERT INTO `inasistencias` VALUES (5,'15621190','2011-04-01','Remunerada','Post-Natal'),(6,'15621190','2011-04-04','Remunerada','Post-Natal'),(7,'15621190','2011-04-05','Remunerada','Post-Natal'),(8,'15621190','2011-04-06','Remunerada','Post-Natal'),(9,'15621190','2011-04-07','Remunerada','Post-Natal'),(10,'15621190','2011-04-08','Remunerada','Post-Natal'),(11,'15621190','2011-04-11','Remunerada','Post-Natal'),(12,'15621190','2011-04-12','Remunerada','Post-Natal'),(13,'15621190','2011-04-13','Remunerada','Post-Natal'),(14,'15621190','2011-04-14','Remunerada','Post-Natal'),(15,'15621190','2011-04-15','Remunerada','Post-Natal'),(16,'15621190','2011-04-18','Remunerada','Post-Natal'),(18,'15621190','2011-04-20','Remunerada','Post-Natal'),(22,'15621190','2011-04-25','Remunerada','Post-Natal'),(23,'8019652','2011-03-01','Remunerada','Vacaciones'),(24,'8019652','2011-03-02','Remunerada','Vacaciones'),(25,'8019652','2011-03-03','Remunerada','Vacaciones'),(26,'8019652','2011-03-04','Remunerada','Vacaciones'),(29,'8019652','2011-03-09','Remunerada','Vacaciones'),(30,'8019652','2011-03-10','Remunerada','Vacaciones'),(31,'8019652','2011-03-11','Remunerada','Vacaciones'),(32,'10715628','2011-03-01','Remunerada','Vacaciones'),(33,'10715628','2011-03-02','Remunerada','Vacaciones'),(34,'10715628','2011-03-03','Remunerada','Vacaciones'),(35,'10715628','2011-03-04','Remunerada','Vacaciones'),(36,'10715628','2011-03-09','Remunerada','Vacaciones'),(37,'8029735','2011-03-17','Remunerada','Permiso'),(38,'8029735','2011-03-18','Remunerada','Permiso'),(39,'8036119','2011-03-21','Remunerada','Permiso (Esposo en Clínica)'),(40,'4264264','2011-03-01','Remunerada','Vacaciones'),(41,'4264264','2011-03-02','Remunerada','Vacaciones'),(42,'4264264','2011-03-03','Remunerada','Vacaciones'),(43,'4264264','2011-03-04','Remunerada','Vacaciones'),(44,'4264264','2011-03-09','Remunerada','Vacaciones'),(45,'4264264','2011-03-10','Remunerada','Vacaciones'),(46,'4264264','2011-03-11','Remunerada','Vacaciones'),(47,'4264264','2011-03-14','Remunerada','Vacaciones'),(48,'4264264','2011-03-15','Remunerada','Vacaciones'),(49,'4264264','2011-03-16','Remunerada','Vacaciones'),(50,'4264264','2011-03-17','Remunerada','Vacaciones'),(51,'4264264','2011-03-18','Remunerada','Vacaciones'),(53,'4264264','2011-03-21','Remunerada','Vacaciones'),(54,'4264264','2011-03-22','Remunerada','Vacaciones'),(55,'4264264','2011-03-23','Remunerada','Vacaciones'),(56,'6343267','2011-03-18','Remunerada','Permiso (Vacaciones)'),(57,'11960597','2011-03-09','Remunerada','Permiso'),(58,'16657924','2011-02-03','Remunerada','Estudios'),(59,'8019652','2011-02-28','Remunerada','Vacaciones'),(60,'10109577','2011-02-04','Remunerada','No Firmó'),(61,'11960597','2011-02-07','No Remunerada','No asistió'),(62,'11960597','2011-02-24','No Remunerada','No asistió'),(63,'17130852','2011-07-01','No Remunerada','No había Ingresado'),(64,'17130852','2011-07-04','No Remunerada','No había Ingresado'),(66,'17130852','2011-07-06','No Remunerada','No había Ingresado'),(67,'17130852','2011-07-07','No Remunerada','No había Ingresado'),(68,'17130852','2011-07-08','No Remunerada','No había Ingresado'),(69,'17130852','2011-07-11','No Remunerada','No había Ingresado'),(70,'11960597','2011-08-09','No Remunerada','Cita en Tribunal');
/*!40000 ALTER TABLE `inasistencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incidencias`
--

DROP TABLE IF EXISTS `incidencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incidencias` (
  `cod_inc` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la asignacion',
  `con_inc` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `mes_inc` int(2) NOT NULL COMMENT 'Mes en que se paga la incidencia',
  `ano_inc` int(4) NOT NULL COMMENT 'Ano en que se paga la incidencia',
  `fini_inc` date NOT NULL COMMENT 'Fecha de inicio del Periodo de Inicidencia',
  `ffin_inc` date NOT NULL COMMENT 'Fecha de fin del Periodo de Inicidencia',
  PRIMARY KEY (`cod_inc`),
  UNIQUE KEY `mes_inc` (`mes_inc`,`ano_inc`,`fini_inc`,`ffin_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incidencias`
--

LOCK TABLES `incidencias` WRITE;
/*!40000 ALTER TABLE `incidencias` DISABLE KEYS */;
INSERT INTO `incidencias` VALUES (1,'asdfasdfasdfasf',6,2011,'2011-06-01','2011-06-30');
/*!40000 ALTER TABLE `incidencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incidencias_sueldos`
--

DROP TABLE IF EXISTS `incidencias_sueldos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incidencias_sueldos` (
  `cod_inc` int(11) NOT NULL COMMENT 'Codigo de incidencias',
  `cod_sue` int(2) NOT NULL COMMENT 'Codigo del sueldo',
  `mnt_inc` double(9,2) NOT NULL COMMENT 'Monto o porcentaje de la incidencia',
  `bas_inc` varchar(1) NOT NULL COMMENT 'Base para el calculo de la Incidencia',
  UNIQUE KEY `cod_inc` (`cod_inc`,`cod_sue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de inicidencias para cada sueldo';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incidencias_sueldos`
--

LOCK TABLES `incidencias_sueldos` WRITE;
/*!40000 ALTER TABLE `incidencias_sueldos` DISABLE KEYS */;
INSERT INTO `incidencias_sueldos` VALUES (1,1,15.00,'S'),(1,2,15.00,'S'),(1,3,15.00,'S'),(1,4,15.00,'S'),(1,5,15.00,'S'),(1,6,15.00,'S'),(1,7,15.00,'S'),(1,8,15.00,'S'),(1,9,15.00,'S'),(1,10,15.00,'S'),(1,11,15.00,'S'),(1,12,15.00,'S'),(1,13,15.00,'S'),(1,14,15.00,'S'),(1,15,15.00,'S'),(1,16,15.00,'S'),(2,1,10.00,'S'),(2,2,0.00,'S'),(2,3,0.00,'S'),(2,4,0.00,'S'),(2,5,0.00,'S'),(2,6,0.00,'S'),(2,7,0.00,'S'),(2,8,0.00,'S'),(2,9,0.00,'S'),(2,10,0.00,'S'),(2,11,0.00,'S'),(2,12,0.00,'S'),(2,13,0.00,'S'),(2,14,0.00,'S'),(2,15,0.00,'S'),(2,16,0.00,'S');
/*!40000 ALTER TABLE `incidencias_sueldos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nomina_asign`
--

DROP TABLE IF EXISTS `nomina_asign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomina_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(50) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nomina_asign`
--

LOCK TABLES `nomina_asign` WRITE;
/*!40000 ALTER TABLE `nomina_asign` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomina_asign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nomina_deduc`
--

DROP TABLE IF EXISTS `nomina_deduc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomina_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nomina_deduc`
--

LOCK TABLES `nomina_deduc` WRITE;
/*!40000 ALTER TABLE `nomina_deduc` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomina_deduc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nomina_incidencias`
--

DROP TABLE IF EXISTS `nomina_incidencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomina_incidencias` (
  `ano_inc` int(4) NOT NULL,
  `mes_inc` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `mnt_inc` double(9,2) NOT NULL,
  `con_inc` varchar(255) NOT NULL,
  `ivss_inc` double(9,2) NOT NULL,
  `spf_inc` double(9,2) NOT NULL,
  `cah_inc` double(9,2) NOT NULL,
  `bnoc_inc` double(9,2) NOT NULL,
  `bvac_inc` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_inc`,`mes_inc`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nomina_incidencias`
--

LOCK TABLES `nomina_incidencias` WRITE;
/*!40000 ALTER TABLE `nomina_incidencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomina_incidencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nomina_pagar`
--

DROP TABLE IF EXISTS `nomina_pagar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomina_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  PRIMARY KEY (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nomina_pagar`
--

LOCK TABLES `nomina_pagar` WRITE;
/*!40000 ALTER TABLE `nomina_pagar` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomina_pagar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nominapre_asign`
--

DROP TABLE IF EXISTS `nominapre_asign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nominapre_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(50) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nominapre_asign`
--

LOCK TABLES `nominapre_asign` WRITE;
/*!40000 ALTER TABLE `nominapre_asign` DISABLE KEYS */;
/*!40000 ALTER TABLE `nominapre_asign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nominapre_deduc`
--

DROP TABLE IF EXISTS `nominapre_deduc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nominapre_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nominapre_deduc`
--

LOCK TABLES `nominapre_deduc` WRITE;
/*!40000 ALTER TABLE `nominapre_deduc` DISABLE KEYS */;
INSERT INTO `nominapre_deduc` VALUES (2011,8,'9028505','10',1,'CBN',2,4,'Préstamo de Caja de Ahorros (1/5)',1,'Préstamo CAPREAMCE',138.27),(2011,8,'15621190','12',1,'UAD',2,8,'Préstamo de Caja de Ahorros (1/36)',1,'Préstamo CAPREAMCE',168.79),(2011,8,'15031097','17',1,'ADM',3,9,'Compra de lentes correctivos (1/1)',3,'Alilente',100.00),(2011,8,'8023210','2',1,'DSC',1,3,'Préstamo de Caja de Ahorros (1/48)',1,'Préstamo CAPREAMCE',524.66),(2011,8,'6343267','3',1,'ADM',1,2,'Préstamo de Caja de Ahorros (1/48)',1,'Préstamo CAPREAMCE',132.12),(2011,8,'8029735','4',1,'SCR',2,5,'Préstamo de Caja de Ahorros (1/48)',1,'Préstamo CAPREAMCE',135.38),(2011,8,'4264264','6',1,'SLT',1,1,'Préstamo Caja de Ahorros (17/24)',1,'Préstamo CAPREAMCE',243.90),(2011,8,'10715628','7',1,'UAD',2,7,'Préstamo de Caja de Ahorros (1/48)',1,'Préstamo CAPREAMCE',316.57),(2011,8,'11960597','9',1,'SLT',2,6,'Préstamo de Caja de Ahorros (1/36)',1,'Préstamo CAPREAMCE',132.10);
/*!40000 ALTER TABLE `nominapre_deduc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nominapre_pagar`
--

DROP TABLE IF EXISTS `nominapre_pagar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nominapre_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  PRIMARY KEY (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nominapre_pagar`
--

LOCK TABLES `nominapre_pagar` WRITE;
/*!40000 ALTER TABLE `nominapre_pagar` DISABLE KEYS */;
INSERT INTO `nominapre_pagar` VALUES (2011,8,1,'8023210','V','José Andrés','Briceño Valero',3059.72,3059.73,585.85,2473.88,0.00,0.00,0.00,0.00,0.00,'DSC','Despacho del Contralor',2,'Contralor Municipal','1993-09-01',1,'Fijo Jefe','FJ','01020441100100028502','2','Venezuela',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,61.19,0.00,0.00,0.00,0.00,0.00,0.00,0.00,6119.45),(2011,8,1,'6343267','V','Luz Marina','Salcedo Pernía',1615.54,1615.54,164.43,1451.11,0.00,0.00,0.00,0.00,0.00,'ADM','Administración',3,'Administrador(a)','1996-05-06',1,'Fijo Jefe','FJ','01020441140100028505','2','Venezuela',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,32.31,0.00,0.00,0.00,0.00,0.00,0.00,0.00,3231.07),(2011,8,1,'8029735','V','Nora Coromoto','Cerrada',829.21,829.21,151.96,677.25,0.00,0.00,0.00,0.00,0.00,'SCR','Secretaría',4,'Secretaria Ejecutiva','1991-07-19',2,'Fijo','F','01050747290747054037','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,16.58,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1658.42),(2011,8,1,'8036119','V','Elda Soraya','Hill Dávila',1468.67,1468.67,29.37,1439.30,0.00,0.00,0.00,0.00,0.00,'SRJ','Servicio Jurídico',5,'Jefe de Servicios Jurídicos','2007-03-01',1,'Fijo Jefe','FJ','01020681240000032968','2','Venezuela',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,29.37,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2937.34),(2011,8,1,'4264264','V','Carlos Julio ','Puentes Uzcátegui ',1468.67,1468.67,273.27,1195.40,0.00,0.00,0.00,0.00,0.00,'SLT','Sala Técnica',6,'Jefe de Sala Técnica','2004-07-12',1,'Fijo Jefe','FJ','01020681210000033640','1','Venezuela',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,29.37,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2937.34),(2011,8,1,'10715628','V','Yoly Josefina','Avendaño Meza',1125.85,1125.85,339.09,786.76,0.00,0.00,0.00,0.00,0.00,'UAD','Unidad de Auditoría',7,'Auditor II','1999-11-15',2,'Fijo','F','01050747210747049807','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,22.52,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2251.70),(2011,8,1,'10109577','V','María Isabel','Rivas de Osuna',1294.33,1294.33,25.89,1268.44,0.00,0.00,0.00,0.00,0.00,'UAD','Unidad de Auditoría',8,'Auditor II ','1996-07-01',2,'Fijo','F','01050747220747049661','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,25.89,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2588.65),(2011,8,1,'11960597','V','Jesus Ramón','Silva Osuna',1050.00,1050.00,153.10,896.90,0.00,0.00,0.00,0.00,0.00,'SLT','Sala Técnica',9,'Inspector de Obras II','1999-11-02',2,'Fijo','F','01050184940184025907','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,21.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2100.00),(2011,8,1,'9028505','V','Ramón António','Fernández Rojas',1078.70,1078.70,159.84,918.86,0.00,0.00,0.00,0.00,0.00,'CBN','Control de Bienes',10,'Registrador de Bienes','2006-01-09',2,'Fijo','F','01050747220747049076','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,21.57,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2157.40),(2011,8,1,'8019652','V','Oswaldo Ramón ','Ramos',774.11,774.11,15.48,758.63,0.00,0.00,0.00,0.00,0.00,'CBN','Control de Bienes',11,'Asistente de Control y Fiscalización','1995-02-08',2,'Fijo','F','01050747210747049483','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,15.48,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1548.22),(2011,8,1,'15621190','V','Yohana','Pérez de Valero',1031.80,1031.80,189.43,842.37,0.00,0.00,0.00,0.00,0.00,'UAD','Unidad de Auditoría',12,'Auditor I','2008-08-18',2,'Fijo','F','01050747290747049548','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.010,20.64,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2063.60),(2011,8,1,'14304657','V','Leidy Josefina','Pereira Ochoa',845.00,845.00,0.00,845.00,0.00,0.00,0.00,0.00,0.00,'UAD','Unidad de Auditoría',13,'Auxiliar de Auditoría','2010-04-22',3,'Contratado','C','01050672757672055038','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.000,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1690.00),(2011,8,1,'12346028','V','José de Jesús','Corredor Alarcón',845.00,845.00,0.00,845.00,0.00,0.00,0.00,0.00,0.00,'SLT','Sala Técnica',14,'Técnico Inspector de Obras Civiles','2010-04-21',3,'Contratado','C','01050672717672028529','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.000,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1690.00),(2011,8,1,'16933715','V','Alejandro António','Sayago Gonzáles ',900.00,900.00,0.00,900.00,0.00,0.00,0.00,0.00,0.00,'SRJ','Servicio Jurídico',15,'Abogado','2011-01-01',3,'Contratado','C','01050747210747059888','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.000,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1800.00),(2011,8,1,'16657924','V','María Alejandra ','Pérez',611.95,611.95,0.00,611.95,0.00,0.00,0.00,0.00,0.00,'ADM','Administración',16,'Aseadora','2010-02-01',3,'Contratado','C','01050747270747054010','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.000,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1223.89),(2011,8,1,'15031097','V','Luis Felipe','Márquez Briceño',845.00,845.00,100.00,745.00,0.00,0.00,0.00,0.00,0.00,'ADM','Administración',17,'Operador de Equipos de Computación','2011-01-01',3,'Contratado','C','01050092310092239498','2','Mercantil',0,0.00,0.00,0.00,0.00,0.00,0.00,0.000,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1690.00);
/*!40000 ALTER TABLE `nominapre_pagar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oficios_enviados`
--

DROP TABLE IF EXISTS `oficios_enviados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oficios_enviados` (
  `cod_ofi` int(11) NOT NULL AUTO_INCREMENT,
  `num_ofi` int(4) unsigned zerofill NOT NULL,
  `fch_ofi` date NOT NULL,
  `rdp_ofi` varchar(75) NOT NULL,
  `rpr_ofi` varchar(75) NOT NULL,
  `ddp_ofi` varchar(75) NOT NULL,
  `dpr_ofi` varchar(75) NOT NULL,
  `des_ofi` varchar(255) NOT NULL,
  `nds_ofi` int(2) NOT NULL,
  `fen_ofi` date DEFAULT NULL,
  `ftr_ofi` date DEFAULT NULL,
  `frs_ofi` date DEFAULT NULL,
  `ref_ofi` varchar(7) NOT NULL,
  `obs_ofi` varchar(255) NOT NULL,
  `est_ofi` varchar(1) NOT NULL COMMENT 'Estado del Oficio',
  PRIMARY KEY (`cod_ofi`),
  UNIQUE KEY `num_ofi` (`num_ofi`,`fch_ofi`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Oficios Enviados';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oficios_enviados`
--

LOCK TABLES `oficios_enviados` WRITE;
/*!40000 ALTER TABLE `oficios_enviados` DISABLE KEYS */;
INSERT INTO `oficios_enviados` VALUES (1,0001,'2011-06-23','Sala Técnica','Carlos Julio Puentes Uzcátegui','Alcaldia','Pedro Alvarez','Solicitud de Info de gestion 2010',3,'2011-06-20','2011-06-23','0000-00-00','','','A');
/*!40000 ALTER TABLE `oficios_enviados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagados`
--

DROP TABLE IF EXISTS `pagados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagados` (
  `ced_per` varchar(12) NOT NULL,
  PRIMARY KEY (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagados`
--

LOCK TABLES `pagados` WRITE;
/*!40000 ALTER TABLE `pagados` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagados_frc`
--

DROP TABLE IF EXISTS `pagados_frc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagados_frc` (
  `cod_car` varchar(12) NOT NULL,
  PRIMARY KEY (`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagados_frc`
--

LOCK TABLES `pagados_frc` WRITE;
/*!40000 ALTER TABLE `pagados_frc` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagados_frc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `part_presup`
--

DROP TABLE IF EXISTS `part_presup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `part_presup` (
  `cod_par` int(11) NOT NULL AUTO_INCREMENT,
  `sec_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `pro_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `act_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `ram_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `par_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `gen_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `esp_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `sub_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `des_par` varchar(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `obs_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`cod_par`),
  UNIQUE KEY `sector` (`par_par`,`gen_par`,`esp_par`,`sub_par`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `part_presup`
--

LOCK TABLES `part_presup` WRITE;
/*!40000 ALTER TABLE `part_presup` DISABLE KEYS */;
INSERT INTO `part_presup` VALUES (1,NULL,NULL,NULL,NULL,'4.01','01','01','00','Sueldos Básico Personal Fijo a Tiempo Completo',NULL),(2,NULL,NULL,NULL,NULL,'4.01','01','03','00','Suplencias a Empleados',NULL),(3,NULL,NULL,NULL,NULL,'4.01','01','18','00','Remuneración al Personal Contratado',NULL),(4,NULL,NULL,NULL,NULL,'4.01','01','19','00','Retribuc. por becas-salarios, bolsas de trabajo, pasantias',NULL),(5,NULL,NULL,NULL,NULL,'4.01','03','03','00','Primas por Hogar',NULL),(6,NULL,NULL,NULL,NULL,'4.01','03','04','00','Prima por hijos a Empleados',NULL),(7,NULL,NULL,NULL,NULL,'4.01','03','08','00','Prima de Profesionalización a Empleados',NULL),(8,NULL,NULL,NULL,NULL,'4.01','03','09','00','Prima por antiguedad a Empelados',NULL),(9,NULL,NULL,NULL,NULL,'4.01','03','10','00','Prima por jerarquía o responsabilidad en el cargo',NULL),(10,NULL,NULL,NULL,NULL,'4.01','03','97','00','Otras primas',NULL),(11,NULL,NULL,NULL,NULL,'4.01','04','05','00','Complemento a empleados por gastos de represent',NULL),(12,NULL,NULL,NULL,NULL,'4.01','04','06','00','Complemento a empleados por comisión de servicio',NULL),(13,NULL,NULL,NULL,NULL,'4.01','04','08','00','Bono Compensatorio de Alimentación a Empleados',NULL),(14,NULL,NULL,NULL,NULL,'4.01','04','26','00','Bono Compensatorio de Alimentación a pesonal contratado',NULL),(15,NULL,NULL,NULL,NULL,'4.01','05','01','00','Aguinaldos a Empleados',NULL),(16,NULL,NULL,NULL,NULL,'4.01','05','03','00','Bono Vacacional a Empleados',NULL),(17,NULL,NULL,NULL,NULL,'4.01','05','07','00','Aguinaldos a personal contratado',NULL),(18,NULL,NULL,NULL,NULL,'4.01','05','08','00','Bono Vacacional a personal contratado',NULL),(19,NULL,NULL,NULL,NULL,'4.01','06','01','00','Aporte Patronal al IVSS por empleados',NULL),(20,NULL,NULL,NULL,NULL,'4.01','06','04','00','Aporte Patronal al Fondo Seg. de Paro Fzoso Empleados',NULL),(21,NULL,NULL,NULL,NULL,'4.01','06','05','00','Aporte Patronal Fondo de Ahorro Hab Empleados',NULL),(22,NULL,NULL,NULL,NULL,'4.01','07','02','00','Becas a Empleados y a hijos de empleados',NULL),(23,NULL,NULL,NULL,NULL,'4.01','07','06','00','Ayud. p/ medicin. Gtos Med. Odont y Hospit Empleados',NULL),(24,NULL,NULL,NULL,NULL,'4.01','07','07','00','Aporte patronal a cajas de ahorro empleados',NULL),(25,NULL,NULL,NULL,NULL,'4.01','07','09','00','Ayudas aempleados para adquisición de uniformes y utiles escolares de sus hijos',NULL),(26,NULL,NULL,NULL,NULL,'4.01','07','10','00','Dotación de Uniformes a Empleados',NULL),(27,NULL,NULL,NULL,NULL,'4.01','07','12','00','Aporte Patronal p la adqui de juguetes p hijos de Empleados',NULL),(28,NULL,NULL,NULL,NULL,'4.01','08','01','00','Prestaciones Sociales e Indemnizaciones a empleados',NULL),(29,NULL,NULL,NULL,NULL,'4.01','08','03','00','Prestaciones Sociales e indemnizaciones a personal contratado',NULL),(30,NULL,NULL,NULL,NULL,'4.02','01','01','00','Alimentos y Bebidas para personas',NULL),(31,NULL,NULL,NULL,NULL,'4.02','04','03','00','Cauchos y tripas para vehiculos',NULL),(32,NULL,NULL,NULL,NULL,'4.02','05','01','00','Pulpa de madera, papel y cartón',NULL),(33,NULL,NULL,NULL,NULL,'4.02','05','03','00','Productos de papel y cartón para oficina',NULL),(34,NULL,NULL,NULL,NULL,'4.02','05','04','00','Libros, revistas y periódicos',NULL),(35,NULL,NULL,NULL,NULL,'4.02','06','03','00','Tintas, pinturas y colorantes',NULL),(36,NULL,NULL,NULL,NULL,'4.02','06','06','00','Combustible y lubricantes',NULL),(37,NULL,NULL,NULL,NULL,'4.02','08','07','00','Material de señalamiento',NULL),(38,NULL,NULL,NULL,NULL,'4.02','08','09','00','Repuestos y Accesorios p/ equipos de transporte',NULL),(39,NULL,NULL,NULL,NULL,'4.02','10','02','00','Materiales y útiles de limpieza y aseo',NULL),(40,NULL,NULL,NULL,NULL,'4.02','10','05','00','Útiles Escritorio, Ofic. y materiales instrucción',NULL),(41,NULL,NULL,NULL,NULL,'4.02','10','06','00','Condecoraciones ofrendas y similares',NULL),(42,NULL,NULL,NULL,NULL,'4.02','10','07','00','Productos de seguridad en el trabajo',NULL),(43,NULL,NULL,NULL,NULL,'4.02','10','08','00','Materiales p/Equipos de Computación',NULL),(44,NULL,NULL,NULL,NULL,'4.02','10','09','00','Especies timbradas y valores',NULL),(45,NULL,NULL,NULL,NULL,'4.02','10','11','00','Materiales Eléctricos',NULL),(46,NULL,NULL,NULL,NULL,'4.02','10','99','00','Otros productos y útiles diversos',NULL),(47,NULL,NULL,NULL,NULL,'4.02','99','01','00','Otros materiales y Suministros',NULL),(49,NULL,NULL,NULL,NULL,'4.03','04','01','00','Electricidad',NULL),(50,NULL,NULL,NULL,NULL,'4.03','04','03','00','Agua',NULL),(51,NULL,NULL,NULL,NULL,'4.03','04','04','00','Teléfono',NULL),(52,NULL,NULL,NULL,NULL,'4.03','04','05','00','Servicio de comunicaciones',NULL),(53,NULL,NULL,NULL,NULL,'4.03','04','06','00','Servicio de Aseo Urbano y Dominciliario',NULL),(54,NULL,NULL,NULL,NULL,'4.03','04','07','00','Servicio de Condominio',NULL),(55,NULL,NULL,NULL,NULL,'4.03','06','01','00','Fletes y embalajes',NULL),(56,NULL,NULL,NULL,NULL,'4.03','07','01','00','Publicidad y propaganda',NULL),(57,NULL,NULL,NULL,NULL,'4.03','07','02','00','Imprenta y Reproducción',NULL),(58,NULL,NULL,NULL,NULL,'4.03','07','03','00','Relaciones Sociales',NULL),(59,NULL,NULL,NULL,NULL,'4.03','07','04','00','Avisos',NULL),(60,NULL,NULL,NULL,NULL,'4.03','08','01','00','Primas y gastos de Seguros',NULL),(61,NULL,NULL,NULL,NULL,'4.03','08','02','00','Comisiones y Gastos Bancarios',NULL),(62,NULL,NULL,NULL,NULL,'4.03','09','01','00','Viaticos y pasajes dentro del país',NULL),(63,NULL,NULL,NULL,NULL,'4.03','10','01','00','Servicios Jurídicos',NULL),(64,NULL,NULL,NULL,NULL,'4.03','10','02','00','Servicios de Contabilidad y Auditoría',NULL),(65,NULL,NULL,NULL,NULL,'4.03','10','07','00','Servicios de Capacitación y Adiestramiento',NULL),(66,NULL,NULL,NULL,NULL,'4.03','11','02','00','Conserv. y reparac. menores equipo de transp. trac y elev',NULL),(67,NULL,NULL,NULL,NULL,'4.03','11','03','00','Conserv.  y rep. Men. de equipos de comunic y señalamiento',NULL),(68,NULL,NULL,NULL,NULL,'4.03','11','07','00','Conserv. y reparac. Men. de máq. muebles y demás equipos',NULL),(69,NULL,NULL,NULL,NULL,'4.03','11','99','00','Conserv.  y rep. Men. de otras maq. y equipos',NULL),(70,NULL,NULL,NULL,NULL,'4.03','15','02','00','Tasas y otros derechos obligatorios',NULL),(71,NULL,NULL,NULL,NULL,'4.03','17','01','00','Serv. de gestión adm. prestados por org. asist. tecnica',NULL),(72,NULL,NULL,NULL,NULL,'4.03','18','01','00','Impuesto al Valor Agregado',NULL),(73,NULL,NULL,NULL,NULL,'4.03','99','01','00','Otros Servicios no profesionales',NULL),(74,NULL,NULL,NULL,NULL,'4.04','01','01','02','Repuestos mayores p/ Equipos Transp. Tracc y elevación',NULL),(75,NULL,NULL,NULL,NULL,'4.04','01','02','02','Reparaciones Mayores p/ Equipos Transp. Trac. y Elevación',NULL),(76,NULL,NULL,NULL,NULL,'4.04','02','01','00','Conserv. Ampliac. y Mejoras mayores de Obras en Bienes Domin Privado',NULL),(77,NULL,NULL,NULL,NULL,'4.04','04','01','00','Vehículos automotores terrestres',NULL),(78,NULL,NULL,NULL,NULL,'4.04','05','01','00','Equipos de Telecomunicaciones',NULL),(79,NULL,NULL,NULL,NULL,'4.04','09','01','00','Mobiliario y equipos de oficina',NULL),(80,NULL,NULL,NULL,NULL,'4.04','09','02','00','Equipos de Computación',NULL),(81,NULL,NULL,NULL,NULL,'4.04','09','03','00','Mobiliario y equipos de Alojamiento',NULL),(82,NULL,NULL,NULL,NULL,'4.04','12','04','00','Paquetes y programas de computación',NULL),(83,NULL,NULL,NULL,NULL,'4.04','99','01','00','Otros Activos Reales',NULL);
/*!40000 ALTER TABLE `part_presup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permisos`
--

DROP TABLE IF EXISTS `permisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permisos` (
  `ing_prm` varchar(1) NOT NULL COMMENT 'permisos para ingresar',
  `mod_prm` varchar(1) NOT NULL COMMENT 'permisos para modificar',
  `con_prm` varchar(1) NOT NULL COMMENT 'permisos para consultar',
  `eli_prm` varchar(1) NOT NULL COMMENT 'permisos para eliminar',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  `cod_sec` int(2) NOT NULL COMMENT 'codigo de la seccion',
  KEY `cod_grp` (`cod_grp`),
  KEY `cod_sec` (`cod_sec`),
  CONSTRAINT `permisos_ibfk_1` FOREIGN KEY (`cod_grp`) REFERENCES `grupos` (`cod_grp`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permisos_ibfk_2` FOREIGN KEY (`cod_sec`) REFERENCES `secciones` (`cod_sec`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Permisologia para los Usuarios del Sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permisos`
--

LOCK TABLES `permisos` WRITE;
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` VALUES ('','','','',4,11),('','','','',4,2),('','','','',4,8),('','','','',4,3),('','','','',4,7),('','','','',4,6),('A','','A','',4,1),('','','','',4,14),('','','','',4,15),('','','','',4,13),('','','','',4,12),('','','','',4,4),('','','','',4,5),('A','','A','',4,16),('','','','',4,10),('','','','',4,9),('','','','',5,11),('','','','',5,2),('','','','',5,8),('','','','',5,3),('','','','',5,7),('','','','',5,6),('','','','',5,1),('','','','',5,14),('','','','',5,15),('','','','',5,13),('','','','',5,12),('','','','',5,4),('','','','',5,5),('','','','',5,16),('','','','',5,10),('','','','',5,9),('A','A','A','A',5,17),('','','','',3,11),('','','','',3,2),('','','','',3,8),('','','','',3,3),('','','','',3,7),('','','','',3,6),('','','A','',3,1),('','','','',3,14),('','','','',3,15),('','','A','',3,13),('','','','',3,12),('','','','',3,4),('','','','',3,5),('','','','',3,16),('','','','',3,10),('A','A','A','A',3,9),('A','','A','',3,17),('','A','A','A',1,18),('','','','',1,11),('A','A','A','A',1,2),('A','A','A','A',1,8),('A','A','A','A',1,3),('A','A','A','A',1,7),('A','A','A','A',1,6),('A','A','A','A',1,1),('A','A','A','A',1,14),('A','A','A','A',1,15),('A','A','A','A',1,13),('A','A','A','A',1,12),('A','A','A','A',1,4),('','','','',1,5),('A','A','A','A',1,16),('A','A','A','A',1,10),('A','A','A','A',1,9),('','','','',1,17),('','A','A','A',2,18),('','','','',2,11),('A','A','A','A',2,2),('A','A','A','A',2,8),('A','A','A','A',2,3),('A','A','A','A',2,7),('A','A','A','A',2,6),('A','A','A','A',2,1),('A','A','A','A',2,14),('A','A','A','A',2,15),('A','A','A','A',2,13),('A','A','A','A',2,12),('A','A','A','A',2,4),('','','','',2,5),('A','A','A','A',2,16),('A','A','A','A',2,10),('A','A','A','A',2,9),('','','','',2,17);
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal`
--

DROP TABLE IF EXISTS `personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `nac_per` varchar(1) NOT NULL COMMENT 'nacionalidad de la persona',
  `nom_per` varchar(50) NOT NULL COMMENT 'nombre de la persona',
  `ape_per` varchar(50) NOT NULL COMMENT 'apellido de la persona',
  `sex_per` varchar(1) NOT NULL COMMENT 'sexo de la persona',
  `fnac_per` date NOT NULL COMMENT 'fecha de nacimiento de la persona',
  `lnac_per` varchar(50) NOT NULL COMMENT 'lugar de nacimiento de la persona',
  `cor_per` varchar(50) NOT NULL COMMENT 'correo de la persona',
  `pro_per` varchar(50) NOT NULL COMMENT 'profesion de la persona',
  `dir_per` varchar(255) NOT NULL COMMENT 'direccion de la persona',
  `tel_per` varchar(12) NOT NULL COMMENT 'telefono de la persona',
  `cel_per` varchar(12) NOT NULL COMMENT 'celular de la persona',
  `lph_des` varchar(1) NOT NULL COMMENT 'Descuento de ley politica habitacional',
  `spf_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro de paro forzoso',
  `sso_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro social obligatorio',
  `cah_des` varchar(1) NOT NULL COMMENT 'Descuento de caja de ahorro',
  `sfu_des` varchar(1) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'Servicio funerario',
  `hog_asg` varchar(1) NOT NULL COMMENT 'Prima por hogar',
  `hij_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos',
  `ant_asg` varchar(1) NOT NULL COMMENT 'Prima por antiguedad',
  `pro_asg` varchar(1) NOT NULL COMMENT 'Prima por profesionalizaci�n',
  `hje_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos excepcionales',
  `jer_asg` varchar(1) NOT NULL COMMENT 'Prima por Jerarquia',
  `otr_asg` varchar(1) NOT NULL COMMENT 'Prima por Postgrado',
  `fch_reg` date NOT NULL COMMENT 'Fecha de registro en el personal',
  PRIMARY KEY (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal`
--

LOCK TABLES `personal` WRITE;
/*!40000 ALTER TABLE `personal` DISABLE KEYS */;
INSERT INTO `personal` VALUES ('10109577','V','María Isabel','Rivas de Osuna','F','1967-11-05','Merida','u_auditoriaint@hotmail.com','lic contaduria  publica','urb zumba calle 4 A N°556','2212896','04141413561','A','A','A','A','','A','A','A','A','','','','2011-05-03'),('10715628','V','Yoly Josefina','Avendaño Meza','F','1970-06-02','Merida edo Merida','','Contador publico','Santa elena calle 9 Nº10-45c','02742630667','04268283389','A','A','A','A','A','A','A','A','A','','','A','2011-04-25'),('11960597','V','Jesus Ramón','Silva Osuna','M','1975-03-23','Merida edo Merida','je2375@hotmail.com',' tsu construcion civil','calle camejo,ejido.RES El trigal EDF APTO 2-3','02742213223','04149785308','A','A','A','A','A','A','A','A','A','','','','2011-04-25'),('12346028','V','José de Jesús','Corredor Alarcón','M','1974-01-30','Merida edo Merida','tscorredor@hotmail.com','inspector de obras','Manzano Bajo calle urdaneta Nº 32ejido edo merida','02744165181','04147484178','A','A','A','','A','','','','','','','','2011-04-25'),('14304657','V','Leidy Josefina','Pereira Ochoa','F','1979-04-07','Bejuma edo carabobo','ladypereirao@hotmail.com','tecnico superior en contaduria','av centenario, residencia el molino torre 10 apto 1-4','02744168377','04147458942','A','A','A','','A','','','','','','','','2011-05-03'),('15031097','V','Luis Felipe','Márquez Briceño','M','1981-03-09','Mérida','felixpe09@hotmail.com','TSU INFORMATICA','ASOPRIETO CALLE 4A #242, EJIDO','02742217980','04266736316','','','','','','','','','','','','','2011-04-25'),('15621190','V','Yohana','Pérez de Valero','F','1981-09-06','Merida edo Merida','yoha19perez@hotmail.com','lic Contaduria Publica','Sector bella vista calle lara casa Nº54 ejido','02742210478','04163791718','A','A','A','A','A','A','A','A','A','','','','2011-04-25'),('16657924','V','María Alejandra ','Pérez','F','1985-02-23','Merida','MARIAP-23@hotmail.com','Bachiller','Calle el porvenir N° 18','','04165780214','A','A','A','','A','','','','','','','','2011-05-03'),('16933715','V','Alejandro António','Sayago Gonzáles ','M','1984-12-19','Merida edo Merida','alejandro_s1@hotmail.com','Abogado','AV 16 de Septiembre casa nro 1-12\r\nSan Jose de Obrero  zona postal','','04147086961','A','A','A','','A','','','','','','','','2011-04-25'),('17130852','V','Irene','Guillén','F','2011-05-04','','','','','','','','','','','','','','','','','','','2011-08-02'),('4264264','V','Carlos Julio ','Puentes Uzcátegui ','M','1955-12-04','La Azulita Edo Merida ','puentesuzca@hotmail.com','Arquitecto','Calle Ppal Boticario, 5-54. Ejido','02744172160','04168002030','A','A','A','A','A','A','A','A','A','','A','','2011-04-25'),('6343267','V','Luz Marina','Salcedo Pernía','F','1970-08-27','caracas','luzmarsal@hotmail.com','lic contaduria  publica','URB Hacienda zumba calle 4-B Araguaney 298','02745118815','04166657051','A','A','A','A','A','A','A','A','A','','A','A','2011-05-03'),('8019652','V','Oswaldo Ramón ','Ramos','M','1962-02-06','Barquisimeto Edo Lara','oswalramra@hotmail.com','analistas y programador desistema de computacionn ','urb la mata serrania casa clb , torre13-0-c ','02745111050','04166742323','A','A','A','A','A','A','A','A','','','','','2011-05-03'),('8023210','V','José Andrés','Briceño Valero','M','1961-11-10','Merida edo Merida','josemeridave@hotmail.com','Abogado',' URB J.J OSUMA RODRIGUEZ VEREDA 17 .Nº07 MERIDA','02742714008','04162749656','A','A','A','A','A','','','','','','','','2011-04-25'),('8029735','V','Nora Coromoto','Cerrada','F','1961-04-19','Merida','Secrenor@hotmail.com','Secretaria ','Manzano alto via la panamericana casa n°031','02748085555','04169760118','A','A','A','A','','A','A','A','A','','','','2011-05-03'),('8036119','V','Elda Soraya','Hill Dávila','F','1965-10-29','Merida edo Merida','servijdcos@hotmail.com','Abogada','chamita calle las Acasias Nº1-278 Merida ','02742665325','04165778653','A','A','A','A','A','A','A','A','A','','A','','2011-04-25'),('9028505','V','Ramón António','Fernández Rojas','M','1962-09-01','El Vigía ','ramonactividad@hotmail.com','Ing. Mecánico','Palmo Calle 5 de Julio casa Nº 0-13','02742212574','04166265746','A','A','A','A','A','A','A','A','A','','','','2011-04-25');
/*!40000 ALTER TABLE `personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos_compras`
--

DROP TABLE IF EXISTS `productos_compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos_compras` (
  `cod_pro_com` int(11) NOT NULL AUTO_INCREMENT,
  `cod_com` int(11) NOT NULL,
  `cnt_pro_com` int(4) NOT NULL,
  `uni_pro_com` int(4) NOT NULL,
  `con_pro_com` varchar(255) NOT NULL,
  `pun_pro_com` double(9,2) NOT NULL,
  `exc_pro_com` varchar(2) DEFAULT NULL,
  KEY `id` (`cod_pro_com`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos_compras`
--

LOCK TABLES `productos_compras` WRITE;
/*!40000 ALTER TABLE `productos_compras` DISABLE KEYS */;
INSERT INTO `productos_compras` VALUES (1,2,1,1,'Sirvase prestar el servicio de Condominio del local Nº 54 correspondiente al mes de Julio',430.74,'SI');
/*!40000 ALTER TABLE `productos_compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos_egresos`
--

DROP TABLE IF EXISTS `productos_egresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos_egresos` (
  `cod_pro_egr` int(11) NOT NULL AUTO_INCREMENT,
  `cod_egr` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `isrl_pro_egr` varchar(1) DEFAULT NULL,
  `mon_pro_egr` double(9,2) NOT NULL,
  PRIMARY KEY (`cod_pro_egr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos_egresos`
--

LOCK TABLES `productos_egresos` WRITE;
/*!40000 ALTER TABLE `productos_egresos` DISABLE KEYS */;
INSERT INTO `productos_egresos` VALUES (1,3,54,'',430.74);
/*!40000 ALTER TABLE `productos_egresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prog_mov_pagos`
--

DROP TABLE IF EXISTS `prog_mov_pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prog_mov_pagos` (
  `cod_pag` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `con_pag` varchar(150) NOT NULL,
  `fch_pag` date NOT NULL,
  `des_car` varchar(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_vac` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prog_mov_pagos`
--

LOCK TABLES `prog_mov_pagos` WRITE;
/*!40000 ALTER TABLE `prog_mov_pagos` DISABLE KEYS */;
/*!40000 ALTER TABLE `prog_mov_pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prog_movimientos`
--

DROP TABLE IF EXISTS `prog_movimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prog_movimientos` (
  `cod_mov` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `accion` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  `fch_asg` date NOT NULL,
  `des_car` varchar(1) NOT NULL,
  `fch_vac` date NOT NULL,
  PRIMARY KEY (`cod_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prog_movimientos`
--

LOCK TABLES `prog_movimientos` WRITE;
/*!40000 ALTER TABLE `prog_movimientos` DISABLE KEYS */;
/*!40000 ALTER TABLE `prog_movimientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programas`
--

DROP TABLE IF EXISTS `programas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programas` (
  `cod_pro` varchar(5) NOT NULL COMMENT 'codigo del Programa',
  `nom_pro` varchar(150) NOT NULL COMMENT 'nombre del Programa',
  PRIMARY KEY (`cod_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programas`
--

LOCK TABLES `programas` WRITE;
/*!40000 ALTER TABLE `programas` DISABLE KEYS */;
INSERT INTO `programas` VALUES ('1','PROGRAMA 01');
/*!40000 ALTER TABLE `programas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `cod_pro` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `fch_pro` date NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `nom_pro` varchar(50) NOT NULL,
  `dir_pro` varchar(255) NOT NULL,
  `act_pro` varchar(50) NOT NULL,
  `cap_pro` double(9,2) NOT NULL,
  `cas_pro` double(9,2) NOT NULL,
  `tel_pro` varchar(11) NOT NULL,
  `fax_pro` varchar(11) NOT NULL,
  `per_pro` varchar(1) NOT NULL,
  `ofi_reg_pro` varchar(50) NOT NULL,
  `num_reg_pro` int(5) NOT NULL,
  `tom_reg_pro` varchar(25) NOT NULL,
  `fol_reg_pro` varchar(4) NOT NULL,
  `fch_reg_pro` date NOT NULL,
  `num_mod_pro` int(5) NOT NULL,
  `tom_mod_pro` varchar(25) NOT NULL,
  `fol_mod_pro` varchar(4) NOT NULL,
  `fch_mod_pro` date NOT NULL,
  `pat_pro` varchar(25) NOT NULL,
  `ivss_pro` varchar(25) NOT NULL,
  `ince_pro` varchar(25) NOT NULL,
  `nom_rep_pro` varchar(50) NOT NULL,
  `ape_rep_pro` varchar(50) NOT NULL,
  `ced_rep_pro` varchar(8) NOT NULL,
  `dir_rep_pro` varchar(255) NOT NULL,
  `tel_rep_pro` varchar(11) NOT NULL,
  `car_rep_pro` varchar(50) NOT NULL,
  PRIMARY KEY (`cod_pro`),
  UNIQUE KEY `rif_pro` (`rif_pro`),
  UNIQUE KEY `nom_pro` (`nom_pro`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 COMMENT='Datos de los Proveedores';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES (0001,'2011-01-01','J298994940','Asociacion Cooperativa Manely','AV. Principal casa N°1-1Sector Zumba','comerciante',2500.00,2500.00,'02742710374','','J','Registro Mercantil Primero del edo Merida',29,'Noveno','241','2010-04-28',0,'','','0000-00-00','','01931016','','Henry de Jesus','Angulo','08019717','Sector Zumba Casa N°1-1 AV Principal','04161770759','Presidente'),(0002,'2011-01-01','J294469752','Asociacion cooperativa coofiandina','Calle 16 Entre Av 5y6 N°5 - 72','Compra y venta de reparacion de equipos de oficina',50.00,50.00,'02742528046','02742528046','J','Registro Mercantil Primero del edo Merida',41,'26','305','2007-05-15',0,'','','0000-00-00','','','','Hugo Ali ','Sulbaran ','08020874','calle 16 entre Av 5y6 N°572','04147455104','Cordinador General '),(0003,'2011-01-01','J296861234','supermercado plaza montalban ','Av Bolivar local N°196 -17 sector montalban','comerciante',300.00,300.00,'02742214682','','J','Registro Mercantil Primero del edo Merida',379,'16','16','2008-10-23',0,'','','0000-00-00','','','','Yhon Isaac','Areas Barrios','08025049','Av Bolivar Sector Montalban','02742214682','GERENTE PERSONAL'),(0004,'2011-01-01','J300180174','SODEXHOPASS VENEZUELA','con Av los chaguaramos torre corp banca piso 16 la castellar ','Emision, comercializacion',2250000.00,2250000.00,'02122065625','02122065620','J','Registro mercantil segundo  del esdo miranda',75,'154-A','5','2007-07-07',75,'154-A','5','0000-00-00','','','','nicolas ','vaga','06972690','','','gerente general'),(0005,'2011-01-01','V123536335','Distrubuiora Alexwenp','Av Bolivar N°231 local 02 ejido','Compra y venta de papeleria ANT de oficios  ',6000.00,6000.00,'02742210571','','J','registro mercantil primero del estado merida ',91,'5','5','2007-08-16',0,'','','0000-00-00','','','','Amilcar Alexander ','Nieto Contreras ','12353633','Av Bolivar N°231 Local de ejido','04165222227','propietario'),(0006,'2011-01-01','J298778377','Asociacion cooperativa ofimatica','oficina matriz Av 25 de noviembre urbanizacion el trapiche bloque 4 edif 1 apto 3-3','Comerciante',0.00,0.00,'02744174697','','J','Registro Publico del Municipio Campo Elias',21,'2','132','2010-05-26',0,'','','0000-00-00','','','','Jose Felix','Rivas Gomez','13524339','URB El trapiche bloque 4 edf 1 pisos 3','04247577644','gerente'),(0007,'2011-01-01','V037653213','Prove Oficina Sosa','Av Don tulio febres cordero entre calle 29y30merida ','comerciante',10.00,10.00,'02742529551','','J','Megistro mercantil primero del estado merida',64,'5','5','2007-06-26',64,'5','5','0000-00-00','','','','Alexis','Sosa','03765321','Av don tulio febres cordero entre calle 29y30 merida','04147742018','representante legal'),(0008,'2011-01-01','V080232108','Jose Andres Briceño','urb. JJ osuna Rodriguez Vereda 17 N°07 Merida','contralor',0.00,0.00,'02742714008','','P','',0,'0','0','2011-01-01',0,'','','0000-00-00','','','','Jose Andres','Briceño','08023210','urb jj Osuna rodriguez','04162749656','contralor'),(0009,'2011-01-01','V080297358','Nora Cerrada','EL Manzano Alto Via Jaji Panamericana','Secretaria',2.00,1.00,'04267032172','','P','S/N',0,'0','009','2011-01-01',4,'S/N','0001','2011-01-01','0','0','0','Nora Coromoto','Cerrada','08029735','El Manzano Alto via jaji panamericana','04167032172','Secretaria'),(0010,'2011-01-01','J029506233','Cadafe','Cento Electronico Nacional ','comerciante',0.00,0.00,'02432722711','','J','N/R',0,'0009','0009','2011-01-01',0,'0','0','2011-01-01','0','0','0','S/R','S/R','00000000','','','S/R'),(0011,'2011-01-01','J090370196','Sovenpfa','Av 4 calle 21edif Don Atilio Nivel Mezanina','Comerciante',0.00,0.00,'02742510444','','J','N/R',1,'0000005','980','2011-01-01',0,'0','0001','2011-01-01','','','','S/R','N/R','88888888','Av 4 con Calle 21 edif Don Atilio Nivel Mezanina','02742510444','N/R'),(0012,'2011-01-01','J001241345','CANTV','Final Av libertador edif CNT Caracas','Cervicio Telefonoco',1.00,1.00,'02125001111','','J','S/N',8,'0000005','5436','2011-01-01',789,'S/N','070','2011-01-01','0','0','0','CANTV','J','01241345','Final Av Libertedor edif CNT Caracas','02125001111','Propietario'),(0013,'2011-01-01','V127803591','Multiservicios del centro ','Av Bolivar Casa N° 162 Ejido edo Merida','Puesto del Estacionamiento',1.00,1.00,'02744168266','','J','S/N',999,'0','465','2011-01-01',999,'S/N','465','2011-01-01','0','0','0','Ramon A ','Ruiz Fernandez','12780359','Av Bolivar Casa N°162 Ejido edo Merida','02744168266','Representante Legal'),(0014,'2011-01-01','J000389233','Seguros Caracas','Av francisco de Miranda CC el Parque Torre Seguros Caracas Nivel C-4 los Palos Grandes Caracas ','Seguros Caracas',1.00,1.00,'02122099556','','J','S/N',0,'0','0','2011-01-01',9,'S/N','008','2011-01-01','0','0','0','Seguros ','Caracas','00389233','Av Francisco de Miranda CC el ParqueTorre Seguros Caracas Nivel c -4 Con Palos Grandes Caracas','02122099556','Representante Legal'),(0015,'2011-01-01','J000268401','Seguros Nuevo Mundo','Av luis roche con carrera trasversal edif nuevo mundo Sector Altamira','Seguros Nuevo Mundo',1.00,1.00,'02122011111','02122011157','J','S/N',8,'0','0','2011-01-01',8,'S/N','0','2011-01-01','0','0','0','Seguros','nuevo mundo','00026840','Av Luis Rochec/3 Torre Nuevo Mundo Sector Altamira','02122011111','Representante Legal'),(0016,'2011-01-01','J070017376','Seguros los Andes','Av las Pilas, urb . Santa Ines edif Seguros los Andes','Seguros lo Andes',1.00,1.00,'02763402611','','J','S/N',9,'0','0','2011-01-01',8,'S/N','008','2011-01-01','0','0','0','Seguros',' Andes','70017376','Av las Pilas urb Santa Ines edif Seguros los Andes','02763402611','Representante Legal'),(0017,'2011-01-01','J311801596','Condominio  Centro Comercial Centenario ','Av Centenario. Centro Comercial Centenrio Nivel PR Oficina Administrador','Condomineo',1.00,1.00,'02744158866','','J','S/N',6,'0','0','2011-01-01',6,'0','0','2011-01-01','0','0','0','condomineo','centenario','31180159','Av centenario . centro comercial ','02744158866','administrador'),(0018,'2011-01-01','J305877432','Capreamce','Ejido edo Merida','Caja De Ahorro',1.00,1.00,'02742214982','','J','S/N',9,'0','0','2011-01-01',9,'S/N','0006','2011-01-01','0','0','0','Adonaida ','peña ','11460606','ejido edo merida','02742214982','Representante Legal'),(0019,'2011-01-01','G200061944','Aguas de Ejido','Av Centenario .Nucleo Norte ,Nivel 1 Local 62','Servicio de la Comunidad',1.00,1.00,'02742215774','','J','S/N',8,'0','0','2011-01-01',8,'S/N','007','2011-01-01','0','0','0','AGUAS','EJIDO','20006194','Av centenario . centro comercial centenario nucleo norte nivel 1 local 62','02742215774','Representante Legal'),(0020,'2011-01-01','J299956066','Asociacion Cooperativa  andimed rl','Edif el Ramiral Piso Planta Baja Sagrado Calle 26','Comercio, Compras, Ventas, Y Exportacion',1.00,1.00,'02742526096','','J','publico d el municipio libertedor estado merida',38,'22','222','2010-10-21',38,'22','222','2010-10-21','','','','Aurimar Aurora ','Angulo Escalante','10717555','edif el Ramiral Piso Planta Baja Sagrado calle 26','04166740194','Representante Legal'),(0021,'2011-01-03','G200000031','Tesoro Nacional','Caracas','Gubernamental',0.00,0.00,'02120000000','','J','0',0,'','','2011-01-03',0,'','','0000-00-00','','','','Tesoro Nacional','Tesoro Nacional','00000000','','','Tesoro Nacional');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secciones`
--

DROP TABLE IF EXISTS `secciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secciones` (
  `cod_sec` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo de la seccion',
  `nom_sec` varchar(20) NOT NULL COMMENT 'nombre de la seccion',
  `des_sec` varchar(255) NOT NULL COMMENT 'descripcion de la seccion',
  `dir_sec` varchar(50) NOT NULL COMMENT 'direccion para apuntar a la seccion',
  `tar_sec` varchar(15) NOT NULL COMMENT 'target de apertura de la seccion',
  `ord_sec` double(9,2) NOT NULL COMMENT 'orden de aparicion en el menu',
  PRIMARY KEY (`cod_sec`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1 COMMENT='Secciones Disponibles para los Usuarios';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secciones`
--

LOCK TABLES `secciones` WRITE;
/*!40000 ALTER TABLE `secciones` DISABLE KEYS */;
INSERT INTO `secciones` VALUES (1,'Personal','Datos del Personal adscrito a: ','personal.php','contenido',7.00),(2,'Dependencias','Dependencias adcritas a:','dependencias.php','contenido',2.00),(3,'Cargos','Cargos adscritos a:','cargos.php','contenido',4.00),(4,'Nomina','Nomina adscrita a:','nomina.php','contenido',12.00),(5,'Incidencias','Incidencias adcritas a:','incidencias.php','contenido',13.00),(6,'Descuentos','Descuentos del Personal adscrito a:','descuentos.php','contenido',6.00),(7,'Concesiones','Concesiones del Personal adscrito a:','concesiones.php','contenido',5.00),(8,'Sueldos','Tabla de Sueldos','sueldos.php','contenido',3.00),(9,'Oficios','Oficios enviados por:','oficios_enviados.php','contenido',15.00),(10,'Compras/Servicios','Módulo de compras de: ','compras.php','contenido',14.00),(11,'Actividades','Actividades administrativas de:','actividades.php','contenido',1.00),(12,'Pre-Nomina','Prenomina Adscrita a: ','nominapre.php','contenido',11.00),(13,'Cesta Ticket','Reporte de Cesta Ticket del personal adscrito a:','cesta_ticket.php','contenido',10.00),(14,'Inasistencias','Registro de las inasistencias del personal adscrito a:','inasistencias.php','contenido',8.00),(15,'Feriados','Registro de días feriados para','feriados.php','contenido',9.00),(16,'Proveedores','Registro de Proveedores registrados a ','proveedores.php','contenido',13.50),(17,'Control de Bienes','Sistema de Control de Bienes','bienes/index.php','contenido',16.00),(18,'Valores','Valores utilizados para los cáculos','valores.php','contenido',0.00);
/*!40000 ALTER TABLE `secciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sueldos`
--

DROP TABLE IF EXISTS `sueldos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sueldos` (
  `cod_sue` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo',
  PRIMARY KEY (`cod_sue`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sueldos`
--

LOCK TABLES `sueldos` WRITE;
/*!40000 ALTER TABLE `sueldos` DISABLE KEYS */;
INSERT INTO `sueldos` VALUES (1,6119.45,'Contralor'),(2,2251.70,'Auditor II'),(3,2588.65,'Auditor  II'),(4,2100.00,'Inspector de Obras II'),(5,1658.42,'Secretaria Ejecutiva'),(6,3231.07,'Administrador(a)'),(7,2937.34,'Jefe Sala Técnica'),(8,2157.40,'Registrador de Bienes'),(9,2937.34,'Jefe de Servicio Jurídico'),(10,1548.22,'Asistente de Control Fiscal'),(11,2063.60,'Auditor I'),(12,1690.00,'Auxiliar de Auditoría'),(13,1690.00,'Técnico Inspector de Obras Civiles'),(14,1223.89,'Aseadora'),(15,1690.00,'Operador de Equipos de Computación'),(16,1800.00,'Abogado'),(17,1407.47,'Auxiliar Administrativo');
/*!40000 ALTER TABLE `sueldos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sueldos_ant`
--

DROP TABLE IF EXISTS `sueldos_ant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sueldos_ant` (
  `cod_sue` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo',
  PRIMARY KEY (`cod_sue`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sueldos_ant`
--

LOCK TABLES `sueldos_ant` WRITE;
/*!40000 ALTER TABLE `sueldos_ant` DISABLE KEYS */;
INSERT INTO `sueldos_ant` VALUES (1,6119.45,'Contralor'),(2,2251.70,'Auditor II'),(3,2588.65,'Auditor  II'),(4,2100.00,'Inspector de Obras II'),(5,1658.42,'Secretaria Ejecutiva'),(6,3231.07,'Administrador(a)'),(7,2937.34,'Jefe Sala Técnica'),(8,2157.40,'Registrador de Bienes'),(9,2937.34,'Jefe de Servicio Jurídico'),(10,1548.22,'Asistente de Control Fiscal'),(11,2063.60,'Auditor I'),(12,1690.00,'Auxiliar de Auditoría'),(13,1690.00,'Técnico Inspector de Obras Civiles'),(14,1223.89,'Aseadora'),(15,1690.00,'Operador de Equipos de Computación'),(16,1800.00,'Abogado');
/*!40000 ALTER TABLE `sueldos_ant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_cargos`
--

DROP TABLE IF EXISTS `tipos_cargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_cargos` (
  `cod_tcar` int(11) NOT NULL AUTO_INCREMENT,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`cod_tcar`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_cargos`
--

LOCK TABLES `tipos_cargos` WRITE;
/*!40000 ALTER TABLE `tipos_cargos` DISABLE KEYS */;
INSERT INTO `tipos_cargos` VALUES (1,'Fijo Jefe','FJ'),(2,'Fijo','F'),(3,'Contratado','C');
/*!40000 ALTER TABLE `tipos_cargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `cod_usr` varchar(12) NOT NULL COMMENT 'codigo del usuario',
  `nom_usr` varchar(50) NOT NULL COMMENT 'nombre del usuario',
  `ape_usr` varchar(50) NOT NULL COMMENT 'apellidos del usuario',
  `freg_usr` date NOT NULL COMMENT 'fecha de registro del usuario',
  `log_usr` varchar(15) NOT NULL COMMENT 'login del usuario',
  `pas_usr` varchar(15) NOT NULL COMMENT 'password del usuario',
  `sts_usr` varchar(1) NOT NULL DEFAULT 'A' COMMENT 'Estado en el que se encuentra el Usuario',
  `int_usr` int(1) NOT NULL DEFAULT '0' COMMENT 'Intentos fallidos de inicio de sesi�n',
  `fcam_usr` date NOT NULL COMMENT 'fecha de cambio del usuario',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  PRIMARY KEY (`cod_usr`),
  KEY `cod_grp` (`cod_grp`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`cod_grp`) REFERENCES `grupos` (`cod_grp`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Usuarios del Sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('10109577','María Isabel','Rivas de Osuna','2011-05-04','isabelr','adt123','A',0,'2011-05-04',3),('10715628','Yoly Josefina','Avendaño Meza','2011-05-04','yolya','adt123','A',0,'2011-05-04',3),('11960592','Jesus Ramón','Silva Osuna','2011-05-04','jesuss','slt1975','A',0,'2011-05-04',3),('12345678','Administrador','Administrador','2011-02-16','admin','123456','A',0,'2011-06-15',1),('12346028','José de Jesús','Corredor Alarcón','2011-05-04','josec','slt1975','A',0,'2011-05-04',3),('14304657','Leidy Josefina','Pereira Ochoa','2011-05-04','leidyp','audt123','A',0,'2011-05-04',3),('15031097','Luis Felipe','Márquez Briceño','2011-01-01','felix','nueva','A',0,'2011-07-18',2),('15621190','Yohana','Pérez de Valero','2011-05-04','yohanap','adt123','A',0,'2011-05-04',3),('16933715','Alejandro Antonio','Sayago González','2011-05-04','alejandros','servju','A',0,'2011-05-04',3),('19422787','Luzmar','Contreras S','2011-07-08','luzmar','123456','A',0,'2011-08-08',5),('21184968','Yaquelin','Rivas Vera','2011-04-25','yaquelin','yaquerivas','A',0,'2011-05-06',4),('4264264','Carlos Julio','Puentes Uzcátegui','2011-05-04','carlosp','slt1975','A',0,'2011-05-04',3),('6343267','Luz Marina ','Salcedo ','2011-01-01','luzmarina','luzmari','A',0,'2011-07-18',2),('8019652','Oswaldo Ramón','ramos','2011-05-04','oswaldor','ctrlbn','A',0,'2011-05-04',3),('8023210','José Andres','Briceño Valero','2011-04-29','josemeridave','jose_1961','A',0,'2011-04-29',1),('8029735','Nora Coromoto','Cerrada','2011-05-04','norac','secejec','A',0,'2011-05-04',3),('8036119','Elda Soraya','Hill Dávila','2011-05-04','sorayah','servju','A',0,'2011-05-04',3),('9028505','Ramón Antonio','Fernández Rojas','2011-05-04','ramonf','ctrlbn','A',0,'2011-05-04',3);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valores`
--

DROP TABLE IF EXISTS `valores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valores` (
  `cod_val` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del Valor',
  `des_val` varchar(25) NOT NULL COMMENT 'Descripcion del Valor',
  `val_val` double(9,3) NOT NULL COMMENT 'Valor del Valor',
  `con_val` varchar(100) NOT NULL COMMENT 'Concepto del Valor',
  PRIMARY KEY (`cod_val`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valores`
--

LOCK TABLES `valores` WRITE;
/*!40000 ALTER TABLE `valores` DISABLE KEYS */;
INSERT INTO `valores` VALUES (1,'BV',40.000,'Días de Bono Vacacional para personal Contratado según LOT'),(2,'AG',90.000,'Días de Aguinaldos'),(3,'UT',76.000,'Valor de la Unidad Tributaria'),(4,'SSO',0.040,'Porcentaje de Retención Seguro Social Obligatorio'),(5,'LPH',0.010,'Porcentaje de Retención Fondo Ahorro Obligatorio para Vivienda'),(6,'SPF',0.005,'Porcentaje de Retención Pérdida Involuntaria de Empleo (PIE)'),(7,'CAH',0.080,'Porcentaje de Retención Caja de Ahorros (CAPREAMCE)'),(8,'SFU',40.000,'Monto Seguro Funerario (SOVENPFA)'),(9,'SM',1407.470,'Monto de Sueldo Mínimo'),(10,'SMMSSO',5.000,'Cantidad de Sueldos Máximos para el SSO'),(12,'APSSO',0.090,'Porcentaje de Aporte Seguro Social Obligatorio'),(13,'APSPF',0.020,'Porcentaje de Aporte Pérdida Involuntaria de Empleo (PIE)'),(14,'APCAH',0.080,'Porcentaje de Aporte Caja de Ahorros (CAPREAMCE)'),(15,'PRMHOG',2.500,'Cantidad de Unidades Tributarias Prima por Hogar'),(16,'PRMHIJ',0.500,'Cantidad de Unidades Tributarias Prima por Hijos'),(17,'PRMANT1',1.000,'Cantidad de Unidades Tributarias Prima por Antiguedad 1er Quinquenio'),(18,'PRMPRO1',1.500,'Cantidad de Unidades Tributarias Prima por Profesionalización 1er Tipo'),(19,'PRMJER',300.000,'Monto Prima Jerarquía'),(20,'PRMHJE',1.000,'Cantidad de Unidades Tributarias Prima por Hijos Excepcionales'),(21,'PRMOTR1',2.500,'Cantidad de Unidades Tributarias Otra Prima 1er Tipo'),(22,'APLPH',0.100,'Porcentaje de Aporte Fondo Ahorro Obligatorio para Vivienda'),(23,'PRMANT2',1.500,'Cantidad de Unidades Tributarias Prima por Antiguedad 2do Quinquenio'),(24,'PRMANT3',2.000,'Cantidad de Unidades Tributarias Prima por Antiguedad 3er Quinquenio'),(25,'PRMANT4',2.500,'Cantidad de Unidades Tributarias Prima por Antiguedad 4to Quinquenio'),(26,'PRMANT5',3.000,'Cantidad de Unidades Tributarias Prima por Antiguedad 5to Quinquenio'),(27,'VACPAG1',45.000,'Días de Bono Vacacional para personal fijo 1er Quinquenio'),(28,'VACPAG2',50.000,'Días de Bono Vacacional para personal fijo 2do Quinquenio'),(29,'VACPAG3',55.000,'Días de Bono Vacacional para personal fijo 3er Quinquenio'),(30,'VACPAG4',60.000,'Días de Bono Vacacional para personal fijo 4to Quinquenio'),(31,'VACPAG5',65.000,'Días de Bono Vacacional para personal fijo 5to Quinquenio'),(32,'VACDIF1',20.000,'Días de disfrute de Vacaciones para personal fijo 1er Quinquenio'),(33,'VACDIF2',23.000,'Días de disfrute de Vacaciones para personal fijo 2do Quinquenio'),(34,'VACDIF3',25.000,'Días de disfrute de Vacaciones para personal fijo 3er Quinquenio'),(35,'VACDIF4',28.000,'Días de disfrute de Vacaciones para personal fijo 4to Quinquenio'),(36,'VACDIF5',30.000,'Días de disfrute de Vacaciones para personal fijo 5to Quinquenio'),(38,'DESPIO',0.000,'Porcentaje de Monte Pio'),(39,'PRMPRO2',2.000,'Cantidad de Unidades Tributarias Prima por Profesionalización 2do Tipo'),(40,'PRMOTR2',3.000,'Cantidad de Unidades Tributarias Otra Prima 2do Tipo');
/*!40000 ALTER TABLE `valores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vista_asignaciones_per`
--

DROP TABLE IF EXISTS `vista_asignaciones_per`;
/*!50001 DROP VIEW IF EXISTS `vista_asignaciones_per`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_asignaciones_per` (
  `cod_cnp` int(11),
  `ced_per` varchar(12),
  `nom_con` varchar(50),
  `con_cnp` varchar(100),
  `ncuo_cnp` int(3),
  `ncp_cnp` int(3),
  `nom_car` varchar(100)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_cargos_per`
--

DROP TABLE IF EXISTS `vista_cargos_per`;
/*!50001 DROP VIEW IF EXISTS `vista_cargos_per`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_cargos_per` (
  `ced_per` varchar(12),
  `cod_car` int(11),
  `cod_dep` varchar(5),
  `num_car` int(3) unsigned zerofill,
  `nom_car` varchar(100),
  `fch_asg` date,
  `fch_vac` date,
  `des_car` varchar(1),
  `nom_per` varchar(50),
  `ape_per` varchar(50),
  `nom_tcar` varchar(50),
  `mon_sue` double(9,2),
  `des_sue` varchar(50),
  `mon_sue_ant` double(9,2)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_deducciones_per`
--

DROP TABLE IF EXISTS `vista_deducciones_per`;
/*!50001 DROP VIEW IF EXISTS `vista_deducciones_per`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_deducciones_per` (
  `cod_dsp` int(11),
  `ced_per` varchar(12),
  `nom_des` varchar(50),
  `con_dsp` varchar(100),
  `ncuo_dsp` int(3),
  `ncp_dsp` int(3),
  `nom_car` varchar(100)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_movimientos`
--

DROP TABLE IF EXISTS `vista_movimientos`;
/*!50001 DROP VIEW IF EXISTS `vista_movimientos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_movimientos` (
  `cod_mov` int(11),
  `ced_per` varchar(12),
  `cod_car` int(11),
  `accion` int(11),
  `estado` int(1),
  `fch_asg` date,
  `des_car` varchar(1),
  `cod_dep` varchar(5),
  `num_car` int(3) unsigned zerofill,
  `nom_car` varchar(100),
  `cod_sue` int(2),
  `cod_tcar` int(11),
  `des_sue` varchar(50),
  `mon_sue` double(9,2),
  `nom_tcar` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_nomin_tot_sum`
--

DROP TABLE IF EXISTS `vista_nomin_tot_sum`;
/*!50001 DROP VIEW IF EXISTS `vista_nomin_tot_sum`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_nomin_tot_sum` (
  `ano_nom` int(4),
  `mes_nom` int(2),
  `por_nom` int(1),
  `cod_tcar` int(11),
  `abr_tcar` varchar(2),
  `cod_dep` varchar(5),
  `nom_tcar` varchar(50),
  `tot_sue` double(19,2),
  `tot_lph` double(19,2),
  `tot_spf` double(19,2),
  `tot_sso` double(19,2),
  `tot_cah` double(19,2),
  `tot_sfu` double(19,2)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_nominapre_proc`
--

DROP TABLE IF EXISTS `vista_nominapre_proc`;
/*!50001 DROP VIEW IF EXISTS `vista_nominapre_proc`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_nominapre_proc` (
  `ano_nom` int(4),
  `mes_nom` int(2),
  `por_nom` int(1)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_nominas_pagadas`
--

DROP TABLE IF EXISTS `vista_nominas_pagadas`;
/*!50001 DROP VIEW IF EXISTS `vista_nominas_pagadas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_nominas_pagadas` (
  `ano_nom` int(4),
  `mes_nom` int(2),
  `por_nom` int(1)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_nominpre_tot_sum`
--

DROP TABLE IF EXISTS `vista_nominpre_tot_sum`;
/*!50001 DROP VIEW IF EXISTS `vista_nominpre_tot_sum`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_nominpre_tot_sum` (
  `ano_nom` int(4),
  `mes_nom` int(2),
  `por_nom` int(1),
  `cod_tcar` int(11),
  `abr_tcar` varchar(2),
  `cod_dep` varchar(5),
  `nom_tcar` varchar(50),
  `tot_sue` double(19,2),
  `tot_lph` double(19,2),
  `tot_spf` double(19,2),
  `tot_sso` double(19,2),
  `tot_cah` double(19,2),
  `tot_sfu` double(19,2)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_personal`
--

DROP TABLE IF EXISTS `vista_personal`;
/*!50001 DROP VIEW IF EXISTS `vista_personal`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_personal` (
  `ced_per` varchar(12),
  `nombre` varchar(101),
  `nac_per` varchar(1)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_salida_sincargo`
--

DROP TABLE IF EXISTS `vista_salida_sincargo`;
/*!50001 DROP VIEW IF EXISTS `vista_salida_sincargo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_salida_sincargo` (
  `cod_pag` int(11),
  `mon_pag` double(9,2),
  `ced_per` varchar(12),
  `con_pag` varchar(150),
  `des_car` varchar(1),
  `fch_pag` date,
  `cod_dep` varchar(5),
  `nom_dep` varchar(150),
  `cod_car` int(11),
  `nom_car` varchar(100),
  `fch_vac` date,
  `cod_tcar` int(11),
  `nom_tcar` varchar(50),
  `abr_tcar` varchar(2)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `vista_asignaciones_per`
--

/*!50001 DROP TABLE IF EXISTS `vista_asignaciones_per`*/;
/*!50001 DROP VIEW IF EXISTS `vista_asignaciones_per`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_asignaciones_per` AS select `asg`.`cod_cnp` AS `cod_cnp`,`asg`.`ced_per` AS `ced_per`,`cn`.`nom_con` AS `nom_con`,`asg`.`con_cnp` AS `con_cnp`,`asg`.`ncuo_cnp` AS `ncuo_cnp`,`asg`.`ncp_cnp` AS `ncp_cnp`,`c`.`nom_car` AS `nom_car` from ((`asignaciones` `asg` join `concesiones` `cn`) join `cargos` `c`) where ((`asg`.`cod_con` = `cn`.`cod_con`) and (`c`.`cod_car` = `asg`.`cod_car`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_cargos_per`
--

/*!50001 DROP TABLE IF EXISTS `vista_cargos_per`*/;
/*!50001 DROP VIEW IF EXISTS `vista_cargos_per`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_cargos_per` AS select `c`.`ced_per` AS `ced_per`,`c`.`cod_car` AS `cod_car`,`c`.`cod_dep` AS `cod_dep`,`c`.`num_car` AS `num_car`,`c`.`nom_car` AS `nom_car`,`c`.`fch_asg` AS `fch_asg`,`c`.`fch_vac` AS `fch_vac`,`c`.`des_car` AS `des_car`,`p`.`nom_per` AS `nom_per`,`p`.`ape_per` AS `ape_per`,`t`.`nom_tcar` AS `nom_tcar`,`s`.`mon_sue` AS `mon_sue`,`s`.`des_sue` AS `des_sue`,`sa`.`mon_sue` AS `mon_sue_ant` from ((((`cargos` `c` join `tipos_cargos` `t`) join `sueldos` `s`) join `sueldos_ant` `sa`) join `personal` `p`) where ((`t`.`cod_tcar` = `c`.`cod_tcar`) and (`s`.`cod_sue` = `c`.`cod_sue`) and (`sa`.`cod_sue` = `c`.`cod_sue`) and (`p`.`ced_per` = `c`.`ced_per`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_deducciones_per`
--

/*!50001 DROP TABLE IF EXISTS `vista_deducciones_per`*/;
/*!50001 DROP VIEW IF EXISTS `vista_deducciones_per`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_deducciones_per` AS select `dd`.`cod_dsp` AS `cod_dsp`,`dd`.`ced_per` AS `ced_per`,`ds`.`nom_des` AS `nom_des`,`dd`.`con_dsp` AS `con_dsp`,`dd`.`ncuo_dsp` AS `ncuo_dsp`,`dd`.`ncp_dsp` AS `ncp_dsp`,`c`.`nom_car` AS `nom_car` from ((`deducciones` `dd` join `descuentos` `ds`) join `cargos` `c`) where ((`dd`.`cod_des` = `ds`.`cod_des`) and (`c`.`cod_car` = `dd`.`cod_car`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_movimientos`
--

/*!50001 DROP TABLE IF EXISTS `vista_movimientos`*/;
/*!50001 DROP VIEW IF EXISTS `vista_movimientos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_movimientos` AS select `m`.`cod_mov` AS `cod_mov`,`m`.`ced_per` AS `ced_per`,`m`.`cod_car` AS `cod_car`,`m`.`accion` AS `accion`,`m`.`estado` AS `estado`,`m`.`fch_asg` AS `fch_asg`,`m`.`des_car` AS `des_car`,`c`.`cod_dep` AS `cod_dep`,`c`.`num_car` AS `num_car`,`c`.`nom_car` AS `nom_car`,`c`.`cod_sue` AS `cod_sue`,`c`.`cod_tcar` AS `cod_tcar`,`s`.`des_sue` AS `des_sue`,`s`.`mon_sue` AS `mon_sue`,`t`.`nom_tcar` AS `nom_tcar` from (((`prog_movimientos` `m` join `cargos` `c`) join `sueldos` `s`) join `tipos_cargos` `t`) where ((`c`.`cod_car` = `m`.`cod_car`) and (`s`.`cod_sue` = `c`.`cod_sue`) and (`t`.`cod_tcar` = `c`.`cod_tcar`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_nomin_tot_sum`
--

/*!50001 DROP TABLE IF EXISTS `vista_nomin_tot_sum`*/;
/*!50001 DROP VIEW IF EXISTS `vista_nomin_tot_sum`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_nomin_tot_sum` AS (select `np`.`ano_nom` AS `ano_nom`,`np`.`mes_nom` AS `mes_nom`,`np`.`por_nom` AS `por_nom`,`np`.`cod_tcar` AS `cod_tcar`,`np`.`abr_tcar` AS `abr_tcar`,`np`.`cod_dep` AS `cod_dep`,`tc`.`nom_tcar` AS `nom_tcar`,sum(`np`.`mon_sue`) AS `tot_sue`,sum(`np`.`lph_des`) AS `tot_lph`,sum(`np`.`spf_des`) AS `tot_spf`,sum(`np`.`sso_des`) AS `tot_sso`,sum(`np`.`cah_des`) AS `tot_cah`,sum(`np`.`sfu_des`) AS `tot_sfu` from (`nomina_pagar` `np` join `tipos_cargos` `tc`) where (`np`.`cod_tcar` = `tc`.`cod_tcar`) group by `np`.`ano_nom`,`np`.`mes_nom`,`np`.`por_nom`,`np`.`cod_tcar`,`np`.`cod_dep`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_nominapre_proc`
--

/*!50001 DROP TABLE IF EXISTS `vista_nominapre_proc`*/;
/*!50001 DROP VIEW IF EXISTS `vista_nominapre_proc`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_nominapre_proc` AS select `nominapre_pagar`.`ano_nom` AS `ano_nom`,`nominapre_pagar`.`mes_nom` AS `mes_nom`,`nominapre_pagar`.`por_nom` AS `por_nom` from `nominapre_pagar` group by `nominapre_pagar`.`ano_nom`,`nominapre_pagar`.`mes_nom`,`nominapre_pagar`.`por_nom` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_nominas_pagadas`
--

/*!50001 DROP TABLE IF EXISTS `vista_nominas_pagadas`*/;
/*!50001 DROP VIEW IF EXISTS `vista_nominas_pagadas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_nominas_pagadas` AS select `nomina_pagar`.`ano_nom` AS `ano_nom`,`nomina_pagar`.`mes_nom` AS `mes_nom`,`nomina_pagar`.`por_nom` AS `por_nom` from `nomina_pagar` group by `nomina_pagar`.`ano_nom`,`nomina_pagar`.`mes_nom`,`nomina_pagar`.`por_nom` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_nominpre_tot_sum`
--

/*!50001 DROP TABLE IF EXISTS `vista_nominpre_tot_sum`*/;
/*!50001 DROP VIEW IF EXISTS `vista_nominpre_tot_sum`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_nominpre_tot_sum` AS (select `np`.`ano_nom` AS `ano_nom`,`np`.`mes_nom` AS `mes_nom`,`np`.`por_nom` AS `por_nom`,`np`.`cod_tcar` AS `cod_tcar`,`np`.`abr_tcar` AS `abr_tcar`,`np`.`cod_dep` AS `cod_dep`,`tc`.`nom_tcar` AS `nom_tcar`,sum(`np`.`mon_sue`) AS `tot_sue`,sum(`np`.`lph_des`) AS `tot_lph`,sum(`np`.`spf_des`) AS `tot_spf`,sum(`np`.`sso_des`) AS `tot_sso`,sum(`np`.`cah_des`) AS `tot_cah`,sum(`np`.`sfu_des`) AS `tot_sfu` from (`nominapre_pagar` `np` join `tipos_cargos` `tc`) where (`np`.`cod_tcar` = `tc`.`cod_tcar`) group by `np`.`ano_nom`,`np`.`mes_nom`,`np`.`por_nom`,`np`.`cod_tcar`,`np`.`cod_dep`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_personal`
--

/*!50001 DROP TABLE IF EXISTS `vista_personal`*/;
/*!50001 DROP VIEW IF EXISTS `vista_personal`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_personal` AS select `personal`.`ced_per` AS `ced_per`,concat(`personal`.`nom_per`,_latin1' ',`personal`.`ape_per`) AS `nombre`,`personal`.`nac_per` AS `nac_per` from `personal` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_salida_sincargo`
--

/*!50001 DROP TABLE IF EXISTS `vista_salida_sincargo`*/;
/*!50001 DROP VIEW IF EXISTS `vista_salida_sincargo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_salida_sincargo` AS select `prog_mov_pagos`.`cod_pag` AS `cod_pag`,`prog_mov_pagos`.`mon_pag` AS `mon_pag`,`prog_mov_pagos`.`ced_per` AS `ced_per`,`prog_mov_pagos`.`con_pag` AS `con_pag`,`prog_mov_pagos`.`des_car` AS `des_car`,`prog_mov_pagos`.`fch_pag` AS `fch_pag`,`prog_mov_pagos`.`cod_dep` AS `cod_dep`,`prog_mov_pagos`.`nom_dep` AS `nom_dep`,`prog_mov_pagos`.`cod_car` AS `cod_car`,`prog_mov_pagos`.`nom_car` AS `nom_car`,`prog_mov_pagos`.`fch_vac` AS `fch_vac`,`prog_mov_pagos`.`cod_tcar` AS `cod_tcar`,`prog_mov_pagos`.`nom_tcar` AS `nom_tcar`,`prog_mov_pagos`.`abr_tcar` AS `abr_tcar` from (`prog_mov_pagos` left join `cargos` on((`cargos`.`ced_per` = `prog_mov_pagos`.`ced_per`))) where (not(`prog_mov_pagos`.`ced_per` in (select distinct `cargos`.`ced_per` AS `ced_per` from `cargos`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-08-11  9:23:42
