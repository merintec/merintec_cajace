<?php 
//////// Calendario Personalizado por Luis M�rquez (felix) 
//////// email: felixpe09@gmail.com
/// Requiere:	
///		$mes_calendario
///		$ano_calendario
///		$evento1  --> arreglo con d�as de los eventos tipo 1 (fechas)
///		$evento12 --> arreglo con d�as de los eventos tipo 1 (descripci�n)
///		$evento2  --> arreglo con d�as de los eventos tipo 2 (fechas)
///		$evento22 --> arreglo con d�as de los eventos tipo 2 (descripci�n)
$diasmes[0] = "Dom�ngo";
$diasmes[1] = "Lunes";
$diasmes[2] = "Martes";
$diasmes[3] = "Mi�rcoles";
$diasmes[4] = "Jueves";
$diasmes[5] = "Viernes";
$diasmes[6] = "S�bado";
/// Colores
if (!$color_calendario) { $color_calendario = "#000000"; }
if (!$color_calendario_back) { $color_calendario_back = "#EEEEEE";}
if (!$color_calendario_border) { $color_calendario_border = "#000000";}
if (!$color_nombre_dias) { $color_nombre_dias = ""; }
if (!$color_nombre_dias_back) { $color_nombre_dias_back = "";}
if (!$color_titulo) { $color_titulo = "#FFFFFF";}
if (!$color_titulo_back) { $color_titulo_back = "#000000";}
if (!$color_finsemana) { $color_finsemana = "#FF0000";}
if (!$color_finsemana_back) { $color_finsemana_back = "";}
if (!$color_evento1) { $color_evento1 = "#000000";}
if (!$color_evento1_back) { $color_evento1_back = "#00FF00";}
if (!$color_evento2) { $color_evento2 = "#FFFFFF";}
if (!$color_evento2_back) { $color_evento2_back = "#0000FF";}
//////
////// Variables
/*
$mes_calendario = $mes_bono;
$ano_calendario = $ano_bono;
$evento1 = $dias_no_laborados;
$evento12 = $dias_no_laborados2;
$evento2 = $dias_feriados;
$evento22 = $dias_feriados2;*/
$dias_mes = dias_mes ($mes_calendario,$ano_calendario);
//////
?>
<table border="1" align="right" cellpadding="2" cellspacing="0" bordercolor="<? echo $color_calendario_border; ?>" bgcolor="<? echo $color_calendario_back;?>" style="color:<? echo $color_calendario;?>">
   <tr><td colspan="7"><table width="100%" cellpadding="2" cellspacing="0" border="0">
   <tr bgcolor="<? echo $color_titulo_back;?>" style="color:<? echo $color_titulo;?>">
      <form id="atras" name="atras" method="post" action="">
      <td width="1px">
         <?php if ($desplazar_l==1) 
         { 
	   $mes_cal_ant = $mes_calendario - 1; 
	   $ano_cal_ant = $ano_calendario;
	   if ($mes_cal_ant==0) { $mes_cal_ant = 12;  $ano_cal_ant = $ano_calendario-1;}
           echo '<input type="hidden" name="mes_cal" id="mes_cal" value="'.$mes_cal_ant.'">
	   <input type="hidden" name="ano_cal" id="ano_cal" value="'.$ano_cal_ant.'">
           <input type="submit" name="atras" value="<">'; 
         } ?>
      </td></form>
      <td colspan="6" align="center" title="<?php escribir_mes($mes_calendario); echo ' '. redondear($ano_calendario,0,'.','').' '.$hoy;?>">
         <b><?php escribir_mes($mes_calendario); echo " ". redondear($ano_calendario,0,'.',''); 
            if ($hoy) { echo '<br>'.$hoy; }?></b>
      </td>
      <form id="adelante" name="adelante" method="post" action="">
      <td width="1px">
	<?php if ($desplazar_r==1) 
        {
	   $mes_cal_sig = $mes_calendario + 1; 
	   $ano_cal_sig = $ano_calendario;
	   if ($mes_cal_sig==13) { $mes_cal_sig = 1;  $ano_cal_sig = $ano_calendario+1;}
           echo '<input type="hidden" name="mes_cal" id="mes_cal" value="'.$mes_cal_sig.'">
	   <input type="hidden" name="ano_cal" id="ano_cal" value="'.$ano_cal_sig.'">
           <input type="submit" name="adelante" value=">">'; 
        } ?>
      </td></form>
   </tr>
   </table></td></tr>
   <tr style="font-size:10px" bgcolor="<? echo $color_nombre_dias_back;?>" >
      <?php for ($i=0; $i<=6; $i++) { echo "<td style='color:".$color_nombre_dias."' title='".$diasmes[$i]."'><B>".$diasmes[$i]."</B></td>"; }?>
   </tr>   <?php 
   // Imprime los d�as
   $dia_print = 1;
   $filas = 5;
   While ($dia_print<=$dias_mes) 
   {
      echo "<tr style='font-size:10px'>";
      for ($j=0; $j<=6; $j++) 
      {
         echo "<td align='center'>";
         $fecha_ini = $ano_calendario."-".$mes_calendario."-".$dia_print;
   	 $fechaComparacion = strtotime($fecha_ini);
         $calculo = $fechaComparacion;
         if ((date("w",$calculo) == $j) && ($dia_print <= $dias_mes)) 
         {
            //Si el d�a es Domingo o Sabado
            if ((date("w",$calculo) == 0) || (date("w",$calculo) == 6)) 
            {
               echo '<font color="'.$color_finsemana.'" style="font-style:italic; background-color:'.$color_finsemana_back.'">';
               $dias_pagar --;  
               $comentario = 'Fin de Semana';
	    } 
            else 
            { 
               if ($evento1) 
               {
                  $fecha1 = strtotime($fecha_ini);
		  $fecha1 = date("Y-m-d",$fecha1);
                  list($anio,$mes,$dia)=explode("-",$fecha1); 
	    	  $fecha1 = $dia."-".$mes."-".$anio; 
                  foreach($evento1 as $c=>$dia_no_laborado)
                  {
                     $fecha2 = strtotime($dia_no_laborado);
                     $fecha2 = date("Y-m-d",$fecha2);
                     list($anio,$mes,$dia)=explode("-",$fecha2); 
	    	     $fecha2 = $dia."-".$mes."-".$anio; 
                     $nds_resta = restaFechas($fecha1,$fecha2);
                     if  ($nds_resta==0) 
                     { 
                        $dias_no_pagar++;
                        echo '<font color="'.$color_evento1.'" style="background-color:'.$color_evento1_back.'">';
                        $comentario = $evento12[$c];
                     }
                  }  
               } 
               if ($evento2) 
               { 
                  $fecha1 = strtotime($fecha_ini);
                  $fecha1 = date("Y-m-d",$fecha1);
                  list($anio,$mes,$dia)=explode("-",$fecha1);
                  $fecha1 = $dia."-".$mes."-".$anio;
                  foreach($evento2 as $c=>$dia_feriado)
                  {
                     $fecha2 = strtotime($dia_feriado);
                     $fecha2 = date("Y-m-d",$fecha2);
                     list($anio,$mes,$dia)=explode("-",$fecha2);
                     $fecha2 = $dia."-".$mes."-".$anio;
                     $nds_resta = restaFechas($fecha1,$fecha2);
                     if  ($nds_resta==0) 
                     {
                        $dias_pagar--;
			echo '<font color="'.$color_evento2.'" style="font-style:italic; background-color:'.$color_evento2_back.'">';
                        $comentario = $evento22[$c];
                     }
                  }  
               } 
               else 
               { 
                  echo '<font color="'.$color_calendario.'">'; 
               }
            } 
            echo '<a title="'.$comentario.'">&nbsp;&nbsp;&nbsp;'.$dia_print.'&nbsp;&nbsp;&nbsp;</a></font>';
	    $comentario = '';
	    $dia_print++;
	 } 
         else 
         {
            echo '&nbsp;'; 
         }         echo "</td>"; 
      } 
      echo"</tr>"; 
   } ?>
</table>
<?php //// fin de calendario?>
