<?php 
/////////////
/* funcion que valida el tipo de caracteres que continene los distintos campos
	los parametros que ésta utiliza son: 
	$valor:  valor del dato
	$etiqueta: La etiqueta que acompaña el campo que contiene el dato
	$tipo: El tipo de datos que es permitido para el dato.
		Los tipos que se permiten son: 
		alfabeticos, numericos, login, fecha, alfanumericos, email
	$min: La longitud mínima del dato
	$max: La longitud máxima del dato*/

function validar_tipocampo ($dataavalidar){
	$nombre = $dataavalidar[0];	//nombre del campo
	$etiqueta = $dataavalidar[1];	//etiqueta del campo 
	$valor = $dataavalidar[2];	//valor del campo
	$min = $dataavalidar[3];	//longitud minima
	$max = $dataavalidar[4];	//longitud Maxima
	$tipo = $dataavalidar[5];	//tipo de datos

	// verificación de caracteres numéricos (0-9)
	if ($tipo == "numericos"){
	$expresion = "/^[\d]{".$min.",$max}$/"; 
	} 
	// verificación de caracteres decimales (0-9.)
	if ($tipo == "decimal"){
	$expresion = "/^[\d.]{".$min.",$max}$/";
	$mensaje_val = "El formato permitido es: 123456789.12";
	} 
	// verificación de caracteres login (a-z0-9_.-)
	if ($tipo == "login"){
	$expresion = "/^[\w.-]{".$min.",$max}$/i";
	} 
	// verificación de caracteres alfabéticos (a-z áéíóú)
	if ($tipo == "alfabeticos"){
	$expresion = "/^[\D]{".$min.",$max}$/i";
	} 
	// verificación de caracteres alfanuméricos (cualquier)
	if ($tipo == "alfanumericos"){
	$expresion ="/^[\W\w]{".$min.",$max}$/"; 
	} 
	// verificación de caracteres para fechas (dd/mm/aaaa ó dd-mm-aaaa)
	if ($tipo == "fecha"){
		if ($valor)
		{
			$min = 10;
		} 
		if ($min>0)
		{
			$min = 10;
		} 
		$valor = str_replace("-","/",$valor);
		$valor = str_replace("-","/",$valor);
		$expresion ="/^(\d{4}\/\d{2}\/\d{2})|(\d{4}\-\d{2}\-\d{2}){".$min.",$max}$/"; 
		$mensaje_val = "El formato permitido es: aaaa/mm/dd";
	} 
	// verificación de caracteres para fechas de inicio de la incidencia (dd/mm/aaaa ó dd-mm-aaaa)
	if ($tipo == "fecha_incidencia_inicio"){
		if ($valor)
		{
			$min = 10;
		} 
		if ($min>0)
		{
			$min = 10;
		} 
		$valor = str_replace("-","/",$valor);
		$valor = str_replace("-","/",$valor);
		$expresion ="/^(\d{4}\/\d{2}\/01)|(\d{4}\-\d{2}\-01){".$min.",$max}$/"; 
		$mensaje_val = "Debes seleccionar el 1er dia del mes";
		$tipo = "fecha de inicio de incidencia";
	} 
	// verificación de caracteres para fechas de fin de la incidencia (dd/mm/aaaa ó dd-mm-aaaa)
	if ($tipo == "fecha_incidencia_fin"){
		if ($valor)
		{
			$min = 10;
		} 
		if ($min>0)
		{
			$min = 10;
		}
		$mes=substr($valor, 5, 2);
		$ano=substr($valor, 0, 4);
		$valor = str_replace("-","/",$valor);
		$valor = str_replace("-","/",$valor);
		$diasdelmes = dias_mes ($mes,$ano);
		$expresion ="/^(\d{4}\/\d{2}\/".$diasdelmes.")|(\d{4}\-\d{2}\-".$diasdelmes."){".$min.",$max}$/"; 
		$mensaje_val = "Debes seleccionar el ultimo dia del mes";
		$tipo = "fecha de fin de incidencia";
	} 
	//verificación de correo electrónico
	if ($tipo == "email"){
		if ($valor)
		{
			$email_ver =  filter_var($valor, FILTER_VALIDATE_EMAIL);
			if (! $email_ver)
			{
				echo '<SCRIPT>alert("El campo '.$etiqueta.' solo admite el siguiente formato: \n\n';
				echo 'tucorreo@tudominio.xxx \n\n");</SCRIPT>';
				return 0;
			} 
		}
		if (($min>0)&&(!$valor))
		{
			echo '<SCRIPT>alert("El campo '.$etiqueta.' solo admite el siguiente formato: \n\n';
			echo 'tucorreo@tudominio.xxx \n\n';
			echo 'Y es un dato obligatorio");</SCRIPT>';
			return 0;
		}
	}
	// ejecución de la verificación del campo
	if ($tipo != "email") 
	{
		$validacion = preg_match($expresion, $valor);
		if ($validacion == 0) 
		{	
			echo '<SCRIPT>alert("El campo '.$etiqueta.' solo admite caracteres '.$tipo.'.';
			if ($min >= "1") { echo '\nY debe contener entre '.$min.' y '.$max.'  caracteres.'; } echo '\n'.$mensaje_val.'");</SCRIPT>';
			return 0; 
		}
	}
return 1;
}
?>
