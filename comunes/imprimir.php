<style type="text/css">
    @media print {
        .print_oculto {
            display:none
        }
    }
    #print_boton {	
        position:absolute;
        z-index:1;
        align: right;
        top: 10px;
        border: 0px solid #000000;
        width: <?php echo $ancho_div_boton; ?>;
        text-align: right;
    }
</style>
<div id="print_boton" class="print_oculto">
<input type="image" src="../imagenes/imprimir.png" id="imprimir" name="imprimir" value="imprimir" title="<?php echo $msg_compromiso_imprimir; ?>" onclick="window.print();"></div>
